﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;


public enum BroadcastMode
{
    send = 0,
    receive = 1,
    unknown = 2
}
public enum BroadcastState
{
    inactive = 0,
    active = 1
}

internal class Example : MonoBehaviour
{
    //private string s_Region;
    //private string s_UUID;
    //private string s_Major;
    //private string s_Minor;

    private BeaconType bt_Type;

    private BroadcastMode bm_Mode;

    private BroadcastState bs_State;

    // GameObject for found Beacons
    //[SerializeField]
    //private GameObject go_ScrollViewContent;

    [SerializeField] GameObject ObjectCube = null;
    [SerializeField] GameObject BeaconCamera = null;
    float currentTime = 0;
    [SerializeField] float spawnDelay = 0;
    //private GameObject go_FoundBeacon;
    //List<GameObject> go_FoundBeaconCloneList = new List<GameObject>();
    GameObject go_FoundBeaconClone;
    private float f_ScrollViewContentRectWidth;
    private float f_ScrollViewContentRectHeight;
    private int i_BeaconCounter = 0;
    [SerializeField] List<GameObject> CubeInGame = new List<GameObject>();

    // Receive
    public List<Beacon> mybeacons = new List<Beacon>();

    private void Start()
    {
        ChangeUUID();
        bm_Mode = BroadcastMode.receive;
        setBeaconPropertiesAtStart(); // please keep here!
        BluetoothState.BluetoothStateChangedEvent += delegate (BluetoothLowEnergyState state)
        {
            StartCoroutine(WaitBlueToothOn(state));
            switch (state)
            {
                case BluetoothLowEnergyState.TURNING_OFF:
                    break;
                case BluetoothLowEnergyState.TURNING_ON:
                    break;
                case BluetoothLowEnergyState.UNKNOWN:
                case BluetoothLowEnergyState.RESETTING:
                    Debug.LogWarning("Checking Device…");
                    break;
                case BluetoothLowEnergyState.UNAUTHORIZED:
                    Debug.LogWarning("You don't have the permission to use beacons.");
                    break;
                case BluetoothLowEnergyState.UNSUPPORTED:
                    Debug.LogWarning("Your device doesn't support beacons.");
                    break;
                case BluetoothLowEnergyState.POWERED_OFF:
                    Debug.LogWarning("Enable Bluetooth");
                    //_bluetoothText.text = "Enable Bluetooth";
                    break;
                case BluetoothLowEnergyState.POWERED_ON:
                    Debug.LogWarning("Bluetooth already enabled");
                    //_bluetoothText.text = "Bluetooth already enabled";
                    break;
                case BluetoothLowEnergyState.IBEACON_ONLY:
                    Debug.LogWarning("iBeacon only");
                    //_bluetoothText.text = "iBeacon only";
                    break;
                default:
                    Debug.LogError("Unknown Error");
                    break;
            }
        };
        //f_ScrollViewContentRectWidth = ((RectTransform)go_FoundBeacon.transform).rect.width;
        //f_ScrollViewContentRectHeight = ((RectTransform)go_FoundBeacon.transform).rect.height;
        BluetoothState.Init();
    }

    IEnumerator WaitBlueToothOn(BluetoothLowEnergyState state)
    {
        while (state == BluetoothLowEnergyState.TURNING_OFF)
        {
            Debug.LogError("偵測藍芽");
            yield return new WaitForSeconds(1f);
        }
        Debug.LogError(state);
        Debug.LogError("開啟藍芽");
        btn_StartStop();
        ScanBeacons();
    }

    private void setBeaconPropertiesAtStart()
    {
        if (bm_Mode == BroadcastMode.unknown)
        { // first start
            bm_Mode = BroadcastMode.receive;
            bt_Type = BeaconType.iBeacon;
        }
        bs_State = BroadcastState.inactive;
        bt_Type = BeaconType.iBeacon;
        Debug.Log("Beacon properties and modes restored");
    }

    // Beacon Properties
    void ChangeUUID()
    { // onRelease
        //s_Region = "games";
        //bt_Type = BeaconType.iBeacon;
        ////s_UUID = "fda50693-a4e2-4fb1-afcf-c6eb07647825";
        //s_UUID = "74278bda-b644-4520-8f0c-720eaf059935";
        //s_Major = "10001";
        //s_Minor = "19641";
        Debug.Log("Change UUID");
        bt_Type = BeaconType.iBeacon;
    }

    // BroadcastState
    public void btn_StartStop()
    {
        Debug.Log("開始偵測");
        //Debug.Log("Button Start / Stop pressed");
        /*** Beacon will start ***/
        if (bs_State == BroadcastState.inactive)
        {
            // ReceiveMode
            if (bm_Mode == BroadcastMode.receive)
            {
                iBeaconReceiver.BeaconRangeChangedEvent += OnBeaconRangeChanged;
                // check if all mandatory propertis are filled
                //if (s_Region == null || s_Region == "")
                //{
                //    //input_Region.image.color = Color.red;
                //    return;
                //}
                //else
                //{
                //    if (s_UUID == null || s_UUID == "")
                //    {
                //        //input_UUID.image.color = Color.red;
                //        return;
                //    }
                if (bt_Type == BeaconType.iBeacon)
                {
                    // 重點
                    //string[] beaconsuuid = MissionManager.Instance.beaconDic.GetAllBeacons();
                    //iBeaconReceiver.regions = new iBeaconRegion[beaconsuuid.Length];
                    //int[] beaconsmajor = MissionManager.Instance.beaconDic.GetAllMajors();
                    //int[] beaconsminor = MissionManager.Instance.beaconDic.GetAllMinors();
                    //for (int i = 0; i < beaconsuuid.Length; i++)
                    //{
                    //    SpawnCube(beaconsuuid[i]);
                    //    Debug.Log("UUID : " + beaconsuuid[i] + " beaconmajor : " + beaconsmajor[i] + " beaconsminor : " + beaconsminor[i]);
                    //    //iBeaconReceiver.regions[i] = new iBeaconRegion(s_Region, new Beacon(beaconsuuid[i], Convert.ToInt32(beaconsmajor[i]), Convert.ToInt32(beaconsminor[i])));
                    //    iBeaconReceiver.regions[i] = new iBeaconRegion(s_Region, new Beacon(beaconsuuid[i]));
                    //}
                    iBeaconReceiver.regions = new iBeaconRegion[] { new iBeaconRegion("game", new Beacon()) };
                }
                //}
                // !!! Bluetooth has to be turned on !!! TODO
                iBeaconReceiver.Scan();
                Debug.Log("Listening for beacons");
            }
            bs_State = BroadcastState.active;
        }
        //else
        //{
        //    if (bm_Mode == BroadcastMode.receive)
        //    {// Stop for receive
        //        iBeaconReceiver.Stop();
        //        iBeaconReceiver.BeaconRangeChangedEvent -= OnBeaconRangeChanged;
        //        removeFoundBeacons();
        //    }
        //    else
        //    { // Stop for send
        //        iBeaconServer.StopTransmit();
        //    }
        //    bs_State = BroadcastState.inactive;
        //}
    }

    void ScanBeacons()
    {
        //Debug.Log("Listening for beacons");
        //iBeaconReceiver.Scan();
        btn_StartStop();
    }

    private void OnBeaconRangeChanged(Beacon[] beacons)
    { // 
        foreach (Beacon b in beacons)
        {
            Debug.Log(b.UUID);
            var index = mybeacons.IndexOf(b);
            if (index == -1)
            {
                mybeacons.Add(b);
            }
            else
            {
                mybeacons[index] = b;
            }
        }
        for (int i = mybeacons.Count - 1; i >= 0; --i)
        {
            if (mybeacons[i].lastSeen.AddSeconds(10) < DateTime.Now)
            {
                mybeacons.RemoveAt(i);
            }
        }
        DisplayOnBeaconFound();
    }

    private void DisplayOnBeaconFound()
    {
        removeFoundBeacons();
        //RectTransform rt_Content = (RectTransform)go_ScrollViewContent.transform;
        if (currentTime < Time.time)
        {
            foreach (Beacon b in mybeacons)
            {
                CheckSpawnCube(b.UUID);
            }
            currentTime = Time.time + spawnDelay;
        }
    }

    private void removeFoundBeacons()
    {
        Debug.Log("removing all found Beacons");
        // set scrollviewcontent to standardsize
        //RectTransform rt_Content = (RectTransform)go_ScrollViewContent.transform;
        //rt_Content.sizeDelta = new Vector2(f_ScrollViewContentRectWidth, f_ScrollViewContentRectHeight);
        // destroying each clone
        //foreach (GameObject go in go_FoundBeaconCloneList)
        //    Destroy(go);
        //go_FoundBeaconCloneList.Clear();
        i_BeaconCounter = 0;
    }

    private void OnEnable()
    {
        ScanBeacons();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            //SpawnCube(MissionManager.Instance.beaconDic.GetBeaconNumber("b9407f30-f5f8-466e-aff9-25556b57fe6d"));
            //SpawnCube(MissionManager.Instance.beaconDic.GetBeaconNumber("aaaaaaaa-bbbb-cccc-ffff-111122223333"));
            SpawnCube(MissionManager.Instance.beaconDic.GetBeaconNumber("BBBBBBBB-1111-2222-3333-000000001111"));
            Debug.Log("生成");
        }
    }

    void CheckSpawnCube(string beaconUUID)
    {
        string[] BeaconNumbers = MissionManager.Instance.beaconDic.GetBeaconNumber(beaconUUID);
        if (BeaconNumbers != null)
        {
            SpawnCube(BeaconNumbers);
        }
    }

    void SpawnCube(string[] letters)
    {
        if (CubeInGame.Count < 40)
        {
            GameObject obj = Instantiate(ObjectCube, BeaconCamera.transform.position + BeaconCamera.transform.forward * 5000 + new Vector3(UnityEngine.Random.Range(3927, -1403), 0, UnityEngine.Random.Range(1532, -1411)), ObjectCube.transform.rotation);
            LetterCubeController cubescript = obj.GetComponent<LetterCubeController>();
            int collisioncount = 0;
            while (cubescript.CheckCollision() == true && collisioncount < 3)
            {
                if (collisioncount == 3)
                {
                    Destroy(obj);
                    return;
                }
                collisioncount += 1;
                Debug.Log("碰撞");
                obj.transform.position = BeaconCamera.transform.position + BeaconCamera.transform.forward * 5000 + new Vector3(UnityEngine.Random.Range(3927, -1403), 0, UnityEngine.Random.Range(1532, -1411));
            }
            if (cubescript.CheckCollision())
            {
                Destroy(obj);
            }
            else
            {
                CubeInGame.Add(obj);
                GameManage.Instance.StartCoroutine(DeleteCube(obj));
                cubescript.enablecube();
                cubescript.SetLetter(letters);
            }
        }
    }

    IEnumerator DeleteCube(GameObject obj) 
    {
        yield return new WaitForSeconds(5);
        CubeInGame.Remove(obj);
        Destroy(obj);
    }
}
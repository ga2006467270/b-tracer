﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Debug.Log("開始偵測碰撞");
        if (Physics.OverlapBox(transform.position, transform.localScale / 2).Length>1)
        {
            Debug.Log("碰撞");
            transform.localPosition = new Vector3(Random.Range(-1150, 2000), -1000, Random.Range(-2350, 2100));
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LetterController : MonoBehaviour
{
    [SerializeField] Text mLetter = null;
    [SerializeField] Text mLetterAmountText = null;
    [HideInInspector] public string Letter;
    public int mLetterAmount;
    bool mSerilize; // 初始化

    public int LetterAmount
    {
        get
        {
            return mLetterAmount;
        }

        set
        {
            mLetterAmount = value;
            mLetterAmountText.text = value.ToString();
        }
    }

    public void SetLetter(string letter, int amount)
    {
        Letter = letter;
        LetterAmount = amount;
        mLetter.text = letter;
        mLetterAmountText.text = amount.ToString();
        //MissionManager.Instance.AddLetterDic(letter, this, amount);
        mSerilize = true;
    }

    public void UseLetterUI()
    {
        if (LetterAmount <= 0)
        {
            Debug.LogError("沒有足夠的單字");
            return;
        }
        else
        {
            bool LetterUse = CardManager.Instance.SpellLetter(mLetter.text);
            if (LetterUse)
            {
                LetterAmount -= 1;
                MissionManager.Instance.SetLetterAmount(Letter, LetterAmount);
            }
        }
    }

    public void AddLetterUI()
    {
        LetterAmount += 1;
        MissionManager.Instance.SetLetterAmount(mLetter.text, LetterAmount);
    }

    private void OnEnable()
    {
        if (mLetter.text != "" && mSerilize)
        {
            int amount = MissionManager.Instance.CheckLetter(mLetter.text);
            mLetterAmount = amount;
            mLetterAmountText.text = amount.ToString();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

public class GameManage : MonoBehaviour
{
    private static GameManage mInsatnce;

    public static GameManage Instance
    {
        get
        {
            if (mInsatnce == null)
            {
                Debug.LogError("無Instance");
            }
            return mInsatnce;
        }
    }

    public PlayerData ThePlayerData;
    [SerializeField] GameObject mShowRewardCard = null;
    [SerializeField] Image mRewardCardImg = null;
    [SerializeField] Text mRewardCardText = null;

    // Use this for initialization
    void Awake()
    {
        if (mInsatnce != null)
        {
            Debug.LogError("有兩個以上Insance");
            Destroy(gameObject);
        }
        else
        {
            mInsatnce = this;
        }
        //ObjectPool.Instance.
    }

    private void Start()
    {
        ResourceLoad.Instance.StartResourcesLoad();
        ObjectPool.Instance.SetObjectPool();
        ObjectPool.Instance.LoadObjectPool();
        StartCoroutine(StartGame());
        ObjectPool.Instance.GetObjectInGame("LoadingBar").SetActive(false);
    }

    public IEnumerator StartGame()
    {
        yield return StartCoroutine(LoginUI.Instance.ShowLogo());
        //Debug.Log("轉換至Login");
    }

    // 讀取排行榜
    // 讀取帳號的Collection
    // 開啟MissionPanel
    public void ChangeToGame()
    {
        PlayerMode.Instance.GameCanvasPanel.SetActive(true);
    }

    public void SavePlayerData(JSONNode result)
    {
        //ThePlayerData ;
        ThePlayerData.PlayerID = result["data"][0][0].AsInt;
        ThePlayerData.Coins = result["data"][0][2].AsInt;
        ThePlayerData.Name = result["data"][0][1].Value;
        ThePlayerData.finishedMission = result["data"][0][3].AsInt;
        ThePlayerData.SchoolID = result["data"][0][6].AsInt;
        PlayerMode.Instance.CheckPlayerCoins();
        PlayerMode.Instance.CheckPlayerName();
    }

    public void SaveAccountPrefab(string account, string password)
    {
        string path = Application.persistentDataPath + "/playerPassPrefab";
        using (FileStream stream = new FileStream(path, FileMode.Create))
        {
            BinaryFormatter formatter = new BinaryFormatter();

            PlayerData data = new PlayerData();
            data.SetPlayerData(account, password);
            if (ThePlayerData != null)
            {
                if (ThePlayerData.GetCardsText().Count > 0)
                {
                    data.SetCardsData(ThePlayerData.GetCardsText());
                }
            }
            formatter.Serialize(stream, data);
        }
        if (ThePlayerData == null)
        {
            ThePlayerData = new PlayerData();
        }
        ThePlayerData.SaveAccount = account;
        ThePlayerData.SavePassword = password;
    }

    public void SaveImageData(DownloadImage[] cards)
    {
        string path = Application.persistentDataPath + "/playerPassPrefab";
        using (FileStream stream = new FileStream(path, FileMode.Create))
        {
            BinaryFormatter formatter = new BinaryFormatter();

            PlayerData data = new PlayerData();
            Debug.LogError(ThePlayerData.SaveAccount);
            Debug.LogError(ThePlayerData.SavePassword);
            data.SetPlayerData(ThePlayerData.SaveAccount, ThePlayerData.SavePassword);
            data.SetCardsData(cards, false);
            formatter.Serialize(stream, data);
        }
    }

    public void ShowCard(Sprite mCard, string mName)
    {
        mShowRewardCard.gameObject.SetActive(true);
        mRewardCardImg.sprite = mCard;
        mRewardCardText.text = mName;
    }

    public void CloseRewardCard()
    {
        mShowRewardCard.gameObject.SetActive(false);
    }

    public void UseLoadingBar()
    {
        ObjectPool.Instance.GetObjectInGame("LoadingBar").SetActive(true);
    }

    public void LoadComplete()
    {
        ObjectPool.Instance.GetObjectInGame("LoadingBar").SetActive(false);
    }

    public static void SetSize(RectTransform scaleImage, float ORIGIANLE_SIZE)
    {
        var screenwidth = Screen.width;
        var screenheight = Screen.height;

        if (screenwidth > screenheight)
        {
            scaleImage.sizeDelta = new Vector2(scaleImage.sizeDelta.x * screenwidth / screenheight * ORIGIANLE_SIZE, scaleImage.sizeDelta.y);
        }
        else
        {
            Vector2 sizedata = new Vector2(scaleImage.sizeDelta.x, scaleImage.sizeDelta.y * screenheight / screenwidth / ORIGIANLE_SIZE);
            scaleImage.sizeDelta = sizedata;
        }
    }
}

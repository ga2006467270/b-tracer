﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BeaconLetterPanelController : MonoBehaviour
{
    public static BeaconLetterPanelController Instance;

    [SerializeField] GameObject mBeaconLetterPanel = null;
    [SerializeField] GameObject mBeaconLetter = null;
    List<BeaconLetter> mBeaconLetterScripts = new List<BeaconLetter>();
    bool mSerilize = false;
    // 接收missionManager資料並關閉
    void Awake()
    {
        Instance = this;
        mBeaconLetterPanel.SetActive(false);
        Debug.Log("接收missionManager資料並關閉");
        mSerilize = true;

    }

    // 重新確認資料
    private void OnEnable()
    {
        if (mSerilize)
        {
            CheckLetters();
        }
    }

    public void InitializeLetters()
    {
        List<LetterController> letters = MissionManager.Instance.CurrentMissionData.mLetters.GetLetters();
        for (int i = 0; i < letters.Count; i++)
        {
            GameObject beaconletterobj = Instantiate(mBeaconLetter, mBeaconLetterPanel.transform);
            BeaconLetter letter = beaconletterobj.GetComponent<BeaconLetter>();
            letter.SetLetter(letters[i].Letter, letters[i].mLetterAmount);
            mBeaconLetterScripts.Add(letter);
        }
    }

    public void CheckLetters()
    {
        if (mSerilize)
        {
            List<LetterController> letters = MissionManager.Instance.CurrentMissionData.mLetters.GetLetters();
            for (int i = 0; i < letters.Count; i++)
            {
                mBeaconLetterScripts[i].SetLetter(letters[i].Letter, letters[i].mLetterAmount);
            }
        }
    }

    public void ClearLetters()
    {
        mBeaconLetterScripts.Clear();
        foreach (BeaconLetter letter in mBeaconLetterPanel.GetComponentsInChildren<BeaconLetter>())
        {
            Destroy(letter.gameObject);
        }
    }
}

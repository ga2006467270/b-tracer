﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

// PlayerID
// PHPURL
// SchoolID
// 根據讀取到的長度調整Content

public class PlayerMode : MonoBehaviour
{
    enum IPlayerMode
    {
        Mission,
        Collection,
        Ranking,
        Shop,
    }

    private static PlayerMode mInstance;
    JSONNode mCardResult;
    int mCardUrlCount = 0;
    int mCardNameCount = 0;
    public string CardValue
    {
        get
        {
            if (mCardResult != null)
            {
                mCardUrlCount += 1;
                return mCardResult["output"][mCardUrlCount - 1][1].Value;
            }
            else
            {
                Debug.LogError("mCardResult == null");
                return null;
            }
        }
    }

    public string CardName
    {
        get
        {
            if (mCardResult != null)
            {
                mCardNameCount += 1;
                return mCardResult["output"][mCardNameCount - 1][2].Value;
            }
            else
            {
                Debug.LogError("mCardResult == null");
                return null;
            }
        }
    }

    int mCardDownloadCount = 0;
    public bool CheckLastCard
    {
        get
        {
            mCardDownloadCount += 1;
            if (mCardDownloadCount == (mCardResult["output"].Count))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    bool mCollectionComplete = false;
    bool mRankingComplete = false;
    bool mLoadPlayerDataComplete = false;

    JSONNode mRankResult;
    int mRankCount = 0;
    int mBestTracerRankCount = 0;
    public GameObject GameCanvasPanel;

    [SerializeField] IPlayerMode mPlayerMode;
    [Space]
    [SerializeField] GameObject[] mPanel = null;
    [SerializeField] GameObject[] mPanelButton = null;
    [Space]
    [SerializeField] GameObject mMissionButton = null;
    [SerializeField] MissionButtonData mMissionButtonData = null;
    [SerializeField] GameObject mResultButton = null;
    [SerializeField] MissionRecordButtonData recordData = null;
    [SerializeField] RectTransform mMissionContent = null;
    [SerializeField] RectTransform mMissionRecordContent = null;
    [SerializeField] GameObject mMissionView = null;
    [SerializeField] GameObject mMissionRecordView = null;
    [SerializeField] GameObject mMissionButtonOn = null;
    [SerializeField] GameObject mMissionButtonOff = null;
    [SerializeField] GameObject mMissionRecordButtonOn = null;
    [SerializeField] GameObject mMissionRecordButtonOff = null;
    [SerializeField] Dictionary<string, GameObject> mPanelDic = new Dictionary<string, GameObject>();
    [SerializeField] Dictionary<string, GameObject> mPanelButtonDic = new Dictionary<string, GameObject>();
    [Space]
    [SerializeField] RectTransform mCollectionContent = null;
    [SerializeField] GridLayoutGroup mRecordLayout = null;
    [Space]
    [SerializeField] RectTransform mWealthTracerRankContent = null;
    [SerializeField] LoadRank[] mWealthTracerRankPanel = null;

    [SerializeField] RectTransform mBestTracerRankContent = null;
    [SerializeField] LoadRank[] mBestTracerRankPanel = null;
    [SerializeField] LoadRank mPlayerRank = null;
    //[SerializeField] GameObject EnumImage = null;
    [SerializeField] RectTransform mBestTracerView = null;
    [SerializeField] RectTransform mWealthTracerView = null;
    [SerializeField] GameObject mBestTracerButtonOn = null;
    [SerializeField] GameObject mBestTracerButtonOff = null;
    [SerializeField] GameObject mWealthTracerButtonOn = null;
    [SerializeField] GameObject mWealthTracerButtonOff = null;
    [Space]
    [SerializeField] Text Money = null;
    [SerializeField] Text Name = null;
    [Space]
    [SerializeField] GridLayoutGroup CollectLayout = null;
    [SerializeField] GridLayoutGroup BestTracerLayoutGroup = null;
    [SerializeField] GridLayoutGroup WealthTracerLayoutGroup = null;
    [SerializeField] GridLayoutGroup MissionLayoutGroup = null;
    public static PlayerMode Instance
    {
        get
        {
            return mInstance;
        }
    }

    public bool CollectionComplete
    {
        get
        {
            return mCollectionComplete;
        }

        set
        {
            mCollectionComplete = value;
        }
    }

    public bool MRankingComplete
    {
        get
        {
            return mRankingComplete;
        }

        set
        {
            mRankingComplete = value;
        }
    }

    public bool LoadPlayerDataComplete
    {
        get
        {
            return mLoadPlayerDataComplete;
        }

        set
        {
            mLoadPlayerDataComplete = value;
        }
    }

    public int RankCount
    {
        get
        {
            return mRankCount;
        }

        set
        {
            mRankCount = value;
        }
    }

    public int BestTracerRankCount
    {
        get
        {
            return mBestTracerRankCount;
        }

        set
        {
            mBestTracerRankCount = value;
        }
    }

    Vector2 CollectionContentORect;
    private void Awake()
    {
        mInstance = this;
        foreach (GameObject obj in mPanel)
        {
            mPanelDic.Add(obj.name, obj);
        }
        foreach (GameObject obj in mPanelButton)
        {
            mPanelButtonDic.Add(obj.name, obj);
        }
        mPlayerMode = IPlayerMode.Mission;
        ChangeMode(mPlayerMode);
        Name.text = GameManage.Instance.ThePlayerData.Name;
        recordData = mResultButton.GetComponent<MissionRecordButtonData>();
        mMissionButtonData = mMissionButton.GetComponent<MissionButtonData>();
        CollectionContentORect = new Vector2(mCollectionContent.rect.width, mCollectionContent.rect.height);
    }

    public void CheckPlayerCoins()
    {
        Money.text = GameManage.Instance.ThePlayerData.Coins.ToString();
    }

    public void CheckPlayerName()
    {
        Name.text = GameManage.Instance.ThePlayerData.Name.ToString();
    }

    string SearchModePanel(IPlayerMode mode)
    {
        string UIname = null;
        switch (mode)
        {
            case IPlayerMode.Mission:
                UIname = "Mission";
                break;
            case IPlayerMode.Collection:
                UIname = "Collection";
                break;
            case IPlayerMode.Ranking:
                UIname = "Ranking";
                break;
            case IPlayerMode.Shop:
                UIname = "Shop";
                break;
            default:
                Debug.LogError("沒有這個Mode的設定");
                return null;
        }
        return UIname;
    }

    // Active Panel 並disActive原本的Panel
    void ChangeMode(IPlayerMode targetMode)
    {
        if (mPlayerMode == targetMode)
        {
            return;
        }
        Debug.Log("切換模式");
        mPlayerMode = targetMode;
        switch (targetMode)
        {
            case IPlayerMode.Mission:
                TurnMissionList();
                break;
            case IPlayerMode.Ranking:
                TurnWealthTracer();
                break;
            case IPlayerMode.Collection:
                TurnCollection();
                break;
        }
        //string closeUIname = SearchModePanel(currentMode);
        string activeUIname = SearchModePanel(targetMode);
        for (int i = 0; i < Enum.GetNames(typeof(IPlayerMode)).Length; i++)
        {
            if (mPlayerMode.ToString() == Enum.GetNames(typeof(IPlayerMode))[i])
            {
                continue;
            }
            else
            {
                Debug.Log("Enum Name : " + Enum.GetNames(typeof(IPlayerMode))[i]);
                SetActiveUI(Enum.GetNames(typeof(IPlayerMode))[i], false);
            }
        }
        Debug.Log(activeUIname);
        SetActiveUI(activeUIname, true);
    }

    // 轉換自身的Active，if true 則變 false，反之亦然
    public void SetActiveUI(string UIName, bool activeMode)
    {
        if (mPanelDic[UIName].activeSelf == false && activeMode == false)
        {
            return;
        }
        mPanelDic[UIName].SetActive(activeMode);
        Debug.Log(UIName + "Button");
        foreach (Image img in mPanelButtonDic[UIName + "Button"].GetComponentsInChildren<Image>())
        {
            if (img.gameObject.name == "EnumImage")
            {
                img.enabled = activeMode;
            }
        }
    }

    public void SetActiveUI(string UIName)
    {
        mPanelDic[UIName].SetActive(!mPanelDic[UIName].activeSelf);
    }

    public void ChangeToMissionMode()
    {
        Debug.Log("Open Mission UI");
        ChangeMode(IPlayerMode.Mission);
    }

    public void ChangeToCollectionMode()
    {
        Debug.Log("Open Collection UI");
        ChangeMode(IPlayerMode.Collection);
    }

    public void ChangeToRankingMode()
    {
        Debug.Log("Open Ranking UI");
        ChangeMode(IPlayerMode.Ranking);
    }

    public void ChangeToShopMode()
    {
        Debug.Log("Open Shop UI");
        //ChangeMode(IPlayerMode.Shop);
    }

    public void ChangeToGuidesMode()
    {
        Debug.Log("Open Guides UI");
        //ChangeMode(IPlayerMode.Guides);
    }

    public void ActiveLogout()
    {
        SetActiveUI("Logout");
        Debug.Log("Logout");
    }

    public void Logout()
    {
        Debug.Log("Back to LoginCanvas");
        Debug.Log("清除Ranking 清除 Collection 清除Mission 關閉UI 清除bool");
        SetActiveUI("Logout");
        ClearGameCanvasData();
        GameManage.Instance.ThePlayerData.Clear();
        LoginUI.Instance.SetLoginUI(true);
        LeavePlayMode();
    }

    public void LeavePlayMode()
    {
        ChangeMode(IPlayerMode.Mission);
        mCollectionContent.sizeDelta = new Vector2(0, 0);
    }

    public void ClearGameCanvasData()
    {
        // 刪除missionbtn
        foreach (MissionButtonData btndata in mMissionContent.GetComponentsInChildren<MissionButtonData>())
        {
            Destroy(btndata.gameObject);
        }
        foreach (MissionRecordButtonData btndata in mMissionRecordContent.GetComponentsInChildren<MissionRecordButtonData>())
        {
            Destroy(btndata.gameObject);
        }
        // 刪除Rankingbtn
        //foreach (LoadRank rankdata in mBestTracerRankContent.GetComponentsInChildren<LoadRank>())
        //{
        //    Destroy(rankdata.gameObject);
        //}
        //foreach (LoadRank rankdata in mWealthTracerRankContent.GetComponentsInChildren<LoadRank>())
        //{
        //    Destroy(rankdata.gameObject);
        //}
        // 刪除collectionbtn 
        DeleteCard();
        Resources.UnloadUnusedAssets();
        CloseGameCanvas();
        SetActiveUI("Collection", true);
        mCardNameCount = 0;
        mCardUrlCount = 0;
        mRankCount = 0;
        mBestTracerRankCount = 0;
        mCardResult = null;
        mRankResult = null;
        MRankingComplete = false;
        CollectionComplete = false;
        mCardDownloadCount = 0;
    }

    void DeleteCard()
    {
        foreach (DownloadImage Imgdata in mCollectionContent.GetComponentsInChildren<DownloadImage>())
        {
            Destroy(Imgdata.gameObject);
        }
    }

    public void CloseGameCanvas()
    {
        ChangeMode(IPlayerMode.Mission);
        GameCanvasPanel.SetActive(false);
    }

    public void OpenGameCanvas()
    {
        GameCanvasPanel.SetActive(true);
    }

    // 讀取Collection
    public void LoadData(int userID)
    {
        StartCoroutine(LoadCards());
        StartCoroutine(LoadMissionList());
        StartCoroutine(LoadFinishRecord());
        StartCoroutine(loadRanking());
        StartCoroutine(CheckLoading());
    }

    bool LoadCardsFromNet = true;
    IEnumerator LoadCards()
    {
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();
        JSon.Add("toDo", "loadCards");
        JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        form.AddField("response", JSon.ToString());
        using (UnityWebRequest sendWWW = UnityWebRequest.Post(MyVar.PHPURL, form))
        {
            yield return sendWWW.SendWebRequest();
            if (sendWWW.isNetworkError || sendWWW.isHttpError)
            {
                print("error: " + sendWWW.error);
                yield return StartCoroutine(LoadCards());
            }
            else
            {
                mCardResult = JSONNode.Parse(sendWWW.downloadHandler.text);
                Debug.Log(mCardResult["output"]);
                if (mCardResult["output"].Count > 0)
                {
                    DownloadImage[] cards = new DownloadImage[mCardResult["output"].Count];

                    Debug.Log(mCardResult["output"].Count);
                    // 如果output的數量跟內存相同,則讀內存
                    if (mCardResult["output"].Count == GameManage.Instance.ThePlayerData.GetCardsText().Count)
                    {
                        Debug.LogError("進入資料夾存取");
                        LoadCardsFromNet = false;
                        for (int i = 0; i < GameManage.Instance.ThePlayerData.GetCardsText().Count; i++)
                        {
                            cards[i] = Instantiate(ObjectPool.Instance.LoadObject("CardImage"), mCollectionContent.transform).GetComponent<DownloadImage>();
                            StartCoroutine(LoadCollectionImgFromCache(cards[i], mCardResult["output"][i][1].Value, mCardResult["output"][i][2].Value));
                            yield return new WaitForSeconds(0.1f);
                        }
                    }
                    else
                    {
                        LoadCardsFromNet = true;
                        //GameManage.Instance.ThePlayerData.SetCardsData(mCardResult["output"].Count);
                        Debug.LogError("output.Count : " + mCardResult["output"].Count);
                        Debug.LogError("GetCardsText : " + GameManage.Instance.ThePlayerData.GetCardsText().Count);
                        for (int i = 0; i < mCardResult["output"].Count; i++)
                        {
                            cards[i] = Instantiate(ObjectPool.Instance.LoadObject("CardImage"), mCollectionContent.transform).GetComponent<DownloadImage>();
                            StartCoroutine(DownloadCollectionImg(cards[i], mCardResult["output"][i][1].Value, mCardResult["output"][i][2].Value));
                            yield return new WaitForSeconds(0.2f);
                        }
                    }
                    StartCoroutine(CheckCardDownload(cards));
                }
                else
                {
                    CollectionComplete = true;
                }
                SetActiveUI("Collection", false);
            }
        }
    }

    IEnumerator CheckCardDownload(DownloadImage[] cards)
    {
        int cardscount = 0;
        int cardsTimeCount = 0;
        bool overtime = false;
        foreach (DownloadImage img in cards)
        {
            while (img.CardInstantiate == false && overtime == false)
            {
                // 待修正
                Debug.LogError("等待讀卡 : " + cardsTimeCount + " 秒鐘");
                yield return new WaitForSeconds(1f);
                cardsTimeCount += 1;
                if (cardsTimeCount > 30)
                {
                    LoadCards();
                    Debug.LogError("重新讀卡");
                    DeleteCard();
                    StartCoroutine(LoadCards());
                    overtime = true;
                }
            }
            if (overtime == false)
            {
                cardscount += 1;
                if (cardscount == cards.Length)
                {
                    if (LoadCardsFromNet)
                    {
                        GameManage.Instance.ThePlayerData.ClearCardsData();
                        GameManage.Instance.SaveImageData(cards);
                        GameManage.Instance.ThePlayerData.SetCardsData(cards, true);
                    }
                    else
                    {
                        //GameManage.Instance.ThePlayerData.SetCardsData(cards, false);
                    }
                    Debug.LogError("完成讀卡");
                    ResizeCardLength();
                    CollectionComplete = true;
                }
            }
        }
    }

    void ResizeCardLength()
    {
        int cardSize = (int)mCollectionContent.rect.width / (int)CollectLayout.cellSize.x;
        float totalCardSize = mCardResult["output"].Count % cardSize + mCardResult["output"].Count / cardSize;
        float ContentSize = totalCardSize * (CollectLayout.cellSize.y + CollectLayout.spacing.y);
        ContentSize -= mCollectionContent.rect.height;
        Debug.LogError(ContentSize);
        if (ContentSize > 0)
        {
            mCollectionContent.sizeDelta = new Vector2(0, ContentSize);
        }
        else
        {
            mCollectionContent.sizeDelta = new Vector2(0, 0);
            Debug.Log(ContentSize);
            Debug.Log(cardSize);
            Debug.Log(CollectLayout.cellSize.y / CollectLayout.cellSize.x);
            Debug.Log(mCardResult["output"].Count);
        }
    }

    public IEnumerator DownloadCollectionImg(DownloadImage img, string cardurl, string cardname)
    {
        string cardURL = cardurl;
        string path = null;
        //Debug.Log(cardURL.Length);
        if (cardURL.Length > 4)
        {
            if (cardURL[cardURL.Length - 4].ToString() == ".")
            {
                path = "https://puzzler.bds.hk/" + "picture/" + cardURL;
            }
            else
            {
                path = "https://puzzler.bds.hk/" + "picture/" + cardURL + ".jpg";
            }
        }
        else
        {
            path = "https://puzzler.bds.hk/" + "picture/" + cardURL + ".jpg";
        }
        using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(path))
        {
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.LogError("error: " + www.error);
                Debug.LogError(path);
                StartCoroutine(DownloadCollectionImg(img, cardurl, cardname));
            }
            else
            {
                Texture2D texture = DownloadHandlerTexture.GetContent(www);

                Sprite image = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                img.SetCard(cardname, image);
            }
            www.Dispose();
        }
    }

    public IEnumerator LoadCollectionImgFromCache(DownloadImage img, string cardurl, string cardname)
    {
        // 從streamasset找出path
        string path = cardname;
        //Debug.LogError(Application.persistentDataPath + "/" + cardname);
        string finalpath = null;
#if UNITY_ANDROID
        finalpath = "file://" + Application.persistentDataPath + "/" + cardname + ".png";
#endif
#if UNITY_IPHONE
        finalpath = "file://" + Application.persistentDataPath + "/" + cardname + ".png";
#endif
#if UNITY_EDITOR
        finalpath = Application.persistentDataPath + "/" + cardname + ".png";
#endif
        using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(finalpath))
        {
            yield return www.SendWebRequest();
            if (www.isHttpError || www.isNetworkError)
            {
                Debug.LogError("error: " + www.error);
                Debug.LogError(finalpath);
                StartCoroutine(LoadCollectionImgFromCache(img, cardurl, cardname));
            }
            else
            {
                Texture2D texture = DownloadHandlerTexture.GetContent(www);

                Sprite image = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                img.SetCard(cardname, image);
            }
            www.Dispose();
        }
    }

    IEnumerator LoadMissionList()
    {
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();
        JSon.Add("toDo", "loadMissionList");
        JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        form.AddField("response", JSon.ToString());
        Debug.LogError("loadMissionList : " + JSon.ToString());
        using (UnityWebRequest sendWWW = UnityWebRequest.Post(MyVar.PHPURL, form))
        {
            yield return sendWWW.SendWebRequest();
            if (sendWWW.isNetworkError || sendWWW.isHttpError)
            {
                //GameObject.Find ("Warning").GetComponent<Text>().text="error: "+sendWWW.error;
                print("error: " + sendWWW.error);
                yield return StartCoroutine(LoadMissionList());
            }
            else
            {
                print(sendWWW.downloadHandler.text);
                var Result = JSONNode.Parse(sendWWW.downloadHandler.text);

                LoadMissionFromCache(Result);
            }
        }
    }

    public void LoadMissionFromCache(JSONNode node)
    {
        for (int i = 0; i < node["do"].Count; i++)
        {
            mMissionButtonData.SetMission(node["do"][i][1].Value, node["do"][i][0].AsInt, node["do"][i][3].AsInt, node["do"][i][4].AsInt, true);
            Instantiate(mMissionButton, mMissionContent.transform);
            //MissionButtonData data = missionbutton.GetComponent<MissionButtonData>();
        }
        for (int i = 0; i < node["yet_do"].Count; i++)
        {
            mMissionButtonData.SetMission(node["yet_do"][i][1].Value, node["yet_do"][i][0].AsInt, node["yet_do"][i][3].AsInt, node["yet_do"][i][4].AsInt, false);
            Instantiate(mMissionButton, mMissionContent.transform);
            //MissionButtonData data = missionbutton.GetComponent<MissionButtonData>();
        }
        if (node["do"].Count + node["yet_do"].Count > 11)
        {
            Debug.LogError("錯誤");
            mMissionRecordContent.sizeDelta = new Vector2(0, ((node["do"].Count + node["yet_do"].Count) - 10) * 118f + 10f);
        }
        else
        {
            mMissionContent.sizeDelta = new Vector2(0, 0);
        }
        MissionLayoutGroup.cellSize = new Vector2(mPlayerRank.GetWidth().rect.width, MissionLayoutGroup.cellSize.y);
    }

    IEnumerator LoadFinishRecord()
    {
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();
        JSon.Add("toDo", "loadFinishedRecord");
        JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        form.AddField("response", JSon.ToString());
        using (UnityWebRequest sendWWW = UnityWebRequest.Post(MyVar.PHPURL, form))
        {
            yield return sendWWW.SendWebRequest();
            if (sendWWW.isNetworkError || sendWWW.isHttpError)
            {
                //GameObject.Find ("Warning").GetComponent<Text>().text="error: "+sendWWW.error;
                print("error: " + sendWWW.error);
                yield return StartCoroutine(LoadFinishRecord());
            }
            else
            {
                Debug.Log(sendWWW.downloadHandler.text);
                var Result = JSONNode.Parse(sendWWW.downloadHandler.text);

                LoadFinishRecordFromCache(Result);
            }
        }
    }

    public void LoadFinishRecordFromCache(JSONNode node)
    {
        mRecordLayout.cellSize = new Vector2(mPlayerRank.GetWidth().rect.width, mRecordLayout.cellSize.y);
        for (int i = 0; i < node["result"].Count; i++)
        {
            recordData.SetMission(node["result"][i][0].Value, node["result"][i][4].AsInt);
            Instantiate(mResultButton, mMissionRecordContent.transform);
            //MissionRecordButtonData data = resultButton.GetComponent<MissionRecordButtonData>();
            //data.SetMission(node["result"][i][0].Value, node["result"][i][4].AsInt);
        }
        if (node["result"].Count > 11)
        {
            mMissionRecordContent.sizeDelta = new Vector2(0, (node["result"].Count - 11) * (118f) + 10f);
        }
        else
        {
            mMissionRecordContent.sizeDelta = new Vector2(0, 0);
        }
    }

    IEnumerator loadRanking()
    {
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();
        JSon.Add("toDo", "loadRanking");
        JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());


        JSon.Add("schoolID", GameManage.Instance.ThePlayerData.SchoolID.ToString());
        form.AddField("response", JSon.ToString());
        using (UnityWebRequest sendWWW = UnityWebRequest.Post(MyVar.PHPURL, form))
        {
            yield return sendWWW.SendWebRequest();
            if (sendWWW.error != null)
            {
                print("error: " + sendWWW.error);
                yield return StartCoroutine(loadRanking());
            }
            else
            {
                print(sendWWW.downloadHandler.text);
                var Result = JSONNode.Parse(sendWWW.downloadHandler.text);
                mRankResult = Result;
                Debug.Log(Result["missionList"]);
                for (int i = 0; i < Result["missionList"].Count; i++)
                {
                    if (i < 10)
                    {
                        mWealthTracerRankPanel[i].SetRankMessage(i + 1, mRankResult["missionList"][i]);
                        //Instantiate(ObjectPool.Instance.LoadObject("BestTracerRankPanel"), mBestTracerRankContent.transform);
                    }
                    if (Result["missionList"][i][0].AsInt == GameManage.Instance.ThePlayerData.PlayerID)
                    {
                        if (Result["missionList"][i][3].AsInt > 0)
                        {
                            mPlayerRank.SetTracer(Result["missionList"][i][1], Result["missionList"][i][3].AsInt, i + 1);
                            GameManage.Instance.ThePlayerData.BestTracerMoney = Result["missionList"][i][3].AsInt;
                            GameManage.Instance.ThePlayerData.BestTracerRank = i + 1;
                        }
                        else
                        {
                            //youScore.transform.Find("order").GetComponent<Text>().text = "-";
                            Debug.Log("no Score");
                            mPlayerRank.SetTracer(Result["missionList"][i][1], 0, i + 1);
                        }
                    }
                }
                for (int i = 0; i < Result["coinList"].Count; i++)
                {
                    if (i < 10)
                    {
                        mBestTracerRankPanel[i].SetRankMessage(i + 1, mRankResult["coinList"][i]);
                        //Instantiate(ObjectPool.Instance.LoadObject("WealthTracerRankPanel"), mWealthTracerRankContent.transform);
                    }
                    if (Result["coinList"][i][0].AsInt == GameManage.Instance.ThePlayerData.PlayerID)
                    {
                        if (Result["coinList"][i][3].AsInt > 0)
                        {
                            mPlayerRank.SetTracer(Result["coinList"][i][1], Result["coinList"][i][3].AsInt, i + 1);
                            GameManage.Instance.ThePlayerData.WealthTracerMoney = Result["coinList"][i][3].AsInt;
                            GameManage.Instance.ThePlayerData.WealthTracerRank = i + 1;
                        }
                        else
                        {
                            //youScore.transform.Find("order").GetComponent<Text>().text = "-";
                            Debug.Log("no Score");
                            mPlayerRank.SetTracer(Result["coinList"][i][1], 0, RankCount);
                        }
                    }
                }

                //if (Result["coinList"].Count > 12)
                //{
                //    mBestTracerRankContent.sizeDelta = new Vector2(0, (Result["coinList"].Count - 12) * (BestTracerLayoutGroup.cellSize.y + BestTracerLayoutGroup.spacing.y));
                //}
                //else
                //{
                mBestTracerRankContent.sizeDelta = new Vector2(0, 0);
                //}

                //if (Result["missionList"].Count > 12)
                //{
                //    mWealthTracerRankContent.sizeDelta = new Vector2(0, (Result["missionList"].Count - 12) * (WealthTracerLayoutGroup.cellSize.y + WealthTracerLayoutGroup.spacing.y));
                //}
                //else
                //{
                mWealthTracerRankContent.sizeDelta = new Vector2(0, 0);
                //}

                SetActiveUI("Ranking", false);
                MRankingComplete = true;
            }
        }
    }

    public JSONNode GetRankList(LoadRank.RankMode mode)
    {
        if (mRankResult != null)
        {
            switch (mode)
            {
                case LoadRank.RankMode.WealthTracer:
                    mRankCount += 1;
                    return mRankResult["coinList"][mRankCount - 1];
                case LoadRank.RankMode.BestTracer:
                    mBestTracerRankCount += 1;
                    return mRankResult["missionList"][mBestTracerRankCount - 1];
                default:
                    Debug.Log("錯誤enum");
                    return null;
            }
        }
        else
        {
            Debug.LogError("mRankResult == null");
            return null;
        }
    }
    bool SetBestTracer = false;
    bool SetWealthTracer = false;

    void TurnBestTracer()
    {
        mBestTracerView.gameObject.SetActive(true);
        mWealthTracerView.gameObject.SetActive(false);
        mBestTracerButtonOn.SetActive(true);
        mBestTracerButtonOff.SetActive(false);
        mWealthTracerButtonOn.SetActive(false);
        mWealthTracerButtonOff.SetActive(true);
        if (SetBestTracer == false)
        {
            mPlayerRank.SetTracer(GameManage.Instance.ThePlayerData.Name, GameManage.Instance.ThePlayerData.BestTracerMoney, GameManage.Instance.ThePlayerData.BestTracerRank);
            BestTracerLayoutGroup.cellSize = new Vector2(mPlayerRank.GetWidth().rect.width, BestTracerLayoutGroup.cellSize.y);
            SetBestTracer = true;
            SetWealthTracer = false;
        }
        mBestTracerRankContent.anchoredPosition = new Vector2(0, 0);
    }

    void TurnWealthTracer()
    {
        mBestTracerView.gameObject.SetActive(false);
        mWealthTracerView.gameObject.SetActive(true);
        mBestTracerButtonOn.SetActive(false);
        mBestTracerButtonOff.SetActive(true);
        mWealthTracerButtonOn.SetActive(true);
        mWealthTracerButtonOff.SetActive(false);
        if (SetWealthTracer == false)
        {
            mPlayerRank.SetTracer(GameManage.Instance.ThePlayerData.Name, GameManage.Instance.ThePlayerData.WealthTracerMoney, GameManage.Instance.ThePlayerData.WealthTracerRank);
            WealthTracerLayoutGroup.cellSize = new Vector2(mPlayerRank.GetWidth().rect.width, WealthTracerLayoutGroup.cellSize.y);
            SetWealthTracer = true;
            SetBestTracer = false;
        }
        mWealthTracerRankContent.anchoredPosition = new Vector2(0, 0);
    }

    void TurnCollection()
    {
        Debug.Log("CollectionAnchor");
        mCollectionContent.anchoredPosition = new Vector2(0, 0);
    }

    public void TurnMissionList()
    {
        mMissionView.SetActive(true);
        mMissionRecordView.SetActive(false);
        mMissionButtonOn.SetActive(true);
        mMissionButtonOff.SetActive(false);
        mMissionRecordButtonOn.SetActive(false);
        mMissionRecordButtonOff.SetActive(true);
        MissionLayoutGroup.cellSize = new Vector2(mPlayerRank.GetWidth().rect.width, MissionLayoutGroup.cellSize.y);
        mMissionContent.anchoredPosition = new Vector2(0, 0);
    }

    public void TurnRecordList()
    {
        mMissionView.SetActive(false);
        mMissionRecordView.SetActive(true);
        mMissionButtonOn.SetActive(false);
        mMissionButtonOff.SetActive(true);
        mMissionRecordButtonOn.SetActive(true);
        mMissionRecordButtonOff.SetActive(false);
        mMissionRecordContent.anchoredPosition = new Vector2(0, 0);
    }

    //float timer = 0;
    IEnumerator CheckLoading()
    {
        int counter = 0;
        while ((MRankingComplete && CollectionComplete) == false)
        {
            if (counter % 10 == 0)
            {
                Debug.Log("讀取未完成 mRankingComplete : " + MRankingComplete + "CollectionComplete : " + CollectionComplete);
            }
            counter += 1;
            yield return new WaitForSeconds(0.1f);
        }
        if ((MRankingComplete && CollectionComplete))
        {
            GameManage.Instance.LoadComplete();
        }
    }

    private void OnEnable()
    {
        ChangeMode(IPlayerMode.Mission);
        CheckPlayerCoins();
    }
}

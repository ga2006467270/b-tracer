﻿using UnityEngine;

public interface IResourseLoadType
{
    void LoadResourses();
}

public class AddResources
{
    /// <summary>
    /// 讀取資料
    /// </summary>
    /// <param name="obejctName">物件名稱.</param>
    public static void LoadObjectResources(string obejctName)
    {
        var loadobject = Resources.Load<GameObject>(obejctName);
        if (loadobject == null)
        {
            return;
        }
        ObjectPool.Instance.AddObject(loadobject, obejctName);
    }
}

public class ResourceLoadBTrace : IResourseLoadType
{
    void IResourseLoadType.LoadResourses()
    {
        AddResources.LoadObjectResources("LoginCanvas");
        AddResources.LoadObjectResources("GameCanvas");
        AddResources.LoadObjectResources("MissionManager");
        AddResources.LoadObjectResources("LoadingCanvas");
        AddResources.LoadObjectResources("LoadingBar");
    }
}

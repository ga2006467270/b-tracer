﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionRecordButtonData : MonoBehaviour
{
    int mScore;

    [SerializeField] Text mMissionName = null;
    [SerializeField] Text mSpentTime = null;
    //[SerializeField] Sprite mStarImage = null;

    public void SetMission(string missionName, int spendTime)
    {
        string[] TheSpendTime = CountTime(spendTime);
        mSpentTime.text = TheSpendTime[0] + ":" + TheSpendTime[1];
        mMissionName.text = missionName;
    }

    //public void SetMission(string missionName, string cards, string cardNumbers, int status, int spendTime, string reward, int targetTime)
    //{
    //    string[] TheSpendTime = CountTime(spendTime);
    //    mSpentTime.text = TheSpendTime[0] + ":" + TheSpendTime[1];
    //    mMissionName.text = missionName;
    //}

    string[] CountTime(int spendTime)
    {
        string mintime = "";
        string sectime = "";
        if ((spendTime % 3600) / 60 < 10)
        {
            mintime += "0";
        }
        if (spendTime % 60 < 10)
        {
            sectime += "0";
        }
        if (spendTime > 6000)
        {
            mintime = "99";
            sectime = "59";
        }
        else
        {
            mintime += ((spendTime % 3600) / 60).ToString();
            sectime += (spendTime % 60).ToString();
        }
        string[] time = { mintime, sectime };
        return time;
    }
}

﻿using UnityEngine;
using System.Collections;


public class DetectLeaks : MonoBehaviour
{
    private static DetectLeaks instance;

    private Object[] originalObjs;
    private bool isFirstSet;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
        isFirstSet = false;
    }

    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    void OnGUI()
    {
        if (GUILayout.Button("Unload Unused Assets"))
        {
            Resources.UnloadUnusedAssets();
        }

        if (GUILayout.Button("Mono Garbage Collect"))
        {
            System.GC.Collect();
        }

        if (GUILayout.Button("List Loaded Textures"))
        {
            ListLoadedTextures();
        }

        if (GUILayout.Button("List Loaded Sounds"))
        {
            ListLoadedAudio();
        }

        if (GUILayout.Button("List Loaded GameObjects"))
        {
            ListLoadedGameObjects();
        }
        if (GUILayout.Button("List Diff Loaded GameObjects"))
        {
            ListDiffLoadedGameObjects();
        }
    }


    private void ListLoadedTextures()
    {
        Object[] textures = Resources.FindObjectsOfTypeAll(typeof(Texture));
        Debug.Log("textures has " + textures.Length);
        string list = string.Empty;

        for (int i = 0; i < textures.Length; i++)
        {
            if (textures[i].name == string.Empty)
            {
                continue;
            }

            list += (i.ToString() + ". " + textures[i].name + "\n");

            if (i == 500)
            {
                Debug.Log(list);
                list = string.Empty;
            }

        }

        Debug.Log(list);
    }

    private void ListLoadedAudio()
    {
        Object[] sounds = Resources.FindObjectsOfTypeAll(typeof(AudioClip));

        string list = string.Empty;

        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == string.Empty)
            {
                continue;
            }
            list += (i.ToString() + ". " + sounds[i].name + "\n");
        }

        Debug.Log(list);
    }

    private void ListLoadedGameObjects()
    {
        Object[] gos = FindObjectsOfType(typeof(GameObject));//Resources.FindObjectsOfTypeAll(typeof(GameObject));
        Debug.Log("gos has " + gos.Length);
        string list = string.Empty;

        for (int i = 0; i < gos.Length; i++)
        {
            if (gos[i].name == string.Empty)
            {
                continue;
            }
            list += (i.ToString() + ". " + gos[i].name + "\n");
        }

        Debug.Log(list);
    }
    private void ListDiffLoadedGameObjects()
    {
        Object[] gos = Resources.FindObjectsOfTypeAll(typeof(GameObject));//Resources.FindObjectsOfTypeAll(typeof(GameObject));
        Debug.Log("gos has " + gos.Length);
        if (!isFirstSet)
        {
            originalObjs = FindObjectsOfType(typeof(GameObject));
            isFirstSet = true;
            Debug.Log("orginal objs set!");
        }
        else
        {
            string list = string.Empty;
            int count = 0;
            for (int i = 0; i < gos.Length; i++)
            {
                bool isFind = true;
                for (int j = 0; j < originalObjs.Length; j++)
                {
                    if (gos[i].Equals(originalObjs[j]))
                    {
                        isFind = false;
                    }
                }
                if (isFind)
                {
                    list += (i.ToString() + ". " + gos[i].name + "\n");
                    count++;
                }
                /*
                      if(gos[i].name == string.Empty)
                      {
                        continue;
                      }
                      list += (i.ToString() + ". " + gos[i].name + "\n");
                      */
            }
            Debug.Log(count.ToString() + " difference Found!");
            Debug.Log(list);
        }
    }
}

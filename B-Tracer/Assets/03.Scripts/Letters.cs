﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 控制Letters
public class Letters
{
    Dictionary<string, int> mTextDic = new Dictionary<string, int>();
    Dictionary<string, LetterController> LetterControllerDic = new Dictionary<string, LetterController>();
    [SerializeField]List<LetterController> LetterArray = new List<LetterController>();
    //取得指定Letter
    public LetterController GetLetter(string text)
    {
        if (LetterControllerDic.ContainsKey(text))
        {
            return LetterControllerDic[text];
        }
        else
        {
            Debug.Log("No This text in the Dic");
            return null;
        }
    }

    public int GetLetterAmount(string text)
    {
        if (mTextDic.ContainsKey(text))
        {
            return mTextDic[text];
        }
        else
        {
            Debug.Log("No This text in the Dic");
            return 0;
        }
    }

    public void SetLetterAmount(string text, int amount)
    {
        if (mTextDic.ContainsKey(text))
        {
            mTextDic[text] = amount;
        }
        else
        {
            Debug.Log("No This text in the Dic");
        }
    }

    // 取得所有Letter
    public List<LetterController> GetLetters()
    {
        return LetterArray;
    }

    // 清空所有Letter
    public void ClearLetter()
    {
        LetterArray.Clear();
        mTextDic.Clear();
        LetterControllerDic.Clear();
    }

    // 匯入Letter
    public void AddLetter(string text, LetterController letterScript, int amount)
    {
        mTextDic.Add(text, amount);
        LetterControllerDic.Add(text, letterScript);
        LetterArray.Add(letterScript);
    }
}

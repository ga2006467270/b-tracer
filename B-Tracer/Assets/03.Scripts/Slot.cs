﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slot : MonoBehaviour, IDropHandler
{
    Image mImgColor;
    public GameObject item
    {
        get
        {
            if (transform.childCount > 0)
            {
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }

    [SerializeField] Text mNumberText = null;

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log(eventData.pointerDrag.name + " Drop to : " + gameObject.name);
        if (gameObject.GetComponentInChildren<FillInWord>())
        {
            Debug.Log("Drop Failed : " + item);
            return;
        }
        if (item)
        {
            FillInWord.ItemBeginDrag.transform.SetParent(transform);
            FillInWord.ItemBeginDrag.transform.position = transform.position;
        }
    }

    public void SetNumberText(int number)
    {
        mNumberText.text = "" + number + ".";
    }
}

﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadRank : MonoBehaviour
{
    public enum RankMode
    {
        WealthTracer,
        BestTracer
    }
    [SerializeField] Text mNameText = null;
    [SerializeField] Text mMoneyText = null;
    [SerializeField] Text mRankText = null;
    [SerializeField] Image mRankImage = null;
    [SerializeField] Image mOtherRankImage = null;
    [SerializeField] Sprite[] mRankLevelImg = null;
    [SerializeField] RectTransform mMyTransform = null;
    int mRank;
    // 有ID值 但目前無作用
    //[SerializeField] Text mIdText;
    //[SerializeField] bool mLoadOnAwake = true;
    [SerializeField] RankMode mRankMode;
    private void Awake()
    {
        //if (mLoadOnAwake)
        //{
        //    switch (mRankMode)
        //    {
        //        case RankMode.WealthTracer:
        //            mNode = PlayerMode.Instance.GetRankList(RankMode.WealthTracer);
        //            SetRankMessage(PlayerMode.Instance.RankCount);
        //            break;
        //        case RankMode.BestTracer:
        //            mNode = PlayerMode.Instance.GetRankList(RankMode.BestTracer);
        //            SetRankMessage(PlayerMode.Instance.BestTracerRankCount);
        //            break;
        //        default:
        //            Debug.Log("錯誤");
        //            break;
        //    }
        //}
    }

    public void SetRankMessage(int rank,JSONNode node)
    {
        mRankText.text = rank.ToString();
        mNameText.text = node[1];
        mMoneyText.text = node[3];
        CheckRankImg(rank);
    }

    public void SetTracer(string name, int money, int rank)
    {
        mNameText.text = name;
        mMoneyText.text = money.ToString();
        mRankText.text = rank.ToString();
        CheckRankImg(rank);
    }

    public RectTransform GetWidth()
    {
        return mMyTransform;
    }

    void CheckRankImg(int rank)
    {
        switch (rank)
        {
            case 1:
                mRankImage.sprite = mRankLevelImg[0];
                break;
            case 2:
                mRankImage.sprite = mRankLevelImg[1];
                break;
            case 3:
                mRankImage.sprite = mRankLevelImg[2];
                break;
            default:
                mRankImage.gameObject.SetActive(false);
                mOtherRankImage.gameObject.SetActive(true);
                mOtherRankImage.sprite = mRankLevelImg[3];
                break;
        }
    }
}

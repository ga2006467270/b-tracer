﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionUI : MonoBehaviour
{
    public static MissionUI Instance;
    [SerializeField] GameObject mHomePanel = null;
    [SerializeField] GameObject mUnCompletePanel = null;
    [SerializeField] Example example = null;
    [Space]

    [SerializeField] Sprite[] InstructionImage = null;
    [SerializeField] Image currentInstuctImg = null;
    [SerializeField] GameObject MainCam;

    public void HomeUI()
    {
        Debug.Log("HomeUI");
        mHomePanel.SetActive(!mHomePanel.activeSelf);
    }

    // 儲存Mission訊息
    public void LogoutMission(bool save)
    {
        if (save)
        {
            Debug.Log("SaveMissionMessage");
            MissionManager.Instance.SaveMission();
            mHomePanel.SetActive(false);
        }
        else
        {
            MissionManager.Instance.CloseCompletePanel();
        }
        //MissionManager.Instance.clearMissionData();
        GameManage.Instance.UseLoadingBar();
        TimeController.Instance.StartCount = false;
        StartCoroutine(LoginUI.Instance.Login(false));
        PlayerMode.Instance.OpenGameCanvas();
    }

    // 關閉HomePanel
    public void LogoutCanel()
    {
        mHomePanel.SetActive(false);
    }

    public void SearchUI()
    {
        Debug.Log("SearchUI");
        MainCam.SetActive(false);
        MissionManager.Instance.BeaconPanel.SetActive(true);
        MissionManager.Instance.mBeaconCamera.SetActive(true);
        MissionManager.Instance.SetMissionPanel(false);
    }

    public void CardsUI()
    {
        MissionManager.Instance.ChangeMode(MissionManager.IMissionMode.Cards);
    }

    public void PassageUI()
    {
        MissionManager.Instance.ChangeMode(MissionManager.IMissionMode.Passage);
        Debug.Log("PassageUI");
    }

    public void GuidesUI()
    {
        MissionManager.Instance.ChangeMode(MissionManager.IMissionMode.Guides);
        currentInstuctImg.sprite = InstructionImage[0];
        Debug.Log("GuidesUI");
    }

    public void BeaconBackUI()
    {
        Debug.Log("BeaconBack");
        example.mybeacons.Clear();
        MissionManager.Instance.BeaconPanel.SetActive(false);
        MissionManager.Instance.SetMissionPanel(true);
        MissionManager.Instance.BeaconCollectedUIPanel.SetActive(false);
        MissionManager.Instance.mBeaconCamera.SetActive(false);
        MainCam.SetActive(true);
    }

    public void BeaconCollectedUI()
    {
        Debug.Log("Open Beacon Collectected UI");
        MissionManager.Instance.OpenBeaconCollected();
    }

    public void CloseUnCompleteUI()
    {
        mUnCompletePanel.SetActive(false);
    }

    public void GuidesRight()
    {
        for (int i = 0; i < InstructionImage.Length; i++)
        {
            if (InstructionImage[i] == currentInstuctImg.sprite)
            {
                if (i == InstructionImage.Length - 1)
                {
                    currentInstuctImg.sprite = InstructionImage[0];
                    return;
                }
                else
                {
                    currentInstuctImg.sprite = InstructionImage[i + 1];
                    return;
                }
            }
        }
    }

    public void GuidesLeft()
    {
        for (int i = 0; i < InstructionImage.Length; i++)
        {
            if (InstructionImage[i] == currentInstuctImg.sprite)
            {
                if (i == 0)
                {
                    currentInstuctImg.sprite = InstructionImage[InstructionImage.Length - 1];
                    return;
                }
                else
                {
                    currentInstuctImg.sprite = InstructionImage[i - 1];
                    return;
                }
            }
        }
    }

    public void SubmitUI()
    {
        bool complete = MissionManager.Instance.CheckMission();
        if (complete)
        {
            Debug.Log("關閉下方botton");
            Debug.Log("給予時間 金錢 collected 任務名");
        }
    }

    public void ResetWordUI()
    {
        Debug.Log("ResetWording");
        CardManager.Instance.ResetCard();
    }

    public void SoundUI()
    {
        Debug.Log("Play Sound");
        CardManager.Instance.PlaySound();
    }

    private void Awake()
    {
        Instance = this;
        MainCam = Camera.main.gameObject;
    }
}

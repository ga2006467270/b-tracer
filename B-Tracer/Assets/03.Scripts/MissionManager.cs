﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MissionManager : MonoBehaviour
{
    public enum IMissionMode
    {
        Passage,
        Cards,
        Guides,
    }

    public static MissionManager Instance
    {
        get
        {
            if (mInstance == null)
            {
                Debug.LogError("無此Instance");
            }
            return mInstance;
        }
    }

    public string[] AnsOrder
    {
        get
        {
            return mAnsOrder;
        }

        set
        {
            mAnsOrder = value;
        }
    }

    public string[] CurrentWording
    {
        get
        {
            return mCurrentWording;
        }

        set
        {
            mCurrentWording = value;
        }
    }

    public GameObject MissionCanvasPanel
    {
        get
        {
            return mMissionCanvasPanel;
        }
    }

    public GameObject BeaconPanel
    {
        get
        {
            return mBeaconPanel;
        }

        set
        {
            mBeaconPanel = value;
        }
    }

    [SerializeField] IMissionMode mCurrentMissionMode;
    [SerializeField] GameObject mMissionCanvasPanel = null;
    [Space]
    //[SerializeField] Text mPassageAnswerText = null;
    [SerializeField] GameObject mHintCardFieldPanel = null;

    [SerializeField] GameObject mHintCardPanel = null;
    [Space]
    [SerializeField] GameObject mAnsCardObj = null;
    [SerializeField] GameObject mAnsFieldObject = null;

    [Space]
    //Image mCurrentInstuctImg;
    [SerializeField] GameObject[] mMissionPanel = null;
    [SerializeField] GameObject[] mMissionButtonPanel = null;
    [SerializeField] GameObject mSelectPanel = null;
    [Space]
    [SerializeField] GameObject mBeaconPanel = null;
    public GameObject BeaconCollectedUIPanel = null;
    public GameObject mBeaconCamera;
    [Space]
    [SerializeField] CompleteImageSetter mCompletePanel = null;
    [Space]
    [SerializeField] Sprite[] memoPic = null;
    [SerializeField] GameObject mUnCompletePanel = null;
    [Space]
    [SerializeField] Text mMissionNameText = null;

    Dictionary<string, GameObject> mMissionPanelDic = new Dictionary<string, GameObject>();
    Dictionary<string, GameObject> mPanelButtonDic = new Dictionary<string, GameObject>();
    public BTracerBeacon beaconDic = new BTracerBeacon();
    public List<string> beaconUUIDList = new List<string>();
    public List<string> beaconContents = new List<string>();
    static MissionManager mInstance;

    // 暫存字母
    [SerializeField] string[] TemporerLetter = null;
    public MissionData CurrentMissionData;
    public string[] Wordings;
    public Text mPassageCardAnswerText;
    public Text mHintCardAnswerText;
    [SerializeField] string[] mCurrentWording;
    //string[] mQuestion;
    [SerializeField] string[] mAnsOrder;
    [SerializeField] string[] mAnsField; // 放置答案的array
    [SerializeField] int mSpentTime;

    DateTime mStartTime;
    int mChecker;
    [SerializeField] int mMostStarBefore;
    [SerializeField] bool IsUploading = false;

    [SerializeField] int[] mCardFileID;
    //string[] cardFileName;
    [SerializeField] int[] time = new int[3];
    [SerializeField] int[] timeReward = new int[3];
    [SerializeField] int mTime3RewardOver;
    [SerializeField] List<Vector2> gestureDetector = new List<Vector2>();
    int gestureCount = 0;
    float gestureLength;
    [SerializeField] Vector2 gestureSum = Vector2.zero;
    [SerializeField] int numOfCircleToShow = 3;
    [SerializeField] bool saveletterDone = false;
    [SerializeField] bool SaveAnswerDone = false;


    public int GestureCount
    {
        get
        {
            return gestureCount;
        }

        set
        {
            gestureCount = value;
        }
    }

    public int StartTime
    {
        get
        {
            return mStartTime.Second;
        }
    }

    [SerializeField] AudioClip CompleteSound = null;
    [SerializeField] AudioSource CompleteMissionSource = null;

    // 意義不明
    string mAnsOrderFilled;

    private void Awake()
    {
        if (mInstance != null)
        {
            Debug.LogError("兩個以上的Instance");
            Destroy(gameObject);
        }
        mInstance = this;
        foreach (GameObject obj in mMissionPanel)
        {
            mMissionPanelDic.Add(obj.name, obj);
        }
        foreach (GameObject obj in mMissionButtonPanel)
        {
            mPanelButtonDic.Add(obj.name, obj);
        }
        CurrentMissionData = new MissionData();
        MissionCanvasPanel.SetActive(false);
    }

    private void Update()
    {
        if (!(Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer))
        {
            if (isGestureDone())
            {
                AddAllLetter();
            }
        }
    }
    // Debug用 轉3圈獲得Letter
    public bool isGestureDone()
    {
        if (Input.GetMouseButtonUp(0))
        {
            gestureDetector.Clear();
            GestureCount = 0;
        }
        else
        {
            if (Input.GetMouseButton(0))
            {
                Vector2 p = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                if (gestureDetector.Count == 0 || (p - gestureDetector[gestureDetector.Count - 1]).magnitude > 10)
                    gestureDetector.Add(p);
            }
        }

        if (gestureDetector.Count < 10)
            return false;

        gestureSum = Vector2.zero;
        gestureLength = 0;
        Vector2 prevDelta = Vector2.zero;
        for (int i = 0; i < gestureDetector.Count - 2; i++)
        {

            Vector2 delta = gestureDetector[i + 1] - gestureDetector[i];
            float deltaLength = delta.magnitude;
            gestureSum += delta;
            gestureLength += deltaLength;

            float dot = Vector2.Dot(delta, prevDelta);
            if (dot < 0f)
            {
                gestureDetector.Clear();
                GestureCount = 0;
                return false;
            }

            prevDelta = delta;
        }

        int gestureBase = (Screen.width + Screen.height) / 4;

        if (gestureLength > gestureBase && gestureSum.magnitude < gestureBase / 2)
        {
            gestureDetector.Clear();
            GestureCount++;
            if (GestureCount >= numOfCircleToShow)
                return true;
        }

        return false;
    }

    public void LoadMission(int missionNumber, string missionName, bool resetStartTime, bool isDo)
    {
        if (missionNumber > 50)
        {
            Debug.LogWarning("English Ver");
#if !UNITY_EDITOR
            BDSLiteSpeech.ChangeLanguageEng();
#endif
        }
        else
        {
            Debug.LogWarning("Chinese Ver");
#if !UNITY_EDITOR
            BDSLiteSpeech.ChangeLanguageHK();
#endif
        }
        mMissionCanvasPanel.SetActive(true);
        mSelectPanel.SetActive(true);
        StartCoroutine(loadRecords(missionNumber));
        StartCoroutine(loadLetter(missionNumber));
        CurrentMissionData.MissionNumber = missionNumber;
        CurrentMissionData.MissionName = missionName;
        StartCoroutine(readChiBeaconData());
        CurrentMissionData.IsDo = isDo;
        TimeController.Instance.StartCount = true;
        mMissionNameText.text = missionName;
        //if (resetStartTime)
        //{
        //    Debug.LogWarning("上傳startTime");
        //    //StartCoroutine(upLoadStartTime());
        //}
    }

    IEnumerator upLoadStartTime()
    {
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();
        JSon.Add("toDo", "loadRecords");
        JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        JSon.Add("questionID", CurrentMissionData.MissionNumber.ToString());
        DateTime myDate = DateTime.Now;
        string myDateString = myDate.ToString("yyyy-MM-dd_HH_mm_ss");
        JSon.Add("StartTime", myDateString);
        form.AddField("response", JSon.ToString());
        using (UnityWebRequest LoginWWW = UnityWebRequest.Post(MyVar.PHPURL, form))
        {
            yield return LoginWWW.SendWebRequest();
            if (LoginWWW.error != null)
            {
                StartCoroutine(upLoadStartTime());
                Debug.LogError("error: " + LoginWWW.error);
            }
        }
    }

    void AddAllLetter()
    {
        foreach (LetterController letters in CurrentMissionData.mLetters.GetLetters())
        {
            letters.AddLetterUI();
        }
    }

    IEnumerator loadRecords(int missionNumber)
    {
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();
        JSon.Add("toDo", "loadRecords");
        JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        JSon.Add("questionID", missionNumber.ToString());
        //JSon.Add("questionID","1");
        Debug.LogError("loadRecords : " + JSon.ToString());
        form.AddField("response", JSon.ToString());
        using (UnityWebRequest LoginWWW = UnityWebRequest.Post(MyVar.PHPURL, form))
        {
            yield return LoginWWW.SendWebRequest();
            if (LoginWWW.isNetworkError || LoginWWW.isHttpError)
            {
                Debug.Log("error: " + LoginWWW.error);
                yield return new WaitForSeconds(0.5f);
                StartCoroutine(loadRecords(missionNumber));
            }
            else
            {
                var Result = JSONNode.Parse(LoginWWW.downloadHandler.text);
                Debug.Log(Result);
                SetAnswerText(Result["Question"]);
                Debug.Log(Result["AnsOrder"]);
                mAnsOrder = CheckAmount(Result["AnsOrder"]);
                SetMissionImage(Result["cardFileName"]);
                SaveCardID(Result);
                SetWording(Result);
                SetAnsOrder(Result);
                SaveReward(Result);
                SaveStartTime(Result);
                SaveChecker(Result);
                Debug.Log("讀取成功");
            }
            yield return new WaitForFixedUpdate(); // 等待下一偵才關閉layout
        }
    }

    private void SaveStartTime(JSONNode result)
    {
        mStartTime = Convert.ToDateTime(result["StartTime"]);
        Debug.Log("StartTime : " + mStartTime);
        TimeSpan timeSpent = DateTime.Now - mStartTime;
        if (mStartTime.Month == 9 && mStartTime.Day == 1 && mStartTime.Year == 2019)
        {
            mStartTime = DateTime.Now;
            TimeController.Instance.SetTime(0);
        }
        else
        {
            TimeController.Instance.SetTime(Convert.ToInt32(timeSpent.TotalSeconds));
        }
        Debug.Log(DateTime.Now);
        Debug.Log(timeSpent);
    }

    private void SaveChecker(JSONNode result)
    {
        mChecker = result["checker"].AsInt;
        Debug.Log("Checker : " + mChecker);
    }


    private void SaveReward(JSONNode result)
    {
        for (int i = 0; i < 3; i++)
        {
            timeReward[i] = result["time" + (i + 1) + "Reward"].AsInt;
            time[i] = result["time" + (i + 1)].AsInt;
        }
        mTime3RewardOver = result["time3RewardOver"].AsInt;
        mMostStarBefore = result["mostStar"].AsInt;
        Debug.LogError("mostStar : " + result["mostStar"].AsInt);
        Debug.LogError(result);
    }

    private void SaveCardID(JSONNode result)
    {
        mCardFileID = new int[result["cardFileID"].Count - 1];
        for (int i = 0; i < result["cardFileID"].Count - 1; i++)
        {
            mCardFileID[i] += result["cardFileID"][i].AsInt;
            //Debug.Log("card id: " + mCardFileID[i] + "   " + result["cardFileID"].Value);
        }
    }

    IEnumerator loadLetter(int missionNumber)
    {
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();
        JSon.Add("toDo", "loadLetters");
        JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        JSon.Add("missionID", missionNumber.ToString());
        form.AddField("response", JSon.ToString());
        using (UnityWebRequest LoginWWW = UnityWebRequest.Post(MyVar.PHPURL, form))
        {
            yield return LoginWWW.SendWebRequest();
            if (LoginWWW.isNetworkError || LoginWWW.isHttpError)
            {
                print("error: " + LoginWWW.error);
                yield return new WaitForSeconds(0.5f);
                StartCoroutine(loadLetter(missionNumber));
            }
            else
            {

                var Result = JSONNode.Parse(LoginWWW.downloadHandler.text);
                Result = JSONNode.Parse(LoginWWW.downloadHandler.text);

                Debug.Log("result of loadLetters: ");
                Debug.Log(Result);
                CardManager.Instance.LetterInitialize(Result);
                BeaconLetterPanelController.Instance.InitializeLetters();
            }
        }
    }


    public void CloseMission()
    {
        ResetMission();
        SetMissionPanel(false);
        beaconDic.RemoveAll();
        beaconUUIDList.Clear();
        beaconContents.Clear();
        Wordings = null;
        mCurrentWording = null;
        mCardFileID = null;
        TemporerLetter = null;
        BeaconLetterPanelController.Instance.ClearLetters();
    }

    public void SaveMission()
    {
        if (IsUploading)
        {
            return;
        }
        else
        {
            IsUploading = true;
        }
        StartCoroutine(saveLetters());
        StartCoroutine(SaveAnswer());
        StartCoroutine(CheckCanCloseMission());
    }

    IEnumerator CheckCanCloseMission()
    {
        while (!saveletterDone || !SaveAnswerDone)
        {
            yield return new WaitForSeconds(0.1f);
            Debug.Log("讀取未完成");
        }
        MissionUI.Instance.PassageUI();
        CloseMission();
        IsUploading = false;
        yield return new WaitForSeconds(0.1f);
    }

    // bool 判定是否開啟
    public void SetMissionPanel(bool mode)
    {
        if (mode == true)
        {
            mMissionCanvasPanel.SetActive(true);
        }
        else
        {
            mMissionCanvasPanel.SetActive(false);
        }
    }

    public bool CheckMission()
    {
        Debug.Log("Check Mission");
        FillInWord[] objstring = mHintCardFieldPanel.GetComponentsInChildren<FillInWord>();
        if (objstring.Length < mAnsOrder.Length)
        {
            Debug.Log("你沒有填完所有空格");
            mUnCompletePanel.SetActive(true);
            return false;
        }
        for (int i = 0; i < objstring.Length; i++)
        {
            if (objstring[i].name == (mAnsOrder[i]))
            {
                Debug.Log("第 " + i + 1 + " 個正確");
                Debug.Log(objstring[i].name);
                Debug.Log(mAnsOrder[i]);
            }
            else
            {
                Debug.Log("第 " + i + 1 + " 個錯誤");
                Debug.Log(objstring[i].name);
                Debug.Log(mAnsOrder[i]);
                mUnCompletePanel.SetActive(true);
                return false;
            }
        }
        Submit();
        return true;
    }

    public void Submit()
    {
        if (IsUploading)
        {
            return;
        }
        else
        {
            IsUploading = true;
            StartCoroutine(submit());
            StartCoroutine(saveLetters());
        }
    }

    IEnumerator submit()
    {
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();
        JSon.Add("toDo", "submit_new");
        JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        int finishtime = (int)TimeController.Instance.SpendTime;
        JSon.Add("FinishTime", finishtime.ToString());
        //int spenttime = CaculateTime();
        JSon.Add("SpentTime", mSpentTime.ToString());
        JSon.Add("StartTime", mStartTime.ToString());
        Debug.Log(mStartTime);
        JSon.Add("questionID", CurrentMissionData.MissionNumber.ToString());
        string saveCardID = null;

        JSon.Add("time1", time[0].ToString());
        JSon.Add("time2", time[1].ToString());
        JSon.Add("time3", time[2].ToString());
        JSon.Add("time1Reward", timeReward[0].ToString());
        JSon.Add("time2Reward", timeReward[1].ToString());
        JSon.Add("time3Reward", timeReward[2].ToString());
        JSon.Add("time3RewardOver", mTime3RewardOver.ToString());
        JSon.Add("finishedMissions", (GameManage.Instance.ThePlayerData.finishedMission + 1).ToString());
        for (int i = 0; i < mCardFileID.Length; i++)
        {
            saveCardID += mCardFileID[i];
            if (i < mCardFileID.Length - 1)
            {
                saveCardID += "^,*";
            }
        }
        Debug.Log(saveCardID);
        JSon.Add("CardIDs", saveCardID);
        JSon.Add("playerOrder", "_^,*_^,*_^,*_");

        //JSon.Add("wordingFilled", "bridge^,*fall_ng^,*d_wn^,*lady");
        JSon.Add("wordingFilled", SaveWording(JSon));
        JSon.Add("checker", mChecker.ToString());
        Debug.LogError(JSon.ToString());

        form.AddField("response", JSon.ToString());
        using (UnityWebRequest LoginWWW = UnityWebRequest.Post(MyVar.PHPURL, form))
        {
            yield return LoginWWW.SendWebRequest();
            if (LoginWWW.isNetworkError || LoginWWW.isHttpError)
            {
                Debug.LogError("error: " + LoginWWW.error);
                yield return StartCoroutine(submit());
            }
            else
            {
                Debug.LogError(LoginWWW.downloadHandler.text);
                var Result = JSONNode.Parse(LoginWWW.downloadHandler.text);
                Debug.LogError(Result);
                CompleteMissionSource.clip = CompleteSound;
                CompleteMissionSource.Play();
                mCompletePanel.gameObject.SetActive(true);
                ChangeMode(IMissionMode.Passage);
                MissionCanvasPanel.SetActive(false);
                mCompletePanel.SetCompleteImage(Result["FinishTime"].AsInt, Result["reward"].Value, Result["stars"].AsInt, CurrentMissionData.MissionName, CardManager.Instance.CardImgs, CurrentWording);
                if (Result["stars"].AsInt > mMostStarBefore)
                {
                    Debug.Log("mMostStarBefore : " + mMostStarBefore);
                    StartCoroutine(upLoadMostStar(Result["stars"].AsInt));
                }
                CurrentMissionData.MissionNumber = 0;
                IsUploading = false;
                mChecker = 0;
            }
        }
    }

    IEnumerator upLoadMostStar(int starAmount)
    {
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();
        JSon.Add("toDo", "submit_new");
        JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        JSon.Add("questionID", CurrentMissionData.MissionNumber.ToString());
        JSon.Add("mostStar", starAmount.ToString());
        form.AddField("response", JSon.ToString());
        Debug.LogError("上傳mostStar : " + starAmount);
        using (UnityWebRequest LoginWWW = UnityWebRequest.Post(MyVar.PHPURL, form))
        {
            yield return LoginWWW.SendWebRequest();
            if (LoginWWW.error != null)
            {
                StartCoroutine(upLoadMostStar(starAmount));
                Debug.LogError("error: " + LoginWWW.error);
            }
            else
            {
                Debug.Log(LoginWWW.downloadHandler.text);
            }
        }
    }

    IEnumerator saveLetters()
    {
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();
        JSon.Add("toDo", "saveChiLetters");
        //save Chinese Letters
        //if (QuestionID > 50)
        string chiLetterString = null;
        string chiLetterNumString = null;
        List<LetterController> letters = CurrentMissionData.mLetters.GetLetters();
        for (int i = 0; i < letters.Count; i++)
        {
            chiLetterString += letters[i].Letter;
            chiLetterNumString += letters[i].mLetterAmount;
            if (i < (letters.Count - 1))
            {
                chiLetterString += "^,*";
                chiLetterNumString += "^,*";
            }
        }
        JSon.Add("chiLetter", chiLetterString);
        JSon.Add("letterNum", chiLetterNumString);
        JSon.Add("missionID", CurrentMissionData.MissionNumber.ToString());
        Debug.Log(CurrentMissionData.MissionNumber);
        Debug.Log(chiLetterString);
        Debug.Log(chiLetterNumString);
        JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        print(JSon);
        form.AddField("response", JSon.ToString());
        using (UnityWebRequest LoginWWW = UnityWebRequest.Post(MyVar.PHPURL, form))
        {
            yield return LoginWWW.SendWebRequest();
            if (LoginWWW.isNetworkError || LoginWWW.isHttpError)
            {
                //GameObject.Find ("Warning").GetComponent<Text>().text="error: "+LoginWWW.error;
                print("error: " + LoginWWW.error);
                StartCoroutine(saveLetters());
            }
            else
            {
                print("Save: " + LoginWWW.downloadHandler.text);
            }
        }
        saveletterDone = true;
    }

    IEnumerator SaveAnswer(/*int saveTimeSpent*/)
    {//temporary save.....................................................
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();
        JSon.Add("toDo", "tempSaveAnswer_new");
        Debug.Log("Uid: " + GameManage.Instance.ThePlayerData.PlayerID.ToString() + " , Qid: " + CurrentMissionData.MissionNumber.ToString());
        JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        JSon.Add("questionID", CurrentMissionData.MissionNumber.ToString());
        //print("thisSpentTime: " + saveTimeSpent.ToString());
        //JSon.Add("thisSpentTime", saveTimeSpent.ToString());
        // Statue暫定1，意義不明
        JSon.Add("status", "1");
        //current 答出的單字
        string savewording = null;
        for (int i = 0; i < Wordings.Length; i++)
        {
            savewording += Wordings[i];
            if (i <= (Wordings.Length - 1))
            {
                savewording += "^,*";
            }
        }
        JSon.Add("wordingfilled", savewording);
        Debug.Log(savewording);

        //playerOrder : 顯示哪個地方上有放字卡
        string playerOrder = "" + 1;
        for (int i = 1; i < Wordings.Length; i++)
        {
            playerOrder += "^,*" + 1;
        }
        JSon.Add("playerOrder", playerOrder);

        //post enable e.g. 1^,*1^,*1^,*1^,*1
        string enableList = "1 ^,*1 ^,*1 ^,*1 ^,*1";
        JSon.Add("enable", enableList);
        JSon.Add("checker", mChecker.ToString());


        print(JSon.ToString());
        form.AddField("response", JSon.ToString());
        using (UnityWebRequest LoginWWW = UnityWebRequest.Post(MyVar.PHPURL, form))
        {
            yield return LoginWWW.SendWebRequest();
            if (LoginWWW.isNetworkError || LoginWWW.isHttpError)
            {
                //GameObject.Find ("Warning").GetComponent<Text>().text="error: "+LoginWWW.error;
                print("error: " + LoginWWW.error);
                StartCoroutine(SaveAnswer());
            }
            else
            {
                Debug.Log(LoginWWW.downloadHandler.text);
                SaveAnswerDone = true;
                mChecker = 0;
            }
        }
    }

    void SetWording(JSONNode node)
    {
        Wordings = node["wordingFilled"].Value.Split(new string[] { "^,*" }, StringSplitOptions.RemoveEmptyEntries);
        CurrentWording = node["wordingFilled"].Value.Split(new string[] { "^,*" }, StringSplitOptions.RemoveEmptyEntries);
        CardManager.Instance.HintedCardPosition = new int[Wordings.Length];
        System.Random rand = new System.Random();
        for (int i = 0; i < Wordings.Length; i++)
        {
            CardManager.Instance.HintedCardPosition[i] = i;
            //Debug.Log(Wordings[i]);
        }
        for (int i = 0; i < CardManager.Instance.HintedCardPosition.Length - 1; i++)
        {
            int j = rand.Next(i, CardManager.Instance.HintedCardPosition.Length);
            int temp = CardManager.Instance.HintedCardPosition[i];
            CardManager.Instance.HintedCardPosition[i] = CardManager.Instance.HintedCardPosition[j];
            CardManager.Instance.HintedCardPosition[j] = temp;
        }
        SetHintCardFieldPanel(mCurrentWording, mAnsOrder);
        TemporerLetter = new string[Wordings.Length];
    }

    void SetAnsOrder(JSONNode node)
    {
        mAnsOrder = node["AnsOrder"].Value.Split(new string[] { "^,*" }, StringSplitOptions.RemoveEmptyEntries);
        for (int i = 0; i < Wordings.Length; i++)
        {
            Debug.Log(mAnsOrder[i]);
        }
    }

    // 暫時儲存字母
    public void TemporaryLetter(string letter, int counter)
    {
        Debug.Log("儲存 : " + letter);
        TemporerLetter[counter] += letter;
        Debug.Log(TemporerLetter[counter]);
    }

    public void ClearTemporaryLetters()
    {
        TemporerLetter = new string[Wordings.Length];
    }

    // 重製暫存字母
    public void ResetTemporaryLetter(int counter)
    {
        if (TemporerLetter[counter] == null)
        {
            return;
        }
        for (int i = 0; i < TemporerLetter[counter].Length; i++)
        {
            string letter = TemporerLetter[counter][i].ToString();
            Debug.Log("返還 : " + letter);
            CardManager.Instance.GiveBackLetter(letter);
        }
        TemporerLetter[counter] = null;
        //重製所有下方letter
    }

    // 重製暫存字母
    public void ClearTemporaryLetter(int counter)
    {
        if (TemporerLetter[counter] == null)
        {
            return;
        }
        for (int i = 0; i < TemporerLetter[counter].Length; i++)
        {
            string letter = TemporerLetter[counter][i].ToString();
            Debug.Log("刪除 : " + letter);
        }
        TemporerLetter[counter] = null;
        //重製所有下方letter
    }

    string[] CheckAmount(string text)
    {
        string[] wordfilled = text.Split(new string[] { "^,*" }, StringSplitOptions.RemoveEmptyEntries);
        Debug.LogError(wordfilled.Length);
        return wordfilled;
    }


    void SetAnswerText(string ansText)
    {
        string[] setnewword = ansText.Split(new string[] { "__" }, StringSplitOptions.None);
        string finalText = null;
        for (int i = 0; i < setnewword.Length; i++)
        {
            if (i != setnewword.Length - 1)
            {
                finalText += setnewword[i];
                finalText += "__" + (i + 1) + "__";
            }
        }
        mHintCardAnswerText.text = finalText;
    }

    void SetHintCardFieldPanel(string[] cardString, string[] ansString)
    {
        for (int i = 0; i < cardString.Length; i++)
        {
            Instantiate(mAnsFieldObject, mHintCardPanel.transform);
            GameObject anscardfieldobj = Instantiate(mAnsFieldObject, mHintCardFieldPanel.transform);
            Slot ansslot = anscardfieldobj.GetComponent<Slot>();
            ansslot.SetNumberText(i + 1);
        }
        for (int i = 0; i < cardString.Length; i++)
        {
            if (cardString[i] == ansString[i])
            {
                AddHintedCard(CardManager.Instance.HintedCardPosition[i], cardString[i]);
            }
        }
    }

    // 最後一個數字是null 調整作用會少算一張圖片
    void SetMissionImage(JSONNode node)
    {
        CardManager.Instance.CardImgs = new Sprite[node.Count - 1];
        Debug.Log(CardManager.Instance.CardImgs.Length);
        for (int i = 0; i < node.Count - 1; i++)
        {
            string path = "https://puzzler.bds.hk/" + "picture/" + node[i];
            StartCoroutine(downloadMissionImg(path, i, (i == node.Count - 2) ? true : false));
            Debug.Log(path);
        }
    }

    IEnumerator downloadMissionImg(string url, int count, bool CardLoadDone)
    {
        using (UnityWebRequest request = UnityWebRequestTexture.GetTexture(url))
        {
            var asyncOp = request.SendWebRequest();
            yield return asyncOp;
            while (!asyncOp.isDone)
            {
                yield return new WaitForSeconds(0.05f);
            }
            if (request.isNetworkError || request.isHttpError)
            {
                StartCoroutine(downloadMissionImg(url, count, CardLoadDone));
                Debug.Log(request.error);
            }
            {
                Texture2D myTexture = ((DownloadHandlerTexture)request.downloadHandler).texture;

                //CardManager.Instance.mCardImgs.Add(image,count)
                CardManager.Instance.CardImgs[count] = Sprite.Create(myTexture, new Rect(0, 0, myTexture.width, myTexture.height), new Vector2(0.5f, 0.5f)); ;

                if (CardLoadDone == true)
                {
                    StartCoroutine(PrepareSerilizeImg());
                }
            }
        }
    }

    IEnumerator PrepareSerilizeImg()
    {
        bool checkcardImg = false;
        while (checkcardImg == false)
        {
            checkcardImg = CheckCard();
            yield return new WaitForSeconds(0.1f);
        }
        if (checkcardImg)
        {
            CardManager.Instance.CardSerilize();
        }
    }

    bool CheckCard()
    {
        for (int i = 0; i < CardManager.Instance.CardImgs.Length; i++)
        {
            if (CardManager.Instance.CardImgs[i] == null)
            {
                return false;
            }
            else if (i == CardManager.Instance.CardImgs.Length - 1)
            {
                return true;
            }
        }
        return false;
    }

    public void ResetMission()
    {
        CardManager.Instance.ClearCard();
        foreach (Slot slot in mHintCardPanel.GetComponentsInChildren<Slot>())
        {
            Destroy(slot.gameObject);
        }

        foreach (Slot slot in mHintCardFieldPanel.GetComponentsInChildren<Slot>())
        {
            Destroy(slot.gameObject);
        }
        mHintCardPanel.GetComponent<GridLayoutGroup>().enabled = true;
    }

    public void ChangeMode(IMissionMode targetMode)
    {
        if (mCurrentMissionMode == targetMode)
        {
            return;
        }
        string closeUIname = SearchModePanel(mCurrentMissionMode);
        string activeUIname = SearchModePanel(targetMode);
        Debug.Log(closeUIname);
        Debug.Log(activeUIname);
        SetActiveUI(closeUIname);
        SetActiveUI(activeUIname);
        mCurrentMissionMode = targetMode;
    }

    // 轉換自身的Active，if true 則變 false，反之亦然
    void SetActiveUI(string UIName)
    {
        Debug.Log(mMissionPanelDic.Count);
        mMissionPanelDic[UIName].SetActive(!mMissionPanelDic[UIName].activeSelf);
        foreach (Image img in mPanelButtonDic[UIName + "Button"].GetComponentsInChildren<Image>())
        {
            if (img.gameObject.name == "EnumImage")
            {
                img.enabled = mMissionPanelDic[UIName].activeSelf;
            }
        }
    }

    string SearchModePanel(IMissionMode mode)
    {
        string UIname = null;
        switch (mode)
        {
            case IMissionMode.Cards:
                UIname = "CardsPanel";
                break;
            case IMissionMode.Guides:
                UIname = "GuidesPanel";
                break;
            case IMissionMode.Passage:
                UIname = "PassagePanel";
                break;
            default:
                Debug.LogError("沒有這個Mode的設定");
                return null;
        }
        return UIname;
    }

    public void AddHintedCard(int cardPos, string cardString)
    {
        Debug.Log("在跟圖片相同的位置增加card");
        Slot[] slots = mHintCardPanel.GetComponentsInChildren<Slot>();
        Debug.Log(slots.Length);
        Debug.Log(cardPos);
        GameObject anscardobj = Instantiate(mAnsCardObj, slots[cardPos].transform);
        FillInWord ansobject = anscardobj.GetComponentInChildren<FillInWord>();
        ansobject.name = cardString;
        Debug.Log(cardPos);
        ansobject.SetImg(memoPic[cardPos]);
    }

    public void OpenBeaconCollected()
    {
        BeaconCollectedUIPanel.SetActive(!BeaconCollectedUIPanel.activeSelf);
        BeaconLetterPanelController.Instance.CheckLetters();
        Debug.Log("讀取Collected");
    }

    public void SetLetterAmount(string letter, int letterAmount)
    {
        CurrentMissionData.mLetters.SetLetterAmount(letter, letterAmount);
    }

    public int CheckLetter(string text)
    {
        return CurrentMissionData.mLetters.GetLetterAmount(text);
    }


    public void CloseCompletePanel()
    {
        mCompletePanel.ClearImgs();
        mCompletePanel.gameObject.SetActive(false);
        CloseMission();
    }

    // 成績 star reward 
    int[] SaveGrade()
    {
        int thisGetReward = 0;
        int thisStar = 0;

        if (Convert.ToInt32(mSpentTime) < time[0])
        {
            thisGetReward = timeReward[0];
            thisStar = 3;
            Debug.Log("save reward1");
        }
        else if (Convert.ToInt32(mSpentTime) < time[1])
        {
            thisGetReward = timeReward[1];
            thisStar = 2;
            Debug.Log("save reward2");
        }
        else if (Convert.ToInt32(mSpentTime) < time[2])
        {
            thisGetReward = timeReward[2];
            thisStar = 1;
            Debug.Log("save reward3");
        }
        else
        {
            thisStar = 0;
            thisGetReward = mTime3RewardOver;
            Debug.Log("save reward4");
        }
        int[] grade = { thisStar, thisGetReward };
        return grade;
    }

    string SaveWording(JSONNode json)
    {
        string wordingfilled = null;
        for (int i = 0; i < Wordings.Length; i++)
        {
            wordingfilled += Wordings[i];
            if (i < (Wordings.Length - 1))
            {
                wordingfilled += "^,*";
            }
        }
        return wordingfilled;
    }

    int CaculateTime()
    {
        Debug.Log(mStartTime);
        DateTime endGameTime = DateTime.Now;
        TimeSpan timeSpent = endGameTime - mStartTime;
        mSpentTime = Convert.ToInt32(timeSpent.TotalSeconds);
        return mSpentTime;
    }

    IEnumerator readChiBeaconData()
    {
        var form = new WWWForm();
        JSONClass JSon = new JSONClass();

        //print (PlayerID.ToString()+" "+nearestID.ToString());
        JSon.Add("toDo", "readChiBeaconData");
        JSon.Add("UserID", GameManage.Instance.ThePlayerData.PlayerID.ToString());
        JSon.Add("missionID", CurrentMissionData.MissionNumber.ToString());
        form.AddField("response", JSon.ToString());

        using (UnityWebRequest LoginWWW = UnityWebRequest.Post(MyVar.PHPURL, form))
        {
            yield return LoginWWW.SendWebRequest();
            if (LoginWWW.isNetworkError || LoginWWW.isHttpError)
            {
                Debug.Log(LoginWWW.error);
                yield return new WaitForSeconds(0.5f);
                StartCoroutine(readChiBeaconData());
            }
            else
            {
                var Result = JSONNode.Parse(LoginWWW.downloadHandler.text);
                print("readChiBeaconData result:");
                print(Result);

                for (int i = 0; i < Result["BeaconData"].Count; i++)
                {
                    beaconUUIDList.Add(Result["BeaconData"][i][0]);
                    beaconContents.Add(Result["BeaconData"][i][1]);
                }
            }
            SetBeacon();
            LoginWWW.downloadHandler.Dispose();
        }
        yield return null;
    }

    void SetBeacon()
    {
        for (int i = 0; i < beaconUUIDList.Count; i++)
        {
            beaconDic.Add(beaconUUIDList[i], beaconContents[i]);
            //if(beaconDic.ContainsKey[])
        }
    }
}

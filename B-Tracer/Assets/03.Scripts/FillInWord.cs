﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FillInWord : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    , IDragHandler, IEndDragHandler, IBeginDragHandler
{
    [SerializeField] Text mWord = null;
    [SerializeField] float mIncreaceSize = 0;
    [SerializeField] Image ImgColor = null;
    Transform mStartParent;
    public static GameObject ItemBeginDrag;
    Vector3 mOriginalPosition;
    Vector3 originalscale;

    void Start()
    {
        mWord.text = gameObject.name;
    }

    public void SetImg(Sprite img)
    {
        ImgColor.sprite = img;
    }

    public void SetWord()
    {
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
    }

    public void OnPointerExit(PointerEventData eventData)
    {
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        gameObject.transform.localScale = new Vector3(transform.localScale.x * mIncreaceSize, transform.localScale.y * mIncreaceSize, transform.localScale.z);
        ItemBeginDrag = gameObject;
        mOriginalPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        mStartParent = transform.parent;
        transform.SetParent(MissionManager.Instance.MissionCanvasPanel.transform);
        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        gameObject.transform.localScale = new Vector3(transform.localScale.x / mIncreaceSize, transform.localScale.y / mIncreaceSize, transform.localScale.z);
        ItemBeginDrag = null;
        if (transform.parent == mStartParent || transform.parent == MissionManager.Instance.MissionCanvasPanel.transform)
        {
            transform.position = mOriginalPosition;
            transform.SetParent(mStartParent);
        }

        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
    }
}

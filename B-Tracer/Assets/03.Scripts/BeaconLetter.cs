﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeaconLetter : MonoBehaviour
{
    [SerializeField] Text mLetterText = null;
    [SerializeField] Text mLetterAmountText = null;


    public void SetLetter(string letter, int mLetterAmount)
    {
        mLetterText.text = letter;
        mLetterAmountText.text = mLetterAmount.ToString();
    }
}

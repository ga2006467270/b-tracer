﻿using System;
[System.Serializable]
public class MissionData
{
    public Letters mLetters = new Letters();
    public int MissionNumber;
    public string MissionName;
    public bool IsDo;
}
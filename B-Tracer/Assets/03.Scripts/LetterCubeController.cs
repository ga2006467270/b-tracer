﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LetterCubeController : MonoBehaviour
{
    string mLetter;
    [SerializeField] TextMesh[] texts = null;
    [SerializeField] Renderer dicemesh = null;

    void OnMouseDown()
    {
        Debug.Log("加入letter");
        CardManager.Instance.AddLetter(mLetter);
        Destroy(this.gameObject);
    }

    public void Update()
    {
        transform.Rotate(Vector3.up, Time.deltaTime * 50, Space.World);
        transform.Rotate(Vector3.right, Time.deltaTime * 50, Space.World);
        // 如果碰到牆壁就自我刪除
        if (CheckCollision()) { Destroy(gameObject); }
    }

    public void SetLetter(string letter)
    {
        mLetter = letter;
        TextMesh[] alltexts = GetComponentsInChildren<TextMesh>();
        foreach (TextMesh text in alltexts)
        {
            text.text = letter;
        }
    }

    public void SetLetter(string[] letters)
    {
        Debug.Log(letters.Length);
        Debug.Log(UnityEngine.Random.Range(0, letters.Length));
        mLetter = letters[UnityEngine.Random.Range(0, letters.Length)];
        foreach (TextMesh tex in texts)
        {
            tex.text = mLetter;
        }
    }

    public bool CheckCollision()
    {
        if (Physics.OverlapBox(transform.position, transform.localScale).Length > 1)
        {
            Debug.Log("碰撞");
            foreach (Collider col in Physics.OverlapBox(transform.position, transform.localScale))
            {
                Debug.Log(col.gameObject.name);
            }
            return true;
        }
        return false;
    }

    public void enablecube()
    {
        dicemesh.enabled = true;
    }
}

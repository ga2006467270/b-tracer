﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TimeController : MonoBehaviour
{
    public static TimeController Instance;
    [SerializeField] Text mTimeText = null;
    public float SpendTime;
    public bool StartCount;
    // Update is called once per frame
    void Awake()
    {
        Instance = this;
    }
    void Update()
    {
        if (StartCount)
        {
            SpendTime += Time.deltaTime;
            int time = Mathf.FloorToInt(SpendTime);

            string min = null;
            string sec = null;
            if ((time / 60) < 10)
            {
                min += "0";
            }
            if (time > 6000)
            {
                min += 99;
                sec += 59;
            }
            else
            {
                min += (time / 60).ToString();
                if (time % 60 < 10)
                {
                    sec += "0";
                }
                sec += (time % 60).ToString();
            }


            mTimeText.text = min + " : " + sec;
        }
    }

    public void SetTime(float time)
    {
        SpendTime = time;
        Debug.Log(SpendTime);
        if (SpendTime < 0)
        {
            SpendTime = 0;
        }
    }
}

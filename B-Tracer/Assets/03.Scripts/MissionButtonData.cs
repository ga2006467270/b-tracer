﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MissionButtonData : MonoBehaviour
{
    [SerializeField] int mMissionNumber;
    [SerializeField] string mMissionName;
    int IsDone;
    bool IsDo;
    [SerializeField] Text mMissionNameText = null;
    [SerializeField] Image[] mStar = new Image[3];
    [SerializeField] Sprite mStarImage = null;
    [SerializeField] Sprite mStarGrayImage = null;
    [SerializeField] Image mIsCompleteImage = null;
    [SerializeField] Sprite[] mCompleteImage = null;

    public void LoadMission()
    {
        Debug.Log("讀取任務 : " + mMissionNumber);
        GameManage.Instance.UseLoadingBar();
        MissionManager.Instance.LoadMission(mMissionNumber, mMissionName, IsDone != 1 ? true : false, IsDo);
        PlayerMode.Instance.ClearGameCanvasData();
        PlayerMode.Instance.CloseGameCanvas();
        PlayerMode.Instance.LeavePlayMode();
    }

    // IsDone : 0 new 1 doing 2 done
    public void SetMission(string missionName, int number, int star, int isDone, bool isDo)
    {
        Debug.Log("missionName : " + star);
        for (int i = 0; i < 3; i++)
        {
            if (i < star)
            {
                //Debug.Log("missionName : " + star);
                mStar[i].sprite = mStarImage;
            }
            else
            {
                mStar[i].sprite = mStarGrayImage;
            }
        }
        mMissionName = missionName;
        mMissionNameText.text = mMissionName;
        mMissionNumber = number;
        mIsCompleteImage.sprite = mCompleteImage[isDone];
        IsDone = isDone;
        IsDo = isDo;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DownloadImage : MonoBehaviour
{
    [SerializeField] Image mCardImage = null;
    [SerializeField] RectTransform mCardTransform = null;
    [SerializeField] Text mCardText = null;
    public bool CardInstantiate = false;
    void Awake()
    {
        GameManage.SetSize(mCardTransform, 2.2f);
        //transform.localScale *= ((float)Screen.height / (float)Screen.width);
    }

    public void SetCard(string text,Sprite sprite)
    {
        mCardText.text = text;
        mCardImage.sprite = sprite;
        CardInstantiate = true;
    }

    public void ShowCard()
    {
        Debug.Log("ShowCard");
        GameManage.Instance.ShowCard(mCardImage.sprite,mCardText.text);
    }

    public string GetCardsText()
    {
        return mCardText.text;
    }

    public Sprite GetCardsSprite()
    {
        return mCardImage.sprite;
    }

}

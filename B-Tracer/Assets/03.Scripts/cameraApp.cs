﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System;

public class cameraApp : MonoBehaviour
{
    public static WebCamTexture mytexture;
    public static cameraApp Instance;
    //[SerializeField] RectTransform mCameraPic;
    const float ORIGIANLE_SIZE = 2.0898148148f;
    const float ORIGIANLECAMERA_SIZE = 0.75f;
    bool cameraon = false;
    bool checkcamera = false;
    private bool camAvailable;
    //WebCamTexture backCam;
    //private Texture defaultBackGround;
    public RawImage backGround;
    //public AspectRatioFitter fit;
    Color32[] data;

    private void Awake()
    {
        Instance = this;
    }
    [SerializeField] GameObject mScaleImage = null;

    void Start()
    {
        StartCoroutine(CheckedCamera());
        StartCoroutine(CameraOn());
        SetCam();
#if UNITY_IPHONE
        gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x, gameObject.transform.localScale.y, gameObject.transform.localScale.z * -1);
#endif
    }

    void SetCam()
    {
        var picturewidth = Screen.width;
        var pictureheight = Screen.height;
        var camerawidth = mytexture.width;
        var cameraheight = mytexture.height;
        //var picturewidth = mytexture.requestedWidth;
        //var pictureheight = mytexture.requestedHeight;
        //Debug.Log(picturewidth);
        //Debug.Log(pictureheight);
        float scalex = mScaleImage.transform.localScale.x;
        float scaley = mScaleImage.transform.localScale.y;
        float scalez = mScaleImage.transform.localScale.z;
        //ORIGIANLE_SIZE == 2.0898148148f
        //1080 *2257=> width:800* height:600
        //1080*800/600 *  2.0898148148 = 3009 
        //3009/2257 = 1.33
        //800 / 600 = 1.33;

        // 1080 * 2257 => 9:16
        // 2257 * 16/9/2.0898148148 = 1920
        // 1080/1920 = 0.5625
        // 9/16 = 0.5625

        if (picturewidth > pictureheight)
        {
            mScaleImage.transform.localScale = new Vector3(scalex * picturewidth / pictureheight * ORIGIANLE_SIZE, scaley, scalez);
        }
        else
        {
            mScaleImage.transform.localScale = new Vector3(scalex, scaley, scalez * pictureheight / picturewidth / ORIGIANLE_SIZE);
        }

        if (camerawidth > cameraheight)
        {
            mScaleImage.transform.localScale = new Vector3(scalex * camerawidth / cameraheight * ORIGIANLECAMERA_SIZE, scaley, scalez);
        }
        else
        {
            mScaleImage.transform.localScale = new Vector3(scalex, scaley, scalez * cameraheight / camerawidth / ORIGIANLECAMERA_SIZE);
        }
        Debug.LogError(" requestedWidth : " + mytexture.requestedWidth);
        Debug.LogError(" requestedHeight : " + mytexture.requestedHeight);
    }

    private void Update()
    {
        if (!camAvailable)
            return;
        //float ratio = (float)mytexture.width / (float)mytexture.height;
        //fit.aspectRatio = ratio;
        float scaleY = mytexture.videoVerticallyMirrored ? -1 : 1f;
        //backGround.rectTransform.localScale = new Vector3(1f, scaleY, 1f);
        //backGround.rectTransform.localScale = new Vector3(backGround.rectTransform.localScale.x, scaleY, backGround.rectTransform.localScale.z);

        int orient = -mytexture.videoRotationAngle;
        //backGround.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
    }


    IEnumerator CameraOn()
    {
        while (!checkcamera)
        {
            yield return new WaitForSeconds(0.2f);
        }
        if (cameraon)
        {
            WebCamDevice[] devices = WebCamTexture.devices;
            mytexture = new WebCamTexture(devices[0].name);

            GetComponent<Renderer>().material.mainTexture = mytexture;
            GetComponent<Renderer>().material.color = Color.white;
            backGround.texture = mytexture;
            mytexture.Play();
            camAvailable = true;
        }
    }

    IEnumerator CheckedCamera()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        if (Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            Debug.Log("webcam found");
            WebCamDevice[] devices = WebCamTexture.devices;
            cameraon = true;
            checkcamera = true;
        }
        else
        {
            Debug.Log("webcam not found");
            cameraon = false;
            checkcamera = true;
        }

        //if (cameraon == false)
        //{
        //    if (mytexture == null)
        //    {
        //}
        //else if (mytexture != null)
        //{
        //    cameraon = true;
        //}
    }


    //void Start()
    //{
    //    defaultBackGround = backGround.texture;
    //    WebCamDevice[] devices = WebCamTexture.devices;

    //    if (devices.Length == 0)
    //    {
    //        Debug.Log("No Camera Detected");
    //        camAvailable = false;
    //        return;
    //    }

    //    for (int i = 0; i < devices.Length; i++)
    //    {
    //        if (!devices[i].isFrontFacing)
    //        {
    //            backCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
    //        }
    //    }

    //    if (backCam == null)
    //    {
    //        Debug.Log("unable to find back camera");
    //        return;
    //    }

    //    backCam.Play();
    //    backGround.texture = backCam;

    //    camAvailable = true;
    //}

    //private void Update()
    //{
    //    if (!camAvailable)
    //        return;
    //    float ratio = (float)backCam.width / (float)backCam.height;
    //    fit.aspectRatio = ratio;
    //    float scaleY = backCam.videoVerticallyMirrored ? -1f : 1f;

    //    backGround.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

    //    int orient = -backCam.videoRotationAngle;
    //    backGround.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
    //}

}

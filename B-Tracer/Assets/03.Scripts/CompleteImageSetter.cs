﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompleteImageSetter : MonoBehaviour
{
    [SerializeField] Text mTime = null;
    [SerializeField] Text mReward = null;
    [SerializeField] Text mMissionName = null;
    [SerializeField] Text mCollectText = null;
    [SerializeField] GridLayoutGroup mCompleteCardLayout = null;
    [SerializeField] GameObject mRewardCardPanel = null;
    [SerializeField] GameObject mCompleteCard = null;
    [SerializeField] RectTransform mCompleteCardRect = null;
    [SerializeField] Image mCompleteStar = null;
    [SerializeField] Sprite[] mCompletePic = null;
    // Use this for initialization

    public void SetCompleteImage(int time, string reward, int starCount, string missionname, Sprite[] cardReward, string[] cardName)
    {
        string mintime = "";
        string sectime = "";
        if (time % 3600 / 60 < 10)
        {
            mintime += "0";
        }
        if (time % 60 < 10)
        {
            sectime += "0";
        }
        if (time > 6000)
        {
            mintime = "99";
            sectime = "59";
        }
        else
        {
            mintime += (time / 60).ToString();
            sectime += (time % 60).ToString();
        }
        mTime.text = mintime + " : " + sectime;
        mReward.text = reward;
        mMissionName.text = missionname;
        switch (starCount)
        {
            case 0:
                mCompleteStar.sprite = mCompletePic[0];
                break;
            case 1:
                mCompleteStar.sprite = mCompletePic[1];
                break;
            case 2:
                mCompleteStar.sprite = mCompletePic[2];
                break;
            case 3:
                mCompleteStar.sprite = mCompletePic[3];
                break;
        }
        if (cardReward.Length == 0)
        {
            mCollectText.text = "";
        }
        else
        {
            Debug.Log("cardReward : " + cardReward.Length);
            mCollectText.text = "You have collected:";
        }
        for (int i = 0; i < cardReward.Length; i++)
        {
            GameObject card = Instantiate(mCompleteCard, mRewardCardPanel.transform);
            card.GetComponent<RewardCard>().SetRewardCard(cardReward[i], cardName[i]);
            Debug.Log(cardReward[i] + ":" + cardName[i]);
        }

        int completeCardHeightCount = (int)(mCompleteCardRect.rect.width / (mCompleteCardLayout.cellSize.x + mCompleteCardLayout.spacing.x));
        float CardSize = (mCompleteCardLayout.cellSize.y + mCompleteCardLayout.spacing.y) * (cardReward.Length / completeCardHeightCount + cardReward.Length % completeCardHeightCount > 0 ? 1 : 0) + mCompleteCardRect.rect.height;
        mCompleteCardRect.sizeDelta = new Vector2(mCompleteCardRect.sizeDelta.x, CardSize);
        Debug.Log(CardSize);
    }

    public void ClearImgs()
    {
        foreach (RewardCard card in mRewardCardPanel.GetComponentsInChildren<RewardCard>())
        {
            Destroy(card.gameObject);
        }
    }
}

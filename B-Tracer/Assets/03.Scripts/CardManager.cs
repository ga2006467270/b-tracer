﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardManager : MonoBehaviour
{
    public static CardManager Instance
    {
        get
        {
            if (mInstance == null)
            {
                Debug.LogError("無此Instance");
            }
            return mInstance;
        }
    }

    static CardManager mInstance;
    // 讀取到卡片資料時 生產相對應的卡片 並給予圖片
    public Sprite[] CardImgs; // 現有照片
    //public List<Sprite> mCardImgs; // 現有照片
    int mCurrentCardCounter = 0; // 目前在哪張照片上

    [SerializeField] Image mEmptyCardImg = null; // 放照片處

    [Space]

    [SerializeField] GameObject mCardLetterPanel = null; // 字母存放處
    [SerializeField] GameObject mLetterPrefab = null; // 字母Prefab
    [SerializeField] GameObject mFinishCardImage = null;
    [SerializeField] GameObject mWrongCardImage = null;
    [SerializeField] GameObject mCardPanel = null;
    BDSLiteSpeech BDSSpeech;
    bool mSpeechInDelay;
    [SerializeField]float mSpeechDelayTime = 0.5f;

    bool mLoadLetterDone;
    bool mLoadCardDone;

    private void Awake()
    {
        mInstance = this;
        StartCoroutine(CheckCardPanelDone());
        BDSLiteSpeech.WakeUp();
    }

    void Start()
    {
        BDSLiteSpeech.ChangeLanguageEng();
    }

    // 將Letter加入到CardText內
    public bool SpellLetter(string text)
    {
        // 如果已經完成 直接return
        if (MissionManager.Instance.Wordings[mCurrentCardCounter] == MissionManager.Instance.AnsOrder[mCurrentCardCounter])
        {
            Debug.Log("這題已完成");
            return false;
        }
        mWrongCardImage.SetActive(false);
        Debug.Log(MissionManager.Instance.CurrentWording[mCurrentCardCounter]);
        string[] setnewword = MissionManager.Instance.CurrentWording[mCurrentCardCounter].Split(new string[] { "_" }, StringSplitOptions.None);
        Debug.Log(setnewword);
        string newword = null;
        for (int i = 0; i < setnewword.Length; i++)
        {
            newword += setnewword[i];
            if (i == 0)
            {
                newword += text;
            }
            else if (i != (setnewword.Length - 1))
            {
                newword += "_";
            }
        }

        MissionManager.Instance.TemporaryLetter(text, mCurrentCardCounter);
        SetCardAnswerText(newword);
        MissionManager.Instance.CurrentWording[mCurrentCardCounter] = newword;
        if (setnewword.Length <= 2)
        {
            if (MissionManager.Instance.CurrentWording[mCurrentCardCounter] != MissionManager.Instance.AnsOrder[mCurrentCardCounter])
            {
                Debug.Log("錯誤" + MissionManager.Instance.CurrentWording[mCurrentCardCounter]);
                MissionManager.Instance.CurrentWording[mCurrentCardCounter] = MissionManager.Instance.Wordings[mCurrentCardCounter];
                SetCardAnswerText(MissionManager.Instance.Wordings[mCurrentCardCounter]);
                Debug.Log(MissionManager.Instance.Wordings[mCurrentCardCounter]);
                MissionManager.Instance.ResetTemporaryLetter(mCurrentCardCounter);
                Debug.Log("錯誤答案");
                mWrongCardImage.SetActive(true);
            }
            else
            {
                Debug.Log("正確答案");
                //將答案儲存 並且不能再這個array內作答
                mFinishCardImage.SetActive(true);
                MissionManager.Instance.ClearTemporaryLetter(mCurrentCardCounter);
                MissionManager.Instance.AddHintedCard(HintedCardPosition[mCurrentCardCounter], MissionManager.Instance.AnsOrder[mCurrentCardCounter]);
                MissionManager.Instance.Wordings[mCurrentCardCounter] = MissionManager.Instance.CurrentWording[mCurrentCardCounter];
            }
            return true;
        }
        Debug.Log(newword);
        return true;
    }
    public int[] HintedCardPosition;

    public void ResetCard()
    {
        MissionManager.Instance.CurrentWording[mCurrentCardCounter] = MissionManager.Instance.Wordings[mCurrentCardCounter];
        SetCardAnswerText(MissionManager.Instance.Wordings[mCurrentCardCounter]);
        MissionManager.Instance.ResetTemporaryLetter(mCurrentCardCounter);
    }

    // 初始化Letter
    public void LetterInitialize(JSONNode node)
    {
        Debug.LogWarning("Add letters");
        for (int i = 0; i < node["letters"].Count; i++)
        {
            AddNewLetter(node["letters"][i][0].ToString()[1].ToString(), node["letters"][i][1].AsInt);
        }
        mLoadLetterDone = true;
    }

    public void AddNewLetter(string letter, int amount)
    {
        GameObject obj = Instantiate(mLetterPrefab, mCardLetterPanel.transform);
        LetterController lettercontrol = obj.GetComponent<LetterController>();
        // 去除頭尾""符號
        lettercontrol.SetLetter(letter, amount);
        MissionManager.Instance.CurrentMissionData.mLetters.AddLetter(letter, lettercontrol, amount);
    }

    public void GiveBackLetter(string letter)
    {
        //MissionManager.Instance.LetterControllerDic[letter].AddLetterUI();
        MissionManager.Instance.CurrentMissionData.mLetters.GetLetter(letter).AddLetterUI();
    }

    void SetCardAnswerText(string word)
    {
        string answord = null;
        for (int i = 0; i < word.Length; i++)
        {
            answord += word[i];
            if (i != (word.Length - 1))
            {
                answord += " ";
            }
        }
        MissionManager.Instance.mPassageCardAnswerText.text = answord;
    }

    // 資料設定完成，開始初始化
    public void CardSerilize()
    {
        Debug.Log(mCurrentCardCounter);
        SetCard();
        Debug.Log("設置 : " + CardImgs.Length);
        mLoadCardDone = true;
        GameManage.Instance.LoadComplete();
    }

    public void RightImg()
    {
        if ((mCurrentCardCounter + 1) == CardImgs.Length)
        {
            mCurrentCardCounter = 0;
        }
        else
        {
            mCurrentCardCounter += 1;
        }
        Debug.Log(mCurrentCardCounter);
        SetCard();
    }

    public void LeftImg()
    {
        if (mCurrentCardCounter <= 0)
        {
            mCurrentCardCounter = CardImgs.Length - 1;
        }
        else
        {
            mCurrentCardCounter -= 1;
        }
        Debug.Log(mCurrentCardCounter);
        SetCard();
    }


    public void PlaySound()
    {
        if (mSpeechInDelay == false)
        {
            Debug.Log("Play : " + MissionManager.Instance.AnsOrder[mCurrentCardCounter]);
            BDSLiteSpeech.Speak(MissionManager.Instance.AnsOrder[mCurrentCardCounter]);
            mSpeechInDelay = true;
            StartCoroutine(SpeechDelay());
            //TTSUnityWin.Instance.TextToSpeech(MissionManager.Instance.AnsOrder[mCurrentCardCounter]);
        }
    }

    IEnumerator SpeechDelay()
    {
        yield return new WaitForSeconds(mSpeechDelayTime);
        mSpeechInDelay = false;
    }

    void SetCard()
    {
        MissionManager.Instance.mPassageCardAnswerText.text = null;
        if (CardImgs[mCurrentCardCounter] == null)
        {
            Debug.LogError("Error");
        }
        mEmptyCardImg.sprite = CardImgs[mCurrentCardCounter];
        for (int i = 0; i < MissionManager.Instance.AnsOrder[mCurrentCardCounter].Length; i++)
        {
            MissionManager.Instance.mPassageCardAnswerText.text += MissionManager.Instance.CurrentWording[mCurrentCardCounter][i];
            if (i < MissionManager.Instance.CurrentWording[mCurrentCardCounter].Length - 1)
            {
                MissionManager.Instance.mPassageCardAnswerText.text += " ";
            }

        }
        if (MissionManager.Instance.CurrentWording[mCurrentCardCounter] == MissionManager.Instance.AnsOrder[mCurrentCardCounter])
        {
            Debug.Log("已完成此卡");
            mFinishCardImage.SetActive(true);
        }
        else
        {
            Debug.Log("此卡未完成");
            mFinishCardImage.SetActive(false);
        }
        mWrongCardImage.SetActive(false);
    }

    public void ClearCard()
    {
        MissionManager.Instance.CurrentMissionData.mLetters.ClearLetter();
        foreach (LetterController letters in mCardLetterPanel.GetComponentsInChildren<LetterController>())
        {
            Destroy(letters.gameObject);
        }
        mCurrentCardCounter = 0;
        mEmptyCardImg.sprite = null;
        CardImgs = null;
    }

    IEnumerator CheckCardPanelDone()
    {
        if (mLoadCardDone && mLoadLetterDone)
        {
            mCardPanel.SetActive(false);
            yield return null;
        }
        else
        {
            yield return new WaitForFixedUpdate();
            yield return StartCoroutine(CheckCardPanelDone());
        }
    }

    public void AddLetter(string mLetter)
    {
        if (MissionManager.Instance.CurrentMissionData.mLetters.GetLetter(mLetter) == null)
        {
            AddNewLetter(mLetter, 1);
            BeaconLetterPanelController.Instance.ClearLetters();
            BeaconLetterPanelController.Instance.InitializeLetters();
            Debug.Log("LetterControllerDic : " + mLetter);
            return;
        }
        else
        {
            MissionManager.Instance.CurrentMissionData.mLetters.GetLetter(mLetter).AddLetterUI();
            Debug.Log("Add Letter : " + mLetter);
        }
    }
}

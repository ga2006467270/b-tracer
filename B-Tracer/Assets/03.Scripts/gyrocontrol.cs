﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gyrocontrol : MonoBehaviour
{
    private bool gyroEnable;
    private Gyroscope gyro;

    private GameObject cameraContainer;
    private Quaternion rot;

    // Use this for initialization
    void Start()
    {
        cameraContainer = new GameObject("Camera Container");
        cameraContainer.transform.position = transform.position;
        transform.SetParent(cameraContainer.transform);
        gyroEnable = Enablegyro();
    }

    private bool Enablegyro()
    {
        cameraContainer.transform.rotation = Quaternion.Euler(0, 0, 0);
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;

            rot = new Quaternion(0, 0, 1, 0);
            return true;
        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        if (gyroEnable)
        {
            transform.rotation = gyro.attitude * rot;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[Serializable]
public class PlayerData
{
    public string SaveAccount;
    public string SavePassword;
    public int PlayerID;
    public int Coins;
    public int WealthTracerRank;
    public int WealthTracerMoney;
    public int BestTracerRank;
    public int BestTracerMoney;
    public int finishedMission;
    public string Name;
    [SerializeField] List<string> mCardsText = new List<string>();

    public int SchoolID;

    public PlayerData SetPlayerData(string account, string password)
    {
        SaveAccount = account;
        SavePassword = password;
        return this;
    }

    public void Clear()
    {
        SaveAccount = null;
        SavePassword = null;
        PlayerID = 0;
        Coins = 0;
        Name = null;
    }

    public void SetCardsData(DownloadImage[] cardsData, bool saveCard)
    {
        Debug.LogError("設定CardsData : " + cardsData.Length);
        foreach (DownloadImage carddata in cardsData)
        {
            mCardsText.Add(carddata.GetCardsText());
            if (saveCard)
            {
                SavePicture(carddata.GetCardsSprite(), carddata.GetCardsText());
            }
        }
    }

    public void SetCardsData(List<string> cardsText)
    {
        mCardsText = cardsText;
    }

    public void ClearCardsData()
    {
        mCardsText.Clear(); ;
    }

    public void SavePicture(Sprite cardSprite, string cardsName)
    {
        Debug.Log("截圖");
        Texture2D screenShot = cardSprite.texture;
        byte[] bytes = screenShot.EncodeToPNG();
        string filename = cardsName;

        if (!Directory.Exists(Application.persistentDataPath))
        {
            Directory.CreateDirectory(Application.persistentDataPath);
        }
        File.WriteAllBytes(Application.persistentDataPath + "/" + filename +".png", bytes);
        Debug.Log(string.Format("Took screenshot to: {0}", Application.persistentDataPath + "/" + filename));
    }


    public List<string> GetCardsText()
    {
        return mCardsText;
    }
}

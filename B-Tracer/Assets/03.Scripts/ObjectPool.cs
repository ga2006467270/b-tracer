﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    public enum eSystemType
    {
        BestTracerAR,
    }

    static ObjectPool mInsatnce;

    [SerializeField] List<GameObject> mObjectPool = new List<GameObject>();

    Dictionary<string, GameObject> mObjectDic = new Dictionary<string, GameObject>();
    Dictionary<string, GameObject> mObjectInGame = new Dictionary<string, GameObject>();
    [SerializeField] eSystemType mSystemType = eSystemType.BestTracerAR;
    ObjectStore mFactoryStore;

    public static ObjectPool Instance
    {
        get
        {
            return mInsatnce;
        }
    }

    void Awake()
    {
        mInsatnce = this;
    }

    // 將物件池的物品放到dictionary中 以便未來呼叫
    public void SetObjectPool()
    {
        foreach (GameObject obj in mObjectPool)
        {
            if (!mObjectDic.ContainsKey(obj.name))
            {
                mObjectDic.Add(obj.name, obj);
            }
        }
    }

    public void LoadObjectPool()
    {
        mFactoryStore = new ObjectStore(new SimpleObjectFactory());
        mFactoryStore.ObjectOrder(mSystemType);
    }

    /// <summary>
    /// 將物件加入Dictionary
    /// </summary>
    /// <param name="name">物件名稱</param>
    public void AddObject(GameObject loadObject, string name)
    {
        if (mObjectDic.ContainsKey(name))
        {
            Debug.Log("已有此物件 : " + loadObject);
            return;
        }
        else
        {
            mObjectPool.Add(loadObject);
            mObjectDic.Add(name, loadObject);
        }
    }

    public GameObject LoadObject(string objName)
    {
        if (mObjectDic.ContainsKey(objName))
        {
            return mObjectDic[objName];
        }
        else
        {
            Debug.LogError("無此物件 : " + objName);
            return null;
        }
    }

    // 尋找在場景中的物件
    public GameObject GetObjectInGame(string stringName)
    {
        if (mObjectInGame.ContainsKey(stringName))
        {
            return mObjectInGame[stringName];
        }
        else
        {
            //Debug.Log("添加 : " + stringName + " 進入場景");
            return null;
        }
    }

    //在場景生成物件
    public void NewInstantiateObject(GameObject poolObject)
    {
        GameObject theobject = Object.Instantiate(poolObject, poolObject.transform.position, poolObject.transform.rotation);
        theobject.name = poolObject.name;
        mObjectInGame.Add(poolObject.name, theobject);
    }

    public void SetActiveObject(string ObjName)
    {
        mObjectInGame[ObjName].SetActive(!mObjectInGame[ObjName].activeSelf);
    }

    public class ObjectStore
    {
        SimpleObjectFactory mFactory;

        public ObjectStore(SimpleObjectFactory factory)
        {
            mFactory = factory;
        }

        public void ObjectOrder(eSystemType objectEnum)
        {
            IObjectLoadType objectloadtype;
            objectloadtype = mFactory.CreateObject(objectEnum);
            objectloadtype.LoadObject();
        }
    }

    public class SimpleObjectFactory
    {
        public IObjectLoadType CreateObject(ObjectPool.eSystemType objectType)
        {
            switch (objectType)
            {
                case ObjectPool.eSystemType.BestTracerAR:
                    return new ObjectLoadMove();
            }
            return null;
        }
    }
}

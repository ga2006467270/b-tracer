﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardCard : MonoBehaviour
{
    Sprite mCard;
    string mName;
    [SerializeField] Image mCardImg = null;
    [SerializeField] Text mImgText = null;

    public void SetRewardCard(Sprite card,string name)
    {
        mCard = card;
        mName = name;
        mCardImg.sprite = mCard;
        mImgText.text = mName;
    }

    public void ShowRewardCard()
    {
        GameManage.Instance.ShowCard(mCard, mName);
    }
}

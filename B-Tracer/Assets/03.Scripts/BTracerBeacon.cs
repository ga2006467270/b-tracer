﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTracerBeacon
{
    List<SingleBeaconData> BeaconName = new List<SingleBeaconData>();

    Dictionary<string, SingleBeaconData> BeaconDic = new Dictionary<string, SingleBeaconData>();

    // 新增Beacon，並根據Beacon名作判定
    public void Add(string UUID,string contents)
    {
        if (BeaconDic.ContainsKey(UUID))
        {
            BeaconDic[UUID].AddNumber(contents);
        }
        else
        {
            SingleBeaconData beacon = new SingleBeaconData();
            beacon.SetBeaconList(UUID, contents);
            //Debug.Log("加入 : " + beacon);
            BeaconName.Add(beacon);
            BeaconDic.Add(UUID, beacon);
        }
        //Debug.Log(BeaconName[BeaconName.Count - 1]);
    }

    public void RemoveAll()
    {
        BeaconName.Clear();
        BeaconDic.Clear();
    }

    // 給所有不同名字的beacon
    public string[] GetAllBeacons()
    {
        string[] beaconstring = new string[BeaconName.Count];
        for (int i = 0; i < beaconstring.Length; i++)
        {
            beaconstring[i] = BeaconName[i].UUID;
        }
        return beaconstring;
    }

    public int[] GetAllMajors()
    {
        int[] beaconstring = new int[BeaconName.Count];
        for (int i = 0; i < beaconstring.Length; i++)
        {
            beaconstring[i] = BeaconName[i].Major;
        }
        return beaconstring;
    }

    public int[] GetAllMinors()
    {
        int[] beaconstring = new int[BeaconName.Count];
        for (int i = 0; i < beaconstring.Length; i++)
        {
            beaconstring[i] = BeaconName[i].Minor;
        }
        return beaconstring;
    }

    public string[] GetBeaconNumber(string UUID)
    {
        if (BeaconDic.ContainsKey(UUID))
        {
            return BeaconDic[UUID].GetThisBeaconNumber();
        }
        else
        {
            Debug.LogError("沒有此UUID : " + UUID);
            return null;
        }
    }

}

public class SingleBeaconData
{
    public string UUID;
    public int Major;
    public int Minor;
    List<string> mContents = new List<string>();

    public void AddNumber(string number)
    {
        mContents.Add(number);
    }

    public string[] GetThisBeaconNumber()
    {
        return mContents.ToArray();
    }

    public void SetBeaconList(string name,string number)
    {
        UUID = name;
        mContents.Add(number);
    }
}

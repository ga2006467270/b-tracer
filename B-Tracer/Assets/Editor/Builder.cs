﻿using System;
using System.Linq;
using UnityEditor;

public static class Builder
{
    [MenuItem("Auto Builder/Build Android")]
    public static void PerformBuildWebGL()
    {
        //var path = "Builds/B-Tracer";

        if (UnityEditorInternal.InternalEditorUtility.inBatchMode)
        {
            //path = GetBatchModeBuildPath();

            //if (path == null)
            //{
            //    throw new ArgumentException("-output is not set");
            //}
        }
        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "assets/02.Scenes/MainScene.unity"};
        buildPlayerOptions.locationPathName = "Builds/AndroidBuild.apk";
        buildPlayerOptions.target = BuildTarget.Android;
        buildPlayerOptions.options = BuildOptions.None;

        //BuildPipeline.BuildPlayer(buildPlayerOptions);
        BuildPipeline.BuildPlayer(new[] { "assets/02.Scenes/MainScene.unity" }, "Builds/AndroidBuild/AndroidBuild.apk", BuildTarget.Android, BuildOptions.None);
        BuildPipeline.BuildPlayer(new[] { "assets/02.Scenes/MainScene.unity" }, "Builds/IOSBuild", BuildTarget.iOS, BuildOptions.None);
        //PerformBuildAuto(path + ".apk", BuildTarget.StandaloneWindows);
    }

    //static void PerformBuildAuto(string path, BuildTarget target)
    //{
    //    var scenes = EditorBuildSettings.scenes.Where((v) => { return v.enabled; }).Select((v) => { return v.path; });
    //    BuildPipeline.BuildPlayer(scenes.ToArray(), path, target, BuildOptions.None);
    //}

    static string GetBatchModeBuildPath()
    {
        var args = Environment.GetCommandLineArgs();
        for (int ii = 0; ii < args.Length; ++ii)
        {
            if (args[ii].ToLower() == "-output")
            {
                if (ii + 1 < args.Length)
                {
                    return args[ii + 1];
                }
            }
        }

        return null;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BDSLiteSpeechSample : MonoBehaviour
{
    [SerializeField]
    InputField SpeakText = null;

    void Awake()
    {
        BDSLiteSpeech.WakeUp();
    }

    // Start is called before the first frame update
    void Start()
    {
        //initialize to English
        BDSLiteSpeech.ChangeLanguageEng();
    }

    public void ClickSpeak()
    {
        BDSLiteSpeech.Speak(SpeakText.text);
    }

    public void ClickChinese()
    {
        BDSLiteSpeech.ChangeLanguageHK();
    }

    public void ClickEnglish()
    {
        BDSLiteSpeech.ChangeLanguageEng();
    }
}

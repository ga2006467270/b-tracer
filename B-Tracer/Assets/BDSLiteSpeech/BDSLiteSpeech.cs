﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class BDSLiteSpeech
{
#if UNITY_ANDROID
    static AndroidJavaClass plugin;
    static AndroidJavaObject context;
#elif UNITY_IPHONE
    [DllImport("__Internal")]
    private static extern void BeginSpeaking(string text);
    [DllImport("__Internal")]
    private static extern void SetIOSLang(string text1, string text2);
    [DllImport("__Internal")]
    private static extern void SetIOSPitch(string text);
    [DllImport("__Internal")]
    private static extern void SetIOSRate(string text);
#endif
    static BDSLiteSpeech()
    {
#if UNITY_ANDROID
#if !UNITY_EDITOR
        using (var activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            context = activity.GetStatic<AndroidJavaObject>("currentActivity");
        }
        plugin = new AndroidJavaClass("com.bds.speechsynthesisbds.SpeechSyn");
        if (plugin != null)
        {
            plugin.CallStatic("InitSpeech", context);
        }
#endif
#endif
    }

    public static void WakeUp()
    {
        //to wake up, better call me before real speak
    }

    public static void SetLang(string lang1, string lang2)
    {
#if UNITY_ANDROID
        plugin.CallStatic("SetLang", lang1, lang2);
#elif UNITY_IPHONE
        string lang = lang1;
        if (lang == "yue") lang = "zh";
#if !UNITY_EDITOR
        SetIOSLang(lang, lang2);
#endif
#endif
    }

    public static void Speak(string toSpeak)
    {
#if UNITY_ANDROID
        plugin.CallStatic("Speak", toSpeak);
#elif UNITY_IPHONE
        BeginSpeaking(toSpeak);
#endif
    }

    public static void StopSpeak()
    {
#if UNITY_ANDROID
        plugin.CallStatic("StopSpeak");
#endif
    }

    public static bool IsSpeaking()
    {
        bool isSpeaking = false;
#if UNITY_ANDROID
        isSpeaking = plugin.CallStatic<bool>("IsSpeaking");
#endif
        return isSpeaking;
    }

    //Short hand for lang switching
    public static void ChangeLanguageHK()
    {
        SetLang("yue", "HK");
    }

    public static void ChangeLanguageTW()
    {
        SetLang("zh", "TW");
    }

    public static void ChangeLanguageEng()
    {
        SetLang("en", "US");
    }

    public static void ChangeLanguageJP()
    {
        SetLang("ja", "JP");
    }

    public static void ChangeLanguageFR()
    {
        SetLang("fr", "BE");
    }
}

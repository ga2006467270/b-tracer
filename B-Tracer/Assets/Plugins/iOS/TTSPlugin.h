//
//  TTSPlugin.h
//  Unity3DTTSPlugin
//
//  Created by Eralp Karaduman on 12/5/16.
//  Copyright © 2016 Super Damage. All rights reserved.
//
//  Simplify by BDS @2019
//

@interface TTSPlugin: NSObject

- (void)beginSpeakingWithString:(NSString*)textString;

@end

﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mono.Xml.Schema.XsdIdentityPath
struct XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE;
// Mono.Xml.Schema.XsdIdentityPath[]
struct XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F;
// Mono.Xml.Schema.XsdIdentitySelector
struct XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8;
// System.AsyncCallback
struct AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.ArrayList
struct ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1;
// System.Collections.Hashtable
struct Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82;
// System.Collections.IComparer
struct IComparer_t971EE8726C43A8A086B35A11585E2AEFF2F7C2EB;
// System.Collections.IDictionary
struct IDictionary_tD35B9437F08BE98D1E0B295CC73C48E168CAB316;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_tFA82075E135A0ED63DE061DB58EB1B4799B3F517;
// System.Collections.IEnumerable
struct IEnumerable_t0B11E371EB34DA6482976C09E2C231E9B32AB517;
// System.Collections.IEnumerator
struct IEnumerator_t5F4AD85C6EA424A50584F741049EA645DBD8EEFC;
// System.Collections.IEqualityComparer
struct IEqualityComparer_tFBE0A1A09BAE01058D6DE65DD4FE16C50CB8E781;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t2302C769B9686FD002965B00B8A3704D828517D5;
// System.Collections.Specialized.HybridDictionary
struct HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747;
// System.Collections.Specialized.ListDictionary
struct ListDictionary_tD949561FF8FD1EBEB444214919203DD41A76ECCA;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB;
// System.Collections.Specialized.NameObjectCollectionBase/_Item
struct _Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7;
// System.Configuration.Configuration
struct Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01;
// System.Configuration.ConfigurationCollectionAttribute
struct ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318;
// System.Configuration.ConfigurationElement
struct ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63;
// System.Configuration.ConfigurationElementCollection
struct ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E;
// System.Configuration.ConfigurationLocationCollection
struct ConfigurationLocationCollection_t8FA9CDDF11231DCA26928D9575D87A679C45B65A;
// System.Configuration.ConfigurationLockCollection
struct ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9;
// System.Configuration.ConfigurationSectionGroup
struct ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79;
// System.Configuration.ElementInformation
struct ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26;
// System.Configuration.ElementMap
struct ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC;
// System.Configuration.Internal.IConfigSystem
struct IConfigSystem_tEFD1419637BF7C34865397450B3E3CA5EFCAEBEC;
// System.Configuration.Internal.IInternalConfigHost
struct IInternalConfigHost_tF3B427C9E9DC4E866B1A69F1E347ABEC76BBCC8C;
// System.Configuration.SectionGroupInfo
struct SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61;
// System.DelegateData
struct DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9;
// System.IAsyncResult
struct IAsyncResult_tDA33C24465239FB383C4C2CDAAC43B9AD3CB7F05;
// System.IntPtr[]
struct IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659;
// System.Object[]
struct ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B;
// System.Text.RegularExpressions.Regex[]
struct RegexU5BU5D_tEA400C40510836EBE79B3CFB8AFC12F94B3690A1;
// System.Type
struct Type_t;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;
// System.Xml.Schema.ValidationEventArgs
struct ValidationEventArgs_t509D590153253EAD1DC5DCEB4CDD56BE30AF3B1D;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696;
// System.Xml.Schema.XmlSchemaAnnotation
struct XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77;
// System.Xml.Schema.XmlSchemaCompilationSettings
struct XmlSchemaCompilationSettings_t84466353D7B09B95C9D10D856332058F60FD31CD;
// System.Xml.Schema.XmlSchemaContent
struct XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64;
// System.Xml.Schema.XmlSchemaGroup
struct XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447;
// System.Xml.Schema.XmlSchemaGroupBase
struct XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F;
// System.Xml.Schema.XmlSchemaIdentityConstraint
struct XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747;
// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D;
// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16;
// System.Xml.Schema.XmlSchemaPatternFacet
struct XmlSchemaPatternFacet_t1B3D1C8F8D7DF14F25DC513CC73DA23F87F0F027;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908;
// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct XmlSchemaSimpleTypeContent_t9BD29AE1058626894FD1F7EEAC14CD35BC3428EE;
// System.Xml.Schema.XmlSchemaSimpleType[]
struct XmlSchemaSimpleTypeU5BU5D_t6E3AC48639AC0737688DBE5439FA9E940F0E1CF0;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE;
// System.Xml.Schema.XmlSchemaXPath
struct XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526;
// System.Xml.Serialization.XmlSerializerNamespaces
struct XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3;
// System.Xml.XmlAttribute[]
struct XmlAttributeU5BU5D_t48E79142153B9C691C51308E60A01F22035CBD01;
// System.Xml.XmlNameTable
struct XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86;
// System.Xml.XmlNodeChangedEventArgs
struct XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004;
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_t51B92152D4EBF947BF1D099E8398C5820F8CC88A;
// System.Xml.XmlReader
struct XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB;
// System.Xml.XmlResolver
struct XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C;




#ifndef U3CMODULEU3E_TC4DF359A31728089F9FBC88966828166FB750DFD_H
#define U3CMODULEU3E_TC4DF359A31728089F9FBC88966828166FB750DFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tC4DF359A31728089F9FBC88966828166FB750DFD 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_TC4DF359A31728089F9FBC88966828166FB750DFD_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#define ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#ifndef COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#define COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F, ___list_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_list_0() const { return ___list_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#ifndef READONLYCOLLECTIONBASE_T100D9B9D5FC37926098ECE105DAC4670F688BD2B_H
#define READONLYCOLLECTIONBASE_T100D9B9D5FC37926098ECE105DAC4670F688BD2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ReadOnlyCollectionBase
struct  ReadOnlyCollectionBase_t100D9B9D5FC37926098ECE105DAC4670F688BD2B  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.ReadOnlyCollectionBase::list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(ReadOnlyCollectionBase_t100D9B9D5FC37926098ECE105DAC4670F688BD2B, ___list_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_list_0() const { return ___list_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READONLYCOLLECTIONBASE_T100D9B9D5FC37926098ECE105DAC4670F688BD2B_H
#ifndef NAMEOBJECTCOLLECTIONBASE_TC2EE4FB130214FAE7365D519959A2A34DE56E070_H
#define NAMEOBJECTCOLLECTIONBASE_TC2EE4FB130214FAE7365D519959A2A34DE56E070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Collections.Specialized.NameObjectCollectionBase::m_ItemsContainer
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___m_ItemsContainer_0;
	// System.Collections.Specialized.NameObjectCollectionBase__Item System.Collections.Specialized.NameObjectCollectionBase::m_NullKeyItem
	_Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7 * ___m_NullKeyItem_1;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::m_ItemsArray
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___m_ItemsArray_2;
	// System.Collections.IHashCodeProvider System.Collections.Specialized.NameObjectCollectionBase::m_hashprovider
	RuntimeObject* ___m_hashprovider_3;
	// System.Collections.IComparer System.Collections.Specialized.NameObjectCollectionBase::m_comparer
	RuntimeObject* ___m_comparer_4;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::m_defCapacity
	int32_t ___m_defCapacity_5;
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::m_readonly
	bool ___m_readonly_6;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::infoCopy
	SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * ___infoCopy_7;
	// System.Collections.Specialized.NameObjectCollectionBase_KeysCollection System.Collections.Specialized.NameObjectCollectionBase::keyscoll
	KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB * ___keyscoll_8;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::equality_comparer
	RuntimeObject* ___equality_comparer_9;

public:
	inline static int32_t get_offset_of_m_ItemsContainer_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_ItemsContainer_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_m_ItemsContainer_0() const { return ___m_ItemsContainer_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_m_ItemsContainer_0() { return &___m_ItemsContainer_0; }
	inline void set_m_ItemsContainer_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___m_ItemsContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsContainer_0), value);
	}

	inline static int32_t get_offset_of_m_NullKeyItem_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_NullKeyItem_1)); }
	inline _Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7 * get_m_NullKeyItem_1() const { return ___m_NullKeyItem_1; }
	inline _Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7 ** get_address_of_m_NullKeyItem_1() { return &___m_NullKeyItem_1; }
	inline void set_m_NullKeyItem_1(_Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7 * value)
	{
		___m_NullKeyItem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NullKeyItem_1), value);
	}

	inline static int32_t get_offset_of_m_ItemsArray_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_ItemsArray_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_m_ItemsArray_2() const { return ___m_ItemsArray_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_m_ItemsArray_2() { return &___m_ItemsArray_2; }
	inline void set_m_ItemsArray_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___m_ItemsArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsArray_2), value);
	}

	inline static int32_t get_offset_of_m_hashprovider_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_hashprovider_3)); }
	inline RuntimeObject* get_m_hashprovider_3() const { return ___m_hashprovider_3; }
	inline RuntimeObject** get_address_of_m_hashprovider_3() { return &___m_hashprovider_3; }
	inline void set_m_hashprovider_3(RuntimeObject* value)
	{
		___m_hashprovider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashprovider_3), value);
	}

	inline static int32_t get_offset_of_m_comparer_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_comparer_4)); }
	inline RuntimeObject* get_m_comparer_4() const { return ___m_comparer_4; }
	inline RuntimeObject** get_address_of_m_comparer_4() { return &___m_comparer_4; }
	inline void set_m_comparer_4(RuntimeObject* value)
	{
		___m_comparer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_comparer_4), value);
	}

	inline static int32_t get_offset_of_m_defCapacity_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_defCapacity_5)); }
	inline int32_t get_m_defCapacity_5() const { return ___m_defCapacity_5; }
	inline int32_t* get_address_of_m_defCapacity_5() { return &___m_defCapacity_5; }
	inline void set_m_defCapacity_5(int32_t value)
	{
		___m_defCapacity_5 = value;
	}

	inline static int32_t get_offset_of_m_readonly_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_readonly_6)); }
	inline bool get_m_readonly_6() const { return ___m_readonly_6; }
	inline bool* get_address_of_m_readonly_6() { return &___m_readonly_6; }
	inline void set_m_readonly_6(bool value)
	{
		___m_readonly_6 = value;
	}

	inline static int32_t get_offset_of_infoCopy_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___infoCopy_7)); }
	inline SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * get_infoCopy_7() const { return ___infoCopy_7; }
	inline SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 ** get_address_of_infoCopy_7() { return &___infoCopy_7; }
	inline void set_infoCopy_7(SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * value)
	{
		___infoCopy_7 = value;
		Il2CppCodeGenWriteBarrier((&___infoCopy_7), value);
	}

	inline static int32_t get_offset_of_keyscoll_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___keyscoll_8)); }
	inline KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB * get_keyscoll_8() const { return ___keyscoll_8; }
	inline KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB ** get_address_of_keyscoll_8() { return &___keyscoll_8; }
	inline void set_keyscoll_8(KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB * value)
	{
		___keyscoll_8 = value;
		Il2CppCodeGenWriteBarrier((&___keyscoll_8), value);
	}

	inline static int32_t get_offset_of_equality_comparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___equality_comparer_9)); }
	inline RuntimeObject* get_equality_comparer_9() const { return ___equality_comparer_9; }
	inline RuntimeObject** get_address_of_equality_comparer_9() { return &___equality_comparer_9; }
	inline void set_equality_comparer_9(RuntimeObject* value)
	{
		___equality_comparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___equality_comparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_TC2EE4FB130214FAE7365D519959A2A34DE56E070_H
#ifndef CLIENTCONFIGURATIONSYSTEM_TF9A420DBA500361EBA6B5F50B8C329DC560FD58B_H
#define CLIENTCONFIGURATIONSYSTEM_TF9A420DBA500361EBA6B5F50B8C329DC560FD58B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ClientConfigurationSystem
struct  ClientConfigurationSystem_tF9A420DBA500361EBA6B5F50B8C329DC560FD58B  : public RuntimeObject
{
public:
	// System.Configuration.Configuration System.Configuration.ClientConfigurationSystem::cfg
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * ___cfg_0;

public:
	inline static int32_t get_offset_of_cfg_0() { return static_cast<int32_t>(offsetof(ClientConfigurationSystem_tF9A420DBA500361EBA6B5F50B8C329DC560FD58B, ___cfg_0)); }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * get_cfg_0() const { return ___cfg_0; }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 ** get_address_of_cfg_0() { return &___cfg_0; }
	inline void set_cfg_0(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * value)
	{
		___cfg_0 = value;
		Il2CppCodeGenWriteBarrier((&___cfg_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIENTCONFIGURATIONSYSTEM_TF9A420DBA500361EBA6B5F50B8C329DC560FD58B_H
#ifndef CONFIGINFO_TCCB111FBFC8E13282C1FACD3B69A2709D96EA698_H
#define CONFIGINFO_TCCB111FBFC8E13282C1FACD3B69A2709D96EA698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigInfo
struct  ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigInfo::Name
	String_t* ___Name_0;
	// System.String System.Configuration.ConfigInfo::TypeName
	String_t* ___TypeName_1;
	// System.Type System.Configuration.ConfigInfo::Type
	Type_t * ___Type_2;
	// System.String System.Configuration.ConfigInfo::streamName
	String_t* ___streamName_3;
	// System.Configuration.ConfigInfo System.Configuration.ConfigInfo::Parent
	ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698 * ___Parent_4;
	// System.Configuration.Internal.IInternalConfigHost System.Configuration.ConfigInfo::ConfigHost
	RuntimeObject* ___ConfigHost_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698, ___Type_2)); }
	inline Type_t * get_Type_2() const { return ___Type_2; }
	inline Type_t ** get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(Type_t * value)
	{
		___Type_2 = value;
		Il2CppCodeGenWriteBarrier((&___Type_2), value);
	}

	inline static int32_t get_offset_of_streamName_3() { return static_cast<int32_t>(offsetof(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698, ___streamName_3)); }
	inline String_t* get_streamName_3() const { return ___streamName_3; }
	inline String_t** get_address_of_streamName_3() { return &___streamName_3; }
	inline void set_streamName_3(String_t* value)
	{
		___streamName_3 = value;
		Il2CppCodeGenWriteBarrier((&___streamName_3), value);
	}

	inline static int32_t get_offset_of_Parent_4() { return static_cast<int32_t>(offsetof(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698, ___Parent_4)); }
	inline ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698 * get_Parent_4() const { return ___Parent_4; }
	inline ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698 ** get_address_of_Parent_4() { return &___Parent_4; }
	inline void set_Parent_4(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698 * value)
	{
		___Parent_4 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_4), value);
	}

	inline static int32_t get_offset_of_ConfigHost_5() { return static_cast<int32_t>(offsetof(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698, ___ConfigHost_5)); }
	inline RuntimeObject* get_ConfigHost_5() const { return ___ConfigHost_5; }
	inline RuntimeObject** get_address_of_ConfigHost_5() { return &___ConfigHost_5; }
	inline void set_ConfigHost_5(RuntimeObject* value)
	{
		___ConfigHost_5 = value;
		Il2CppCodeGenWriteBarrier((&___ConfigHost_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGINFO_TCCB111FBFC8E13282C1FACD3B69A2709D96EA698_H
#ifndef CONFIGURATION_T29470A3F7814259C2078D6C6229F221EE569FC01_H
#define CONFIGURATION_T29470A3F7814259C2078D6C6229F221EE569FC01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.Configuration
struct  Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01  : public RuntimeObject
{
public:
	// System.Configuration.Configuration System.Configuration.Configuration::parent
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * ___parent_0;
	// System.Collections.Hashtable System.Configuration.Configuration::elementData
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___elementData_1;
	// System.String System.Configuration.Configuration::streamName
	String_t* ___streamName_2;
	// System.Configuration.ConfigurationSectionGroup System.Configuration.Configuration::rootSectionGroup
	ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79 * ___rootSectionGroup_3;
	// System.Configuration.ConfigurationLocationCollection System.Configuration.Configuration::locations
	ConfigurationLocationCollection_t8FA9CDDF11231DCA26928D9575D87A679C45B65A * ___locations_4;
	// System.Configuration.SectionGroupInfo System.Configuration.Configuration::rootGroup
	SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 * ___rootGroup_5;
	// System.Configuration.Internal.IConfigSystem System.Configuration.Configuration::system
	RuntimeObject* ___system_6;
	// System.Boolean System.Configuration.Configuration::hasFile
	bool ___hasFile_7;
	// System.String System.Configuration.Configuration::rootNamespace
	String_t* ___rootNamespace_8;
	// System.String System.Configuration.Configuration::configPath
	String_t* ___configPath_9;
	// System.String System.Configuration.Configuration::locationConfigPath
	String_t* ___locationConfigPath_10;
	// System.String System.Configuration.Configuration::locationSubPath
	String_t* ___locationSubPath_11;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01, ___parent_0)); }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * get_parent_0() const { return ___parent_0; }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_elementData_1() { return static_cast<int32_t>(offsetof(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01, ___elementData_1)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_elementData_1() const { return ___elementData_1; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_elementData_1() { return &___elementData_1; }
	inline void set_elementData_1(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___elementData_1 = value;
		Il2CppCodeGenWriteBarrier((&___elementData_1), value);
	}

	inline static int32_t get_offset_of_streamName_2() { return static_cast<int32_t>(offsetof(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01, ___streamName_2)); }
	inline String_t* get_streamName_2() const { return ___streamName_2; }
	inline String_t** get_address_of_streamName_2() { return &___streamName_2; }
	inline void set_streamName_2(String_t* value)
	{
		___streamName_2 = value;
		Il2CppCodeGenWriteBarrier((&___streamName_2), value);
	}

	inline static int32_t get_offset_of_rootSectionGroup_3() { return static_cast<int32_t>(offsetof(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01, ___rootSectionGroup_3)); }
	inline ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79 * get_rootSectionGroup_3() const { return ___rootSectionGroup_3; }
	inline ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79 ** get_address_of_rootSectionGroup_3() { return &___rootSectionGroup_3; }
	inline void set_rootSectionGroup_3(ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79 * value)
	{
		___rootSectionGroup_3 = value;
		Il2CppCodeGenWriteBarrier((&___rootSectionGroup_3), value);
	}

	inline static int32_t get_offset_of_locations_4() { return static_cast<int32_t>(offsetof(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01, ___locations_4)); }
	inline ConfigurationLocationCollection_t8FA9CDDF11231DCA26928D9575D87A679C45B65A * get_locations_4() const { return ___locations_4; }
	inline ConfigurationLocationCollection_t8FA9CDDF11231DCA26928D9575D87A679C45B65A ** get_address_of_locations_4() { return &___locations_4; }
	inline void set_locations_4(ConfigurationLocationCollection_t8FA9CDDF11231DCA26928D9575D87A679C45B65A * value)
	{
		___locations_4 = value;
		Il2CppCodeGenWriteBarrier((&___locations_4), value);
	}

	inline static int32_t get_offset_of_rootGroup_5() { return static_cast<int32_t>(offsetof(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01, ___rootGroup_5)); }
	inline SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 * get_rootGroup_5() const { return ___rootGroup_5; }
	inline SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 ** get_address_of_rootGroup_5() { return &___rootGroup_5; }
	inline void set_rootGroup_5(SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 * value)
	{
		___rootGroup_5 = value;
		Il2CppCodeGenWriteBarrier((&___rootGroup_5), value);
	}

	inline static int32_t get_offset_of_system_6() { return static_cast<int32_t>(offsetof(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01, ___system_6)); }
	inline RuntimeObject* get_system_6() const { return ___system_6; }
	inline RuntimeObject** get_address_of_system_6() { return &___system_6; }
	inline void set_system_6(RuntimeObject* value)
	{
		___system_6 = value;
		Il2CppCodeGenWriteBarrier((&___system_6), value);
	}

	inline static int32_t get_offset_of_hasFile_7() { return static_cast<int32_t>(offsetof(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01, ___hasFile_7)); }
	inline bool get_hasFile_7() const { return ___hasFile_7; }
	inline bool* get_address_of_hasFile_7() { return &___hasFile_7; }
	inline void set_hasFile_7(bool value)
	{
		___hasFile_7 = value;
	}

	inline static int32_t get_offset_of_rootNamespace_8() { return static_cast<int32_t>(offsetof(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01, ___rootNamespace_8)); }
	inline String_t* get_rootNamespace_8() const { return ___rootNamespace_8; }
	inline String_t** get_address_of_rootNamespace_8() { return &___rootNamespace_8; }
	inline void set_rootNamespace_8(String_t* value)
	{
		___rootNamespace_8 = value;
		Il2CppCodeGenWriteBarrier((&___rootNamespace_8), value);
	}

	inline static int32_t get_offset_of_configPath_9() { return static_cast<int32_t>(offsetof(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01, ___configPath_9)); }
	inline String_t* get_configPath_9() const { return ___configPath_9; }
	inline String_t** get_address_of_configPath_9() { return &___configPath_9; }
	inline void set_configPath_9(String_t* value)
	{
		___configPath_9 = value;
		Il2CppCodeGenWriteBarrier((&___configPath_9), value);
	}

	inline static int32_t get_offset_of_locationConfigPath_10() { return static_cast<int32_t>(offsetof(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01, ___locationConfigPath_10)); }
	inline String_t* get_locationConfigPath_10() const { return ___locationConfigPath_10; }
	inline String_t** get_address_of_locationConfigPath_10() { return &___locationConfigPath_10; }
	inline void set_locationConfigPath_10(String_t* value)
	{
		___locationConfigPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___locationConfigPath_10), value);
	}

	inline static int32_t get_offset_of_locationSubPath_11() { return static_cast<int32_t>(offsetof(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01, ___locationSubPath_11)); }
	inline String_t* get_locationSubPath_11() const { return ___locationSubPath_11; }
	inline String_t** get_address_of_locationSubPath_11() { return &___locationSubPath_11; }
	inline void set_locationSubPath_11(String_t* value)
	{
		___locationSubPath_11 = value;
		Il2CppCodeGenWriteBarrier((&___locationSubPath_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATION_T29470A3F7814259C2078D6C6229F221EE569FC01_H
#ifndef CONFIGURATIONELEMENT_T0102651B4A826E7C1A21DB9AF0C7565C66A53D63_H
#define CONFIGURATIONELEMENT_T0102651B4A826E7C1A21DB9AF0C7565C66A53D63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElement
struct  ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationElement::rawXml
	String_t* ___rawXml_0;
	// System.Boolean System.Configuration.ConfigurationElement::modified
	bool ___modified_1;
	// System.Configuration.ElementMap System.Configuration.ConfigurationElement::map
	ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC * ___map_2;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ConfigurationElement::keyProps
	ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * ___keyProps_3;
	// System.Configuration.ConfigurationElementCollection System.Configuration.ConfigurationElement::defaultCollection
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E * ___defaultCollection_4;
	// System.Boolean System.Configuration.ConfigurationElement::readOnly
	bool ___readOnly_5;
	// System.Configuration.ElementInformation System.Configuration.ConfigurationElement::elementInfo
	ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26 * ___elementInfo_6;
	// System.Configuration.Configuration System.Configuration.ConfigurationElement::_configuration
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * ____configuration_7;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAllAttributesExcept
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * ___lockAllAttributesExcept_8;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAllElementsExcept
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * ___lockAllElementsExcept_9;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAttributes
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * ___lockAttributes_10;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockElements
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * ___lockElements_11;
	// System.Boolean System.Configuration.ConfigurationElement::lockItem
	bool ___lockItem_12;

public:
	inline static int32_t get_offset_of_rawXml_0() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___rawXml_0)); }
	inline String_t* get_rawXml_0() const { return ___rawXml_0; }
	inline String_t** get_address_of_rawXml_0() { return &___rawXml_0; }
	inline void set_rawXml_0(String_t* value)
	{
		___rawXml_0 = value;
		Il2CppCodeGenWriteBarrier((&___rawXml_0), value);
	}

	inline static int32_t get_offset_of_modified_1() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___modified_1)); }
	inline bool get_modified_1() const { return ___modified_1; }
	inline bool* get_address_of_modified_1() { return &___modified_1; }
	inline void set_modified_1(bool value)
	{
		___modified_1 = value;
	}

	inline static int32_t get_offset_of_map_2() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___map_2)); }
	inline ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC * get_map_2() const { return ___map_2; }
	inline ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC ** get_address_of_map_2() { return &___map_2; }
	inline void set_map_2(ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC * value)
	{
		___map_2 = value;
		Il2CppCodeGenWriteBarrier((&___map_2), value);
	}

	inline static int32_t get_offset_of_keyProps_3() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___keyProps_3)); }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * get_keyProps_3() const { return ___keyProps_3; }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 ** get_address_of_keyProps_3() { return &___keyProps_3; }
	inline void set_keyProps_3(ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * value)
	{
		___keyProps_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyProps_3), value);
	}

	inline static int32_t get_offset_of_defaultCollection_4() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___defaultCollection_4)); }
	inline ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E * get_defaultCollection_4() const { return ___defaultCollection_4; }
	inline ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E ** get_address_of_defaultCollection_4() { return &___defaultCollection_4; }
	inline void set_defaultCollection_4(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E * value)
	{
		___defaultCollection_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultCollection_4), value);
	}

	inline static int32_t get_offset_of_readOnly_5() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___readOnly_5)); }
	inline bool get_readOnly_5() const { return ___readOnly_5; }
	inline bool* get_address_of_readOnly_5() { return &___readOnly_5; }
	inline void set_readOnly_5(bool value)
	{
		___readOnly_5 = value;
	}

	inline static int32_t get_offset_of_elementInfo_6() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___elementInfo_6)); }
	inline ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26 * get_elementInfo_6() const { return ___elementInfo_6; }
	inline ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26 ** get_address_of_elementInfo_6() { return &___elementInfo_6; }
	inline void set_elementInfo_6(ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26 * value)
	{
		___elementInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___elementInfo_6), value);
	}

	inline static int32_t get_offset_of__configuration_7() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ____configuration_7)); }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * get__configuration_7() const { return ____configuration_7; }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 ** get_address_of__configuration_7() { return &____configuration_7; }
	inline void set__configuration_7(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * value)
	{
		____configuration_7 = value;
		Il2CppCodeGenWriteBarrier((&____configuration_7), value);
	}

	inline static int32_t get_offset_of_lockAllAttributesExcept_8() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___lockAllAttributesExcept_8)); }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * get_lockAllAttributesExcept_8() const { return ___lockAllAttributesExcept_8; }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C ** get_address_of_lockAllAttributesExcept_8() { return &___lockAllAttributesExcept_8; }
	inline void set_lockAllAttributesExcept_8(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * value)
	{
		___lockAllAttributesExcept_8 = value;
		Il2CppCodeGenWriteBarrier((&___lockAllAttributesExcept_8), value);
	}

	inline static int32_t get_offset_of_lockAllElementsExcept_9() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___lockAllElementsExcept_9)); }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * get_lockAllElementsExcept_9() const { return ___lockAllElementsExcept_9; }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C ** get_address_of_lockAllElementsExcept_9() { return &___lockAllElementsExcept_9; }
	inline void set_lockAllElementsExcept_9(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * value)
	{
		___lockAllElementsExcept_9 = value;
		Il2CppCodeGenWriteBarrier((&___lockAllElementsExcept_9), value);
	}

	inline static int32_t get_offset_of_lockAttributes_10() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___lockAttributes_10)); }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * get_lockAttributes_10() const { return ___lockAttributes_10; }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C ** get_address_of_lockAttributes_10() { return &___lockAttributes_10; }
	inline void set_lockAttributes_10(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * value)
	{
		___lockAttributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___lockAttributes_10), value);
	}

	inline static int32_t get_offset_of_lockElements_11() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___lockElements_11)); }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * get_lockElements_11() const { return ___lockElements_11; }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C ** get_address_of_lockElements_11() { return &___lockElements_11; }
	inline void set_lockElements_11(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * value)
	{
		___lockElements_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockElements_11), value);
	}

	inline static int32_t get_offset_of_lockItem_12() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___lockItem_12)); }
	inline bool get_lockItem_12() const { return ___lockItem_12; }
	inline bool* get_address_of_lockItem_12() { return &___lockItem_12; }
	inline void set_lockItem_12(bool value)
	{
		___lockItem_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENT_T0102651B4A826E7C1A21DB9AF0C7565C66A53D63_H
#ifndef CONFIGURATIONFILEMAP_TE596BD02408620847DC8AA9D93423B610AB224F0_H
#define CONFIGURATIONFILEMAP_TE596BD02408620847DC8AA9D93423B610AB224F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationFileMap
struct  ConfigurationFileMap_tE596BD02408620847DC8AA9D93423B610AB224F0  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationFileMap::machineConfigFilename
	String_t* ___machineConfigFilename_0;

public:
	inline static int32_t get_offset_of_machineConfigFilename_0() { return static_cast<int32_t>(offsetof(ConfigurationFileMap_tE596BD02408620847DC8AA9D93423B610AB224F0, ___machineConfigFilename_0)); }
	inline String_t* get_machineConfigFilename_0() const { return ___machineConfigFilename_0; }
	inline String_t** get_address_of_machineConfigFilename_0() { return &___machineConfigFilename_0; }
	inline void set_machineConfigFilename_0(String_t* value)
	{
		___machineConfigFilename_0 = value;
		Il2CppCodeGenWriteBarrier((&___machineConfigFilename_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONFILEMAP_TE596BD02408620847DC8AA9D93423B610AB224F0_H
#ifndef CONFIGURATIONLOCATION_T1D40B1D62EAFBBB2FA8ACF23596B350693267C84_H
#define CONFIGURATIONLOCATION_T1D40B1D62EAFBBB2FA8ACF23596B350693267C84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationLocation
struct  ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationLocation::path
	String_t* ___path_1;
	// System.Configuration.Configuration System.Configuration.ConfigurationLocation::configuration
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * ___configuration_2;
	// System.Configuration.Configuration System.Configuration.ConfigurationLocation::parent
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * ___parent_3;
	// System.String System.Configuration.ConfigurationLocation::xmlContent
	String_t* ___xmlContent_4;
	// System.Boolean System.Configuration.ConfigurationLocation::parentResolved
	bool ___parentResolved_5;
	// System.Boolean System.Configuration.ConfigurationLocation::allowOverride
	bool ___allowOverride_6;

public:
	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((&___path_1), value);
	}

	inline static int32_t get_offset_of_configuration_2() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84, ___configuration_2)); }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * get_configuration_2() const { return ___configuration_2; }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 ** get_address_of_configuration_2() { return &___configuration_2; }
	inline void set_configuration_2(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * value)
	{
		___configuration_2 = value;
		Il2CppCodeGenWriteBarrier((&___configuration_2), value);
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84, ___parent_3)); }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * get_parent_3() const { return ___parent_3; }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((&___parent_3), value);
	}

	inline static int32_t get_offset_of_xmlContent_4() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84, ___xmlContent_4)); }
	inline String_t* get_xmlContent_4() const { return ___xmlContent_4; }
	inline String_t** get_address_of_xmlContent_4() { return &___xmlContent_4; }
	inline void set_xmlContent_4(String_t* value)
	{
		___xmlContent_4 = value;
		Il2CppCodeGenWriteBarrier((&___xmlContent_4), value);
	}

	inline static int32_t get_offset_of_parentResolved_5() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84, ___parentResolved_5)); }
	inline bool get_parentResolved_5() const { return ___parentResolved_5; }
	inline bool* get_address_of_parentResolved_5() { return &___parentResolved_5; }
	inline void set_parentResolved_5(bool value)
	{
		___parentResolved_5 = value;
	}

	inline static int32_t get_offset_of_allowOverride_6() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84, ___allowOverride_6)); }
	inline bool get_allowOverride_6() const { return ___allowOverride_6; }
	inline bool* get_address_of_allowOverride_6() { return &___allowOverride_6; }
	inline void set_allowOverride_6(bool value)
	{
		___allowOverride_6 = value;
	}
};

struct ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84_StaticFields
{
public:
	// System.Char[] System.Configuration.ConfigurationLocation::pathTrimChars
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___pathTrimChars_0;

public:
	inline static int32_t get_offset_of_pathTrimChars_0() { return static_cast<int32_t>(offsetof(ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84_StaticFields, ___pathTrimChars_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_pathTrimChars_0() const { return ___pathTrimChars_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_pathTrimChars_0() { return &___pathTrimChars_0; }
	inline void set_pathTrimChars_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___pathTrimChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___pathTrimChars_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONLOCATION_T1D40B1D62EAFBBB2FA8ACF23596B350693267C84_H
#ifndef ELEMENTMAP_T6891F6CAB535C88620E528446A2766C300236EEC_H
#define ELEMENTMAP_T6891F6CAB535C88620E528446A2766C300236EEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ElementMap
struct  ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC  : public RuntimeObject
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ElementMap::properties
	ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * ___properties_1;
	// System.Configuration.ConfigurationCollectionAttribute System.Configuration.ElementMap::collectionAttribute
	ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318 * ___collectionAttribute_2;

public:
	inline static int32_t get_offset_of_properties_1() { return static_cast<int32_t>(offsetof(ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC, ___properties_1)); }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * get_properties_1() const { return ___properties_1; }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 ** get_address_of_properties_1() { return &___properties_1; }
	inline void set_properties_1(ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * value)
	{
		___properties_1 = value;
		Il2CppCodeGenWriteBarrier((&___properties_1), value);
	}

	inline static int32_t get_offset_of_collectionAttribute_2() { return static_cast<int32_t>(offsetof(ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC, ___collectionAttribute_2)); }
	inline ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318 * get_collectionAttribute_2() const { return ___collectionAttribute_2; }
	inline ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318 ** get_address_of_collectionAttribute_2() { return &___collectionAttribute_2; }
	inline void set_collectionAttribute_2(ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318 * value)
	{
		___collectionAttribute_2 = value;
		Il2CppCodeGenWriteBarrier((&___collectionAttribute_2), value);
	}
};

struct ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC_StaticFields
{
public:
	// System.Collections.Hashtable System.Configuration.ElementMap::elementMaps
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___elementMaps_0;

public:
	inline static int32_t get_offset_of_elementMaps_0() { return static_cast<int32_t>(offsetof(ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC_StaticFields, ___elementMaps_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_elementMaps_0() const { return ___elementMaps_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_elementMaps_0() { return &___elementMaps_0; }
	inline void set_elementMaps_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___elementMaps_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementMaps_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTMAP_T6891F6CAB535C88620E528446A2766C300236EEC_H
#ifndef PROVIDERBASE_T92D989F50324ADBD9423D8B2E92FEE752477D380_H
#define PROVIDERBASE_T92D989F50324ADBD9423D8B2E92FEE752477D380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.Provider.ProviderBase
struct  ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380  : public RuntimeObject
{
public:
	// System.Boolean System.Configuration.Provider.ProviderBase::alreadyInitialized
	bool ___alreadyInitialized_0;
	// System.String System.Configuration.Provider.ProviderBase::_description
	String_t* ____description_1;
	// System.String System.Configuration.Provider.ProviderBase::_name
	String_t* ____name_2;

public:
	inline static int32_t get_offset_of_alreadyInitialized_0() { return static_cast<int32_t>(offsetof(ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380, ___alreadyInitialized_0)); }
	inline bool get_alreadyInitialized_0() const { return ___alreadyInitialized_0; }
	inline bool* get_address_of_alreadyInitialized_0() { return &___alreadyInitialized_0; }
	inline void set_alreadyInitialized_0(bool value)
	{
		___alreadyInitialized_0 = value;
	}

	inline static int32_t get_offset_of__description_1() { return static_cast<int32_t>(offsetof(ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380, ____description_1)); }
	inline String_t* get__description_1() const { return ____description_1; }
	inline String_t** get_address_of__description_1() { return &____description_1; }
	inline void set__description_1(String_t* value)
	{
		____description_1 = value;
		Il2CppCodeGenWriteBarrier((&____description_1), value);
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERBASE_T92D989F50324ADBD9423D8B2E92FEE752477D380_H
#ifndef PROVIDERCOLLECTION_TF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386_H
#define PROVIDERCOLLECTION_TF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.Provider.ProviderCollection
struct  ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Configuration.Provider.ProviderCollection::lookup
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___lookup_0;
	// System.Boolean System.Configuration.Provider.ProviderCollection::readOnly
	bool ___readOnly_1;
	// System.Collections.ArrayList System.Configuration.Provider.ProviderCollection::values
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___values_2;

public:
	inline static int32_t get_offset_of_lookup_0() { return static_cast<int32_t>(offsetof(ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386, ___lookup_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_lookup_0() const { return ___lookup_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_lookup_0() { return &___lookup_0; }
	inline void set_lookup_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___lookup_0 = value;
		Il2CppCodeGenWriteBarrier((&___lookup_0), value);
	}

	inline static int32_t get_offset_of_readOnly_1() { return static_cast<int32_t>(offsetof(ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386, ___readOnly_1)); }
	inline bool get_readOnly_1() const { return ___readOnly_1; }
	inline bool* get_address_of_readOnly_1() { return &___readOnly_1; }
	inline void set_readOnly_1(bool value)
	{
		___readOnly_1 = value;
	}

	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386, ___values_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_values_2() const { return ___values_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier((&___values_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERCOLLECTION_TF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef VALIDATIONHANDLER_T2E35D69E882387E5F75A7D962A4BE4F31B79E909_H
#define VALIDATIONHANDLER_T2E35D69E882387E5F75A7D962A4BE4F31B79E909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidationHandler
struct  ValidationHandler_t2E35D69E882387E5F75A7D962A4BE4F31B79E909  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONHANDLER_T2E35D69E882387E5F75A7D962A4BE4F31B79E909_H
#ifndef XMLSCHEMAOBJECTENUMERATOR_TFD0F223924987EA798B376639B07E158F1F009E9_H
#define XMLSCHEMAOBJECTENUMERATOR_TFD0F223924987EA798B376639B07E158F1F009E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectEnumerator
struct  XmlSchemaObjectEnumerator_tFD0F223924987EA798B376639B07E158F1F009E9  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Xml.Schema.XmlSchemaObjectEnumerator::ienum
	RuntimeObject* ___ienum_0;

public:
	inline static int32_t get_offset_of_ienum_0() { return static_cast<int32_t>(offsetof(XmlSchemaObjectEnumerator_tFD0F223924987EA798B376639B07E158F1F009E9, ___ienum_0)); }
	inline RuntimeObject* get_ienum_0() const { return ___ienum_0; }
	inline RuntimeObject** get_address_of_ienum_0() { return &___ienum_0; }
	inline void set_ienum_0(RuntimeObject* value)
	{
		___ienum_0 = value;
		Il2CppCodeGenWriteBarrier((&___ienum_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECTENUMERATOR_TFD0F223924987EA798B376639B07E158F1F009E9_H
#ifndef XMLSCHEMAOBJECTTABLE_TA53C8BFD98C0E322B437AA48D3C89E01F6540D16_H
#define XMLSCHEMAOBJECTTABLE_TA53C8BFD98C0E322B437AA48D3C89E01F6540D16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable
struct  XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16  : public RuntimeObject
{
public:
	// System.Collections.Specialized.HybridDictionary System.Xml.Schema.XmlSchemaObjectTable::table
	HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747 * ___table_0;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16, ___table_0)); }
	inline HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747 * get_table_0() const { return ___table_0; }
	inline HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(HybridDictionary_t5AD529BFF21493C38235716C9AD62F1F7623C747 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECTTABLE_TA53C8BFD98C0E322B437AA48D3C89E01F6540D16_H
#ifndef XMLSCHEMAOBJECTTABLEENUMERATOR_T98D4DF6B98106D35E056E3A74981D1B96D577171_H
#define XMLSCHEMAOBJECTTABLEENUMERATOR_T98D4DF6B98106D35E056E3A74981D1B96D577171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectTable_XmlSchemaObjectTableEnumerator
struct  XmlSchemaObjectTableEnumerator_t98D4DF6B98106D35E056E3A74981D1B96D577171  : public RuntimeObject
{
public:
	// System.Collections.IDictionaryEnumerator System.Xml.Schema.XmlSchemaObjectTable_XmlSchemaObjectTableEnumerator::xenum
	RuntimeObject* ___xenum_0;
	// System.Collections.IEnumerable System.Xml.Schema.XmlSchemaObjectTable_XmlSchemaObjectTableEnumerator::tmp
	RuntimeObject* ___tmp_1;

public:
	inline static int32_t get_offset_of_xenum_0() { return static_cast<int32_t>(offsetof(XmlSchemaObjectTableEnumerator_t98D4DF6B98106D35E056E3A74981D1B96D577171, ___xenum_0)); }
	inline RuntimeObject* get_xenum_0() const { return ___xenum_0; }
	inline RuntimeObject** get_address_of_xenum_0() { return &___xenum_0; }
	inline void set_xenum_0(RuntimeObject* value)
	{
		___xenum_0 = value;
		Il2CppCodeGenWriteBarrier((&___xenum_0), value);
	}

	inline static int32_t get_offset_of_tmp_1() { return static_cast<int32_t>(offsetof(XmlSchemaObjectTableEnumerator_t98D4DF6B98106D35E056E3A74981D1B96D577171, ___tmp_1)); }
	inline RuntimeObject* get_tmp_1() const { return ___tmp_1; }
	inline RuntimeObject** get_address_of_tmp_1() { return &___tmp_1; }
	inline void set_tmp_1(RuntimeObject* value)
	{
		___tmp_1 = value;
		Il2CppCodeGenWriteBarrier((&___tmp_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECTTABLEENUMERATOR_T98D4DF6B98106D35E056E3A74981D1B96D577171_H
#ifndef CODEIDENTIFIER_T271B589AAAA73A8C8F21B5C83FB25A77D1E12D27_H
#define CODEIDENTIFIER_T271B589AAAA73A8C8F21B5C83FB25A77D1E12D27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.CodeIdentifier
struct  CodeIdentifier_t271B589AAAA73A8C8F21B5C83FB25A77D1E12D27  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEIDENTIFIER_T271B589AAAA73A8C8F21B5C83FB25A77D1E12D27_H
#ifndef TYPETRANSLATOR_T09CD0ECBC589939001BCD6109B5DDC7E499B78F8_H
#define TYPETRANSLATOR_T09CD0ECBC589939001BCD6109B5DDC7E499B78F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.TypeTranslator
struct  TypeTranslator_t09CD0ECBC589939001BCD6109B5DDC7E499B78F8  : public RuntimeObject
{
public:

public:
};

struct TypeTranslator_t09CD0ECBC589939001BCD6109B5DDC7E499B78F8_StaticFields
{
public:
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::nameCache
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___nameCache_0;
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::primitiveTypes
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___primitiveTypes_1;
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::primitiveArrayTypes
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___primitiveArrayTypes_2;
	// System.Collections.Hashtable System.Xml.Serialization.TypeTranslator::nullableTypes
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___nullableTypes_3;

public:
	inline static int32_t get_offset_of_nameCache_0() { return static_cast<int32_t>(offsetof(TypeTranslator_t09CD0ECBC589939001BCD6109B5DDC7E499B78F8_StaticFields, ___nameCache_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_nameCache_0() const { return ___nameCache_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_nameCache_0() { return &___nameCache_0; }
	inline void set_nameCache_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___nameCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameCache_0), value);
	}

	inline static int32_t get_offset_of_primitiveTypes_1() { return static_cast<int32_t>(offsetof(TypeTranslator_t09CD0ECBC589939001BCD6109B5DDC7E499B78F8_StaticFields, ___primitiveTypes_1)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_primitiveTypes_1() const { return ___primitiveTypes_1; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_primitiveTypes_1() { return &___primitiveTypes_1; }
	inline void set_primitiveTypes_1(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___primitiveTypes_1 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveTypes_1), value);
	}

	inline static int32_t get_offset_of_primitiveArrayTypes_2() { return static_cast<int32_t>(offsetof(TypeTranslator_t09CD0ECBC589939001BCD6109B5DDC7E499B78F8_StaticFields, ___primitiveArrayTypes_2)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_primitiveArrayTypes_2() const { return ___primitiveArrayTypes_2; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_primitiveArrayTypes_2() { return &___primitiveArrayTypes_2; }
	inline void set_primitiveArrayTypes_2(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___primitiveArrayTypes_2 = value;
		Il2CppCodeGenWriteBarrier((&___primitiveArrayTypes_2), value);
	}

	inline static int32_t get_offset_of_nullableTypes_3() { return static_cast<int32_t>(offsetof(TypeTranslator_t09CD0ECBC589939001BCD6109B5DDC7E499B78F8_StaticFields, ___nullableTypes_3)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_nullableTypes_3() const { return ___nullableTypes_3; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_nullableTypes_3() { return &___nullableTypes_3; }
	inline void set_nullableTypes_3(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___nullableTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___nullableTypes_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPETRANSLATOR_T09CD0ECBC589939001BCD6109B5DDC7E499B78F8_H
#ifndef XMLSERIALIZERNAMESPACES_TC38F759F99861DDF16FBA041554B4BBA2C1B12E3_H
#define XMLSERIALIZERNAMESPACES_TC38F759F99861DDF16FBA041554B4BBA2C1B12E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlSerializerNamespaces
struct  XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3  : public RuntimeObject
{
public:
	// System.Collections.Specialized.ListDictionary System.Xml.Serialization.XmlSerializerNamespaces::namespaces
	ListDictionary_tD949561FF8FD1EBEB444214919203DD41A76ECCA * ___namespaces_0;

public:
	inline static int32_t get_offset_of_namespaces_0() { return static_cast<int32_t>(offsetof(XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3, ___namespaces_0)); }
	inline ListDictionary_tD949561FF8FD1EBEB444214919203DD41A76ECCA * get_namespaces_0() const { return ___namespaces_0; }
	inline ListDictionary_tD949561FF8FD1EBEB444214919203DD41A76ECCA ** get_address_of_namespaces_0() { return &___namespaces_0; }
	inline void set_namespaces_0(ListDictionary_tD949561FF8FD1EBEB444214919203DD41A76ECCA * value)
	{
		___namespaces_0 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZERNAMESPACES_TC38F759F99861DDF16FBA041554B4BBA2C1B12E3_H
#ifndef XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#define XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293, ___binary_0)); }
	inline XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293, ___settings_1)); }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#ifndef U24ARRAYTYPEU2412_T240C25AFE6D004BFFB06A982F7E42749780F8A23_H
#define U24ARRAYTYPEU2412_T240C25AFE6D004BFFB06A982F7E42749780F8A23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU2412
struct  U24ArrayTypeU2412_t240C25AFE6D004BFFB06A982F7E42749780F8A23 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2412_t240C25AFE6D004BFFB06A982F7E42749780F8A23__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2412_T240C25AFE6D004BFFB06A982F7E42749780F8A23_H
#ifndef U24ARRAYTYPEU241280_T07BD843025ECC2676C90DA85D9C7D398AA8A6DBA_H
#define U24ARRAYTYPEU241280_T07BD843025ECC2676C90DA85D9C7D398AA8A6DBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU241280
struct  U24ArrayTypeU241280_t07BD843025ECC2676C90DA85D9C7D398AA8A6DBA 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU241280_t07BD843025ECC2676C90DA85D9C7D398AA8A6DBA__padding[1280];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU241280_T07BD843025ECC2676C90DA85D9C7D398AA8A6DBA_H
#ifndef U24ARRAYTYPEU24256_T6264193FFFEE81B3F24E58F2F58D3CCFD2957611_H
#define U24ARRAYTYPEU24256_T6264193FFFEE81B3F24E58F2F58D3CCFD2957611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU24256
struct  U24ArrayTypeU24256_t6264193FFFEE81B3F24E58F2F58D3CCFD2957611 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24256_t6264193FFFEE81B3F24E58F2F58D3CCFD2957611__padding[256];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24256_T6264193FFFEE81B3F24E58F2F58D3CCFD2957611_H
#ifndef U24ARRAYTYPEU248_T46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8_H
#define U24ARRAYTYPEU248_T46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU248
struct  U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8__padding[8];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU248_T46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8_H
#ifndef NAMEVALUECOLLECTION_TB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF_H
#define NAMEVALUECOLLECTION_TB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameValueCollection
struct  NameValueCollection_tB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF  : public NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070
{
public:
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAllKeys
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___cachedAllKeys_10;
	// System.String[] System.Collections.Specialized.NameValueCollection::cachedAll
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___cachedAll_11;

public:
	inline static int32_t get_offset_of_cachedAllKeys_10() { return static_cast<int32_t>(offsetof(NameValueCollection_tB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF, ___cachedAllKeys_10)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_cachedAllKeys_10() const { return ___cachedAllKeys_10; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_cachedAllKeys_10() { return &___cachedAllKeys_10; }
	inline void set_cachedAllKeys_10(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___cachedAllKeys_10 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAllKeys_10), value);
	}

	inline static int32_t get_offset_of_cachedAll_11() { return static_cast<int32_t>(offsetof(NameValueCollection_tB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF, ___cachedAll_11)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_cachedAll_11() const { return ___cachedAll_11; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_cachedAll_11() { return &___cachedAll_11; }
	inline void set_cachedAll_11(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___cachedAll_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAll_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUECOLLECTION_TB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF_H
#ifndef CONFIGURATIONELEMENTCOLLECTION_T5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E_H
#define CONFIGURATIONELEMENTCOLLECTION_T5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElementCollection
struct  ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E  : public ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63
{
public:
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___list_13;
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::removed
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___removed_14;
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::inherited
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___inherited_15;
	// System.Boolean System.Configuration.ConfigurationElementCollection::emitClear
	bool ___emitClear_16;
	// System.Boolean System.Configuration.ConfigurationElementCollection::modified
	bool ___modified_17;
	// System.Collections.IComparer System.Configuration.ConfigurationElementCollection::comparer
	RuntimeObject* ___comparer_18;
	// System.Int32 System.Configuration.ConfigurationElementCollection::inheritedLimitIndex
	int32_t ___inheritedLimitIndex_19;
	// System.String System.Configuration.ConfigurationElementCollection::addElementName
	String_t* ___addElementName_20;
	// System.String System.Configuration.ConfigurationElementCollection::clearElementName
	String_t* ___clearElementName_21;
	// System.String System.Configuration.ConfigurationElementCollection::removeElementName
	String_t* ___removeElementName_22;

public:
	inline static int32_t get_offset_of_list_13() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___list_13)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_list_13() const { return ___list_13; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_list_13() { return &___list_13; }
	inline void set_list_13(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___list_13 = value;
		Il2CppCodeGenWriteBarrier((&___list_13), value);
	}

	inline static int32_t get_offset_of_removed_14() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___removed_14)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_removed_14() const { return ___removed_14; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_removed_14() { return &___removed_14; }
	inline void set_removed_14(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___removed_14 = value;
		Il2CppCodeGenWriteBarrier((&___removed_14), value);
	}

	inline static int32_t get_offset_of_inherited_15() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___inherited_15)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_inherited_15() const { return ___inherited_15; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_inherited_15() { return &___inherited_15; }
	inline void set_inherited_15(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___inherited_15 = value;
		Il2CppCodeGenWriteBarrier((&___inherited_15), value);
	}

	inline static int32_t get_offset_of_emitClear_16() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___emitClear_16)); }
	inline bool get_emitClear_16() const { return ___emitClear_16; }
	inline bool* get_address_of_emitClear_16() { return &___emitClear_16; }
	inline void set_emitClear_16(bool value)
	{
		___emitClear_16 = value;
	}

	inline static int32_t get_offset_of_modified_17() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___modified_17)); }
	inline bool get_modified_17() const { return ___modified_17; }
	inline bool* get_address_of_modified_17() { return &___modified_17; }
	inline void set_modified_17(bool value)
	{
		___modified_17 = value;
	}

	inline static int32_t get_offset_of_comparer_18() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___comparer_18)); }
	inline RuntimeObject* get_comparer_18() const { return ___comparer_18; }
	inline RuntimeObject** get_address_of_comparer_18() { return &___comparer_18; }
	inline void set_comparer_18(RuntimeObject* value)
	{
		___comparer_18 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_18), value);
	}

	inline static int32_t get_offset_of_inheritedLimitIndex_19() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___inheritedLimitIndex_19)); }
	inline int32_t get_inheritedLimitIndex_19() const { return ___inheritedLimitIndex_19; }
	inline int32_t* get_address_of_inheritedLimitIndex_19() { return &___inheritedLimitIndex_19; }
	inline void set_inheritedLimitIndex_19(int32_t value)
	{
		___inheritedLimitIndex_19 = value;
	}

	inline static int32_t get_offset_of_addElementName_20() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___addElementName_20)); }
	inline String_t* get_addElementName_20() const { return ___addElementName_20; }
	inline String_t** get_address_of_addElementName_20() { return &___addElementName_20; }
	inline void set_addElementName_20(String_t* value)
	{
		___addElementName_20 = value;
		Il2CppCodeGenWriteBarrier((&___addElementName_20), value);
	}

	inline static int32_t get_offset_of_clearElementName_21() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___clearElementName_21)); }
	inline String_t* get_clearElementName_21() const { return ___clearElementName_21; }
	inline String_t** get_address_of_clearElementName_21() { return &___clearElementName_21; }
	inline void set_clearElementName_21(String_t* value)
	{
		___clearElementName_21 = value;
		Il2CppCodeGenWriteBarrier((&___clearElementName_21), value);
	}

	inline static int32_t get_offset_of_removeElementName_22() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___removeElementName_22)); }
	inline String_t* get_removeElementName_22() const { return ___removeElementName_22; }
	inline String_t** get_address_of_removeElementName_22() { return &___removeElementName_22; }
	inline void set_removeElementName_22(String_t* value)
	{
		___removeElementName_22 = value;
		Il2CppCodeGenWriteBarrier((&___removeElementName_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENTCOLLECTION_T5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E_H
#ifndef CONFIGURATIONREMOVEELEMENT_T577415495CA1304AB82547789A48AE35FFE325CD_H
#define CONFIGURATIONREMOVEELEMENT_T577415495CA1304AB82547789A48AE35FFE325CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElementCollection_ConfigurationRemoveElement
struct  ConfigurationRemoveElement_t577415495CA1304AB82547789A48AE35FFE325CD  : public ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ConfigurationElementCollection_ConfigurationRemoveElement::properties
	ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * ___properties_13;
	// System.Configuration.ConfigurationElement System.Configuration.ConfigurationElementCollection_ConfigurationRemoveElement::_origElement
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 * ____origElement_14;
	// System.Configuration.ConfigurationElementCollection System.Configuration.ConfigurationElementCollection_ConfigurationRemoveElement::_origCollection
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E * ____origCollection_15;

public:
	inline static int32_t get_offset_of_properties_13() { return static_cast<int32_t>(offsetof(ConfigurationRemoveElement_t577415495CA1304AB82547789A48AE35FFE325CD, ___properties_13)); }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * get_properties_13() const { return ___properties_13; }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 ** get_address_of_properties_13() { return &___properties_13; }
	inline void set_properties_13(ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * value)
	{
		___properties_13 = value;
		Il2CppCodeGenWriteBarrier((&___properties_13), value);
	}

	inline static int32_t get_offset_of__origElement_14() { return static_cast<int32_t>(offsetof(ConfigurationRemoveElement_t577415495CA1304AB82547789A48AE35FFE325CD, ____origElement_14)); }
	inline ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 * get__origElement_14() const { return ____origElement_14; }
	inline ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 ** get_address_of__origElement_14() { return &____origElement_14; }
	inline void set__origElement_14(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 * value)
	{
		____origElement_14 = value;
		Il2CppCodeGenWriteBarrier((&____origElement_14), value);
	}

	inline static int32_t get_offset_of__origCollection_15() { return static_cast<int32_t>(offsetof(ConfigurationRemoveElement_t577415495CA1304AB82547789A48AE35FFE325CD, ____origCollection_15)); }
	inline ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E * get__origCollection_15() const { return ____origCollection_15; }
	inline ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E ** get_address_of__origCollection_15() { return &____origCollection_15; }
	inline void set__origCollection_15(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E * value)
	{
		____origCollection_15 = value;
		Il2CppCodeGenWriteBarrier((&____origCollection_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONREMOVEELEMENT_T577415495CA1304AB82547789A48AE35FFE325CD_H
#ifndef CONFIGURATIONLOCATIONCOLLECTION_T8FA9CDDF11231DCA26928D9575D87A679C45B65A_H
#define CONFIGURATIONLOCATIONCOLLECTION_T8FA9CDDF11231DCA26928D9575D87A679C45B65A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationLocationCollection
struct  ConfigurationLocationCollection_t8FA9CDDF11231DCA26928D9575D87A679C45B65A  : public ReadOnlyCollectionBase_t100D9B9D5FC37926098ECE105DAC4670F688BD2B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONLOCATIONCOLLECTION_T8FA9CDDF11231DCA26928D9575D87A679C45B65A_H
#ifndef DECIMAL_T46BBDD161320E361BC57E00CD6C1F87256CA27F3_H
#define DECIMAL_T46BBDD161320E361BC57E00CD6C1F87256CA27F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 
{
public:
	// System.UInt32 System.Decimal::flags
	uint32_t ___flags_5;
	// System.UInt32 System.Decimal::hi
	uint32_t ___hi_6;
	// System.UInt32 System.Decimal::lo
	uint32_t ___lo_7;
	// System.UInt32 System.Decimal::mid
	uint32_t ___mid_8;

public:
	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3, ___flags_5)); }
	inline uint32_t get_flags_5() const { return ___flags_5; }
	inline uint32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(uint32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_hi_6() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3, ___hi_6)); }
	inline uint32_t get_hi_6() const { return ___hi_6; }
	inline uint32_t* get_address_of_hi_6() { return &___hi_6; }
	inline void set_hi_6(uint32_t value)
	{
		___hi_6 = value;
	}

	inline static int32_t get_offset_of_lo_7() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3, ___lo_7)); }
	inline uint32_t get_lo_7() const { return ___lo_7; }
	inline uint32_t* get_address_of_lo_7() { return &___lo_7; }
	inline void set_lo_7(uint32_t value)
	{
		___lo_7 = value;
	}

	inline static int32_t get_offset_of_mid_8() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3, ___mid_8)); }
	inline uint32_t get_mid_8() const { return ___mid_8; }
	inline uint32_t* get_address_of_mid_8() { return &___mid_8; }
	inline void set_mid_8(uint32_t value)
	{
		___mid_8 = value;
	}
};

struct Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3_StaticFields
{
public:
	// System.Decimal System.Decimal::MinValue
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___MinValue_0;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___MaxValue_1;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___MinusOne_2;
	// System.Decimal System.Decimal::One
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___One_3;
	// System.Decimal System.Decimal::MaxValueDiv10
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___MaxValueDiv10_4;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3_StaticFields, ___MinValue_0)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_MinValue_0() const { return ___MinValue_0; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3_StaticFields, ___MaxValue_1)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_MaxValue_1() const { return ___MaxValue_1; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinusOne_2() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3_StaticFields, ___MinusOne_2)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_MinusOne_2() const { return ___MinusOne_2; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_MinusOne_2() { return &___MinusOne_2; }
	inline void set_MinusOne_2(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___MinusOne_2 = value;
	}

	inline static int32_t get_offset_of_One_3() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3_StaticFields, ___One_3)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_One_3() const { return ___One_3; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_One_3() { return &___One_3; }
	inline void set_One_3(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___One_3 = value;
	}

	inline static int32_t get_offset_of_MaxValueDiv10_4() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3_StaticFields, ___MaxValueDiv10_4)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_MaxValueDiv10_4() const { return ___MaxValueDiv10_4; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_MaxValueDiv10_4() { return &___MaxValueDiv10_4; }
	inline void set_MaxValueDiv10_4(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___MaxValueDiv10_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T46BBDD161320E361BC57E00CD6C1F87256CA27F3_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#define SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifndef VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#define VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_tDB81A15FA2AB53E2401A76B745D215397B29F783 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifndef XMLSCHEMAOBJECTCOLLECTION_T69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D_H
#define XMLSCHEMAOBJECTCOLLECTION_T69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObjectCollection
struct  XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D  : public CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECTCOLLECTION_T69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D_H
#ifndef XMLSCHEMAREADER_TB568152CC3CDCC0D93125CEC8BA1B48526548C9F_H
#define XMLSCHEMAREADER_TB568152CC3CDCC0D93125CEC8BA1B48526548C9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaReader
struct  XmlSchemaReader_tB568152CC3CDCC0D93125CEC8BA1B48526548C9F  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// System.Xml.XmlReader System.Xml.Schema.XmlSchemaReader::reader
	XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * ___reader_2;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaReader::handler
	ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * ___handler_3;
	// System.Boolean System.Xml.Schema.XmlSchemaReader::hasLineInfo
	bool ___hasLineInfo_4;

public:
	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(XmlSchemaReader_tB568152CC3CDCC0D93125CEC8BA1B48526548C9F, ___reader_2)); }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * get_reader_2() const { return ___reader_2; }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier((&___reader_2), value);
	}

	inline static int32_t get_offset_of_handler_3() { return static_cast<int32_t>(offsetof(XmlSchemaReader_tB568152CC3CDCC0D93125CEC8BA1B48526548C9F, ___handler_3)); }
	inline ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * get_handler_3() const { return ___handler_3; }
	inline ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B ** get_address_of_handler_3() { return &___handler_3; }
	inline void set_handler_3(ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * value)
	{
		___handler_3 = value;
		Il2CppCodeGenWriteBarrier((&___handler_3), value);
	}

	inline static int32_t get_offset_of_hasLineInfo_4() { return static_cast<int32_t>(offsetof(XmlSchemaReader_tB568152CC3CDCC0D93125CEC8BA1B48526548C9F, ___hasLineInfo_4)); }
	inline bool get_hasLineInfo_4() const { return ___hasLineInfo_4; }
	inline bool* get_address_of_hasLineInfo_4() { return &___hasLineInfo_4; }
	inline void set_hasLineInfo_4(bool value)
	{
		___hasLineInfo_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAREADER_TB568152CC3CDCC0D93125CEC8BA1B48526548C9F_H
#ifndef XMLANYATTRIBUTEATTRIBUTE_T2F5EDE6F0E37829D42BDFB90007730B3903893B3_H
#define XMLANYATTRIBUTEATTRIBUTE_T2F5EDE6F0E37829D42BDFB90007730B3903893B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAnyAttributeAttribute
struct  XmlAnyAttributeAttribute_t2F5EDE6F0E37829D42BDFB90007730B3903893B3  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYATTRIBUTEATTRIBUTE_T2F5EDE6F0E37829D42BDFB90007730B3903893B3_H
#ifndef XMLANYELEMENTATTRIBUTE_TFA4AE6E799FD8B3E5BF87E96219A5EBB1E5BA99E_H
#define XMLANYELEMENTATTRIBUTE_TFA4AE6E799FD8B3E5BF87E96219A5EBB1E5BA99E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAnyElementAttribute
struct  XmlAnyElementAttribute_tFA4AE6E799FD8B3E5BF87E96219A5EBB1E5BA99E  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:
	// System.Int32 System.Xml.Serialization.XmlAnyElementAttribute::order
	int32_t ___order_0;

public:
	inline static int32_t get_offset_of_order_0() { return static_cast<int32_t>(offsetof(XmlAnyElementAttribute_tFA4AE6E799FD8B3E5BF87E96219A5EBB1E5BA99E, ___order_0)); }
	inline int32_t get_order_0() const { return ___order_0; }
	inline int32_t* get_address_of_order_0() { return &___order_0; }
	inline void set_order_0(int32_t value)
	{
		___order_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLANYELEMENTATTRIBUTE_TFA4AE6E799FD8B3E5BF87E96219A5EBB1E5BA99E_H
#ifndef XMLATTRIBUTEATTRIBUTE_TE67EC29BD9EDACC25B99CDE09E045A5BC09096FE_H
#define XMLATTRIBUTEATTRIBUTE_TE67EC29BD9EDACC25B99CDE09E045A5BC09096FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlAttributeAttribute
struct  XmlAttributeAttribute_tE67EC29BD9EDACC25B99CDE09E045A5BC09096FE  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:
	// System.String System.Xml.Serialization.XmlAttributeAttribute::attributeName
	String_t* ___attributeName_0;
	// System.String System.Xml.Serialization.XmlAttributeAttribute::dataType
	String_t* ___dataType_1;

public:
	inline static int32_t get_offset_of_attributeName_0() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_tE67EC29BD9EDACC25B99CDE09E045A5BC09096FE, ___attributeName_0)); }
	inline String_t* get_attributeName_0() const { return ___attributeName_0; }
	inline String_t** get_address_of_attributeName_0() { return &___attributeName_0; }
	inline void set_attributeName_0(String_t* value)
	{
		___attributeName_0 = value;
		Il2CppCodeGenWriteBarrier((&___attributeName_0), value);
	}

	inline static int32_t get_offset_of_dataType_1() { return static_cast<int32_t>(offsetof(XmlAttributeAttribute_tE67EC29BD9EDACC25B99CDE09E045A5BC09096FE, ___dataType_1)); }
	inline String_t* get_dataType_1() const { return ___dataType_1; }
	inline String_t** get_address_of_dataType_1() { return &___dataType_1; }
	inline void set_dataType_1(String_t* value)
	{
		___dataType_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTEATTRIBUTE_TE67EC29BD9EDACC25B99CDE09E045A5BC09096FE_H
#ifndef XMLELEMENTATTRIBUTE_T92C0D894D9E76C38B59E1D573EC9F6AF80494B5E_H
#define XMLELEMENTATTRIBUTE_T92C0D894D9E76C38B59E1D573EC9F6AF80494B5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlElementAttribute
struct  XmlElementAttribute_t92C0D894D9E76C38B59E1D573EC9F6AF80494B5E  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:
	// System.String System.Xml.Serialization.XmlElementAttribute::elementName
	String_t* ___elementName_0;
	// System.Type System.Xml.Serialization.XmlElementAttribute::type
	Type_t * ___type_1;
	// System.Int32 System.Xml.Serialization.XmlElementAttribute::order
	int32_t ___order_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t92C0D894D9E76C38B59E1D573EC9F6AF80494B5E, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t92C0D894D9E76C38B59E1D573EC9F6AF80494B5E, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((&___type_1), value);
	}

	inline static int32_t get_offset_of_order_2() { return static_cast<int32_t>(offsetof(XmlElementAttribute_t92C0D894D9E76C38B59E1D573EC9F6AF80494B5E, ___order_2)); }
	inline int32_t get_order_2() const { return ___order_2; }
	inline int32_t* get_address_of_order_2() { return &___order_2; }
	inline void set_order_2(int32_t value)
	{
		___order_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTATTRIBUTE_T92C0D894D9E76C38B59E1D573EC9F6AF80494B5E_H
#ifndef XMLENUMATTRIBUTE_T13159B8D8CA3F8FA0319C49762A6FBDA37F4E763_H
#define XMLENUMATTRIBUTE_T13159B8D8CA3F8FA0319C49762A6FBDA37F4E763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlEnumAttribute
struct  XmlEnumAttribute_t13159B8D8CA3F8FA0319C49762A6FBDA37F4E763  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:
	// System.String System.Xml.Serialization.XmlEnumAttribute::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(XmlEnumAttribute_t13159B8D8CA3F8FA0319C49762A6FBDA37F4E763, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENUMATTRIBUTE_T13159B8D8CA3F8FA0319C49762A6FBDA37F4E763_H
#ifndef XMLIGNOREATTRIBUTE_TEA0415D4AC0E0C702B55CAB899E8339F1AF60BE1_H
#define XMLIGNOREATTRIBUTE_TEA0415D4AC0E0C702B55CAB899E8339F1AF60BE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlIgnoreAttribute
struct  XmlIgnoreAttribute_tEA0415D4AC0E0C702B55CAB899E8339F1AF60BE1  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIGNOREATTRIBUTE_TEA0415D4AC0E0C702B55CAB899E8339F1AF60BE1_H
#ifndef XMLNAMESPACEDECLARATIONSATTRIBUTE_TFDF5AA2717A634FDB6D2E82CA7E52785726931D0_H
#define XMLNAMESPACEDECLARATIONSATTRIBUTE_TFDF5AA2717A634FDB6D2E82CA7E52785726931D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlNamespaceDeclarationsAttribute
struct  XmlNamespaceDeclarationsAttribute_tFDF5AA2717A634FDB6D2E82CA7E52785726931D0  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEDECLARATIONSATTRIBUTE_TFDF5AA2717A634FDB6D2E82CA7E52785726931D0_H
#ifndef XMLROOTATTRIBUTE_T32A9E7F45477EB665B77943F34F4471F0169457C_H
#define XMLROOTATTRIBUTE_T32A9E7F45477EB665B77943F34F4471F0169457C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlRootAttribute
struct  XmlRootAttribute_t32A9E7F45477EB665B77943F34F4471F0169457C  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:
	// System.String System.Xml.Serialization.XmlRootAttribute::elementName
	String_t* ___elementName_0;
	// System.Boolean System.Xml.Serialization.XmlRootAttribute::isNullable
	bool ___isNullable_1;
	// System.String System.Xml.Serialization.XmlRootAttribute::ns
	String_t* ___ns_2;

public:
	inline static int32_t get_offset_of_elementName_0() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t32A9E7F45477EB665B77943F34F4471F0169457C, ___elementName_0)); }
	inline String_t* get_elementName_0() const { return ___elementName_0; }
	inline String_t** get_address_of_elementName_0() { return &___elementName_0; }
	inline void set_elementName_0(String_t* value)
	{
		___elementName_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_0), value);
	}

	inline static int32_t get_offset_of_isNullable_1() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t32A9E7F45477EB665B77943F34F4471F0169457C, ___isNullable_1)); }
	inline bool get_isNullable_1() const { return ___isNullable_1; }
	inline bool* get_address_of_isNullable_1() { return &___isNullable_1; }
	inline void set_isNullable_1(bool value)
	{
		___isNullable_1 = value;
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlRootAttribute_t32A9E7F45477EB665B77943F34F4471F0169457C, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((&___ns_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLROOTATTRIBUTE_T32A9E7F45477EB665B77943F34F4471F0169457C_H
#ifndef XMLTEXTATTRIBUTE_T4AB1AF51B935E1358C3461C709E14D3F0DF0099F_H
#define XMLTEXTATTRIBUTE_T4AB1AF51B935E1358C3461C709E14D3F0DF0099F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.XmlTextAttribute
struct  XmlTextAttribute_t4AB1AF51B935E1358C3461C709E14D3F0DF0099F  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTATTRIBUTE_T4AB1AF51B935E1358C3461C709E14D3F0DF0099F_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields
{
public:
	// <PrivateImplementationDetails>_U24ArrayTypeU248 <PrivateImplementationDetails>::U24U24fieldU2D36
	U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8  ___U24U24fieldU2D36_0;
	// <PrivateImplementationDetails>_U24ArrayTypeU24256 <PrivateImplementationDetails>::U24U24fieldU2D37
	U24ArrayTypeU24256_t6264193FFFEE81B3F24E58F2F58D3CCFD2957611  ___U24U24fieldU2D37_1;
	// <PrivateImplementationDetails>_U24ArrayTypeU24256 <PrivateImplementationDetails>::U24U24fieldU2D38
	U24ArrayTypeU24256_t6264193FFFEE81B3F24E58F2F58D3CCFD2957611  ___U24U24fieldU2D38_2;
	// <PrivateImplementationDetails>_U24ArrayTypeU241280 <PrivateImplementationDetails>::U24U24fieldU2D39
	U24ArrayTypeU241280_t07BD843025ECC2676C90DA85D9C7D398AA8A6DBA  ___U24U24fieldU2D39_3;
	// <PrivateImplementationDetails>_U24ArrayTypeU2412 <PrivateImplementationDetails>::U24U24fieldU2D40
	U24ArrayTypeU2412_t240C25AFE6D004BFFB06A982F7E42749780F8A23  ___U24U24fieldU2D40_4;
	// <PrivateImplementationDetails>_U24ArrayTypeU2412 <PrivateImplementationDetails>::U24U24fieldU2D41
	U24ArrayTypeU2412_t240C25AFE6D004BFFB06A982F7E42749780F8A23  ___U24U24fieldU2D41_5;
	// <PrivateImplementationDetails>_U24ArrayTypeU248 <PrivateImplementationDetails>::U24U24fieldU2D43
	U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8  ___U24U24fieldU2D43_6;
	// <PrivateImplementationDetails>_U24ArrayTypeU248 <PrivateImplementationDetails>::U24U24fieldU2D44
	U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8  ___U24U24fieldU2D44_7;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D36_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields, ___U24U24fieldU2D36_0)); }
	inline U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8  get_U24U24fieldU2D36_0() const { return ___U24U24fieldU2D36_0; }
	inline U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8 * get_address_of_U24U24fieldU2D36_0() { return &___U24U24fieldU2D36_0; }
	inline void set_U24U24fieldU2D36_0(U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8  value)
	{
		___U24U24fieldU2D36_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D37_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields, ___U24U24fieldU2D37_1)); }
	inline U24ArrayTypeU24256_t6264193FFFEE81B3F24E58F2F58D3CCFD2957611  get_U24U24fieldU2D37_1() const { return ___U24U24fieldU2D37_1; }
	inline U24ArrayTypeU24256_t6264193FFFEE81B3F24E58F2F58D3CCFD2957611 * get_address_of_U24U24fieldU2D37_1() { return &___U24U24fieldU2D37_1; }
	inline void set_U24U24fieldU2D37_1(U24ArrayTypeU24256_t6264193FFFEE81B3F24E58F2F58D3CCFD2957611  value)
	{
		___U24U24fieldU2D37_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D38_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields, ___U24U24fieldU2D38_2)); }
	inline U24ArrayTypeU24256_t6264193FFFEE81B3F24E58F2F58D3CCFD2957611  get_U24U24fieldU2D38_2() const { return ___U24U24fieldU2D38_2; }
	inline U24ArrayTypeU24256_t6264193FFFEE81B3F24E58F2F58D3CCFD2957611 * get_address_of_U24U24fieldU2D38_2() { return &___U24U24fieldU2D38_2; }
	inline void set_U24U24fieldU2D38_2(U24ArrayTypeU24256_t6264193FFFEE81B3F24E58F2F58D3CCFD2957611  value)
	{
		___U24U24fieldU2D38_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D39_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields, ___U24U24fieldU2D39_3)); }
	inline U24ArrayTypeU241280_t07BD843025ECC2676C90DA85D9C7D398AA8A6DBA  get_U24U24fieldU2D39_3() const { return ___U24U24fieldU2D39_3; }
	inline U24ArrayTypeU241280_t07BD843025ECC2676C90DA85D9C7D398AA8A6DBA * get_address_of_U24U24fieldU2D39_3() { return &___U24U24fieldU2D39_3; }
	inline void set_U24U24fieldU2D39_3(U24ArrayTypeU241280_t07BD843025ECC2676C90DA85D9C7D398AA8A6DBA  value)
	{
		___U24U24fieldU2D39_3 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D40_4() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields, ___U24U24fieldU2D40_4)); }
	inline U24ArrayTypeU2412_t240C25AFE6D004BFFB06A982F7E42749780F8A23  get_U24U24fieldU2D40_4() const { return ___U24U24fieldU2D40_4; }
	inline U24ArrayTypeU2412_t240C25AFE6D004BFFB06A982F7E42749780F8A23 * get_address_of_U24U24fieldU2D40_4() { return &___U24U24fieldU2D40_4; }
	inline void set_U24U24fieldU2D40_4(U24ArrayTypeU2412_t240C25AFE6D004BFFB06A982F7E42749780F8A23  value)
	{
		___U24U24fieldU2D40_4 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D41_5() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields, ___U24U24fieldU2D41_5)); }
	inline U24ArrayTypeU2412_t240C25AFE6D004BFFB06A982F7E42749780F8A23  get_U24U24fieldU2D41_5() const { return ___U24U24fieldU2D41_5; }
	inline U24ArrayTypeU2412_t240C25AFE6D004BFFB06A982F7E42749780F8A23 * get_address_of_U24U24fieldU2D41_5() { return &___U24U24fieldU2D41_5; }
	inline void set_U24U24fieldU2D41_5(U24ArrayTypeU2412_t240C25AFE6D004BFFB06A982F7E42749780F8A23  value)
	{
		___U24U24fieldU2D41_5 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D43_6() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields, ___U24U24fieldU2D43_6)); }
	inline U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8  get_U24U24fieldU2D43_6() const { return ___U24U24fieldU2D43_6; }
	inline U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8 * get_address_of_U24U24fieldU2D43_6() { return &___U24U24fieldU2D43_6; }
	inline void set_U24U24fieldU2D43_6(U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8  value)
	{
		___U24U24fieldU2D43_6 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D44_7() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields, ___U24U24fieldU2D44_7)); }
	inline U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8  get_U24U24fieldU2D44_7() const { return ___U24U24fieldU2D44_7; }
	inline U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8 * get_address_of_U24U24fieldU2D44_7() { return &___U24U24fieldU2D44_7; }
	inline void set_U24U24fieldU2D44_7(U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8  value)
	{
		___U24U24fieldU2D44_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_H
#ifndef CONFIGNAMEVALUECOLLECTION_T6BC2AF313208E6493F818D04E62F2C4ECE42A5CA_H
#define CONFIGNAMEVALUECOLLECTION_T6BC2AF313208E6493F818D04E62F2C4ECE42A5CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigNameValueCollection
struct  ConfigNameValueCollection_t6BC2AF313208E6493F818D04E62F2C4ECE42A5CA  : public NameValueCollection_tB87E7DD2A8341E8E48EEC4D39322C1CE72E103EF
{
public:
	// System.Boolean System.Configuration.ConfigNameValueCollection::modified
	bool ___modified_12;

public:
	inline static int32_t get_offset_of_modified_12() { return static_cast<int32_t>(offsetof(ConfigNameValueCollection_t6BC2AF313208E6493F818D04E62F2C4ECE42A5CA, ___modified_12)); }
	inline bool get_modified_12() const { return ___modified_12; }
	inline bool* get_address_of_modified_12() { return &___modified_12; }
	inline void set_modified_12(bool value)
	{
		___modified_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGNAMEVALUECOLLECTION_T6BC2AF313208E6493F818D04E62F2C4ECE42A5CA_H
#ifndef CONFIGURATIONALLOWDEFINITION_T18B6E25650B50173DD5DFB01BCF332724756329D_H
#define CONFIGURATIONALLOWDEFINITION_T18B6E25650B50173DD5DFB01BCF332724756329D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationAllowDefinition
struct  ConfigurationAllowDefinition_t18B6E25650B50173DD5DFB01BCF332724756329D 
{
public:
	// System.Int32 System.Configuration.ConfigurationAllowDefinition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationAllowDefinition_t18B6E25650B50173DD5DFB01BCF332724756329D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONALLOWDEFINITION_T18B6E25650B50173DD5DFB01BCF332724756329D_H
#ifndef CONFIGURATIONALLOWEXEDEFINITION_T66ED2E212F6E3FCB79A1A28752B982FA68AED8BC_H
#define CONFIGURATIONALLOWEXEDEFINITION_T66ED2E212F6E3FCB79A1A28752B982FA68AED8BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationAllowExeDefinition
struct  ConfigurationAllowExeDefinition_t66ED2E212F6E3FCB79A1A28752B982FA68AED8BC 
{
public:
	// System.Int32 System.Configuration.ConfigurationAllowExeDefinition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationAllowExeDefinition_t66ED2E212F6E3FCB79A1A28752B982FA68AED8BC, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONALLOWEXEDEFINITION_T66ED2E212F6E3FCB79A1A28752B982FA68AED8BC_H
#ifndef CONFIGURATIONELEMENTCOLLECTIONTYPE_T164CCC3030B723DE6437FA5DC7B9D21F49D5A8BE_H
#define CONFIGURATIONELEMENTCOLLECTIONTYPE_T164CCC3030B723DE6437FA5DC7B9D21F49D5A8BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElementCollectionType
struct  ConfigurationElementCollectionType_t164CCC3030B723DE6437FA5DC7B9D21F49D5A8BE 
{
public:
	// System.Int32 System.Configuration.ConfigurationElementCollectionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationElementCollectionType_t164CCC3030B723DE6437FA5DC7B9D21F49D5A8BE, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENTCOLLECTIONTYPE_T164CCC3030B723DE6437FA5DC7B9D21F49D5A8BE_H
#ifndef CONFIGURATIONEXCEPTION_TA484D28F513EFC1D9AA44129F86EB8EF6A359536_H
#define CONFIGURATIONEXCEPTION_TA484D28F513EFC1D9AA44129F86EB8EF6A359536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationException
struct  ConfigurationException_tA484D28F513EFC1D9AA44129F86EB8EF6A359536  : public SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301
{
public:
	// System.String System.Configuration.ConfigurationException::filename
	String_t* ___filename_11;
	// System.Int32 System.Configuration.ConfigurationException::line
	int32_t ___line_12;

public:
	inline static int32_t get_offset_of_filename_11() { return static_cast<int32_t>(offsetof(ConfigurationException_tA484D28F513EFC1D9AA44129F86EB8EF6A359536, ___filename_11)); }
	inline String_t* get_filename_11() const { return ___filename_11; }
	inline String_t** get_address_of_filename_11() { return &___filename_11; }
	inline void set_filename_11(String_t* value)
	{
		___filename_11 = value;
		Il2CppCodeGenWriteBarrier((&___filename_11), value);
	}

	inline static int32_t get_offset_of_line_12() { return static_cast<int32_t>(offsetof(ConfigurationException_tA484D28F513EFC1D9AA44129F86EB8EF6A359536, ___line_12)); }
	inline int32_t get_line_12() const { return ___line_12; }
	inline int32_t* get_address_of_line_12() { return &___line_12; }
	inline void set_line_12(int32_t value)
	{
		___line_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONEXCEPTION_TA484D28F513EFC1D9AA44129F86EB8EF6A359536_H
#ifndef CONFIGURATIONLOCKTYPE_TFAC681894C188BA192647A4170910E9D1F078534_H
#define CONFIGURATIONLOCKTYPE_TFAC681894C188BA192647A4170910E9D1F078534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationLockType
struct  ConfigurationLockType_tFAC681894C188BA192647A4170910E9D1F078534 
{
public:
	// System.Int32 System.Configuration.ConfigurationLockType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationLockType_tFAC681894C188BA192647A4170910E9D1F078534, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONLOCKTYPE_TFAC681894C188BA192647A4170910E9D1F078534_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_8)); }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * get_data_8() const { return ___data_8; }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T_H
#ifndef NUMBERSTYLES_TFAC1E79E7CB54B1C83E44967D68356FC558A6454_H
#define NUMBERSTYLES_TFAC1E79E7CB54B1C83E44967D68356FC558A6454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.NumberStyles
struct  NumberStyles_tFAC1E79E7CB54B1C83E44967D68356FC558A6454 
{
public:
	// System.Int32 System.Globalization.NumberStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NumberStyles_tFAC1E79E7CB54B1C83E44967D68356FC558A6454, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMBERSTYLES_TFAC1E79E7CB54B1C83E44967D68356FC558A6454_H
#ifndef XMLSCHEMADERIVATIONMETHOD_TCA0F660D1F9BAE9B164EC4393AA19EBA107F8C07_H
#define XMLSCHEMADERIVATIONMETHOD_TCA0F660D1F9BAE9B164EC4393AA19EBA107F8C07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDerivationMethod
struct  XmlSchemaDerivationMethod_tCA0F660D1F9BAE9B164EC4393AA19EBA107F8C07 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaDerivationMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaDerivationMethod_tCA0F660D1F9BAE9B164EC4393AA19EBA107F8C07, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADERIVATIONMETHOD_TCA0F660D1F9BAE9B164EC4393AA19EBA107F8C07_H
#ifndef XMLSCHEMAEXCEPTION_TCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D_H
#define XMLSCHEMAEXCEPTION_TCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaException
struct  XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D  : public SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaException::hasLineInfo
	bool ___hasLineInfo_11;
	// System.Int32 System.Xml.Schema.XmlSchemaException::lineNumber
	int32_t ___lineNumber_12;
	// System.Int32 System.Xml.Schema.XmlSchemaException::linePosition
	int32_t ___linePosition_13;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaException::sourceObj
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * ___sourceObj_14;
	// System.String System.Xml.Schema.XmlSchemaException::sourceUri
	String_t* ___sourceUri_15;

public:
	inline static int32_t get_offset_of_hasLineInfo_11() { return static_cast<int32_t>(offsetof(XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D, ___hasLineInfo_11)); }
	inline bool get_hasLineInfo_11() const { return ___hasLineInfo_11; }
	inline bool* get_address_of_hasLineInfo_11() { return &___hasLineInfo_11; }
	inline void set_hasLineInfo_11(bool value)
	{
		___hasLineInfo_11 = value;
	}

	inline static int32_t get_offset_of_lineNumber_12() { return static_cast<int32_t>(offsetof(XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D, ___lineNumber_12)); }
	inline int32_t get_lineNumber_12() const { return ___lineNumber_12; }
	inline int32_t* get_address_of_lineNumber_12() { return &___lineNumber_12; }
	inline void set_lineNumber_12(int32_t value)
	{
		___lineNumber_12 = value;
	}

	inline static int32_t get_offset_of_linePosition_13() { return static_cast<int32_t>(offsetof(XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D, ___linePosition_13)); }
	inline int32_t get_linePosition_13() const { return ___linePosition_13; }
	inline int32_t* get_address_of_linePosition_13() { return &___linePosition_13; }
	inline void set_linePosition_13(int32_t value)
	{
		___linePosition_13 = value;
	}

	inline static int32_t get_offset_of_sourceObj_14() { return static_cast<int32_t>(offsetof(XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D, ___sourceObj_14)); }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * get_sourceObj_14() const { return ___sourceObj_14; }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 ** get_address_of_sourceObj_14() { return &___sourceObj_14; }
	inline void set_sourceObj_14(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * value)
	{
		___sourceObj_14 = value;
		Il2CppCodeGenWriteBarrier((&___sourceObj_14), value);
	}

	inline static int32_t get_offset_of_sourceUri_15() { return static_cast<int32_t>(offsetof(XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D, ___sourceUri_15)); }
	inline String_t* get_sourceUri_15() const { return ___sourceUri_15; }
	inline String_t** get_address_of_sourceUri_15() { return &___sourceUri_15; }
	inline void set_sourceUri_15(String_t* value)
	{
		___sourceUri_15 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAEXCEPTION_TCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D_H
#ifndef FACET_TC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B_H
#define FACET_TC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet_Facet
struct  Facet_tC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaFacet_Facet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Facet_tC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACET_TC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B_H
#ifndef XMLSCHEMAOBJECT_TDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747_H
#define XMLSCHEMAOBJECT_TDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObject
struct  XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaObject::lineNumber
	int32_t ___lineNumber_0;
	// System.Int32 System.Xml.Schema.XmlSchemaObject::linePosition
	int32_t ___linePosition_1;
	// System.String System.Xml.Schema.XmlSchemaObject::sourceUri
	String_t* ___sourceUri_2;
	// System.Xml.Serialization.XmlSerializerNamespaces System.Xml.Schema.XmlSchemaObject::namespaces
	XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3 * ___namespaces_3;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaObject::unhandledAttributeList
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___unhandledAttributeList_4;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isCompiled
	bool ___isCompiled_5;
	// System.Int32 System.Xml.Schema.XmlSchemaObject::errorCount
	int32_t ___errorCount_6;
	// System.Guid System.Xml.Schema.XmlSchemaObject::CompilationId
	Guid_t  ___CompilationId_7;
	// System.Guid System.Xml.Schema.XmlSchemaObject::ValidationId
	Guid_t  ___ValidationId_8;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isRedefineChild
	bool ___isRedefineChild_9;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isRedefinedComponent
	bool ___isRedefinedComponent_10;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObject::redefinedObject
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * ___redefinedObject_11;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObject::parent
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * ___parent_12;

public:
	inline static int32_t get_offset_of_lineNumber_0() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___lineNumber_0)); }
	inline int32_t get_lineNumber_0() const { return ___lineNumber_0; }
	inline int32_t* get_address_of_lineNumber_0() { return &___lineNumber_0; }
	inline void set_lineNumber_0(int32_t value)
	{
		___lineNumber_0 = value;
	}

	inline static int32_t get_offset_of_linePosition_1() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___linePosition_1)); }
	inline int32_t get_linePosition_1() const { return ___linePosition_1; }
	inline int32_t* get_address_of_linePosition_1() { return &___linePosition_1; }
	inline void set_linePosition_1(int32_t value)
	{
		___linePosition_1 = value;
	}

	inline static int32_t get_offset_of_sourceUri_2() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___sourceUri_2)); }
	inline String_t* get_sourceUri_2() const { return ___sourceUri_2; }
	inline String_t** get_address_of_sourceUri_2() { return &___sourceUri_2; }
	inline void set_sourceUri_2(String_t* value)
	{
		___sourceUri_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_2), value);
	}

	inline static int32_t get_offset_of_namespaces_3() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___namespaces_3)); }
	inline XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3 * get_namespaces_3() const { return ___namespaces_3; }
	inline XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3 ** get_address_of_namespaces_3() { return &___namespaces_3; }
	inline void set_namespaces_3(XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3 * value)
	{
		___namespaces_3 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_3), value);
	}

	inline static int32_t get_offset_of_unhandledAttributeList_4() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___unhandledAttributeList_4)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_unhandledAttributeList_4() const { return ___unhandledAttributeList_4; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_unhandledAttributeList_4() { return &___unhandledAttributeList_4; }
	inline void set_unhandledAttributeList_4(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___unhandledAttributeList_4 = value;
		Il2CppCodeGenWriteBarrier((&___unhandledAttributeList_4), value);
	}

	inline static int32_t get_offset_of_isCompiled_5() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___isCompiled_5)); }
	inline bool get_isCompiled_5() const { return ___isCompiled_5; }
	inline bool* get_address_of_isCompiled_5() { return &___isCompiled_5; }
	inline void set_isCompiled_5(bool value)
	{
		___isCompiled_5 = value;
	}

	inline static int32_t get_offset_of_errorCount_6() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___errorCount_6)); }
	inline int32_t get_errorCount_6() const { return ___errorCount_6; }
	inline int32_t* get_address_of_errorCount_6() { return &___errorCount_6; }
	inline void set_errorCount_6(int32_t value)
	{
		___errorCount_6 = value;
	}

	inline static int32_t get_offset_of_CompilationId_7() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___CompilationId_7)); }
	inline Guid_t  get_CompilationId_7() const { return ___CompilationId_7; }
	inline Guid_t * get_address_of_CompilationId_7() { return &___CompilationId_7; }
	inline void set_CompilationId_7(Guid_t  value)
	{
		___CompilationId_7 = value;
	}

	inline static int32_t get_offset_of_ValidationId_8() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___ValidationId_8)); }
	inline Guid_t  get_ValidationId_8() const { return ___ValidationId_8; }
	inline Guid_t * get_address_of_ValidationId_8() { return &___ValidationId_8; }
	inline void set_ValidationId_8(Guid_t  value)
	{
		___ValidationId_8 = value;
	}

	inline static int32_t get_offset_of_isRedefineChild_9() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___isRedefineChild_9)); }
	inline bool get_isRedefineChild_9() const { return ___isRedefineChild_9; }
	inline bool* get_address_of_isRedefineChild_9() { return &___isRedefineChild_9; }
	inline void set_isRedefineChild_9(bool value)
	{
		___isRedefineChild_9 = value;
	}

	inline static int32_t get_offset_of_isRedefinedComponent_10() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___isRedefinedComponent_10)); }
	inline bool get_isRedefinedComponent_10() const { return ___isRedefinedComponent_10; }
	inline bool* get_address_of_isRedefinedComponent_10() { return &___isRedefinedComponent_10; }
	inline void set_isRedefinedComponent_10(bool value)
	{
		___isRedefinedComponent_10 = value;
	}

	inline static int32_t get_offset_of_redefinedObject_11() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___redefinedObject_11)); }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * get_redefinedObject_11() const { return ___redefinedObject_11; }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 ** get_address_of_redefinedObject_11() { return &___redefinedObject_11; }
	inline void set_redefinedObject_11(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * value)
	{
		___redefinedObject_11 = value;
		Il2CppCodeGenWriteBarrier((&___redefinedObject_11), value);
	}

	inline static int32_t get_offset_of_parent_12() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___parent_12)); }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * get_parent_12() const { return ___parent_12; }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 ** get_address_of_parent_12() { return &___parent_12; }
	inline void set_parent_12(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * value)
	{
		___parent_12 = value;
		Il2CppCodeGenWriteBarrier((&___parent_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECT_TDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747_H
#ifndef XMLSCHEMASET_T57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D_H
#define XMLSCHEMASET_T57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSet
struct  XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.XmlSchemaSet::nameTable
	XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * ___nameTable_0;
	// System.Xml.XmlResolver System.Xml.Schema.XmlSchemaSet::xmlResolver
	XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * ___xmlResolver_1;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaSet::schemas
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___schemas_2;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::attributes
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___attributes_3;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::elements
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___elements_4;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::types
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___types_5;
	// System.Collections.Hashtable System.Xml.Schema.XmlSchemaSet::idCollection
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___idCollection_6;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaSet::namedIdentities
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___namedIdentities_7;
	// System.Xml.Schema.XmlSchemaCompilationSettings System.Xml.Schema.XmlSchemaSet::settings
	XmlSchemaCompilationSettings_t84466353D7B09B95C9D10D856332058F60FD31CD * ___settings_8;
	// System.Boolean System.Xml.Schema.XmlSchemaSet::isCompiled
	bool ___isCompiled_9;
	// System.Guid System.Xml.Schema.XmlSchemaSet::CompilationId
	Guid_t  ___CompilationId_10;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaSet::ValidationEventHandler
	ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * ___ValidationEventHandler_11;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D, ___nameTable_0)); }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_0), value);
	}

	inline static int32_t get_offset_of_xmlResolver_1() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D, ___xmlResolver_1)); }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * get_xmlResolver_1() const { return ___xmlResolver_1; }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C ** get_address_of_xmlResolver_1() { return &___xmlResolver_1; }
	inline void set_xmlResolver_1(XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * value)
	{
		___xmlResolver_1 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_1), value);
	}

	inline static int32_t get_offset_of_schemas_2() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D, ___schemas_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_schemas_2() const { return ___schemas_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_schemas_2() { return &___schemas_2; }
	inline void set_schemas_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___schemas_2 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_2), value);
	}

	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D, ___attributes_3)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_attributes_3() const { return ___attributes_3; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___attributes_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_3), value);
	}

	inline static int32_t get_offset_of_elements_4() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D, ___elements_4)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_elements_4() const { return ___elements_4; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_elements_4() { return &___elements_4; }
	inline void set_elements_4(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___elements_4 = value;
		Il2CppCodeGenWriteBarrier((&___elements_4), value);
	}

	inline static int32_t get_offset_of_types_5() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D, ___types_5)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_types_5() const { return ___types_5; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_types_5() { return &___types_5; }
	inline void set_types_5(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___types_5 = value;
		Il2CppCodeGenWriteBarrier((&___types_5), value);
	}

	inline static int32_t get_offset_of_idCollection_6() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D, ___idCollection_6)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_idCollection_6() const { return ___idCollection_6; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_idCollection_6() { return &___idCollection_6; }
	inline void set_idCollection_6(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___idCollection_6 = value;
		Il2CppCodeGenWriteBarrier((&___idCollection_6), value);
	}

	inline static int32_t get_offset_of_namedIdentities_7() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D, ___namedIdentities_7)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_namedIdentities_7() const { return ___namedIdentities_7; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_namedIdentities_7() { return &___namedIdentities_7; }
	inline void set_namedIdentities_7(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___namedIdentities_7 = value;
		Il2CppCodeGenWriteBarrier((&___namedIdentities_7), value);
	}

	inline static int32_t get_offset_of_settings_8() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D, ___settings_8)); }
	inline XmlSchemaCompilationSettings_t84466353D7B09B95C9D10D856332058F60FD31CD * get_settings_8() const { return ___settings_8; }
	inline XmlSchemaCompilationSettings_t84466353D7B09B95C9D10D856332058F60FD31CD ** get_address_of_settings_8() { return &___settings_8; }
	inline void set_settings_8(XmlSchemaCompilationSettings_t84466353D7B09B95C9D10D856332058F60FD31CD * value)
	{
		___settings_8 = value;
		Il2CppCodeGenWriteBarrier((&___settings_8), value);
	}

	inline static int32_t get_offset_of_isCompiled_9() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D, ___isCompiled_9)); }
	inline bool get_isCompiled_9() const { return ___isCompiled_9; }
	inline bool* get_address_of_isCompiled_9() { return &___isCompiled_9; }
	inline void set_isCompiled_9(bool value)
	{
		___isCompiled_9 = value;
	}

	inline static int32_t get_offset_of_CompilationId_10() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D, ___CompilationId_10)); }
	inline Guid_t  get_CompilationId_10() const { return ___CompilationId_10; }
	inline Guid_t * get_address_of_CompilationId_10() { return &___CompilationId_10; }
	inline void set_CompilationId_10(Guid_t  value)
	{
		___CompilationId_10 = value;
	}

	inline static int32_t get_offset_of_ValidationEventHandler_11() { return static_cast<int32_t>(offsetof(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D, ___ValidationEventHandler_11)); }
	inline ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * get_ValidationEventHandler_11() const { return ___ValidationEventHandler_11; }
	inline ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B ** get_address_of_ValidationEventHandler_11() { return &___ValidationEventHandler_11; }
	inline void set_ValidationEventHandler_11(ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * value)
	{
		___ValidationEventHandler_11 = value;
		Il2CppCodeGenWriteBarrier((&___ValidationEventHandler_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASET_T57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D_H
#ifndef XMLSCHEMAUSE_T74E0EE4116E212E01A0E2CBE17B468F5F5A728D8_H
#define XMLSCHEMAUSE_T74E0EE4116E212E01A0E2CBE17B468F5F5A728D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaUse
struct  XmlSchemaUse_t74E0EE4116E212E01A0E2CBE17B468F5F5A728D8 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaUse::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaUse_t74E0EE4116E212E01A0E2CBE17B468F5F5A728D8, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAUSE_T74E0EE4116E212E01A0E2CBE17B468F5F5A728D8_H
#ifndef XMLSCHEMAVALIDATIONFLAGS_TECE1A6138368A7455AC47A24FC5FD4920CDB8ACA_H
#define XMLSCHEMAVALIDATIONFLAGS_TECE1A6138368A7455AC47A24FC5FD4920CDB8ACA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidationFlags
struct  XmlSchemaValidationFlags_tECE1A6138368A7455AC47A24FC5FD4920CDB8ACA 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidationFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaValidationFlags_tECE1A6138368A7455AC47A24FC5FD4920CDB8ACA, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATIONFLAGS_TECE1A6138368A7455AC47A24FC5FD4920CDB8ACA_H
#ifndef XMLSCHEMAVALIDITY_T602DD49FD94103C168EBCB02880FBE16C4B3D54B_H
#define XMLSCHEMAVALIDITY_T602DD49FD94103C168EBCB02880FBE16C4B3D54B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidity
struct  XmlSchemaValidity_t602DD49FD94103C168EBCB02880FBE16C4B3D54B 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidity::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaValidity_t602DD49FD94103C168EBCB02880FBE16C4B3D54B, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDITY_T602DD49FD94103C168EBCB02880FBE16C4B3D54B_H
#ifndef XMLSEVERITYTYPE_T128BFAEA33288999198A6D82DF83832E1A2DA060_H
#define XMLSEVERITYTYPE_T128BFAEA33288999198A6D82DF83832E1A2DA060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSeverityType
struct  XmlSeverityType_t128BFAEA33288999198A6D82DF83832E1A2DA060 
{
public:
	// System.Int32 System.Xml.Schema.XmlSeverityType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSeverityType_t128BFAEA33288999198A6D82DF83832E1A2DA060, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSEVERITYTYPE_T128BFAEA33288999198A6D82DF83832E1A2DA060_H
#ifndef XMLTYPECODE_T947DEC4BB4A883BC1EF9C796B8293EB10AB2B3A1_H
#define XMLTYPECODE_T947DEC4BB4A883BC1EF9C796B8293EB10AB2B3A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlTypeCode
struct  XmlTypeCode_t947DEC4BB4A883BC1EF9C796B8293EB10AB2B3A1 
{
public:
	// System.Int32 System.Xml.Schema.XmlTypeCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlTypeCode_t947DEC4BB4A883BC1EF9C796B8293EB10AB2B3A1, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPECODE_T947DEC4BB4A883BC1EF9C796B8293EB10AB2B3A1_H
#ifndef SCHEMATYPES_T06F1E2A36C11AA3C6F9458923A5C423FE8EF6060_H
#define SCHEMATYPES_T06F1E2A36C11AA3C6F9458923A5C423FE8EF6060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.SchemaTypes
struct  SchemaTypes_t06F1E2A36C11AA3C6F9458923A5C423FE8EF6060 
{
public:
	// System.Int32 System.Xml.Serialization.SchemaTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SchemaTypes_t06F1E2A36C11AA3C6F9458923A5C423FE8EF6060, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMATYPES_T06F1E2A36C11AA3C6F9458923A5C423FE8EF6060_H
#ifndef CONFIGURATIONCOLLECTIONATTRIBUTE_T306510D81BCFFB7163142679D521E72022976318_H
#define CONFIGURATIONCOLLECTIONATTRIBUTE_T306510D81BCFFB7163142679D521E72022976318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationCollectionAttribute
struct  ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:
	// System.String System.Configuration.ConfigurationCollectionAttribute::addItemName
	String_t* ___addItemName_0;
	// System.String System.Configuration.ConfigurationCollectionAttribute::clearItemsName
	String_t* ___clearItemsName_1;
	// System.String System.Configuration.ConfigurationCollectionAttribute::removeItemName
	String_t* ___removeItemName_2;
	// System.Configuration.ConfigurationElementCollectionType System.Configuration.ConfigurationCollectionAttribute::collectionType
	int32_t ___collectionType_3;
	// System.Type System.Configuration.ConfigurationCollectionAttribute::itemType
	Type_t * ___itemType_4;

public:
	inline static int32_t get_offset_of_addItemName_0() { return static_cast<int32_t>(offsetof(ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318, ___addItemName_0)); }
	inline String_t* get_addItemName_0() const { return ___addItemName_0; }
	inline String_t** get_address_of_addItemName_0() { return &___addItemName_0; }
	inline void set_addItemName_0(String_t* value)
	{
		___addItemName_0 = value;
		Il2CppCodeGenWriteBarrier((&___addItemName_0), value);
	}

	inline static int32_t get_offset_of_clearItemsName_1() { return static_cast<int32_t>(offsetof(ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318, ___clearItemsName_1)); }
	inline String_t* get_clearItemsName_1() const { return ___clearItemsName_1; }
	inline String_t** get_address_of_clearItemsName_1() { return &___clearItemsName_1; }
	inline void set_clearItemsName_1(String_t* value)
	{
		___clearItemsName_1 = value;
		Il2CppCodeGenWriteBarrier((&___clearItemsName_1), value);
	}

	inline static int32_t get_offset_of_removeItemName_2() { return static_cast<int32_t>(offsetof(ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318, ___removeItemName_2)); }
	inline String_t* get_removeItemName_2() const { return ___removeItemName_2; }
	inline String_t** get_address_of_removeItemName_2() { return &___removeItemName_2; }
	inline void set_removeItemName_2(String_t* value)
	{
		___removeItemName_2 = value;
		Il2CppCodeGenWriteBarrier((&___removeItemName_2), value);
	}

	inline static int32_t get_offset_of_collectionType_3() { return static_cast<int32_t>(offsetof(ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318, ___collectionType_3)); }
	inline int32_t get_collectionType_3() const { return ___collectionType_3; }
	inline int32_t* get_address_of_collectionType_3() { return &___collectionType_3; }
	inline void set_collectionType_3(int32_t value)
	{
		___collectionType_3 = value;
	}

	inline static int32_t get_offset_of_itemType_4() { return static_cast<int32_t>(offsetof(ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318, ___itemType_4)); }
	inline Type_t * get_itemType_4() const { return ___itemType_4; }
	inline Type_t ** get_address_of_itemType_4() { return &___itemType_4; }
	inline void set_itemType_4(Type_t * value)
	{
		___itemType_4 = value;
		Il2CppCodeGenWriteBarrier((&___itemType_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONCOLLECTIONATTRIBUTE_T306510D81BCFFB7163142679D521E72022976318_H
#ifndef CONFIGURATIONERRORSEXCEPTION_TD1E3755B5D234D30892B41886D8515DCA40199F8_H
#define CONFIGURATIONERRORSEXCEPTION_TD1E3755B5D234D30892B41886D8515DCA40199F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationErrorsException
struct  ConfigurationErrorsException_tD1E3755B5D234D30892B41886D8515DCA40199F8  : public ConfigurationException_tA484D28F513EFC1D9AA44129F86EB8EF6A359536
{
public:
	// System.String System.Configuration.ConfigurationErrorsException::filename
	String_t* ___filename_13;
	// System.Int32 System.Configuration.ConfigurationErrorsException::line
	int32_t ___line_14;

public:
	inline static int32_t get_offset_of_filename_13() { return static_cast<int32_t>(offsetof(ConfigurationErrorsException_tD1E3755B5D234D30892B41886D8515DCA40199F8, ___filename_13)); }
	inline String_t* get_filename_13() const { return ___filename_13; }
	inline String_t** get_address_of_filename_13() { return &___filename_13; }
	inline void set_filename_13(String_t* value)
	{
		___filename_13 = value;
		Il2CppCodeGenWriteBarrier((&___filename_13), value);
	}

	inline static int32_t get_offset_of_line_14() { return static_cast<int32_t>(offsetof(ConfigurationErrorsException_tD1E3755B5D234D30892B41886D8515DCA40199F8, ___line_14)); }
	inline int32_t get_line_14() const { return ___line_14; }
	inline int32_t* get_address_of_line_14() { return &___line_14; }
	inline void set_line_14(int32_t value)
	{
		___line_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONERRORSEXCEPTION_TD1E3755B5D234D30892B41886D8515DCA40199F8_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef XMLSCHEMAANNOTATED_T86F7843F771EEE4F6A372B3B46CC4497928C3741_H
#define XMLSCHEMAANNOTATED_T86F7843F771EEE4F6A372B3B46CC4497928C3741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnnotated
struct  XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741  : public XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747
{
public:
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaAnnotated::annotation
	XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED * ___annotation_13;
	// System.String System.Xml.Schema.XmlSchemaAnnotated::id
	String_t* ___id_14;
	// System.Xml.XmlAttribute[] System.Xml.Schema.XmlSchemaAnnotated::unhandledAttributes
	XmlAttributeU5BU5D_t48E79142153B9C691C51308E60A01F22035CBD01* ___unhandledAttributes_15;

public:
	inline static int32_t get_offset_of_annotation_13() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741, ___annotation_13)); }
	inline XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED * get_annotation_13() const { return ___annotation_13; }
	inline XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED ** get_address_of_annotation_13() { return &___annotation_13; }
	inline void set_annotation_13(XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED * value)
	{
		___annotation_13 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_13), value);
	}

	inline static int32_t get_offset_of_id_14() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741, ___id_14)); }
	inline String_t* get_id_14() const { return ___id_14; }
	inline String_t** get_address_of_id_14() { return &___id_14; }
	inline void set_id_14(String_t* value)
	{
		___id_14 = value;
		Il2CppCodeGenWriteBarrier((&___id_14), value);
	}

	inline static int32_t get_offset_of_unhandledAttributes_15() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741, ___unhandledAttributes_15)); }
	inline XmlAttributeU5BU5D_t48E79142153B9C691C51308E60A01F22035CBD01* get_unhandledAttributes_15() const { return ___unhandledAttributes_15; }
	inline XmlAttributeU5BU5D_t48E79142153B9C691C51308E60A01F22035CBD01** get_address_of_unhandledAttributes_15() { return &___unhandledAttributes_15; }
	inline void set_unhandledAttributes_15(XmlAttributeU5BU5D_t48E79142153B9C691C51308E60A01F22035CBD01* value)
	{
		___unhandledAttributes_15 = value;
		Il2CppCodeGenWriteBarrier((&___unhandledAttributes_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANNOTATED_T86F7843F771EEE4F6A372B3B46CC4497928C3741_H
#ifndef XMLSCHEMAEXTERNAL_TF75596B2A753B85E30D6F9675F05E107D9A287D9_H
#define XMLSCHEMAEXTERNAL_TF75596B2A753B85E30D6F9675F05E107D9A287D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaExternal
struct  XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9  : public XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747
{
public:
	// System.String System.Xml.Schema.XmlSchemaExternal::id
	String_t* ___id_13;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaExternal::schema
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * ___schema_14;
	// System.String System.Xml.Schema.XmlSchemaExternal::location
	String_t* ___location_15;

public:
	inline static int32_t get_offset_of_id_13() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9, ___id_13)); }
	inline String_t* get_id_13() const { return ___id_13; }
	inline String_t** get_address_of_id_13() { return &___id_13; }
	inline void set_id_13(String_t* value)
	{
		___id_13 = value;
		Il2CppCodeGenWriteBarrier((&___id_13), value);
	}

	inline static int32_t get_offset_of_schema_14() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9, ___schema_14)); }
	inline XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * get_schema_14() const { return ___schema_14; }
	inline XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 ** get_address_of_schema_14() { return &___schema_14; }
	inline void set_schema_14(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * value)
	{
		___schema_14 = value;
		Il2CppCodeGenWriteBarrier((&___schema_14), value);
	}

	inline static int32_t get_offset_of_location_15() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9, ___location_15)); }
	inline String_t* get_location_15() const { return ___location_15; }
	inline String_t** get_address_of_location_15() { return &___location_15; }
	inline void set_location_15(String_t* value)
	{
		___location_15 = value;
		Il2CppCodeGenWriteBarrier((&___location_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAEXTERNAL_TF75596B2A753B85E30D6F9675F05E107D9A287D9_H
#ifndef XMLSCHEMAINFO_T14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C_H
#define XMLSCHEMAINFO_T14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaInfo
struct  XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaInfo::isDefault
	bool ___isDefault_0;
	// System.Boolean System.Xml.Schema.XmlSchemaInfo::isNil
	bool ___isNil_1;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaInfo::memberType
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___memberType_2;
	// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.XmlSchemaInfo::attr
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77 * ___attr_3;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaInfo::elem
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * ___elem_4;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaInfo::type
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * ___type_5;
	// System.Xml.Schema.XmlSchemaValidity System.Xml.Schema.XmlSchemaInfo::validity
	int32_t ___validity_6;

public:
	inline static int32_t get_offset_of_isDefault_0() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C, ___isDefault_0)); }
	inline bool get_isDefault_0() const { return ___isDefault_0; }
	inline bool* get_address_of_isDefault_0() { return &___isDefault_0; }
	inline void set_isDefault_0(bool value)
	{
		___isDefault_0 = value;
	}

	inline static int32_t get_offset_of_isNil_1() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C, ___isNil_1)); }
	inline bool get_isNil_1() const { return ___isNil_1; }
	inline bool* get_address_of_isNil_1() { return &___isNil_1; }
	inline void set_isNil_1(bool value)
	{
		___isNil_1 = value;
	}

	inline static int32_t get_offset_of_memberType_2() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C, ___memberType_2)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_memberType_2() const { return ___memberType_2; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_memberType_2() { return &___memberType_2; }
	inline void set_memberType_2(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___memberType_2 = value;
		Il2CppCodeGenWriteBarrier((&___memberType_2), value);
	}

	inline static int32_t get_offset_of_attr_3() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C, ___attr_3)); }
	inline XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77 * get_attr_3() const { return ___attr_3; }
	inline XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77 ** get_address_of_attr_3() { return &___attr_3; }
	inline void set_attr_3(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77 * value)
	{
		___attr_3 = value;
		Il2CppCodeGenWriteBarrier((&___attr_3), value);
	}

	inline static int32_t get_offset_of_elem_4() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C, ___elem_4)); }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * get_elem_4() const { return ___elem_4; }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 ** get_address_of_elem_4() { return &___elem_4; }
	inline void set_elem_4(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * value)
	{
		___elem_4 = value;
		Il2CppCodeGenWriteBarrier((&___elem_4), value);
	}

	inline static int32_t get_offset_of_type_5() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C, ___type_5)); }
	inline XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * get_type_5() const { return ___type_5; }
	inline XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE ** get_address_of_type_5() { return &___type_5; }
	inline void set_type_5(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * value)
	{
		___type_5 = value;
		Il2CppCodeGenWriteBarrier((&___type_5), value);
	}

	inline static int32_t get_offset_of_validity_6() { return static_cast<int32_t>(offsetof(XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C, ___validity_6)); }
	inline int32_t get_validity_6() const { return ___validity_6; }
	inline int32_t* get_address_of_validity_6() { return &___validity_6; }
	inline void set_validity_6(int32_t value)
	{
		___validity_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAINFO_T14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C_H
#ifndef XMLSCHEMAUTIL_TBC58C398A2D837AC4A0992CCAD07B223ADBE4377_H
#define XMLSCHEMAUTIL_TBC58C398A2D837AC4A0992CCAD07B223ADBE4377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaUtil
struct  XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377  : public RuntimeObject
{
public:

public:
};

struct XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaUtil::FinalAllowed
	int32_t ___FinalAllowed_0;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaUtil::ElementBlockAllowed
	int32_t ___ElementBlockAllowed_1;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaUtil::ComplexTypeBlockAllowed
	int32_t ___ComplexTypeBlockAllowed_2;
	// System.Boolean System.Xml.Schema.XmlSchemaUtil::StrictMsCompliant
	bool ___StrictMsCompliant_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaUtil::<>f__switchU24map4B
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map4B_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaUtil::<>f__switchU24map4C
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map4C_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaUtil::<>f__switchU24map4D
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map4D_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaUtil::<>f__switchU24map4E
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map4E_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaUtil::<>f__switchU24map4F
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map4F_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaUtil::<>f__switchU24map50
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map50_9;

public:
	inline static int32_t get_offset_of_FinalAllowed_0() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields, ___FinalAllowed_0)); }
	inline int32_t get_FinalAllowed_0() const { return ___FinalAllowed_0; }
	inline int32_t* get_address_of_FinalAllowed_0() { return &___FinalAllowed_0; }
	inline void set_FinalAllowed_0(int32_t value)
	{
		___FinalAllowed_0 = value;
	}

	inline static int32_t get_offset_of_ElementBlockAllowed_1() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields, ___ElementBlockAllowed_1)); }
	inline int32_t get_ElementBlockAllowed_1() const { return ___ElementBlockAllowed_1; }
	inline int32_t* get_address_of_ElementBlockAllowed_1() { return &___ElementBlockAllowed_1; }
	inline void set_ElementBlockAllowed_1(int32_t value)
	{
		___ElementBlockAllowed_1 = value;
	}

	inline static int32_t get_offset_of_ComplexTypeBlockAllowed_2() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields, ___ComplexTypeBlockAllowed_2)); }
	inline int32_t get_ComplexTypeBlockAllowed_2() const { return ___ComplexTypeBlockAllowed_2; }
	inline int32_t* get_address_of_ComplexTypeBlockAllowed_2() { return &___ComplexTypeBlockAllowed_2; }
	inline void set_ComplexTypeBlockAllowed_2(int32_t value)
	{
		___ComplexTypeBlockAllowed_2 = value;
	}

	inline static int32_t get_offset_of_StrictMsCompliant_3() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields, ___StrictMsCompliant_3)); }
	inline bool get_StrictMsCompliant_3() const { return ___StrictMsCompliant_3; }
	inline bool* get_address_of_StrictMsCompliant_3() { return &___StrictMsCompliant_3; }
	inline void set_StrictMsCompliant_3(bool value)
	{
		___StrictMsCompliant_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4B_4() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields, ___U3CU3Ef__switchU24map4B_4)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map4B_4() const { return ___U3CU3Ef__switchU24map4B_4; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map4B_4() { return &___U3CU3Ef__switchU24map4B_4; }
	inline void set_U3CU3Ef__switchU24map4B_4(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map4B_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4B_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4C_5() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields, ___U3CU3Ef__switchU24map4C_5)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map4C_5() const { return ___U3CU3Ef__switchU24map4C_5; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map4C_5() { return &___U3CU3Ef__switchU24map4C_5; }
	inline void set_U3CU3Ef__switchU24map4C_5(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map4C_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4C_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4D_6() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields, ___U3CU3Ef__switchU24map4D_6)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map4D_6() const { return ___U3CU3Ef__switchU24map4D_6; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map4D_6() { return &___U3CU3Ef__switchU24map4D_6; }
	inline void set_U3CU3Ef__switchU24map4D_6(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map4D_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4D_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4E_7() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields, ___U3CU3Ef__switchU24map4E_7)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map4E_7() const { return ___U3CU3Ef__switchU24map4E_7; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map4E_7() { return &___U3CU3Ef__switchU24map4E_7; }
	inline void set_U3CU3Ef__switchU24map4E_7(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map4E_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4E_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4F_8() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields, ___U3CU3Ef__switchU24map4F_8)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map4F_8() const { return ___U3CU3Ef__switchU24map4F_8; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map4F_8() { return &___U3CU3Ef__switchU24map4F_8; }
	inline void set_U3CU3Ef__switchU24map4F_8(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map4F_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4F_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map50_9() { return static_cast<int32_t>(offsetof(XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields, ___U3CU3Ef__switchU24map50_9)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map50_9() const { return ___U3CU3Ef__switchU24map50_9; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map50_9() { return &___U3CU3Ef__switchU24map50_9; }
	inline void set_U3CU3Ef__switchU24map50_9(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map50_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map50_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAUTIL_TBC58C398A2D837AC4A0992CCAD07B223ADBE4377_H
#ifndef XMLSCHEMAVALIDATIONEXCEPTION_T7291A846B1FDCA169661064F2F1960FF1B5E29D8_H
#define XMLSCHEMAVALIDATIONEXCEPTION_T7291A846B1FDCA169661064F2F1960FF1B5E29D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidationException
struct  XmlSchemaValidationException_t7291A846B1FDCA169661064F2F1960FF1B5E29D8  : public XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATIONEXCEPTION_T7291A846B1FDCA169661064F2F1960FF1B5E29D8_H
#ifndef TYPEDATA_T1D7643AA75738523EEA8F9D5969BEB7C3D509BA0_H
#define TYPEDATA_T1D7643AA75738523EEA8F9D5969BEB7C3D509BA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Serialization.TypeData
struct  TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0  : public RuntimeObject
{
public:
	// System.Type System.Xml.Serialization.TypeData::type
	Type_t * ___type_0;
	// System.String System.Xml.Serialization.TypeData::elementName
	String_t* ___elementName_1;
	// System.Xml.Serialization.SchemaTypes System.Xml.Serialization.TypeData::sType
	int32_t ___sType_2;
	// System.Type System.Xml.Serialization.TypeData::listItemType
	Type_t * ___listItemType_3;
	// System.String System.Xml.Serialization.TypeData::typeName
	String_t* ___typeName_4;
	// System.String System.Xml.Serialization.TypeData::fullTypeName
	String_t* ___fullTypeName_5;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.TypeData::listItemTypeData
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0 * ___listItemTypeData_6;
	// System.Xml.Serialization.TypeData System.Xml.Serialization.TypeData::mappedType
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0 * ___mappedType_7;
	// System.Xml.Schema.XmlSchemaPatternFacet System.Xml.Serialization.TypeData::facet
	XmlSchemaPatternFacet_t1B3D1C8F8D7DF14F25DC513CC73DA23F87F0F027 * ___facet_8;
	// System.Boolean System.Xml.Serialization.TypeData::hasPublicConstructor
	bool ___hasPublicConstructor_9;
	// System.Boolean System.Xml.Serialization.TypeData::nullableOverride
	bool ___nullableOverride_10;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_elementName_1() { return static_cast<int32_t>(offsetof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0, ___elementName_1)); }
	inline String_t* get_elementName_1() const { return ___elementName_1; }
	inline String_t** get_address_of_elementName_1() { return &___elementName_1; }
	inline void set_elementName_1(String_t* value)
	{
		___elementName_1 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_1), value);
	}

	inline static int32_t get_offset_of_sType_2() { return static_cast<int32_t>(offsetof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0, ___sType_2)); }
	inline int32_t get_sType_2() const { return ___sType_2; }
	inline int32_t* get_address_of_sType_2() { return &___sType_2; }
	inline void set_sType_2(int32_t value)
	{
		___sType_2 = value;
	}

	inline static int32_t get_offset_of_listItemType_3() { return static_cast<int32_t>(offsetof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0, ___listItemType_3)); }
	inline Type_t * get_listItemType_3() const { return ___listItemType_3; }
	inline Type_t ** get_address_of_listItemType_3() { return &___listItemType_3; }
	inline void set_listItemType_3(Type_t * value)
	{
		___listItemType_3 = value;
		Il2CppCodeGenWriteBarrier((&___listItemType_3), value);
	}

	inline static int32_t get_offset_of_typeName_4() { return static_cast<int32_t>(offsetof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0, ___typeName_4)); }
	inline String_t* get_typeName_4() const { return ___typeName_4; }
	inline String_t** get_address_of_typeName_4() { return &___typeName_4; }
	inline void set_typeName_4(String_t* value)
	{
		___typeName_4 = value;
		Il2CppCodeGenWriteBarrier((&___typeName_4), value);
	}

	inline static int32_t get_offset_of_fullTypeName_5() { return static_cast<int32_t>(offsetof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0, ___fullTypeName_5)); }
	inline String_t* get_fullTypeName_5() const { return ___fullTypeName_5; }
	inline String_t** get_address_of_fullTypeName_5() { return &___fullTypeName_5; }
	inline void set_fullTypeName_5(String_t* value)
	{
		___fullTypeName_5 = value;
		Il2CppCodeGenWriteBarrier((&___fullTypeName_5), value);
	}

	inline static int32_t get_offset_of_listItemTypeData_6() { return static_cast<int32_t>(offsetof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0, ___listItemTypeData_6)); }
	inline TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0 * get_listItemTypeData_6() const { return ___listItemTypeData_6; }
	inline TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0 ** get_address_of_listItemTypeData_6() { return &___listItemTypeData_6; }
	inline void set_listItemTypeData_6(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0 * value)
	{
		___listItemTypeData_6 = value;
		Il2CppCodeGenWriteBarrier((&___listItemTypeData_6), value);
	}

	inline static int32_t get_offset_of_mappedType_7() { return static_cast<int32_t>(offsetof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0, ___mappedType_7)); }
	inline TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0 * get_mappedType_7() const { return ___mappedType_7; }
	inline TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0 ** get_address_of_mappedType_7() { return &___mappedType_7; }
	inline void set_mappedType_7(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0 * value)
	{
		___mappedType_7 = value;
		Il2CppCodeGenWriteBarrier((&___mappedType_7), value);
	}

	inline static int32_t get_offset_of_facet_8() { return static_cast<int32_t>(offsetof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0, ___facet_8)); }
	inline XmlSchemaPatternFacet_t1B3D1C8F8D7DF14F25DC513CC73DA23F87F0F027 * get_facet_8() const { return ___facet_8; }
	inline XmlSchemaPatternFacet_t1B3D1C8F8D7DF14F25DC513CC73DA23F87F0F027 ** get_address_of_facet_8() { return &___facet_8; }
	inline void set_facet_8(XmlSchemaPatternFacet_t1B3D1C8F8D7DF14F25DC513CC73DA23F87F0F027 * value)
	{
		___facet_8 = value;
		Il2CppCodeGenWriteBarrier((&___facet_8), value);
	}

	inline static int32_t get_offset_of_hasPublicConstructor_9() { return static_cast<int32_t>(offsetof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0, ___hasPublicConstructor_9)); }
	inline bool get_hasPublicConstructor_9() const { return ___hasPublicConstructor_9; }
	inline bool* get_address_of_hasPublicConstructor_9() { return &___hasPublicConstructor_9; }
	inline void set_hasPublicConstructor_9(bool value)
	{
		___hasPublicConstructor_9 = value;
	}

	inline static int32_t get_offset_of_nullableOverride_10() { return static_cast<int32_t>(offsetof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0, ___nullableOverride_10)); }
	inline bool get_nullableOverride_10() const { return ___nullableOverride_10; }
	inline bool* get_address_of_nullableOverride_10() { return &___nullableOverride_10; }
	inline void set_nullableOverride_10(bool value)
	{
		___nullableOverride_10 = value;
	}
};

struct TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0_StaticFields
{
public:
	// System.String[] System.Xml.Serialization.TypeData::keywords
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___keywords_11;

public:
	inline static int32_t get_offset_of_keywords_11() { return static_cast<int32_t>(offsetof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0_StaticFields, ___keywords_11)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_keywords_11() const { return ___keywords_11; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_keywords_11() { return &___keywords_11; }
	inline void set_keywords_11(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___keywords_11 = value;
		Il2CppCodeGenWriteBarrier((&___keywords_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEDATA_T1D7643AA75738523EEA8F9D5969BEB7C3D509BA0_H
#ifndef VALIDATIONEVENTHANDLER_T4E1A0D7874804802BF369D671E548EE69B97AD6B_H
#define VALIDATIONEVENTHANDLER_T4E1A0D7874804802BF369D671E548EE69B97AD6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidationEventHandler
struct  ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONEVENTHANDLER_T4E1A0D7874804802BF369D671E548EE69B97AD6B_H
#ifndef XMLSCHEMACONTENT_T809DC205F7F607C93C1B078FF8B77BDF4EAD31CA_H
#define XMLSCHEMACONTENT_T809DC205F7F607C93C1B078FF8B77BDF4EAD31CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContent
struct  XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Object System.Xml.Schema.XmlSchemaContent::actualBaseSchemaType
	RuntimeObject * ___actualBaseSchemaType_16;

public:
	inline static int32_t get_offset_of_actualBaseSchemaType_16() { return static_cast<int32_t>(offsetof(XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA, ___actualBaseSchemaType_16)); }
	inline RuntimeObject * get_actualBaseSchemaType_16() const { return ___actualBaseSchemaType_16; }
	inline RuntimeObject ** get_address_of_actualBaseSchemaType_16() { return &___actualBaseSchemaType_16; }
	inline void set_actualBaseSchemaType_16(RuntimeObject * value)
	{
		___actualBaseSchemaType_16 = value;
		Il2CppCodeGenWriteBarrier((&___actualBaseSchemaType_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENT_T809DC205F7F607C93C1B078FF8B77BDF4EAD31CA_H
#ifndef XMLSCHEMACONTENTMODEL_TB057D601009DC1517A2A538E001E592F465568D9_H
#define XMLSCHEMACONTENTMODEL_TB057D601009DC1517A2A538E001E592F465568D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentModel
struct  XmlSchemaContentModel_tB057D601009DC1517A2A538E001E592F465568D9  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTMODEL_TB057D601009DC1517A2A538E001E592F465568D9_H
#ifndef XMLSCHEMAFACET_T44B0D153A389F7A8C0769773496C6D041D8C8318_H
#define XMLSCHEMAFACET_T44B0D153A389F7A8C0769773496C6D041D8C8318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet
struct  XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaFacet::isFixed
	bool ___isFixed_17;
	// System.String System.Xml.Schema.XmlSchemaFacet::val
	String_t* ___val_18;

public:
	inline static int32_t get_offset_of_isFixed_17() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318, ___isFixed_17)); }
	inline bool get_isFixed_17() const { return ___isFixed_17; }
	inline bool* get_address_of_isFixed_17() { return &___isFixed_17; }
	inline void set_isFixed_17(bool value)
	{
		___isFixed_17 = value;
	}

	inline static int32_t get_offset_of_val_18() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318, ___val_18)); }
	inline String_t* get_val_18() const { return ___val_18; }
	inline String_t** get_address_of_val_18() { return &___val_18; }
	inline void set_val_18(String_t* value)
	{
		___val_18 = value;
		Il2CppCodeGenWriteBarrier((&___val_18), value);
	}
};

struct XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaFacet_Facet System.Xml.Schema.XmlSchemaFacet::AllFacets
	int32_t ___AllFacets_16;

public:
	inline static int32_t get_offset_of_AllFacets_16() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318_StaticFields, ___AllFacets_16)); }
	inline int32_t get_AllFacets_16() const { return ___AllFacets_16; }
	inline int32_t* get_address_of_AllFacets_16() { return &___AllFacets_16; }
	inline void set_AllFacets_16(int32_t value)
	{
		___AllFacets_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFACET_T44B0D153A389F7A8C0769773496C6D041D8C8318_H
#ifndef XMLSCHEMAGROUP_TE769C5EFDC105F19982EE5842FA5880B106F2447_H
#define XMLSCHEMAGROUP_TE769C5EFDC105F19982EE5842FA5880B106F2447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaGroup
struct  XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.String System.Xml.Schema.XmlSchemaGroup::name
	String_t* ___name_16;
	// System.Xml.Schema.XmlSchemaGroupBase System.Xml.Schema.XmlSchemaGroup::particle
	XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F * ___particle_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaGroup::qualifiedName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___qualifiedName_18;
	// System.Boolean System.Xml.Schema.XmlSchemaGroup::isCircularDefinition
	bool ___isCircularDefinition_19;

public:
	inline static int32_t get_offset_of_name_16() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447, ___name_16)); }
	inline String_t* get_name_16() const { return ___name_16; }
	inline String_t** get_address_of_name_16() { return &___name_16; }
	inline void set_name_16(String_t* value)
	{
		___name_16 = value;
		Il2CppCodeGenWriteBarrier((&___name_16), value);
	}

	inline static int32_t get_offset_of_particle_17() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447, ___particle_17)); }
	inline XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F * get_particle_17() const { return ___particle_17; }
	inline XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F ** get_address_of_particle_17() { return &___particle_17; }
	inline void set_particle_17(XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F * value)
	{
		___particle_17 = value;
		Il2CppCodeGenWriteBarrier((&___particle_17), value);
	}

	inline static int32_t get_offset_of_qualifiedName_18() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447, ___qualifiedName_18)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_qualifiedName_18() const { return ___qualifiedName_18; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_qualifiedName_18() { return &___qualifiedName_18; }
	inline void set_qualifiedName_18(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___qualifiedName_18 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedName_18), value);
	}

	inline static int32_t get_offset_of_isCircularDefinition_19() { return static_cast<int32_t>(offsetof(XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447, ___isCircularDefinition_19)); }
	inline bool get_isCircularDefinition_19() const { return ___isCircularDefinition_19; }
	inline bool* get_address_of_isCircularDefinition_19() { return &___isCircularDefinition_19; }
	inline void set_isCircularDefinition_19(bool value)
	{
		___isCircularDefinition_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAGROUP_TE769C5EFDC105F19982EE5842FA5880B106F2447_H
#ifndef XMLSCHEMAIDENTITYCONSTRAINT_TD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9_H
#define XMLSCHEMAIDENTITYCONSTRAINT_TD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaIdentityConstraint
struct  XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaIdentityConstraint::fields
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___fields_16;
	// System.String System.Xml.Schema.XmlSchemaIdentityConstraint::name
	String_t* ___name_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaIdentityConstraint::qName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___qName_18;
	// System.Xml.Schema.XmlSchemaXPath System.Xml.Schema.XmlSchemaIdentityConstraint::selector
	XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526 * ___selector_19;
	// Mono.Xml.Schema.XsdIdentitySelector System.Xml.Schema.XmlSchemaIdentityConstraint::compiledSelector
	XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8 * ___compiledSelector_20;

public:
	inline static int32_t get_offset_of_fields_16() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9, ___fields_16)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_fields_16() const { return ___fields_16; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_fields_16() { return &___fields_16; }
	inline void set_fields_16(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___fields_16 = value;
		Il2CppCodeGenWriteBarrier((&___fields_16), value);
	}

	inline static int32_t get_offset_of_name_17() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9, ___name_17)); }
	inline String_t* get_name_17() const { return ___name_17; }
	inline String_t** get_address_of_name_17() { return &___name_17; }
	inline void set_name_17(String_t* value)
	{
		___name_17 = value;
		Il2CppCodeGenWriteBarrier((&___name_17), value);
	}

	inline static int32_t get_offset_of_qName_18() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9, ___qName_18)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_qName_18() const { return ___qName_18; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_qName_18() { return &___qName_18; }
	inline void set_qName_18(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___qName_18 = value;
		Il2CppCodeGenWriteBarrier((&___qName_18), value);
	}

	inline static int32_t get_offset_of_selector_19() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9, ___selector_19)); }
	inline XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526 * get_selector_19() const { return ___selector_19; }
	inline XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526 ** get_address_of_selector_19() { return &___selector_19; }
	inline void set_selector_19(XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526 * value)
	{
		___selector_19 = value;
		Il2CppCodeGenWriteBarrier((&___selector_19), value);
	}

	inline static int32_t get_offset_of_compiledSelector_20() { return static_cast<int32_t>(offsetof(XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9, ___compiledSelector_20)); }
	inline XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8 * get_compiledSelector_20() const { return ___compiledSelector_20; }
	inline XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8 ** get_address_of_compiledSelector_20() { return &___compiledSelector_20; }
	inline void set_compiledSelector_20(XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8 * value)
	{
		___compiledSelector_20 = value;
		Il2CppCodeGenWriteBarrier((&___compiledSelector_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAIDENTITYCONSTRAINT_TD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9_H
#ifndef XMLSCHEMAIMPORT_T6171304957CE0DE0A7528CFE1AAC8A828165BED1_H
#define XMLSCHEMAIMPORT_T6171304957CE0DE0A7528CFE1AAC8A828165BED1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaImport
struct  XmlSchemaImport_t6171304957CE0DE0A7528CFE1AAC8A828165BED1  : public XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9
{
public:
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaImport::annotation
	XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED * ___annotation_16;
	// System.String System.Xml.Schema.XmlSchemaImport::nameSpace
	String_t* ___nameSpace_17;

public:
	inline static int32_t get_offset_of_annotation_16() { return static_cast<int32_t>(offsetof(XmlSchemaImport_t6171304957CE0DE0A7528CFE1AAC8A828165BED1, ___annotation_16)); }
	inline XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED * get_annotation_16() const { return ___annotation_16; }
	inline XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED ** get_address_of_annotation_16() { return &___annotation_16; }
	inline void set_annotation_16(XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED * value)
	{
		___annotation_16 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_16), value);
	}

	inline static int32_t get_offset_of_nameSpace_17() { return static_cast<int32_t>(offsetof(XmlSchemaImport_t6171304957CE0DE0A7528CFE1AAC8A828165BED1, ___nameSpace_17)); }
	inline String_t* get_nameSpace_17() const { return ___nameSpace_17; }
	inline String_t** get_address_of_nameSpace_17() { return &___nameSpace_17; }
	inline void set_nameSpace_17(String_t* value)
	{
		___nameSpace_17 = value;
		Il2CppCodeGenWriteBarrier((&___nameSpace_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAIMPORT_T6171304957CE0DE0A7528CFE1AAC8A828165BED1_H
#ifndef XMLSCHEMAINCLUDE_T4386F1693047C7C283432338D09B7F97DF1A844D_H
#define XMLSCHEMAINCLUDE_T4386F1693047C7C283432338D09B7F97DF1A844D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaInclude
struct  XmlSchemaInclude_t4386F1693047C7C283432338D09B7F97DF1A844D  : public XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9
{
public:
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaInclude::annotation
	XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED * ___annotation_16;

public:
	inline static int32_t get_offset_of_annotation_16() { return static_cast<int32_t>(offsetof(XmlSchemaInclude_t4386F1693047C7C283432338D09B7F97DF1A844D, ___annotation_16)); }
	inline XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED * get_annotation_16() const { return ___annotation_16; }
	inline XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED ** get_address_of_annotation_16() { return &___annotation_16; }
	inline void set_annotation_16(XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED * value)
	{
		___annotation_16 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAINCLUDE_T4386F1693047C7C283432338D09B7F97DF1A844D_H
#ifndef XMLSCHEMANOTATION_T0A5D8512C01294894FF143DDB678782ECBF0C82D_H
#define XMLSCHEMANOTATION_T0A5D8512C01294894FF143DDB678782ECBF0C82D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaNotation
struct  XmlSchemaNotation_t0A5D8512C01294894FF143DDB678782ECBF0C82D  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.String System.Xml.Schema.XmlSchemaNotation::name
	String_t* ___name_16;
	// System.String System.Xml.Schema.XmlSchemaNotation::pub
	String_t* ___pub_17;
	// System.String System.Xml.Schema.XmlSchemaNotation::system
	String_t* ___system_18;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaNotation::qualifiedName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___qualifiedName_19;

public:
	inline static int32_t get_offset_of_name_16() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_t0A5D8512C01294894FF143DDB678782ECBF0C82D, ___name_16)); }
	inline String_t* get_name_16() const { return ___name_16; }
	inline String_t** get_address_of_name_16() { return &___name_16; }
	inline void set_name_16(String_t* value)
	{
		___name_16 = value;
		Il2CppCodeGenWriteBarrier((&___name_16), value);
	}

	inline static int32_t get_offset_of_pub_17() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_t0A5D8512C01294894FF143DDB678782ECBF0C82D, ___pub_17)); }
	inline String_t* get_pub_17() const { return ___pub_17; }
	inline String_t** get_address_of_pub_17() { return &___pub_17; }
	inline void set_pub_17(String_t* value)
	{
		___pub_17 = value;
		Il2CppCodeGenWriteBarrier((&___pub_17), value);
	}

	inline static int32_t get_offset_of_system_18() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_t0A5D8512C01294894FF143DDB678782ECBF0C82D, ___system_18)); }
	inline String_t* get_system_18() const { return ___system_18; }
	inline String_t** get_address_of_system_18() { return &___system_18; }
	inline void set_system_18(String_t* value)
	{
		___system_18 = value;
		Il2CppCodeGenWriteBarrier((&___system_18), value);
	}

	inline static int32_t get_offset_of_qualifiedName_19() { return static_cast<int32_t>(offsetof(XmlSchemaNotation_t0A5D8512C01294894FF143DDB678782ECBF0C82D, ___qualifiedName_19)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_qualifiedName_19() const { return ___qualifiedName_19; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_qualifiedName_19() { return &___qualifiedName_19; }
	inline void set_qualifiedName_19(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___qualifiedName_19 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedName_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMANOTATION_T0A5D8512C01294894FF143DDB678782ECBF0C82D_H
#ifndef XMLSCHEMAPARTICLE_T7A09A84556A27FD42B9A6F7A933345158E7752BC_H
#define XMLSCHEMAPARTICLE_T7A09A84556A27FD42B9A6F7A933345158E7752BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaParticle
struct  XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::minOccurs
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___minOccurs_16;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::maxOccurs
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___maxOccurs_17;
	// System.String System.Xml.Schema.XmlSchemaParticle::minstr
	String_t* ___minstr_18;
	// System.String System.Xml.Schema.XmlSchemaParticle::maxstr
	String_t* ___maxstr_19;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::validatedMinOccurs
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___validatedMinOccurs_21;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::validatedMaxOccurs
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___validatedMaxOccurs_22;
	// System.Int32 System.Xml.Schema.XmlSchemaParticle::recursionDepth
	int32_t ___recursionDepth_23;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::minEffectiveTotalRange
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___minEffectiveTotalRange_24;
	// System.Boolean System.Xml.Schema.XmlSchemaParticle::parentIsGroupDefinition
	bool ___parentIsGroupDefinition_25;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaParticle::OptimizedParticle
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * ___OptimizedParticle_26;

public:
	inline static int32_t get_offset_of_minOccurs_16() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___minOccurs_16)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_minOccurs_16() const { return ___minOccurs_16; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_minOccurs_16() { return &___minOccurs_16; }
	inline void set_minOccurs_16(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___minOccurs_16 = value;
	}

	inline static int32_t get_offset_of_maxOccurs_17() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___maxOccurs_17)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_maxOccurs_17() const { return ___maxOccurs_17; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_maxOccurs_17() { return &___maxOccurs_17; }
	inline void set_maxOccurs_17(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___maxOccurs_17 = value;
	}

	inline static int32_t get_offset_of_minstr_18() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___minstr_18)); }
	inline String_t* get_minstr_18() const { return ___minstr_18; }
	inline String_t** get_address_of_minstr_18() { return &___minstr_18; }
	inline void set_minstr_18(String_t* value)
	{
		___minstr_18 = value;
		Il2CppCodeGenWriteBarrier((&___minstr_18), value);
	}

	inline static int32_t get_offset_of_maxstr_19() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___maxstr_19)); }
	inline String_t* get_maxstr_19() const { return ___maxstr_19; }
	inline String_t** get_address_of_maxstr_19() { return &___maxstr_19; }
	inline void set_maxstr_19(String_t* value)
	{
		___maxstr_19 = value;
		Il2CppCodeGenWriteBarrier((&___maxstr_19), value);
	}

	inline static int32_t get_offset_of_validatedMinOccurs_21() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___validatedMinOccurs_21)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_validatedMinOccurs_21() const { return ___validatedMinOccurs_21; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_validatedMinOccurs_21() { return &___validatedMinOccurs_21; }
	inline void set_validatedMinOccurs_21(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___validatedMinOccurs_21 = value;
	}

	inline static int32_t get_offset_of_validatedMaxOccurs_22() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___validatedMaxOccurs_22)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_validatedMaxOccurs_22() const { return ___validatedMaxOccurs_22; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_validatedMaxOccurs_22() { return &___validatedMaxOccurs_22; }
	inline void set_validatedMaxOccurs_22(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___validatedMaxOccurs_22 = value;
	}

	inline static int32_t get_offset_of_recursionDepth_23() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___recursionDepth_23)); }
	inline int32_t get_recursionDepth_23() const { return ___recursionDepth_23; }
	inline int32_t* get_address_of_recursionDepth_23() { return &___recursionDepth_23; }
	inline void set_recursionDepth_23(int32_t value)
	{
		___recursionDepth_23 = value;
	}

	inline static int32_t get_offset_of_minEffectiveTotalRange_24() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___minEffectiveTotalRange_24)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_minEffectiveTotalRange_24() const { return ___minEffectiveTotalRange_24; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_minEffectiveTotalRange_24() { return &___minEffectiveTotalRange_24; }
	inline void set_minEffectiveTotalRange_24(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___minEffectiveTotalRange_24 = value;
	}

	inline static int32_t get_offset_of_parentIsGroupDefinition_25() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___parentIsGroupDefinition_25)); }
	inline bool get_parentIsGroupDefinition_25() const { return ___parentIsGroupDefinition_25; }
	inline bool* get_address_of_parentIsGroupDefinition_25() { return &___parentIsGroupDefinition_25; }
	inline void set_parentIsGroupDefinition_25(bool value)
	{
		___parentIsGroupDefinition_25 = value;
	}

	inline static int32_t get_offset_of_OptimizedParticle_26() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___OptimizedParticle_26)); }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * get_OptimizedParticle_26() const { return ___OptimizedParticle_26; }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC ** get_address_of_OptimizedParticle_26() { return &___OptimizedParticle_26; }
	inline void set_OptimizedParticle_26(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * value)
	{
		___OptimizedParticle_26 = value;
		Il2CppCodeGenWriteBarrier((&___OptimizedParticle_26), value);
	}
};

struct XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaParticle::empty
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * ___empty_20;

public:
	inline static int32_t get_offset_of_empty_20() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC_StaticFields, ___empty_20)); }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * get_empty_20() const { return ___empty_20; }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC ** get_address_of_empty_20() { return &___empty_20; }
	inline void set_empty_20(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * value)
	{
		___empty_20 = value;
		Il2CppCodeGenWriteBarrier((&___empty_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAPARTICLE_T7A09A84556A27FD42B9A6F7A933345158E7752BC_H
#ifndef XMLSCHEMAREDEFINE_T7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC_H
#define XMLSCHEMAREDEFINE_T7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaRedefine
struct  XmlSchemaRedefine_t7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC  : public XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9
{
public:
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaRedefine::attributeGroups
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___attributeGroups_16;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaRedefine::groups
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___groups_17;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaRedefine::items
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___items_18;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaRedefine::schemaTypes
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___schemaTypes_19;

public:
	inline static int32_t get_offset_of_attributeGroups_16() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC, ___attributeGroups_16)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_attributeGroups_16() const { return ___attributeGroups_16; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_attributeGroups_16() { return &___attributeGroups_16; }
	inline void set_attributeGroups_16(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___attributeGroups_16 = value;
		Il2CppCodeGenWriteBarrier((&___attributeGroups_16), value);
	}

	inline static int32_t get_offset_of_groups_17() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC, ___groups_17)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_groups_17() const { return ___groups_17; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_groups_17() { return &___groups_17; }
	inline void set_groups_17(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___groups_17 = value;
		Il2CppCodeGenWriteBarrier((&___groups_17), value);
	}

	inline static int32_t get_offset_of_items_18() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC, ___items_18)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_items_18() const { return ___items_18; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_items_18() { return &___items_18; }
	inline void set_items_18(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___items_18 = value;
		Il2CppCodeGenWriteBarrier((&___items_18), value);
	}

	inline static int32_t get_offset_of_schemaTypes_19() { return static_cast<int32_t>(offsetof(XmlSchemaRedefine_t7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC, ___schemaTypes_19)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_schemaTypes_19() const { return ___schemaTypes_19; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_schemaTypes_19() { return &___schemaTypes_19; }
	inline void set_schemaTypes_19(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___schemaTypes_19 = value;
		Il2CppCodeGenWriteBarrier((&___schemaTypes_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAREDEFINE_T7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC_H
#ifndef XMLSCHEMASIMPLETYPECONTENT_T9BD29AE1058626894FD1F7EEAC14CD35BC3428EE_H
#define XMLSCHEMASIMPLETYPECONTENT_T9BD29AE1058626894FD1F7EEAC14CD35BC3428EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeContent
struct  XmlSchemaSimpleTypeContent_t9BD29AE1058626894FD1F7EEAC14CD35BC3428EE  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeContent::OwnerType
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___OwnerType_16;

public:
	inline static int32_t get_offset_of_OwnerType_16() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeContent_t9BD29AE1058626894FD1F7EEAC14CD35BC3428EE, ___OwnerType_16)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_OwnerType_16() const { return ___OwnerType_16; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_OwnerType_16() { return &___OwnerType_16; }
	inline void set_OwnerType_16(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___OwnerType_16 = value;
		Il2CppCodeGenWriteBarrier((&___OwnerType_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPECONTENT_T9BD29AE1058626894FD1F7EEAC14CD35BC3428EE_H
#ifndef XMLSCHEMATYPE_TDF0987DC710BC7BED17E5008774575FA2E6647AE_H
#define XMLSCHEMATYPE_TDF0987DC710BC7BED17E5008774575FA2E6647AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaType
struct  XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::final
	int32_t ___final_16;
	// System.Boolean System.Xml.Schema.XmlSchemaType::isMixed
	bool ___isMixed_17;
	// System.String System.Xml.Schema.XmlSchemaType::name
	String_t* ___name_18;
	// System.Boolean System.Xml.Schema.XmlSchemaType::recursed
	bool ___recursed_19;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaType::BaseSchemaTypeName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___BaseSchemaTypeName_20;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaType::BaseXmlSchemaTypeInternal
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * ___BaseXmlSchemaTypeInternal_21;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaType::DatatypeInternal
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19 * ___DatatypeInternal_22;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::resolvedDerivedBy
	int32_t ___resolvedDerivedBy_23;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::finalResolved
	int32_t ___finalResolved_24;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaType::QNameInternal
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___QNameInternal_25;

public:
	inline static int32_t get_offset_of_final_16() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___final_16)); }
	inline int32_t get_final_16() const { return ___final_16; }
	inline int32_t* get_address_of_final_16() { return &___final_16; }
	inline void set_final_16(int32_t value)
	{
		___final_16 = value;
	}

	inline static int32_t get_offset_of_isMixed_17() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___isMixed_17)); }
	inline bool get_isMixed_17() const { return ___isMixed_17; }
	inline bool* get_address_of_isMixed_17() { return &___isMixed_17; }
	inline void set_isMixed_17(bool value)
	{
		___isMixed_17 = value;
	}

	inline static int32_t get_offset_of_name_18() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___name_18)); }
	inline String_t* get_name_18() const { return ___name_18; }
	inline String_t** get_address_of_name_18() { return &___name_18; }
	inline void set_name_18(String_t* value)
	{
		___name_18 = value;
		Il2CppCodeGenWriteBarrier((&___name_18), value);
	}

	inline static int32_t get_offset_of_recursed_19() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___recursed_19)); }
	inline bool get_recursed_19() const { return ___recursed_19; }
	inline bool* get_address_of_recursed_19() { return &___recursed_19; }
	inline void set_recursed_19(bool value)
	{
		___recursed_19 = value;
	}

	inline static int32_t get_offset_of_BaseSchemaTypeName_20() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___BaseSchemaTypeName_20)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_BaseSchemaTypeName_20() const { return ___BaseSchemaTypeName_20; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_BaseSchemaTypeName_20() { return &___BaseSchemaTypeName_20; }
	inline void set_BaseSchemaTypeName_20(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___BaseSchemaTypeName_20 = value;
		Il2CppCodeGenWriteBarrier((&___BaseSchemaTypeName_20), value);
	}

	inline static int32_t get_offset_of_BaseXmlSchemaTypeInternal_21() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___BaseXmlSchemaTypeInternal_21)); }
	inline XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * get_BaseXmlSchemaTypeInternal_21() const { return ___BaseXmlSchemaTypeInternal_21; }
	inline XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE ** get_address_of_BaseXmlSchemaTypeInternal_21() { return &___BaseXmlSchemaTypeInternal_21; }
	inline void set_BaseXmlSchemaTypeInternal_21(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * value)
	{
		___BaseXmlSchemaTypeInternal_21 = value;
		Il2CppCodeGenWriteBarrier((&___BaseXmlSchemaTypeInternal_21), value);
	}

	inline static int32_t get_offset_of_DatatypeInternal_22() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___DatatypeInternal_22)); }
	inline XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19 * get_DatatypeInternal_22() const { return ___DatatypeInternal_22; }
	inline XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19 ** get_address_of_DatatypeInternal_22() { return &___DatatypeInternal_22; }
	inline void set_DatatypeInternal_22(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19 * value)
	{
		___DatatypeInternal_22 = value;
		Il2CppCodeGenWriteBarrier((&___DatatypeInternal_22), value);
	}

	inline static int32_t get_offset_of_resolvedDerivedBy_23() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___resolvedDerivedBy_23)); }
	inline int32_t get_resolvedDerivedBy_23() const { return ___resolvedDerivedBy_23; }
	inline int32_t* get_address_of_resolvedDerivedBy_23() { return &___resolvedDerivedBy_23; }
	inline void set_resolvedDerivedBy_23(int32_t value)
	{
		___resolvedDerivedBy_23 = value;
	}

	inline static int32_t get_offset_of_finalResolved_24() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___finalResolved_24)); }
	inline int32_t get_finalResolved_24() const { return ___finalResolved_24; }
	inline int32_t* get_address_of_finalResolved_24() { return &___finalResolved_24; }
	inline void set_finalResolved_24(int32_t value)
	{
		___finalResolved_24 = value;
	}

	inline static int32_t get_offset_of_QNameInternal_25() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___QNameInternal_25)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_QNameInternal_25() const { return ___QNameInternal_25; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_QNameInternal_25() { return &___QNameInternal_25; }
	inline void set_QNameInternal_25(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___QNameInternal_25 = value;
		Il2CppCodeGenWriteBarrier((&___QNameInternal_25), value);
	}
};

struct XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaType::<>f__switchU24map42
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map42_26;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaType::<>f__switchU24map43
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map43_27;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map42_26() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE_StaticFields, ___U3CU3Ef__switchU24map42_26)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map42_26() const { return ___U3CU3Ef__switchU24map42_26; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map42_26() { return &___U3CU3Ef__switchU24map42_26; }
	inline void set_U3CU3Ef__switchU24map42_26(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map42_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map42_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map43_27() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE_StaticFields, ___U3CU3Ef__switchU24map43_27)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map43_27() const { return ___U3CU3Ef__switchU24map43_27; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map43_27() { return &___U3CU3Ef__switchU24map43_27; }
	inline void set_U3CU3Ef__switchU24map43_27(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map43_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map43_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMATYPE_TDF0987DC710BC7BED17E5008774575FA2E6647AE_H
#ifndef XMLSCHEMAXPATH_T6CD59C385D5CEEC66D37A483111F039665C0A526_H
#define XMLSCHEMAXPATH_T6CD59C385D5CEEC66D37A483111F039665C0A526_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaXPath
struct  XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.String System.Xml.Schema.XmlSchemaXPath::xpath
	String_t* ___xpath_16;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XmlSchemaXPath::nsmgr
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * ___nsmgr_17;
	// System.Boolean System.Xml.Schema.XmlSchemaXPath::isSelector
	bool ___isSelector_18;
	// Mono.Xml.Schema.XsdIdentityPath[] System.Xml.Schema.XmlSchemaXPath::compiledExpression
	XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* ___compiledExpression_19;
	// Mono.Xml.Schema.XsdIdentityPath System.Xml.Schema.XmlSchemaXPath::currentPath
	XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE * ___currentPath_20;

public:
	inline static int32_t get_offset_of_xpath_16() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526, ___xpath_16)); }
	inline String_t* get_xpath_16() const { return ___xpath_16; }
	inline String_t** get_address_of_xpath_16() { return &___xpath_16; }
	inline void set_xpath_16(String_t* value)
	{
		___xpath_16 = value;
		Il2CppCodeGenWriteBarrier((&___xpath_16), value);
	}

	inline static int32_t get_offset_of_nsmgr_17() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526, ___nsmgr_17)); }
	inline XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * get_nsmgr_17() const { return ___nsmgr_17; }
	inline XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 ** get_address_of_nsmgr_17() { return &___nsmgr_17; }
	inline void set_nsmgr_17(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * value)
	{
		___nsmgr_17 = value;
		Il2CppCodeGenWriteBarrier((&___nsmgr_17), value);
	}

	inline static int32_t get_offset_of_isSelector_18() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526, ___isSelector_18)); }
	inline bool get_isSelector_18() const { return ___isSelector_18; }
	inline bool* get_address_of_isSelector_18() { return &___isSelector_18; }
	inline void set_isSelector_18(bool value)
	{
		___isSelector_18 = value;
	}

	inline static int32_t get_offset_of_compiledExpression_19() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526, ___compiledExpression_19)); }
	inline XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* get_compiledExpression_19() const { return ___compiledExpression_19; }
	inline XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F** get_address_of_compiledExpression_19() { return &___compiledExpression_19; }
	inline void set_compiledExpression_19(XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* value)
	{
		___compiledExpression_19 = value;
		Il2CppCodeGenWriteBarrier((&___compiledExpression_19), value);
	}

	inline static int32_t get_offset_of_currentPath_20() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526, ___currentPath_20)); }
	inline XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE * get_currentPath_20() const { return ___currentPath_20; }
	inline XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE ** get_address_of_currentPath_20() { return &___currentPath_20; }
	inline void set_currentPath_20(XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE * value)
	{
		___currentPath_20 = value;
		Il2CppCodeGenWriteBarrier((&___currentPath_20), value);
	}
};

struct XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaXPath::<>f__switchU24map4A
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map4A_21;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4A_21() { return static_cast<int32_t>(offsetof(XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526_StaticFields, ___U3CU3Ef__switchU24map4A_21)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map4A_21() const { return ___U3CU3Ef__switchU24map4A_21; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map4A_21() { return &___U3CU3Ef__switchU24map4A_21; }
	inline void set_U3CU3Ef__switchU24map4A_21(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map4A_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4A_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAXPATH_T6CD59C385D5CEEC66D37A483111F039665C0A526_H
#ifndef XMLNODECHANGEDEVENTHANDLER_TDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC_H
#define XMLNODECHANGEDEVENTHANDLER_TDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedEventHandler
struct  XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDEVENTHANDLER_TDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC_H
#ifndef XMLSCHEMAGROUPBASE_TA64F460BB1ABB1F903265373A8216E35A19A9D9F_H
#define XMLSCHEMAGROUPBASE_TA64F460BB1ABB1F903265373A8216E35A19A9D9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaGroupBase
struct  XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F  : public XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaGroupBase::compiledItems
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___compiledItems_27;

public:
	inline static int32_t get_offset_of_compiledItems_27() { return static_cast<int32_t>(offsetof(XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F, ___compiledItems_27)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_compiledItems_27() const { return ___compiledItems_27; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_compiledItems_27() { return &___compiledItems_27; }
	inline void set_compiledItems_27(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___compiledItems_27 = value;
		Il2CppCodeGenWriteBarrier((&___compiledItems_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAGROUPBASE_TA64F460BB1ABB1F903265373A8216E35A19A9D9F_H
#ifndef XMLSCHEMAGROUPREF_T8051EFEAA73D8F0262C6D43F7A34430C4D018045_H
#define XMLSCHEMAGROUPREF_T8051EFEAA73D8F0262C6D43F7A34430C4D018045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaGroupRef
struct  XmlSchemaGroupRef_t8051EFEAA73D8F0262C6D43F7A34430C4D018045  : public XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaGroupRef::schema
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * ___schema_27;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaGroupRef::refName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___refName_28;
	// System.Xml.Schema.XmlSchemaGroup System.Xml.Schema.XmlSchemaGroupRef::referencedGroup
	XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447 * ___referencedGroup_29;
	// System.Boolean System.Xml.Schema.XmlSchemaGroupRef::busy
	bool ___busy_30;

public:
	inline static int32_t get_offset_of_schema_27() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_t8051EFEAA73D8F0262C6D43F7A34430C4D018045, ___schema_27)); }
	inline XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * get_schema_27() const { return ___schema_27; }
	inline XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 ** get_address_of_schema_27() { return &___schema_27; }
	inline void set_schema_27(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * value)
	{
		___schema_27 = value;
		Il2CppCodeGenWriteBarrier((&___schema_27), value);
	}

	inline static int32_t get_offset_of_refName_28() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_t8051EFEAA73D8F0262C6D43F7A34430C4D018045, ___refName_28)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_refName_28() const { return ___refName_28; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_refName_28() { return &___refName_28; }
	inline void set_refName_28(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___refName_28 = value;
		Il2CppCodeGenWriteBarrier((&___refName_28), value);
	}

	inline static int32_t get_offset_of_referencedGroup_29() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_t8051EFEAA73D8F0262C6D43F7A34430C4D018045, ___referencedGroup_29)); }
	inline XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447 * get_referencedGroup_29() const { return ___referencedGroup_29; }
	inline XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447 ** get_address_of_referencedGroup_29() { return &___referencedGroup_29; }
	inline void set_referencedGroup_29(XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447 * value)
	{
		___referencedGroup_29 = value;
		Il2CppCodeGenWriteBarrier((&___referencedGroup_29), value);
	}

	inline static int32_t get_offset_of_busy_30() { return static_cast<int32_t>(offsetof(XmlSchemaGroupRef_t8051EFEAA73D8F0262C6D43F7A34430C4D018045, ___busy_30)); }
	inline bool get_busy_30() const { return ___busy_30; }
	inline bool* get_address_of_busy_30() { return &___busy_30; }
	inline void set_busy_30(bool value)
	{
		___busy_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAGROUPREF_T8051EFEAA73D8F0262C6D43F7A34430C4D018045_H
#ifndef XMLSCHEMAKEY_T606D00D96AD3852E929D286EBF25F22425DFCE37_H
#define XMLSCHEMAKEY_T606D00D96AD3852E929D286EBF25F22425DFCE37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaKey
struct  XmlSchemaKey_t606D00D96AD3852E929D286EBF25F22425DFCE37  : public XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAKEY_T606D00D96AD3852E929D286EBF25F22425DFCE37_H
#ifndef XMLSCHEMAKEYREF_T6EA2162743310431E999C40A1FC1878AD96333CD_H
#define XMLSCHEMAKEYREF_T6EA2162743310431E999C40A1FC1878AD96333CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaKeyref
struct  XmlSchemaKeyref_t6EA2162743310431E999C40A1FC1878AD96333CD  : public XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaKeyref::refer
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___refer_21;
	// System.Xml.Schema.XmlSchemaIdentityConstraint System.Xml.Schema.XmlSchemaKeyref::target
	XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9 * ___target_22;

public:
	inline static int32_t get_offset_of_refer_21() { return static_cast<int32_t>(offsetof(XmlSchemaKeyref_t6EA2162743310431E999C40A1FC1878AD96333CD, ___refer_21)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_refer_21() const { return ___refer_21; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_refer_21() { return &___refer_21; }
	inline void set_refer_21(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___refer_21 = value;
		Il2CppCodeGenWriteBarrier((&___refer_21), value);
	}

	inline static int32_t get_offset_of_target_22() { return static_cast<int32_t>(offsetof(XmlSchemaKeyref_t6EA2162743310431E999C40A1FC1878AD96333CD, ___target_22)); }
	inline XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9 * get_target_22() const { return ___target_22; }
	inline XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9 ** get_address_of_target_22() { return &___target_22; }
	inline void set_target_22(XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9 * value)
	{
		___target_22 = value;
		Il2CppCodeGenWriteBarrier((&___target_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAKEYREF_T6EA2162743310431E999C40A1FC1878AD96333CD_H
#ifndef XMLSCHEMAMAXEXCLUSIVEFACET_TEBF888C0BDC463B0C0EC907651F24FA917948C03_H
#define XMLSCHEMAMAXEXCLUSIVEFACET_TEBF888C0BDC463B0C0EC907651F24FA917948C03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMaxExclusiveFacet
struct  XmlSchemaMaxExclusiveFacet_tEBF888C0BDC463B0C0EC907651F24FA917948C03  : public XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMAXEXCLUSIVEFACET_TEBF888C0BDC463B0C0EC907651F24FA917948C03_H
#ifndef XMLSCHEMAMAXINCLUSIVEFACET_TD6CCD809FC23EC06DE88B355BF245546486D56DD_H
#define XMLSCHEMAMAXINCLUSIVEFACET_TD6CCD809FC23EC06DE88B355BF245546486D56DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMaxInclusiveFacet
struct  XmlSchemaMaxInclusiveFacet_tD6CCD809FC23EC06DE88B355BF245546486D56DD  : public XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMAXINCLUSIVEFACET_TD6CCD809FC23EC06DE88B355BF245546486D56DD_H
#ifndef XMLSCHEMAMINEXCLUSIVEFACET_T78C40AF49FF59C16947EE4C24F5DDA4EEA8DE1EE_H
#define XMLSCHEMAMINEXCLUSIVEFACET_T78C40AF49FF59C16947EE4C24F5DDA4EEA8DE1EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMinExclusiveFacet
struct  XmlSchemaMinExclusiveFacet_t78C40AF49FF59C16947EE4C24F5DDA4EEA8DE1EE  : public XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMINEXCLUSIVEFACET_T78C40AF49FF59C16947EE4C24F5DDA4EEA8DE1EE_H
#ifndef XMLSCHEMAMININCLUSIVEFACET_T078A14BD8767AE5F6BAE256D2B586C7FED9C6DA7_H
#define XMLSCHEMAMININCLUSIVEFACET_T078A14BD8767AE5F6BAE256D2B586C7FED9C6DA7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMinInclusiveFacet
struct  XmlSchemaMinInclusiveFacet_t078A14BD8767AE5F6BAE256D2B586C7FED9C6DA7  : public XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMININCLUSIVEFACET_T078A14BD8767AE5F6BAE256D2B586C7FED9C6DA7_H
#ifndef XMLSCHEMANUMERICFACET_TC683FA814030977CB1A1D5C7421972B919022093_H
#define XMLSCHEMANUMERICFACET_TC683FA814030977CB1A1D5C7421972B919022093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaNumericFacet
struct  XmlSchemaNumericFacet_tC683FA814030977CB1A1D5C7421972B919022093  : public XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMANUMERICFACET_TC683FA814030977CB1A1D5C7421972B919022093_H
#ifndef EMPTYPARTICLE_T057637DE53E002545F1DD9A85A68E91CDE6DD069_H
#define EMPTYPARTICLE_T057637DE53E002545F1DD9A85A68E91CDE6DD069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaParticle_EmptyParticle
struct  EmptyParticle_t057637DE53E002545F1DD9A85A68E91CDE6DD069  : public XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYPARTICLE_T057637DE53E002545F1DD9A85A68E91CDE6DD069_H
#ifndef XMLSCHEMAPATTERNFACET_T1B3D1C8F8D7DF14F25DC513CC73DA23F87F0F027_H
#define XMLSCHEMAPATTERNFACET_T1B3D1C8F8D7DF14F25DC513CC73DA23F87F0F027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaPatternFacet
struct  XmlSchemaPatternFacet_t1B3D1C8F8D7DF14F25DC513CC73DA23F87F0F027  : public XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAPATTERNFACET_T1B3D1C8F8D7DF14F25DC513CC73DA23F87F0F027_H
#ifndef XMLSCHEMASIMPLECONTENT_TDF35C7E8421B617A50454FE566C8F58367307C2B_H
#define XMLSCHEMASIMPLECONTENT_TDF35C7E8421B617A50454FE566C8F58367307C2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleContent
struct  XmlSchemaSimpleContent_tDF35C7E8421B617A50454FE566C8F58367307C2B  : public XmlSchemaContentModel_tB057D601009DC1517A2A538E001E592F465568D9
{
public:
	// System.Xml.Schema.XmlSchemaContent System.Xml.Schema.XmlSchemaSimpleContent::content
	XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA * ___content_16;

public:
	inline static int32_t get_offset_of_content_16() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContent_tDF35C7E8421B617A50454FE566C8F58367307C2B, ___content_16)); }
	inline XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA * get_content_16() const { return ___content_16; }
	inline XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA ** get_address_of_content_16() { return &___content_16; }
	inline void set_content_16(XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA * value)
	{
		___content_16 = value;
		Il2CppCodeGenWriteBarrier((&___content_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLECONTENT_TDF35C7E8421B617A50454FE566C8F58367307C2B_H
#ifndef XMLSCHEMASIMPLECONTENTEXTENSION_T9B356432170682F512C2C1765E2D2021E6BE4A09_H
#define XMLSCHEMASIMPLECONTENTEXTENSION_T9B356432170682F512C2C1765E2D2021E6BE4A09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleContentExtension
struct  XmlSchemaSimpleContentExtension_t9B356432170682F512C2C1765E2D2021E6BE4A09  : public XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA
{
public:
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaSimpleContentExtension::any
	XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * ___any_17;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleContentExtension::attributes
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___attributes_18;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleContentExtension::baseTypeName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___baseTypeName_19;

public:
	inline static int32_t get_offset_of_any_17() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentExtension_t9B356432170682F512C2C1765E2D2021E6BE4A09, ___any_17)); }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * get_any_17() const { return ___any_17; }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 ** get_address_of_any_17() { return &___any_17; }
	inline void set_any_17(XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * value)
	{
		___any_17 = value;
		Il2CppCodeGenWriteBarrier((&___any_17), value);
	}

	inline static int32_t get_offset_of_attributes_18() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentExtension_t9B356432170682F512C2C1765E2D2021E6BE4A09, ___attributes_18)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_attributes_18() const { return ___attributes_18; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_attributes_18() { return &___attributes_18; }
	inline void set_attributes_18(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___attributes_18 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_18), value);
	}

	inline static int32_t get_offset_of_baseTypeName_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentExtension_t9B356432170682F512C2C1765E2D2021E6BE4A09, ___baseTypeName_19)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_baseTypeName_19() const { return ___baseTypeName_19; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_baseTypeName_19() { return &___baseTypeName_19; }
	inline void set_baseTypeName_19(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___baseTypeName_19 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLECONTENTEXTENSION_T9B356432170682F512C2C1765E2D2021E6BE4A09_H
#ifndef XMLSCHEMASIMPLECONTENTRESTRICTION_T0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB_H
#define XMLSCHEMASIMPLECONTENTRESTRICTION_T0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleContentRestriction
struct  XmlSchemaSimpleContentRestriction_t0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB  : public XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA
{
public:
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaSimpleContentRestriction::any
	XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * ___any_17;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleContentRestriction::attributes
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___attributes_18;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleContentRestriction::baseType
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___baseType_19;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleContentRestriction::baseTypeName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___baseTypeName_20;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleContentRestriction::facets
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___facets_21;

public:
	inline static int32_t get_offset_of_any_17() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB, ___any_17)); }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * get_any_17() const { return ___any_17; }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 ** get_address_of_any_17() { return &___any_17; }
	inline void set_any_17(XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * value)
	{
		___any_17 = value;
		Il2CppCodeGenWriteBarrier((&___any_17), value);
	}

	inline static int32_t get_offset_of_attributes_18() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB, ___attributes_18)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_attributes_18() const { return ___attributes_18; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_attributes_18() { return &___attributes_18; }
	inline void set_attributes_18(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___attributes_18 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_18), value);
	}

	inline static int32_t get_offset_of_baseType_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB, ___baseType_19)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_baseType_19() const { return ___baseType_19; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_baseType_19() { return &___baseType_19; }
	inline void set_baseType_19(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___baseType_19 = value;
		Il2CppCodeGenWriteBarrier((&___baseType_19), value);
	}

	inline static int32_t get_offset_of_baseTypeName_20() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB, ___baseTypeName_20)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_baseTypeName_20() const { return ___baseTypeName_20; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_baseTypeName_20() { return &___baseTypeName_20; }
	inline void set_baseTypeName_20(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___baseTypeName_20 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_20), value);
	}

	inline static int32_t get_offset_of_facets_21() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleContentRestriction_t0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB, ___facets_21)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_facets_21() const { return ___facets_21; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_facets_21() { return &___facets_21; }
	inline void set_facets_21(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___facets_21 = value;
		Il2CppCodeGenWriteBarrier((&___facets_21), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLECONTENTRESTRICTION_T0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB_H
#ifndef XMLSCHEMASIMPLETYPE_T0750B9C370AB270CCE4C37FD04AC0F7809BAC908_H
#define XMLSCHEMASIMPLETYPE_T0750B9C370AB270CCE4C37FD04AC0F7809BAC908_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleType
struct  XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908  : public XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE
{
public:
	// System.Xml.Schema.XmlSchemaSimpleTypeContent System.Xml.Schema.XmlSchemaSimpleType::content
	XmlSchemaSimpleTypeContent_t9BD29AE1058626894FD1F7EEAC14CD35BC3428EE * ___content_29;
	// System.Boolean System.Xml.Schema.XmlSchemaSimpleType::islocal
	bool ___islocal_30;
	// System.Boolean System.Xml.Schema.XmlSchemaSimpleType::recursed
	bool ___recursed_31;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaSimpleType::variety
	int32_t ___variety_32;

public:
	inline static int32_t get_offset_of_content_29() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908, ___content_29)); }
	inline XmlSchemaSimpleTypeContent_t9BD29AE1058626894FD1F7EEAC14CD35BC3428EE * get_content_29() const { return ___content_29; }
	inline XmlSchemaSimpleTypeContent_t9BD29AE1058626894FD1F7EEAC14CD35BC3428EE ** get_address_of_content_29() { return &___content_29; }
	inline void set_content_29(XmlSchemaSimpleTypeContent_t9BD29AE1058626894FD1F7EEAC14CD35BC3428EE * value)
	{
		___content_29 = value;
		Il2CppCodeGenWriteBarrier((&___content_29), value);
	}

	inline static int32_t get_offset_of_islocal_30() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908, ___islocal_30)); }
	inline bool get_islocal_30() const { return ___islocal_30; }
	inline bool* get_address_of_islocal_30() { return &___islocal_30; }
	inline void set_islocal_30(bool value)
	{
		___islocal_30 = value;
	}

	inline static int32_t get_offset_of_recursed_31() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908, ___recursed_31)); }
	inline bool get_recursed_31() const { return ___recursed_31; }
	inline bool* get_address_of_recursed_31() { return &___recursed_31; }
	inline void set_recursed_31(bool value)
	{
		___recursed_31 = value;
	}

	inline static int32_t get_offset_of_variety_32() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908, ___variety_32)); }
	inline int32_t get_variety_32() const { return ___variety_32; }
	inline int32_t* get_address_of_variety_32() { return &___variety_32; }
	inline void set_variety_32(int32_t value)
	{
		___variety_32 = value;
	}
};

struct XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::schemaLocationType
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___schemaLocationType_28;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsAnySimpleType
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsAnySimpleType_33;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsString
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsString_34;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsBoolean
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsBoolean_35;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDecimal
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsDecimal_36;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsFloat
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsFloat_37;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDouble
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsDouble_38;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDuration
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsDuration_39;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDateTime
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsDateTime_40;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsTime
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsTime_41;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsDate
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsDate_42;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGYearMonth
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsGYearMonth_43;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGYear
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsGYear_44;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGMonthDay
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsGMonthDay_45;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGDay
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsGDay_46;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsGMonth
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsGMonth_47;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsHexBinary
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsHexBinary_48;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsBase64Binary
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsBase64Binary_49;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsAnyUri
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsAnyUri_50;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsQName
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsQName_51;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNotation
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsNotation_52;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNormalizedString
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsNormalizedString_53;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsToken
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsToken_54;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsLanguage
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsLanguage_55;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNMToken
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsNMToken_56;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNMTokens
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsNMTokens_57;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsName
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsName_58;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNCName
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsNCName_59;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsID
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsID_60;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsIDRef
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsIDRef_61;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsIDRefs
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsIDRefs_62;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsEntity
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsEntity_63;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsEntities
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsEntities_64;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsInteger
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsInteger_65;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNonPositiveInteger
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsNonPositiveInteger_66;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNegativeInteger
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsNegativeInteger_67;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsLong
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsLong_68;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsInt
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsInt_69;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsShort
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsShort_70;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsByte
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsByte_71;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsNonNegativeInteger
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsNonNegativeInteger_72;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedLong
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsUnsignedLong_73;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedInt
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsUnsignedInt_74;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedShort
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsUnsignedShort_75;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsUnsignedByte
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsUnsignedByte_76;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XsPositiveInteger
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XsPositiveInteger_77;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtUntypedAtomic
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XdtUntypedAtomic_78;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtAnyAtomicType
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XdtAnyAtomicType_79;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtYearMonthDuration
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XdtYearMonthDuration_80;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleType::XdtDayTimeDuration
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___XdtDayTimeDuration_81;

public:
	inline static int32_t get_offset_of_schemaLocationType_28() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___schemaLocationType_28)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_schemaLocationType_28() const { return ___schemaLocationType_28; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_schemaLocationType_28() { return &___schemaLocationType_28; }
	inline void set_schemaLocationType_28(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___schemaLocationType_28 = value;
		Il2CppCodeGenWriteBarrier((&___schemaLocationType_28), value);
	}

	inline static int32_t get_offset_of_XsAnySimpleType_33() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsAnySimpleType_33)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsAnySimpleType_33() const { return ___XsAnySimpleType_33; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsAnySimpleType_33() { return &___XsAnySimpleType_33; }
	inline void set_XsAnySimpleType_33(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsAnySimpleType_33 = value;
		Il2CppCodeGenWriteBarrier((&___XsAnySimpleType_33), value);
	}

	inline static int32_t get_offset_of_XsString_34() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsString_34)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsString_34() const { return ___XsString_34; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsString_34() { return &___XsString_34; }
	inline void set_XsString_34(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsString_34 = value;
		Il2CppCodeGenWriteBarrier((&___XsString_34), value);
	}

	inline static int32_t get_offset_of_XsBoolean_35() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsBoolean_35)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsBoolean_35() const { return ___XsBoolean_35; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsBoolean_35() { return &___XsBoolean_35; }
	inline void set_XsBoolean_35(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsBoolean_35 = value;
		Il2CppCodeGenWriteBarrier((&___XsBoolean_35), value);
	}

	inline static int32_t get_offset_of_XsDecimal_36() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsDecimal_36)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsDecimal_36() const { return ___XsDecimal_36; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsDecimal_36() { return &___XsDecimal_36; }
	inline void set_XsDecimal_36(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsDecimal_36 = value;
		Il2CppCodeGenWriteBarrier((&___XsDecimal_36), value);
	}

	inline static int32_t get_offset_of_XsFloat_37() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsFloat_37)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsFloat_37() const { return ___XsFloat_37; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsFloat_37() { return &___XsFloat_37; }
	inline void set_XsFloat_37(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsFloat_37 = value;
		Il2CppCodeGenWriteBarrier((&___XsFloat_37), value);
	}

	inline static int32_t get_offset_of_XsDouble_38() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsDouble_38)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsDouble_38() const { return ___XsDouble_38; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsDouble_38() { return &___XsDouble_38; }
	inline void set_XsDouble_38(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsDouble_38 = value;
		Il2CppCodeGenWriteBarrier((&___XsDouble_38), value);
	}

	inline static int32_t get_offset_of_XsDuration_39() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsDuration_39)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsDuration_39() const { return ___XsDuration_39; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsDuration_39() { return &___XsDuration_39; }
	inline void set_XsDuration_39(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsDuration_39 = value;
		Il2CppCodeGenWriteBarrier((&___XsDuration_39), value);
	}

	inline static int32_t get_offset_of_XsDateTime_40() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsDateTime_40)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsDateTime_40() const { return ___XsDateTime_40; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsDateTime_40() { return &___XsDateTime_40; }
	inline void set_XsDateTime_40(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsDateTime_40 = value;
		Il2CppCodeGenWriteBarrier((&___XsDateTime_40), value);
	}

	inline static int32_t get_offset_of_XsTime_41() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsTime_41)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsTime_41() const { return ___XsTime_41; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsTime_41() { return &___XsTime_41; }
	inline void set_XsTime_41(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsTime_41 = value;
		Il2CppCodeGenWriteBarrier((&___XsTime_41), value);
	}

	inline static int32_t get_offset_of_XsDate_42() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsDate_42)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsDate_42() const { return ___XsDate_42; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsDate_42() { return &___XsDate_42; }
	inline void set_XsDate_42(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsDate_42 = value;
		Il2CppCodeGenWriteBarrier((&___XsDate_42), value);
	}

	inline static int32_t get_offset_of_XsGYearMonth_43() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsGYearMonth_43)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsGYearMonth_43() const { return ___XsGYearMonth_43; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsGYearMonth_43() { return &___XsGYearMonth_43; }
	inline void set_XsGYearMonth_43(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsGYearMonth_43 = value;
		Il2CppCodeGenWriteBarrier((&___XsGYearMonth_43), value);
	}

	inline static int32_t get_offset_of_XsGYear_44() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsGYear_44)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsGYear_44() const { return ___XsGYear_44; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsGYear_44() { return &___XsGYear_44; }
	inline void set_XsGYear_44(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsGYear_44 = value;
		Il2CppCodeGenWriteBarrier((&___XsGYear_44), value);
	}

	inline static int32_t get_offset_of_XsGMonthDay_45() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsGMonthDay_45)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsGMonthDay_45() const { return ___XsGMonthDay_45; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsGMonthDay_45() { return &___XsGMonthDay_45; }
	inline void set_XsGMonthDay_45(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsGMonthDay_45 = value;
		Il2CppCodeGenWriteBarrier((&___XsGMonthDay_45), value);
	}

	inline static int32_t get_offset_of_XsGDay_46() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsGDay_46)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsGDay_46() const { return ___XsGDay_46; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsGDay_46() { return &___XsGDay_46; }
	inline void set_XsGDay_46(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsGDay_46 = value;
		Il2CppCodeGenWriteBarrier((&___XsGDay_46), value);
	}

	inline static int32_t get_offset_of_XsGMonth_47() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsGMonth_47)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsGMonth_47() const { return ___XsGMonth_47; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsGMonth_47() { return &___XsGMonth_47; }
	inline void set_XsGMonth_47(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsGMonth_47 = value;
		Il2CppCodeGenWriteBarrier((&___XsGMonth_47), value);
	}

	inline static int32_t get_offset_of_XsHexBinary_48() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsHexBinary_48)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsHexBinary_48() const { return ___XsHexBinary_48; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsHexBinary_48() { return &___XsHexBinary_48; }
	inline void set_XsHexBinary_48(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsHexBinary_48 = value;
		Il2CppCodeGenWriteBarrier((&___XsHexBinary_48), value);
	}

	inline static int32_t get_offset_of_XsBase64Binary_49() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsBase64Binary_49)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsBase64Binary_49() const { return ___XsBase64Binary_49; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsBase64Binary_49() { return &___XsBase64Binary_49; }
	inline void set_XsBase64Binary_49(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsBase64Binary_49 = value;
		Il2CppCodeGenWriteBarrier((&___XsBase64Binary_49), value);
	}

	inline static int32_t get_offset_of_XsAnyUri_50() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsAnyUri_50)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsAnyUri_50() const { return ___XsAnyUri_50; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsAnyUri_50() { return &___XsAnyUri_50; }
	inline void set_XsAnyUri_50(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsAnyUri_50 = value;
		Il2CppCodeGenWriteBarrier((&___XsAnyUri_50), value);
	}

	inline static int32_t get_offset_of_XsQName_51() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsQName_51)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsQName_51() const { return ___XsQName_51; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsQName_51() { return &___XsQName_51; }
	inline void set_XsQName_51(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsQName_51 = value;
		Il2CppCodeGenWriteBarrier((&___XsQName_51), value);
	}

	inline static int32_t get_offset_of_XsNotation_52() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsNotation_52)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsNotation_52() const { return ___XsNotation_52; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsNotation_52() { return &___XsNotation_52; }
	inline void set_XsNotation_52(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsNotation_52 = value;
		Il2CppCodeGenWriteBarrier((&___XsNotation_52), value);
	}

	inline static int32_t get_offset_of_XsNormalizedString_53() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsNormalizedString_53)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsNormalizedString_53() const { return ___XsNormalizedString_53; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsNormalizedString_53() { return &___XsNormalizedString_53; }
	inline void set_XsNormalizedString_53(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsNormalizedString_53 = value;
		Il2CppCodeGenWriteBarrier((&___XsNormalizedString_53), value);
	}

	inline static int32_t get_offset_of_XsToken_54() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsToken_54)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsToken_54() const { return ___XsToken_54; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsToken_54() { return &___XsToken_54; }
	inline void set_XsToken_54(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsToken_54 = value;
		Il2CppCodeGenWriteBarrier((&___XsToken_54), value);
	}

	inline static int32_t get_offset_of_XsLanguage_55() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsLanguage_55)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsLanguage_55() const { return ___XsLanguage_55; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsLanguage_55() { return &___XsLanguage_55; }
	inline void set_XsLanguage_55(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsLanguage_55 = value;
		Il2CppCodeGenWriteBarrier((&___XsLanguage_55), value);
	}

	inline static int32_t get_offset_of_XsNMToken_56() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsNMToken_56)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsNMToken_56() const { return ___XsNMToken_56; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsNMToken_56() { return &___XsNMToken_56; }
	inline void set_XsNMToken_56(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsNMToken_56 = value;
		Il2CppCodeGenWriteBarrier((&___XsNMToken_56), value);
	}

	inline static int32_t get_offset_of_XsNMTokens_57() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsNMTokens_57)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsNMTokens_57() const { return ___XsNMTokens_57; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsNMTokens_57() { return &___XsNMTokens_57; }
	inline void set_XsNMTokens_57(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsNMTokens_57 = value;
		Il2CppCodeGenWriteBarrier((&___XsNMTokens_57), value);
	}

	inline static int32_t get_offset_of_XsName_58() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsName_58)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsName_58() const { return ___XsName_58; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsName_58() { return &___XsName_58; }
	inline void set_XsName_58(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsName_58 = value;
		Il2CppCodeGenWriteBarrier((&___XsName_58), value);
	}

	inline static int32_t get_offset_of_XsNCName_59() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsNCName_59)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsNCName_59() const { return ___XsNCName_59; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsNCName_59() { return &___XsNCName_59; }
	inline void set_XsNCName_59(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsNCName_59 = value;
		Il2CppCodeGenWriteBarrier((&___XsNCName_59), value);
	}

	inline static int32_t get_offset_of_XsID_60() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsID_60)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsID_60() const { return ___XsID_60; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsID_60() { return &___XsID_60; }
	inline void set_XsID_60(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsID_60 = value;
		Il2CppCodeGenWriteBarrier((&___XsID_60), value);
	}

	inline static int32_t get_offset_of_XsIDRef_61() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsIDRef_61)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsIDRef_61() const { return ___XsIDRef_61; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsIDRef_61() { return &___XsIDRef_61; }
	inline void set_XsIDRef_61(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsIDRef_61 = value;
		Il2CppCodeGenWriteBarrier((&___XsIDRef_61), value);
	}

	inline static int32_t get_offset_of_XsIDRefs_62() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsIDRefs_62)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsIDRefs_62() const { return ___XsIDRefs_62; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsIDRefs_62() { return &___XsIDRefs_62; }
	inline void set_XsIDRefs_62(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsIDRefs_62 = value;
		Il2CppCodeGenWriteBarrier((&___XsIDRefs_62), value);
	}

	inline static int32_t get_offset_of_XsEntity_63() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsEntity_63)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsEntity_63() const { return ___XsEntity_63; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsEntity_63() { return &___XsEntity_63; }
	inline void set_XsEntity_63(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsEntity_63 = value;
		Il2CppCodeGenWriteBarrier((&___XsEntity_63), value);
	}

	inline static int32_t get_offset_of_XsEntities_64() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsEntities_64)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsEntities_64() const { return ___XsEntities_64; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsEntities_64() { return &___XsEntities_64; }
	inline void set_XsEntities_64(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsEntities_64 = value;
		Il2CppCodeGenWriteBarrier((&___XsEntities_64), value);
	}

	inline static int32_t get_offset_of_XsInteger_65() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsInteger_65)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsInteger_65() const { return ___XsInteger_65; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsInteger_65() { return &___XsInteger_65; }
	inline void set_XsInteger_65(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsInteger_65 = value;
		Il2CppCodeGenWriteBarrier((&___XsInteger_65), value);
	}

	inline static int32_t get_offset_of_XsNonPositiveInteger_66() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsNonPositiveInteger_66)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsNonPositiveInteger_66() const { return ___XsNonPositiveInteger_66; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsNonPositiveInteger_66() { return &___XsNonPositiveInteger_66; }
	inline void set_XsNonPositiveInteger_66(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsNonPositiveInteger_66 = value;
		Il2CppCodeGenWriteBarrier((&___XsNonPositiveInteger_66), value);
	}

	inline static int32_t get_offset_of_XsNegativeInteger_67() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsNegativeInteger_67)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsNegativeInteger_67() const { return ___XsNegativeInteger_67; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsNegativeInteger_67() { return &___XsNegativeInteger_67; }
	inline void set_XsNegativeInteger_67(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsNegativeInteger_67 = value;
		Il2CppCodeGenWriteBarrier((&___XsNegativeInteger_67), value);
	}

	inline static int32_t get_offset_of_XsLong_68() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsLong_68)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsLong_68() const { return ___XsLong_68; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsLong_68() { return &___XsLong_68; }
	inline void set_XsLong_68(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsLong_68 = value;
		Il2CppCodeGenWriteBarrier((&___XsLong_68), value);
	}

	inline static int32_t get_offset_of_XsInt_69() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsInt_69)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsInt_69() const { return ___XsInt_69; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsInt_69() { return &___XsInt_69; }
	inline void set_XsInt_69(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsInt_69 = value;
		Il2CppCodeGenWriteBarrier((&___XsInt_69), value);
	}

	inline static int32_t get_offset_of_XsShort_70() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsShort_70)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsShort_70() const { return ___XsShort_70; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsShort_70() { return &___XsShort_70; }
	inline void set_XsShort_70(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsShort_70 = value;
		Il2CppCodeGenWriteBarrier((&___XsShort_70), value);
	}

	inline static int32_t get_offset_of_XsByte_71() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsByte_71)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsByte_71() const { return ___XsByte_71; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsByte_71() { return &___XsByte_71; }
	inline void set_XsByte_71(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsByte_71 = value;
		Il2CppCodeGenWriteBarrier((&___XsByte_71), value);
	}

	inline static int32_t get_offset_of_XsNonNegativeInteger_72() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsNonNegativeInteger_72)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsNonNegativeInteger_72() const { return ___XsNonNegativeInteger_72; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsNonNegativeInteger_72() { return &___XsNonNegativeInteger_72; }
	inline void set_XsNonNegativeInteger_72(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsNonNegativeInteger_72 = value;
		Il2CppCodeGenWriteBarrier((&___XsNonNegativeInteger_72), value);
	}

	inline static int32_t get_offset_of_XsUnsignedLong_73() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsUnsignedLong_73)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsUnsignedLong_73() const { return ___XsUnsignedLong_73; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsUnsignedLong_73() { return &___XsUnsignedLong_73; }
	inline void set_XsUnsignedLong_73(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsUnsignedLong_73 = value;
		Il2CppCodeGenWriteBarrier((&___XsUnsignedLong_73), value);
	}

	inline static int32_t get_offset_of_XsUnsignedInt_74() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsUnsignedInt_74)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsUnsignedInt_74() const { return ___XsUnsignedInt_74; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsUnsignedInt_74() { return &___XsUnsignedInt_74; }
	inline void set_XsUnsignedInt_74(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsUnsignedInt_74 = value;
		Il2CppCodeGenWriteBarrier((&___XsUnsignedInt_74), value);
	}

	inline static int32_t get_offset_of_XsUnsignedShort_75() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsUnsignedShort_75)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsUnsignedShort_75() const { return ___XsUnsignedShort_75; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsUnsignedShort_75() { return &___XsUnsignedShort_75; }
	inline void set_XsUnsignedShort_75(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsUnsignedShort_75 = value;
		Il2CppCodeGenWriteBarrier((&___XsUnsignedShort_75), value);
	}

	inline static int32_t get_offset_of_XsUnsignedByte_76() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsUnsignedByte_76)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsUnsignedByte_76() const { return ___XsUnsignedByte_76; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsUnsignedByte_76() { return &___XsUnsignedByte_76; }
	inline void set_XsUnsignedByte_76(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsUnsignedByte_76 = value;
		Il2CppCodeGenWriteBarrier((&___XsUnsignedByte_76), value);
	}

	inline static int32_t get_offset_of_XsPositiveInteger_77() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XsPositiveInteger_77)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XsPositiveInteger_77() const { return ___XsPositiveInteger_77; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XsPositiveInteger_77() { return &___XsPositiveInteger_77; }
	inline void set_XsPositiveInteger_77(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XsPositiveInteger_77 = value;
		Il2CppCodeGenWriteBarrier((&___XsPositiveInteger_77), value);
	}

	inline static int32_t get_offset_of_XdtUntypedAtomic_78() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XdtUntypedAtomic_78)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XdtUntypedAtomic_78() const { return ___XdtUntypedAtomic_78; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XdtUntypedAtomic_78() { return &___XdtUntypedAtomic_78; }
	inline void set_XdtUntypedAtomic_78(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XdtUntypedAtomic_78 = value;
		Il2CppCodeGenWriteBarrier((&___XdtUntypedAtomic_78), value);
	}

	inline static int32_t get_offset_of_XdtAnyAtomicType_79() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XdtAnyAtomicType_79)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XdtAnyAtomicType_79() const { return ___XdtAnyAtomicType_79; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XdtAnyAtomicType_79() { return &___XdtAnyAtomicType_79; }
	inline void set_XdtAnyAtomicType_79(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XdtAnyAtomicType_79 = value;
		Il2CppCodeGenWriteBarrier((&___XdtAnyAtomicType_79), value);
	}

	inline static int32_t get_offset_of_XdtYearMonthDuration_80() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XdtYearMonthDuration_80)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XdtYearMonthDuration_80() const { return ___XdtYearMonthDuration_80; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XdtYearMonthDuration_80() { return &___XdtYearMonthDuration_80; }
	inline void set_XdtYearMonthDuration_80(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XdtYearMonthDuration_80 = value;
		Il2CppCodeGenWriteBarrier((&___XdtYearMonthDuration_80), value);
	}

	inline static int32_t get_offset_of_XdtDayTimeDuration_81() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields, ___XdtDayTimeDuration_81)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_XdtDayTimeDuration_81() const { return ___XdtDayTimeDuration_81; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_XdtDayTimeDuration_81() { return &___XdtDayTimeDuration_81; }
	inline void set_XdtDayTimeDuration_81(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___XdtDayTimeDuration_81 = value;
		Il2CppCodeGenWriteBarrier((&___XdtDayTimeDuration_81), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPE_T0750B9C370AB270CCE4C37FD04AC0F7809BAC908_H
#ifndef XMLSCHEMASIMPLETYPELIST_TCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D_H
#define XMLSCHEMASIMPLETYPELIST_TCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeList
struct  XmlSchemaSimpleTypeList_tCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D  : public XmlSchemaSimpleTypeContent_t9BD29AE1058626894FD1F7EEAC14CD35BC3428EE
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeList::itemType
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___itemType_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleTypeList::itemTypeName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___itemTypeName_18;
	// System.Object System.Xml.Schema.XmlSchemaSimpleTypeList::validatedListItemType
	RuntimeObject * ___validatedListItemType_19;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeList::validatedListItemSchemaType
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___validatedListItemSchemaType_20;

public:
	inline static int32_t get_offset_of_itemType_17() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_tCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D, ___itemType_17)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_itemType_17() const { return ___itemType_17; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_itemType_17() { return &___itemType_17; }
	inline void set_itemType_17(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___itemType_17 = value;
		Il2CppCodeGenWriteBarrier((&___itemType_17), value);
	}

	inline static int32_t get_offset_of_itemTypeName_18() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_tCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D, ___itemTypeName_18)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_itemTypeName_18() const { return ___itemTypeName_18; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_itemTypeName_18() { return &___itemTypeName_18; }
	inline void set_itemTypeName_18(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___itemTypeName_18 = value;
		Il2CppCodeGenWriteBarrier((&___itemTypeName_18), value);
	}

	inline static int32_t get_offset_of_validatedListItemType_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_tCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D, ___validatedListItemType_19)); }
	inline RuntimeObject * get_validatedListItemType_19() const { return ___validatedListItemType_19; }
	inline RuntimeObject ** get_address_of_validatedListItemType_19() { return &___validatedListItemType_19; }
	inline void set_validatedListItemType_19(RuntimeObject * value)
	{
		___validatedListItemType_19 = value;
		Il2CppCodeGenWriteBarrier((&___validatedListItemType_19), value);
	}

	inline static int32_t get_offset_of_validatedListItemSchemaType_20() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeList_tCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D, ___validatedListItemSchemaType_20)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_validatedListItemSchemaType_20() const { return ___validatedListItemSchemaType_20; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_validatedListItemSchemaType_20() { return &___validatedListItemSchemaType_20; }
	inline void set_validatedListItemSchemaType_20(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___validatedListItemSchemaType_20 = value;
		Il2CppCodeGenWriteBarrier((&___validatedListItemSchemaType_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPELIST_TCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D_H
#ifndef XMLSCHEMASIMPLETYPERESTRICTION_T1C64CC105547BCA6817A9FE12D358DB5CDDEA600_H
#define XMLSCHEMASIMPLETYPERESTRICTION_T1C64CC105547BCA6817A9FE12D358DB5CDDEA600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeRestriction
struct  XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600  : public XmlSchemaSimpleTypeContent_t9BD29AE1058626894FD1F7EEAC14CD35BC3428EE
{
public:
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaSimpleTypeRestriction::baseType
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___baseType_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaSimpleTypeRestriction::baseTypeName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___baseTypeName_18;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleTypeRestriction::facets
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___facets_19;
	// System.String[] System.Xml.Schema.XmlSchemaSimpleTypeRestriction::enumarationFacetValues
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___enumarationFacetValues_20;
	// System.String[] System.Xml.Schema.XmlSchemaSimpleTypeRestriction::patternFacetValues
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___patternFacetValues_21;
	// System.Text.RegularExpressions.Regex[] System.Xml.Schema.XmlSchemaSimpleTypeRestriction::rexPatterns
	RegexU5BU5D_tEA400C40510836EBE79B3CFB8AFC12F94B3690A1* ___rexPatterns_22;
	// System.Decimal System.Xml.Schema.XmlSchemaSimpleTypeRestriction::lengthFacet
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___lengthFacet_23;
	// System.Decimal System.Xml.Schema.XmlSchemaSimpleTypeRestriction::maxLengthFacet
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___maxLengthFacet_24;
	// System.Decimal System.Xml.Schema.XmlSchemaSimpleTypeRestriction::minLengthFacet
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___minLengthFacet_25;
	// System.Decimal System.Xml.Schema.XmlSchemaSimpleTypeRestriction::fractionDigitsFacet
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___fractionDigitsFacet_26;
	// System.Decimal System.Xml.Schema.XmlSchemaSimpleTypeRestriction::totalDigitsFacet
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___totalDigitsFacet_27;
	// System.Object System.Xml.Schema.XmlSchemaSimpleTypeRestriction::maxInclusiveFacet
	RuntimeObject * ___maxInclusiveFacet_28;
	// System.Object System.Xml.Schema.XmlSchemaSimpleTypeRestriction::maxExclusiveFacet
	RuntimeObject * ___maxExclusiveFacet_29;
	// System.Object System.Xml.Schema.XmlSchemaSimpleTypeRestriction::minInclusiveFacet
	RuntimeObject * ___minInclusiveFacet_30;
	// System.Object System.Xml.Schema.XmlSchemaSimpleTypeRestriction::minExclusiveFacet
	RuntimeObject * ___minExclusiveFacet_31;
	// System.Xml.Schema.XmlSchemaFacet_Facet System.Xml.Schema.XmlSchemaSimpleTypeRestriction::fixedFacets
	int32_t ___fixedFacets_32;

public:
	inline static int32_t get_offset_of_baseType_17() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___baseType_17)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_baseType_17() const { return ___baseType_17; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_baseType_17() { return &___baseType_17; }
	inline void set_baseType_17(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___baseType_17 = value;
		Il2CppCodeGenWriteBarrier((&___baseType_17), value);
	}

	inline static int32_t get_offset_of_baseTypeName_18() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___baseTypeName_18)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_baseTypeName_18() const { return ___baseTypeName_18; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_baseTypeName_18() { return &___baseTypeName_18; }
	inline void set_baseTypeName_18(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___baseTypeName_18 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_18), value);
	}

	inline static int32_t get_offset_of_facets_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___facets_19)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_facets_19() const { return ___facets_19; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_facets_19() { return &___facets_19; }
	inline void set_facets_19(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___facets_19 = value;
		Il2CppCodeGenWriteBarrier((&___facets_19), value);
	}

	inline static int32_t get_offset_of_enumarationFacetValues_20() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___enumarationFacetValues_20)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_enumarationFacetValues_20() const { return ___enumarationFacetValues_20; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_enumarationFacetValues_20() { return &___enumarationFacetValues_20; }
	inline void set_enumarationFacetValues_20(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___enumarationFacetValues_20 = value;
		Il2CppCodeGenWriteBarrier((&___enumarationFacetValues_20), value);
	}

	inline static int32_t get_offset_of_patternFacetValues_21() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___patternFacetValues_21)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_patternFacetValues_21() const { return ___patternFacetValues_21; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_patternFacetValues_21() { return &___patternFacetValues_21; }
	inline void set_patternFacetValues_21(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___patternFacetValues_21 = value;
		Il2CppCodeGenWriteBarrier((&___patternFacetValues_21), value);
	}

	inline static int32_t get_offset_of_rexPatterns_22() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___rexPatterns_22)); }
	inline RegexU5BU5D_tEA400C40510836EBE79B3CFB8AFC12F94B3690A1* get_rexPatterns_22() const { return ___rexPatterns_22; }
	inline RegexU5BU5D_tEA400C40510836EBE79B3CFB8AFC12F94B3690A1** get_address_of_rexPatterns_22() { return &___rexPatterns_22; }
	inline void set_rexPatterns_22(RegexU5BU5D_tEA400C40510836EBE79B3CFB8AFC12F94B3690A1* value)
	{
		___rexPatterns_22 = value;
		Il2CppCodeGenWriteBarrier((&___rexPatterns_22), value);
	}

	inline static int32_t get_offset_of_lengthFacet_23() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___lengthFacet_23)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_lengthFacet_23() const { return ___lengthFacet_23; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_lengthFacet_23() { return &___lengthFacet_23; }
	inline void set_lengthFacet_23(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___lengthFacet_23 = value;
	}

	inline static int32_t get_offset_of_maxLengthFacet_24() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___maxLengthFacet_24)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_maxLengthFacet_24() const { return ___maxLengthFacet_24; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_maxLengthFacet_24() { return &___maxLengthFacet_24; }
	inline void set_maxLengthFacet_24(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___maxLengthFacet_24 = value;
	}

	inline static int32_t get_offset_of_minLengthFacet_25() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___minLengthFacet_25)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_minLengthFacet_25() const { return ___minLengthFacet_25; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_minLengthFacet_25() { return &___minLengthFacet_25; }
	inline void set_minLengthFacet_25(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___minLengthFacet_25 = value;
	}

	inline static int32_t get_offset_of_fractionDigitsFacet_26() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___fractionDigitsFacet_26)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_fractionDigitsFacet_26() const { return ___fractionDigitsFacet_26; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_fractionDigitsFacet_26() { return &___fractionDigitsFacet_26; }
	inline void set_fractionDigitsFacet_26(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___fractionDigitsFacet_26 = value;
	}

	inline static int32_t get_offset_of_totalDigitsFacet_27() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___totalDigitsFacet_27)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_totalDigitsFacet_27() const { return ___totalDigitsFacet_27; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_totalDigitsFacet_27() { return &___totalDigitsFacet_27; }
	inline void set_totalDigitsFacet_27(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___totalDigitsFacet_27 = value;
	}

	inline static int32_t get_offset_of_maxInclusiveFacet_28() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___maxInclusiveFacet_28)); }
	inline RuntimeObject * get_maxInclusiveFacet_28() const { return ___maxInclusiveFacet_28; }
	inline RuntimeObject ** get_address_of_maxInclusiveFacet_28() { return &___maxInclusiveFacet_28; }
	inline void set_maxInclusiveFacet_28(RuntimeObject * value)
	{
		___maxInclusiveFacet_28 = value;
		Il2CppCodeGenWriteBarrier((&___maxInclusiveFacet_28), value);
	}

	inline static int32_t get_offset_of_maxExclusiveFacet_29() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___maxExclusiveFacet_29)); }
	inline RuntimeObject * get_maxExclusiveFacet_29() const { return ___maxExclusiveFacet_29; }
	inline RuntimeObject ** get_address_of_maxExclusiveFacet_29() { return &___maxExclusiveFacet_29; }
	inline void set_maxExclusiveFacet_29(RuntimeObject * value)
	{
		___maxExclusiveFacet_29 = value;
		Il2CppCodeGenWriteBarrier((&___maxExclusiveFacet_29), value);
	}

	inline static int32_t get_offset_of_minInclusiveFacet_30() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___minInclusiveFacet_30)); }
	inline RuntimeObject * get_minInclusiveFacet_30() const { return ___minInclusiveFacet_30; }
	inline RuntimeObject ** get_address_of_minInclusiveFacet_30() { return &___minInclusiveFacet_30; }
	inline void set_minInclusiveFacet_30(RuntimeObject * value)
	{
		___minInclusiveFacet_30 = value;
		Il2CppCodeGenWriteBarrier((&___minInclusiveFacet_30), value);
	}

	inline static int32_t get_offset_of_minExclusiveFacet_31() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___minExclusiveFacet_31)); }
	inline RuntimeObject * get_minExclusiveFacet_31() const { return ___minExclusiveFacet_31; }
	inline RuntimeObject ** get_address_of_minExclusiveFacet_31() { return &___minExclusiveFacet_31; }
	inline void set_minExclusiveFacet_31(RuntimeObject * value)
	{
		___minExclusiveFacet_31 = value;
		Il2CppCodeGenWriteBarrier((&___minExclusiveFacet_31), value);
	}

	inline static int32_t get_offset_of_fixedFacets_32() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600, ___fixedFacets_32)); }
	inline int32_t get_fixedFacets_32() const { return ___fixedFacets_32; }
	inline int32_t* get_address_of_fixedFacets_32() { return &___fixedFacets_32; }
	inline void set_fixedFacets_32(int32_t value)
	{
		___fixedFacets_32 = value;
	}
};

struct XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600_StaticFields
{
public:
	// System.Globalization.NumberStyles System.Xml.Schema.XmlSchemaSimpleTypeRestriction::lengthStyle
	int32_t ___lengthStyle_33;
	// System.Xml.Schema.XmlSchemaFacet_Facet System.Xml.Schema.XmlSchemaSimpleTypeRestriction::listFacets
	int32_t ___listFacets_34;

public:
	inline static int32_t get_offset_of_lengthStyle_33() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600_StaticFields, ___lengthStyle_33)); }
	inline int32_t get_lengthStyle_33() const { return ___lengthStyle_33; }
	inline int32_t* get_address_of_lengthStyle_33() { return &___lengthStyle_33; }
	inline void set_lengthStyle_33(int32_t value)
	{
		___lengthStyle_33 = value;
	}

	inline static int32_t get_offset_of_listFacets_34() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600_StaticFields, ___listFacets_34)); }
	inline int32_t get_listFacets_34() const { return ___listFacets_34; }
	inline int32_t* get_address_of_listFacets_34() { return &___listFacets_34; }
	inline void set_listFacets_34(int32_t value)
	{
		___listFacets_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPERESTRICTION_T1C64CC105547BCA6817A9FE12D358DB5CDDEA600_H
#ifndef XMLSCHEMASIMPLETYPEUNION_T028C5EE22C3F4662E04D5D5A1A4804A94D7230A2_H
#define XMLSCHEMASIMPLETYPEUNION_T028C5EE22C3F4662E04D5D5A1A4804A94D7230A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSimpleTypeUnion
struct  XmlSchemaSimpleTypeUnion_t028C5EE22C3F4662E04D5D5A1A4804A94D7230A2  : public XmlSchemaSimpleTypeContent_t9BD29AE1058626894FD1F7EEAC14CD35BC3428EE
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSimpleTypeUnion::baseTypes
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___baseTypes_17;
	// System.Xml.XmlQualifiedName[] System.Xml.Schema.XmlSchemaSimpleTypeUnion::memberTypes
	XmlQualifiedNameU5BU5D_t51B92152D4EBF947BF1D099E8398C5820F8CC88A* ___memberTypes_18;
	// System.Object[] System.Xml.Schema.XmlSchemaSimpleTypeUnion::validatedTypes
	ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* ___validatedTypes_19;
	// System.Xml.Schema.XmlSchemaSimpleType[] System.Xml.Schema.XmlSchemaSimpleTypeUnion::validatedSchemaTypes
	XmlSchemaSimpleTypeU5BU5D_t6E3AC48639AC0737688DBE5439FA9E940F0E1CF0* ___validatedSchemaTypes_20;

public:
	inline static int32_t get_offset_of_baseTypes_17() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t028C5EE22C3F4662E04D5D5A1A4804A94D7230A2, ___baseTypes_17)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_baseTypes_17() const { return ___baseTypes_17; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_baseTypes_17() { return &___baseTypes_17; }
	inline void set_baseTypes_17(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___baseTypes_17 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypes_17), value);
	}

	inline static int32_t get_offset_of_memberTypes_18() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t028C5EE22C3F4662E04D5D5A1A4804A94D7230A2, ___memberTypes_18)); }
	inline XmlQualifiedNameU5BU5D_t51B92152D4EBF947BF1D099E8398C5820F8CC88A* get_memberTypes_18() const { return ___memberTypes_18; }
	inline XmlQualifiedNameU5BU5D_t51B92152D4EBF947BF1D099E8398C5820F8CC88A** get_address_of_memberTypes_18() { return &___memberTypes_18; }
	inline void set_memberTypes_18(XmlQualifiedNameU5BU5D_t51B92152D4EBF947BF1D099E8398C5820F8CC88A* value)
	{
		___memberTypes_18 = value;
		Il2CppCodeGenWriteBarrier((&___memberTypes_18), value);
	}

	inline static int32_t get_offset_of_validatedTypes_19() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t028C5EE22C3F4662E04D5D5A1A4804A94D7230A2, ___validatedTypes_19)); }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* get_validatedTypes_19() const { return ___validatedTypes_19; }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D** get_address_of_validatedTypes_19() { return &___validatedTypes_19; }
	inline void set_validatedTypes_19(ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* value)
	{
		___validatedTypes_19 = value;
		Il2CppCodeGenWriteBarrier((&___validatedTypes_19), value);
	}

	inline static int32_t get_offset_of_validatedSchemaTypes_20() { return static_cast<int32_t>(offsetof(XmlSchemaSimpleTypeUnion_t028C5EE22C3F4662E04D5D5A1A4804A94D7230A2, ___validatedSchemaTypes_20)); }
	inline XmlSchemaSimpleTypeU5BU5D_t6E3AC48639AC0737688DBE5439FA9E940F0E1CF0* get_validatedSchemaTypes_20() const { return ___validatedSchemaTypes_20; }
	inline XmlSchemaSimpleTypeU5BU5D_t6E3AC48639AC0737688DBE5439FA9E940F0E1CF0** get_address_of_validatedSchemaTypes_20() { return &___validatedSchemaTypes_20; }
	inline void set_validatedSchemaTypes_20(XmlSchemaSimpleTypeU5BU5D_t6E3AC48639AC0737688DBE5439FA9E940F0E1CF0* value)
	{
		___validatedSchemaTypes_20 = value;
		Il2CppCodeGenWriteBarrier((&___validatedSchemaTypes_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASIMPLETYPEUNION_T028C5EE22C3F4662E04D5D5A1A4804A94D7230A2_H
#ifndef XMLSCHEMAUNIQUE_TCDD8BDD09DC0F6A7DC89713300BACCB3E0DE47DB_H
#define XMLSCHEMAUNIQUE_TCDD8BDD09DC0F6A7DC89713300BACCB3E0DE47DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaUnique
struct  XmlSchemaUnique_tCDD8BDD09DC0F6A7DC89713300BACCB3E0DE47DB  : public XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAUNIQUE_TCDD8BDD09DC0F6A7DC89713300BACCB3E0DE47DB_H
#ifndef XMLSCHEMAWHITESPACEFACET_T18FF49A883F305B0AD4386AA5C38FA170242D69C_H
#define XMLSCHEMAWHITESPACEFACET_T18FF49A883F305B0AD4386AA5C38FA170242D69C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaWhiteSpaceFacet
struct  XmlSchemaWhiteSpaceFacet_t18FF49A883F305B0AD4386AA5C38FA170242D69C  : public XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAWHITESPACEFACET_T18FF49A883F305B0AD4386AA5C38FA170242D69C_H
#ifndef XMLSCHEMAFRACTIONDIGITSFACET_TE6B195488B1F8CA18308803CCF44ECDD0D6CA436_H
#define XMLSCHEMAFRACTIONDIGITSFACET_TE6B195488B1F8CA18308803CCF44ECDD0D6CA436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFractionDigitsFacet
struct  XmlSchemaFractionDigitsFacet_tE6B195488B1F8CA18308803CCF44ECDD0D6CA436  : public XmlSchemaNumericFacet_tC683FA814030977CB1A1D5C7421972B919022093
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFRACTIONDIGITSFACET_TE6B195488B1F8CA18308803CCF44ECDD0D6CA436_H
#ifndef XMLSCHEMALENGTHFACET_T3830B4E2C1FC60F4D70ED12EE98322DECB29DADB_H
#define XMLSCHEMALENGTHFACET_T3830B4E2C1FC60F4D70ED12EE98322DECB29DADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaLengthFacet
struct  XmlSchemaLengthFacet_t3830B4E2C1FC60F4D70ED12EE98322DECB29DADB  : public XmlSchemaNumericFacet_tC683FA814030977CB1A1D5C7421972B919022093
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMALENGTHFACET_T3830B4E2C1FC60F4D70ED12EE98322DECB29DADB_H
#ifndef XMLSCHEMAMAXLENGTHFACET_TCA8010F52512E8C5989019422D228524E3A285E3_H
#define XMLSCHEMAMAXLENGTHFACET_TCA8010F52512E8C5989019422D228524E3A285E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMaxLengthFacet
struct  XmlSchemaMaxLengthFacet_tCA8010F52512E8C5989019422D228524E3A285E3  : public XmlSchemaNumericFacet_tC683FA814030977CB1A1D5C7421972B919022093
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMAXLENGTHFACET_TCA8010F52512E8C5989019422D228524E3A285E3_H
#ifndef XMLSCHEMAMINLENGTHFACET_T8E13AD03D1FD3A9D21991323E264E3B4BA8A939B_H
#define XMLSCHEMAMINLENGTHFACET_T8E13AD03D1FD3A9D21991323E264E3B4BA8A939B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaMinLengthFacet
struct  XmlSchemaMinLengthFacet_t8E13AD03D1FD3A9D21991323E264E3B4BA8A939B  : public XmlSchemaNumericFacet_tC683FA814030977CB1A1D5C7421972B919022093
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAMINLENGTHFACET_T8E13AD03D1FD3A9D21991323E264E3B4BA8A939B_H
#ifndef XMLSCHEMASEQUENCE_TC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483_H
#define XMLSCHEMASEQUENCE_TC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaSequence
struct  XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483  : public XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaSequence::items
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___items_28;

public:
	inline static int32_t get_offset_of_items_28() { return static_cast<int32_t>(offsetof(XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483, ___items_28)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_items_28() const { return ___items_28; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_items_28() { return &___items_28; }
	inline void set_items_28(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___items_28 = value;
		Il2CppCodeGenWriteBarrier((&___items_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMASEQUENCE_TC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483_H
#ifndef XMLSCHEMATOTALDIGITSFACET_T5032254A794559D12A5C1079F013A62C937E45FF_H
#define XMLSCHEMATOTALDIGITSFACET_T5032254A794559D12A5C1079F013A62C937E45FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaTotalDigitsFacet
struct  XmlSchemaTotalDigitsFacet_t5032254A794559D12A5C1079F013A62C937E45FF  : public XmlSchemaNumericFacet_tC683FA814030977CB1A1D5C7421972B919022093
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMATOTALDIGITSFACET_T5032254A794559D12A5C1079F013A62C937E45FF_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (XmlSchemaFractionDigitsFacet_tE6B195488B1F8CA18308803CCF44ECDD0D6CA436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[4] = 
{
	XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447::get_offset_of_name_16(),
	XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447::get_offset_of_particle_17(),
	XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447::get_offset_of_qualifiedName_18(),
	XmlSchemaGroup_tE769C5EFDC105F19982EE5842FA5880B106F2447::get_offset_of_isCircularDefinition_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[1] = 
{
	XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F::get_offset_of_compiledItems_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (XmlSchemaGroupRef_t8051EFEAA73D8F0262C6D43F7A34430C4D018045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[4] = 
{
	XmlSchemaGroupRef_t8051EFEAA73D8F0262C6D43F7A34430C4D018045::get_offset_of_schema_27(),
	XmlSchemaGroupRef_t8051EFEAA73D8F0262C6D43F7A34430C4D018045::get_offset_of_refName_28(),
	XmlSchemaGroupRef_t8051EFEAA73D8F0262C6D43F7A34430C4D018045::get_offset_of_referencedGroup_29(),
	XmlSchemaGroupRef_t8051EFEAA73D8F0262C6D43F7A34430C4D018045::get_offset_of_busy_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[5] = 
{
	XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9::get_offset_of_fields_16(),
	XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9::get_offset_of_name_17(),
	XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9::get_offset_of_qName_18(),
	XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9::get_offset_of_selector_19(),
	XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9::get_offset_of_compiledSelector_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (XmlSchemaImport_t6171304957CE0DE0A7528CFE1AAC8A828165BED1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[2] = 
{
	XmlSchemaImport_t6171304957CE0DE0A7528CFE1AAC8A828165BED1::get_offset_of_annotation_16(),
	XmlSchemaImport_t6171304957CE0DE0A7528CFE1AAC8A828165BED1::get_offset_of_nameSpace_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (XmlSchemaInclude_t4386F1693047C7C283432338D09B7F97DF1A844D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[1] = 
{
	XmlSchemaInclude_t4386F1693047C7C283432338D09B7F97DF1A844D::get_offset_of_annotation_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[7] = 
{
	XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C::get_offset_of_isDefault_0(),
	XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C::get_offset_of_isNil_1(),
	XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C::get_offset_of_memberType_2(),
	XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C::get_offset_of_attr_3(),
	XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C::get_offset_of_elem_4(),
	XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C::get_offset_of_type_5(),
	XmlSchemaInfo_t14DC1A0ED52BD1B9A2C0EC58D00D4C01D254795C::get_offset_of_validity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (XmlSchemaKey_t606D00D96AD3852E929D286EBF25F22425DFCE37), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (XmlSchemaKeyref_t6EA2162743310431E999C40A1FC1878AD96333CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1709[2] = 
{
	XmlSchemaKeyref_t6EA2162743310431E999C40A1FC1878AD96333CD::get_offset_of_refer_21(),
	XmlSchemaKeyref_t6EA2162743310431E999C40A1FC1878AD96333CD::get_offset_of_target_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (XmlSchemaLengthFacet_t3830B4E2C1FC60F4D70ED12EE98322DECB29DADB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (XmlSchemaMaxExclusiveFacet_tEBF888C0BDC463B0C0EC907651F24FA917948C03), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (XmlSchemaMaxInclusiveFacet_tD6CCD809FC23EC06DE88B355BF245546486D56DD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (XmlSchemaMaxLengthFacet_tCA8010F52512E8C5989019422D228524E3A285E3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (XmlSchemaMinExclusiveFacet_t78C40AF49FF59C16947EE4C24F5DDA4EEA8DE1EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (XmlSchemaMinInclusiveFacet_t078A14BD8767AE5F6BAE256D2B586C7FED9C6DA7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (XmlSchemaMinLengthFacet_t8E13AD03D1FD3A9D21991323E264E3B4BA8A939B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (XmlSchemaNotation_t0A5D8512C01294894FF143DDB678782ECBF0C82D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[4] = 
{
	XmlSchemaNotation_t0A5D8512C01294894FF143DDB678782ECBF0C82D::get_offset_of_name_16(),
	XmlSchemaNotation_t0A5D8512C01294894FF143DDB678782ECBF0C82D::get_offset_of_pub_17(),
	XmlSchemaNotation_t0A5D8512C01294894FF143DDB678782ECBF0C82D::get_offset_of_system_18(),
	XmlSchemaNotation_t0A5D8512C01294894FF143DDB678782ECBF0C82D::get_offset_of_qualifiedName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (XmlSchemaNumericFacet_tC683FA814030977CB1A1D5C7421972B919022093), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[13] = 
{
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_lineNumber_0(),
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_linePosition_1(),
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_sourceUri_2(),
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_namespaces_3(),
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_unhandledAttributeList_4(),
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_isCompiled_5(),
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_errorCount_6(),
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_CompilationId_7(),
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_ValidationId_8(),
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_isRedefineChild_9(),
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_isRedefinedComponent_10(),
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_redefinedObject_11(),
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747::get_offset_of_parent_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (XmlSchemaObjectEnumerator_tFD0F223924987EA798B376639B07E158F1F009E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[1] = 
{
	XmlSchemaObjectEnumerator_tFD0F223924987EA798B376639B07E158F1F009E9::get_offset_of_ienum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1722[1] = 
{
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16::get_offset_of_table_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (XmlSchemaObjectTableEnumerator_t98D4DF6B98106D35E056E3A74981D1B96D577171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[2] = 
{
	XmlSchemaObjectTableEnumerator_t98D4DF6B98106D35E056E3A74981D1B96D577171::get_offset_of_xenum_0(),
	XmlSchemaObjectTableEnumerator_t98D4DF6B98106D35E056E3A74981D1B96D577171::get_offset_of_tmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC), -1, sizeof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1724[11] = 
{
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC::get_offset_of_minOccurs_16(),
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC::get_offset_of_maxOccurs_17(),
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC::get_offset_of_minstr_18(),
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC::get_offset_of_maxstr_19(),
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC_StaticFields::get_offset_of_empty_20(),
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC::get_offset_of_validatedMinOccurs_21(),
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC::get_offset_of_validatedMaxOccurs_22(),
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC::get_offset_of_recursionDepth_23(),
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC::get_offset_of_minEffectiveTotalRange_24(),
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC::get_offset_of_parentIsGroupDefinition_25(),
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC::get_offset_of_OptimizedParticle_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (EmptyParticle_t057637DE53E002545F1DD9A85A68E91CDE6DD069), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (XmlSchemaPatternFacet_t1B3D1C8F8D7DF14F25DC513CC73DA23F87F0F027), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (XmlSchemaRedefine_t7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[4] = 
{
	XmlSchemaRedefine_t7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC::get_offset_of_attributeGroups_16(),
	XmlSchemaRedefine_t7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC::get_offset_of_groups_17(),
	XmlSchemaRedefine_t7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC::get_offset_of_items_18(),
	XmlSchemaRedefine_t7E7411ADD2ABB8813BAF8A43DF3795A0D00114BC::get_offset_of_schemaTypes_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[12] = 
{
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D::get_offset_of_nameTable_0(),
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D::get_offset_of_xmlResolver_1(),
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D::get_offset_of_schemas_2(),
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D::get_offset_of_attributes_3(),
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D::get_offset_of_elements_4(),
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D::get_offset_of_types_5(),
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D::get_offset_of_idCollection_6(),
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D::get_offset_of_namedIdentities_7(),
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D::get_offset_of_settings_8(),
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D::get_offset_of_isCompiled_9(),
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D::get_offset_of_CompilationId_10(),
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D::get_offset_of_ValidationEventHandler_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[1] = 
{
	XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483::get_offset_of_items_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (XmlSchemaSimpleContent_tDF35C7E8421B617A50454FE566C8F58367307C2B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[1] = 
{
	XmlSchemaSimpleContent_tDF35C7E8421B617A50454FE566C8F58367307C2B::get_offset_of_content_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (XmlSchemaSimpleContentExtension_t9B356432170682F512C2C1765E2D2021E6BE4A09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[3] = 
{
	XmlSchemaSimpleContentExtension_t9B356432170682F512C2C1765E2D2021E6BE4A09::get_offset_of_any_17(),
	XmlSchemaSimpleContentExtension_t9B356432170682F512C2C1765E2D2021E6BE4A09::get_offset_of_attributes_18(),
	XmlSchemaSimpleContentExtension_t9B356432170682F512C2C1765E2D2021E6BE4A09::get_offset_of_baseTypeName_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (XmlSchemaSimpleContentRestriction_t0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[5] = 
{
	XmlSchemaSimpleContentRestriction_t0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB::get_offset_of_any_17(),
	XmlSchemaSimpleContentRestriction_t0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB::get_offset_of_attributes_18(),
	XmlSchemaSimpleContentRestriction_t0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB::get_offset_of_baseType_19(),
	XmlSchemaSimpleContentRestriction_t0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB::get_offset_of_baseTypeName_20(),
	XmlSchemaSimpleContentRestriction_t0EB5C75B34D430D2D5B581A71BB30D10B0ACAAFB::get_offset_of_facets_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908), -1, sizeof(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1733[54] = 
{
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_schemaLocationType_28(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908::get_offset_of_content_29(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908::get_offset_of_islocal_30(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908::get_offset_of_recursed_31(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908::get_offset_of_variety_32(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsAnySimpleType_33(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsString_34(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsBoolean_35(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsDecimal_36(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsFloat_37(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsDouble_38(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsDuration_39(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsDateTime_40(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsTime_41(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsDate_42(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsGYearMonth_43(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsGYear_44(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsGMonthDay_45(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsGDay_46(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsGMonth_47(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsHexBinary_48(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsBase64Binary_49(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsAnyUri_50(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsQName_51(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsNotation_52(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsNormalizedString_53(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsToken_54(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsLanguage_55(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsNMToken_56(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsNMTokens_57(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsName_58(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsNCName_59(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsID_60(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsIDRef_61(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsIDRefs_62(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsEntity_63(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsEntities_64(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsInteger_65(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsNonPositiveInteger_66(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsNegativeInteger_67(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsLong_68(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsInt_69(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsShort_70(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsByte_71(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsNonNegativeInteger_72(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsUnsignedLong_73(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsUnsignedInt_74(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsUnsignedShort_75(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsUnsignedByte_76(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XsPositiveInteger_77(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XdtUntypedAtomic_78(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XdtAnyAtomicType_79(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XdtYearMonthDuration_80(),
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908_StaticFields::get_offset_of_XdtDayTimeDuration_81(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (XmlSchemaSimpleTypeContent_t9BD29AE1058626894FD1F7EEAC14CD35BC3428EE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1734[1] = 
{
	XmlSchemaSimpleTypeContent_t9BD29AE1058626894FD1F7EEAC14CD35BC3428EE::get_offset_of_OwnerType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (XmlSchemaSimpleTypeList_tCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[4] = 
{
	XmlSchemaSimpleTypeList_tCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D::get_offset_of_itemType_17(),
	XmlSchemaSimpleTypeList_tCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D::get_offset_of_itemTypeName_18(),
	XmlSchemaSimpleTypeList_tCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D::get_offset_of_validatedListItemType_19(),
	XmlSchemaSimpleTypeList_tCA4A53E0AF6FF7A10DFF5513E8152F4E1767292D::get_offset_of_validatedListItemSchemaType_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600), -1, sizeof(XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1736[18] = 
{
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_baseType_17(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_baseTypeName_18(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_facets_19(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_enumarationFacetValues_20(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_patternFacetValues_21(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_rexPatterns_22(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_lengthFacet_23(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_maxLengthFacet_24(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_minLengthFacet_25(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_fractionDigitsFacet_26(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_totalDigitsFacet_27(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_maxInclusiveFacet_28(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_maxExclusiveFacet_29(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_minInclusiveFacet_30(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_minExclusiveFacet_31(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600::get_offset_of_fixedFacets_32(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600_StaticFields::get_offset_of_lengthStyle_33(),
	XmlSchemaSimpleTypeRestriction_t1C64CC105547BCA6817A9FE12D358DB5CDDEA600_StaticFields::get_offset_of_listFacets_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (XmlSchemaSimpleTypeUnion_t028C5EE22C3F4662E04D5D5A1A4804A94D7230A2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[4] = 
{
	XmlSchemaSimpleTypeUnion_t028C5EE22C3F4662E04D5D5A1A4804A94D7230A2::get_offset_of_baseTypes_17(),
	XmlSchemaSimpleTypeUnion_t028C5EE22C3F4662E04D5D5A1A4804A94D7230A2::get_offset_of_memberTypes_18(),
	XmlSchemaSimpleTypeUnion_t028C5EE22C3F4662E04D5D5A1A4804A94D7230A2::get_offset_of_validatedTypes_19(),
	XmlSchemaSimpleTypeUnion_t028C5EE22C3F4662E04D5D5A1A4804A94D7230A2::get_offset_of_validatedSchemaTypes_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (XmlSchemaTotalDigitsFacet_t5032254A794559D12A5C1079F013A62C937E45FF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE), -1, sizeof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1739[12] = 
{
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE::get_offset_of_final_16(),
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE::get_offset_of_isMixed_17(),
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE::get_offset_of_name_18(),
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE::get_offset_of_recursed_19(),
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE::get_offset_of_BaseSchemaTypeName_20(),
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE::get_offset_of_BaseXmlSchemaTypeInternal_21(),
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE::get_offset_of_DatatypeInternal_22(),
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE::get_offset_of_resolvedDerivedBy_23(),
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE::get_offset_of_finalResolved_24(),
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE::get_offset_of_QNameInternal_25(),
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE_StaticFields::get_offset_of_U3CU3Ef__switchU24map42_26(),
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE_StaticFields::get_offset_of_U3CU3Ef__switchU24map43_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (XmlSchemaUnique_tCDD8BDD09DC0F6A7DC89713300BACCB3E0DE47DB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (XmlSchemaUse_t74E0EE4116E212E01A0E2CBE17B468F5F5A728D8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1741[5] = 
{
	XmlSchemaUse_t74E0EE4116E212E01A0E2CBE17B468F5F5A728D8::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (XmlSchemaValidity_t602DD49FD94103C168EBCB02880FBE16C4B3D54B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1742[4] = 
{
	XmlSchemaValidity_t602DD49FD94103C168EBCB02880FBE16C4B3D54B::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (XmlSchemaValidationException_t7291A846B1FDCA169661064F2F1960FF1B5E29D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (XmlSchemaWhiteSpaceFacet_t18FF49A883F305B0AD4386AA5C38FA170242D69C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526), -1, sizeof(XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1745[6] = 
{
	XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526::get_offset_of_xpath_16(),
	XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526::get_offset_of_nsmgr_17(),
	XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526::get_offset_of_isSelector_18(),
	XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526::get_offset_of_compiledExpression_19(),
	XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526::get_offset_of_currentPath_20(),
	XmlSchemaXPath_t6CD59C385D5CEEC66D37A483111F039665C0A526_StaticFields::get_offset_of_U3CU3Ef__switchU24map4A_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (XmlSeverityType_t128BFAEA33288999198A6D82DF83832E1A2DA060)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1746[3] = 
{
	XmlSeverityType_t128BFAEA33288999198A6D82DF83832E1A2DA060::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (ValidationHandler_t2E35D69E882387E5F75A7D962A4BE4F31B79E909), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377), -1, sizeof(XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1748[10] = 
{
	XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields::get_offset_of_FinalAllowed_0(),
	XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields::get_offset_of_ElementBlockAllowed_1(),
	XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields::get_offset_of_ComplexTypeBlockAllowed_2(),
	XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields::get_offset_of_StrictMsCompliant_3(),
	XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields::get_offset_of_U3CU3Ef__switchU24map4B_4(),
	XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields::get_offset_of_U3CU3Ef__switchU24map4C_5(),
	XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields::get_offset_of_U3CU3Ef__switchU24map4D_6(),
	XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields::get_offset_of_U3CU3Ef__switchU24map4E_7(),
	XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields::get_offset_of_U3CU3Ef__switchU24map4F_8(),
	XmlSchemaUtil_tBC58C398A2D837AC4A0992CCAD07B223ADBE4377_StaticFields::get_offset_of_U3CU3Ef__switchU24map50_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (XmlSchemaReader_tB568152CC3CDCC0D93125CEC8BA1B48526548C9F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1749[3] = 
{
	XmlSchemaReader_tB568152CC3CDCC0D93125CEC8BA1B48526548C9F::get_offset_of_reader_2(),
	XmlSchemaReader_tB568152CC3CDCC0D93125CEC8BA1B48526548C9F::get_offset_of_handler_3(),
	XmlSchemaReader_tB568152CC3CDCC0D93125CEC8BA1B48526548C9F::get_offset_of_hasLineInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (XmlSchemaValidationFlags_tECE1A6138368A7455AC47A24FC5FD4920CDB8ACA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1750[7] = 
{
	XmlSchemaValidationFlags_tECE1A6138368A7455AC47A24FC5FD4920CDB8ACA::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (XmlTypeCode_t947DEC4BB4A883BC1EF9C796B8293EB10AB2B3A1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1751[56] = 
{
	XmlTypeCode_t947DEC4BB4A883BC1EF9C796B8293EB10AB2B3A1::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (CodeIdentifier_t271B589AAAA73A8C8F21B5C83FB25A77D1E12D27), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (SchemaTypes_t06F1E2A36C11AA3C6F9458923A5C423FE8EF6060)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1754[9] = 
{
	SchemaTypes_t06F1E2A36C11AA3C6F9458923A5C423FE8EF6060::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0), -1, sizeof(TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1755[12] = 
{
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0::get_offset_of_type_0(),
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0::get_offset_of_elementName_1(),
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0::get_offset_of_sType_2(),
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0::get_offset_of_listItemType_3(),
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0::get_offset_of_typeName_4(),
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0::get_offset_of_fullTypeName_5(),
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0::get_offset_of_listItemTypeData_6(),
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0::get_offset_of_mappedType_7(),
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0::get_offset_of_facet_8(),
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0::get_offset_of_hasPublicConstructor_9(),
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0::get_offset_of_nullableOverride_10(),
	TypeData_t1D7643AA75738523EEA8F9D5969BEB7C3D509BA0_StaticFields::get_offset_of_keywords_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (TypeTranslator_t09CD0ECBC589939001BCD6109B5DDC7E499B78F8), -1, sizeof(TypeTranslator_t09CD0ECBC589939001BCD6109B5DDC7E499B78F8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1756[4] = 
{
	TypeTranslator_t09CD0ECBC589939001BCD6109B5DDC7E499B78F8_StaticFields::get_offset_of_nameCache_0(),
	TypeTranslator_t09CD0ECBC589939001BCD6109B5DDC7E499B78F8_StaticFields::get_offset_of_primitiveTypes_1(),
	TypeTranslator_t09CD0ECBC589939001BCD6109B5DDC7E499B78F8_StaticFields::get_offset_of_primitiveArrayTypes_2(),
	TypeTranslator_t09CD0ECBC589939001BCD6109B5DDC7E499B78F8_StaticFields::get_offset_of_nullableTypes_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (XmlAnyAttributeAttribute_t2F5EDE6F0E37829D42BDFB90007730B3903893B3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (XmlAnyElementAttribute_tFA4AE6E799FD8B3E5BF87E96219A5EBB1E5BA99E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[1] = 
{
	XmlAnyElementAttribute_tFA4AE6E799FD8B3E5BF87E96219A5EBB1E5BA99E::get_offset_of_order_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (XmlAttributeAttribute_tE67EC29BD9EDACC25B99CDE09E045A5BC09096FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[2] = 
{
	XmlAttributeAttribute_tE67EC29BD9EDACC25B99CDE09E045A5BC09096FE::get_offset_of_attributeName_0(),
	XmlAttributeAttribute_tE67EC29BD9EDACC25B99CDE09E045A5BC09096FE::get_offset_of_dataType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (XmlElementAttribute_t92C0D894D9E76C38B59E1D573EC9F6AF80494B5E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[3] = 
{
	XmlElementAttribute_t92C0D894D9E76C38B59E1D573EC9F6AF80494B5E::get_offset_of_elementName_0(),
	XmlElementAttribute_t92C0D894D9E76C38B59E1D573EC9F6AF80494B5E::get_offset_of_type_1(),
	XmlElementAttribute_t92C0D894D9E76C38B59E1D573EC9F6AF80494B5E::get_offset_of_order_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (XmlEnumAttribute_t13159B8D8CA3F8FA0319C49762A6FBDA37F4E763), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[1] = 
{
	XmlEnumAttribute_t13159B8D8CA3F8FA0319C49762A6FBDA37F4E763::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (XmlIgnoreAttribute_tEA0415D4AC0E0C702B55CAB899E8339F1AF60BE1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (XmlNamespaceDeclarationsAttribute_tFDF5AA2717A634FDB6D2E82CA7E52785726931D0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (XmlRootAttribute_t32A9E7F45477EB665B77943F34F4471F0169457C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[3] = 
{
	XmlRootAttribute_t32A9E7F45477EB665B77943F34F4471F0169457C::get_offset_of_elementName_0(),
	XmlRootAttribute_t32A9E7F45477EB665B77943F34F4471F0169457C::get_offset_of_isNullable_1(),
	XmlRootAttribute_t32A9E7F45477EB665B77943F34F4471F0169457C::get_offset_of_ns_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[1] = 
{
	XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3::get_offset_of_namespaces_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (XmlTextAttribute_t4AB1AF51B935E1358C3461C709E14D3F0DF0099F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4), -1, sizeof(U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1769[8] = 
{
	U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields::get_offset_of_U24U24fieldU2D36_0(),
	U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields::get_offset_of_U24U24fieldU2D37_1(),
	U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields::get_offset_of_U24U24fieldU2D38_2(),
	U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields::get_offset_of_U24U24fieldU2D39_3(),
	U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields::get_offset_of_U24U24fieldU2D40_4(),
	U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields::get_offset_of_U24U24fieldU2D41_5(),
	U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields::get_offset_of_U24U24fieldU2D43_6(),
	U3CPrivateImplementationDetailsU3E_tABCDEB03D02815D5F6F33FF21BBF0880559C9DA4_StaticFields::get_offset_of_U24U24fieldU2D44_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (U24ArrayTypeU2412_t240C25AFE6D004BFFB06A982F7E42749780F8A23)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2412_t240C25AFE6D004BFFB06A982F7E42749780F8A23 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU248_t46D59E10C7F0BFA066E0D741BF43D9C65C9F0CB8 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (U24ArrayTypeU24256_t6264193FFFEE81B3F24E58F2F58D3CCFD2957611)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU24256_t6264193FFFEE81B3F24E58F2F58D3CCFD2957611 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (U24ArrayTypeU241280_t07BD843025ECC2676C90DA85D9C7D398AA8A6DBA)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU241280_t07BD843025ECC2676C90DA85D9C7D398AA8A6DBA ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (U3CModuleU3E_tC4DF359A31728089F9FBC88966828166FB750DFD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[3] = 
{
	ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380::get_offset_of_alreadyInitialized_0(),
	ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380::get_offset_of__description_1(),
	ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380::get_offset_of__name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[3] = 
{
	ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386::get_offset_of_lookup_0(),
	ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386::get_offset_of_readOnly_1(),
	ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386::get_offset_of_values_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (ClientConfigurationSystem_tF9A420DBA500361EBA6B5F50B8C329DC560FD58B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1783[1] = 
{
	ClientConfigurationSystem_tF9A420DBA500361EBA6B5F50B8C329DC560FD58B::get_offset_of_cfg_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (ConfigNameValueCollection_t6BC2AF313208E6493F818D04E62F2C4ECE42A5CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1784[1] = 
{
	ConfigNameValueCollection_t6BC2AF313208E6493F818D04E62F2C4ECE42A5CA::get_offset_of_modified_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1785[6] = 
{
	ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698::get_offset_of_Name_0(),
	ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698::get_offset_of_TypeName_1(),
	ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698::get_offset_of_Type_2(),
	ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698::get_offset_of_streamName_3(),
	ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698::get_offset_of_Parent_4(),
	ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698::get_offset_of_ConfigHost_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1786[12] = 
{
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01::get_offset_of_parent_0(),
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01::get_offset_of_elementData_1(),
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01::get_offset_of_streamName_2(),
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01::get_offset_of_rootSectionGroup_3(),
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01::get_offset_of_locations_4(),
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01::get_offset_of_rootGroup_5(),
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01::get_offset_of_system_6(),
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01::get_offset_of_hasFile_7(),
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01::get_offset_of_rootNamespace_8(),
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01::get_offset_of_configPath_9(),
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01::get_offset_of_locationConfigPath_10(),
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01::get_offset_of_locationSubPath_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (ConfigurationAllowDefinition_t18B6E25650B50173DD5DFB01BCF332724756329D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1787[5] = 
{
	ConfigurationAllowDefinition_t18B6E25650B50173DD5DFB01BCF332724756329D::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (ConfigurationAllowExeDefinition_t66ED2E212F6E3FCB79A1A28752B982FA68AED8BC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1788[5] = 
{
	ConfigurationAllowExeDefinition_t66ED2E212F6E3FCB79A1A28752B982FA68AED8BC::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1789[5] = 
{
	ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318::get_offset_of_addItemName_0(),
	ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318::get_offset_of_clearItemsName_1(),
	ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318::get_offset_of_removeItemName_2(),
	ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318::get_offset_of_collectionType_3(),
	ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318::get_offset_of_itemType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1790[13] = 
{
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of_rawXml_0(),
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of_modified_1(),
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of_map_2(),
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of_keyProps_3(),
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of_defaultCollection_4(),
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of_readOnly_5(),
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of_elementInfo_6(),
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of__configuration_7(),
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of_lockAllAttributesExcept_8(),
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of_lockAllElementsExcept_9(),
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of_lockAttributes_10(),
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of_lockElements_11(),
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63::get_offset_of_lockItem_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC), -1, sizeof(ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1791[3] = 
{
	ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC_StaticFields::get_offset_of_elementMaps_0(),
	ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC::get_offset_of_properties_1(),
	ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC::get_offset_of_collectionAttribute_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[10] = 
{
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E::get_offset_of_list_13(),
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E::get_offset_of_removed_14(),
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E::get_offset_of_inherited_15(),
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E::get_offset_of_emitClear_16(),
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E::get_offset_of_modified_17(),
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E::get_offset_of_comparer_18(),
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E::get_offset_of_inheritedLimitIndex_19(),
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E::get_offset_of_addElementName_20(),
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E::get_offset_of_clearElementName_21(),
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E::get_offset_of_removeElementName_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (ConfigurationRemoveElement_t577415495CA1304AB82547789A48AE35FFE325CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1793[3] = 
{
	ConfigurationRemoveElement_t577415495CA1304AB82547789A48AE35FFE325CD::get_offset_of_properties_13(),
	ConfigurationRemoveElement_t577415495CA1304AB82547789A48AE35FFE325CD::get_offset_of__origElement_14(),
	ConfigurationRemoveElement_t577415495CA1304AB82547789A48AE35FFE325CD::get_offset_of__origCollection_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (ConfigurationElementCollectionType_t164CCC3030B723DE6437FA5DC7B9D21F49D5A8BE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1794[5] = 
{
	ConfigurationElementCollectionType_t164CCC3030B723DE6437FA5DC7B9D21F49D5A8BE::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (ConfigurationErrorsException_tD1E3755B5D234D30892B41886D8515DCA40199F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[2] = 
{
	ConfigurationErrorsException_tD1E3755B5D234D30892B41886D8515DCA40199F8::get_offset_of_filename_13(),
	ConfigurationErrorsException_tD1E3755B5D234D30892B41886D8515DCA40199F8::get_offset_of_line_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (ConfigurationFileMap_tE596BD02408620847DC8AA9D93423B610AB224F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[1] = 
{
	ConfigurationFileMap_tE596BD02408620847DC8AA9D93423B610AB224F0::get_offset_of_machineConfigFilename_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84), -1, sizeof(ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1797[7] = 
{
	ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84_StaticFields::get_offset_of_pathTrimChars_0(),
	ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84::get_offset_of_path_1(),
	ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84::get_offset_of_configuration_2(),
	ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84::get_offset_of_parent_3(),
	ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84::get_offset_of_xmlContent_4(),
	ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84::get_offset_of_parentResolved_5(),
	ConfigurationLocation_t1D40B1D62EAFBBB2FA8ACF23596B350693267C84::get_offset_of_allowOverride_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (ConfigurationLocationCollection_t8FA9CDDF11231DCA26928D9575D87A679C45B65A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (ConfigurationLockType_tFAC681894C188BA192647A4170910E9D1F078534)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1799[4] = 
{
	ConfigurationLockType_tFAC681894C188BA192647A4170910E9D1F078534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif

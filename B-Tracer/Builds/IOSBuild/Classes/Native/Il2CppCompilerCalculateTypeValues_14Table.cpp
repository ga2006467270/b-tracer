﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mono.Xml.DTDAutomata
struct DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E;
// Mono.Xml.IHasXmlSchemaInfo
struct IHasXmlSchemaInfo_tA3B2C478D5F5102CB79A687B2F03ED13BC529461;
// Mono.Xml.Schema.XsdAnySimpleType
struct XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0;
// Mono.Xml.Schema.XsdIDManager
struct XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E;
// Mono.Xml.Schema.XsdIdentityField
struct XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7;
// Mono.Xml.Schema.XsdIdentityField[]
struct XsdIdentityFieldU5BU5D_tFC0450B63854F4164FF29197CBCB165727475092;
// Mono.Xml.Schema.XsdIdentityPath
struct XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE;
// Mono.Xml.Schema.XsdIdentityPath[]
struct XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F;
// Mono.Xml.Schema.XsdIdentitySelector
struct XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8;
// Mono.Xml.Schema.XsdIdentityStep[]
struct XsdIdentityStepU5BU5D_t86F66436C951723412A544B608105218C570B61C;
// Mono.Xml.Schema.XsdInvalidValidationState
struct XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF;
// Mono.Xml.Schema.XsdKeyEntry
struct XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3;
// Mono.Xml.Schema.XsdKeyEntryCollection
struct XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265;
// Mono.Xml.Schema.XsdKeyEntryFieldCollection
struct XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC;
// Mono.Xml.Schema.XsdKeyTable
struct XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6;
// Mono.Xml.Schema.XsdParticleStateManager
struct XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68;
// Mono.Xml.Schema.XsdValidationContext
struct XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD;
// Mono.Xml.Schema.XsdValidationState
struct XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74;
// System.AsyncCallback
struct AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2;
// System.Byte[]
struct ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.ArrayList
struct ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F;
// System.Collections.BitArray
struct BitArray_t3C48B44FE42C242A7A85C6C32E91BFA25463358D;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1;
// System.Collections.Hashtable
struct Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82;
// System.Collections.IDictionary
struct IDictionary_tD35B9437F08BE98D1E0B295CC73C48E168CAB316;
// System.Collections.IList
struct IList_t1CFF887D6A6212B2F35969C95D9FB19749BA8580;
// System.Collections.Specialized.StringCollection
struct StringCollection_t1921D1DDEF33942CB6DF73338C52ECD8C6ABC5E9;
// System.Collections.Stack
struct Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5;
// System.DelegateData
struct DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9;
// System.IAsyncResult
struct IAsyncResult_tDA33C24465239FB383C4C2CDAAC43B9AD3CB7F05;
// System.Int32[]
struct Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074;
// System.IntPtr[]
struct IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659;
// System.Net.IPEndPoint
struct IPEndPoint_tE99679FFDD0135AA3881DF00523DD9D6BE7E6622;
// System.Net.ServicePoint
struct ServicePoint_tE7D15B7B032D02714D4271B226BB921E2B32D1B3;
// System.Net.WebHeaderCollection
struct WebHeaderCollection_t01C9818A1AB6381C83A59AD104E8F0092AC87B66;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_tC6D94A8A63C0640494BE76372C6E4C291D2826DE;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t4326FD1F39792B6FD659D19F8647F52BE2BBDDC7;
// System.Security.Cryptography.X509Certificates.X509Chain
struct X509Chain_t3C6670F9D7A771364E5CF47A6DA3EFB4A029CD56;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B;
// System.Text.RegularExpressions.CaptureCollection
struct CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20;
// System.Text.RegularExpressions.EvalDelegate
struct EvalDelegate_tEAE36D514400FE58E4761FEDBB29920820C4B2FB;
// System.Text.RegularExpressions.FactoryCache
struct FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9;
// System.Text.RegularExpressions.GroupCollection
struct GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968;
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_tD5F0C5F00520A3213D8E6F225B1CF51985D2ACFE;
// System.Text.RegularExpressions.IMachine
struct IMachine_tB6A67B7F7CBCEBFB1EE3CDF0D00B88CD8443A776;
// System.Text.RegularExpressions.IMachineFactory
struct IMachineFactory_t44F4CC962A53B1F37D93579205124A16FD0B1471;
// System.Text.RegularExpressions.Interpreter/RepeatContext
struct RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938;
// System.Text.RegularExpressions.IntervalCollection
struct IntervalCollection_t33C1E9164A6B24465B908CB2D90E23C9F60F7DD5;
// System.Text.RegularExpressions.Mark[]
struct MarkU5BU5D_t528ADB54CF1308C521A7235B7686FF22BF928765;
// System.Text.RegularExpressions.Match
struct Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53;
// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E;
// System.Text.RegularExpressions.QuickSearch
struct QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849;
// System.Text.RegularExpressions.Regex
struct Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B;
// System.Text.RegularExpressions.RegexRunnerFactory
struct RegexRunnerFactory_tF345EBFE2597908D47A0A7C0F4D7194718DACF61;
// System.Text.RegularExpressions.RxInterpreter
struct RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740;
// System.Text.RegularExpressions.RxInterpreter/RepeatContext
struct RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622;
// System.Text.RegularExpressions.Syntax.CapturingGroup
struct CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7;
// System.Text.RegularExpressions.Syntax.Expression
struct Expression_t6A43E18B89A25A32D44C3FD396F6139A8AA73F94;
// System.Text.RegularExpressions.Syntax.ExpressionAssertion
struct ExpressionAssertion_t3094472FB37123ECF6BD025105AE4FC207E36153;
// System.Text.RegularExpressions.Syntax.ExpressionCollection
struct ExpressionCollection_tCA7FC699936CBAE10FB637D8927714CD3AEA282A;
// System.Text.RegularExpressions.Syntax.Literal
struct Literal_tC4E6C9861C7A338A35934AA8ECC758AC63DC587A;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.UInt16[]
struct UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC;
// System.Uri
struct Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E;
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6;
// System.UriParser
struct UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_t4DFB3B0D35B53EFBE9B6D120BEA11AD88DA2F3F3;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B;
// System.Xml.Schema.XmlSchemaAll
struct XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB;
// System.Xml.Schema.XmlSchemaAny
struct XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF;
// System.Xml.Schema.XmlSchemaAttribute[]
struct XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95;
// System.Xml.Schema.XmlSchemaChoice
struct XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64;
// System.Xml.Schema.XmlSchemaIdentityConstraint
struct XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747;
// System.Xml.Schema.XmlSchemaSequence
struct XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004;
// System.Xml.XmlReader
struct XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB;
// System.Xml.XmlResolver
struct XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C;




#ifndef U3CMODULEU3E_T59A29D11DE086BC643857AFBADB05C659B348470_H
#define U3CMODULEU3E_T59A29D11DE086BC643857AFBADB05C659B348470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t59A29D11DE086BC643857AFBADB05C659B348470 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T59A29D11DE086BC643857AFBADB05C659B348470_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DTDAUTOMATA_T831152C2924A60A794E81D920F5504585B94BFE7_H
#define DTDAUTOMATA_T831152C2924A60A794E81D920F5504585B94BFE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAutomata
struct  DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDAutomata::root
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * ___root_0;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7, ___root_0)); }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDAUTOMATA_T831152C2924A60A794E81D920F5504585B94BFE7_H
#ifndef DTDAUTOMATAFACTORY_T8081E28AEC61216CB0502E9C8584C09D7A13F340_H
#define DTDAUTOMATAFACTORY_T8081E28AEC61216CB0502E9C8584C09D7A13F340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAutomataFactory
struct  DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDAutomataFactory::root
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * ___root_0;
	// System.Collections.Hashtable Mono.Xml.DTDAutomataFactory::choiceTable
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___choiceTable_1;
	// System.Collections.Hashtable Mono.Xml.DTDAutomataFactory::sequenceTable
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___sequenceTable_2;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340, ___root_0)); }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_choiceTable_1() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340, ___choiceTable_1)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_choiceTable_1() const { return ___choiceTable_1; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_choiceTable_1() { return &___choiceTable_1; }
	inline void set_choiceTable_1(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___choiceTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___choiceTable_1), value);
	}

	inline static int32_t get_offset_of_sequenceTable_2() { return static_cast<int32_t>(offsetof(DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340, ___sequenceTable_2)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_sequenceTable_2() const { return ___sequenceTable_2; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_sequenceTable_2() { return &___sequenceTable_2; }
	inline void set_sequenceTable_2(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___sequenceTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceTable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDAUTOMATAFACTORY_T8081E28AEC61216CB0502E9C8584C09D7A13F340_H
#ifndef XSDIDMANAGER_T1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E_H
#define XSDIDMANAGER_T1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIDManager
struct  XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.Schema.XsdIDManager::idList
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___idList_0;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdIDManager::missingIDReferences
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___missingIDReferences_1;
	// System.String Mono.Xml.Schema.XsdIDManager::thisElementId
	String_t* ___thisElementId_2;

public:
	inline static int32_t get_offset_of_idList_0() { return static_cast<int32_t>(offsetof(XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E, ___idList_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_idList_0() const { return ___idList_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_idList_0() { return &___idList_0; }
	inline void set_idList_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___idList_0 = value;
		Il2CppCodeGenWriteBarrier((&___idList_0), value);
	}

	inline static int32_t get_offset_of_missingIDReferences_1() { return static_cast<int32_t>(offsetof(XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E, ___missingIDReferences_1)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_missingIDReferences_1() const { return ___missingIDReferences_1; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_missingIDReferences_1() { return &___missingIDReferences_1; }
	inline void set_missingIDReferences_1(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___missingIDReferences_1 = value;
		Il2CppCodeGenWriteBarrier((&___missingIDReferences_1), value);
	}

	inline static int32_t get_offset_of_thisElementId_2() { return static_cast<int32_t>(offsetof(XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E, ___thisElementId_2)); }
	inline String_t* get_thisElementId_2() const { return ___thisElementId_2; }
	inline String_t** get_address_of_thisElementId_2() { return &___thisElementId_2; }
	inline void set_thisElementId_2(String_t* value)
	{
		___thisElementId_2 = value;
		Il2CppCodeGenWriteBarrier((&___thisElementId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDMANAGER_T1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E_H
#ifndef XSDIDENTITYFIELD_T47467B5E2857C75A53026E207DC4063868D49DC7_H
#define XSDIDENTITYFIELD_T47467B5E2857C75A53026E207DC4063868D49DC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentityField
struct  XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdIdentityPath[] Mono.Xml.Schema.XsdIdentityField::fieldPaths
	XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* ___fieldPaths_0;
	// System.Int32 Mono.Xml.Schema.XsdIdentityField::index
	int32_t ___index_1;

public:
	inline static int32_t get_offset_of_fieldPaths_0() { return static_cast<int32_t>(offsetof(XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7, ___fieldPaths_0)); }
	inline XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* get_fieldPaths_0() const { return ___fieldPaths_0; }
	inline XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F** get_address_of_fieldPaths_0() { return &___fieldPaths_0; }
	inline void set_fieldPaths_0(XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* value)
	{
		___fieldPaths_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldPaths_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYFIELD_T47467B5E2857C75A53026E207DC4063868D49DC7_H
#ifndef XSDIDENTITYPATH_TF37220F64B3F2E405306A9813980F5F02A788BFE_H
#define XSDIDENTITYPATH_TF37220F64B3F2E405306A9813980F5F02A788BFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentityPath
struct  XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdIdentityStep[] Mono.Xml.Schema.XsdIdentityPath::OrderedSteps
	XsdIdentityStepU5BU5D_t86F66436C951723412A544B608105218C570B61C* ___OrderedSteps_0;
	// System.Boolean Mono.Xml.Schema.XsdIdentityPath::Descendants
	bool ___Descendants_1;

public:
	inline static int32_t get_offset_of_OrderedSteps_0() { return static_cast<int32_t>(offsetof(XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE, ___OrderedSteps_0)); }
	inline XsdIdentityStepU5BU5D_t86F66436C951723412A544B608105218C570B61C* get_OrderedSteps_0() const { return ___OrderedSteps_0; }
	inline XsdIdentityStepU5BU5D_t86F66436C951723412A544B608105218C570B61C** get_address_of_OrderedSteps_0() { return &___OrderedSteps_0; }
	inline void set_OrderedSteps_0(XsdIdentityStepU5BU5D_t86F66436C951723412A544B608105218C570B61C* value)
	{
		___OrderedSteps_0 = value;
		Il2CppCodeGenWriteBarrier((&___OrderedSteps_0), value);
	}

	inline static int32_t get_offset_of_Descendants_1() { return static_cast<int32_t>(offsetof(XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE, ___Descendants_1)); }
	inline bool get_Descendants_1() const { return ___Descendants_1; }
	inline bool* get_address_of_Descendants_1() { return &___Descendants_1; }
	inline void set_Descendants_1(bool value)
	{
		___Descendants_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYPATH_TF37220F64B3F2E405306A9813980F5F02A788BFE_H
#ifndef XSDIDENTITYSELECTOR_T6ABC7388F5B0EECB1374E1B6137E2ACC506699D8_H
#define XSDIDENTITYSELECTOR_T6ABC7388F5B0EECB1374E1B6137E2ACC506699D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentitySelector
struct  XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdIdentityPath[] Mono.Xml.Schema.XsdIdentitySelector::selectorPaths
	XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* ___selectorPaths_0;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdIdentitySelector::fields
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___fields_1;
	// Mono.Xml.Schema.XsdIdentityField[] Mono.Xml.Schema.XsdIdentitySelector::cachedFields
	XsdIdentityFieldU5BU5D_tFC0450B63854F4164FF29197CBCB165727475092* ___cachedFields_2;

public:
	inline static int32_t get_offset_of_selectorPaths_0() { return static_cast<int32_t>(offsetof(XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8, ___selectorPaths_0)); }
	inline XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* get_selectorPaths_0() const { return ___selectorPaths_0; }
	inline XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F** get_address_of_selectorPaths_0() { return &___selectorPaths_0; }
	inline void set_selectorPaths_0(XsdIdentityPathU5BU5D_tF8744EE69E53D965E12925A86B3E073CCA04DD2F* value)
	{
		___selectorPaths_0 = value;
		Il2CppCodeGenWriteBarrier((&___selectorPaths_0), value);
	}

	inline static int32_t get_offset_of_fields_1() { return static_cast<int32_t>(offsetof(XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8, ___fields_1)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_fields_1() const { return ___fields_1; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_fields_1() { return &___fields_1; }
	inline void set_fields_1(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___fields_1 = value;
		Il2CppCodeGenWriteBarrier((&___fields_1), value);
	}

	inline static int32_t get_offset_of_cachedFields_2() { return static_cast<int32_t>(offsetof(XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8, ___cachedFields_2)); }
	inline XsdIdentityFieldU5BU5D_tFC0450B63854F4164FF29197CBCB165727475092* get_cachedFields_2() const { return ___cachedFields_2; }
	inline XsdIdentityFieldU5BU5D_tFC0450B63854F4164FF29197CBCB165727475092** get_address_of_cachedFields_2() { return &___cachedFields_2; }
	inline void set_cachedFields_2(XsdIdentityFieldU5BU5D_tFC0450B63854F4164FF29197CBCB165727475092* value)
	{
		___cachedFields_2 = value;
		Il2CppCodeGenWriteBarrier((&___cachedFields_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYSELECTOR_T6ABC7388F5B0EECB1374E1B6137E2ACC506699D8_H
#ifndef XSDIDENTITYSTEP_T870B49AFC712ECC1798E341EFD170C1B841ED48D_H
#define XSDIDENTITYSTEP_T870B49AFC712ECC1798E341EFD170C1B841ED48D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIdentityStep
struct  XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D  : public RuntimeObject
{
public:
	// System.Boolean Mono.Xml.Schema.XsdIdentityStep::IsCurrent
	bool ___IsCurrent_0;
	// System.Boolean Mono.Xml.Schema.XsdIdentityStep::IsAttribute
	bool ___IsAttribute_1;
	// System.Boolean Mono.Xml.Schema.XsdIdentityStep::IsAnyName
	bool ___IsAnyName_2;
	// System.String Mono.Xml.Schema.XsdIdentityStep::NsName
	String_t* ___NsName_3;
	// System.String Mono.Xml.Schema.XsdIdentityStep::Name
	String_t* ___Name_4;
	// System.String Mono.Xml.Schema.XsdIdentityStep::Namespace
	String_t* ___Namespace_5;

public:
	inline static int32_t get_offset_of_IsCurrent_0() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D, ___IsCurrent_0)); }
	inline bool get_IsCurrent_0() const { return ___IsCurrent_0; }
	inline bool* get_address_of_IsCurrent_0() { return &___IsCurrent_0; }
	inline void set_IsCurrent_0(bool value)
	{
		___IsCurrent_0 = value;
	}

	inline static int32_t get_offset_of_IsAttribute_1() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D, ___IsAttribute_1)); }
	inline bool get_IsAttribute_1() const { return ___IsAttribute_1; }
	inline bool* get_address_of_IsAttribute_1() { return &___IsAttribute_1; }
	inline void set_IsAttribute_1(bool value)
	{
		___IsAttribute_1 = value;
	}

	inline static int32_t get_offset_of_IsAnyName_2() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D, ___IsAnyName_2)); }
	inline bool get_IsAnyName_2() const { return ___IsAnyName_2; }
	inline bool* get_address_of_IsAnyName_2() { return &___IsAnyName_2; }
	inline void set_IsAnyName_2(bool value)
	{
		___IsAnyName_2 = value;
	}

	inline static int32_t get_offset_of_NsName_3() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D, ___NsName_3)); }
	inline String_t* get_NsName_3() const { return ___NsName_3; }
	inline String_t** get_address_of_NsName_3() { return &___NsName_3; }
	inline void set_NsName_3(String_t* value)
	{
		___NsName_3 = value;
		Il2CppCodeGenWriteBarrier((&___NsName_3), value);
	}

	inline static int32_t get_offset_of_Name_4() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D, ___Name_4)); }
	inline String_t* get_Name_4() const { return ___Name_4; }
	inline String_t** get_address_of_Name_4() { return &___Name_4; }
	inline void set_Name_4(String_t* value)
	{
		___Name_4 = value;
		Il2CppCodeGenWriteBarrier((&___Name_4), value);
	}

	inline static int32_t get_offset_of_Namespace_5() { return static_cast<int32_t>(offsetof(XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D, ___Namespace_5)); }
	inline String_t* get_Namespace_5() const { return ___Namespace_5; }
	inline String_t** get_address_of_Namespace_5() { return &___Namespace_5; }
	inline void set_Namespace_5(String_t* value)
	{
		___Namespace_5 = value;
		Il2CppCodeGenWriteBarrier((&___Namespace_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDENTITYSTEP_T870B49AFC712ECC1798E341EFD170C1B841ED48D_H
#ifndef XSDKEYENTRY_T1A916C808B855D066198566E0DD1D93B3C8722B3_H
#define XSDKEYENTRY_T1A916C808B855D066198566E0DD1D93B3C8722B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntry
struct  XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3  : public RuntimeObject
{
public:
	// System.Int32 Mono.Xml.Schema.XsdKeyEntry::StartDepth
	int32_t ___StartDepth_0;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntry::SelectorLineNumber
	int32_t ___SelectorLineNumber_1;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntry::SelectorLinePosition
	int32_t ___SelectorLinePosition_2;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntry::SelectorHasLineInfo
	bool ___SelectorHasLineInfo_3;
	// Mono.Xml.Schema.XsdKeyEntryFieldCollection Mono.Xml.Schema.XsdKeyEntry::KeyFields
	XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC * ___KeyFields_4;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntry::KeyRefFound
	bool ___KeyRefFound_5;
	// Mono.Xml.Schema.XsdKeyTable Mono.Xml.Schema.XsdKeyEntry::OwnerSequence
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 * ___OwnerSequence_6;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntry::keyFound
	bool ___keyFound_7;

public:
	inline static int32_t get_offset_of_StartDepth_0() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___StartDepth_0)); }
	inline int32_t get_StartDepth_0() const { return ___StartDepth_0; }
	inline int32_t* get_address_of_StartDepth_0() { return &___StartDepth_0; }
	inline void set_StartDepth_0(int32_t value)
	{
		___StartDepth_0 = value;
	}

	inline static int32_t get_offset_of_SelectorLineNumber_1() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___SelectorLineNumber_1)); }
	inline int32_t get_SelectorLineNumber_1() const { return ___SelectorLineNumber_1; }
	inline int32_t* get_address_of_SelectorLineNumber_1() { return &___SelectorLineNumber_1; }
	inline void set_SelectorLineNumber_1(int32_t value)
	{
		___SelectorLineNumber_1 = value;
	}

	inline static int32_t get_offset_of_SelectorLinePosition_2() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___SelectorLinePosition_2)); }
	inline int32_t get_SelectorLinePosition_2() const { return ___SelectorLinePosition_2; }
	inline int32_t* get_address_of_SelectorLinePosition_2() { return &___SelectorLinePosition_2; }
	inline void set_SelectorLinePosition_2(int32_t value)
	{
		___SelectorLinePosition_2 = value;
	}

	inline static int32_t get_offset_of_SelectorHasLineInfo_3() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___SelectorHasLineInfo_3)); }
	inline bool get_SelectorHasLineInfo_3() const { return ___SelectorHasLineInfo_3; }
	inline bool* get_address_of_SelectorHasLineInfo_3() { return &___SelectorHasLineInfo_3; }
	inline void set_SelectorHasLineInfo_3(bool value)
	{
		___SelectorHasLineInfo_3 = value;
	}

	inline static int32_t get_offset_of_KeyFields_4() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___KeyFields_4)); }
	inline XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC * get_KeyFields_4() const { return ___KeyFields_4; }
	inline XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC ** get_address_of_KeyFields_4() { return &___KeyFields_4; }
	inline void set_KeyFields_4(XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC * value)
	{
		___KeyFields_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyFields_4), value);
	}

	inline static int32_t get_offset_of_KeyRefFound_5() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___KeyRefFound_5)); }
	inline bool get_KeyRefFound_5() const { return ___KeyRefFound_5; }
	inline bool* get_address_of_KeyRefFound_5() { return &___KeyRefFound_5; }
	inline void set_KeyRefFound_5(bool value)
	{
		___KeyRefFound_5 = value;
	}

	inline static int32_t get_offset_of_OwnerSequence_6() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___OwnerSequence_6)); }
	inline XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 * get_OwnerSequence_6() const { return ___OwnerSequence_6; }
	inline XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 ** get_address_of_OwnerSequence_6() { return &___OwnerSequence_6; }
	inline void set_OwnerSequence_6(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 * value)
	{
		___OwnerSequence_6 = value;
		Il2CppCodeGenWriteBarrier((&___OwnerSequence_6), value);
	}

	inline static int32_t get_offset_of_keyFound_7() { return static_cast<int32_t>(offsetof(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3, ___keyFound_7)); }
	inline bool get_keyFound_7() const { return ___keyFound_7; }
	inline bool* get_address_of_keyFound_7() { return &___keyFound_7; }
	inline void set_keyFound_7(bool value)
	{
		___keyFound_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRY_T1A916C808B855D066198566E0DD1D93B3C8722B3_H
#ifndef XSDKEYENTRYFIELD_T46A4E7087BCB9D206208679EA739175225D551D2_H
#define XSDKEYENTRYFIELD_T46A4E7087BCB9D206208679EA739175225D551D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntryField
struct  XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdKeyEntry Mono.Xml.Schema.XsdKeyEntryField::entry
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3 * ___entry_0;
	// Mono.Xml.Schema.XsdIdentityField Mono.Xml.Schema.XsdKeyEntryField::field
	XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7 * ___field_1;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::FieldFound
	bool ___FieldFound_2;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntryField::FieldLineNumber
	int32_t ___FieldLineNumber_3;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntryField::FieldLinePosition
	int32_t ___FieldLinePosition_4;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::FieldHasLineInfo
	bool ___FieldHasLineInfo_5;
	// Mono.Xml.Schema.XsdAnySimpleType Mono.Xml.Schema.XsdKeyEntryField::FieldType
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * ___FieldType_6;
	// System.Object Mono.Xml.Schema.XsdKeyEntryField::Identity
	RuntimeObject * ___Identity_7;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::IsXsiNil
	bool ___IsXsiNil_8;
	// System.Int32 Mono.Xml.Schema.XsdKeyEntryField::FieldFoundDepth
	int32_t ___FieldFoundDepth_9;
	// Mono.Xml.Schema.XsdIdentityPath Mono.Xml.Schema.XsdKeyEntryField::FieldFoundPath
	XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE * ___FieldFoundPath_10;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::Consuming
	bool ___Consuming_11;
	// System.Boolean Mono.Xml.Schema.XsdKeyEntryField::Consumed
	bool ___Consumed_12;

public:
	inline static int32_t get_offset_of_entry_0() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___entry_0)); }
	inline XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3 * get_entry_0() const { return ___entry_0; }
	inline XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3 ** get_address_of_entry_0() { return &___entry_0; }
	inline void set_entry_0(XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3 * value)
	{
		___entry_0 = value;
		Il2CppCodeGenWriteBarrier((&___entry_0), value);
	}

	inline static int32_t get_offset_of_field_1() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___field_1)); }
	inline XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7 * get_field_1() const { return ___field_1; }
	inline XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7 ** get_address_of_field_1() { return &___field_1; }
	inline void set_field_1(XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7 * value)
	{
		___field_1 = value;
		Il2CppCodeGenWriteBarrier((&___field_1), value);
	}

	inline static int32_t get_offset_of_FieldFound_2() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldFound_2)); }
	inline bool get_FieldFound_2() const { return ___FieldFound_2; }
	inline bool* get_address_of_FieldFound_2() { return &___FieldFound_2; }
	inline void set_FieldFound_2(bool value)
	{
		___FieldFound_2 = value;
	}

	inline static int32_t get_offset_of_FieldLineNumber_3() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldLineNumber_3)); }
	inline int32_t get_FieldLineNumber_3() const { return ___FieldLineNumber_3; }
	inline int32_t* get_address_of_FieldLineNumber_3() { return &___FieldLineNumber_3; }
	inline void set_FieldLineNumber_3(int32_t value)
	{
		___FieldLineNumber_3 = value;
	}

	inline static int32_t get_offset_of_FieldLinePosition_4() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldLinePosition_4)); }
	inline int32_t get_FieldLinePosition_4() const { return ___FieldLinePosition_4; }
	inline int32_t* get_address_of_FieldLinePosition_4() { return &___FieldLinePosition_4; }
	inline void set_FieldLinePosition_4(int32_t value)
	{
		___FieldLinePosition_4 = value;
	}

	inline static int32_t get_offset_of_FieldHasLineInfo_5() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldHasLineInfo_5)); }
	inline bool get_FieldHasLineInfo_5() const { return ___FieldHasLineInfo_5; }
	inline bool* get_address_of_FieldHasLineInfo_5() { return &___FieldHasLineInfo_5; }
	inline void set_FieldHasLineInfo_5(bool value)
	{
		___FieldHasLineInfo_5 = value;
	}

	inline static int32_t get_offset_of_FieldType_6() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldType_6)); }
	inline XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * get_FieldType_6() const { return ___FieldType_6; }
	inline XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 ** get_address_of_FieldType_6() { return &___FieldType_6; }
	inline void set_FieldType_6(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * value)
	{
		___FieldType_6 = value;
		Il2CppCodeGenWriteBarrier((&___FieldType_6), value);
	}

	inline static int32_t get_offset_of_Identity_7() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___Identity_7)); }
	inline RuntimeObject * get_Identity_7() const { return ___Identity_7; }
	inline RuntimeObject ** get_address_of_Identity_7() { return &___Identity_7; }
	inline void set_Identity_7(RuntimeObject * value)
	{
		___Identity_7 = value;
		Il2CppCodeGenWriteBarrier((&___Identity_7), value);
	}

	inline static int32_t get_offset_of_IsXsiNil_8() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___IsXsiNil_8)); }
	inline bool get_IsXsiNil_8() const { return ___IsXsiNil_8; }
	inline bool* get_address_of_IsXsiNil_8() { return &___IsXsiNil_8; }
	inline void set_IsXsiNil_8(bool value)
	{
		___IsXsiNil_8 = value;
	}

	inline static int32_t get_offset_of_FieldFoundDepth_9() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldFoundDepth_9)); }
	inline int32_t get_FieldFoundDepth_9() const { return ___FieldFoundDepth_9; }
	inline int32_t* get_address_of_FieldFoundDepth_9() { return &___FieldFoundDepth_9; }
	inline void set_FieldFoundDepth_9(int32_t value)
	{
		___FieldFoundDepth_9 = value;
	}

	inline static int32_t get_offset_of_FieldFoundPath_10() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___FieldFoundPath_10)); }
	inline XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE * get_FieldFoundPath_10() const { return ___FieldFoundPath_10; }
	inline XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE ** get_address_of_FieldFoundPath_10() { return &___FieldFoundPath_10; }
	inline void set_FieldFoundPath_10(XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE * value)
	{
		___FieldFoundPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___FieldFoundPath_10), value);
	}

	inline static int32_t get_offset_of_Consuming_11() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___Consuming_11)); }
	inline bool get_Consuming_11() const { return ___Consuming_11; }
	inline bool* get_address_of_Consuming_11() { return &___Consuming_11; }
	inline void set_Consuming_11(bool value)
	{
		___Consuming_11 = value;
	}

	inline static int32_t get_offset_of_Consumed_12() { return static_cast<int32_t>(offsetof(XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2, ___Consumed_12)); }
	inline bool get_Consumed_12() const { return ___Consumed_12; }
	inline bool* get_address_of_Consumed_12() { return &___Consumed_12; }
	inline void set_Consumed_12(bool value)
	{
		___Consumed_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRYFIELD_T46A4E7087BCB9D206208679EA739175225D551D2_H
#ifndef XSDKEYTABLE_T75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6_H
#define XSDKEYTABLE_T75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyTable
struct  XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6  : public RuntimeObject
{
public:
	// System.Boolean Mono.Xml.Schema.XsdKeyTable::alwaysTrue
	bool ___alwaysTrue_0;
	// Mono.Xml.Schema.XsdIdentitySelector Mono.Xml.Schema.XsdKeyTable::selector
	XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8 * ___selector_1;
	// System.Xml.Schema.XmlSchemaIdentityConstraint Mono.Xml.Schema.XsdKeyTable::source
	XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9 * ___source_2;
	// System.Xml.XmlQualifiedName Mono.Xml.Schema.XsdKeyTable::qname
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___qname_3;
	// System.Xml.XmlQualifiedName Mono.Xml.Schema.XsdKeyTable::refKeyName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___refKeyName_4;
	// Mono.Xml.Schema.XsdKeyEntryCollection Mono.Xml.Schema.XsdKeyTable::Entries
	XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 * ___Entries_5;
	// Mono.Xml.Schema.XsdKeyEntryCollection Mono.Xml.Schema.XsdKeyTable::FinishedEntries
	XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 * ___FinishedEntries_6;
	// System.Int32 Mono.Xml.Schema.XsdKeyTable::StartDepth
	int32_t ___StartDepth_7;
	// Mono.Xml.Schema.XsdKeyTable Mono.Xml.Schema.XsdKeyTable::ReferencedKey
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 * ___ReferencedKey_8;

public:
	inline static int32_t get_offset_of_alwaysTrue_0() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___alwaysTrue_0)); }
	inline bool get_alwaysTrue_0() const { return ___alwaysTrue_0; }
	inline bool* get_address_of_alwaysTrue_0() { return &___alwaysTrue_0; }
	inline void set_alwaysTrue_0(bool value)
	{
		___alwaysTrue_0 = value;
	}

	inline static int32_t get_offset_of_selector_1() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___selector_1)); }
	inline XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8 * get_selector_1() const { return ___selector_1; }
	inline XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8 ** get_address_of_selector_1() { return &___selector_1; }
	inline void set_selector_1(XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8 * value)
	{
		___selector_1 = value;
		Il2CppCodeGenWriteBarrier((&___selector_1), value);
	}

	inline static int32_t get_offset_of_source_2() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___source_2)); }
	inline XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9 * get_source_2() const { return ___source_2; }
	inline XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9 ** get_address_of_source_2() { return &___source_2; }
	inline void set_source_2(XmlSchemaIdentityConstraint_tD10C4B19A7BC67E0D6C6B3C9764645DC0A4E47A9 * value)
	{
		___source_2 = value;
		Il2CppCodeGenWriteBarrier((&___source_2), value);
	}

	inline static int32_t get_offset_of_qname_3() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___qname_3)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_qname_3() const { return ___qname_3; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_qname_3() { return &___qname_3; }
	inline void set_qname_3(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___qname_3 = value;
		Il2CppCodeGenWriteBarrier((&___qname_3), value);
	}

	inline static int32_t get_offset_of_refKeyName_4() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___refKeyName_4)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_refKeyName_4() const { return ___refKeyName_4; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_refKeyName_4() { return &___refKeyName_4; }
	inline void set_refKeyName_4(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___refKeyName_4 = value;
		Il2CppCodeGenWriteBarrier((&___refKeyName_4), value);
	}

	inline static int32_t get_offset_of_Entries_5() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___Entries_5)); }
	inline XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 * get_Entries_5() const { return ___Entries_5; }
	inline XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 ** get_address_of_Entries_5() { return &___Entries_5; }
	inline void set_Entries_5(XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 * value)
	{
		___Entries_5 = value;
		Il2CppCodeGenWriteBarrier((&___Entries_5), value);
	}

	inline static int32_t get_offset_of_FinishedEntries_6() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___FinishedEntries_6)); }
	inline XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 * get_FinishedEntries_6() const { return ___FinishedEntries_6; }
	inline XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 ** get_address_of_FinishedEntries_6() { return &___FinishedEntries_6; }
	inline void set_FinishedEntries_6(XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265 * value)
	{
		___FinishedEntries_6 = value;
		Il2CppCodeGenWriteBarrier((&___FinishedEntries_6), value);
	}

	inline static int32_t get_offset_of_StartDepth_7() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___StartDepth_7)); }
	inline int32_t get_StartDepth_7() const { return ___StartDepth_7; }
	inline int32_t* get_address_of_StartDepth_7() { return &___StartDepth_7; }
	inline void set_StartDepth_7(int32_t value)
	{
		___StartDepth_7 = value;
	}

	inline static int32_t get_offset_of_ReferencedKey_8() { return static_cast<int32_t>(offsetof(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6, ___ReferencedKey_8)); }
	inline XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 * get_ReferencedKey_8() const { return ___ReferencedKey_8; }
	inline XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 ** get_address_of_ReferencedKey_8() { return &___ReferencedKey_8; }
	inline void set_ReferencedKey_8(XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6 * value)
	{
		___ReferencedKey_8 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencedKey_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYTABLE_T75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6_H
#ifndef XSDVALIDATIONCONTEXT_TC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD_H
#define XSDVALIDATIONCONTEXT_TC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdValidationContext
struct  XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD  : public RuntimeObject
{
public:
	// System.Object Mono.Xml.Schema.XsdValidationContext::xsi_type
	RuntimeObject * ___xsi_type_0;
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdValidationContext::State
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * ___State_1;
	// System.Collections.Stack Mono.Xml.Schema.XsdValidationContext::element_stack
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___element_stack_2;

public:
	inline static int32_t get_offset_of_xsi_type_0() { return static_cast<int32_t>(offsetof(XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD, ___xsi_type_0)); }
	inline RuntimeObject * get_xsi_type_0() const { return ___xsi_type_0; }
	inline RuntimeObject ** get_address_of_xsi_type_0() { return &___xsi_type_0; }
	inline void set_xsi_type_0(RuntimeObject * value)
	{
		___xsi_type_0 = value;
		Il2CppCodeGenWriteBarrier((&___xsi_type_0), value);
	}

	inline static int32_t get_offset_of_State_1() { return static_cast<int32_t>(offsetof(XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD, ___State_1)); }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * get_State_1() const { return ___State_1; }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 ** get_address_of_State_1() { return &___State_1; }
	inline void set_State_1(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * value)
	{
		___State_1 = value;
		Il2CppCodeGenWriteBarrier((&___State_1), value);
	}

	inline static int32_t get_offset_of_element_stack_2() { return static_cast<int32_t>(offsetof(XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD, ___element_stack_2)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_element_stack_2() const { return ___element_stack_2; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_element_stack_2() { return &___element_stack_2; }
	inline void set_element_stack_2(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___element_stack_2 = value;
		Il2CppCodeGenWriteBarrier((&___element_stack_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATIONCONTEXT_TC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD_H
#ifndef XSDVALIDATIONSTATE_T6CEE1D803E048D746FBDC7439403F070F3D66C74_H
#define XSDVALIDATIONSTATE_T6CEE1D803E048D746FBDC7439403F070F3D66C74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdValidationState
struct  XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74  : public RuntimeObject
{
public:
	// System.Int32 Mono.Xml.Schema.XsdValidationState::occured
	int32_t ___occured_1;
	// Mono.Xml.Schema.XsdParticleStateManager Mono.Xml.Schema.XsdValidationState::manager
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 * ___manager_2;

public:
	inline static int32_t get_offset_of_occured_1() { return static_cast<int32_t>(offsetof(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74, ___occured_1)); }
	inline int32_t get_occured_1() const { return ___occured_1; }
	inline int32_t* get_address_of_occured_1() { return &___occured_1; }
	inline void set_occured_1(int32_t value)
	{
		___occured_1 = value;
	}

	inline static int32_t get_offset_of_manager_2() { return static_cast<int32_t>(offsetof(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74, ___manager_2)); }
	inline XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 * get_manager_2() const { return ___manager_2; }
	inline XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 ** get_address_of_manager_2() { return &___manager_2; }
	inline void set_manager_2(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 * value)
	{
		___manager_2 = value;
		Il2CppCodeGenWriteBarrier((&___manager_2), value);
	}
};

struct XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74_StaticFields
{
public:
	// Mono.Xml.Schema.XsdInvalidValidationState Mono.Xml.Schema.XsdValidationState::invalid
	XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF * ___invalid_0;

public:
	inline static int32_t get_offset_of_invalid_0() { return static_cast<int32_t>(offsetof(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74_StaticFields, ___invalid_0)); }
	inline XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF * get_invalid_0() const { return ___invalid_0; }
	inline XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF ** get_address_of_invalid_0() { return &___invalid_0; }
	inline void set_invalid_0(XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF * value)
	{
		___invalid_0 = value;
		Il2CppCodeGenWriteBarrier((&___invalid_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATIONSTATE_T6CEE1D803E048D746FBDC7439403F070F3D66C74_H
#ifndef ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#define ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#ifndef COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#define COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F, ___list_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_list_0() const { return ___list_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#ifndef TYPECONVERTER_T5801C9F7100E1D849000ED0914E01E4CB2541B71_H
#define TYPECONVERTER_T5801C9F7100E1D849000ED0914E01E4CB2541B71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.TypeConverter
struct  TypeConverter_t5801C9F7100E1D849000ED0914E01E4CB2541B71  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPECONVERTER_T5801C9F7100E1D849000ED0914E01E4CB2541B71_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef BASEMACHINE_T7846A209B98125A8E5B5FAFE5258CC5CBFE2F857_H
#define BASEMACHINE_T7846A209B98125A8E5B5FAFE5258CC5CBFE2F857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.BaseMachine
struct  BaseMachine_t7846A209B98125A8E5B5FAFE5258CC5CBFE2F857  : public RuntimeObject
{
public:
	// System.Boolean System.Text.RegularExpressions.BaseMachine::needs_groups_or_captures
	bool ___needs_groups_or_captures_0;

public:
	inline static int32_t get_offset_of_needs_groups_or_captures_0() { return static_cast<int32_t>(offsetof(BaseMachine_t7846A209B98125A8E5B5FAFE5258CC5CBFE2F857, ___needs_groups_or_captures_0)); }
	inline bool get_needs_groups_or_captures_0() const { return ___needs_groups_or_captures_0; }
	inline bool* get_address_of_needs_groups_or_captures_0() { return &___needs_groups_or_captures_0; }
	inline void set_needs_groups_or_captures_0(bool value)
	{
		___needs_groups_or_captures_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMACHINE_T7846A209B98125A8E5B5FAFE5258CC5CBFE2F857_H
#ifndef CAPTURE_T426EB72741D828F4539C131E1DC839DD00F01F22_H
#define CAPTURE_T426EB72741D828F4539C131E1DC839DD00F01F22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Capture
struct  Capture_t426EB72741D828F4539C131E1DC839DD00F01F22  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.Capture::index
	int32_t ___index_0;
	// System.Int32 System.Text.RegularExpressions.Capture::length
	int32_t ___length_1;
	// System.String System.Text.RegularExpressions.Capture::text
	String_t* ___text_2;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Capture_t426EB72741D828F4539C131E1DC839DD00F01F22, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(Capture_t426EB72741D828F4539C131E1DC839DD00F01F22, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(Capture_t426EB72741D828F4539C131E1DC839DD00F01F22, ___text_2)); }
	inline String_t* get_text_2() const { return ___text_2; }
	inline String_t** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(String_t* value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier((&___text_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURE_T426EB72741D828F4539C131E1DC839DD00F01F22_H
#ifndef GROUPCOLLECTION_TBEA4EDB210F04963227CBC58E1434202F3CEC968_H
#define GROUPCOLLECTION_TBEA4EDB210F04963227CBC58E1434202F3CEC968_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.GroupCollection
struct  GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Group[] System.Text.RegularExpressions.GroupCollection::list
	GroupU5BU5D_tD5F0C5F00520A3213D8E6F225B1CF51985D2ACFE* ___list_0;
	// System.Int32 System.Text.RegularExpressions.GroupCollection::gap
	int32_t ___gap_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968, ___list_0)); }
	inline GroupU5BU5D_tD5F0C5F00520A3213D8E6F225B1CF51985D2ACFE* get_list_0() const { return ___list_0; }
	inline GroupU5BU5D_tD5F0C5F00520A3213D8E6F225B1CF51985D2ACFE** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(GroupU5BU5D_tD5F0C5F00520A3213D8E6F225B1CF51985D2ACFE* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_gap_1() { return static_cast<int32_t>(offsetof(GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968, ___gap_1)); }
	inline int32_t get_gap_1() const { return ___gap_1; }
	inline int32_t* get_address_of_gap_1() { return &___gap_1; }
	inline void set_gap_1(int32_t value)
	{
		___gap_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCOLLECTION_TBEA4EDB210F04963227CBC58E1434202F3CEC968_H
#ifndef REPEATCONTEXT_T53FA55142C49998080EFE4D1A0FF96573E46E938_H
#define REPEATCONTEXT_T53FA55142C49998080EFE4D1A0FF96573E46E938_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Interpreter_RepeatContext
struct  RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.Interpreter_RepeatContext::start
	int32_t ___start_0;
	// System.Int32 System.Text.RegularExpressions.Interpreter_RepeatContext::min
	int32_t ___min_1;
	// System.Int32 System.Text.RegularExpressions.Interpreter_RepeatContext::max
	int32_t ___max_2;
	// System.Boolean System.Text.RegularExpressions.Interpreter_RepeatContext::lazy
	bool ___lazy_3;
	// System.Int32 System.Text.RegularExpressions.Interpreter_RepeatContext::expr_pc
	int32_t ___expr_pc_4;
	// System.Text.RegularExpressions.Interpreter_RepeatContext System.Text.RegularExpressions.Interpreter_RepeatContext::previous
	RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 * ___previous_5;
	// System.Int32 System.Text.RegularExpressions.Interpreter_RepeatContext::count
	int32_t ___count_6;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938, ___min_1)); }
	inline int32_t get_min_1() const { return ___min_1; }
	inline int32_t* get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(int32_t value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938, ___max_2)); }
	inline int32_t get_max_2() const { return ___max_2; }
	inline int32_t* get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(int32_t value)
	{
		___max_2 = value;
	}

	inline static int32_t get_offset_of_lazy_3() { return static_cast<int32_t>(offsetof(RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938, ___lazy_3)); }
	inline bool get_lazy_3() const { return ___lazy_3; }
	inline bool* get_address_of_lazy_3() { return &___lazy_3; }
	inline void set_lazy_3(bool value)
	{
		___lazy_3 = value;
	}

	inline static int32_t get_offset_of_expr_pc_4() { return static_cast<int32_t>(offsetof(RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938, ___expr_pc_4)); }
	inline int32_t get_expr_pc_4() const { return ___expr_pc_4; }
	inline int32_t* get_address_of_expr_pc_4() { return &___expr_pc_4; }
	inline void set_expr_pc_4(int32_t value)
	{
		___expr_pc_4 = value;
	}

	inline static int32_t get_offset_of_previous_5() { return static_cast<int32_t>(offsetof(RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938, ___previous_5)); }
	inline RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 * get_previous_5() const { return ___previous_5; }
	inline RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 ** get_address_of_previous_5() { return &___previous_5; }
	inline void set_previous_5(RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 * value)
	{
		___previous_5 = value;
		Il2CppCodeGenWriteBarrier((&___previous_5), value);
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPEATCONTEXT_T53FA55142C49998080EFE4D1A0FF96573E46E938_H
#ifndef INTERVALCOLLECTION_T33C1E9164A6B24465B908CB2D90E23C9F60F7DD5_H
#define INTERVALCOLLECTION_T33C1E9164A6B24465B908CB2D90E23C9F60F7DD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.IntervalCollection
struct  IntervalCollection_t33C1E9164A6B24465B908CB2D90E23C9F60F7DD5  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Text.RegularExpressions.IntervalCollection::intervals
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___intervals_0;

public:
	inline static int32_t get_offset_of_intervals_0() { return static_cast<int32_t>(offsetof(IntervalCollection_t33C1E9164A6B24465B908CB2D90E23C9F60F7DD5, ___intervals_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_intervals_0() const { return ___intervals_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_intervals_0() { return &___intervals_0; }
	inline void set_intervals_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___intervals_0 = value;
		Il2CppCodeGenWriteBarrier((&___intervals_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERVALCOLLECTION_T33C1E9164A6B24465B908CB2D90E23C9F60F7DD5_H
#ifndef ENUMERATOR_T34D7E14B7AA7548F34E7C1BDA39BD0583A1590E4_H
#define ENUMERATOR_T34D7E14B7AA7548F34E7C1BDA39BD0583A1590E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.IntervalCollection_Enumerator
struct  Enumerator_t34D7E14B7AA7548F34E7C1BDA39BD0583A1590E4  : public RuntimeObject
{
public:
	// System.Collections.IList System.Text.RegularExpressions.IntervalCollection_Enumerator::list
	RuntimeObject* ___list_0;
	// System.Int32 System.Text.RegularExpressions.IntervalCollection_Enumerator::ptr
	int32_t ___ptr_1;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t34D7E14B7AA7548F34E7C1BDA39BD0583A1590E4, ___list_0)); }
	inline RuntimeObject* get_list_0() const { return ___list_0; }
	inline RuntimeObject** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(RuntimeObject* value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_ptr_1() { return static_cast<int32_t>(offsetof(Enumerator_t34D7E14B7AA7548F34E7C1BDA39BD0583A1590E4, ___ptr_1)); }
	inline int32_t get_ptr_1() const { return ___ptr_1; }
	inline int32_t* get_address_of_ptr_1() { return &___ptr_1; }
	inline void set_ptr_1(int32_t value)
	{
		___ptr_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T34D7E14B7AA7548F34E7C1BDA39BD0583A1590E4_H
#ifndef LINKREF_T4A317D95D5F6964647AE5BBB13F5CAE3F1C81FAB_H
#define LINKREF_T4A317D95D5F6964647AE5BBB13F5CAE3F1C81FAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.LinkRef
struct  LinkRef_t4A317D95D5F6964647AE5BBB13F5CAE3F1C81FAB  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKREF_T4A317D95D5F6964647AE5BBB13F5CAE3F1C81FAB_H
#ifndef MATCHCOLLECTION_TD651B345093D8CEC4F8E3302458C17355E017B2E_H
#define MATCHCOLLECTION_TD651B345093D8CEC4F8E3302458C17355E017B2E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchCollection
struct  MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.MatchCollection::current
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 * ___current_0;
	// System.Collections.ArrayList System.Text.RegularExpressions.MatchCollection::list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___list_1;

public:
	inline static int32_t get_offset_of_current_0() { return static_cast<int32_t>(offsetof(MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E, ___current_0)); }
	inline Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 * get_current_0() const { return ___current_0; }
	inline Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 ** get_address_of_current_0() { return &___current_0; }
	inline void set_current_0(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 * value)
	{
		___current_0 = value;
		Il2CppCodeGenWriteBarrier((&___current_0), value);
	}

	inline static int32_t get_offset_of_list_1() { return static_cast<int32_t>(offsetof(MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E, ___list_1)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_list_1() const { return ___list_1; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_list_1() { return &___list_1; }
	inline void set_list_1(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___list_1 = value;
		Il2CppCodeGenWriteBarrier((&___list_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCHCOLLECTION_TD651B345093D8CEC4F8E3302458C17355E017B2E_H
#ifndef ENUMERATOR_TEA319EDBF9ADA404733366A75202340DAD456FD8_H
#define ENUMERATOR_TEA319EDBF9ADA404733366A75202340DAD456FD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.MatchCollection_Enumerator
struct  Enumerator_tEA319EDBF9ADA404733366A75202340DAD456FD8  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.MatchCollection_Enumerator::index
	int32_t ___index_0;
	// System.Text.RegularExpressions.MatchCollection System.Text.RegularExpressions.MatchCollection_Enumerator::coll
	MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E * ___coll_1;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(Enumerator_tEA319EDBF9ADA404733366A75202340DAD456FD8, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_coll_1() { return static_cast<int32_t>(offsetof(Enumerator_tEA319EDBF9ADA404733366A75202340DAD456FD8, ___coll_1)); }
	inline MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E * get_coll_1() const { return ___coll_1; }
	inline MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E ** get_address_of_coll_1() { return &___coll_1; }
	inline void set_coll_1(MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E * value)
	{
		___coll_1 = value;
		Il2CppCodeGenWriteBarrier((&___coll_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TEA319EDBF9ADA404733366A75202340DAD456FD8_H
#ifndef QUICKSEARCH_T6623C87766BCF0EB6754150BF8B4EF4AEB718849_H
#define QUICKSEARCH_T6623C87766BCF0EB6754150BF8B4EF4AEB718849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.QuickSearch
struct  QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.QuickSearch::str
	String_t* ___str_0;
	// System.Int32 System.Text.RegularExpressions.QuickSearch::len
	int32_t ___len_1;
	// System.Boolean System.Text.RegularExpressions.QuickSearch::ignore
	bool ___ignore_2;
	// System.Boolean System.Text.RegularExpressions.QuickSearch::reverse
	bool ___reverse_3;
	// System.Byte[] System.Text.RegularExpressions.QuickSearch::shift
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___shift_4;
	// System.Collections.Hashtable System.Text.RegularExpressions.QuickSearch::shiftExtended
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___shiftExtended_5;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}

	inline static int32_t get_offset_of_len_1() { return static_cast<int32_t>(offsetof(QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849, ___len_1)); }
	inline int32_t get_len_1() const { return ___len_1; }
	inline int32_t* get_address_of_len_1() { return &___len_1; }
	inline void set_len_1(int32_t value)
	{
		___len_1 = value;
	}

	inline static int32_t get_offset_of_ignore_2() { return static_cast<int32_t>(offsetof(QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849, ___ignore_2)); }
	inline bool get_ignore_2() const { return ___ignore_2; }
	inline bool* get_address_of_ignore_2() { return &___ignore_2; }
	inline void set_ignore_2(bool value)
	{
		___ignore_2 = value;
	}

	inline static int32_t get_offset_of_reverse_3() { return static_cast<int32_t>(offsetof(QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849, ___reverse_3)); }
	inline bool get_reverse_3() const { return ___reverse_3; }
	inline bool* get_address_of_reverse_3() { return &___reverse_3; }
	inline void set_reverse_3(bool value)
	{
		___reverse_3 = value;
	}

	inline static int32_t get_offset_of_shift_4() { return static_cast<int32_t>(offsetof(QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849, ___shift_4)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_shift_4() const { return ___shift_4; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_shift_4() { return &___shift_4; }
	inline void set_shift_4(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___shift_4 = value;
		Il2CppCodeGenWriteBarrier((&___shift_4), value);
	}

	inline static int32_t get_offset_of_shiftExtended_5() { return static_cast<int32_t>(offsetof(QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849, ___shiftExtended_5)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_shiftExtended_5() const { return ___shiftExtended_5; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_shiftExtended_5() { return &___shiftExtended_5; }
	inline void set_shiftExtended_5(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___shiftExtended_5 = value;
		Il2CppCodeGenWriteBarrier((&___shiftExtended_5), value);
	}
};

struct QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849_StaticFields
{
public:
	// System.Int32 System.Text.RegularExpressions.QuickSearch::THRESHOLD
	int32_t ___THRESHOLD_6;

public:
	inline static int32_t get_offset_of_THRESHOLD_6() { return static_cast<int32_t>(offsetof(QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849_StaticFields, ___THRESHOLD_6)); }
	inline int32_t get_THRESHOLD_6() const { return ___THRESHOLD_6; }
	inline int32_t* get_address_of_THRESHOLD_6() { return &___THRESHOLD_6; }
	inline void set_THRESHOLD_6(int32_t value)
	{
		___THRESHOLD_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUICKSEARCH_T6623C87766BCF0EB6754150BF8B4EF4AEB718849_H
#ifndef REGEXRUNNERFACTORY_TF345EBFE2597908D47A0A7C0F4D7194718DACF61_H
#define REGEXRUNNERFACTORY_TF345EBFE2597908D47A0A7C0F4D7194718DACF61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexRunnerFactory
struct  RegexRunnerFactory_tF345EBFE2597908D47A0A7C0F4D7194718DACF61  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXRUNNERFACTORY_TF345EBFE2597908D47A0A7C0F4D7194718DACF61_H
#ifndef RXCOMPILER_TCBBC6E8FA1D074636E9B158778B40E01B1384708_H
#define RXCOMPILER_TCBBC6E8FA1D074636E9B158778B40E01B1384708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RxCompiler
struct  RxCompiler_tCBBC6E8FA1D074636E9B158778B40E01B1384708  : public RuntimeObject
{
public:
	// System.Byte[] System.Text.RegularExpressions.RxCompiler::program
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___program_0;
	// System.Int32 System.Text.RegularExpressions.RxCompiler::curpos
	int32_t ___curpos_1;

public:
	inline static int32_t get_offset_of_program_0() { return static_cast<int32_t>(offsetof(RxCompiler_tCBBC6E8FA1D074636E9B158778B40E01B1384708, ___program_0)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_program_0() const { return ___program_0; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_program_0() { return &___program_0; }
	inline void set_program_0(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___program_0 = value;
		Il2CppCodeGenWriteBarrier((&___program_0), value);
	}

	inline static int32_t get_offset_of_curpos_1() { return static_cast<int32_t>(offsetof(RxCompiler_tCBBC6E8FA1D074636E9B158778B40E01B1384708, ___curpos_1)); }
	inline int32_t get_curpos_1() const { return ___curpos_1; }
	inline int32_t* get_address_of_curpos_1() { return &___curpos_1; }
	inline void set_curpos_1(int32_t value)
	{
		___curpos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RXCOMPILER_TCBBC6E8FA1D074636E9B158778B40E01B1384708_H
#ifndef REPEATCONTEXT_T720E27A2CE416B69952FD9273E80FB41DD337622_H
#define REPEATCONTEXT_T720E27A2CE416B69952FD9273E80FB41DD337622_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RxInterpreter_RepeatContext
struct  RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622  : public RuntimeObject
{
public:
	// System.Int32 System.Text.RegularExpressions.RxInterpreter_RepeatContext::start
	int32_t ___start_0;
	// System.Int32 System.Text.RegularExpressions.RxInterpreter_RepeatContext::min
	int32_t ___min_1;
	// System.Int32 System.Text.RegularExpressions.RxInterpreter_RepeatContext::max
	int32_t ___max_2;
	// System.Boolean System.Text.RegularExpressions.RxInterpreter_RepeatContext::lazy
	bool ___lazy_3;
	// System.Int32 System.Text.RegularExpressions.RxInterpreter_RepeatContext::expr_pc
	int32_t ___expr_pc_4;
	// System.Text.RegularExpressions.RxInterpreter_RepeatContext System.Text.RegularExpressions.RxInterpreter_RepeatContext::previous
	RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622 * ___previous_5;
	// System.Int32 System.Text.RegularExpressions.RxInterpreter_RepeatContext::count
	int32_t ___count_6;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622, ___min_1)); }
	inline int32_t get_min_1() const { return ___min_1; }
	inline int32_t* get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(int32_t value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622, ___max_2)); }
	inline int32_t get_max_2() const { return ___max_2; }
	inline int32_t* get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(int32_t value)
	{
		___max_2 = value;
	}

	inline static int32_t get_offset_of_lazy_3() { return static_cast<int32_t>(offsetof(RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622, ___lazy_3)); }
	inline bool get_lazy_3() const { return ___lazy_3; }
	inline bool* get_address_of_lazy_3() { return &___lazy_3; }
	inline void set_lazy_3(bool value)
	{
		___lazy_3 = value;
	}

	inline static int32_t get_offset_of_expr_pc_4() { return static_cast<int32_t>(offsetof(RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622, ___expr_pc_4)); }
	inline int32_t get_expr_pc_4() const { return ___expr_pc_4; }
	inline int32_t* get_address_of_expr_pc_4() { return &___expr_pc_4; }
	inline void set_expr_pc_4(int32_t value)
	{
		___expr_pc_4 = value;
	}

	inline static int32_t get_offset_of_previous_5() { return static_cast<int32_t>(offsetof(RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622, ___previous_5)); }
	inline RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622 * get_previous_5() const { return ___previous_5; }
	inline RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622 ** get_address_of_previous_5() { return &___previous_5; }
	inline void set_previous_5(RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622 * value)
	{
		___previous_5 = value;
		Il2CppCodeGenWriteBarrier((&___previous_5), value);
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPEATCONTEXT_T720E27A2CE416B69952FD9273E80FB41DD337622_H
#ifndef RXINTERPRETERFACTORY_T62BD38060C13E7258420C8AA03BCB07C70754081_H
#define RXINTERPRETERFACTORY_T62BD38060C13E7258420C8AA03BCB07C70754081_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RxInterpreterFactory
struct  RxInterpreterFactory_t62BD38060C13E7258420C8AA03BCB07C70754081  : public RuntimeObject
{
public:
	// System.Collections.IDictionary System.Text.RegularExpressions.RxInterpreterFactory::mapping
	RuntimeObject* ___mapping_0;
	// System.Byte[] System.Text.RegularExpressions.RxInterpreterFactory::program
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___program_1;
	// System.Text.RegularExpressions.EvalDelegate System.Text.RegularExpressions.RxInterpreterFactory::eval_del
	EvalDelegate_tEAE36D514400FE58E4761FEDBB29920820C4B2FB * ___eval_del_2;
	// System.String[] System.Text.RegularExpressions.RxInterpreterFactory::namesMapping
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___namesMapping_3;
	// System.Int32 System.Text.RegularExpressions.RxInterpreterFactory::gap
	int32_t ___gap_4;

public:
	inline static int32_t get_offset_of_mapping_0() { return static_cast<int32_t>(offsetof(RxInterpreterFactory_t62BD38060C13E7258420C8AA03BCB07C70754081, ___mapping_0)); }
	inline RuntimeObject* get_mapping_0() const { return ___mapping_0; }
	inline RuntimeObject** get_address_of_mapping_0() { return &___mapping_0; }
	inline void set_mapping_0(RuntimeObject* value)
	{
		___mapping_0 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_0), value);
	}

	inline static int32_t get_offset_of_program_1() { return static_cast<int32_t>(offsetof(RxInterpreterFactory_t62BD38060C13E7258420C8AA03BCB07C70754081, ___program_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_program_1() const { return ___program_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_program_1() { return &___program_1; }
	inline void set_program_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___program_1 = value;
		Il2CppCodeGenWriteBarrier((&___program_1), value);
	}

	inline static int32_t get_offset_of_eval_del_2() { return static_cast<int32_t>(offsetof(RxInterpreterFactory_t62BD38060C13E7258420C8AA03BCB07C70754081, ___eval_del_2)); }
	inline EvalDelegate_tEAE36D514400FE58E4761FEDBB29920820C4B2FB * get_eval_del_2() const { return ___eval_del_2; }
	inline EvalDelegate_tEAE36D514400FE58E4761FEDBB29920820C4B2FB ** get_address_of_eval_del_2() { return &___eval_del_2; }
	inline void set_eval_del_2(EvalDelegate_tEAE36D514400FE58E4761FEDBB29920820C4B2FB * value)
	{
		___eval_del_2 = value;
		Il2CppCodeGenWriteBarrier((&___eval_del_2), value);
	}

	inline static int32_t get_offset_of_namesMapping_3() { return static_cast<int32_t>(offsetof(RxInterpreterFactory_t62BD38060C13E7258420C8AA03BCB07C70754081, ___namesMapping_3)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_namesMapping_3() const { return ___namesMapping_3; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_namesMapping_3() { return &___namesMapping_3; }
	inline void set_namesMapping_3(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___namesMapping_3 = value;
		Il2CppCodeGenWriteBarrier((&___namesMapping_3), value);
	}

	inline static int32_t get_offset_of_gap_4() { return static_cast<int32_t>(offsetof(RxInterpreterFactory_t62BD38060C13E7258420C8AA03BCB07C70754081, ___gap_4)); }
	inline int32_t get_gap_4() const { return ___gap_4; }
	inline int32_t* get_address_of_gap_4() { return &___gap_4; }
	inline void set_gap_4(int32_t value)
	{
		___gap_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RXINTERPRETERFACTORY_T62BD38060C13E7258420C8AA03BCB07C70754081_H
#ifndef EXPRESSION_T6A43E18B89A25A32D44C3FD396F6139A8AA73F94_H
#define EXPRESSION_T6A43E18B89A25A32D44C3FD396F6139A8AA73F94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.Expression
struct  Expression_t6A43E18B89A25A32D44C3FD396F6139A8AA73F94  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSION_T6A43E18B89A25A32D44C3FD396F6139A8AA73F94_H
#ifndef PARSER_T781E0BC79514BED615EBBD67291C3D4B48519019_H
#define PARSER_T781E0BC79514BED615EBBD67291C3D4B48519019_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.Parser
struct  Parser_t781E0BC79514BED615EBBD67291C3D4B48519019  : public RuntimeObject
{
public:
	// System.String System.Text.RegularExpressions.Syntax.Parser::pattern
	String_t* ___pattern_0;
	// System.Int32 System.Text.RegularExpressions.Syntax.Parser::ptr
	int32_t ___ptr_1;
	// System.Collections.ArrayList System.Text.RegularExpressions.Syntax.Parser::caps
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___caps_2;
	// System.Collections.Hashtable System.Text.RegularExpressions.Syntax.Parser::refs
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___refs_3;
	// System.Int32 System.Text.RegularExpressions.Syntax.Parser::num_groups
	int32_t ___num_groups_4;
	// System.Int32 System.Text.RegularExpressions.Syntax.Parser::gap
	int32_t ___gap_5;

public:
	inline static int32_t get_offset_of_pattern_0() { return static_cast<int32_t>(offsetof(Parser_t781E0BC79514BED615EBBD67291C3D4B48519019, ___pattern_0)); }
	inline String_t* get_pattern_0() const { return ___pattern_0; }
	inline String_t** get_address_of_pattern_0() { return &___pattern_0; }
	inline void set_pattern_0(String_t* value)
	{
		___pattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_0), value);
	}

	inline static int32_t get_offset_of_ptr_1() { return static_cast<int32_t>(offsetof(Parser_t781E0BC79514BED615EBBD67291C3D4B48519019, ___ptr_1)); }
	inline int32_t get_ptr_1() const { return ___ptr_1; }
	inline int32_t* get_address_of_ptr_1() { return &___ptr_1; }
	inline void set_ptr_1(int32_t value)
	{
		___ptr_1 = value;
	}

	inline static int32_t get_offset_of_caps_2() { return static_cast<int32_t>(offsetof(Parser_t781E0BC79514BED615EBBD67291C3D4B48519019, ___caps_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_caps_2() const { return ___caps_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_caps_2() { return &___caps_2; }
	inline void set_caps_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___caps_2 = value;
		Il2CppCodeGenWriteBarrier((&___caps_2), value);
	}

	inline static int32_t get_offset_of_refs_3() { return static_cast<int32_t>(offsetof(Parser_t781E0BC79514BED615EBBD67291C3D4B48519019, ___refs_3)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_refs_3() const { return ___refs_3; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_refs_3() { return &___refs_3; }
	inline void set_refs_3(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___refs_3 = value;
		Il2CppCodeGenWriteBarrier((&___refs_3), value);
	}

	inline static int32_t get_offset_of_num_groups_4() { return static_cast<int32_t>(offsetof(Parser_t781E0BC79514BED615EBBD67291C3D4B48519019, ___num_groups_4)); }
	inline int32_t get_num_groups_4() const { return ___num_groups_4; }
	inline int32_t* get_address_of_num_groups_4() { return &___num_groups_4; }
	inline void set_num_groups_4(int32_t value)
	{
		___num_groups_4 = value;
	}

	inline static int32_t get_offset_of_gap_5() { return static_cast<int32_t>(offsetof(Parser_t781E0BC79514BED615EBBD67291C3D4B48519019, ___gap_5)); }
	inline int32_t get_gap_5() const { return ___gap_5; }
	inline int32_t* get_address_of_gap_5() { return &___gap_5; }
	inline void set_gap_5(int32_t value)
	{
		___gap_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T781E0BC79514BED615EBBD67291C3D4B48519019_H
#ifndef URI_TF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_H
#define URI_TF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E  : public RuntimeObject
{
public:
	// System.Boolean System.Uri::isUnixFilePath
	bool ___isUnixFilePath_0;
	// System.String System.Uri::source
	String_t* ___source_1;
	// System.String System.Uri::scheme
	String_t* ___scheme_2;
	// System.String System.Uri::host
	String_t* ___host_3;
	// System.Int32 System.Uri::port
	int32_t ___port_4;
	// System.String System.Uri::path
	String_t* ___path_5;
	// System.String System.Uri::query
	String_t* ___query_6;
	// System.String System.Uri::fragment
	String_t* ___fragment_7;
	// System.String System.Uri::userinfo
	String_t* ___userinfo_8;
	// System.Boolean System.Uri::isUnc
	bool ___isUnc_9;
	// System.Boolean System.Uri::isOpaquePart
	bool ___isOpaquePart_10;
	// System.Boolean System.Uri::isAbsoluteUri
	bool ___isAbsoluteUri_11;
	// System.Boolean System.Uri::userEscaped
	bool ___userEscaped_12;
	// System.String System.Uri::cachedAbsoluteUri
	String_t* ___cachedAbsoluteUri_13;
	// System.String System.Uri::cachedToString
	String_t* ___cachedToString_14;
	// System.String System.Uri::cachedLocalPath
	String_t* ___cachedLocalPath_15;
	// System.Int32 System.Uri::cachedHashCode
	int32_t ___cachedHashCode_16;
	// System.UriParser System.Uri::parser
	UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A * ___parser_30;

public:
	inline static int32_t get_offset_of_isUnixFilePath_0() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___isUnixFilePath_0)); }
	inline bool get_isUnixFilePath_0() const { return ___isUnixFilePath_0; }
	inline bool* get_address_of_isUnixFilePath_0() { return &___isUnixFilePath_0; }
	inline void set_isUnixFilePath_0(bool value)
	{
		___isUnixFilePath_0 = value;
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___source_1)); }
	inline String_t* get_source_1() const { return ___source_1; }
	inline String_t** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(String_t* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_scheme_2() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___scheme_2)); }
	inline String_t* get_scheme_2() const { return ___scheme_2; }
	inline String_t** get_address_of_scheme_2() { return &___scheme_2; }
	inline void set_scheme_2(String_t* value)
	{
		___scheme_2 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_2), value);
	}

	inline static int32_t get_offset_of_host_3() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___host_3)); }
	inline String_t* get_host_3() const { return ___host_3; }
	inline String_t** get_address_of_host_3() { return &___host_3; }
	inline void set_host_3(String_t* value)
	{
		___host_3 = value;
		Il2CppCodeGenWriteBarrier((&___host_3), value);
	}

	inline static int32_t get_offset_of_port_4() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___port_4)); }
	inline int32_t get_port_4() const { return ___port_4; }
	inline int32_t* get_address_of_port_4() { return &___port_4; }
	inline void set_port_4(int32_t value)
	{
		___port_4 = value;
	}

	inline static int32_t get_offset_of_path_5() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___path_5)); }
	inline String_t* get_path_5() const { return ___path_5; }
	inline String_t** get_address_of_path_5() { return &___path_5; }
	inline void set_path_5(String_t* value)
	{
		___path_5 = value;
		Il2CppCodeGenWriteBarrier((&___path_5), value);
	}

	inline static int32_t get_offset_of_query_6() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___query_6)); }
	inline String_t* get_query_6() const { return ___query_6; }
	inline String_t** get_address_of_query_6() { return &___query_6; }
	inline void set_query_6(String_t* value)
	{
		___query_6 = value;
		Il2CppCodeGenWriteBarrier((&___query_6), value);
	}

	inline static int32_t get_offset_of_fragment_7() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___fragment_7)); }
	inline String_t* get_fragment_7() const { return ___fragment_7; }
	inline String_t** get_address_of_fragment_7() { return &___fragment_7; }
	inline void set_fragment_7(String_t* value)
	{
		___fragment_7 = value;
		Il2CppCodeGenWriteBarrier((&___fragment_7), value);
	}

	inline static int32_t get_offset_of_userinfo_8() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___userinfo_8)); }
	inline String_t* get_userinfo_8() const { return ___userinfo_8; }
	inline String_t** get_address_of_userinfo_8() { return &___userinfo_8; }
	inline void set_userinfo_8(String_t* value)
	{
		___userinfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___userinfo_8), value);
	}

	inline static int32_t get_offset_of_isUnc_9() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___isUnc_9)); }
	inline bool get_isUnc_9() const { return ___isUnc_9; }
	inline bool* get_address_of_isUnc_9() { return &___isUnc_9; }
	inline void set_isUnc_9(bool value)
	{
		___isUnc_9 = value;
	}

	inline static int32_t get_offset_of_isOpaquePart_10() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___isOpaquePart_10)); }
	inline bool get_isOpaquePart_10() const { return ___isOpaquePart_10; }
	inline bool* get_address_of_isOpaquePart_10() { return &___isOpaquePart_10; }
	inline void set_isOpaquePart_10(bool value)
	{
		___isOpaquePart_10 = value;
	}

	inline static int32_t get_offset_of_isAbsoluteUri_11() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___isAbsoluteUri_11)); }
	inline bool get_isAbsoluteUri_11() const { return ___isAbsoluteUri_11; }
	inline bool* get_address_of_isAbsoluteUri_11() { return &___isAbsoluteUri_11; }
	inline void set_isAbsoluteUri_11(bool value)
	{
		___isAbsoluteUri_11 = value;
	}

	inline static int32_t get_offset_of_userEscaped_12() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___userEscaped_12)); }
	inline bool get_userEscaped_12() const { return ___userEscaped_12; }
	inline bool* get_address_of_userEscaped_12() { return &___userEscaped_12; }
	inline void set_userEscaped_12(bool value)
	{
		___userEscaped_12 = value;
	}

	inline static int32_t get_offset_of_cachedAbsoluteUri_13() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___cachedAbsoluteUri_13)); }
	inline String_t* get_cachedAbsoluteUri_13() const { return ___cachedAbsoluteUri_13; }
	inline String_t** get_address_of_cachedAbsoluteUri_13() { return &___cachedAbsoluteUri_13; }
	inline void set_cachedAbsoluteUri_13(String_t* value)
	{
		___cachedAbsoluteUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAbsoluteUri_13), value);
	}

	inline static int32_t get_offset_of_cachedToString_14() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___cachedToString_14)); }
	inline String_t* get_cachedToString_14() const { return ___cachedToString_14; }
	inline String_t** get_address_of_cachedToString_14() { return &___cachedToString_14; }
	inline void set_cachedToString_14(String_t* value)
	{
		___cachedToString_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedToString_14), value);
	}

	inline static int32_t get_offset_of_cachedLocalPath_15() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___cachedLocalPath_15)); }
	inline String_t* get_cachedLocalPath_15() const { return ___cachedLocalPath_15; }
	inline String_t** get_address_of_cachedLocalPath_15() { return &___cachedLocalPath_15; }
	inline void set_cachedLocalPath_15(String_t* value)
	{
		___cachedLocalPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedLocalPath_15), value);
	}

	inline static int32_t get_offset_of_cachedHashCode_16() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___cachedHashCode_16)); }
	inline int32_t get_cachedHashCode_16() const { return ___cachedHashCode_16; }
	inline int32_t* get_address_of_cachedHashCode_16() { return &___cachedHashCode_16; }
	inline void set_cachedHashCode_16(int32_t value)
	{
		___cachedHashCode_16 = value;
	}

	inline static int32_t get_offset_of_parser_30() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___parser_30)); }
	inline UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A * get_parser_30() const { return ___parser_30; }
	inline UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A ** get_address_of_parser_30() { return &___parser_30; }
	inline void set_parser_30(UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A * value)
	{
		___parser_30 = value;
		Il2CppCodeGenWriteBarrier((&___parser_30), value);
	}
};

struct Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields
{
public:
	// System.String System.Uri::hexUpperChars
	String_t* ___hexUpperChars_17;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_18;
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_19;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_20;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_21;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_22;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_23;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_24;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_25;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_26;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_27;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_28;
	// System.Uri_UriScheme[] System.Uri::schemes
	UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6* ___schemes_29;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switchU24map1C
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map1C_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switchU24map1D
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map1D_32;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switchU24map1E
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map1E_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switchU24map1F
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map1F_34;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switchU24map20
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map20_35;

public:
	inline static int32_t get_offset_of_hexUpperChars_17() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___hexUpperChars_17)); }
	inline String_t* get_hexUpperChars_17() const { return ___hexUpperChars_17; }
	inline String_t** get_address_of_hexUpperChars_17() { return &___hexUpperChars_17; }
	inline void set_hexUpperChars_17(String_t* value)
	{
		___hexUpperChars_17 = value;
		Il2CppCodeGenWriteBarrier((&___hexUpperChars_17), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_18() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___SchemeDelimiter_18)); }
	inline String_t* get_SchemeDelimiter_18() const { return ___SchemeDelimiter_18; }
	inline String_t** get_address_of_SchemeDelimiter_18() { return &___SchemeDelimiter_18; }
	inline void set_SchemeDelimiter_18(String_t* value)
	{
		___SchemeDelimiter_18 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_18), value);
	}

	inline static int32_t get_offset_of_UriSchemeFile_19() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeFile_19)); }
	inline String_t* get_UriSchemeFile_19() const { return ___UriSchemeFile_19; }
	inline String_t** get_address_of_UriSchemeFile_19() { return &___UriSchemeFile_19; }
	inline void set_UriSchemeFile_19(String_t* value)
	{
		___UriSchemeFile_19 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_19), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_20() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeFtp_20)); }
	inline String_t* get_UriSchemeFtp_20() const { return ___UriSchemeFtp_20; }
	inline String_t** get_address_of_UriSchemeFtp_20() { return &___UriSchemeFtp_20; }
	inline void set_UriSchemeFtp_20(String_t* value)
	{
		___UriSchemeFtp_20 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_20), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_21() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeGopher_21)); }
	inline String_t* get_UriSchemeGopher_21() const { return ___UriSchemeGopher_21; }
	inline String_t** get_address_of_UriSchemeGopher_21() { return &___UriSchemeGopher_21; }
	inline void set_UriSchemeGopher_21(String_t* value)
	{
		___UriSchemeGopher_21 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_21), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_22() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeHttp_22)); }
	inline String_t* get_UriSchemeHttp_22() const { return ___UriSchemeHttp_22; }
	inline String_t** get_address_of_UriSchemeHttp_22() { return &___UriSchemeHttp_22; }
	inline void set_UriSchemeHttp_22(String_t* value)
	{
		___UriSchemeHttp_22 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_22), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_23() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeHttps_23)); }
	inline String_t* get_UriSchemeHttps_23() const { return ___UriSchemeHttps_23; }
	inline String_t** get_address_of_UriSchemeHttps_23() { return &___UriSchemeHttps_23; }
	inline void set_UriSchemeHttps_23(String_t* value)
	{
		___UriSchemeHttps_23 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_23), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_24() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeMailto_24)); }
	inline String_t* get_UriSchemeMailto_24() const { return ___UriSchemeMailto_24; }
	inline String_t** get_address_of_UriSchemeMailto_24() { return &___UriSchemeMailto_24; }
	inline void set_UriSchemeMailto_24(String_t* value)
	{
		___UriSchemeMailto_24 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_24), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_25() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeNews_25)); }
	inline String_t* get_UriSchemeNews_25() const { return ___UriSchemeNews_25; }
	inline String_t** get_address_of_UriSchemeNews_25() { return &___UriSchemeNews_25; }
	inline void set_UriSchemeNews_25(String_t* value)
	{
		___UriSchemeNews_25 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_25), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_26() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeNntp_26)); }
	inline String_t* get_UriSchemeNntp_26() const { return ___UriSchemeNntp_26; }
	inline String_t** get_address_of_UriSchemeNntp_26() { return &___UriSchemeNntp_26; }
	inline void set_UriSchemeNntp_26(String_t* value)
	{
		___UriSchemeNntp_26 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_26), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_27() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeNetPipe_27)); }
	inline String_t* get_UriSchemeNetPipe_27() const { return ___UriSchemeNetPipe_27; }
	inline String_t** get_address_of_UriSchemeNetPipe_27() { return &___UriSchemeNetPipe_27; }
	inline void set_UriSchemeNetPipe_27(String_t* value)
	{
		___UriSchemeNetPipe_27 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_27), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_28() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeNetTcp_28)); }
	inline String_t* get_UriSchemeNetTcp_28() const { return ___UriSchemeNetTcp_28; }
	inline String_t** get_address_of_UriSchemeNetTcp_28() { return &___UriSchemeNetTcp_28; }
	inline void set_UriSchemeNetTcp_28(String_t* value)
	{
		___UriSchemeNetTcp_28 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_28), value);
	}

	inline static int32_t get_offset_of_schemes_29() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___schemes_29)); }
	inline UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6* get_schemes_29() const { return ___schemes_29; }
	inline UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6** get_address_of_schemes_29() { return &___schemes_29; }
	inline void set_schemes_29(UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6* value)
	{
		___schemes_29 = value;
		Il2CppCodeGenWriteBarrier((&___schemes_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1C_31() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map1C_31)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map1C_31() const { return ___U3CU3Ef__switchU24map1C_31; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map1C_31() { return &___U3CU3Ef__switchU24map1C_31; }
	inline void set_U3CU3Ef__switchU24map1C_31(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map1C_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1C_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1D_32() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map1D_32)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map1D_32() const { return ___U3CU3Ef__switchU24map1D_32; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map1D_32() { return &___U3CU3Ef__switchU24map1D_32; }
	inline void set_U3CU3Ef__switchU24map1D_32(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map1D_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1D_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1E_33() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map1E_33)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map1E_33() const { return ___U3CU3Ef__switchU24map1E_33; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map1E_33() { return &___U3CU3Ef__switchU24map1E_33; }
	inline void set_U3CU3Ef__switchU24map1E_33(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map1E_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1E_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1F_34() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map1F_34)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map1F_34() const { return ___U3CU3Ef__switchU24map1F_34; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map1F_34() { return &___U3CU3Ef__switchU24map1F_34; }
	inline void set_U3CU3Ef__switchU24map1F_34(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map1F_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1F_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map20_35() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map20_35)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map20_35() const { return ___U3CU3Ef__switchU24map20_35; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map20_35() { return &___U3CU3Ef__switchU24map20_35; }
	inline void set_U3CU3Ef__switchU24map20_35(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map20_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map20_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_TF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_H
#ifndef URIBUILDER_T9417C2D2E047DBD48E41898A4978B618EE500295_H
#define URIBUILDER_T9417C2D2E047DBD48E41898A4978B618EE500295_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriBuilder
struct  UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295  : public RuntimeObject
{
public:
	// System.String System.UriBuilder::scheme
	String_t* ___scheme_0;
	// System.String System.UriBuilder::host
	String_t* ___host_1;
	// System.Int32 System.UriBuilder::port
	int32_t ___port_2;
	// System.String System.UriBuilder::path
	String_t* ___path_3;
	// System.String System.UriBuilder::query
	String_t* ___query_4;
	// System.String System.UriBuilder::fragment
	String_t* ___fragment_5;
	// System.String System.UriBuilder::username
	String_t* ___username_6;
	// System.String System.UriBuilder::password
	String_t* ___password_7;
	// System.Uri System.UriBuilder::uri
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * ___uri_8;
	// System.Boolean System.UriBuilder::modified
	bool ___modified_9;

public:
	inline static int32_t get_offset_of_scheme_0() { return static_cast<int32_t>(offsetof(UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295, ___scheme_0)); }
	inline String_t* get_scheme_0() const { return ___scheme_0; }
	inline String_t** get_address_of_scheme_0() { return &___scheme_0; }
	inline void set_scheme_0(String_t* value)
	{
		___scheme_0 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_0), value);
	}

	inline static int32_t get_offset_of_host_1() { return static_cast<int32_t>(offsetof(UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295, ___host_1)); }
	inline String_t* get_host_1() const { return ___host_1; }
	inline String_t** get_address_of_host_1() { return &___host_1; }
	inline void set_host_1(String_t* value)
	{
		___host_1 = value;
		Il2CppCodeGenWriteBarrier((&___host_1), value);
	}

	inline static int32_t get_offset_of_port_2() { return static_cast<int32_t>(offsetof(UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295, ___port_2)); }
	inline int32_t get_port_2() const { return ___port_2; }
	inline int32_t* get_address_of_port_2() { return &___port_2; }
	inline void set_port_2(int32_t value)
	{
		___port_2 = value;
	}

	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295, ___path_3)); }
	inline String_t* get_path_3() const { return ___path_3; }
	inline String_t** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(String_t* value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier((&___path_3), value);
	}

	inline static int32_t get_offset_of_query_4() { return static_cast<int32_t>(offsetof(UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295, ___query_4)); }
	inline String_t* get_query_4() const { return ___query_4; }
	inline String_t** get_address_of_query_4() { return &___query_4; }
	inline void set_query_4(String_t* value)
	{
		___query_4 = value;
		Il2CppCodeGenWriteBarrier((&___query_4), value);
	}

	inline static int32_t get_offset_of_fragment_5() { return static_cast<int32_t>(offsetof(UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295, ___fragment_5)); }
	inline String_t* get_fragment_5() const { return ___fragment_5; }
	inline String_t** get_address_of_fragment_5() { return &___fragment_5; }
	inline void set_fragment_5(String_t* value)
	{
		___fragment_5 = value;
		Il2CppCodeGenWriteBarrier((&___fragment_5), value);
	}

	inline static int32_t get_offset_of_username_6() { return static_cast<int32_t>(offsetof(UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295, ___username_6)); }
	inline String_t* get_username_6() const { return ___username_6; }
	inline String_t** get_address_of_username_6() { return &___username_6; }
	inline void set_username_6(String_t* value)
	{
		___username_6 = value;
		Il2CppCodeGenWriteBarrier((&___username_6), value);
	}

	inline static int32_t get_offset_of_password_7() { return static_cast<int32_t>(offsetof(UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295, ___password_7)); }
	inline String_t* get_password_7() const { return ___password_7; }
	inline String_t** get_address_of_password_7() { return &___password_7; }
	inline void set_password_7(String_t* value)
	{
		___password_7 = value;
		Il2CppCodeGenWriteBarrier((&___password_7), value);
	}

	inline static int32_t get_offset_of_uri_8() { return static_cast<int32_t>(offsetof(UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295, ___uri_8)); }
	inline Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * get_uri_8() const { return ___uri_8; }
	inline Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E ** get_address_of_uri_8() { return &___uri_8; }
	inline void set_uri_8(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * value)
	{
		___uri_8 = value;
		Il2CppCodeGenWriteBarrier((&___uri_8), value);
	}

	inline static int32_t get_offset_of_modified_9() { return static_cast<int32_t>(offsetof(UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295, ___modified_9)); }
	inline bool get_modified_9() const { return ___modified_9; }
	inline bool* get_address_of_modified_9() { return &___modified_9; }
	inline void set_modified_9(bool value)
	{
		___modified_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIBUILDER_T9417C2D2E047DBD48E41898A4978B618EE500295_H
#ifndef URIPARSER_TD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_H
#define URIPARSER_TD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriParser
struct  UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A  : public RuntimeObject
{
public:
	// System.String System.UriParser::scheme_name
	String_t* ___scheme_name_2;
	// System.Int32 System.UriParser::default_port
	int32_t ___default_port_3;

public:
	inline static int32_t get_offset_of_scheme_name_2() { return static_cast<int32_t>(offsetof(UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A, ___scheme_name_2)); }
	inline String_t* get_scheme_name_2() const { return ___scheme_name_2; }
	inline String_t** get_address_of_scheme_name_2() { return &___scheme_name_2; }
	inline void set_scheme_name_2(String_t* value)
	{
		___scheme_name_2 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_name_2), value);
	}

	inline static int32_t get_offset_of_default_port_3() { return static_cast<int32_t>(offsetof(UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A, ___default_port_3)); }
	inline int32_t get_default_port_3() const { return ___default_port_3; }
	inline int32_t* get_address_of_default_port_3() { return &___default_port_3; }
	inline void set_default_port_3(int32_t value)
	{
		___default_port_3 = value;
	}
};

struct UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_StaticFields
{
public:
	// System.Object System.UriParser::lock_object
	RuntimeObject * ___lock_object_0;
	// System.Collections.Hashtable System.UriParser::table
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___table_1;
	// System.Text.RegularExpressions.Regex System.UriParser::uri_regex
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * ___uri_regex_4;
	// System.Text.RegularExpressions.Regex System.UriParser::auth_regex
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * ___auth_regex_5;

public:
	inline static int32_t get_offset_of_lock_object_0() { return static_cast<int32_t>(offsetof(UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_StaticFields, ___lock_object_0)); }
	inline RuntimeObject * get_lock_object_0() const { return ___lock_object_0; }
	inline RuntimeObject ** get_address_of_lock_object_0() { return &___lock_object_0; }
	inline void set_lock_object_0(RuntimeObject * value)
	{
		___lock_object_0 = value;
		Il2CppCodeGenWriteBarrier((&___lock_object_0), value);
	}

	inline static int32_t get_offset_of_table_1() { return static_cast<int32_t>(offsetof(UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_StaticFields, ___table_1)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_table_1() const { return ___table_1; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_table_1() { return &___table_1; }
	inline void set_table_1(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___table_1 = value;
		Il2CppCodeGenWriteBarrier((&___table_1), value);
	}

	inline static int32_t get_offset_of_uri_regex_4() { return static_cast<int32_t>(offsetof(UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_StaticFields, ___uri_regex_4)); }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * get_uri_regex_4() const { return ___uri_regex_4; }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B ** get_address_of_uri_regex_4() { return &___uri_regex_4; }
	inline void set_uri_regex_4(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * value)
	{
		___uri_regex_4 = value;
		Il2CppCodeGenWriteBarrier((&___uri_regex_4), value);
	}

	inline static int32_t get_offset_of_auth_regex_5() { return static_cast<int32_t>(offsetof(UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_StaticFields, ___auth_regex_5)); }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * get_auth_regex_5() const { return ___auth_regex_5; }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B ** get_address_of_auth_regex_5() { return &___auth_regex_5; }
	inline void set_auth_regex_5(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * value)
	{
		___auth_regex_5 = value;
		Il2CppCodeGenWriteBarrier((&___auth_regex_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIPARSER_TD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#define XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293, ___binary_0)); }
	inline XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293, ___settings_1)); }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#ifndef U24ARRAYTYPEU2412_T91D3DF413568F08A27A79C7AD08EB23506A60F51_H
#define U24ARRAYTYPEU2412_T91D3DF413568F08A27A79C7AD08EB23506A60F51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU2412
struct  U24ArrayTypeU2412_t91D3DF413568F08A27A79C7AD08EB23506A60F51 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2412_t91D3DF413568F08A27A79C7AD08EB23506A60F51__padding[12];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2412_T91D3DF413568F08A27A79C7AD08EB23506A60F51_H
#ifndef U24ARRAYTYPEU24128_TC2FBD9B9E13A39460761EEDD6034845832CDBAD7_H
#define U24ARRAYTYPEU24128_TC2FBD9B9E13A39460761EEDD6034845832CDBAD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU24128
struct  U24ArrayTypeU24128_tC2FBD9B9E13A39460761EEDD6034845832CDBAD7 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU24128_tC2FBD9B9E13A39460761EEDD6034845832CDBAD7__padding[128];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU24128_TC2FBD9B9E13A39460761EEDD6034845832CDBAD7_H
#ifndef U24ARRAYTYPEU2416_T2DDBD265BE6606DAC46187FDC58335362DC6AE4C_H
#define U24ARRAYTYPEU2416_T2DDBD265BE6606DAC46187FDC58335362DC6AE4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU2416
struct  U24ArrayTypeU2416_t2DDBD265BE6606DAC46187FDC58335362DC6AE4C 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t U24ArrayTypeU2416_t2DDBD265BE6606DAC46187FDC58335362DC6AE4C__padding[16];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU2416_T2DDBD265BE6606DAC46187FDC58335362DC6AE4C_H
#ifndef DTDANYAUTOMATA_T7D15632FA965F7D26DBF45EAA3D3BA11B59E7A06_H
#define DTDANYAUTOMATA_T7D15632FA965F7D26DBF45EAA3D3BA11B59E7A06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAnyAutomata
struct  DTDAnyAutomata_t7D15632FA965F7D26DBF45EAA3D3BA11B59E7A06  : public DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDANYAUTOMATA_T7D15632FA965F7D26DBF45EAA3D3BA11B59E7A06_H
#ifndef DTDCHOICEAUTOMATA_T50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE_H
#define DTDCHOICEAUTOMATA_T50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDChoiceAutomata
struct  DTDChoiceAutomata_t50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE  : public DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7
{
public:
	// Mono.Xml.DTDAutomata Mono.Xml.DTDChoiceAutomata::left
	DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * ___left_1;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDChoiceAutomata::right
	DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * ___right_2;
	// System.Boolean Mono.Xml.DTDChoiceAutomata::hasComputedEmptiable
	bool ___hasComputedEmptiable_3;
	// System.Boolean Mono.Xml.DTDChoiceAutomata::cachedEmptiable
	bool ___cachedEmptiable_4;

public:
	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(DTDChoiceAutomata_t50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE, ___left_1)); }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * get_left_1() const { return ___left_1; }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 ** get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * value)
	{
		___left_1 = value;
		Il2CppCodeGenWriteBarrier((&___left_1), value);
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(DTDChoiceAutomata_t50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE, ___right_2)); }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * get_right_2() const { return ___right_2; }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 ** get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * value)
	{
		___right_2 = value;
		Il2CppCodeGenWriteBarrier((&___right_2), value);
	}

	inline static int32_t get_offset_of_hasComputedEmptiable_3() { return static_cast<int32_t>(offsetof(DTDChoiceAutomata_t50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE, ___hasComputedEmptiable_3)); }
	inline bool get_hasComputedEmptiable_3() const { return ___hasComputedEmptiable_3; }
	inline bool* get_address_of_hasComputedEmptiable_3() { return &___hasComputedEmptiable_3; }
	inline void set_hasComputedEmptiable_3(bool value)
	{
		___hasComputedEmptiable_3 = value;
	}

	inline static int32_t get_offset_of_cachedEmptiable_4() { return static_cast<int32_t>(offsetof(DTDChoiceAutomata_t50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE, ___cachedEmptiable_4)); }
	inline bool get_cachedEmptiable_4() const { return ___cachedEmptiable_4; }
	inline bool* get_address_of_cachedEmptiable_4() { return &___cachedEmptiable_4; }
	inline void set_cachedEmptiable_4(bool value)
	{
		___cachedEmptiable_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCHOICEAUTOMATA_T50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE_H
#ifndef DTDELEMENTAUTOMATA_T4F31CD3B4096E7EA16D973937D76EB96F23B1BD2_H
#define DTDELEMENTAUTOMATA_T4F31CD3B4096E7EA16D973937D76EB96F23B1BD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementAutomata
struct  DTDElementAutomata_t4F31CD3B4096E7EA16D973937D76EB96F23B1BD2  : public DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7
{
public:
	// System.String Mono.Xml.DTDElementAutomata::name
	String_t* ___name_1;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(DTDElementAutomata_t4F31CD3B4096E7EA16D973937D76EB96F23B1BD2, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTAUTOMATA_T4F31CD3B4096E7EA16D973937D76EB96F23B1BD2_H
#ifndef DTDEMPTYAUTOMATA_T055216A06878AB4743EF9FC55703460B24117B41_H
#define DTDEMPTYAUTOMATA_T055216A06878AB4743EF9FC55703460B24117B41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEmptyAutomata
struct  DTDEmptyAutomata_t055216A06878AB4743EF9FC55703460B24117B41  : public DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDEMPTYAUTOMATA_T055216A06878AB4743EF9FC55703460B24117B41_H
#ifndef DTDONEORMOREAUTOMATA_T4139380E3B7FCA01D730B0DE9133E1A69A32CBFD_H
#define DTDONEORMOREAUTOMATA_T4139380E3B7FCA01D730B0DE9133E1A69A32CBFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDOneOrMoreAutomata
struct  DTDOneOrMoreAutomata_t4139380E3B7FCA01D730B0DE9133E1A69A32CBFD  : public DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7
{
public:
	// Mono.Xml.DTDAutomata Mono.Xml.DTDOneOrMoreAutomata::children
	DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * ___children_1;

public:
	inline static int32_t get_offset_of_children_1() { return static_cast<int32_t>(offsetof(DTDOneOrMoreAutomata_t4139380E3B7FCA01D730B0DE9133E1A69A32CBFD, ___children_1)); }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * get_children_1() const { return ___children_1; }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 ** get_address_of_children_1() { return &___children_1; }
	inline void set_children_1(DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * value)
	{
		___children_1 = value;
		Il2CppCodeGenWriteBarrier((&___children_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDONEORMOREAUTOMATA_T4139380E3B7FCA01D730B0DE9133E1A69A32CBFD_H
#ifndef DTDSEQUENCEAUTOMATA_TD0DB5A3840DEAA9B4925544B078572C3D18433F8_H
#define DTDSEQUENCEAUTOMATA_TD0DB5A3840DEAA9B4925544B078572C3D18433F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDSequenceAutomata
struct  DTDSequenceAutomata_tD0DB5A3840DEAA9B4925544B078572C3D18433F8  : public DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7
{
public:
	// Mono.Xml.DTDAutomata Mono.Xml.DTDSequenceAutomata::left
	DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * ___left_1;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDSequenceAutomata::right
	DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * ___right_2;
	// System.Boolean Mono.Xml.DTDSequenceAutomata::hasComputedEmptiable
	bool ___hasComputedEmptiable_3;
	// System.Boolean Mono.Xml.DTDSequenceAutomata::cachedEmptiable
	bool ___cachedEmptiable_4;

public:
	inline static int32_t get_offset_of_left_1() { return static_cast<int32_t>(offsetof(DTDSequenceAutomata_tD0DB5A3840DEAA9B4925544B078572C3D18433F8, ___left_1)); }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * get_left_1() const { return ___left_1; }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 ** get_address_of_left_1() { return &___left_1; }
	inline void set_left_1(DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * value)
	{
		___left_1 = value;
		Il2CppCodeGenWriteBarrier((&___left_1), value);
	}

	inline static int32_t get_offset_of_right_2() { return static_cast<int32_t>(offsetof(DTDSequenceAutomata_tD0DB5A3840DEAA9B4925544B078572C3D18433F8, ___right_2)); }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * get_right_2() const { return ___right_2; }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 ** get_address_of_right_2() { return &___right_2; }
	inline void set_right_2(DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * value)
	{
		___right_2 = value;
		Il2CppCodeGenWriteBarrier((&___right_2), value);
	}

	inline static int32_t get_offset_of_hasComputedEmptiable_3() { return static_cast<int32_t>(offsetof(DTDSequenceAutomata_tD0DB5A3840DEAA9B4925544B078572C3D18433F8, ___hasComputedEmptiable_3)); }
	inline bool get_hasComputedEmptiable_3() const { return ___hasComputedEmptiable_3; }
	inline bool* get_address_of_hasComputedEmptiable_3() { return &___hasComputedEmptiable_3; }
	inline void set_hasComputedEmptiable_3(bool value)
	{
		___hasComputedEmptiable_3 = value;
	}

	inline static int32_t get_offset_of_cachedEmptiable_4() { return static_cast<int32_t>(offsetof(DTDSequenceAutomata_tD0DB5A3840DEAA9B4925544B078572C3D18433F8, ___cachedEmptiable_4)); }
	inline bool get_cachedEmptiable_4() const { return ___cachedEmptiable_4; }
	inline bool* get_address_of_cachedEmptiable_4() { return &___cachedEmptiable_4; }
	inline void set_cachedEmptiable_4(bool value)
	{
		___cachedEmptiable_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDSEQUENCEAUTOMATA_TD0DB5A3840DEAA9B4925544B078572C3D18433F8_H
#ifndef XSDALLVALIDATIONSTATE_T4ED82B2BF3F8403103526301F3BB097F4F951758_H
#define XSDALLVALIDATIONSTATE_T4ED82B2BF3F8403103526301F3BB097F4F951758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAllValidationState
struct  XsdAllValidationState_t4ED82B2BF3F8403103526301F3BB097F4F951758  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:
	// System.Xml.Schema.XmlSchemaAll Mono.Xml.Schema.XsdAllValidationState::all
	XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB * ___all_3;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdAllValidationState::consumed
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___consumed_4;

public:
	inline static int32_t get_offset_of_all_3() { return static_cast<int32_t>(offsetof(XsdAllValidationState_t4ED82B2BF3F8403103526301F3BB097F4F951758, ___all_3)); }
	inline XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB * get_all_3() const { return ___all_3; }
	inline XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB ** get_address_of_all_3() { return &___all_3; }
	inline void set_all_3(XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB * value)
	{
		___all_3 = value;
		Il2CppCodeGenWriteBarrier((&___all_3), value);
	}

	inline static int32_t get_offset_of_consumed_4() { return static_cast<int32_t>(offsetof(XsdAllValidationState_t4ED82B2BF3F8403103526301F3BB097F4F951758, ___consumed_4)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_consumed_4() const { return ___consumed_4; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_consumed_4() { return &___consumed_4; }
	inline void set_consumed_4(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___consumed_4 = value;
		Il2CppCodeGenWriteBarrier((&___consumed_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDALLVALIDATIONSTATE_T4ED82B2BF3F8403103526301F3BB097F4F951758_H
#ifndef XSDANYVALIDATIONSTATE_T6ABF8A32C88C9171AA284E91FB0ADED6F91267A3_H
#define XSDANYVALIDATIONSTATE_T6ABF8A32C88C9171AA284E91FB0ADED6F91267A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnyValidationState
struct  XsdAnyValidationState_t6ABF8A32C88C9171AA284E91FB0ADED6F91267A3  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:
	// System.Xml.Schema.XmlSchemaAny Mono.Xml.Schema.XsdAnyValidationState::any
	XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF * ___any_3;

public:
	inline static int32_t get_offset_of_any_3() { return static_cast<int32_t>(offsetof(XsdAnyValidationState_t6ABF8A32C88C9171AA284E91FB0ADED6F91267A3, ___any_3)); }
	inline XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF * get_any_3() const { return ___any_3; }
	inline XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF ** get_address_of_any_3() { return &___any_3; }
	inline void set_any_3(XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF * value)
	{
		___any_3 = value;
		Il2CppCodeGenWriteBarrier((&___any_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYVALIDATIONSTATE_T6ABF8A32C88C9171AA284E91FB0ADED6F91267A3_H
#ifndef XSDAPPENDEDVALIDATIONSTATE_TF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD_H
#define XSDAPPENDEDVALIDATIONSTATE_TF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAppendedValidationState
struct  XsdAppendedValidationState_tF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdAppendedValidationState::head
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * ___head_3;
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdAppendedValidationState::rest
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * ___rest_4;

public:
	inline static int32_t get_offset_of_head_3() { return static_cast<int32_t>(offsetof(XsdAppendedValidationState_tF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD, ___head_3)); }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * get_head_3() const { return ___head_3; }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 ** get_address_of_head_3() { return &___head_3; }
	inline void set_head_3(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * value)
	{
		___head_3 = value;
		Il2CppCodeGenWriteBarrier((&___head_3), value);
	}

	inline static int32_t get_offset_of_rest_4() { return static_cast<int32_t>(offsetof(XsdAppendedValidationState_tF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD, ___rest_4)); }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * get_rest_4() const { return ___rest_4; }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 ** get_address_of_rest_4() { return &___rest_4; }
	inline void set_rest_4(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * value)
	{
		___rest_4 = value;
		Il2CppCodeGenWriteBarrier((&___rest_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDAPPENDEDVALIDATIONSTATE_TF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD_H
#ifndef XSDCHOICEVALIDATIONSTATE_T6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2_H
#define XSDCHOICEVALIDATIONSTATE_T6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdChoiceValidationState
struct  XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:
	// System.Xml.Schema.XmlSchemaChoice Mono.Xml.Schema.XsdChoiceValidationState::choice
	XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5 * ___choice_3;
	// System.Boolean Mono.Xml.Schema.XsdChoiceValidationState::emptiable
	bool ___emptiable_4;
	// System.Boolean Mono.Xml.Schema.XsdChoiceValidationState::emptiableComputed
	bool ___emptiableComputed_5;

public:
	inline static int32_t get_offset_of_choice_3() { return static_cast<int32_t>(offsetof(XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2, ___choice_3)); }
	inline XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5 * get_choice_3() const { return ___choice_3; }
	inline XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5 ** get_address_of_choice_3() { return &___choice_3; }
	inline void set_choice_3(XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5 * value)
	{
		___choice_3 = value;
		Il2CppCodeGenWriteBarrier((&___choice_3), value);
	}

	inline static int32_t get_offset_of_emptiable_4() { return static_cast<int32_t>(offsetof(XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2, ___emptiable_4)); }
	inline bool get_emptiable_4() const { return ___emptiable_4; }
	inline bool* get_address_of_emptiable_4() { return &___emptiable_4; }
	inline void set_emptiable_4(bool value)
	{
		___emptiable_4 = value;
	}

	inline static int32_t get_offset_of_emptiableComputed_5() { return static_cast<int32_t>(offsetof(XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2, ___emptiableComputed_5)); }
	inline bool get_emptiableComputed_5() const { return ___emptiableComputed_5; }
	inline bool* get_address_of_emptiableComputed_5() { return &___emptiableComputed_5; }
	inline void set_emptiableComputed_5(bool value)
	{
		___emptiableComputed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDCHOICEVALIDATIONSTATE_T6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2_H
#ifndef XSDELEMENTVALIDATIONSTATE_TE3F398F18D4968EA5F91FEE92C646B19389C1C13_H
#define XSDELEMENTVALIDATIONSTATE_TE3F398F18D4968EA5F91FEE92C646B19389C1C13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdElementValidationState
struct  XsdElementValidationState_tE3F398F18D4968EA5F91FEE92C646B19389C1C13  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:
	// System.Xml.Schema.XmlSchemaElement Mono.Xml.Schema.XsdElementValidationState::element
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * ___element_3;

public:
	inline static int32_t get_offset_of_element_3() { return static_cast<int32_t>(offsetof(XsdElementValidationState_tE3F398F18D4968EA5F91FEE92C646B19389C1C13, ___element_3)); }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * get_element_3() const { return ___element_3; }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 ** get_address_of_element_3() { return &___element_3; }
	inline void set_element_3(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * value)
	{
		___element_3 = value;
		Il2CppCodeGenWriteBarrier((&___element_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDELEMENTVALIDATIONSTATE_TE3F398F18D4968EA5F91FEE92C646B19389C1C13_H
#ifndef XSDEMPTYVALIDATIONSTATE_T9F4AC1CC30BFB15E1B14F72386E85F2E78EFD045_H
#define XSDEMPTYVALIDATIONSTATE_T9F4AC1CC30BFB15E1B14F72386E85F2E78EFD045_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdEmptyValidationState
struct  XsdEmptyValidationState_t9F4AC1CC30BFB15E1B14F72386E85F2E78EFD045  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDEMPTYVALIDATIONSTATE_T9F4AC1CC30BFB15E1B14F72386E85F2E78EFD045_H
#ifndef XSDINVALIDVALIDATIONSTATE_T3532998F32C0C00F8E0F0C199D2308C867084FFF_H
#define XSDINVALIDVALIDATIONSTATE_T3532998F32C0C00F8E0F0C199D2308C867084FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInvalidValidationState
struct  XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINVALIDVALIDATIONSTATE_T3532998F32C0C00F8E0F0C199D2308C867084FFF_H
#ifndef XSDKEYENTRYCOLLECTION_TF37F133E46ECFF1F08CBB56894E1B18CB931B265_H
#define XSDKEYENTRYCOLLECTION_TF37F133E46ECFF1F08CBB56894E1B18CB931B265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntryCollection
struct  XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265  : public CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRYCOLLECTION_TF37F133E46ECFF1F08CBB56894E1B18CB931B265_H
#ifndef XSDKEYENTRYFIELDCOLLECTION_T8536E011BAA035EF7757EE7BBE9843160ABBF4EC_H
#define XSDKEYENTRYFIELDCOLLECTION_T8536E011BAA035EF7757EE7BBE9843160ABBF4EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdKeyEntryFieldCollection
struct  XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC  : public CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDKEYENTRYFIELDCOLLECTION_T8536E011BAA035EF7757EE7BBE9843160ABBF4EC_H
#ifndef XSDSEQUENCEVALIDATIONSTATE_TB6D68439C7FBDA9D3510C267EF0038D735D6658E_H
#define XSDSEQUENCEVALIDATIONSTATE_TB6D68439C7FBDA9D3510C267EF0038D735D6658E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdSequenceValidationState
struct  XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E  : public XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74
{
public:
	// System.Xml.Schema.XmlSchemaSequence Mono.Xml.Schema.XsdSequenceValidationState::seq
	XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483 * ___seq_3;
	// System.Int32 Mono.Xml.Schema.XsdSequenceValidationState::current
	int32_t ___current_4;
	// Mono.Xml.Schema.XsdValidationState Mono.Xml.Schema.XsdSequenceValidationState::currentAutomata
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * ___currentAutomata_5;
	// System.Boolean Mono.Xml.Schema.XsdSequenceValidationState::emptiable
	bool ___emptiable_6;

public:
	inline static int32_t get_offset_of_seq_3() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E, ___seq_3)); }
	inline XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483 * get_seq_3() const { return ___seq_3; }
	inline XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483 ** get_address_of_seq_3() { return &___seq_3; }
	inline void set_seq_3(XmlSchemaSequence_tC5A4D3A17AC43E34770AC810A3DDAFBC0D0C9483 * value)
	{
		___seq_3 = value;
		Il2CppCodeGenWriteBarrier((&___seq_3), value);
	}

	inline static int32_t get_offset_of_current_4() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E, ___current_4)); }
	inline int32_t get_current_4() const { return ___current_4; }
	inline int32_t* get_address_of_current_4() { return &___current_4; }
	inline void set_current_4(int32_t value)
	{
		___current_4 = value;
	}

	inline static int32_t get_offset_of_currentAutomata_5() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E, ___currentAutomata_5)); }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * get_currentAutomata_5() const { return ___currentAutomata_5; }
	inline XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 ** get_address_of_currentAutomata_5() { return &___currentAutomata_5; }
	inline void set_currentAutomata_5(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74 * value)
	{
		___currentAutomata_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentAutomata_5), value);
	}

	inline static int32_t get_offset_of_emptiable_6() { return static_cast<int32_t>(offsetof(XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E, ___emptiable_6)); }
	inline bool get_emptiable_6() const { return ___emptiable_6; }
	inline bool* get_address_of_emptiable_6() { return &___emptiable_6; }
	inline void set_emptiable_6(bool value)
	{
		___emptiable_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSEQUENCEVALIDATIONSTATE_TB6D68439C7FBDA9D3510C267EF0038D735D6658E_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#define INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_tC16F64335CE8B56D99229DE94BB3A876ED55FE87 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_tC16F64335CE8B56D99229DE94BB3A876ED55FE87, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MONOTODOATTRIBUTE_T2D21BFE6EE9459D34164629D1D797DBB606A57A0_H
#define MONOTODOATTRIBUTE_T2D21BFE6EE9459D34164629D1D797DBB606A57A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoTODOAttribute
struct  MonoTODOAttribute_t2D21BFE6EE9459D34164629D1D797DBB606A57A0  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTODOATTRIBUTE_T2D21BFE6EE9459D34164629D1D797DBB606A57A0_H
#ifndef SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#define SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifndef GROUP_T4D7149047343E5C195C706AC7222A4492D9D3A4C_H
#define GROUP_T4D7149047343E5C195C706AC7222A4492D9D3A4C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Group
struct  Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C  : public Capture_t426EB72741D828F4539C131E1DC839DD00F01F22
{
public:
	// System.Boolean System.Text.RegularExpressions.Group::success
	bool ___success_4;
	// System.Text.RegularExpressions.CaptureCollection System.Text.RegularExpressions.Group::captures
	CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20 * ___captures_5;

public:
	inline static int32_t get_offset_of_success_4() { return static_cast<int32_t>(offsetof(Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C, ___success_4)); }
	inline bool get_success_4() const { return ___success_4; }
	inline bool* get_address_of_success_4() { return &___success_4; }
	inline void set_success_4(bool value)
	{
		___success_4 = value;
	}

	inline static int32_t get_offset_of_captures_5() { return static_cast<int32_t>(offsetof(Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C, ___captures_5)); }
	inline CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20 * get_captures_5() const { return ___captures_5; }
	inline CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20 ** get_address_of_captures_5() { return &___captures_5; }
	inline void set_captures_5(CaptureCollection_t5F80E90F8954A443D29D1EBEFC7119831DFB9C20 * value)
	{
		___captures_5 = value;
		Il2CppCodeGenWriteBarrier((&___captures_5), value);
	}
};

struct Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C_StaticFields
{
public:
	// System.Text.RegularExpressions.Group System.Text.RegularExpressions.Group::Fail
	Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C * ___Fail_3;

public:
	inline static int32_t get_offset_of_Fail_3() { return static_cast<int32_t>(offsetof(Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C_StaticFields, ___Fail_3)); }
	inline Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C * get_Fail_3() const { return ___Fail_3; }
	inline Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C ** get_address_of_Fail_3() { return &___Fail_3; }
	inline void set_Fail_3(Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C * value)
	{
		___Fail_3 = value;
		Il2CppCodeGenWriteBarrier((&___Fail_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUP_T4D7149047343E5C195C706AC7222A4492D9D3A4C_H
#ifndef INTSTACK_T1716D1B970D9D96F49C07E230CFA994086B6F83D_H
#define INTSTACK_T1716D1B970D9D96F49C07E230CFA994086B6F83D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Interpreter_IntStack
struct  IntStack_t1716D1B970D9D96F49C07E230CFA994086B6F83D 
{
public:
	// System.Int32[] System.Text.RegularExpressions.Interpreter_IntStack::values
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___values_0;
	// System.Int32 System.Text.RegularExpressions.Interpreter_IntStack::count
	int32_t ___count_1;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(IntStack_t1716D1B970D9D96F49C07E230CFA994086B6F83D, ___values_0)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_values_0() const { return ___values_0; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(IntStack_t1716D1B970D9D96F49C07E230CFA994086B6F83D, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTSTACK_T1716D1B970D9D96F49C07E230CFA994086B6F83D_H
#ifndef INTERVAL_T3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F_H
#define INTERVAL_T3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Interval
struct  Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F 
{
public:
	// System.Int32 System.Text.RegularExpressions.Interval::low
	int32_t ___low_0;
	// System.Int32 System.Text.RegularExpressions.Interval::high
	int32_t ___high_1;
	// System.Boolean System.Text.RegularExpressions.Interval::contiguous
	bool ___contiguous_2;

public:
	inline static int32_t get_offset_of_low_0() { return static_cast<int32_t>(offsetof(Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F, ___low_0)); }
	inline int32_t get_low_0() const { return ___low_0; }
	inline int32_t* get_address_of_low_0() { return &___low_0; }
	inline void set_low_0(int32_t value)
	{
		___low_0 = value;
	}

	inline static int32_t get_offset_of_high_1() { return static_cast<int32_t>(offsetof(Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F, ___high_1)); }
	inline int32_t get_high_1() const { return ___high_1; }
	inline int32_t* get_address_of_high_1() { return &___high_1; }
	inline void set_high_1(int32_t value)
	{
		___high_1 = value;
	}

	inline static int32_t get_offset_of_contiguous_2() { return static_cast<int32_t>(offsetof(Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F, ___contiguous_2)); }
	inline bool get_contiguous_2() const { return ___contiguous_2; }
	inline bool* get_address_of_contiguous_2() { return &___contiguous_2; }
	inline void set_contiguous_2(bool value)
	{
		___contiguous_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Text.RegularExpressions.Interval
struct Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F_marshaled_pinvoke
{
	int32_t ___low_0;
	int32_t ___high_1;
	int32_t ___contiguous_2;
};
// Native definition for COM marshalling of System.Text.RegularExpressions.Interval
struct Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F_marshaled_com
{
	int32_t ___low_0;
	int32_t ___high_1;
	int32_t ___contiguous_2;
};
#endif // INTERVAL_T3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F_H
#ifndef LINKSTACK_T71FC86A248572C5736999BDA60C154F0AD923949_H
#define LINKSTACK_T71FC86A248572C5736999BDA60C154F0AD923949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.LinkStack
struct  LinkStack_t71FC86A248572C5736999BDA60C154F0AD923949  : public LinkRef_t4A317D95D5F6964647AE5BBB13F5CAE3F1C81FAB
{
public:
	// System.Collections.Stack System.Text.RegularExpressions.LinkStack::stack
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___stack_0;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(LinkStack_t71FC86A248572C5736999BDA60C154F0AD923949, ___stack_0)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_stack_0() const { return ___stack_0; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___stack_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKSTACK_T71FC86A248572C5736999BDA60C154F0AD923949_H
#ifndef MARK_T6F95F4C0F20F9D6B1059C179A2E3940267A6672F_H
#define MARK_T6F95F4C0F20F9D6B1059C179A2E3940267A6672F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Mark
struct  Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F 
{
public:
	// System.Int32 System.Text.RegularExpressions.Mark::Start
	int32_t ___Start_0;
	// System.Int32 System.Text.RegularExpressions.Mark::End
	int32_t ___End_1;
	// System.Int32 System.Text.RegularExpressions.Mark::Previous
	int32_t ___Previous_2;

public:
	inline static int32_t get_offset_of_Start_0() { return static_cast<int32_t>(offsetof(Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F, ___Start_0)); }
	inline int32_t get_Start_0() const { return ___Start_0; }
	inline int32_t* get_address_of_Start_0() { return &___Start_0; }
	inline void set_Start_0(int32_t value)
	{
		___Start_0 = value;
	}

	inline static int32_t get_offset_of_End_1() { return static_cast<int32_t>(offsetof(Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F, ___End_1)); }
	inline int32_t get_End_1() const { return ___End_1; }
	inline int32_t* get_address_of_End_1() { return &___End_1; }
	inline void set_End_1(int32_t value)
	{
		___End_1 = value;
	}

	inline static int32_t get_offset_of_Previous_2() { return static_cast<int32_t>(offsetof(Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F, ___Previous_2)); }
	inline int32_t get_Previous_2() const { return ___Previous_2; }
	inline int32_t* get_address_of_Previous_2() { return &___Previous_2; }
	inline void set_Previous_2(int32_t value)
	{
		___Previous_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARK_T6F95F4C0F20F9D6B1059C179A2E3940267A6672F_H
#ifndef INTSTACK_T70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE_H
#define INTSTACK_T70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RxInterpreter_IntStack
struct  IntStack_t70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE 
{
public:
	// System.Int32[] System.Text.RegularExpressions.RxInterpreter_IntStack::values
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___values_0;
	// System.Int32 System.Text.RegularExpressions.RxInterpreter_IntStack::count
	int32_t ___count_1;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(IntStack_t70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE, ___values_0)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_values_0() const { return ___values_0; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(IntStack_t70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTSTACK_T70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE_H
#ifndef RXLINKREF_TE55F192CDD19A7BEF91C841779877F95DC8DED89_H
#define RXLINKREF_TE55F192CDD19A7BEF91C841779877F95DC8DED89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RxLinkRef
struct  RxLinkRef_tE55F192CDD19A7BEF91C841779877F95DC8DED89  : public LinkRef_t4A317D95D5F6964647AE5BBB13F5CAE3F1C81FAB
{
public:
	// System.Int32[] System.Text.RegularExpressions.RxLinkRef::offsets
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___offsets_0;
	// System.Int32 System.Text.RegularExpressions.RxLinkRef::current
	int32_t ___current_1;

public:
	inline static int32_t get_offset_of_offsets_0() { return static_cast<int32_t>(offsetof(RxLinkRef_tE55F192CDD19A7BEF91C841779877F95DC8DED89, ___offsets_0)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_offsets_0() const { return ___offsets_0; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_offsets_0() { return &___offsets_0; }
	inline void set_offsets_0(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___offsets_0 = value;
		Il2CppCodeGenWriteBarrier((&___offsets_0), value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(RxLinkRef_tE55F192CDD19A7BEF91C841779877F95DC8DED89, ___current_1)); }
	inline int32_t get_current_1() const { return ___current_1; }
	inline int32_t* get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(int32_t value)
	{
		___current_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RXLINKREF_TE55F192CDD19A7BEF91C841779877F95DC8DED89_H
#ifndef COMPOSITEEXPRESSION_T750AF90954FAB323F201EA628A6123FCAC3A46EC_H
#define COMPOSITEEXPRESSION_T750AF90954FAB323F201EA628A6123FCAC3A46EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.CompositeExpression
struct  CompositeExpression_t750AF90954FAB323F201EA628A6123FCAC3A46EC  : public Expression_t6A43E18B89A25A32D44C3FD396F6139A8AA73F94
{
public:
	// System.Text.RegularExpressions.Syntax.ExpressionCollection System.Text.RegularExpressions.Syntax.CompositeExpression::expressions
	ExpressionCollection_tCA7FC699936CBAE10FB637D8927714CD3AEA282A * ___expressions_0;

public:
	inline static int32_t get_offset_of_expressions_0() { return static_cast<int32_t>(offsetof(CompositeExpression_t750AF90954FAB323F201EA628A6123FCAC3A46EC, ___expressions_0)); }
	inline ExpressionCollection_tCA7FC699936CBAE10FB637D8927714CD3AEA282A * get_expressions_0() const { return ___expressions_0; }
	inline ExpressionCollection_tCA7FC699936CBAE10FB637D8927714CD3AEA282A ** get_address_of_expressions_0() { return &___expressions_0; }
	inline void set_expressions_0(ExpressionCollection_tCA7FC699936CBAE10FB637D8927714CD3AEA282A * value)
	{
		___expressions_0 = value;
		Il2CppCodeGenWriteBarrier((&___expressions_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSITEEXPRESSION_T750AF90954FAB323F201EA628A6123FCAC3A46EC_H
#ifndef EXPRESSIONCOLLECTION_TCA7FC699936CBAE10FB637D8927714CD3AEA282A_H
#define EXPRESSIONCOLLECTION_TCA7FC699936CBAE10FB637D8927714CD3AEA282A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.ExpressionCollection
struct  ExpressionCollection_tCA7FC699936CBAE10FB637D8927714CD3AEA282A  : public CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONCOLLECTION_TCA7FC699936CBAE10FB637D8927714CD3AEA282A_H
#ifndef LITERAL_TC4E6C9861C7A338A35934AA8ECC758AC63DC587A_H
#define LITERAL_TC4E6C9861C7A338A35934AA8ECC758AC63DC587A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.Literal
struct  Literal_tC4E6C9861C7A338A35934AA8ECC758AC63DC587A  : public Expression_t6A43E18B89A25A32D44C3FD396F6139A8AA73F94
{
public:
	// System.String System.Text.RegularExpressions.Syntax.Literal::str
	String_t* ___str_0;
	// System.Boolean System.Text.RegularExpressions.Syntax.Literal::ignore
	bool ___ignore_1;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(Literal_tC4E6C9861C7A338A35934AA8ECC758AC63DC587A, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}

	inline static int32_t get_offset_of_ignore_1() { return static_cast<int32_t>(offsetof(Literal_tC4E6C9861C7A338A35934AA8ECC758AC63DC587A, ___ignore_1)); }
	inline bool get_ignore_1() const { return ___ignore_1; }
	inline bool* get_address_of_ignore_1() { return &___ignore_1; }
	inline void set_ignore_1(bool value)
	{
		___ignore_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LITERAL_TC4E6C9861C7A338A35934AA8ECC758AC63DC587A_H
#ifndef REFERENCE_T683D20A9EF06251885F0ACC287F31F5B670F47A6_H
#define REFERENCE_T683D20A9EF06251885F0ACC287F31F5B670F47A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.Reference
struct  Reference_t683D20A9EF06251885F0ACC287F31F5B670F47A6  : public Expression_t6A43E18B89A25A32D44C3FD396F6139A8AA73F94
{
public:
	// System.Text.RegularExpressions.Syntax.CapturingGroup System.Text.RegularExpressions.Syntax.Reference::group
	CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7 * ___group_0;
	// System.Boolean System.Text.RegularExpressions.Syntax.Reference::ignore
	bool ___ignore_1;

public:
	inline static int32_t get_offset_of_group_0() { return static_cast<int32_t>(offsetof(Reference_t683D20A9EF06251885F0ACC287F31F5B670F47A6, ___group_0)); }
	inline CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7 * get_group_0() const { return ___group_0; }
	inline CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7 ** get_address_of_group_0() { return &___group_0; }
	inline void set_group_0(CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7 * value)
	{
		___group_0 = value;
		Il2CppCodeGenWriteBarrier((&___group_0), value);
	}

	inline static int32_t get_offset_of_ignore_1() { return static_cast<int32_t>(offsetof(Reference_t683D20A9EF06251885F0ACC287F31F5B670F47A6, ___ignore_1)); }
	inline bool get_ignore_1() const { return ___ignore_1; }
	inline bool* get_address_of_ignore_1() { return &___ignore_1; }
	inline void set_ignore_1(bool value)
	{
		___ignore_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCE_T683D20A9EF06251885F0ACC287F31F5B670F47A6_H
#ifndef URISCHEME_TAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE_H
#define URISCHEME_TAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri_UriScheme
struct  UriScheme_tAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE 
{
public:
	// System.String System.Uri_UriScheme::scheme
	String_t* ___scheme_0;
	// System.String System.Uri_UriScheme::delimiter
	String_t* ___delimiter_1;
	// System.Int32 System.Uri_UriScheme::defaultPort
	int32_t ___defaultPort_2;

public:
	inline static int32_t get_offset_of_scheme_0() { return static_cast<int32_t>(offsetof(UriScheme_tAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE, ___scheme_0)); }
	inline String_t* get_scheme_0() const { return ___scheme_0; }
	inline String_t** get_address_of_scheme_0() { return &___scheme_0; }
	inline void set_scheme_0(String_t* value)
	{
		___scheme_0 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_0), value);
	}

	inline static int32_t get_offset_of_delimiter_1() { return static_cast<int32_t>(offsetof(UriScheme_tAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE, ___delimiter_1)); }
	inline String_t* get_delimiter_1() const { return ___delimiter_1; }
	inline String_t** get_address_of_delimiter_1() { return &___delimiter_1; }
	inline void set_delimiter_1(String_t* value)
	{
		___delimiter_1 = value;
		Il2CppCodeGenWriteBarrier((&___delimiter_1), value);
	}

	inline static int32_t get_offset_of_defaultPort_2() { return static_cast<int32_t>(offsetof(UriScheme_tAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE, ___defaultPort_2)); }
	inline int32_t get_defaultPort_2() const { return ___defaultPort_2; }
	inline int32_t* get_address_of_defaultPort_2() { return &___defaultPort_2; }
	inline void set_defaultPort_2(int32_t value)
	{
		___defaultPort_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Uri/UriScheme
struct UriScheme_tAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE_marshaled_pinvoke
{
	char* ___scheme_0;
	char* ___delimiter_1;
	int32_t ___defaultPort_2;
};
// Native definition for COM marshalling of System.Uri/UriScheme
struct UriScheme_tAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE_marshaled_com
{
	Il2CppChar* ___scheme_0;
	Il2CppChar* ___delimiter_1;
	int32_t ___defaultPort_2;
};
#endif // URISCHEME_TAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE_H
#ifndef URITYPECONVERTER_TA85B11B1FD8D9C1FD1FF5DE1FCA9AAE13CCF1E64_H
#define URITYPECONVERTER_TA85B11B1FD8D9C1FD1FF5DE1FCA9AAE13CCF1E64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriTypeConverter
struct  UriTypeConverter_tA85B11B1FD8D9C1FD1FF5DE1FCA9AAE13CCF1E64  : public TypeConverter_t5801C9F7100E1D849000ED0914E01E4CB2541B71
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URITYPECONVERTER_TA85B11B1FD8D9C1FD1FF5DE1FCA9AAE13CCF1E64_H
#ifndef VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#define VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_tDB81A15FA2AB53E2401A76B745D215397B29F783 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_StaticFields
{
public:
	// <PrivateImplementationDetails>_U24ArrayTypeU2416 <PrivateImplementationDetails>::U24U24fieldU2D1
	U24ArrayTypeU2416_t2DDBD265BE6606DAC46187FDC58335362DC6AE4C  ___U24U24fieldU2D1_0;
	// <PrivateImplementationDetails>_U24ArrayTypeU24128 <PrivateImplementationDetails>::U24U24fieldU2D3
	U24ArrayTypeU24128_tC2FBD9B9E13A39460761EEDD6034845832CDBAD7  ___U24U24fieldU2D3_1;
	// <PrivateImplementationDetails>_U24ArrayTypeU2412 <PrivateImplementationDetails>::U24U24fieldU2D4
	U24ArrayTypeU2412_t91D3DF413568F08A27A79C7AD08EB23506A60F51  ___U24U24fieldU2D4_2;
	// <PrivateImplementationDetails>_U24ArrayTypeU2412 <PrivateImplementationDetails>::U24U24fieldU2D5
	U24ArrayTypeU2412_t91D3DF413568F08A27A79C7AD08EB23506A60F51  ___U24U24fieldU2D5_3;

public:
	inline static int32_t get_offset_of_U24U24fieldU2D1_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_StaticFields, ___U24U24fieldU2D1_0)); }
	inline U24ArrayTypeU2416_t2DDBD265BE6606DAC46187FDC58335362DC6AE4C  get_U24U24fieldU2D1_0() const { return ___U24U24fieldU2D1_0; }
	inline U24ArrayTypeU2416_t2DDBD265BE6606DAC46187FDC58335362DC6AE4C * get_address_of_U24U24fieldU2D1_0() { return &___U24U24fieldU2D1_0; }
	inline void set_U24U24fieldU2D1_0(U24ArrayTypeU2416_t2DDBD265BE6606DAC46187FDC58335362DC6AE4C  value)
	{
		___U24U24fieldU2D1_0 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D3_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_StaticFields, ___U24U24fieldU2D3_1)); }
	inline U24ArrayTypeU24128_tC2FBD9B9E13A39460761EEDD6034845832CDBAD7  get_U24U24fieldU2D3_1() const { return ___U24U24fieldU2D3_1; }
	inline U24ArrayTypeU24128_tC2FBD9B9E13A39460761EEDD6034845832CDBAD7 * get_address_of_U24U24fieldU2D3_1() { return &___U24U24fieldU2D3_1; }
	inline void set_U24U24fieldU2D3_1(U24ArrayTypeU24128_tC2FBD9B9E13A39460761EEDD6034845832CDBAD7  value)
	{
		___U24U24fieldU2D3_1 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D4_2() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_StaticFields, ___U24U24fieldU2D4_2)); }
	inline U24ArrayTypeU2412_t91D3DF413568F08A27A79C7AD08EB23506A60F51  get_U24U24fieldU2D4_2() const { return ___U24U24fieldU2D4_2; }
	inline U24ArrayTypeU2412_t91D3DF413568F08A27A79C7AD08EB23506A60F51 * get_address_of_U24U24fieldU2D4_2() { return &___U24U24fieldU2D4_2; }
	inline void set_U24U24fieldU2D4_2(U24ArrayTypeU2412_t91D3DF413568F08A27A79C7AD08EB23506A60F51  value)
	{
		___U24U24fieldU2D4_2 = value;
	}

	inline static int32_t get_offset_of_U24U24fieldU2D5_3() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_StaticFields, ___U24U24fieldU2D5_3)); }
	inline U24ArrayTypeU2412_t91D3DF413568F08A27A79C7AD08EB23506A60F51  get_U24U24fieldU2D5_3() const { return ___U24U24fieldU2D5_3; }
	inline U24ArrayTypeU2412_t91D3DF413568F08A27A79C7AD08EB23506A60F51 * get_address_of_U24U24fieldU2D5_3() { return &___U24U24fieldU2D5_3; }
	inline void set_U24U24fieldU2D5_3(U24ArrayTypeU2412_t91D3DF413568F08A27A79C7AD08EB23506A60F51  value)
	{
		___U24U24fieldU2D5_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_8)); }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * get_data_8() const { return ___data_8; }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T_H
#ifndef FORMATEXCEPTION_T6148832BF22FDB9F1FB5BE067694E8326F42D592_H
#define FORMATEXCEPTION_T6148832BF22FDB9F1FB5BE067694E8326F42D592_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.FormatException
struct  FormatException_t6148832BF22FDB9F1FB5BE067694E8326F42D592  : public SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T6148832BF22FDB9F1FB5BE067694E8326F42D592_H
#ifndef SSLPOLICYERRORS_T821E0247A29EDB8CD8327153A81349E907DBE6E9_H
#define SSLPOLICYERRORS_T821E0247A29EDB8CD8327153A81349E907DBE6E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.SslPolicyErrors
struct  SslPolicyErrors_t821E0247A29EDB8CD8327153A81349E907DBE6E9 
{
public:
	// System.Int32 System.Net.Security.SslPolicyErrors::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SslPolicyErrors_t821E0247A29EDB8CD8327153A81349E907DBE6E9, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSLPOLICYERRORS_T821E0247A29EDB8CD8327153A81349E907DBE6E9_H
#ifndef INTERPRETER_TA670BA400C626675069426A40EF3981BAC726E63_H
#define INTERPRETER_TA670BA400C626675069426A40EF3981BAC726E63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Interpreter
struct  Interpreter_tA670BA400C626675069426A40EF3981BAC726E63  : public BaseMachine_t7846A209B98125A8E5B5FAFE5258CC5CBFE2F857
{
public:
	// System.UInt16[] System.Text.RegularExpressions.Interpreter::program
	UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC* ___program_1;
	// System.Int32 System.Text.RegularExpressions.Interpreter::program_start
	int32_t ___program_start_2;
	// System.String System.Text.RegularExpressions.Interpreter::text
	String_t* ___text_3;
	// System.Int32 System.Text.RegularExpressions.Interpreter::text_end
	int32_t ___text_end_4;
	// System.Int32 System.Text.RegularExpressions.Interpreter::group_count
	int32_t ___group_count_5;
	// System.Int32 System.Text.RegularExpressions.Interpreter::match_min
	int32_t ___match_min_6;
	// System.Text.RegularExpressions.QuickSearch System.Text.RegularExpressions.Interpreter::qs
	QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849 * ___qs_7;
	// System.Int32 System.Text.RegularExpressions.Interpreter::scan_ptr
	int32_t ___scan_ptr_8;
	// System.Text.RegularExpressions.Interpreter_RepeatContext System.Text.RegularExpressions.Interpreter::repeat
	RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 * ___repeat_9;
	// System.Text.RegularExpressions.Interpreter_RepeatContext System.Text.RegularExpressions.Interpreter::fast
	RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 * ___fast_10;
	// System.Text.RegularExpressions.Interpreter_IntStack System.Text.RegularExpressions.Interpreter::stack
	IntStack_t1716D1B970D9D96F49C07E230CFA994086B6F83D  ___stack_11;
	// System.Text.RegularExpressions.Interpreter_RepeatContext System.Text.RegularExpressions.Interpreter::deep
	RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 * ___deep_12;
	// System.Text.RegularExpressions.Mark[] System.Text.RegularExpressions.Interpreter::marks
	MarkU5BU5D_t528ADB54CF1308C521A7235B7686FF22BF928765* ___marks_13;
	// System.Int32 System.Text.RegularExpressions.Interpreter::mark_start
	int32_t ___mark_start_14;
	// System.Int32 System.Text.RegularExpressions.Interpreter::mark_end
	int32_t ___mark_end_15;
	// System.Int32[] System.Text.RegularExpressions.Interpreter::groups
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___groups_16;

public:
	inline static int32_t get_offset_of_program_1() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___program_1)); }
	inline UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC* get_program_1() const { return ___program_1; }
	inline UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC** get_address_of_program_1() { return &___program_1; }
	inline void set_program_1(UInt16U5BU5D_tFACC39F5AB94FCCEAD5ED082922B0263437F9ABC* value)
	{
		___program_1 = value;
		Il2CppCodeGenWriteBarrier((&___program_1), value);
	}

	inline static int32_t get_offset_of_program_start_2() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___program_start_2)); }
	inline int32_t get_program_start_2() const { return ___program_start_2; }
	inline int32_t* get_address_of_program_start_2() { return &___program_start_2; }
	inline void set_program_start_2(int32_t value)
	{
		___program_start_2 = value;
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___text_3)); }
	inline String_t* get_text_3() const { return ___text_3; }
	inline String_t** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(String_t* value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}

	inline static int32_t get_offset_of_text_end_4() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___text_end_4)); }
	inline int32_t get_text_end_4() const { return ___text_end_4; }
	inline int32_t* get_address_of_text_end_4() { return &___text_end_4; }
	inline void set_text_end_4(int32_t value)
	{
		___text_end_4 = value;
	}

	inline static int32_t get_offset_of_group_count_5() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___group_count_5)); }
	inline int32_t get_group_count_5() const { return ___group_count_5; }
	inline int32_t* get_address_of_group_count_5() { return &___group_count_5; }
	inline void set_group_count_5(int32_t value)
	{
		___group_count_5 = value;
	}

	inline static int32_t get_offset_of_match_min_6() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___match_min_6)); }
	inline int32_t get_match_min_6() const { return ___match_min_6; }
	inline int32_t* get_address_of_match_min_6() { return &___match_min_6; }
	inline void set_match_min_6(int32_t value)
	{
		___match_min_6 = value;
	}

	inline static int32_t get_offset_of_qs_7() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___qs_7)); }
	inline QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849 * get_qs_7() const { return ___qs_7; }
	inline QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849 ** get_address_of_qs_7() { return &___qs_7; }
	inline void set_qs_7(QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849 * value)
	{
		___qs_7 = value;
		Il2CppCodeGenWriteBarrier((&___qs_7), value);
	}

	inline static int32_t get_offset_of_scan_ptr_8() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___scan_ptr_8)); }
	inline int32_t get_scan_ptr_8() const { return ___scan_ptr_8; }
	inline int32_t* get_address_of_scan_ptr_8() { return &___scan_ptr_8; }
	inline void set_scan_ptr_8(int32_t value)
	{
		___scan_ptr_8 = value;
	}

	inline static int32_t get_offset_of_repeat_9() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___repeat_9)); }
	inline RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 * get_repeat_9() const { return ___repeat_9; }
	inline RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 ** get_address_of_repeat_9() { return &___repeat_9; }
	inline void set_repeat_9(RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 * value)
	{
		___repeat_9 = value;
		Il2CppCodeGenWriteBarrier((&___repeat_9), value);
	}

	inline static int32_t get_offset_of_fast_10() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___fast_10)); }
	inline RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 * get_fast_10() const { return ___fast_10; }
	inline RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 ** get_address_of_fast_10() { return &___fast_10; }
	inline void set_fast_10(RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 * value)
	{
		___fast_10 = value;
		Il2CppCodeGenWriteBarrier((&___fast_10), value);
	}

	inline static int32_t get_offset_of_stack_11() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___stack_11)); }
	inline IntStack_t1716D1B970D9D96F49C07E230CFA994086B6F83D  get_stack_11() const { return ___stack_11; }
	inline IntStack_t1716D1B970D9D96F49C07E230CFA994086B6F83D * get_address_of_stack_11() { return &___stack_11; }
	inline void set_stack_11(IntStack_t1716D1B970D9D96F49C07E230CFA994086B6F83D  value)
	{
		___stack_11 = value;
	}

	inline static int32_t get_offset_of_deep_12() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___deep_12)); }
	inline RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 * get_deep_12() const { return ___deep_12; }
	inline RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 ** get_address_of_deep_12() { return &___deep_12; }
	inline void set_deep_12(RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938 * value)
	{
		___deep_12 = value;
		Il2CppCodeGenWriteBarrier((&___deep_12), value);
	}

	inline static int32_t get_offset_of_marks_13() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___marks_13)); }
	inline MarkU5BU5D_t528ADB54CF1308C521A7235B7686FF22BF928765* get_marks_13() const { return ___marks_13; }
	inline MarkU5BU5D_t528ADB54CF1308C521A7235B7686FF22BF928765** get_address_of_marks_13() { return &___marks_13; }
	inline void set_marks_13(MarkU5BU5D_t528ADB54CF1308C521A7235B7686FF22BF928765* value)
	{
		___marks_13 = value;
		Il2CppCodeGenWriteBarrier((&___marks_13), value);
	}

	inline static int32_t get_offset_of_mark_start_14() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___mark_start_14)); }
	inline int32_t get_mark_start_14() const { return ___mark_start_14; }
	inline int32_t* get_address_of_mark_start_14() { return &___mark_start_14; }
	inline void set_mark_start_14(int32_t value)
	{
		___mark_start_14 = value;
	}

	inline static int32_t get_offset_of_mark_end_15() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___mark_end_15)); }
	inline int32_t get_mark_end_15() const { return ___mark_end_15; }
	inline int32_t* get_address_of_mark_end_15() { return &___mark_end_15; }
	inline void set_mark_end_15(int32_t value)
	{
		___mark_end_15 = value;
	}

	inline static int32_t get_offset_of_groups_16() { return static_cast<int32_t>(offsetof(Interpreter_tA670BA400C626675069426A40EF3981BAC726E63, ___groups_16)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_groups_16() const { return ___groups_16; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_groups_16() { return &___groups_16; }
	inline void set_groups_16(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___groups_16 = value;
		Il2CppCodeGenWriteBarrier((&___groups_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPRETER_TA670BA400C626675069426A40EF3981BAC726E63_H
#ifndef MODE_T2F28EDCB4C766AF8BF5FC594CBE2BDF7FAFE4715_H
#define MODE_T2F28EDCB4C766AF8BF5FC594CBE2BDF7FAFE4715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Interpreter_Mode
struct  Mode_t2F28EDCB4C766AF8BF5FC594CBE2BDF7FAFE4715 
{
public:
	// System.Int32 System.Text.RegularExpressions.Interpreter_Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t2F28EDCB4C766AF8BF5FC594CBE2BDF7FAFE4715, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T2F28EDCB4C766AF8BF5FC594CBE2BDF7FAFE4715_H
#ifndef MATCH_T2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_H
#define MATCH_T2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Match
struct  Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53  : public Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C
{
public:
	// System.Text.RegularExpressions.Regex System.Text.RegularExpressions.Match::regex
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * ___regex_6;
	// System.Text.RegularExpressions.IMachine System.Text.RegularExpressions.Match::machine
	RuntimeObject* ___machine_7;
	// System.Int32 System.Text.RegularExpressions.Match::text_length
	int32_t ___text_length_8;
	// System.Text.RegularExpressions.GroupCollection System.Text.RegularExpressions.Match::groups
	GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968 * ___groups_9;

public:
	inline static int32_t get_offset_of_regex_6() { return static_cast<int32_t>(offsetof(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53, ___regex_6)); }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * get_regex_6() const { return ___regex_6; }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B ** get_address_of_regex_6() { return &___regex_6; }
	inline void set_regex_6(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * value)
	{
		___regex_6 = value;
		Il2CppCodeGenWriteBarrier((&___regex_6), value);
	}

	inline static int32_t get_offset_of_machine_7() { return static_cast<int32_t>(offsetof(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53, ___machine_7)); }
	inline RuntimeObject* get_machine_7() const { return ___machine_7; }
	inline RuntimeObject** get_address_of_machine_7() { return &___machine_7; }
	inline void set_machine_7(RuntimeObject* value)
	{
		___machine_7 = value;
		Il2CppCodeGenWriteBarrier((&___machine_7), value);
	}

	inline static int32_t get_offset_of_text_length_8() { return static_cast<int32_t>(offsetof(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53, ___text_length_8)); }
	inline int32_t get_text_length_8() const { return ___text_length_8; }
	inline int32_t* get_address_of_text_length_8() { return &___text_length_8; }
	inline void set_text_length_8(int32_t value)
	{
		___text_length_8 = value;
	}

	inline static int32_t get_offset_of_groups_9() { return static_cast<int32_t>(offsetof(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53, ___groups_9)); }
	inline GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968 * get_groups_9() const { return ___groups_9; }
	inline GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968 ** get_address_of_groups_9() { return &___groups_9; }
	inline void set_groups_9(GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968 * value)
	{
		___groups_9 = value;
		Il2CppCodeGenWriteBarrier((&___groups_9), value);
	}
};

struct Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_StaticFields
{
public:
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.Match::empty
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 * ___empty_10;

public:
	inline static int32_t get_offset_of_empty_10() { return static_cast<int32_t>(offsetof(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_StaticFields, ___empty_10)); }
	inline Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 * get_empty_10() const { return ___empty_10; }
	inline Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 ** get_address_of_empty_10() { return &___empty_10; }
	inline void set_empty_10(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53 * value)
	{
		___empty_10 = value;
		Il2CppCodeGenWriteBarrier((&___empty_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATCH_T2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_H
#ifndef POSITION_T19E037E87BCB1E737603EA15C59A22D1C2E34C1C_H
#define POSITION_T19E037E87BCB1E737603EA15C59A22D1C2E34C1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Position
struct  Position_t19E037E87BCB1E737603EA15C59A22D1C2E34C1C 
{
public:
	// System.UInt16 System.Text.RegularExpressions.Position::value__
	uint16_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Position_t19E037E87BCB1E737603EA15C59A22D1C2E34C1C, ___value___1)); }
	inline uint16_t get_value___1() const { return ___value___1; }
	inline uint16_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint16_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITION_T19E037E87BCB1E737603EA15C59A22D1C2E34C1C_H
#ifndef REGEXOPTIONS_T6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624_H
#define REGEXOPTIONS_T6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexOptions
struct  RegexOptions_t6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624 
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RegexOptions_t6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXOPTIONS_T6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624_H
#ifndef RXINTERPRETER_TE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740_H
#define RXINTERPRETER_TE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RxInterpreter
struct  RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740  : public BaseMachine_t7846A209B98125A8E5B5FAFE5258CC5CBFE2F857
{
public:
	// System.Byte[] System.Text.RegularExpressions.RxInterpreter::program
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___program_1;
	// System.String System.Text.RegularExpressions.RxInterpreter::str
	String_t* ___str_2;
	// System.Int32 System.Text.RegularExpressions.RxInterpreter::string_start
	int32_t ___string_start_3;
	// System.Int32 System.Text.RegularExpressions.RxInterpreter::string_end
	int32_t ___string_end_4;
	// System.Int32 System.Text.RegularExpressions.RxInterpreter::group_count
	int32_t ___group_count_5;
	// System.Int32[] System.Text.RegularExpressions.RxInterpreter::groups
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___groups_6;
	// System.Text.RegularExpressions.EvalDelegate System.Text.RegularExpressions.RxInterpreter::eval_del
	EvalDelegate_tEAE36D514400FE58E4761FEDBB29920820C4B2FB * ___eval_del_7;
	// System.Text.RegularExpressions.Mark[] System.Text.RegularExpressions.RxInterpreter::marks
	MarkU5BU5D_t528ADB54CF1308C521A7235B7686FF22BF928765* ___marks_8;
	// System.Int32 System.Text.RegularExpressions.RxInterpreter::mark_start
	int32_t ___mark_start_9;
	// System.Int32 System.Text.RegularExpressions.RxInterpreter::mark_end
	int32_t ___mark_end_10;
	// System.Text.RegularExpressions.RxInterpreter_IntStack System.Text.RegularExpressions.RxInterpreter::stack
	IntStack_t70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE  ___stack_11;
	// System.Text.RegularExpressions.RxInterpreter_RepeatContext System.Text.RegularExpressions.RxInterpreter::repeat
	RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622 * ___repeat_12;
	// System.Text.RegularExpressions.RxInterpreter_RepeatContext System.Text.RegularExpressions.RxInterpreter::deep
	RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622 * ___deep_13;

public:
	inline static int32_t get_offset_of_program_1() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___program_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_program_1() const { return ___program_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_program_1() { return &___program_1; }
	inline void set_program_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___program_1 = value;
		Il2CppCodeGenWriteBarrier((&___program_1), value);
	}

	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}

	inline static int32_t get_offset_of_string_start_3() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___string_start_3)); }
	inline int32_t get_string_start_3() const { return ___string_start_3; }
	inline int32_t* get_address_of_string_start_3() { return &___string_start_3; }
	inline void set_string_start_3(int32_t value)
	{
		___string_start_3 = value;
	}

	inline static int32_t get_offset_of_string_end_4() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___string_end_4)); }
	inline int32_t get_string_end_4() const { return ___string_end_4; }
	inline int32_t* get_address_of_string_end_4() { return &___string_end_4; }
	inline void set_string_end_4(int32_t value)
	{
		___string_end_4 = value;
	}

	inline static int32_t get_offset_of_group_count_5() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___group_count_5)); }
	inline int32_t get_group_count_5() const { return ___group_count_5; }
	inline int32_t* get_address_of_group_count_5() { return &___group_count_5; }
	inline void set_group_count_5(int32_t value)
	{
		___group_count_5 = value;
	}

	inline static int32_t get_offset_of_groups_6() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___groups_6)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_groups_6() const { return ___groups_6; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_groups_6() { return &___groups_6; }
	inline void set_groups_6(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___groups_6 = value;
		Il2CppCodeGenWriteBarrier((&___groups_6), value);
	}

	inline static int32_t get_offset_of_eval_del_7() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___eval_del_7)); }
	inline EvalDelegate_tEAE36D514400FE58E4761FEDBB29920820C4B2FB * get_eval_del_7() const { return ___eval_del_7; }
	inline EvalDelegate_tEAE36D514400FE58E4761FEDBB29920820C4B2FB ** get_address_of_eval_del_7() { return &___eval_del_7; }
	inline void set_eval_del_7(EvalDelegate_tEAE36D514400FE58E4761FEDBB29920820C4B2FB * value)
	{
		___eval_del_7 = value;
		Il2CppCodeGenWriteBarrier((&___eval_del_7), value);
	}

	inline static int32_t get_offset_of_marks_8() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___marks_8)); }
	inline MarkU5BU5D_t528ADB54CF1308C521A7235B7686FF22BF928765* get_marks_8() const { return ___marks_8; }
	inline MarkU5BU5D_t528ADB54CF1308C521A7235B7686FF22BF928765** get_address_of_marks_8() { return &___marks_8; }
	inline void set_marks_8(MarkU5BU5D_t528ADB54CF1308C521A7235B7686FF22BF928765* value)
	{
		___marks_8 = value;
		Il2CppCodeGenWriteBarrier((&___marks_8), value);
	}

	inline static int32_t get_offset_of_mark_start_9() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___mark_start_9)); }
	inline int32_t get_mark_start_9() const { return ___mark_start_9; }
	inline int32_t* get_address_of_mark_start_9() { return &___mark_start_9; }
	inline void set_mark_start_9(int32_t value)
	{
		___mark_start_9 = value;
	}

	inline static int32_t get_offset_of_mark_end_10() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___mark_end_10)); }
	inline int32_t get_mark_end_10() const { return ___mark_end_10; }
	inline int32_t* get_address_of_mark_end_10() { return &___mark_end_10; }
	inline void set_mark_end_10(int32_t value)
	{
		___mark_end_10 = value;
	}

	inline static int32_t get_offset_of_stack_11() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___stack_11)); }
	inline IntStack_t70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE  get_stack_11() const { return ___stack_11; }
	inline IntStack_t70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE * get_address_of_stack_11() { return &___stack_11; }
	inline void set_stack_11(IntStack_t70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE  value)
	{
		___stack_11 = value;
	}

	inline static int32_t get_offset_of_repeat_12() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___repeat_12)); }
	inline RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622 * get_repeat_12() const { return ___repeat_12; }
	inline RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622 ** get_address_of_repeat_12() { return &___repeat_12; }
	inline void set_repeat_12(RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622 * value)
	{
		___repeat_12 = value;
		Il2CppCodeGenWriteBarrier((&___repeat_12), value);
	}

	inline static int32_t get_offset_of_deep_13() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740, ___deep_13)); }
	inline RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622 * get_deep_13() const { return ___deep_13; }
	inline RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622 ** get_address_of_deep_13() { return &___deep_13; }
	inline void set_deep_13(RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622 * value)
	{
		___deep_13 = value;
		Il2CppCodeGenWriteBarrier((&___deep_13), value);
	}
};

struct RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740_StaticFields
{
public:
	// System.Boolean System.Text.RegularExpressions.RxInterpreter::trace_rx
	bool ___trace_rx_14;

public:
	inline static int32_t get_offset_of_trace_rx_14() { return static_cast<int32_t>(offsetof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740_StaticFields, ___trace_rx_14)); }
	inline bool get_trace_rx_14() const { return ___trace_rx_14; }
	inline bool* get_address_of_trace_rx_14() { return &___trace_rx_14; }
	inline void set_trace_rx_14(bool value)
	{
		___trace_rx_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RXINTERPRETER_TE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740_H
#ifndef RXOP_TEA232A1F515411AD944C2FCA40CCAD2F4DC34B48_H
#define RXOP_TEA232A1F515411AD944C2FCA40CCAD2F4DC34B48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RxOp
struct  RxOp_tEA232A1F515411AD944C2FCA40CCAD2F4DC34B48 
{
public:
	// System.Byte System.Text.RegularExpressions.RxOp::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RxOp_tEA232A1F515411AD944C2FCA40CCAD2F4DC34B48, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RXOP_TEA232A1F515411AD944C2FCA40CCAD2F4DC34B48_H
#ifndef ALTERNATION_T7CD0712B6ECD03E6F746753266C725B8EADB196F_H
#define ALTERNATION_T7CD0712B6ECD03E6F746753266C725B8EADB196F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.Alternation
struct  Alternation_t7CD0712B6ECD03E6F746753266C725B8EADB196F  : public CompositeExpression_t750AF90954FAB323F201EA628A6123FCAC3A46EC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALTERNATION_T7CD0712B6ECD03E6F746753266C725B8EADB196F_H
#ifndef ASSERTION_TD8EB44CA6D108EF40E4F031DF25C5962F9E69AE1_H
#define ASSERTION_TD8EB44CA6D108EF40E4F031DF25C5962F9E69AE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.Assertion
struct  Assertion_tD8EB44CA6D108EF40E4F031DF25C5962F9E69AE1  : public CompositeExpression_t750AF90954FAB323F201EA628A6123FCAC3A46EC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSERTION_TD8EB44CA6D108EF40E4F031DF25C5962F9E69AE1_H
#ifndef BACKSLASHNUMBER_T9BC00EB1624ACAD40C1D9DD453AD365DE92F9B45_H
#define BACKSLASHNUMBER_T9BC00EB1624ACAD40C1D9DD453AD365DE92F9B45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.BackslashNumber
struct  BackslashNumber_t9BC00EB1624ACAD40C1D9DD453AD365DE92F9B45  : public Reference_t683D20A9EF06251885F0ACC287F31F5B670F47A6
{
public:
	// System.String System.Text.RegularExpressions.Syntax.BackslashNumber::literal
	String_t* ___literal_2;
	// System.Boolean System.Text.RegularExpressions.Syntax.BackslashNumber::ecma
	bool ___ecma_3;

public:
	inline static int32_t get_offset_of_literal_2() { return static_cast<int32_t>(offsetof(BackslashNumber_t9BC00EB1624ACAD40C1D9DD453AD365DE92F9B45, ___literal_2)); }
	inline String_t* get_literal_2() const { return ___literal_2; }
	inline String_t** get_address_of_literal_2() { return &___literal_2; }
	inline void set_literal_2(String_t* value)
	{
		___literal_2 = value;
		Il2CppCodeGenWriteBarrier((&___literal_2), value);
	}

	inline static int32_t get_offset_of_ecma_3() { return static_cast<int32_t>(offsetof(BackslashNumber_t9BC00EB1624ACAD40C1D9DD453AD365DE92F9B45, ___ecma_3)); }
	inline bool get_ecma_3() const { return ___ecma_3; }
	inline bool* get_address_of_ecma_3() { return &___ecma_3; }
	inline void set_ecma_3(bool value)
	{
		___ecma_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKSLASHNUMBER_T9BC00EB1624ACAD40C1D9DD453AD365DE92F9B45_H
#ifndef CHARACTERCLASS_TC781D8EF48C7AF5E49E5F15C7B82712243C97365_H
#define CHARACTERCLASS_TC781D8EF48C7AF5E49E5F15C7B82712243C97365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.CharacterClass
struct  CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365  : public Expression_t6A43E18B89A25A32D44C3FD396F6139A8AA73F94
{
public:
	// System.Boolean System.Text.RegularExpressions.Syntax.CharacterClass::negate
	bool ___negate_1;
	// System.Boolean System.Text.RegularExpressions.Syntax.CharacterClass::ignore
	bool ___ignore_2;
	// System.Collections.BitArray System.Text.RegularExpressions.Syntax.CharacterClass::pos_cats
	BitArray_t3C48B44FE42C242A7A85C6C32E91BFA25463358D * ___pos_cats_3;
	// System.Collections.BitArray System.Text.RegularExpressions.Syntax.CharacterClass::neg_cats
	BitArray_t3C48B44FE42C242A7A85C6C32E91BFA25463358D * ___neg_cats_4;
	// System.Text.RegularExpressions.IntervalCollection System.Text.RegularExpressions.Syntax.CharacterClass::intervals
	IntervalCollection_t33C1E9164A6B24465B908CB2D90E23C9F60F7DD5 * ___intervals_5;

public:
	inline static int32_t get_offset_of_negate_1() { return static_cast<int32_t>(offsetof(CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365, ___negate_1)); }
	inline bool get_negate_1() const { return ___negate_1; }
	inline bool* get_address_of_negate_1() { return &___negate_1; }
	inline void set_negate_1(bool value)
	{
		___negate_1 = value;
	}

	inline static int32_t get_offset_of_ignore_2() { return static_cast<int32_t>(offsetof(CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365, ___ignore_2)); }
	inline bool get_ignore_2() const { return ___ignore_2; }
	inline bool* get_address_of_ignore_2() { return &___ignore_2; }
	inline void set_ignore_2(bool value)
	{
		___ignore_2 = value;
	}

	inline static int32_t get_offset_of_pos_cats_3() { return static_cast<int32_t>(offsetof(CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365, ___pos_cats_3)); }
	inline BitArray_t3C48B44FE42C242A7A85C6C32E91BFA25463358D * get_pos_cats_3() const { return ___pos_cats_3; }
	inline BitArray_t3C48B44FE42C242A7A85C6C32E91BFA25463358D ** get_address_of_pos_cats_3() { return &___pos_cats_3; }
	inline void set_pos_cats_3(BitArray_t3C48B44FE42C242A7A85C6C32E91BFA25463358D * value)
	{
		___pos_cats_3 = value;
		Il2CppCodeGenWriteBarrier((&___pos_cats_3), value);
	}

	inline static int32_t get_offset_of_neg_cats_4() { return static_cast<int32_t>(offsetof(CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365, ___neg_cats_4)); }
	inline BitArray_t3C48B44FE42C242A7A85C6C32E91BFA25463358D * get_neg_cats_4() const { return ___neg_cats_4; }
	inline BitArray_t3C48B44FE42C242A7A85C6C32E91BFA25463358D ** get_address_of_neg_cats_4() { return &___neg_cats_4; }
	inline void set_neg_cats_4(BitArray_t3C48B44FE42C242A7A85C6C32E91BFA25463358D * value)
	{
		___neg_cats_4 = value;
		Il2CppCodeGenWriteBarrier((&___neg_cats_4), value);
	}

	inline static int32_t get_offset_of_intervals_5() { return static_cast<int32_t>(offsetof(CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365, ___intervals_5)); }
	inline IntervalCollection_t33C1E9164A6B24465B908CB2D90E23C9F60F7DD5 * get_intervals_5() const { return ___intervals_5; }
	inline IntervalCollection_t33C1E9164A6B24465B908CB2D90E23C9F60F7DD5 ** get_address_of_intervals_5() { return &___intervals_5; }
	inline void set_intervals_5(IntervalCollection_t33C1E9164A6B24465B908CB2D90E23C9F60F7DD5 * value)
	{
		___intervals_5 = value;
		Il2CppCodeGenWriteBarrier((&___intervals_5), value);
	}
};

struct CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365_StaticFields
{
public:
	// System.Text.RegularExpressions.Interval System.Text.RegularExpressions.Syntax.CharacterClass::upper_case_characters
	Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F  ___upper_case_characters_0;

public:
	inline static int32_t get_offset_of_upper_case_characters_0() { return static_cast<int32_t>(offsetof(CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365_StaticFields, ___upper_case_characters_0)); }
	inline Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F  get_upper_case_characters_0() const { return ___upper_case_characters_0; }
	inline Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F * get_address_of_upper_case_characters_0() { return &___upper_case_characters_0; }
	inline void set_upper_case_characters_0(Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F  value)
	{
		___upper_case_characters_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARACTERCLASS_TC781D8EF48C7AF5E49E5F15C7B82712243C97365_H
#ifndef GROUP_T9EAAD4F220F411990E6DA025C34483FDC8CF905D_H
#define GROUP_T9EAAD4F220F411990E6DA025C34483FDC8CF905D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.Group
struct  Group_t9EAAD4F220F411990E6DA025C34483FDC8CF905D  : public CompositeExpression_t750AF90954FAB323F201EA628A6123FCAC3A46EC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUP_T9EAAD4F220F411990E6DA025C34483FDC8CF905D_H
#ifndef REPETITION_T60545A1C9349248736914B4A126F1919F346D4B7_H
#define REPETITION_T60545A1C9349248736914B4A126F1919F346D4B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.Repetition
struct  Repetition_t60545A1C9349248736914B4A126F1919F346D4B7  : public CompositeExpression_t750AF90954FAB323F201EA628A6123FCAC3A46EC
{
public:
	// System.Int32 System.Text.RegularExpressions.Syntax.Repetition::min
	int32_t ___min_1;
	// System.Int32 System.Text.RegularExpressions.Syntax.Repetition::max
	int32_t ___max_2;
	// System.Boolean System.Text.RegularExpressions.Syntax.Repetition::lazy
	bool ___lazy_3;

public:
	inline static int32_t get_offset_of_min_1() { return static_cast<int32_t>(offsetof(Repetition_t60545A1C9349248736914B4A126F1919F346D4B7, ___min_1)); }
	inline int32_t get_min_1() const { return ___min_1; }
	inline int32_t* get_address_of_min_1() { return &___min_1; }
	inline void set_min_1(int32_t value)
	{
		___min_1 = value;
	}

	inline static int32_t get_offset_of_max_2() { return static_cast<int32_t>(offsetof(Repetition_t60545A1C9349248736914B4A126F1919F346D4B7, ___max_2)); }
	inline int32_t get_max_2() const { return ___max_2; }
	inline int32_t* get_address_of_max_2() { return &___max_2; }
	inline void set_max_2(int32_t value)
	{
		___max_2 = value;
	}

	inline static int32_t get_offset_of_lazy_3() { return static_cast<int32_t>(offsetof(Repetition_t60545A1C9349248736914B4A126F1919F346D4B7, ___lazy_3)); }
	inline bool get_lazy_3() const { return ___lazy_3; }
	inline bool* get_address_of_lazy_3() { return &___lazy_3; }
	inline void set_lazy_3(bool value)
	{
		___lazy_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPETITION_T60545A1C9349248736914B4A126F1919F346D4B7_H
#ifndef URIHOSTNAMETYPE_TD5C10F880C524C8028C474B23B619228AE144479_H
#define URIHOSTNAMETYPE_TD5C10F880C524C8028C474B23B619228AE144479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriHostNameType
struct  UriHostNameType_tD5C10F880C524C8028C474B23B619228AE144479 
{
public:
	// System.Int32 System.UriHostNameType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UriHostNameType_tD5C10F880C524C8028C474B23B619228AE144479, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIHOSTNAMETYPE_TD5C10F880C524C8028C474B23B619228AE144479_H
#ifndef URIKIND_T602575BB5018420CCE336A35E0D95B1E18F13069_H
#define URIKIND_T602575BB5018420CCE336A35E0D95B1E18F13069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriKind
struct  UriKind_t602575BB5018420CCE336A35E0D95B1E18F13069 
{
public:
	// System.Int32 System.UriKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UriKind_t602575BB5018420CCE336A35E0D95B1E18F13069, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIKIND_T602575BB5018420CCE336A35E0D95B1E18F13069_H
#ifndef URIPARTIAL_TDD0004CFD64BC37CB615A5A3428BB2CC05CB5912_H
#define URIPARTIAL_TDD0004CFD64BC37CB615A5A3428BB2CC05CB5912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriPartial
struct  UriPartial_tDD0004CFD64BC37CB615A5A3428BB2CC05CB5912 
{
public:
	// System.Int32 System.UriPartial::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UriPartial_tDD0004CFD64BC37CB615A5A3428BB2CC05CB5912, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIPARTIAL_TDD0004CFD64BC37CB615A5A3428BB2CC05CB5912_H
#ifndef CONFORMANCELEVEL_TBA47CA81D3009DF2AD8DC7CE769E6982D1EA96CB_H
#define CONFORMANCELEVEL_TBA47CA81D3009DF2AD8DC7CE769E6982D1EA96CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_tBA47CA81D3009DF2AD8DC7CE769E6982D1EA96CB 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConformanceLevel_tBA47CA81D3009DF2AD8DC7CE769E6982D1EA96CB, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_TBA47CA81D3009DF2AD8DC7CE769E6982D1EA96CB_H
#ifndef XMLSCHEMACONTENTPROCESSING_T392DB8F98BDBDD400CD2370DE517690F99622330_H
#define XMLSCHEMACONTENTPROCESSING_T392DB8F98BDBDD400CD2370DE517690F99622330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentProcessing
struct  XmlSchemaContentProcessing_t392DB8F98BDBDD400CD2370DE517690F99622330 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentProcessing::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaContentProcessing_t392DB8F98BDBDD400CD2370DE517690F99622330, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTPROCESSING_T392DB8F98BDBDD400CD2370DE517690F99622330_H
#ifndef VALIDATIONTYPE_T31525A080D050BCFADE54364812432A4C5BDBE63_H
#define VALIDATIONTYPE_T31525A080D050BCFADE54364812432A4C5BDBE63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidationType
struct  ValidationType_t31525A080D050BCFADE54364812432A4C5BDBE63 
{
public:
	// System.Int32 System.Xml.ValidationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ValidationType_t31525A080D050BCFADE54364812432A4C5BDBE63, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONTYPE_T31525A080D050BCFADE54364812432A4C5BDBE63_H
#ifndef XSDPARTICLESTATEMANAGER_T9C730C7B40880A4D7409B129A8D041BD47438E68_H
#define XSDPARTICLESTATEMANAGER_T9C730C7B40880A4D7409B129A8D041BD47438E68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdParticleStateManager
struct  XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.Schema.XsdParticleStateManager::table
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___table_0;
	// System.Xml.Schema.XmlSchemaContentProcessing Mono.Xml.Schema.XsdParticleStateManager::processContents
	int32_t ___processContents_1;
	// System.Xml.Schema.XmlSchemaElement Mono.Xml.Schema.XsdParticleStateManager::CurrentElement
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * ___CurrentElement_2;
	// System.Collections.Stack Mono.Xml.Schema.XsdParticleStateManager::ContextStack
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___ContextStack_3;
	// Mono.Xml.Schema.XsdValidationContext Mono.Xml.Schema.XsdParticleStateManager::Context
	XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD * ___Context_4;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68, ___table_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_table_0() const { return ___table_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}

	inline static int32_t get_offset_of_processContents_1() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68, ___processContents_1)); }
	inline int32_t get_processContents_1() const { return ___processContents_1; }
	inline int32_t* get_address_of_processContents_1() { return &___processContents_1; }
	inline void set_processContents_1(int32_t value)
	{
		___processContents_1 = value;
	}

	inline static int32_t get_offset_of_CurrentElement_2() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68, ___CurrentElement_2)); }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * get_CurrentElement_2() const { return ___CurrentElement_2; }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 ** get_address_of_CurrentElement_2() { return &___CurrentElement_2; }
	inline void set_CurrentElement_2(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * value)
	{
		___CurrentElement_2 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentElement_2), value);
	}

	inline static int32_t get_offset_of_ContextStack_3() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68, ___ContextStack_3)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_ContextStack_3() const { return ___ContextStack_3; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_ContextStack_3() { return &___ContextStack_3; }
	inline void set_ContextStack_3(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___ContextStack_3 = value;
		Il2CppCodeGenWriteBarrier((&___ContextStack_3), value);
	}

	inline static int32_t get_offset_of_Context_4() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68, ___Context_4)); }
	inline XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD * get_Context_4() const { return ___Context_4; }
	inline XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD ** get_address_of_Context_4() { return &___Context_4; }
	inline void set_Context_4(XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD * value)
	{
		___Context_4 = value;
		Il2CppCodeGenWriteBarrier((&___Context_4), value);
	}
};

struct XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdParticleStateManager::<>f__switchU24map2
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map2_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2_5() { return static_cast<int32_t>(offsetof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68_StaticFields, ___U3CU3Ef__switchU24map2_5)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map2_5() const { return ___U3CU3Ef__switchU24map2_5; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map2_5() { return &___U3CU3Ef__switchU24map2_5; }
	inline void set_U3CU3Ef__switchU24map2_5(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDPARTICLESTATEMANAGER_T9C730C7B40880A4D7409B129A8D041BD47438E68_H
#ifndef XSDVALIDATINGREADER_T2C211F16BDE384F56FB73B293505DEADE5AFCEB3_H
#define XSDVALIDATINGREADER_T2C211F16BDE384F56FB73B293505DEADE5AFCEB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdValidatingReader
struct  XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// System.Xml.XmlReader Mono.Xml.Schema.XsdValidatingReader::reader
	XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * ___reader_3;
	// System.Xml.XmlResolver Mono.Xml.Schema.XsdValidatingReader::resolver
	XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * ___resolver_4;
	// Mono.Xml.IHasXmlSchemaInfo Mono.Xml.Schema.XsdValidatingReader::sourceReaderSchemaInfo
	RuntimeObject* ___sourceReaderSchemaInfo_5;
	// System.Xml.IXmlLineInfo Mono.Xml.Schema.XsdValidatingReader::readerLineInfo
	RuntimeObject* ___readerLineInfo_6;
	// System.Xml.ValidationType Mono.Xml.Schema.XsdValidatingReader::validationType
	int32_t ___validationType_7;
	// System.Xml.Schema.XmlSchemaSet Mono.Xml.Schema.XsdValidatingReader::schemas
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * ___schemas_8;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::namespaces
	bool ___namespaces_9;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::validationStarted
	bool ___validationStarted_10;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::checkIdentity
	bool ___checkIdentity_11;
	// Mono.Xml.Schema.XsdIDManager Mono.Xml.Schema.XsdValidatingReader::idManager
	XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E * ___idManager_12;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::checkKeyConstraints
	bool ___checkKeyConstraints_13;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::keyTables
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___keyTables_14;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::currentKeyFieldConsumers
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___currentKeyFieldConsumers_15;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::tmpKeyrefPool
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___tmpKeyrefPool_16;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::elementQNameStack
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___elementQNameStack_17;
	// Mono.Xml.Schema.XsdParticleStateManager Mono.Xml.Schema.XsdValidatingReader::state
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 * ___state_18;
	// System.Int32 Mono.Xml.Schema.XsdValidatingReader::skipValidationDepth
	int32_t ___skipValidationDepth_19;
	// System.Int32 Mono.Xml.Schema.XsdValidatingReader::xsiNilDepth
	int32_t ___xsiNilDepth_20;
	// System.Text.StringBuilder Mono.Xml.Schema.XsdValidatingReader::storedCharacters
	StringBuilder_t * ___storedCharacters_21;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::shouldValidateCharacters
	bool ___shouldValidateCharacters_22;
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XsdValidatingReader::defaultAttributes
	XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* ___defaultAttributes_23;
	// System.Int32 Mono.Xml.Schema.XsdValidatingReader::currentDefaultAttribute
	int32_t ___currentDefaultAttribute_24;
	// System.Collections.ArrayList Mono.Xml.Schema.XsdValidatingReader::defaultAttributesCache
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___defaultAttributesCache_25;
	// System.Boolean Mono.Xml.Schema.XsdValidatingReader::defaultAttributeConsumed
	bool ___defaultAttributeConsumed_26;
	// System.Object Mono.Xml.Schema.XsdValidatingReader::currentAttrType
	RuntimeObject * ___currentAttrType_27;
	// System.Xml.Schema.ValidationEventHandler Mono.Xml.Schema.XsdValidatingReader::ValidationEventHandler
	ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * ___ValidationEventHandler_28;

public:
	inline static int32_t get_offset_of_reader_3() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___reader_3)); }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * get_reader_3() const { return ___reader_3; }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 ** get_address_of_reader_3() { return &___reader_3; }
	inline void set_reader_3(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * value)
	{
		___reader_3 = value;
		Il2CppCodeGenWriteBarrier((&___reader_3), value);
	}

	inline static int32_t get_offset_of_resolver_4() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___resolver_4)); }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * get_resolver_4() const { return ___resolver_4; }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C ** get_address_of_resolver_4() { return &___resolver_4; }
	inline void set_resolver_4(XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * value)
	{
		___resolver_4 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_4), value);
	}

	inline static int32_t get_offset_of_sourceReaderSchemaInfo_5() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___sourceReaderSchemaInfo_5)); }
	inline RuntimeObject* get_sourceReaderSchemaInfo_5() const { return ___sourceReaderSchemaInfo_5; }
	inline RuntimeObject** get_address_of_sourceReaderSchemaInfo_5() { return &___sourceReaderSchemaInfo_5; }
	inline void set_sourceReaderSchemaInfo_5(RuntimeObject* value)
	{
		___sourceReaderSchemaInfo_5 = value;
		Il2CppCodeGenWriteBarrier((&___sourceReaderSchemaInfo_5), value);
	}

	inline static int32_t get_offset_of_readerLineInfo_6() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___readerLineInfo_6)); }
	inline RuntimeObject* get_readerLineInfo_6() const { return ___readerLineInfo_6; }
	inline RuntimeObject** get_address_of_readerLineInfo_6() { return &___readerLineInfo_6; }
	inline void set_readerLineInfo_6(RuntimeObject* value)
	{
		___readerLineInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___readerLineInfo_6), value);
	}

	inline static int32_t get_offset_of_validationType_7() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___validationType_7)); }
	inline int32_t get_validationType_7() const { return ___validationType_7; }
	inline int32_t* get_address_of_validationType_7() { return &___validationType_7; }
	inline void set_validationType_7(int32_t value)
	{
		___validationType_7 = value;
	}

	inline static int32_t get_offset_of_schemas_8() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___schemas_8)); }
	inline XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * get_schemas_8() const { return ___schemas_8; }
	inline XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D ** get_address_of_schemas_8() { return &___schemas_8; }
	inline void set_schemas_8(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * value)
	{
		___schemas_8 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_8), value);
	}

	inline static int32_t get_offset_of_namespaces_9() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___namespaces_9)); }
	inline bool get_namespaces_9() const { return ___namespaces_9; }
	inline bool* get_address_of_namespaces_9() { return &___namespaces_9; }
	inline void set_namespaces_9(bool value)
	{
		___namespaces_9 = value;
	}

	inline static int32_t get_offset_of_validationStarted_10() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___validationStarted_10)); }
	inline bool get_validationStarted_10() const { return ___validationStarted_10; }
	inline bool* get_address_of_validationStarted_10() { return &___validationStarted_10; }
	inline void set_validationStarted_10(bool value)
	{
		___validationStarted_10 = value;
	}

	inline static int32_t get_offset_of_checkIdentity_11() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___checkIdentity_11)); }
	inline bool get_checkIdentity_11() const { return ___checkIdentity_11; }
	inline bool* get_address_of_checkIdentity_11() { return &___checkIdentity_11; }
	inline void set_checkIdentity_11(bool value)
	{
		___checkIdentity_11 = value;
	}

	inline static int32_t get_offset_of_idManager_12() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___idManager_12)); }
	inline XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E * get_idManager_12() const { return ___idManager_12; }
	inline XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E ** get_address_of_idManager_12() { return &___idManager_12; }
	inline void set_idManager_12(XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E * value)
	{
		___idManager_12 = value;
		Il2CppCodeGenWriteBarrier((&___idManager_12), value);
	}

	inline static int32_t get_offset_of_checkKeyConstraints_13() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___checkKeyConstraints_13)); }
	inline bool get_checkKeyConstraints_13() const { return ___checkKeyConstraints_13; }
	inline bool* get_address_of_checkKeyConstraints_13() { return &___checkKeyConstraints_13; }
	inline void set_checkKeyConstraints_13(bool value)
	{
		___checkKeyConstraints_13 = value;
	}

	inline static int32_t get_offset_of_keyTables_14() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___keyTables_14)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_keyTables_14() const { return ___keyTables_14; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_keyTables_14() { return &___keyTables_14; }
	inline void set_keyTables_14(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___keyTables_14 = value;
		Il2CppCodeGenWriteBarrier((&___keyTables_14), value);
	}

	inline static int32_t get_offset_of_currentKeyFieldConsumers_15() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___currentKeyFieldConsumers_15)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_currentKeyFieldConsumers_15() const { return ___currentKeyFieldConsumers_15; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_currentKeyFieldConsumers_15() { return &___currentKeyFieldConsumers_15; }
	inline void set_currentKeyFieldConsumers_15(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___currentKeyFieldConsumers_15 = value;
		Il2CppCodeGenWriteBarrier((&___currentKeyFieldConsumers_15), value);
	}

	inline static int32_t get_offset_of_tmpKeyrefPool_16() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___tmpKeyrefPool_16)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_tmpKeyrefPool_16() const { return ___tmpKeyrefPool_16; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_tmpKeyrefPool_16() { return &___tmpKeyrefPool_16; }
	inline void set_tmpKeyrefPool_16(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___tmpKeyrefPool_16 = value;
		Il2CppCodeGenWriteBarrier((&___tmpKeyrefPool_16), value);
	}

	inline static int32_t get_offset_of_elementQNameStack_17() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___elementQNameStack_17)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_elementQNameStack_17() const { return ___elementQNameStack_17; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_elementQNameStack_17() { return &___elementQNameStack_17; }
	inline void set_elementQNameStack_17(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___elementQNameStack_17 = value;
		Il2CppCodeGenWriteBarrier((&___elementQNameStack_17), value);
	}

	inline static int32_t get_offset_of_state_18() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___state_18)); }
	inline XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 * get_state_18() const { return ___state_18; }
	inline XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 ** get_address_of_state_18() { return &___state_18; }
	inline void set_state_18(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68 * value)
	{
		___state_18 = value;
		Il2CppCodeGenWriteBarrier((&___state_18), value);
	}

	inline static int32_t get_offset_of_skipValidationDepth_19() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___skipValidationDepth_19)); }
	inline int32_t get_skipValidationDepth_19() const { return ___skipValidationDepth_19; }
	inline int32_t* get_address_of_skipValidationDepth_19() { return &___skipValidationDepth_19; }
	inline void set_skipValidationDepth_19(int32_t value)
	{
		___skipValidationDepth_19 = value;
	}

	inline static int32_t get_offset_of_xsiNilDepth_20() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___xsiNilDepth_20)); }
	inline int32_t get_xsiNilDepth_20() const { return ___xsiNilDepth_20; }
	inline int32_t* get_address_of_xsiNilDepth_20() { return &___xsiNilDepth_20; }
	inline void set_xsiNilDepth_20(int32_t value)
	{
		___xsiNilDepth_20 = value;
	}

	inline static int32_t get_offset_of_storedCharacters_21() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___storedCharacters_21)); }
	inline StringBuilder_t * get_storedCharacters_21() const { return ___storedCharacters_21; }
	inline StringBuilder_t ** get_address_of_storedCharacters_21() { return &___storedCharacters_21; }
	inline void set_storedCharacters_21(StringBuilder_t * value)
	{
		___storedCharacters_21 = value;
		Il2CppCodeGenWriteBarrier((&___storedCharacters_21), value);
	}

	inline static int32_t get_offset_of_shouldValidateCharacters_22() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___shouldValidateCharacters_22)); }
	inline bool get_shouldValidateCharacters_22() const { return ___shouldValidateCharacters_22; }
	inline bool* get_address_of_shouldValidateCharacters_22() { return &___shouldValidateCharacters_22; }
	inline void set_shouldValidateCharacters_22(bool value)
	{
		___shouldValidateCharacters_22 = value;
	}

	inline static int32_t get_offset_of_defaultAttributes_23() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___defaultAttributes_23)); }
	inline XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* get_defaultAttributes_23() const { return ___defaultAttributes_23; }
	inline XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95** get_address_of_defaultAttributes_23() { return &___defaultAttributes_23; }
	inline void set_defaultAttributes_23(XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* value)
	{
		___defaultAttributes_23 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributes_23), value);
	}

	inline static int32_t get_offset_of_currentDefaultAttribute_24() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___currentDefaultAttribute_24)); }
	inline int32_t get_currentDefaultAttribute_24() const { return ___currentDefaultAttribute_24; }
	inline int32_t* get_address_of_currentDefaultAttribute_24() { return &___currentDefaultAttribute_24; }
	inline void set_currentDefaultAttribute_24(int32_t value)
	{
		___currentDefaultAttribute_24 = value;
	}

	inline static int32_t get_offset_of_defaultAttributesCache_25() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___defaultAttributesCache_25)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_defaultAttributesCache_25() const { return ___defaultAttributesCache_25; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_defaultAttributesCache_25() { return &___defaultAttributesCache_25; }
	inline void set_defaultAttributesCache_25(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___defaultAttributesCache_25 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributesCache_25), value);
	}

	inline static int32_t get_offset_of_defaultAttributeConsumed_26() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___defaultAttributeConsumed_26)); }
	inline bool get_defaultAttributeConsumed_26() const { return ___defaultAttributeConsumed_26; }
	inline bool* get_address_of_defaultAttributeConsumed_26() { return &___defaultAttributeConsumed_26; }
	inline void set_defaultAttributeConsumed_26(bool value)
	{
		___defaultAttributeConsumed_26 = value;
	}

	inline static int32_t get_offset_of_currentAttrType_27() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___currentAttrType_27)); }
	inline RuntimeObject * get_currentAttrType_27() const { return ___currentAttrType_27; }
	inline RuntimeObject ** get_address_of_currentAttrType_27() { return &___currentAttrType_27; }
	inline void set_currentAttrType_27(RuntimeObject * value)
	{
		___currentAttrType_27 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttrType_27), value);
	}

	inline static int32_t get_offset_of_ValidationEventHandler_28() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3, ___ValidationEventHandler_28)); }
	inline ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * get_ValidationEventHandler_28() const { return ___ValidationEventHandler_28; }
	inline ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B ** get_address_of_ValidationEventHandler_28() { return &___ValidationEventHandler_28; }
	inline void set_ValidationEventHandler_28(ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * value)
	{
		___ValidationEventHandler_28 = value;
		Il2CppCodeGenWriteBarrier((&___ValidationEventHandler_28), value);
	}
};

struct XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaAttribute[] Mono.Xml.Schema.XsdValidatingReader::emptyAttributeArray
	XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* ___emptyAttributeArray_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdValidatingReader::<>f__switchU24map3
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map3_29;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdValidatingReader::<>f__switchU24map4
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map4_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdValidatingReader::<>f__switchU24map5
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map5_31;

public:
	inline static int32_t get_offset_of_emptyAttributeArray_2() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields, ___emptyAttributeArray_2)); }
	inline XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* get_emptyAttributeArray_2() const { return ___emptyAttributeArray_2; }
	inline XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95** get_address_of_emptyAttributeArray_2() { return &___emptyAttributeArray_2; }
	inline void set_emptyAttributeArray_2(XmlSchemaAttributeU5BU5D_tC4DF4D638E4C3887DC92B7A37937F47F6B99AF95* value)
	{
		___emptyAttributeArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptyAttributeArray_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3_29() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields, ___U3CU3Ef__switchU24map3_29)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map3_29() const { return ___U3CU3Ef__switchU24map3_29; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map3_29() { return &___U3CU3Ef__switchU24map3_29; }
	inline void set_U3CU3Ef__switchU24map3_29(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map3_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_30() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields, ___U3CU3Ef__switchU24map4_30)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map4_30() const { return ___U3CU3Ef__switchU24map4_30; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map4_30() { return &___U3CU3Ef__switchU24map4_30; }
	inline void set_U3CU3Ef__switchU24map4_30(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map4_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map4_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_31() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields, ___U3CU3Ef__switchU24map5_31)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map5_31() const { return ___U3CU3Ef__switchU24map5_31; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map5_31() { return &___U3CU3Ef__switchU24map5_31; }
	inline void set_U3CU3Ef__switchU24map5_31(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map5_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map5_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATINGREADER_T2C211F16BDE384F56FB73B293505DEADE5AFCEB3_H
#ifndef XSDWILDCARD_T320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_H
#define XSDWILDCARD_T320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdWildcard
struct  XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaObject Mono.Xml.Schema.XsdWildcard::xsobj
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * ___xsobj_0;
	// System.Xml.Schema.XmlSchemaContentProcessing Mono.Xml.Schema.XsdWildcard::ResolvedProcessing
	int32_t ___ResolvedProcessing_1;
	// System.String Mono.Xml.Schema.XsdWildcard::TargetNamespace
	String_t* ___TargetNamespace_2;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::SkipCompile
	bool ___SkipCompile_3;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueAny
	bool ___HasValueAny_4;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueLocal
	bool ___HasValueLocal_5;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueOther
	bool ___HasValueOther_6;
	// System.Boolean Mono.Xml.Schema.XsdWildcard::HasValueTargetNamespace
	bool ___HasValueTargetNamespace_7;
	// System.Collections.Specialized.StringCollection Mono.Xml.Schema.XsdWildcard::ResolvedNamespaces
	StringCollection_t1921D1DDEF33942CB6DF73338C52ECD8C6ABC5E9 * ___ResolvedNamespaces_8;

public:
	inline static int32_t get_offset_of_xsobj_0() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___xsobj_0)); }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * get_xsobj_0() const { return ___xsobj_0; }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 ** get_address_of_xsobj_0() { return &___xsobj_0; }
	inline void set_xsobj_0(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * value)
	{
		___xsobj_0 = value;
		Il2CppCodeGenWriteBarrier((&___xsobj_0), value);
	}

	inline static int32_t get_offset_of_ResolvedProcessing_1() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___ResolvedProcessing_1)); }
	inline int32_t get_ResolvedProcessing_1() const { return ___ResolvedProcessing_1; }
	inline int32_t* get_address_of_ResolvedProcessing_1() { return &___ResolvedProcessing_1; }
	inline void set_ResolvedProcessing_1(int32_t value)
	{
		___ResolvedProcessing_1 = value;
	}

	inline static int32_t get_offset_of_TargetNamespace_2() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___TargetNamespace_2)); }
	inline String_t* get_TargetNamespace_2() const { return ___TargetNamespace_2; }
	inline String_t** get_address_of_TargetNamespace_2() { return &___TargetNamespace_2; }
	inline void set_TargetNamespace_2(String_t* value)
	{
		___TargetNamespace_2 = value;
		Il2CppCodeGenWriteBarrier((&___TargetNamespace_2), value);
	}

	inline static int32_t get_offset_of_SkipCompile_3() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___SkipCompile_3)); }
	inline bool get_SkipCompile_3() const { return ___SkipCompile_3; }
	inline bool* get_address_of_SkipCompile_3() { return &___SkipCompile_3; }
	inline void set_SkipCompile_3(bool value)
	{
		___SkipCompile_3 = value;
	}

	inline static int32_t get_offset_of_HasValueAny_4() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___HasValueAny_4)); }
	inline bool get_HasValueAny_4() const { return ___HasValueAny_4; }
	inline bool* get_address_of_HasValueAny_4() { return &___HasValueAny_4; }
	inline void set_HasValueAny_4(bool value)
	{
		___HasValueAny_4 = value;
	}

	inline static int32_t get_offset_of_HasValueLocal_5() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___HasValueLocal_5)); }
	inline bool get_HasValueLocal_5() const { return ___HasValueLocal_5; }
	inline bool* get_address_of_HasValueLocal_5() { return &___HasValueLocal_5; }
	inline void set_HasValueLocal_5(bool value)
	{
		___HasValueLocal_5 = value;
	}

	inline static int32_t get_offset_of_HasValueOther_6() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___HasValueOther_6)); }
	inline bool get_HasValueOther_6() const { return ___HasValueOther_6; }
	inline bool* get_address_of_HasValueOther_6() { return &___HasValueOther_6; }
	inline void set_HasValueOther_6(bool value)
	{
		___HasValueOther_6 = value;
	}

	inline static int32_t get_offset_of_HasValueTargetNamespace_7() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___HasValueTargetNamespace_7)); }
	inline bool get_HasValueTargetNamespace_7() const { return ___HasValueTargetNamespace_7; }
	inline bool* get_address_of_HasValueTargetNamespace_7() { return &___HasValueTargetNamespace_7; }
	inline void set_HasValueTargetNamespace_7(bool value)
	{
		___HasValueTargetNamespace_7 = value;
	}

	inline static int32_t get_offset_of_ResolvedNamespaces_8() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7, ___ResolvedNamespaces_8)); }
	inline StringCollection_t1921D1DDEF33942CB6DF73338C52ECD8C6ABC5E9 * get_ResolvedNamespaces_8() const { return ___ResolvedNamespaces_8; }
	inline StringCollection_t1921D1DDEF33942CB6DF73338C52ECD8C6ABC5E9 ** get_address_of_ResolvedNamespaces_8() { return &___ResolvedNamespaces_8; }
	inline void set_ResolvedNamespaces_8(StringCollection_t1921D1DDEF33942CB6DF73338C52ECD8C6ABC5E9 * value)
	{
		___ResolvedNamespaces_8 = value;
		Il2CppCodeGenWriteBarrier((&___ResolvedNamespaces_8), value);
	}
};

struct XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.Schema.XsdWildcard::<>f__switchU24map6
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map6_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_9() { return static_cast<int32_t>(offsetof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_StaticFields, ___U3CU3Ef__switchU24map6_9)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map6_9() const { return ___U3CU3Ef__switchU24map6_9; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map6_9() { return &___U3CU3Ef__switchU24map6_9; }
	inline void set_U3CU3Ef__switchU24map6_9(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map6_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map6_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDWILDCARD_T320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef REGEX_T829CB33C547AB824BFEAFF320BB8F59B3A76620B_H
#define REGEX_T829CB33C547AB824BFEAFF320BB8F59B3A76620B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Regex
struct  Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.Regex::machineFactory
	RuntimeObject* ___machineFactory_2;
	// System.Collections.IDictionary System.Text.RegularExpressions.Regex::mapping
	RuntimeObject* ___mapping_3;
	// System.Int32 System.Text.RegularExpressions.Regex::group_count
	int32_t ___group_count_4;
	// System.Int32 System.Text.RegularExpressions.Regex::gap
	int32_t ___gap_5;
	// System.Boolean System.Text.RegularExpressions.Regex::refsInitialized
	bool ___refsInitialized_6;
	// System.String[] System.Text.RegularExpressions.Regex::group_names
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___group_names_7;
	// System.Int32[] System.Text.RegularExpressions.Regex::group_numbers
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___group_numbers_8;
	// System.String System.Text.RegularExpressions.Regex::pattern
	String_t* ___pattern_9;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.Regex::roptions
	int32_t ___roptions_10;
	// System.Collections.Hashtable System.Text.RegularExpressions.Regex::capnames
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___capnames_11;
	// System.Collections.Hashtable System.Text.RegularExpressions.Regex::caps
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___caps_12;
	// System.Text.RegularExpressions.RegexRunnerFactory System.Text.RegularExpressions.Regex::factory
	RegexRunnerFactory_tF345EBFE2597908D47A0A7C0F4D7194718DACF61 * ___factory_13;
	// System.Int32 System.Text.RegularExpressions.Regex::capsize
	int32_t ___capsize_14;
	// System.String[] System.Text.RegularExpressions.Regex::capslist
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___capslist_15;

public:
	inline static int32_t get_offset_of_machineFactory_2() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___machineFactory_2)); }
	inline RuntimeObject* get_machineFactory_2() const { return ___machineFactory_2; }
	inline RuntimeObject** get_address_of_machineFactory_2() { return &___machineFactory_2; }
	inline void set_machineFactory_2(RuntimeObject* value)
	{
		___machineFactory_2 = value;
		Il2CppCodeGenWriteBarrier((&___machineFactory_2), value);
	}

	inline static int32_t get_offset_of_mapping_3() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___mapping_3)); }
	inline RuntimeObject* get_mapping_3() const { return ___mapping_3; }
	inline RuntimeObject** get_address_of_mapping_3() { return &___mapping_3; }
	inline void set_mapping_3(RuntimeObject* value)
	{
		___mapping_3 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_3), value);
	}

	inline static int32_t get_offset_of_group_count_4() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___group_count_4)); }
	inline int32_t get_group_count_4() const { return ___group_count_4; }
	inline int32_t* get_address_of_group_count_4() { return &___group_count_4; }
	inline void set_group_count_4(int32_t value)
	{
		___group_count_4 = value;
	}

	inline static int32_t get_offset_of_gap_5() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___gap_5)); }
	inline int32_t get_gap_5() const { return ___gap_5; }
	inline int32_t* get_address_of_gap_5() { return &___gap_5; }
	inline void set_gap_5(int32_t value)
	{
		___gap_5 = value;
	}

	inline static int32_t get_offset_of_refsInitialized_6() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___refsInitialized_6)); }
	inline bool get_refsInitialized_6() const { return ___refsInitialized_6; }
	inline bool* get_address_of_refsInitialized_6() { return &___refsInitialized_6; }
	inline void set_refsInitialized_6(bool value)
	{
		___refsInitialized_6 = value;
	}

	inline static int32_t get_offset_of_group_names_7() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___group_names_7)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_group_names_7() const { return ___group_names_7; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_group_names_7() { return &___group_names_7; }
	inline void set_group_names_7(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___group_names_7 = value;
		Il2CppCodeGenWriteBarrier((&___group_names_7), value);
	}

	inline static int32_t get_offset_of_group_numbers_8() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___group_numbers_8)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_group_numbers_8() const { return ___group_numbers_8; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_group_numbers_8() { return &___group_numbers_8; }
	inline void set_group_numbers_8(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___group_numbers_8 = value;
		Il2CppCodeGenWriteBarrier((&___group_numbers_8), value);
	}

	inline static int32_t get_offset_of_pattern_9() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___pattern_9)); }
	inline String_t* get_pattern_9() const { return ___pattern_9; }
	inline String_t** get_address_of_pattern_9() { return &___pattern_9; }
	inline void set_pattern_9(String_t* value)
	{
		___pattern_9 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_9), value);
	}

	inline static int32_t get_offset_of_roptions_10() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___roptions_10)); }
	inline int32_t get_roptions_10() const { return ___roptions_10; }
	inline int32_t* get_address_of_roptions_10() { return &___roptions_10; }
	inline void set_roptions_10(int32_t value)
	{
		___roptions_10 = value;
	}

	inline static int32_t get_offset_of_capnames_11() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___capnames_11)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_capnames_11() const { return ___capnames_11; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_capnames_11() { return &___capnames_11; }
	inline void set_capnames_11(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___capnames_11 = value;
		Il2CppCodeGenWriteBarrier((&___capnames_11), value);
	}

	inline static int32_t get_offset_of_caps_12() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___caps_12)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_caps_12() const { return ___caps_12; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_caps_12() { return &___caps_12; }
	inline void set_caps_12(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___caps_12 = value;
		Il2CppCodeGenWriteBarrier((&___caps_12), value);
	}

	inline static int32_t get_offset_of_factory_13() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___factory_13)); }
	inline RegexRunnerFactory_tF345EBFE2597908D47A0A7C0F4D7194718DACF61 * get_factory_13() const { return ___factory_13; }
	inline RegexRunnerFactory_tF345EBFE2597908D47A0A7C0F4D7194718DACF61 ** get_address_of_factory_13() { return &___factory_13; }
	inline void set_factory_13(RegexRunnerFactory_tF345EBFE2597908D47A0A7C0F4D7194718DACF61 * value)
	{
		___factory_13 = value;
		Il2CppCodeGenWriteBarrier((&___factory_13), value);
	}

	inline static int32_t get_offset_of_capsize_14() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___capsize_14)); }
	inline int32_t get_capsize_14() const { return ___capsize_14; }
	inline int32_t* get_address_of_capsize_14() { return &___capsize_14; }
	inline void set_capsize_14(int32_t value)
	{
		___capsize_14 = value;
	}

	inline static int32_t get_offset_of_capslist_15() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B, ___capslist_15)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_capslist_15() const { return ___capslist_15; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_capslist_15() { return &___capslist_15; }
	inline void set_capslist_15(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___capslist_15 = value;
		Il2CppCodeGenWriteBarrier((&___capslist_15), value);
	}
};

struct Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B_StaticFields
{
public:
	// System.Text.RegularExpressions.FactoryCache System.Text.RegularExpressions.Regex::cache
	FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9 * ___cache_0;
	// System.Boolean System.Text.RegularExpressions.Regex::old_rx
	bool ___old_rx_1;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B_StaticFields, ___cache_0)); }
	inline FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9 * get_cache_0() const { return ___cache_0; }
	inline FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(FactoryCache_t8BE3083C5F3C2185BA9AE97F4886454CB85C06B9 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}

	inline static int32_t get_offset_of_old_rx_1() { return static_cast<int32_t>(offsetof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B_StaticFields, ___old_rx_1)); }
	inline bool get_old_rx_1() const { return ___old_rx_1; }
	inline bool* get_address_of_old_rx_1() { return &___old_rx_1; }
	inline void set_old_rx_1(bool value)
	{
		___old_rx_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEX_T829CB33C547AB824BFEAFF320BB8F59B3A76620B_H
#ifndef ANCHORINFO_T82C2F6B32A20B889C741097F41349F0B8FE17213_H
#define ANCHORINFO_T82C2F6B32A20B889C741097F41349F0B8FE17213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.AnchorInfo
struct  AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Syntax.Expression System.Text.RegularExpressions.Syntax.AnchorInfo::expr
	Expression_t6A43E18B89A25A32D44C3FD396F6139A8AA73F94 * ___expr_0;
	// System.Text.RegularExpressions.Position System.Text.RegularExpressions.Syntax.AnchorInfo::pos
	uint16_t ___pos_1;
	// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::offset
	int32_t ___offset_2;
	// System.String System.Text.RegularExpressions.Syntax.AnchorInfo::str
	String_t* ___str_3;
	// System.Int32 System.Text.RegularExpressions.Syntax.AnchorInfo::width
	int32_t ___width_4;
	// System.Boolean System.Text.RegularExpressions.Syntax.AnchorInfo::ignore
	bool ___ignore_5;

public:
	inline static int32_t get_offset_of_expr_0() { return static_cast<int32_t>(offsetof(AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213, ___expr_0)); }
	inline Expression_t6A43E18B89A25A32D44C3FD396F6139A8AA73F94 * get_expr_0() const { return ___expr_0; }
	inline Expression_t6A43E18B89A25A32D44C3FD396F6139A8AA73F94 ** get_address_of_expr_0() { return &___expr_0; }
	inline void set_expr_0(Expression_t6A43E18B89A25A32D44C3FD396F6139A8AA73F94 * value)
	{
		___expr_0 = value;
		Il2CppCodeGenWriteBarrier((&___expr_0), value);
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213, ___pos_1)); }
	inline uint16_t get_pos_1() const { return ___pos_1; }
	inline uint16_t* get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(uint16_t value)
	{
		___pos_1 = value;
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213, ___offset_2)); }
	inline int32_t get_offset_2() const { return ___offset_2; }
	inline int32_t* get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(int32_t value)
	{
		___offset_2 = value;
	}

	inline static int32_t get_offset_of_str_3() { return static_cast<int32_t>(offsetof(AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213, ___str_3)); }
	inline String_t* get_str_3() const { return ___str_3; }
	inline String_t** get_address_of_str_3() { return &___str_3; }
	inline void set_str_3(String_t* value)
	{
		___str_3 = value;
		Il2CppCodeGenWriteBarrier((&___str_3), value);
	}

	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213, ___width_4)); }
	inline int32_t get_width_4() const { return ___width_4; }
	inline int32_t* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(int32_t value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_ignore_5() { return static_cast<int32_t>(offsetof(AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213, ___ignore_5)); }
	inline bool get_ignore_5() const { return ___ignore_5; }
	inline bool* get_address_of_ignore_5() { return &___ignore_5; }
	inline void set_ignore_5(bool value)
	{
		___ignore_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHORINFO_T82C2F6B32A20B889C741097F41349F0B8FE17213_H
#ifndef CAPTUREASSERTION_T0E0C67AEFD5635C113F96B91B575E7F6022C9822_H
#define CAPTUREASSERTION_T0E0C67AEFD5635C113F96B91B575E7F6022C9822_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.CaptureAssertion
struct  CaptureAssertion_t0E0C67AEFD5635C113F96B91B575E7F6022C9822  : public Assertion_tD8EB44CA6D108EF40E4F031DF25C5962F9E69AE1
{
public:
	// System.Text.RegularExpressions.Syntax.ExpressionAssertion System.Text.RegularExpressions.Syntax.CaptureAssertion::alternate
	ExpressionAssertion_t3094472FB37123ECF6BD025105AE4FC207E36153 * ___alternate_1;
	// System.Text.RegularExpressions.Syntax.CapturingGroup System.Text.RegularExpressions.Syntax.CaptureAssertion::group
	CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7 * ___group_2;
	// System.Text.RegularExpressions.Syntax.Literal System.Text.RegularExpressions.Syntax.CaptureAssertion::literal
	Literal_tC4E6C9861C7A338A35934AA8ECC758AC63DC587A * ___literal_3;

public:
	inline static int32_t get_offset_of_alternate_1() { return static_cast<int32_t>(offsetof(CaptureAssertion_t0E0C67AEFD5635C113F96B91B575E7F6022C9822, ___alternate_1)); }
	inline ExpressionAssertion_t3094472FB37123ECF6BD025105AE4FC207E36153 * get_alternate_1() const { return ___alternate_1; }
	inline ExpressionAssertion_t3094472FB37123ECF6BD025105AE4FC207E36153 ** get_address_of_alternate_1() { return &___alternate_1; }
	inline void set_alternate_1(ExpressionAssertion_t3094472FB37123ECF6BD025105AE4FC207E36153 * value)
	{
		___alternate_1 = value;
		Il2CppCodeGenWriteBarrier((&___alternate_1), value);
	}

	inline static int32_t get_offset_of_group_2() { return static_cast<int32_t>(offsetof(CaptureAssertion_t0E0C67AEFD5635C113F96B91B575E7F6022C9822, ___group_2)); }
	inline CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7 * get_group_2() const { return ___group_2; }
	inline CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7 ** get_address_of_group_2() { return &___group_2; }
	inline void set_group_2(CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7 * value)
	{
		___group_2 = value;
		Il2CppCodeGenWriteBarrier((&___group_2), value);
	}

	inline static int32_t get_offset_of_literal_3() { return static_cast<int32_t>(offsetof(CaptureAssertion_t0E0C67AEFD5635C113F96B91B575E7F6022C9822, ___literal_3)); }
	inline Literal_tC4E6C9861C7A338A35934AA8ECC758AC63DC587A * get_literal_3() const { return ___literal_3; }
	inline Literal_tC4E6C9861C7A338A35934AA8ECC758AC63DC587A ** get_address_of_literal_3() { return &___literal_3; }
	inline void set_literal_3(Literal_tC4E6C9861C7A338A35934AA8ECC758AC63DC587A * value)
	{
		___literal_3 = value;
		Il2CppCodeGenWriteBarrier((&___literal_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTUREASSERTION_T0E0C67AEFD5635C113F96B91B575E7F6022C9822_H
#ifndef CAPTURINGGROUP_TAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7_H
#define CAPTURINGGROUP_TAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.CapturingGroup
struct  CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7  : public Group_t9EAAD4F220F411990E6DA025C34483FDC8CF905D
{
public:
	// System.Int32 System.Text.RegularExpressions.Syntax.CapturingGroup::gid
	int32_t ___gid_1;
	// System.String System.Text.RegularExpressions.Syntax.CapturingGroup::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_gid_1() { return static_cast<int32_t>(offsetof(CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7, ___gid_1)); }
	inline int32_t get_gid_1() const { return ___gid_1; }
	inline int32_t* get_address_of_gid_1() { return &___gid_1; }
	inline void set_gid_1(int32_t value)
	{
		___gid_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAPTURINGGROUP_TAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7_H
#ifndef EXPRESSIONASSERTION_T3094472FB37123ECF6BD025105AE4FC207E36153_H
#define EXPRESSIONASSERTION_T3094472FB37123ECF6BD025105AE4FC207E36153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.ExpressionAssertion
struct  ExpressionAssertion_t3094472FB37123ECF6BD025105AE4FC207E36153  : public Assertion_tD8EB44CA6D108EF40E4F031DF25C5962F9E69AE1
{
public:
	// System.Boolean System.Text.RegularExpressions.Syntax.ExpressionAssertion::reverse
	bool ___reverse_1;
	// System.Boolean System.Text.RegularExpressions.Syntax.ExpressionAssertion::negate
	bool ___negate_2;

public:
	inline static int32_t get_offset_of_reverse_1() { return static_cast<int32_t>(offsetof(ExpressionAssertion_t3094472FB37123ECF6BD025105AE4FC207E36153, ___reverse_1)); }
	inline bool get_reverse_1() const { return ___reverse_1; }
	inline bool* get_address_of_reverse_1() { return &___reverse_1; }
	inline void set_reverse_1(bool value)
	{
		___reverse_1 = value;
	}

	inline static int32_t get_offset_of_negate_2() { return static_cast<int32_t>(offsetof(ExpressionAssertion_t3094472FB37123ECF6BD025105AE4FC207E36153, ___negate_2)); }
	inline bool get_negate_2() const { return ___negate_2; }
	inline bool* get_address_of_negate_2() { return &___negate_2; }
	inline void set_negate_2(bool value)
	{
		___negate_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONASSERTION_T3094472FB37123ECF6BD025105AE4FC207E36153_H
#ifndef NONBACKTRACKINGGROUP_TE08F2C77763E8B828101008F12C6EB12CAF9C457_H
#define NONBACKTRACKINGGROUP_TE08F2C77763E8B828101008F12C6EB12CAF9C457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.NonBacktrackingGroup
struct  NonBacktrackingGroup_tE08F2C77763E8B828101008F12C6EB12CAF9C457  : public Group_t9EAAD4F220F411990E6DA025C34483FDC8CF905D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONBACKTRACKINGGROUP_TE08F2C77763E8B828101008F12C6EB12CAF9C457_H
#ifndef POSITIONASSERTION_T3F8108F0A3B70EA75CDC85F3F3ABFFCB031A1B9E_H
#define POSITIONASSERTION_T3F8108F0A3B70EA75CDC85F3F3ABFFCB031A1B9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.PositionAssertion
struct  PositionAssertion_t3F8108F0A3B70EA75CDC85F3F3ABFFCB031A1B9E  : public Expression_t6A43E18B89A25A32D44C3FD396F6139A8AA73F94
{
public:
	// System.Text.RegularExpressions.Position System.Text.RegularExpressions.Syntax.PositionAssertion::pos
	uint16_t ___pos_0;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(PositionAssertion_t3F8108F0A3B70EA75CDC85F3F3ABFFCB031A1B9E, ___pos_0)); }
	inline uint16_t get_pos_0() const { return ___pos_0; }
	inline uint16_t* get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(uint16_t value)
	{
		___pos_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASSERTION_T3F8108F0A3B70EA75CDC85F3F3ABFFCB031A1B9E_H
#ifndef REGULAREXPRESSION_T7F182BD5D2123BFE206F0734F4CCE6FE3BEE2A0E_H
#define REGULAREXPRESSION_T7F182BD5D2123BFE206F0734F4CCE6FE3BEE2A0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.RegularExpression
struct  RegularExpression_t7F182BD5D2123BFE206F0734F4CCE6FE3BEE2A0E  : public Group_t9EAAD4F220F411990E6DA025C34483FDC8CF905D
{
public:
	// System.Int32 System.Text.RegularExpressions.Syntax.RegularExpression::group_count
	int32_t ___group_count_1;

public:
	inline static int32_t get_offset_of_group_count_1() { return static_cast<int32_t>(offsetof(RegularExpression_t7F182BD5D2123BFE206F0734F4CCE6FE3BEE2A0E, ___group_count_1)); }
	inline int32_t get_group_count_1() const { return ___group_count_1; }
	inline int32_t* get_address_of_group_count_1() { return &___group_count_1; }
	inline void set_group_count_1(int32_t value)
	{
		___group_count_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGULAREXPRESSION_T7F182BD5D2123BFE206F0734F4CCE6FE3BEE2A0E_H
#ifndef URIFORMATEXCEPTION_TCF3A5E41A4694669507EF301FE0C741BF03ADED4_H
#define URIFORMATEXCEPTION_TCF3A5E41A4694669507EF301FE0C741BF03ADED4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriFormatException
struct  UriFormatException_tCF3A5E41A4694669507EF301FE0C741BF03ADED4  : public FormatException_t6148832BF22FDB9F1FB5BE067694E8326F42D592
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIFORMATEXCEPTION_TCF3A5E41A4694669507EF301FE0C741BF03ADED4_H
#ifndef BINDIPENDPOINT_TC8DC0C67D3B4E2155A6ADD8956CBA19A6E02A148_H
#define BINDIPENDPOINT_TC8DC0C67D3B4E2155A6ADD8956CBA19A6E02A148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.BindIPEndPoint
struct  BindIPEndPoint_tC8DC0C67D3B4E2155A6ADD8956CBA19A6E02A148  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDIPENDPOINT_TC8DC0C67D3B4E2155A6ADD8956CBA19A6E02A148_H
#ifndef HTTPCONTINUEDELEGATE_TDA101C5FC870D7DFDC3381D3DC9DD56367C6F61C_H
#define HTTPCONTINUEDELEGATE_TDA101C5FC870D7DFDC3381D3DC9DD56367C6F61C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpContinueDelegate
struct  HttpContinueDelegate_tDA101C5FC870D7DFDC3381D3DC9DD56367C6F61C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONTINUEDELEGATE_TDA101C5FC870D7DFDC3381D3DC9DD56367C6F61C_H
#ifndef LOCALCERTIFICATESELECTIONCALLBACK_TE1266150C68C19D7DCBB2CE25CF18122C9337981_H
#define LOCALCERTIFICATESELECTIONCALLBACK_TE1266150C68C19D7DCBB2CE25CF18122C9337981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.LocalCertificateSelectionCallback
struct  LocalCertificateSelectionCallback_tE1266150C68C19D7DCBB2CE25CF18122C9337981  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALCERTIFICATESELECTIONCALLBACK_TE1266150C68C19D7DCBB2CE25CF18122C9337981_H
#ifndef REMOTECERTIFICATEVALIDATIONCALLBACK_T1276D648C0AECBBE8AF2790937BA182B64D3A489_H
#define REMOTECERTIFICATEVALIDATIONCALLBACK_T1276D648C0AECBBE8AF2790937BA182B64D3A489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Security.RemoteCertificateValidationCallback
struct  RemoteCertificateValidationCallback_t1276D648C0AECBBE8AF2790937BA182B64D3A489  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTECERTIFICATEVALIDATIONCALLBACK_T1276D648C0AECBBE8AF2790937BA182B64D3A489_H
#ifndef EVALDELEGATE_TEAE36D514400FE58E4761FEDBB29920820C4B2FB_H
#define EVALDELEGATE_TEAE36D514400FE58E4761FEDBB29920820C4B2FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.EvalDelegate
struct  EvalDelegate_tEAE36D514400FE58E4761FEDBB29920820C4B2FB  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVALDELEGATE_TEAE36D514400FE58E4761FEDBB29920820C4B2FB_H
#ifndef COSTDELEGATE_T2EAFF95B171E834403492F55DD9CAE28B3E95C41_H
#define COSTDELEGATE_T2EAFF95B171E834403492F55DD9CAE28B3E95C41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.IntervalCollection_CostDelegate
struct  CostDelegate_t2EAFF95B171E834403492F55DD9CAE28B3E95C41  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COSTDELEGATE_T2EAFF95B171E834403492F55DD9CAE28B3E95C41_H
#ifndef BALANCINGGROUP_T840577CDC84133AFFC77F3CFF302ACBBF84290C6_H
#define BALANCINGGROUP_T840577CDC84133AFFC77F3CFF302ACBBF84290C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Syntax.BalancingGroup
struct  BalancingGroup_t840577CDC84133AFFC77F3CFF302ACBBF84290C6  : public CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7
{
public:
	// System.Text.RegularExpressions.Syntax.CapturingGroup System.Text.RegularExpressions.Syntax.BalancingGroup::balance
	CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7 * ___balance_3;

public:
	inline static int32_t get_offset_of_balance_3() { return static_cast<int32_t>(offsetof(BalancingGroup_t840577CDC84133AFFC77F3CFF302ACBBF84290C6, ___balance_3)); }
	inline CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7 * get_balance_3() const { return ___balance_3; }
	inline CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7 ** get_address_of_balance_3() { return &___balance_3; }
	inline void set_balance_3(CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7 * value)
	{
		___balance_3 = value;
		Il2CppCodeGenWriteBarrier((&___balance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BALANCINGGROUP_T840577CDC84133AFFC77F3CFF302ACBBF84290C6_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1400 = { sizeof (LinkStack_t71FC86A248572C5736999BDA60C154F0AD923949), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1400[1] = 
{
	LinkStack_t71FC86A248572C5736999BDA60C154F0AD923949::get_offset_of_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1401 = { sizeof (Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F)+ sizeof (RuntimeObject), sizeof(Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F ), 0, 0 };
extern const int32_t g_FieldOffsetTable1401[3] = 
{
	Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F::get_offset_of_Start_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F::get_offset_of_End_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Mark_t6F95F4C0F20F9D6B1059C179A2E3940267A6672F::get_offset_of_Previous_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1402 = { sizeof (GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1402[2] = 
{
	GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968::get_offset_of_list_0(),
	GroupCollection_tBEA4EDB210F04963227CBC58E1434202F3CEC968::get_offset_of_gap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1403 = { sizeof (Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C), -1, sizeof(Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1403[3] = 
{
	Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C_StaticFields::get_offset_of_Fail_3(),
	Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C::get_offset_of_success_4(),
	Group_t4D7149047343E5C195C706AC7222A4492D9D3A4C::get_offset_of_captures_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1404 = { sizeof (Interpreter_tA670BA400C626675069426A40EF3981BAC726E63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1404[16] = 
{
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_program_1(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_program_start_2(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_text_3(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_text_end_4(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_group_count_5(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_match_min_6(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_qs_7(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_scan_ptr_8(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_repeat_9(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_fast_10(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_stack_11(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_deep_12(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_marks_13(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_mark_start_14(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_mark_end_15(),
	Interpreter_tA670BA400C626675069426A40EF3981BAC726E63::get_offset_of_groups_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1405 = { sizeof (IntStack_t1716D1B970D9D96F49C07E230CFA994086B6F83D)+ sizeof (RuntimeObject), sizeof(IntStack_t1716D1B970D9D96F49C07E230CFA994086B6F83D ), 0, 0 };
extern const int32_t g_FieldOffsetTable1405[2] = 
{
	IntStack_t1716D1B970D9D96F49C07E230CFA994086B6F83D::get_offset_of_values_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntStack_t1716D1B970D9D96F49C07E230CFA994086B6F83D::get_offset_of_count_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1406 = { sizeof (RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1406[7] = 
{
	RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938::get_offset_of_start_0(),
	RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938::get_offset_of_min_1(),
	RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938::get_offset_of_max_2(),
	RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938::get_offset_of_lazy_3(),
	RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938::get_offset_of_expr_pc_4(),
	RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938::get_offset_of_previous_5(),
	RepeatContext_t53FA55142C49998080EFE4D1A0FF96573E46E938::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1407 = { sizeof (Mode_t2F28EDCB4C766AF8BF5FC594CBE2BDF7FAFE4715)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1407[4] = 
{
	Mode_t2F28EDCB4C766AF8BF5FC594CBE2BDF7FAFE4715::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1408 = { sizeof (Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F)+ sizeof (RuntimeObject), sizeof(Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1408[3] = 
{
	Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F::get_offset_of_low_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F::get_offset_of_high_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Interval_t3D43CC5930ADDA26C1D3C50CEFCA15EC5481374F::get_offset_of_contiguous_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1409 = { sizeof (IntervalCollection_t33C1E9164A6B24465B908CB2D90E23C9F60F7DD5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1409[1] = 
{
	IntervalCollection_t33C1E9164A6B24465B908CB2D90E23C9F60F7DD5::get_offset_of_intervals_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1410 = { sizeof (Enumerator_t34D7E14B7AA7548F34E7C1BDA39BD0583A1590E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1410[2] = 
{
	Enumerator_t34D7E14B7AA7548F34E7C1BDA39BD0583A1590E4::get_offset_of_list_0(),
	Enumerator_t34D7E14B7AA7548F34E7C1BDA39BD0583A1590E4::get_offset_of_ptr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1411 = { sizeof (CostDelegate_t2EAFF95B171E834403492F55DD9CAE28B3E95C41), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1412 = { sizeof (MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1412[2] = 
{
	MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E::get_offset_of_current_0(),
	MatchCollection_tD651B345093D8CEC4F8E3302458C17355E017B2E::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1413 = { sizeof (Enumerator_tEA319EDBF9ADA404733366A75202340DAD456FD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1413[2] = 
{
	Enumerator_tEA319EDBF9ADA404733366A75202340DAD456FD8::get_offset_of_index_0(),
	Enumerator_tEA319EDBF9ADA404733366A75202340DAD456FD8::get_offset_of_coll_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1414 = { sizeof (Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53), -1, sizeof(Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1414[5] = 
{
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53::get_offset_of_regex_6(),
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53::get_offset_of_machine_7(),
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53::get_offset_of_text_length_8(),
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53::get_offset_of_groups_9(),
	Match_t2D7477CC8013F3D56D21C807BA9D3E8EB3DFBE53_StaticFields::get_offset_of_empty_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1415 = { sizeof (Parser_t781E0BC79514BED615EBBD67291C3D4B48519019), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1415[6] = 
{
	Parser_t781E0BC79514BED615EBBD67291C3D4B48519019::get_offset_of_pattern_0(),
	Parser_t781E0BC79514BED615EBBD67291C3D4B48519019::get_offset_of_ptr_1(),
	Parser_t781E0BC79514BED615EBBD67291C3D4B48519019::get_offset_of_caps_2(),
	Parser_t781E0BC79514BED615EBBD67291C3D4B48519019::get_offset_of_refs_3(),
	Parser_t781E0BC79514BED615EBBD67291C3D4B48519019::get_offset_of_num_groups_4(),
	Parser_t781E0BC79514BED615EBBD67291C3D4B48519019::get_offset_of_gap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1416 = { sizeof (QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849), -1, sizeof(QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1416[7] = 
{
	QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849::get_offset_of_str_0(),
	QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849::get_offset_of_len_1(),
	QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849::get_offset_of_ignore_2(),
	QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849::get_offset_of_reverse_3(),
	QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849::get_offset_of_shift_4(),
	QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849::get_offset_of_shiftExtended_5(),
	QuickSearch_t6623C87766BCF0EB6754150BF8B4EF4AEB718849_StaticFields::get_offset_of_THRESHOLD_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1417 = { sizeof (Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B), -1, sizeof(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1417[16] = 
{
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B_StaticFields::get_offset_of_cache_0(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B_StaticFields::get_offset_of_old_rx_1(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_machineFactory_2(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_mapping_3(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_group_count_4(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_gap_5(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_refsInitialized_6(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_group_names_7(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_group_numbers_8(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_pattern_9(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_roptions_10(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_capnames_11(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_caps_12(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_factory_13(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_capsize_14(),
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B::get_offset_of_capslist_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1418 = { sizeof (RegexOptions_t6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1418[11] = 
{
	RegexOptions_t6DDB25DC8ABB8CFC65500D99B4CDAFF3B26D0624::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1419 = { sizeof (RegexRunnerFactory_tF345EBFE2597908D47A0A7C0F4D7194718DACF61), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1420 = { sizeof (RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740), -1, sizeof(RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1420[14] = 
{
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_program_1(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_str_2(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_string_start_3(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_string_end_4(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_group_count_5(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_groups_6(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_eval_del_7(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_marks_8(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_mark_start_9(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_mark_end_10(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_stack_11(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_repeat_12(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740::get_offset_of_deep_13(),
	RxInterpreter_tE82B3AFD4A9B2947C0C7BE8B18DA3C6B51719740_StaticFields::get_offset_of_trace_rx_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1421 = { sizeof (IntStack_t70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE)+ sizeof (RuntimeObject), sizeof(IntStack_t70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE ), 0, 0 };
extern const int32_t g_FieldOffsetTable1421[2] = 
{
	IntStack_t70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE::get_offset_of_values_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntStack_t70C2029B6E79BCDBF2CD9F378F0F018383FF2ABE::get_offset_of_count_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1422 = { sizeof (RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1422[7] = 
{
	RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622::get_offset_of_start_0(),
	RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622::get_offset_of_min_1(),
	RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622::get_offset_of_max_2(),
	RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622::get_offset_of_lazy_3(),
	RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622::get_offset_of_expr_pc_4(),
	RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622::get_offset_of_previous_5(),
	RepeatContext_t720E27A2CE416B69952FD9273E80FB41DD337622::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1423 = { sizeof (RxLinkRef_tE55F192CDD19A7BEF91C841779877F95DC8DED89), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1423[2] = 
{
	RxLinkRef_tE55F192CDD19A7BEF91C841779877F95DC8DED89::get_offset_of_offsets_0(),
	RxLinkRef_tE55F192CDD19A7BEF91C841779877F95DC8DED89::get_offset_of_current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1424 = { sizeof (RxCompiler_tCBBC6E8FA1D074636E9B158778B40E01B1384708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1424[2] = 
{
	RxCompiler_tCBBC6E8FA1D074636E9B158778B40E01B1384708::get_offset_of_program_0(),
	RxCompiler_tCBBC6E8FA1D074636E9B158778B40E01B1384708::get_offset_of_curpos_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1425 = { sizeof (RxInterpreterFactory_t62BD38060C13E7258420C8AA03BCB07C70754081), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1425[5] = 
{
	RxInterpreterFactory_t62BD38060C13E7258420C8AA03BCB07C70754081::get_offset_of_mapping_0(),
	RxInterpreterFactory_t62BD38060C13E7258420C8AA03BCB07C70754081::get_offset_of_program_1(),
	RxInterpreterFactory_t62BD38060C13E7258420C8AA03BCB07C70754081::get_offset_of_eval_del_2(),
	RxInterpreterFactory_t62BD38060C13E7258420C8AA03BCB07C70754081::get_offset_of_namesMapping_3(),
	RxInterpreterFactory_t62BD38060C13E7258420C8AA03BCB07C70754081::get_offset_of_gap_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1426 = { sizeof (RxOp_tEA232A1F515411AD944C2FCA40CCAD2F4DC34B48)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1426[160] = 
{
	RxOp_tEA232A1F515411AD944C2FCA40CCAD2F4DC34B48::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1427 = { sizeof (ExpressionCollection_tCA7FC699936CBAE10FB637D8927714CD3AEA282A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1428 = { sizeof (Expression_t6A43E18B89A25A32D44C3FD396F6139A8AA73F94), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1429 = { sizeof (CompositeExpression_t750AF90954FAB323F201EA628A6123FCAC3A46EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1429[1] = 
{
	CompositeExpression_t750AF90954FAB323F201EA628A6123FCAC3A46EC::get_offset_of_expressions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1430 = { sizeof (Group_t9EAAD4F220F411990E6DA025C34483FDC8CF905D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1431 = { sizeof (RegularExpression_t7F182BD5D2123BFE206F0734F4CCE6FE3BEE2A0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1431[1] = 
{
	RegularExpression_t7F182BD5D2123BFE206F0734F4CCE6FE3BEE2A0E::get_offset_of_group_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1432 = { sizeof (CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1432[2] = 
{
	CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7::get_offset_of_gid_1(),
	CapturingGroup_tAFE8488C7C2CC06936764A0173F1C3DA0F67BEE7::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1433 = { sizeof (BalancingGroup_t840577CDC84133AFFC77F3CFF302ACBBF84290C6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1433[1] = 
{
	BalancingGroup_t840577CDC84133AFFC77F3CFF302ACBBF84290C6::get_offset_of_balance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1434 = { sizeof (NonBacktrackingGroup_tE08F2C77763E8B828101008F12C6EB12CAF9C457), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1435 = { sizeof (Repetition_t60545A1C9349248736914B4A126F1919F346D4B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1435[3] = 
{
	Repetition_t60545A1C9349248736914B4A126F1919F346D4B7::get_offset_of_min_1(),
	Repetition_t60545A1C9349248736914B4A126F1919F346D4B7::get_offset_of_max_2(),
	Repetition_t60545A1C9349248736914B4A126F1919F346D4B7::get_offset_of_lazy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1436 = { sizeof (Assertion_tD8EB44CA6D108EF40E4F031DF25C5962F9E69AE1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1437 = { sizeof (CaptureAssertion_t0E0C67AEFD5635C113F96B91B575E7F6022C9822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1437[3] = 
{
	CaptureAssertion_t0E0C67AEFD5635C113F96B91B575E7F6022C9822::get_offset_of_alternate_1(),
	CaptureAssertion_t0E0C67AEFD5635C113F96B91B575E7F6022C9822::get_offset_of_group_2(),
	CaptureAssertion_t0E0C67AEFD5635C113F96B91B575E7F6022C9822::get_offset_of_literal_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1438 = { sizeof (ExpressionAssertion_t3094472FB37123ECF6BD025105AE4FC207E36153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1438[2] = 
{
	ExpressionAssertion_t3094472FB37123ECF6BD025105AE4FC207E36153::get_offset_of_reverse_1(),
	ExpressionAssertion_t3094472FB37123ECF6BD025105AE4FC207E36153::get_offset_of_negate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1439 = { sizeof (Alternation_t7CD0712B6ECD03E6F746753266C725B8EADB196F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1440 = { sizeof (Literal_tC4E6C9861C7A338A35934AA8ECC758AC63DC587A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1440[2] = 
{
	Literal_tC4E6C9861C7A338A35934AA8ECC758AC63DC587A::get_offset_of_str_0(),
	Literal_tC4E6C9861C7A338A35934AA8ECC758AC63DC587A::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1441 = { sizeof (PositionAssertion_t3F8108F0A3B70EA75CDC85F3F3ABFFCB031A1B9E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1441[1] = 
{
	PositionAssertion_t3F8108F0A3B70EA75CDC85F3F3ABFFCB031A1B9E::get_offset_of_pos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1442 = { sizeof (Reference_t683D20A9EF06251885F0ACC287F31F5B670F47A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1442[2] = 
{
	Reference_t683D20A9EF06251885F0ACC287F31F5B670F47A6::get_offset_of_group_0(),
	Reference_t683D20A9EF06251885F0ACC287F31F5B670F47A6::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1443 = { sizeof (BackslashNumber_t9BC00EB1624ACAD40C1D9DD453AD365DE92F9B45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1443[2] = 
{
	BackslashNumber_t9BC00EB1624ACAD40C1D9DD453AD365DE92F9B45::get_offset_of_literal_2(),
	BackslashNumber_t9BC00EB1624ACAD40C1D9DD453AD365DE92F9B45::get_offset_of_ecma_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1444 = { sizeof (CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365), -1, sizeof(CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1444[6] = 
{
	CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365_StaticFields::get_offset_of_upper_case_characters_0(),
	CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365::get_offset_of_negate_1(),
	CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365::get_offset_of_ignore_2(),
	CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365::get_offset_of_pos_cats_3(),
	CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365::get_offset_of_neg_cats_4(),
	CharacterClass_tC781D8EF48C7AF5E49E5F15C7B82712243C97365::get_offset_of_intervals_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1445 = { sizeof (AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1445[6] = 
{
	AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213::get_offset_of_expr_0(),
	AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213::get_offset_of_pos_1(),
	AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213::get_offset_of_offset_2(),
	AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213::get_offset_of_str_3(),
	AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213::get_offset_of_width_4(),
	AnchorInfo_t82C2F6B32A20B889C741097F41349F0B8FE17213::get_offset_of_ignore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1446 = { sizeof (UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1446[10] = 
{
	UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295::get_offset_of_scheme_0(),
	UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295::get_offset_of_host_1(),
	UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295::get_offset_of_port_2(),
	UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295::get_offset_of_path_3(),
	UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295::get_offset_of_query_4(),
	UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295::get_offset_of_fragment_5(),
	UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295::get_offset_of_username_6(),
	UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295::get_offset_of_password_7(),
	UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295::get_offset_of_uri_8(),
	UriBuilder_t9417C2D2E047DBD48E41898A4978B618EE500295::get_offset_of_modified_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1447 = { sizeof (Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E), -1, sizeof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1447[36] = 
{
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_isUnixFilePath_0(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_source_1(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_scheme_2(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_host_3(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_port_4(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_path_5(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_query_6(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_fragment_7(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_userinfo_8(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_isUnc_9(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_isOpaquePart_10(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_isAbsoluteUri_11(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_userEscaped_12(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_cachedAbsoluteUri_13(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_cachedToString_14(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_cachedLocalPath_15(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_cachedHashCode_16(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_hexUpperChars_17(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_SchemeDelimiter_18(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_UriSchemeFile_19(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_UriSchemeFtp_20(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_UriSchemeGopher_21(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_UriSchemeHttp_22(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_UriSchemeHttps_23(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_UriSchemeMailto_24(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_UriSchemeNews_25(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_UriSchemeNntp_26(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_UriSchemeNetPipe_27(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_UriSchemeNetTcp_28(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_schemes_29(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E::get_offset_of_parser_30(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_U3CU3Ef__switchU24map1C_31(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_U3CU3Ef__switchU24map1D_32(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_U3CU3Ef__switchU24map1E_33(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_U3CU3Ef__switchU24map1F_34(),
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields::get_offset_of_U3CU3Ef__switchU24map20_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1448 = { sizeof (UriScheme_tAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE)+ sizeof (RuntimeObject), sizeof(UriScheme_tAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1448[3] = 
{
	UriScheme_tAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE::get_offset_of_scheme_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UriScheme_tAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE::get_offset_of_delimiter_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	UriScheme_tAC5BAEC36F275C5986740C3BE3FE2BEA8B512BDE::get_offset_of_defaultPort_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1449 = { sizeof (UriFormatException_tCF3A5E41A4694669507EF301FE0C741BF03ADED4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1450 = { sizeof (UriHostNameType_tD5C10F880C524C8028C474B23B619228AE144479)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1450[6] = 
{
	UriHostNameType_tD5C10F880C524C8028C474B23B619228AE144479::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1451 = { sizeof (UriKind_t602575BB5018420CCE336A35E0D95B1E18F13069)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1451[4] = 
{
	UriKind_t602575BB5018420CCE336A35E0D95B1E18F13069::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1452 = { sizeof (UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A), -1, sizeof(UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1452[6] = 
{
	UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_StaticFields::get_offset_of_lock_object_0(),
	UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_StaticFields::get_offset_of_table_1(),
	UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A::get_offset_of_scheme_name_2(),
	UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A::get_offset_of_default_port_3(),
	UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_StaticFields::get_offset_of_uri_regex_4(),
	UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A_StaticFields::get_offset_of_auth_regex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1453 = { sizeof (UriPartial_tDD0004CFD64BC37CB615A5A3428BB2CC05CB5912)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1453[5] = 
{
	UriPartial_tDD0004CFD64BC37CB615A5A3428BB2CC05CB5912::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1454 = { sizeof (UriTypeConverter_tA85B11B1FD8D9C1FD1FF5DE1FCA9AAE13CCF1E64), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1455 = { sizeof (BindIPEndPoint_tC8DC0C67D3B4E2155A6ADD8956CBA19A6E02A148), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1456 = { sizeof (HttpContinueDelegate_tDA101C5FC870D7DFDC3381D3DC9DD56367C6F61C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1457 = { sizeof (LocalCertificateSelectionCallback_tE1266150C68C19D7DCBB2CE25CF18122C9337981), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1458 = { sizeof (RemoteCertificateValidationCallback_t1276D648C0AECBBE8AF2790937BA182B64D3A489), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1459 = { sizeof (EvalDelegate_tEAE36D514400FE58E4761FEDBB29920820C4B2FB), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1460 = { sizeof (U3CPrivateImplementationDetailsU3E_t6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B), -1, sizeof(U3CPrivateImplementationDetailsU3E_t6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1460[4] = 
{
	U3CPrivateImplementationDetailsU3E_t6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_StaticFields::get_offset_of_U24U24fieldU2D1_0(),
	U3CPrivateImplementationDetailsU3E_t6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_StaticFields::get_offset_of_U24U24fieldU2D3_1(),
	U3CPrivateImplementationDetailsU3E_t6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_StaticFields::get_offset_of_U24U24fieldU2D4_2(),
	U3CPrivateImplementationDetailsU3E_t6CD60EA7FE3C11FCDC45D632AC3B1A66A8913A3B_StaticFields::get_offset_of_U24U24fieldU2D5_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1461 = { sizeof (U24ArrayTypeU2416_t2DDBD265BE6606DAC46187FDC58335362DC6AE4C)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2416_t2DDBD265BE6606DAC46187FDC58335362DC6AE4C ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1462 = { sizeof (U24ArrayTypeU24128_tC2FBD9B9E13A39460761EEDD6034845832CDBAD7)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU24128_tC2FBD9B9E13A39460761EEDD6034845832CDBAD7 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1463 = { sizeof (U24ArrayTypeU2412_t91D3DF413568F08A27A79C7AD08EB23506A60F51)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU2412_t91D3DF413568F08A27A79C7AD08EB23506A60F51 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1464 = { sizeof (U3CModuleU3E_t59A29D11DE086BC643857AFBADB05C659B348470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1465 = { sizeof (MonoTODOAttribute_t2D21BFE6EE9459D34164629D1D797DBB606A57A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1466 = { sizeof (XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1466[3] = 
{
	XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8::get_offset_of_selectorPaths_0(),
	XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8::get_offset_of_fields_1(),
	XsdIdentitySelector_t6ABC7388F5B0EECB1374E1B6137E2ACC506699D8::get_offset_of_cachedFields_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1467 = { sizeof (XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1467[2] = 
{
	XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7::get_offset_of_fieldPaths_0(),
	XsdIdentityField_t47467B5E2857C75A53026E207DC4063868D49DC7::get_offset_of_index_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1468 = { sizeof (XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1468[2] = 
{
	XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE::get_offset_of_OrderedSteps_0(),
	XsdIdentityPath_tF37220F64B3F2E405306A9813980F5F02A788BFE::get_offset_of_Descendants_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1469 = { sizeof (XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1469[6] = 
{
	XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D::get_offset_of_IsCurrent_0(),
	XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D::get_offset_of_IsAttribute_1(),
	XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D::get_offset_of_IsAnyName_2(),
	XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D::get_offset_of_NsName_3(),
	XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D::get_offset_of_Name_4(),
	XsdIdentityStep_t870B49AFC712ECC1798E341EFD170C1B841ED48D::get_offset_of_Namespace_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1470 = { sizeof (XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1470[13] = 
{
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_entry_0(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_field_1(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldFound_2(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldLineNumber_3(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldLinePosition_4(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldHasLineInfo_5(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldType_6(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_Identity_7(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_IsXsiNil_8(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldFoundDepth_9(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_FieldFoundPath_10(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_Consuming_11(),
	XsdKeyEntryField_t46A4E7087BCB9D206208679EA739175225D551D2::get_offset_of_Consumed_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1471 = { sizeof (XsdKeyEntryFieldCollection_t8536E011BAA035EF7757EE7BBE9843160ABBF4EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1472 = { sizeof (XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1472[8] = 
{
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_StartDepth_0(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_SelectorLineNumber_1(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_SelectorLinePosition_2(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_SelectorHasLineInfo_3(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_KeyFields_4(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_KeyRefFound_5(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_OwnerSequence_6(),
	XsdKeyEntry_t1A916C808B855D066198566E0DD1D93B3C8722B3::get_offset_of_keyFound_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1473 = { sizeof (XsdKeyEntryCollection_tF37F133E46ECFF1F08CBB56894E1B18CB931B265), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1474 = { sizeof (XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1474[9] = 
{
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_alwaysTrue_0(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_selector_1(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_source_2(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_qname_3(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_refKeyName_4(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_Entries_5(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_FinishedEntries_6(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_StartDepth_7(),
	XsdKeyTable_t75417D656D1C88FCE37B501DEA7BCE4CCE1A4AF6::get_offset_of_ReferencedKey_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1475 = { sizeof (XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68), -1, sizeof(XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1475[6] = 
{
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68::get_offset_of_table_0(),
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68::get_offset_of_processContents_1(),
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68::get_offset_of_CurrentElement_2(),
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68::get_offset_of_ContextStack_3(),
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68::get_offset_of_Context_4(),
	XsdParticleStateManager_t9C730C7B40880A4D7409B129A8D041BD47438E68_StaticFields::get_offset_of_U3CU3Ef__switchU24map2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1476 = { sizeof (XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74), -1, sizeof(XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1476[3] = 
{
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74_StaticFields::get_offset_of_invalid_0(),
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74::get_offset_of_occured_1(),
	XsdValidationState_t6CEE1D803E048D746FBDC7439403F070F3D66C74::get_offset_of_manager_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1477 = { sizeof (XsdElementValidationState_tE3F398F18D4968EA5F91FEE92C646B19389C1C13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1477[1] = 
{
	XsdElementValidationState_tE3F398F18D4968EA5F91FEE92C646B19389C1C13::get_offset_of_element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1478 = { sizeof (XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1478[4] = 
{
	XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E::get_offset_of_seq_3(),
	XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E::get_offset_of_current_4(),
	XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E::get_offset_of_currentAutomata_5(),
	XsdSequenceValidationState_tB6D68439C7FBDA9D3510C267EF0038D735D6658E::get_offset_of_emptiable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1479 = { sizeof (XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1479[3] = 
{
	XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2::get_offset_of_choice_3(),
	XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2::get_offset_of_emptiable_4(),
	XsdChoiceValidationState_t6A72354CC424E101B3DB7472BBD8ADA7F61ACFC2::get_offset_of_emptiableComputed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1480 = { sizeof (XsdAllValidationState_t4ED82B2BF3F8403103526301F3BB097F4F951758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1480[2] = 
{
	XsdAllValidationState_t4ED82B2BF3F8403103526301F3BB097F4F951758::get_offset_of_all_3(),
	XsdAllValidationState_t4ED82B2BF3F8403103526301F3BB097F4F951758::get_offset_of_consumed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1481 = { sizeof (XsdAnyValidationState_t6ABF8A32C88C9171AA284E91FB0ADED6F91267A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1481[1] = 
{
	XsdAnyValidationState_t6ABF8A32C88C9171AA284E91FB0ADED6F91267A3::get_offset_of_any_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1482 = { sizeof (XsdAppendedValidationState_tF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1482[2] = 
{
	XsdAppendedValidationState_tF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD::get_offset_of_head_3(),
	XsdAppendedValidationState_tF78BEDEB51F43A455D52D86B17F4E77A28D2BFCD::get_offset_of_rest_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1483 = { sizeof (XsdEmptyValidationState_t9F4AC1CC30BFB15E1B14F72386E85F2E78EFD045), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1484 = { sizeof (XsdInvalidValidationState_t3532998F32C0C00F8E0F0C199D2308C867084FFF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1485 = { sizeof (XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3), -1, sizeof(XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1485[30] = 
{
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields::get_offset_of_emptyAttributeArray_2(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_reader_3(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_resolver_4(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_sourceReaderSchemaInfo_5(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_readerLineInfo_6(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_validationType_7(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_schemas_8(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_namespaces_9(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_validationStarted_10(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_checkIdentity_11(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_idManager_12(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_checkKeyConstraints_13(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_keyTables_14(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_currentKeyFieldConsumers_15(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_tmpKeyrefPool_16(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_elementQNameStack_17(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_state_18(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_skipValidationDepth_19(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_xsiNilDepth_20(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_storedCharacters_21(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_shouldValidateCharacters_22(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_defaultAttributes_23(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_currentDefaultAttribute_24(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_defaultAttributesCache_25(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_defaultAttributeConsumed_26(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_currentAttrType_27(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3::get_offset_of_ValidationEventHandler_28(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields::get_offset_of_U3CU3Ef__switchU24map3_29(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields::get_offset_of_U3CU3Ef__switchU24map4_30(),
	XsdValidatingReader_t2C211F16BDE384F56FB73B293505DEADE5AFCEB3_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1486 = { sizeof (XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1486[3] = 
{
	XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD::get_offset_of_xsi_type_0(),
	XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD::get_offset_of_State_1(),
	XsdValidationContext_tC54B6F9C7FD147249C53E35CED23F23D1DD1F1CD::get_offset_of_element_stack_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1487 = { sizeof (XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1487[3] = 
{
	XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E::get_offset_of_idList_0(),
	XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E::get_offset_of_missingIDReferences_1(),
	XsdIDManager_t1859711C9A89E294EF4AC6BF8E7B1C6C4EEC6D3E::get_offset_of_thisElementId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1488 = { sizeof (XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7), -1, sizeof(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1488[10] = 
{
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_xsobj_0(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_ResolvedProcessing_1(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_TargetNamespace_2(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_SkipCompile_3(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_HasValueAny_4(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_HasValueLocal_5(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_HasValueOther_6(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_HasValueTargetNamespace_7(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7::get_offset_of_ResolvedNamespaces_8(),
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1489 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1490 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1491 = { sizeof (ConformanceLevel_tBA47CA81D3009DF2AD8DC7CE769E6982D1EA96CB)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1491[4] = 
{
	ConformanceLevel_tBA47CA81D3009DF2AD8DC7CE769E6982D1EA96CB::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1492 = { sizeof (DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1492[3] = 
{
	DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340::get_offset_of_root_0(),
	DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340::get_offset_of_choiceTable_1(),
	DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340::get_offset_of_sequenceTable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1493 = { sizeof (DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1493[1] = 
{
	DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7::get_offset_of_root_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1494 = { sizeof (DTDElementAutomata_t4F31CD3B4096E7EA16D973937D76EB96F23B1BD2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1494[1] = 
{
	DTDElementAutomata_t4F31CD3B4096E7EA16D973937D76EB96F23B1BD2::get_offset_of_name_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1495 = { sizeof (DTDChoiceAutomata_t50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1495[4] = 
{
	DTDChoiceAutomata_t50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE::get_offset_of_left_1(),
	DTDChoiceAutomata_t50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE::get_offset_of_right_2(),
	DTDChoiceAutomata_t50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE::get_offset_of_hasComputedEmptiable_3(),
	DTDChoiceAutomata_t50BE3C3543DCBCAA84EAEB90631C49B2D0D7F3FE::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1496 = { sizeof (DTDSequenceAutomata_tD0DB5A3840DEAA9B4925544B078572C3D18433F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1496[4] = 
{
	DTDSequenceAutomata_tD0DB5A3840DEAA9B4925544B078572C3D18433F8::get_offset_of_left_1(),
	DTDSequenceAutomata_tD0DB5A3840DEAA9B4925544B078572C3D18433F8::get_offset_of_right_2(),
	DTDSequenceAutomata_tD0DB5A3840DEAA9B4925544B078572C3D18433F8::get_offset_of_hasComputedEmptiable_3(),
	DTDSequenceAutomata_tD0DB5A3840DEAA9B4925544B078572C3D18433F8::get_offset_of_cachedEmptiable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1497 = { sizeof (DTDOneOrMoreAutomata_t4139380E3B7FCA01D730B0DE9133E1A69A32CBFD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1497[1] = 
{
	DTDOneOrMoreAutomata_t4139380E3B7FCA01D730B0DE9133E1A69A32CBFD::get_offset_of_children_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1498 = { sizeof (DTDEmptyAutomata_t055216A06878AB4743EF9FC55703460B24117B41), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1499 = { sizeof (DTDAnyAutomata_t7D15632FA965F7D26DBF45EAA3D3BA11B59E7A06), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif

﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mono.Xml.Schema.XdtAnyAtomicType
struct XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7;
// Mono.Xml.Schema.XdtDayTimeDuration
struct XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F;
// Mono.Xml.Schema.XdtUntypedAtomic
struct XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED;
// Mono.Xml.Schema.XdtYearMonthDuration
struct XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68;
// Mono.Xml.Schema.XmlSchemaUri
struct XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0;
// Mono.Xml.Schema.XsdAnySimpleType
struct XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0;
// Mono.Xml.Schema.XsdAnyURI
struct XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A;
// Mono.Xml.Schema.XsdBase64Binary
struct XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F;
// Mono.Xml.Schema.XsdBoolean
struct XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959;
// Mono.Xml.Schema.XsdByte
struct XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351;
// Mono.Xml.Schema.XsdDate
struct XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964;
// Mono.Xml.Schema.XsdDateTime
struct XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D;
// Mono.Xml.Schema.XsdDecimal
struct XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE;
// Mono.Xml.Schema.XsdDouble
struct XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269;
// Mono.Xml.Schema.XsdDuration
struct XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78;
// Mono.Xml.Schema.XsdEntities
struct XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C;
// Mono.Xml.Schema.XsdEntity
struct XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371;
// Mono.Xml.Schema.XsdFloat
struct XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8;
// Mono.Xml.Schema.XsdGDay
struct XsdGDay_tEC936DF85B2878EB240E093D4B56BBDDC75A463B;
// Mono.Xml.Schema.XsdGMonth
struct XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312;
// Mono.Xml.Schema.XsdGMonthDay
struct XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669;
// Mono.Xml.Schema.XsdGYear
struct XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2;
// Mono.Xml.Schema.XsdGYearMonth
struct XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385;
// Mono.Xml.Schema.XsdHexBinary
struct XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229;
// Mono.Xml.Schema.XsdID
struct XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B;
// Mono.Xml.Schema.XsdIDRef
struct XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B;
// Mono.Xml.Schema.XsdIDRefs
struct XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F;
// Mono.Xml.Schema.XsdInt
struct XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F;
// Mono.Xml.Schema.XsdInteger
struct XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348;
// Mono.Xml.Schema.XsdLanguage
struct XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85;
// Mono.Xml.Schema.XsdLong
struct XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC;
// Mono.Xml.Schema.XsdNCName
struct XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7;
// Mono.Xml.Schema.XsdNMToken
struct XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91;
// Mono.Xml.Schema.XsdNMTokens
struct XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095;
// Mono.Xml.Schema.XsdName
struct XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8;
// Mono.Xml.Schema.XsdNegativeInteger
struct XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B;
// Mono.Xml.Schema.XsdNonNegativeInteger
struct XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30;
// Mono.Xml.Schema.XsdNonPositiveInteger
struct XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62;
// Mono.Xml.Schema.XsdNormalizedString
struct XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0;
// Mono.Xml.Schema.XsdNotation
struct XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3;
// Mono.Xml.Schema.XsdPositiveInteger
struct XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2;
// Mono.Xml.Schema.XsdQName
struct XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0;
// Mono.Xml.Schema.XsdShort
struct XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332;
// Mono.Xml.Schema.XsdString
struct XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD;
// Mono.Xml.Schema.XsdTime
struct XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C;
// Mono.Xml.Schema.XsdToken
struct XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C;
// Mono.Xml.Schema.XsdUnsignedByte
struct XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256;
// Mono.Xml.Schema.XsdUnsignedInt
struct XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32;
// Mono.Xml.Schema.XsdUnsignedLong
struct XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9;
// Mono.Xml.Schema.XsdUnsignedShort
struct XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785;
// Mono.Xml.Schema.XsdWildcard
struct XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7;
// System.Byte[]
struct ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.ArrayList
struct ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1;
// System.Collections.IDictionary
struct IDictionary_tD35B9437F08BE98D1E0B295CC73C48E168CAB316;
// System.Collections.IEnumerator
struct IEnumerator_t5F4AD85C6EA424A50584F741049EA645DBD8EEFC;
// System.Collections.Stack
struct Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5;
// System.Globalization.CompareInfo
struct CompareInfo_t704FFB923C9EF69DFAE63C1950E5B466A94C0F4B;
// System.Globalization.CultureInfo
struct CultureInfo_t2089C879B1CBB363EF1CF59D058AD2D2B0C9D28D;
// System.IO.Stream
struct Stream_tCFD27E43C18381861212C0288CACF467FB602009;
// System.IO.StringWriter
struct StringWriter_t3C72AF79544098A9167A3748E65721DB7ED2960D;
// System.IO.TextReader
struct TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520;
// System.IO.TextWriter
struct TextWriter_t584EB24550495420EF2EC2E56D5404D0A0A79A45;
// System.IntPtr[]
struct IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t3C5F8BAEFAC94CF69694273ACCB6CB41355E0B5C;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B;
// System.Text.Decoder
struct Decoder_t95F6C32901671048111BEE6EAD9273760D7881C6;
// System.Text.Encoding
struct Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6;
// System.UriParser
struct UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B;
// System.Xml.Schema.XmlSchema
struct XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696;
// System.Xml.Schema.XmlSchemaAnnotation
struct XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3;
// System.Xml.Schema.XmlSchemaContent
struct XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA;
// System.Xml.Schema.XmlSchemaContentModel
struct XmlSchemaContentModel_tB057D601009DC1517A2A538E001E592F465568D9;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19;
// System.Xml.Schema.XmlSchemaException
struct XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747;
// System.Xml.Schema.XmlSchemaObjectCollection
struct XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D;
// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16;
// System.Xml.Schema.XmlSchemaParticle
struct XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE;
// System.Xml.Serialization.XmlSerializerNamespaces
struct XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3;
// System.Xml.XmlAttribute[]
struct XmlAttributeU5BU5D_t48E79142153B9C691C51308E60A01F22035CBD01;
// System.Xml.XmlException
struct XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1;
// System.Xml.XmlInputStream
struct XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629;
// System.Xml.XmlNameTable
struct XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86;
// System.Xml.XmlNode[]
struct XmlNodeU5BU5D_t880B356D14B59983F6FDD527C6B3C1B9BD673739;
// System.Xml.XmlParserInput/XmlParserInputSource
struct XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004;
// System.Xml.XmlTextWriter/XmlNodeInfo[]
struct XmlNodeInfoU5BU5D_tCB7829A10CA8B41C2A87C0CFE790ADD724A1D80D;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EVENTARGS_TA4C15C1D2AB4B139169B1942C1477933E00DCA17_H
#define EVENTARGS_TA4C15C1D2AB4B139169B1942C1477933E00DCA17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17  : public RuntimeObject
{
public:

public:
};

struct EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17_StaticFields, ___Empty_0)); }
	inline EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_TA4C15C1D2AB4B139169B1942C1477933E00DCA17_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_T05F62A8AC86E36BAE3063CA28097945DE9E179C4_H
#define MARSHALBYREFOBJECT_T05F62A8AC86E36BAE3063CA28097945DE9E179C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t05F62A8AC86E36BAE3063CA28097945DE9E179C4  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t3C5F8BAEFAC94CF69694273ACCB6CB41355E0B5C * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t05F62A8AC86E36BAE3063CA28097945DE9E179C4, ____identity_0)); }
	inline ServerIdentity_t3C5F8BAEFAC94CF69694273ACCB6CB41355E0B5C * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t3C5F8BAEFAC94CF69694273ACCB6CB41355E0B5C ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t3C5F8BAEFAC94CF69694273ACCB6CB41355E0B5C * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T05F62A8AC86E36BAE3063CA28097945DE9E179C4_H
#ifndef URI_TF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_H
#define URI_TF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E  : public RuntimeObject
{
public:
	// System.Boolean System.Uri::isUnixFilePath
	bool ___isUnixFilePath_0;
	// System.String System.Uri::source
	String_t* ___source_1;
	// System.String System.Uri::scheme
	String_t* ___scheme_2;
	// System.String System.Uri::host
	String_t* ___host_3;
	// System.Int32 System.Uri::port
	int32_t ___port_4;
	// System.String System.Uri::path
	String_t* ___path_5;
	// System.String System.Uri::query
	String_t* ___query_6;
	// System.String System.Uri::fragment
	String_t* ___fragment_7;
	// System.String System.Uri::userinfo
	String_t* ___userinfo_8;
	// System.Boolean System.Uri::isUnc
	bool ___isUnc_9;
	// System.Boolean System.Uri::isOpaquePart
	bool ___isOpaquePart_10;
	// System.Boolean System.Uri::isAbsoluteUri
	bool ___isAbsoluteUri_11;
	// System.Boolean System.Uri::userEscaped
	bool ___userEscaped_12;
	// System.String System.Uri::cachedAbsoluteUri
	String_t* ___cachedAbsoluteUri_13;
	// System.String System.Uri::cachedToString
	String_t* ___cachedToString_14;
	// System.String System.Uri::cachedLocalPath
	String_t* ___cachedLocalPath_15;
	// System.Int32 System.Uri::cachedHashCode
	int32_t ___cachedHashCode_16;
	// System.UriParser System.Uri::parser
	UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A * ___parser_30;

public:
	inline static int32_t get_offset_of_isUnixFilePath_0() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___isUnixFilePath_0)); }
	inline bool get_isUnixFilePath_0() const { return ___isUnixFilePath_0; }
	inline bool* get_address_of_isUnixFilePath_0() { return &___isUnixFilePath_0; }
	inline void set_isUnixFilePath_0(bool value)
	{
		___isUnixFilePath_0 = value;
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___source_1)); }
	inline String_t* get_source_1() const { return ___source_1; }
	inline String_t** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(String_t* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_scheme_2() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___scheme_2)); }
	inline String_t* get_scheme_2() const { return ___scheme_2; }
	inline String_t** get_address_of_scheme_2() { return &___scheme_2; }
	inline void set_scheme_2(String_t* value)
	{
		___scheme_2 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_2), value);
	}

	inline static int32_t get_offset_of_host_3() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___host_3)); }
	inline String_t* get_host_3() const { return ___host_3; }
	inline String_t** get_address_of_host_3() { return &___host_3; }
	inline void set_host_3(String_t* value)
	{
		___host_3 = value;
		Il2CppCodeGenWriteBarrier((&___host_3), value);
	}

	inline static int32_t get_offset_of_port_4() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___port_4)); }
	inline int32_t get_port_4() const { return ___port_4; }
	inline int32_t* get_address_of_port_4() { return &___port_4; }
	inline void set_port_4(int32_t value)
	{
		___port_4 = value;
	}

	inline static int32_t get_offset_of_path_5() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___path_5)); }
	inline String_t* get_path_5() const { return ___path_5; }
	inline String_t** get_address_of_path_5() { return &___path_5; }
	inline void set_path_5(String_t* value)
	{
		___path_5 = value;
		Il2CppCodeGenWriteBarrier((&___path_5), value);
	}

	inline static int32_t get_offset_of_query_6() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___query_6)); }
	inline String_t* get_query_6() const { return ___query_6; }
	inline String_t** get_address_of_query_6() { return &___query_6; }
	inline void set_query_6(String_t* value)
	{
		___query_6 = value;
		Il2CppCodeGenWriteBarrier((&___query_6), value);
	}

	inline static int32_t get_offset_of_fragment_7() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___fragment_7)); }
	inline String_t* get_fragment_7() const { return ___fragment_7; }
	inline String_t** get_address_of_fragment_7() { return &___fragment_7; }
	inline void set_fragment_7(String_t* value)
	{
		___fragment_7 = value;
		Il2CppCodeGenWriteBarrier((&___fragment_7), value);
	}

	inline static int32_t get_offset_of_userinfo_8() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___userinfo_8)); }
	inline String_t* get_userinfo_8() const { return ___userinfo_8; }
	inline String_t** get_address_of_userinfo_8() { return &___userinfo_8; }
	inline void set_userinfo_8(String_t* value)
	{
		___userinfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___userinfo_8), value);
	}

	inline static int32_t get_offset_of_isUnc_9() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___isUnc_9)); }
	inline bool get_isUnc_9() const { return ___isUnc_9; }
	inline bool* get_address_of_isUnc_9() { return &___isUnc_9; }
	inline void set_isUnc_9(bool value)
	{
		___isUnc_9 = value;
	}

	inline static int32_t get_offset_of_isOpaquePart_10() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___isOpaquePart_10)); }
	inline bool get_isOpaquePart_10() const { return ___isOpaquePart_10; }
	inline bool* get_address_of_isOpaquePart_10() { return &___isOpaquePart_10; }
	inline void set_isOpaquePart_10(bool value)
	{
		___isOpaquePart_10 = value;
	}

	inline static int32_t get_offset_of_isAbsoluteUri_11() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___isAbsoluteUri_11)); }
	inline bool get_isAbsoluteUri_11() const { return ___isAbsoluteUri_11; }
	inline bool* get_address_of_isAbsoluteUri_11() { return &___isAbsoluteUri_11; }
	inline void set_isAbsoluteUri_11(bool value)
	{
		___isAbsoluteUri_11 = value;
	}

	inline static int32_t get_offset_of_userEscaped_12() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___userEscaped_12)); }
	inline bool get_userEscaped_12() const { return ___userEscaped_12; }
	inline bool* get_address_of_userEscaped_12() { return &___userEscaped_12; }
	inline void set_userEscaped_12(bool value)
	{
		___userEscaped_12 = value;
	}

	inline static int32_t get_offset_of_cachedAbsoluteUri_13() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___cachedAbsoluteUri_13)); }
	inline String_t* get_cachedAbsoluteUri_13() const { return ___cachedAbsoluteUri_13; }
	inline String_t** get_address_of_cachedAbsoluteUri_13() { return &___cachedAbsoluteUri_13; }
	inline void set_cachedAbsoluteUri_13(String_t* value)
	{
		___cachedAbsoluteUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAbsoluteUri_13), value);
	}

	inline static int32_t get_offset_of_cachedToString_14() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___cachedToString_14)); }
	inline String_t* get_cachedToString_14() const { return ___cachedToString_14; }
	inline String_t** get_address_of_cachedToString_14() { return &___cachedToString_14; }
	inline void set_cachedToString_14(String_t* value)
	{
		___cachedToString_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedToString_14), value);
	}

	inline static int32_t get_offset_of_cachedLocalPath_15() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___cachedLocalPath_15)); }
	inline String_t* get_cachedLocalPath_15() const { return ___cachedLocalPath_15; }
	inline String_t** get_address_of_cachedLocalPath_15() { return &___cachedLocalPath_15; }
	inline void set_cachedLocalPath_15(String_t* value)
	{
		___cachedLocalPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedLocalPath_15), value);
	}

	inline static int32_t get_offset_of_cachedHashCode_16() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___cachedHashCode_16)); }
	inline int32_t get_cachedHashCode_16() const { return ___cachedHashCode_16; }
	inline int32_t* get_address_of_cachedHashCode_16() { return &___cachedHashCode_16; }
	inline void set_cachedHashCode_16(int32_t value)
	{
		___cachedHashCode_16 = value;
	}

	inline static int32_t get_offset_of_parser_30() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E, ___parser_30)); }
	inline UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A * get_parser_30() const { return ___parser_30; }
	inline UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A ** get_address_of_parser_30() { return &___parser_30; }
	inline void set_parser_30(UriParser_tD4D7B8A484DAFD6458BBD9AAE2828EBFA77C6D2A * value)
	{
		___parser_30 = value;
		Il2CppCodeGenWriteBarrier((&___parser_30), value);
	}
};

struct Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields
{
public:
	// System.String System.Uri::hexUpperChars
	String_t* ___hexUpperChars_17;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_18;
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_19;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_20;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_21;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_22;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_23;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_24;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_25;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_26;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_27;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_28;
	// System.Uri_UriScheme[] System.Uri::schemes
	UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6* ___schemes_29;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switchU24map1C
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map1C_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switchU24map1D
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map1D_32;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switchU24map1E
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map1E_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switchU24map1F
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map1F_34;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switchU24map20
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map20_35;

public:
	inline static int32_t get_offset_of_hexUpperChars_17() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___hexUpperChars_17)); }
	inline String_t* get_hexUpperChars_17() const { return ___hexUpperChars_17; }
	inline String_t** get_address_of_hexUpperChars_17() { return &___hexUpperChars_17; }
	inline void set_hexUpperChars_17(String_t* value)
	{
		___hexUpperChars_17 = value;
		Il2CppCodeGenWriteBarrier((&___hexUpperChars_17), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_18() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___SchemeDelimiter_18)); }
	inline String_t* get_SchemeDelimiter_18() const { return ___SchemeDelimiter_18; }
	inline String_t** get_address_of_SchemeDelimiter_18() { return &___SchemeDelimiter_18; }
	inline void set_SchemeDelimiter_18(String_t* value)
	{
		___SchemeDelimiter_18 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_18), value);
	}

	inline static int32_t get_offset_of_UriSchemeFile_19() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeFile_19)); }
	inline String_t* get_UriSchemeFile_19() const { return ___UriSchemeFile_19; }
	inline String_t** get_address_of_UriSchemeFile_19() { return &___UriSchemeFile_19; }
	inline void set_UriSchemeFile_19(String_t* value)
	{
		___UriSchemeFile_19 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_19), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_20() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeFtp_20)); }
	inline String_t* get_UriSchemeFtp_20() const { return ___UriSchemeFtp_20; }
	inline String_t** get_address_of_UriSchemeFtp_20() { return &___UriSchemeFtp_20; }
	inline void set_UriSchemeFtp_20(String_t* value)
	{
		___UriSchemeFtp_20 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_20), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_21() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeGopher_21)); }
	inline String_t* get_UriSchemeGopher_21() const { return ___UriSchemeGopher_21; }
	inline String_t** get_address_of_UriSchemeGopher_21() { return &___UriSchemeGopher_21; }
	inline void set_UriSchemeGopher_21(String_t* value)
	{
		___UriSchemeGopher_21 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_21), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_22() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeHttp_22)); }
	inline String_t* get_UriSchemeHttp_22() const { return ___UriSchemeHttp_22; }
	inline String_t** get_address_of_UriSchemeHttp_22() { return &___UriSchemeHttp_22; }
	inline void set_UriSchemeHttp_22(String_t* value)
	{
		___UriSchemeHttp_22 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_22), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_23() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeHttps_23)); }
	inline String_t* get_UriSchemeHttps_23() const { return ___UriSchemeHttps_23; }
	inline String_t** get_address_of_UriSchemeHttps_23() { return &___UriSchemeHttps_23; }
	inline void set_UriSchemeHttps_23(String_t* value)
	{
		___UriSchemeHttps_23 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_23), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_24() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeMailto_24)); }
	inline String_t* get_UriSchemeMailto_24() const { return ___UriSchemeMailto_24; }
	inline String_t** get_address_of_UriSchemeMailto_24() { return &___UriSchemeMailto_24; }
	inline void set_UriSchemeMailto_24(String_t* value)
	{
		___UriSchemeMailto_24 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_24), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_25() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeNews_25)); }
	inline String_t* get_UriSchemeNews_25() const { return ___UriSchemeNews_25; }
	inline String_t** get_address_of_UriSchemeNews_25() { return &___UriSchemeNews_25; }
	inline void set_UriSchemeNews_25(String_t* value)
	{
		___UriSchemeNews_25 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_25), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_26() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeNntp_26)); }
	inline String_t* get_UriSchemeNntp_26() const { return ___UriSchemeNntp_26; }
	inline String_t** get_address_of_UriSchemeNntp_26() { return &___UriSchemeNntp_26; }
	inline void set_UriSchemeNntp_26(String_t* value)
	{
		___UriSchemeNntp_26 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_26), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_27() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeNetPipe_27)); }
	inline String_t* get_UriSchemeNetPipe_27() const { return ___UriSchemeNetPipe_27; }
	inline String_t** get_address_of_UriSchemeNetPipe_27() { return &___UriSchemeNetPipe_27; }
	inline void set_UriSchemeNetPipe_27(String_t* value)
	{
		___UriSchemeNetPipe_27 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_27), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_28() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___UriSchemeNetTcp_28)); }
	inline String_t* get_UriSchemeNetTcp_28() const { return ___UriSchemeNetTcp_28; }
	inline String_t** get_address_of_UriSchemeNetTcp_28() { return &___UriSchemeNetTcp_28; }
	inline void set_UriSchemeNetTcp_28(String_t* value)
	{
		___UriSchemeNetTcp_28 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_28), value);
	}

	inline static int32_t get_offset_of_schemes_29() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___schemes_29)); }
	inline UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6* get_schemes_29() const { return ___schemes_29; }
	inline UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6** get_address_of_schemes_29() { return &___schemes_29; }
	inline void set_schemes_29(UriSchemeU5BU5D_tA21320F0E7A6C8B70FD19E286075856BFB5DB2E6* value)
	{
		___schemes_29 = value;
		Il2CppCodeGenWriteBarrier((&___schemes_29), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1C_31() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map1C_31)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map1C_31() const { return ___U3CU3Ef__switchU24map1C_31; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map1C_31() { return &___U3CU3Ef__switchU24map1C_31; }
	inline void set_U3CU3Ef__switchU24map1C_31(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map1C_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1C_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1D_32() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map1D_32)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map1D_32() const { return ___U3CU3Ef__switchU24map1D_32; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map1D_32() { return &___U3CU3Ef__switchU24map1D_32; }
	inline void set_U3CU3Ef__switchU24map1D_32(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map1D_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1D_32), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1E_33() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map1E_33)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map1E_33() const { return ___U3CU3Ef__switchU24map1E_33; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map1E_33() { return &___U3CU3Ef__switchU24map1E_33; }
	inline void set_U3CU3Ef__switchU24map1E_33(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map1E_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1E_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1F_34() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map1F_34)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map1F_34() const { return ___U3CU3Ef__switchU24map1F_34; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map1F_34() { return &___U3CU3Ef__switchU24map1F_34; }
	inline void set_U3CU3Ef__switchU24map1F_34(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map1F_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1F_34), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map20_35() { return static_cast<int32_t>(offsetof(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_StaticFields, ___U3CU3Ef__switchU24map20_35)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map20_35() const { return ___U3CU3Ef__switchU24map20_35; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map20_35() { return &___U3CU3Ef__switchU24map20_35; }
	inline void set_U3CU3Ef__switchU24map20_35(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map20_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map20_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_TF93A2559917F312C27D9DEAC7CAE4400D3A40F1E_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef XMLSCHEMACOLLECTION_T532C46B1FC9D97F471412FAC2FFCD7F7A93E3513_H
#define XMLSCHEMACOLLECTION_T532C46B1FC9D97F471412FAC2FFCD7F7A93E3513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCollection
struct  XmlSchemaCollection_t532C46B1FC9D97F471412FAC2FFCD7F7A93E3513  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaSet System.Xml.Schema.XmlSchemaCollection::schemaSet
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * ___schemaSet_0;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XmlSchemaCollection::ValidationEventHandler
	ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * ___ValidationEventHandler_1;

public:
	inline static int32_t get_offset_of_schemaSet_0() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_t532C46B1FC9D97F471412FAC2FFCD7F7A93E3513, ___schemaSet_0)); }
	inline XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * get_schemaSet_0() const { return ___schemaSet_0; }
	inline XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D ** get_address_of_schemaSet_0() { return &___schemaSet_0; }
	inline void set_schemaSet_0(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * value)
	{
		___schemaSet_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaSet_0), value);
	}

	inline static int32_t get_offset_of_ValidationEventHandler_1() { return static_cast<int32_t>(offsetof(XmlSchemaCollection_t532C46B1FC9D97F471412FAC2FFCD7F7A93E3513, ___ValidationEventHandler_1)); }
	inline ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * get_ValidationEventHandler_1() const { return ___ValidationEventHandler_1; }
	inline ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B ** get_address_of_ValidationEventHandler_1() { return &___ValidationEventHandler_1; }
	inline void set_ValidationEventHandler_1(ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * value)
	{
		___ValidationEventHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___ValidationEventHandler_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOLLECTION_T532C46B1FC9D97F471412FAC2FFCD7F7A93E3513_H
#ifndef XMLSCHEMACOLLECTIONENUMERATOR_T7D03BFEE26AB5317F66B13E8FA91A8882FBC70E8_H
#define XMLSCHEMACOLLECTIONENUMERATOR_T7D03BFEE26AB5317F66B13E8FA91A8882FBC70E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCollectionEnumerator
struct  XmlSchemaCollectionEnumerator_t7D03BFEE26AB5317F66B13E8FA91A8882FBC70E8  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Xml.Schema.XmlSchemaCollectionEnumerator::xenum
	RuntimeObject* ___xenum_0;

public:
	inline static int32_t get_offset_of_xenum_0() { return static_cast<int32_t>(offsetof(XmlSchemaCollectionEnumerator_t7D03BFEE26AB5317F66B13E8FA91A8882FBC70E8, ___xenum_0)); }
	inline RuntimeObject* get_xenum_0() const { return ___xenum_0; }
	inline RuntimeObject** get_address_of_xenum_0() { return &___xenum_0; }
	inline void set_xenum_0(RuntimeObject* value)
	{
		___xenum_0 = value;
		Il2CppCodeGenWriteBarrier((&___xenum_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOLLECTIONENUMERATOR_T7D03BFEE26AB5317F66B13E8FA91A8882FBC70E8_H
#ifndef XMLSCHEMACOMPILATIONSETTINGS_T84466353D7B09B95C9D10D856332058F60FD31CD_H
#define XMLSCHEMACOMPILATIONSETTINGS_T84466353D7B09B95C9D10D856332058F60FD31CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaCompilationSettings
struct  XmlSchemaCompilationSettings_t84466353D7B09B95C9D10D856332058F60FD31CD  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaCompilationSettings::enable_upa_check
	bool ___enable_upa_check_0;

public:
	inline static int32_t get_offset_of_enable_upa_check_0() { return static_cast<int32_t>(offsetof(XmlSchemaCompilationSettings_t84466353D7B09B95C9D10D856332058F60FD31CD, ___enable_upa_check_0)); }
	inline bool get_enable_upa_check_0() const { return ___enable_upa_check_0; }
	inline bool* get_address_of_enable_upa_check_0() { return &___enable_upa_check_0; }
	inline void set_enable_upa_check_0(bool value)
	{
		___enable_upa_check_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPILATIONSETTINGS_T84466353D7B09B95C9D10D856332058F60FD31CD_H
#ifndef XMLPARSERINPUT_T61A048BD6169B3A13F2876710997B12CF93C5F82_H
#define XMLPARSERINPUT_T61A048BD6169B3A13F2876710997B12CF93C5F82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserInput
struct  XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82  : public RuntimeObject
{
public:
	// System.Collections.Stack System.Xml.XmlParserInput::sourceStack
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___sourceStack_0;
	// System.Xml.XmlParserInput_XmlParserInputSource System.Xml.XmlParserInput::source
	XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC * ___source_1;
	// System.Boolean System.Xml.XmlParserInput::has_peek
	bool ___has_peek_2;
	// System.Int32 System.Xml.XmlParserInput::peek_char
	int32_t ___peek_char_3;
	// System.Boolean System.Xml.XmlParserInput::allowTextDecl
	bool ___allowTextDecl_4;

public:
	inline static int32_t get_offset_of_sourceStack_0() { return static_cast<int32_t>(offsetof(XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82, ___sourceStack_0)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_sourceStack_0() const { return ___sourceStack_0; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_sourceStack_0() { return &___sourceStack_0; }
	inline void set_sourceStack_0(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___sourceStack_0 = value;
		Il2CppCodeGenWriteBarrier((&___sourceStack_0), value);
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82, ___source_1)); }
	inline XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC * get_source_1() const { return ___source_1; }
	inline XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC ** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC * value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_has_peek_2() { return static_cast<int32_t>(offsetof(XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82, ___has_peek_2)); }
	inline bool get_has_peek_2() const { return ___has_peek_2; }
	inline bool* get_address_of_has_peek_2() { return &___has_peek_2; }
	inline void set_has_peek_2(bool value)
	{
		___has_peek_2 = value;
	}

	inline static int32_t get_offset_of_peek_char_3() { return static_cast<int32_t>(offsetof(XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82, ___peek_char_3)); }
	inline int32_t get_peek_char_3() const { return ___peek_char_3; }
	inline int32_t* get_address_of_peek_char_3() { return &___peek_char_3; }
	inline void set_peek_char_3(int32_t value)
	{
		___peek_char_3 = value;
	}

	inline static int32_t get_offset_of_allowTextDecl_4() { return static_cast<int32_t>(offsetof(XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82, ___allowTextDecl_4)); }
	inline bool get_allowTextDecl_4() const { return ___allowTextDecl_4; }
	inline bool* get_address_of_allowTextDecl_4() { return &___allowTextDecl_4; }
	inline void set_allowTextDecl_4(bool value)
	{
		___allowTextDecl_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERINPUT_T61A048BD6169B3A13F2876710997B12CF93C5F82_H
#ifndef XMLPARSERINPUTSOURCE_T91A9158D94809823091460776ED5C8688D6D54CC_H
#define XMLPARSERINPUTSOURCE_T91A9158D94809823091460776ED5C8688D6D54CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserInput_XmlParserInputSource
struct  XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlParserInput_XmlParserInputSource::BaseURI
	String_t* ___BaseURI_0;
	// System.IO.TextReader System.Xml.XmlParserInput_XmlParserInputSource::reader
	TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520 * ___reader_1;
	// System.Int32 System.Xml.XmlParserInput_XmlParserInputSource::state
	int32_t ___state_2;
	// System.Boolean System.Xml.XmlParserInput_XmlParserInputSource::isPE
	bool ___isPE_3;
	// System.Int32 System.Xml.XmlParserInput_XmlParserInputSource::line
	int32_t ___line_4;
	// System.Int32 System.Xml.XmlParserInput_XmlParserInputSource::column
	int32_t ___column_5;

public:
	inline static int32_t get_offset_of_BaseURI_0() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC, ___BaseURI_0)); }
	inline String_t* get_BaseURI_0() const { return ___BaseURI_0; }
	inline String_t** get_address_of_BaseURI_0() { return &___BaseURI_0; }
	inline void set_BaseURI_0(String_t* value)
	{
		___BaseURI_0 = value;
		Il2CppCodeGenWriteBarrier((&___BaseURI_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC, ___reader_1)); }
	inline TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520 * get_reader_1() const { return ___reader_1; }
	inline TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520 ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520 * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_isPE_3() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC, ___isPE_3)); }
	inline bool get_isPE_3() const { return ___isPE_3; }
	inline bool* get_address_of_isPE_3() { return &___isPE_3; }
	inline void set_isPE_3(bool value)
	{
		___isPE_3 = value;
	}

	inline static int32_t get_offset_of_line_4() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC, ___line_4)); }
	inline int32_t get_line_4() const { return ___line_4; }
	inline int32_t* get_address_of_line_4() { return &___line_4; }
	inline void set_line_4(int32_t value)
	{
		___line_4 = value;
	}

	inline static int32_t get_offset_of_column_5() { return static_cast<int32_t>(offsetof(XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC, ___column_5)); }
	inline int32_t get_column_5() const { return ___column_5; }
	inline int32_t* get_address_of_column_5() { return &___column_5; }
	inline void set_column_5(int32_t value)
	{
		___column_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERINPUTSOURCE_T91A9158D94809823091460776ED5C8688D6D54CC_H
#ifndef STRINGUTIL_T6B1C7DC3B724BD6FC51242BBACBCC786751CA110_H
#define STRINGUTIL_T6B1C7DC3B724BD6FC51242BBACBCC786751CA110_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter_StringUtil
struct  StringUtil_t6B1C7DC3B724BD6FC51242BBACBCC786751CA110  : public RuntimeObject
{
public:

public:
};

struct StringUtil_t6B1C7DC3B724BD6FC51242BBACBCC786751CA110_StaticFields
{
public:
	// System.Globalization.CultureInfo System.Xml.XmlTextWriter_StringUtil::cul
	CultureInfo_t2089C879B1CBB363EF1CF59D058AD2D2B0C9D28D * ___cul_0;
	// System.Globalization.CompareInfo System.Xml.XmlTextWriter_StringUtil::cmp
	CompareInfo_t704FFB923C9EF69DFAE63C1950E5B466A94C0F4B * ___cmp_1;

public:
	inline static int32_t get_offset_of_cul_0() { return static_cast<int32_t>(offsetof(StringUtil_t6B1C7DC3B724BD6FC51242BBACBCC786751CA110_StaticFields, ___cul_0)); }
	inline CultureInfo_t2089C879B1CBB363EF1CF59D058AD2D2B0C9D28D * get_cul_0() const { return ___cul_0; }
	inline CultureInfo_t2089C879B1CBB363EF1CF59D058AD2D2B0C9D28D ** get_address_of_cul_0() { return &___cul_0; }
	inline void set_cul_0(CultureInfo_t2089C879B1CBB363EF1CF59D058AD2D2B0C9D28D * value)
	{
		___cul_0 = value;
		Il2CppCodeGenWriteBarrier((&___cul_0), value);
	}

	inline static int32_t get_offset_of_cmp_1() { return static_cast<int32_t>(offsetof(StringUtil_t6B1C7DC3B724BD6FC51242BBACBCC786751CA110_StaticFields, ___cmp_1)); }
	inline CompareInfo_t704FFB923C9EF69DFAE63C1950E5B466A94C0F4B * get_cmp_1() const { return ___cmp_1; }
	inline CompareInfo_t704FFB923C9EF69DFAE63C1950E5B466A94C0F4B ** get_address_of_cmp_1() { return &___cmp_1; }
	inline void set_cmp_1(CompareInfo_t704FFB923C9EF69DFAE63C1950E5B466A94C0F4B * value)
	{
		___cmp_1 = value;
		Il2CppCodeGenWriteBarrier((&___cmp_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTIL_T6B1C7DC3B724BD6FC51242BBACBCC786751CA110_H
#ifndef XMLWRITER_TF67065A3B83E8B7E2B24D172D8223F4328F437B6_H
#define XMLWRITER_TF67065A3B83E8B7E2B24D172D8223F4328F437B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWriter
struct  XmlWriter_tF67065A3B83E8B7E2B24D172D8223F4328F437B6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITER_TF67065A3B83E8B7E2B24D172D8223F4328F437B6_H
#ifndef XMLSCHEMAURI_T93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0_H
#define XMLSCHEMAURI_T93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XmlSchemaUri
struct  XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0  : public Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E
{
public:
	// System.String Mono.Xml.Schema.XmlSchemaUri::value
	String_t* ___value_36;

public:
	inline static int32_t get_offset_of_value_36() { return static_cast<int32_t>(offsetof(XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0, ___value_36)); }
	inline String_t* get_value_36() const { return ___value_36; }
	inline String_t** get_address_of_value_36() { return &___value_36; }
	inline void set_value_36(String_t* value)
	{
		___value_36 = value;
		Il2CppCodeGenWriteBarrier((&___value_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAURI_T93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0_H
#ifndef DECIMAL_T46BBDD161320E361BC57E00CD6C1F87256CA27F3_H
#define DECIMAL_T46BBDD161320E361BC57E00CD6C1F87256CA27F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 
{
public:
	// System.UInt32 System.Decimal::flags
	uint32_t ___flags_5;
	// System.UInt32 System.Decimal::hi
	uint32_t ___hi_6;
	// System.UInt32 System.Decimal::lo
	uint32_t ___lo_7;
	// System.UInt32 System.Decimal::mid
	uint32_t ___mid_8;

public:
	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3, ___flags_5)); }
	inline uint32_t get_flags_5() const { return ___flags_5; }
	inline uint32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(uint32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_hi_6() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3, ___hi_6)); }
	inline uint32_t get_hi_6() const { return ___hi_6; }
	inline uint32_t* get_address_of_hi_6() { return &___hi_6; }
	inline void set_hi_6(uint32_t value)
	{
		___hi_6 = value;
	}

	inline static int32_t get_offset_of_lo_7() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3, ___lo_7)); }
	inline uint32_t get_lo_7() const { return ___lo_7; }
	inline uint32_t* get_address_of_lo_7() { return &___lo_7; }
	inline void set_lo_7(uint32_t value)
	{
		___lo_7 = value;
	}

	inline static int32_t get_offset_of_mid_8() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3, ___mid_8)); }
	inline uint32_t get_mid_8() const { return ___mid_8; }
	inline uint32_t* get_address_of_mid_8() { return &___mid_8; }
	inline void set_mid_8(uint32_t value)
	{
		___mid_8 = value;
	}
};

struct Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3_StaticFields
{
public:
	// System.Decimal System.Decimal::MinValue
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___MinValue_0;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___MaxValue_1;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___MinusOne_2;
	// System.Decimal System.Decimal::One
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___One_3;
	// System.Decimal System.Decimal::MaxValueDiv10
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___MaxValueDiv10_4;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3_StaticFields, ___MinValue_0)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_MinValue_0() const { return ___MinValue_0; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3_StaticFields, ___MaxValue_1)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_MaxValue_1() const { return ___MaxValue_1; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinusOne_2() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3_StaticFields, ___MinusOne_2)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_MinusOne_2() const { return ___MinusOne_2; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_MinusOne_2() { return &___MinusOne_2; }
	inline void set_MinusOne_2(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___MinusOne_2 = value;
	}

	inline static int32_t get_offset_of_One_3() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3_StaticFields, ___One_3)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_One_3() const { return ___One_3; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_One_3() { return &___One_3; }
	inline void set_One_3(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___One_3 = value;
	}

	inline static int32_t get_offset_of_MaxValueDiv10_4() { return static_cast<int32_t>(offsetof(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3_StaticFields, ___MaxValueDiv10_4)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_MaxValueDiv10_4() const { return ___MaxValueDiv10_4; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_MaxValueDiv10_4() { return &___MaxValueDiv10_4; }
	inline void set_MaxValueDiv10_4(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___MaxValueDiv10_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T46BBDD161320E361BC57E00CD6C1F87256CA27F3_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef STREAM_TCFD27E43C18381861212C0288CACF467FB602009_H
#define STREAM_TCFD27E43C18381861212C0288CACF467FB602009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tCFD27E43C18381861212C0288CACF467FB602009  : public MarshalByRefObject_t05F62A8AC86E36BAE3063CA28097945DE9E179C4
{
public:

public:
};

struct Stream_tCFD27E43C18381861212C0288CACF467FB602009_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tCFD27E43C18381861212C0288CACF467FB602009 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tCFD27E43C18381861212C0288CACF467FB602009_StaticFields, ___Null_1)); }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 * get_Null_1() const { return ___Null_1; }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tCFD27E43C18381861212C0288CACF467FB602009 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TCFD27E43C18381861212C0288CACF467FB602009_H
#ifndef TEXTREADER_TC6C39132CD1389E729FA893F06C34EE351D24520_H
#define TEXTREADER_TC6C39132CD1389E729FA893F06C34EE351D24520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextReader
struct  TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520  : public MarshalByRefObject_t05F62A8AC86E36BAE3063CA28097945DE9E179C4
{
public:

public:
};

struct TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520_StaticFields
{
public:
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520_StaticFields, ___Null_1)); }
	inline TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520 * get_Null_1() const { return ___Null_1; }
	inline TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTREADER_TC6C39132CD1389E729FA893F06C34EE351D24520_H
#ifndef SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#define SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifndef QNAMEVALUETYPE_T4CB12E2F7D0A45A5F96D22C26D4CA08ABBABC3D9_H
#define QNAMEVALUETYPE_T4CB12E2F7D0A45A5F96D22C26D4CA08ABBABC3D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.QNameValueType
struct  QNameValueType_t4CB12E2F7D0A45A5F96D22C26D4CA08ABBABC3D9 
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.QNameValueType::value
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(QNameValueType_t4CB12E2F7D0A45A5F96D22C26D4CA08ABBABC3D9, ___value_0)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_value_0() const { return ___value_0; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.QNameValueType
struct QNameValueType_t4CB12E2F7D0A45A5F96D22C26D4CA08ABBABC3D9_marshaled_pinvoke
{
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___value_0;
};
// Native definition for COM marshalling of System.Xml.Schema.QNameValueType
struct QNameValueType_t4CB12E2F7D0A45A5F96D22C26D4CA08ABBABC3D9_marshaled_com
{
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___value_0;
};
#endif // QNAMEVALUETYPE_T4CB12E2F7D0A45A5F96D22C26D4CA08ABBABC3D9_H
#ifndef STRINGARRAYVALUETYPE_T2A9744C9A0B6AA9295574EF041B231EDD04510CD_H
#define STRINGARRAYVALUETYPE_T2A9744C9A0B6AA9295574EF041B231EDD04510CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.StringArrayValueType
struct  StringArrayValueType_t2A9744C9A0B6AA9295574EF041B231EDD04510CD 
{
public:
	// System.String[] System.Xml.Schema.StringArrayValueType::value
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(StringArrayValueType_t2A9744C9A0B6AA9295574EF041B231EDD04510CD, ___value_0)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_value_0() const { return ___value_0; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.StringArrayValueType
struct StringArrayValueType_t2A9744C9A0B6AA9295574EF041B231EDD04510CD_marshaled_pinvoke
{
	char** ___value_0;
};
// Native definition for COM marshalling of System.Xml.Schema.StringArrayValueType
struct StringArrayValueType_t2A9744C9A0B6AA9295574EF041B231EDD04510CD_marshaled_com
{
	Il2CppChar** ___value_0;
};
#endif // STRINGARRAYVALUETYPE_T2A9744C9A0B6AA9295574EF041B231EDD04510CD_H
#ifndef STRINGVALUETYPE_T20023BC77B65D51D249852CAC6D9DB267D469A01_H
#define STRINGVALUETYPE_T20023BC77B65D51D249852CAC6D9DB267D469A01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.StringValueType
struct  StringValueType_t20023BC77B65D51D249852CAC6D9DB267D469A01 
{
public:
	// System.String System.Xml.Schema.StringValueType::value
	String_t* ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(StringValueType_t20023BC77B65D51D249852CAC6D9DB267D469A01, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.StringValueType
struct StringValueType_t20023BC77B65D51D249852CAC6D9DB267D469A01_marshaled_pinvoke
{
	char* ___value_0;
};
// Native definition for COM marshalling of System.Xml.Schema.StringValueType
struct StringValueType_t20023BC77B65D51D249852CAC6D9DB267D469A01_marshaled_com
{
	Il2CppChar* ___value_0;
};
#endif // STRINGVALUETYPE_T20023BC77B65D51D249852CAC6D9DB267D469A01_H
#ifndef URIVALUETYPE_TEEC6B4D97CBB2DE8DDAFD52DD400AA2FAF116A70_H
#define URIVALUETYPE_TEEC6B4D97CBB2DE8DDAFD52DD400AA2FAF116A70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.UriValueType
struct  UriValueType_tEEC6B4D97CBB2DE8DDAFD52DD400AA2FAF116A70 
{
public:
	// Mono.Xml.Schema.XmlSchemaUri System.Xml.Schema.UriValueType::value
	XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0 * ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(UriValueType_tEEC6B4D97CBB2DE8DDAFD52DD400AA2FAF116A70, ___value_0)); }
	inline XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0 * get_value_0() const { return ___value_0; }
	inline XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0 ** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0 * value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.UriValueType
struct UriValueType_tEEC6B4D97CBB2DE8DDAFD52DD400AA2FAF116A70_marshaled_pinvoke
{
	XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0 * ___value_0;
};
// Native definition for COM marshalling of System.Xml.Schema.UriValueType
struct UriValueType_tEEC6B4D97CBB2DE8DDAFD52DD400AA2FAF116A70_marshaled_com
{
	XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0 * ___value_0;
};
#endif // URIVALUETYPE_TEEC6B4D97CBB2DE8DDAFD52DD400AA2FAF116A70_H
#ifndef XSDORDERING_T27E88D8D2082F10D686D9004720EBEA75EAA05E8_H
#define XSDORDERING_T27E88D8D2082F10D686D9004720EBEA75EAA05E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdOrdering
struct  XsdOrdering_t27E88D8D2082F10D686D9004720EBEA75EAA05E8 
{
public:
	// System.Int32 Mono.Xml.Schema.XsdOrdering::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XsdOrdering_t27E88D8D2082F10D686D9004720EBEA75EAA05E8, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDORDERING_T27E88D8D2082F10D686D9004720EBEA75EAA05E8_H
#ifndef XSDWHITESPACEFACET_TCADC752FD2332AC9C1304C86653374FF61C0B91A_H
#define XSDWHITESPACEFACET_TCADC752FD2332AC9C1304C86653374FF61C0B91A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdWhitespaceFacet
struct  XsdWhitespaceFacet_tCADC752FD2332AC9C1304C86653374FF61C0B91A 
{
public:
	// System.Int32 Mono.Xml.Schema.XsdWhitespaceFacet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XsdWhitespaceFacet_tCADC752FD2332AC9C1304C86653374FF61C0B91A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDWHITESPACEFACET_TCADC752FD2332AC9C1304C86653374FF61C0B91A_H
#ifndef NAMESPACEHANDLING_T2F02A279F33A1348B8E4AB835A10E0C06237939A_H
#define NAMESPACEHANDLING_T2F02A279F33A1348B8E4AB835A10E0C06237939A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NamespaceHandling
struct  NamespaceHandling_t2F02A279F33A1348B8E4AB835A10E0C06237939A 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamespaceHandling_t2F02A279F33A1348B8E4AB835A10E0C06237939A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEHANDLING_T2F02A279F33A1348B8E4AB835A10E0C06237939A_H
#ifndef NEWLINEHANDLING_TC5B27F2FDF6CAA10BBBB61E1F03BAF891FF30FC9_H
#define NEWLINEHANDLING_TC5B27F2FDF6CAA10BBBB61E1F03BAF891FF30FC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_tC5B27F2FDF6CAA10BBBB61E1F03BAF891FF30FC9 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NewLineHandling_tC5B27F2FDF6CAA10BBBB61E1F03BAF891FF30FC9, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_TC5B27F2FDF6CAA10BBBB61E1F03BAF891FF30FC9_H
#ifndef NONBLOCKINGSTREAMREADER_TCD1035F526FC01E59384659F6AF39B6DEA3365D8_H
#define NONBLOCKINGSTREAMREADER_TCD1035F526FC01E59384659F6AF39B6DEA3365D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NonBlockingStreamReader
struct  NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8  : public TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520
{
public:
	// System.Byte[] System.Xml.NonBlockingStreamReader::input_buffer
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___input_buffer_2;
	// System.Char[] System.Xml.NonBlockingStreamReader::decoded_buffer
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___decoded_buffer_3;
	// System.Int32 System.Xml.NonBlockingStreamReader::decoded_count
	int32_t ___decoded_count_4;
	// System.Int32 System.Xml.NonBlockingStreamReader::pos
	int32_t ___pos_5;
	// System.Int32 System.Xml.NonBlockingStreamReader::buffer_size
	int32_t ___buffer_size_6;
	// System.Text.Encoding System.Xml.NonBlockingStreamReader::encoding
	Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * ___encoding_7;
	// System.Text.Decoder System.Xml.NonBlockingStreamReader::decoder
	Decoder_t95F6C32901671048111BEE6EAD9273760D7881C6 * ___decoder_8;
	// System.IO.Stream System.Xml.NonBlockingStreamReader::base_stream
	Stream_tCFD27E43C18381861212C0288CACF467FB602009 * ___base_stream_9;
	// System.Boolean System.Xml.NonBlockingStreamReader::mayBlock
	bool ___mayBlock_10;
	// System.Text.StringBuilder System.Xml.NonBlockingStreamReader::line_builder
	StringBuilder_t * ___line_builder_11;
	// System.Boolean System.Xml.NonBlockingStreamReader::foundCR
	bool ___foundCR_12;

public:
	inline static int32_t get_offset_of_input_buffer_2() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8, ___input_buffer_2)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_input_buffer_2() const { return ___input_buffer_2; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_input_buffer_2() { return &___input_buffer_2; }
	inline void set_input_buffer_2(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___input_buffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___input_buffer_2), value);
	}

	inline static int32_t get_offset_of_decoded_buffer_3() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8, ___decoded_buffer_3)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_decoded_buffer_3() const { return ___decoded_buffer_3; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_decoded_buffer_3() { return &___decoded_buffer_3; }
	inline void set_decoded_buffer_3(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___decoded_buffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___decoded_buffer_3), value);
	}

	inline static int32_t get_offset_of_decoded_count_4() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8, ___decoded_count_4)); }
	inline int32_t get_decoded_count_4() const { return ___decoded_count_4; }
	inline int32_t* get_address_of_decoded_count_4() { return &___decoded_count_4; }
	inline void set_decoded_count_4(int32_t value)
	{
		___decoded_count_4 = value;
	}

	inline static int32_t get_offset_of_pos_5() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8, ___pos_5)); }
	inline int32_t get_pos_5() const { return ___pos_5; }
	inline int32_t* get_address_of_pos_5() { return &___pos_5; }
	inline void set_pos_5(int32_t value)
	{
		___pos_5 = value;
	}

	inline static int32_t get_offset_of_buffer_size_6() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8, ___buffer_size_6)); }
	inline int32_t get_buffer_size_6() const { return ___buffer_size_6; }
	inline int32_t* get_address_of_buffer_size_6() { return &___buffer_size_6; }
	inline void set_buffer_size_6(int32_t value)
	{
		___buffer_size_6 = value;
	}

	inline static int32_t get_offset_of_encoding_7() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8, ___encoding_7)); }
	inline Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * get_encoding_7() const { return ___encoding_7; }
	inline Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 ** get_address_of_encoding_7() { return &___encoding_7; }
	inline void set_encoding_7(Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * value)
	{
		___encoding_7 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_7), value);
	}

	inline static int32_t get_offset_of_decoder_8() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8, ___decoder_8)); }
	inline Decoder_t95F6C32901671048111BEE6EAD9273760D7881C6 * get_decoder_8() const { return ___decoder_8; }
	inline Decoder_t95F6C32901671048111BEE6EAD9273760D7881C6 ** get_address_of_decoder_8() { return &___decoder_8; }
	inline void set_decoder_8(Decoder_t95F6C32901671048111BEE6EAD9273760D7881C6 * value)
	{
		___decoder_8 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_8), value);
	}

	inline static int32_t get_offset_of_base_stream_9() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8, ___base_stream_9)); }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 * get_base_stream_9() const { return ___base_stream_9; }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 ** get_address_of_base_stream_9() { return &___base_stream_9; }
	inline void set_base_stream_9(Stream_tCFD27E43C18381861212C0288CACF467FB602009 * value)
	{
		___base_stream_9 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_9), value);
	}

	inline static int32_t get_offset_of_mayBlock_10() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8, ___mayBlock_10)); }
	inline bool get_mayBlock_10() const { return ___mayBlock_10; }
	inline bool* get_address_of_mayBlock_10() { return &___mayBlock_10; }
	inline void set_mayBlock_10(bool value)
	{
		___mayBlock_10 = value;
	}

	inline static int32_t get_offset_of_line_builder_11() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8, ___line_builder_11)); }
	inline StringBuilder_t * get_line_builder_11() const { return ___line_builder_11; }
	inline StringBuilder_t ** get_address_of_line_builder_11() { return &___line_builder_11; }
	inline void set_line_builder_11(StringBuilder_t * value)
	{
		___line_builder_11 = value;
		Il2CppCodeGenWriteBarrier((&___line_builder_11), value);
	}

	inline static int32_t get_offset_of_foundCR_12() { return static_cast<int32_t>(offsetof(NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8, ___foundCR_12)); }
	inline bool get_foundCR_12() const { return ___foundCR_12; }
	inline bool* get_address_of_foundCR_12() { return &___foundCR_12; }
	inline void set_foundCR_12(bool value)
	{
		___foundCR_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONBLOCKINGSTREAMREADER_TCD1035F526FC01E59384659F6AF39B6DEA3365D8_H
#ifndef XMLSCHEMACONTENTPROCESSING_T392DB8F98BDBDD400CD2370DE517690F99622330_H
#define XMLSCHEMACONTENTPROCESSING_T392DB8F98BDBDD400CD2370DE517690F99622330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentProcessing
struct  XmlSchemaContentProcessing_t392DB8F98BDBDD400CD2370DE517690F99622330 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentProcessing::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaContentProcessing_t392DB8F98BDBDD400CD2370DE517690F99622330, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTPROCESSING_T392DB8F98BDBDD400CD2370DE517690F99622330_H
#ifndef XMLSCHEMACONTENTTYPE_TBCF71BAA43A0320A521F3B5E607A8EB2F57676A6_H
#define XMLSCHEMACONTENTTYPE_TBCF71BAA43A0320A521F3B5E607A8EB2F57676A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentType
struct  XmlSchemaContentType_tBCF71BAA43A0320A521F3B5E607A8EB2F57676A6 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaContentType_tBCF71BAA43A0320A521F3B5E607A8EB2F57676A6, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTTYPE_TBCF71BAA43A0320A521F3B5E607A8EB2F57676A6_H
#ifndef XMLSCHEMADERIVATIONMETHOD_TCA0F660D1F9BAE9B164EC4393AA19EBA107F8C07_H
#define XMLSCHEMADERIVATIONMETHOD_TCA0F660D1F9BAE9B164EC4393AA19EBA107F8C07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDerivationMethod
struct  XmlSchemaDerivationMethod_tCA0F660D1F9BAE9B164EC4393AA19EBA107F8C07 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaDerivationMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaDerivationMethod_tCA0F660D1F9BAE9B164EC4393AA19EBA107F8C07, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADERIVATIONMETHOD_TCA0F660D1F9BAE9B164EC4393AA19EBA107F8C07_H
#ifndef XMLSCHEMAEXCEPTION_TCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D_H
#define XMLSCHEMAEXCEPTION_TCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaException
struct  XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D  : public SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaException::hasLineInfo
	bool ___hasLineInfo_11;
	// System.Int32 System.Xml.Schema.XmlSchemaException::lineNumber
	int32_t ___lineNumber_12;
	// System.Int32 System.Xml.Schema.XmlSchemaException::linePosition
	int32_t ___linePosition_13;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaException::sourceObj
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * ___sourceObj_14;
	// System.String System.Xml.Schema.XmlSchemaException::sourceUri
	String_t* ___sourceUri_15;

public:
	inline static int32_t get_offset_of_hasLineInfo_11() { return static_cast<int32_t>(offsetof(XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D, ___hasLineInfo_11)); }
	inline bool get_hasLineInfo_11() const { return ___hasLineInfo_11; }
	inline bool* get_address_of_hasLineInfo_11() { return &___hasLineInfo_11; }
	inline void set_hasLineInfo_11(bool value)
	{
		___hasLineInfo_11 = value;
	}

	inline static int32_t get_offset_of_lineNumber_12() { return static_cast<int32_t>(offsetof(XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D, ___lineNumber_12)); }
	inline int32_t get_lineNumber_12() const { return ___lineNumber_12; }
	inline int32_t* get_address_of_lineNumber_12() { return &___lineNumber_12; }
	inline void set_lineNumber_12(int32_t value)
	{
		___lineNumber_12 = value;
	}

	inline static int32_t get_offset_of_linePosition_13() { return static_cast<int32_t>(offsetof(XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D, ___linePosition_13)); }
	inline int32_t get_linePosition_13() const { return ___linePosition_13; }
	inline int32_t* get_address_of_linePosition_13() { return &___linePosition_13; }
	inline void set_linePosition_13(int32_t value)
	{
		___linePosition_13 = value;
	}

	inline static int32_t get_offset_of_sourceObj_14() { return static_cast<int32_t>(offsetof(XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D, ___sourceObj_14)); }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * get_sourceObj_14() const { return ___sourceObj_14; }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 ** get_address_of_sourceObj_14() { return &___sourceObj_14; }
	inline void set_sourceObj_14(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * value)
	{
		___sourceObj_14 = value;
		Il2CppCodeGenWriteBarrier((&___sourceObj_14), value);
	}

	inline static int32_t get_offset_of_sourceUri_15() { return static_cast<int32_t>(offsetof(XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D, ___sourceUri_15)); }
	inline String_t* get_sourceUri_15() const { return ___sourceUri_15; }
	inline String_t** get_address_of_sourceUri_15() { return &___sourceUri_15; }
	inline void set_sourceUri_15(String_t* value)
	{
		___sourceUri_15 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAEXCEPTION_TCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D_H
#ifndef FACET_TC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B_H
#define FACET_TC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet_Facet
struct  Facet_tC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaFacet_Facet::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Facet_tC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACET_TC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B_H
#ifndef XMLSCHEMAFORM_T704AB79E1BD496D7F43D4687FBEC3B134884942E_H
#define XMLSCHEMAFORM_T704AB79E1BD496D7F43D4687FBEC3B134884942E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaForm
struct  XmlSchemaForm_t704AB79E1BD496D7F43D4687FBEC3B134884942E 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaForm::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaForm_t704AB79E1BD496D7F43D4687FBEC3B134884942E, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFORM_T704AB79E1BD496D7F43D4687FBEC3B134884942E_H
#ifndef XMLSCHEMAOBJECT_TDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747_H
#define XMLSCHEMAOBJECT_TDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaObject
struct  XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747  : public RuntimeObject
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaObject::lineNumber
	int32_t ___lineNumber_0;
	// System.Int32 System.Xml.Schema.XmlSchemaObject::linePosition
	int32_t ___linePosition_1;
	// System.String System.Xml.Schema.XmlSchemaObject::sourceUri
	String_t* ___sourceUri_2;
	// System.Xml.Serialization.XmlSerializerNamespaces System.Xml.Schema.XmlSchemaObject::namespaces
	XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3 * ___namespaces_3;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaObject::unhandledAttributeList
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___unhandledAttributeList_4;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isCompiled
	bool ___isCompiled_5;
	// System.Int32 System.Xml.Schema.XmlSchemaObject::errorCount
	int32_t ___errorCount_6;
	// System.Guid System.Xml.Schema.XmlSchemaObject::CompilationId
	Guid_t  ___CompilationId_7;
	// System.Guid System.Xml.Schema.XmlSchemaObject::ValidationId
	Guid_t  ___ValidationId_8;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isRedefineChild
	bool ___isRedefineChild_9;
	// System.Boolean System.Xml.Schema.XmlSchemaObject::isRedefinedComponent
	bool ___isRedefinedComponent_10;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObject::redefinedObject
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * ___redefinedObject_11;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaObject::parent
	XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * ___parent_12;

public:
	inline static int32_t get_offset_of_lineNumber_0() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___lineNumber_0)); }
	inline int32_t get_lineNumber_0() const { return ___lineNumber_0; }
	inline int32_t* get_address_of_lineNumber_0() { return &___lineNumber_0; }
	inline void set_lineNumber_0(int32_t value)
	{
		___lineNumber_0 = value;
	}

	inline static int32_t get_offset_of_linePosition_1() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___linePosition_1)); }
	inline int32_t get_linePosition_1() const { return ___linePosition_1; }
	inline int32_t* get_address_of_linePosition_1() { return &___linePosition_1; }
	inline void set_linePosition_1(int32_t value)
	{
		___linePosition_1 = value;
	}

	inline static int32_t get_offset_of_sourceUri_2() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___sourceUri_2)); }
	inline String_t* get_sourceUri_2() const { return ___sourceUri_2; }
	inline String_t** get_address_of_sourceUri_2() { return &___sourceUri_2; }
	inline void set_sourceUri_2(String_t* value)
	{
		___sourceUri_2 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_2), value);
	}

	inline static int32_t get_offset_of_namespaces_3() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___namespaces_3)); }
	inline XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3 * get_namespaces_3() const { return ___namespaces_3; }
	inline XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3 ** get_address_of_namespaces_3() { return &___namespaces_3; }
	inline void set_namespaces_3(XmlSerializerNamespaces_tC38F759F99861DDF16FBA041554B4BBA2C1B12E3 * value)
	{
		___namespaces_3 = value;
		Il2CppCodeGenWriteBarrier((&___namespaces_3), value);
	}

	inline static int32_t get_offset_of_unhandledAttributeList_4() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___unhandledAttributeList_4)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_unhandledAttributeList_4() const { return ___unhandledAttributeList_4; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_unhandledAttributeList_4() { return &___unhandledAttributeList_4; }
	inline void set_unhandledAttributeList_4(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___unhandledAttributeList_4 = value;
		Il2CppCodeGenWriteBarrier((&___unhandledAttributeList_4), value);
	}

	inline static int32_t get_offset_of_isCompiled_5() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___isCompiled_5)); }
	inline bool get_isCompiled_5() const { return ___isCompiled_5; }
	inline bool* get_address_of_isCompiled_5() { return &___isCompiled_5; }
	inline void set_isCompiled_5(bool value)
	{
		___isCompiled_5 = value;
	}

	inline static int32_t get_offset_of_errorCount_6() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___errorCount_6)); }
	inline int32_t get_errorCount_6() const { return ___errorCount_6; }
	inline int32_t* get_address_of_errorCount_6() { return &___errorCount_6; }
	inline void set_errorCount_6(int32_t value)
	{
		___errorCount_6 = value;
	}

	inline static int32_t get_offset_of_CompilationId_7() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___CompilationId_7)); }
	inline Guid_t  get_CompilationId_7() const { return ___CompilationId_7; }
	inline Guid_t * get_address_of_CompilationId_7() { return &___CompilationId_7; }
	inline void set_CompilationId_7(Guid_t  value)
	{
		___CompilationId_7 = value;
	}

	inline static int32_t get_offset_of_ValidationId_8() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___ValidationId_8)); }
	inline Guid_t  get_ValidationId_8() const { return ___ValidationId_8; }
	inline Guid_t * get_address_of_ValidationId_8() { return &___ValidationId_8; }
	inline void set_ValidationId_8(Guid_t  value)
	{
		___ValidationId_8 = value;
	}

	inline static int32_t get_offset_of_isRedefineChild_9() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___isRedefineChild_9)); }
	inline bool get_isRedefineChild_9() const { return ___isRedefineChild_9; }
	inline bool* get_address_of_isRedefineChild_9() { return &___isRedefineChild_9; }
	inline void set_isRedefineChild_9(bool value)
	{
		___isRedefineChild_9 = value;
	}

	inline static int32_t get_offset_of_isRedefinedComponent_10() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___isRedefinedComponent_10)); }
	inline bool get_isRedefinedComponent_10() const { return ___isRedefinedComponent_10; }
	inline bool* get_address_of_isRedefinedComponent_10() { return &___isRedefinedComponent_10; }
	inline void set_isRedefinedComponent_10(bool value)
	{
		___isRedefinedComponent_10 = value;
	}

	inline static int32_t get_offset_of_redefinedObject_11() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___redefinedObject_11)); }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * get_redefinedObject_11() const { return ___redefinedObject_11; }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 ** get_address_of_redefinedObject_11() { return &___redefinedObject_11; }
	inline void set_redefinedObject_11(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * value)
	{
		___redefinedObject_11 = value;
		Il2CppCodeGenWriteBarrier((&___redefinedObject_11), value);
	}

	inline static int32_t get_offset_of_parent_12() { return static_cast<int32_t>(offsetof(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747, ___parent_12)); }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * get_parent_12() const { return ___parent_12; }
	inline XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 ** get_address_of_parent_12() { return &___parent_12; }
	inline void set_parent_12(XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747 * value)
	{
		___parent_12 = value;
		Il2CppCodeGenWriteBarrier((&___parent_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAOBJECT_TDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747_H
#ifndef XMLSCHEMAUSE_T74E0EE4116E212E01A0E2CBE17B468F5F5A728D8_H
#define XMLSCHEMAUSE_T74E0EE4116E212E01A0E2CBE17B468F5F5A728D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaUse
struct  XmlSchemaUse_t74E0EE4116E212E01A0E2CBE17B468F5F5A728D8 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaUse::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaUse_t74E0EE4116E212E01A0E2CBE17B468F5F5A728D8, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAUSE_T74E0EE4116E212E01A0E2CBE17B468F5F5A728D8_H
#ifndef XMLSEVERITYTYPE_T128BFAEA33288999198A6D82DF83832E1A2DA060_H
#define XMLSEVERITYTYPE_T128BFAEA33288999198A6D82DF83832E1A2DA060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSeverityType
struct  XmlSeverityType_t128BFAEA33288999198A6D82DF83832E1A2DA060 
{
public:
	// System.Int32 System.Xml.Schema.XmlSeverityType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSeverityType_t128BFAEA33288999198A6D82DF83832E1A2DA060, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSEVERITYTYPE_T128BFAEA33288999198A6D82DF83832E1A2DA060_H
#ifndef WRITESTATE_T77FFF7A78C6BEB469AF5904B89980997CDC8735F_H
#define WRITESTATE_T77FFF7A78C6BEB469AF5904B89980997CDC8735F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WriteState
struct  WriteState_t77FFF7A78C6BEB469AF5904B89980997CDC8735F 
{
public:
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WriteState_t77FFF7A78C6BEB469AF5904B89980997CDC8735F, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T77FFF7A78C6BEB469AF5904B89980997CDC8735F_H
#ifndef XMLINPUTSTREAM_TD179743E3D4482E2297EEC3A71D082F2F2B2F629_H
#define XMLINPUTSTREAM_TD179743E3D4482E2297EEC3A71D082F2F2B2F629_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlInputStream
struct  XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629  : public Stream_tCFD27E43C18381861212C0288CACF467FB602009
{
public:
	// System.Text.Encoding System.Xml.XmlInputStream::enc
	Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * ___enc_3;
	// System.IO.Stream System.Xml.XmlInputStream::stream
	Stream_tCFD27E43C18381861212C0288CACF467FB602009 * ___stream_4;
	// System.Byte[] System.Xml.XmlInputStream::buffer
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___buffer_5;
	// System.Int32 System.Xml.XmlInputStream::bufLength
	int32_t ___bufLength_6;
	// System.Int32 System.Xml.XmlInputStream::bufPos
	int32_t ___bufPos_7;

public:
	inline static int32_t get_offset_of_enc_3() { return static_cast<int32_t>(offsetof(XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629, ___enc_3)); }
	inline Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * get_enc_3() const { return ___enc_3; }
	inline Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 ** get_address_of_enc_3() { return &___enc_3; }
	inline void set_enc_3(Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * value)
	{
		___enc_3 = value;
		Il2CppCodeGenWriteBarrier((&___enc_3), value);
	}

	inline static int32_t get_offset_of_stream_4() { return static_cast<int32_t>(offsetof(XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629, ___stream_4)); }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 * get_stream_4() const { return ___stream_4; }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 ** get_address_of_stream_4() { return &___stream_4; }
	inline void set_stream_4(Stream_tCFD27E43C18381861212C0288CACF467FB602009 * value)
	{
		___stream_4 = value;
		Il2CppCodeGenWriteBarrier((&___stream_4), value);
	}

	inline static int32_t get_offset_of_buffer_5() { return static_cast<int32_t>(offsetof(XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629, ___buffer_5)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_buffer_5() const { return ___buffer_5; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_buffer_5() { return &___buffer_5; }
	inline void set_buffer_5(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_5), value);
	}

	inline static int32_t get_offset_of_bufLength_6() { return static_cast<int32_t>(offsetof(XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629, ___bufLength_6)); }
	inline int32_t get_bufLength_6() const { return ___bufLength_6; }
	inline int32_t* get_address_of_bufLength_6() { return &___bufLength_6; }
	inline void set_bufLength_6(int32_t value)
	{
		___bufLength_6 = value;
	}

	inline static int32_t get_offset_of_bufPos_7() { return static_cast<int32_t>(offsetof(XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629, ___bufPos_7)); }
	inline int32_t get_bufPos_7() const { return ___bufPos_7; }
	inline int32_t* get_address_of_bufPos_7() { return &___bufPos_7; }
	inline void set_bufPos_7(int32_t value)
	{
		___bufPos_7 = value;
	}
};

struct XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629_StaticFields
{
public:
	// System.Text.Encoding System.Xml.XmlInputStream::StrictUTF8
	Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * ___StrictUTF8_2;
	// System.Xml.XmlException System.Xml.XmlInputStream::encodingException
	XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1 * ___encodingException_8;

public:
	inline static int32_t get_offset_of_StrictUTF8_2() { return static_cast<int32_t>(offsetof(XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629_StaticFields, ___StrictUTF8_2)); }
	inline Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * get_StrictUTF8_2() const { return ___StrictUTF8_2; }
	inline Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 ** get_address_of_StrictUTF8_2() { return &___StrictUTF8_2; }
	inline void set_StrictUTF8_2(Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * value)
	{
		___StrictUTF8_2 = value;
		Il2CppCodeGenWriteBarrier((&___StrictUTF8_2), value);
	}

	inline static int32_t get_offset_of_encodingException_8() { return static_cast<int32_t>(offsetof(XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629_StaticFields, ___encodingException_8)); }
	inline XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1 * get_encodingException_8() const { return ___encodingException_8; }
	inline XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1 ** get_address_of_encodingException_8() { return &___encodingException_8; }
	inline void set_encodingException_8(XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1 * value)
	{
		___encodingException_8 = value;
		Il2CppCodeGenWriteBarrier((&___encodingException_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLINPUTSTREAM_TD179743E3D4482E2297EEC3A71D082F2F2B2F629_H
#ifndef XMLNODETYPE_TF9194E434ED56EA8A4CBE6EB7181252E20BB94FA_H
#define XMLNODETYPE_TF9194E434ED56EA8A4CBE6EB7181252E20BB94FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_tF9194E434ED56EA8A4CBE6EB7181252E20BB94FA 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeType_tF9194E434ED56EA8A4CBE6EB7181252E20BB94FA, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_TF9194E434ED56EA8A4CBE6EB7181252E20BB94FA_H
#ifndef XMLSPACE_T6A54DEF6D051D3E7B2509C3E101CE153C833F7E0_H
#define XMLSPACE_T6A54DEF6D051D3E7B2509C3E101CE153C833F7E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSpace
struct  XmlSpace_t6A54DEF6D051D3E7B2509C3E101CE153C833F7E0 
{
public:
	// System.Int32 System.Xml.XmlSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSpace_t6A54DEF6D051D3E7B2509C3E101CE153C833F7E0, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSPACE_T6A54DEF6D051D3E7B2509C3E101CE153C833F7E0_H
#ifndef XMLDECLSTATE_T1D1721A2F7823A6484A591BC8F6775DFEF3A13E0_H
#define XMLDECLSTATE_T1D1721A2F7823A6484A591BC8F6775DFEF3A13E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter_XmlDeclState
struct  XmlDeclState_t1D1721A2F7823A6484A591BC8F6775DFEF3A13E0 
{
public:
	// System.Int32 System.Xml.XmlTextWriter_XmlDeclState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlDeclState_t1D1721A2F7823A6484A591BC8F6775DFEF3A13E0, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLSTATE_T1D1721A2F7823A6484A591BC8F6775DFEF3A13E0_H
#ifndef VALIDATIONEVENTARGS_T509D590153253EAD1DC5DCEB4CDD56BE30AF3B1D_H
#define VALIDATIONEVENTARGS_T509D590153253EAD1DC5DCEB4CDD56BE30AF3B1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidationEventArgs
struct  ValidationEventArgs_t509D590153253EAD1DC5DCEB4CDD56BE30AF3B1D  : public EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17
{
public:
	// System.Xml.Schema.XmlSchemaException System.Xml.Schema.ValidationEventArgs::exception
	XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D * ___exception_1;
	// System.String System.Xml.Schema.ValidationEventArgs::message
	String_t* ___message_2;
	// System.Xml.Schema.XmlSeverityType System.Xml.Schema.ValidationEventArgs::severity
	int32_t ___severity_3;

public:
	inline static int32_t get_offset_of_exception_1() { return static_cast<int32_t>(offsetof(ValidationEventArgs_t509D590153253EAD1DC5DCEB4CDD56BE30AF3B1D, ___exception_1)); }
	inline XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D * get_exception_1() const { return ___exception_1; }
	inline XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D ** get_address_of_exception_1() { return &___exception_1; }
	inline void set_exception_1(XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D * value)
	{
		___exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(ValidationEventArgs_t509D590153253EAD1DC5DCEB4CDD56BE30AF3B1D, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_severity_3() { return static_cast<int32_t>(offsetof(ValidationEventArgs_t509D590153253EAD1DC5DCEB4CDD56BE30AF3B1D, ___severity_3)); }
	inline int32_t get_severity_3() const { return ___severity_3; }
	inline int32_t* get_address_of_severity_3() { return &___severity_3; }
	inline void set_severity_3(int32_t value)
	{
		___severity_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONEVENTARGS_T509D590153253EAD1DC5DCEB4CDD56BE30AF3B1D_H
#ifndef XMLSCHEMA_T418D644D822F712374BE88CB3DBE2C010E2D3696_H
#define XMLSCHEMA_T418D644D822F712374BE88CB3DBE2C010E2D3696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchema
struct  XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696  : public XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747
{
public:
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchema::attributeFormDefault
	int32_t ___attributeFormDefault_13;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::attributeGroups
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___attributeGroups_14;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::attributes
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___attributes_15;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchema::blockDefault
	int32_t ___blockDefault_16;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchema::elementFormDefault
	int32_t ___elementFormDefault_17;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::elements
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___elements_18;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchema::finalDefault
	int32_t ___finalDefault_19;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::groups
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___groups_20;
	// System.String System.Xml.Schema.XmlSchema::id
	String_t* ___id_21;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchema::includes
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___includes_22;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchema::items
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___items_23;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::notations
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___notations_24;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchema::schemaTypes
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___schemaTypes_25;
	// System.String System.Xml.Schema.XmlSchema::targetNamespace
	String_t* ___targetNamespace_26;
	// System.String System.Xml.Schema.XmlSchema::version
	String_t* ___version_27;
	// System.Xml.Schema.XmlSchemaSet System.Xml.Schema.XmlSchema::schemas
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * ___schemas_28;
	// System.Xml.XmlNameTable System.Xml.Schema.XmlSchema::nameTable
	XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * ___nameTable_29;
	// System.Boolean System.Xml.Schema.XmlSchema::missedSubComponents
	bool ___missedSubComponents_30;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchema::compilationItems
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___compilationItems_31;

public:
	inline static int32_t get_offset_of_attributeFormDefault_13() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___attributeFormDefault_13)); }
	inline int32_t get_attributeFormDefault_13() const { return ___attributeFormDefault_13; }
	inline int32_t* get_address_of_attributeFormDefault_13() { return &___attributeFormDefault_13; }
	inline void set_attributeFormDefault_13(int32_t value)
	{
		___attributeFormDefault_13 = value;
	}

	inline static int32_t get_offset_of_attributeGroups_14() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___attributeGroups_14)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_attributeGroups_14() const { return ___attributeGroups_14; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_attributeGroups_14() { return &___attributeGroups_14; }
	inline void set_attributeGroups_14(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___attributeGroups_14 = value;
		Il2CppCodeGenWriteBarrier((&___attributeGroups_14), value);
	}

	inline static int32_t get_offset_of_attributes_15() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___attributes_15)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_attributes_15() const { return ___attributes_15; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_attributes_15() { return &___attributes_15; }
	inline void set_attributes_15(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___attributes_15 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_15), value);
	}

	inline static int32_t get_offset_of_blockDefault_16() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___blockDefault_16)); }
	inline int32_t get_blockDefault_16() const { return ___blockDefault_16; }
	inline int32_t* get_address_of_blockDefault_16() { return &___blockDefault_16; }
	inline void set_blockDefault_16(int32_t value)
	{
		___blockDefault_16 = value;
	}

	inline static int32_t get_offset_of_elementFormDefault_17() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___elementFormDefault_17)); }
	inline int32_t get_elementFormDefault_17() const { return ___elementFormDefault_17; }
	inline int32_t* get_address_of_elementFormDefault_17() { return &___elementFormDefault_17; }
	inline void set_elementFormDefault_17(int32_t value)
	{
		___elementFormDefault_17 = value;
	}

	inline static int32_t get_offset_of_elements_18() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___elements_18)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_elements_18() const { return ___elements_18; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_elements_18() { return &___elements_18; }
	inline void set_elements_18(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___elements_18 = value;
		Il2CppCodeGenWriteBarrier((&___elements_18), value);
	}

	inline static int32_t get_offset_of_finalDefault_19() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___finalDefault_19)); }
	inline int32_t get_finalDefault_19() const { return ___finalDefault_19; }
	inline int32_t* get_address_of_finalDefault_19() { return &___finalDefault_19; }
	inline void set_finalDefault_19(int32_t value)
	{
		___finalDefault_19 = value;
	}

	inline static int32_t get_offset_of_groups_20() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___groups_20)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_groups_20() const { return ___groups_20; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_groups_20() { return &___groups_20; }
	inline void set_groups_20(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___groups_20 = value;
		Il2CppCodeGenWriteBarrier((&___groups_20), value);
	}

	inline static int32_t get_offset_of_id_21() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___id_21)); }
	inline String_t* get_id_21() const { return ___id_21; }
	inline String_t** get_address_of_id_21() { return &___id_21; }
	inline void set_id_21(String_t* value)
	{
		___id_21 = value;
		Il2CppCodeGenWriteBarrier((&___id_21), value);
	}

	inline static int32_t get_offset_of_includes_22() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___includes_22)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_includes_22() const { return ___includes_22; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_includes_22() { return &___includes_22; }
	inline void set_includes_22(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___includes_22 = value;
		Il2CppCodeGenWriteBarrier((&___includes_22), value);
	}

	inline static int32_t get_offset_of_items_23() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___items_23)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_items_23() const { return ___items_23; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_items_23() { return &___items_23; }
	inline void set_items_23(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___items_23 = value;
		Il2CppCodeGenWriteBarrier((&___items_23), value);
	}

	inline static int32_t get_offset_of_notations_24() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___notations_24)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_notations_24() const { return ___notations_24; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_notations_24() { return &___notations_24; }
	inline void set_notations_24(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___notations_24 = value;
		Il2CppCodeGenWriteBarrier((&___notations_24), value);
	}

	inline static int32_t get_offset_of_schemaTypes_25() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___schemaTypes_25)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_schemaTypes_25() const { return ___schemaTypes_25; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_schemaTypes_25() { return &___schemaTypes_25; }
	inline void set_schemaTypes_25(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___schemaTypes_25 = value;
		Il2CppCodeGenWriteBarrier((&___schemaTypes_25), value);
	}

	inline static int32_t get_offset_of_targetNamespace_26() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___targetNamespace_26)); }
	inline String_t* get_targetNamespace_26() const { return ___targetNamespace_26; }
	inline String_t** get_address_of_targetNamespace_26() { return &___targetNamespace_26; }
	inline void set_targetNamespace_26(String_t* value)
	{
		___targetNamespace_26 = value;
		Il2CppCodeGenWriteBarrier((&___targetNamespace_26), value);
	}

	inline static int32_t get_offset_of_version_27() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___version_27)); }
	inline String_t* get_version_27() const { return ___version_27; }
	inline String_t** get_address_of_version_27() { return &___version_27; }
	inline void set_version_27(String_t* value)
	{
		___version_27 = value;
		Il2CppCodeGenWriteBarrier((&___version_27), value);
	}

	inline static int32_t get_offset_of_schemas_28() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___schemas_28)); }
	inline XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * get_schemas_28() const { return ___schemas_28; }
	inline XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D ** get_address_of_schemas_28() { return &___schemas_28; }
	inline void set_schemas_28(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * value)
	{
		___schemas_28 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_28), value);
	}

	inline static int32_t get_offset_of_nameTable_29() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___nameTable_29)); }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * get_nameTable_29() const { return ___nameTable_29; }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B ** get_address_of_nameTable_29() { return &___nameTable_29; }
	inline void set_nameTable_29(XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * value)
	{
		___nameTable_29 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_29), value);
	}

	inline static int32_t get_offset_of_missedSubComponents_30() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___missedSubComponents_30)); }
	inline bool get_missedSubComponents_30() const { return ___missedSubComponents_30; }
	inline bool* get_address_of_missedSubComponents_30() { return &___missedSubComponents_30; }
	inline void set_missedSubComponents_30(bool value)
	{
		___missedSubComponents_30 = value;
	}

	inline static int32_t get_offset_of_compilationItems_31() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696, ___compilationItems_31)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_compilationItems_31() const { return ___compilationItems_31; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_compilationItems_31() { return &___compilationItems_31; }
	inline void set_compilationItems_31(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___compilationItems_31 = value;
		Il2CppCodeGenWriteBarrier((&___compilationItems_31), value);
	}
};

struct XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchema::<>f__switchU24map41
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map41_32;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map41_32() { return static_cast<int32_t>(offsetof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696_StaticFields, ___U3CU3Ef__switchU24map41_32)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map41_32() const { return ___U3CU3Ef__switchU24map41_32; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map41_32() { return &___U3CU3Ef__switchU24map41_32; }
	inline void set_U3CU3Ef__switchU24map41_32(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map41_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map41_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMA_T418D644D822F712374BE88CB3DBE2C010E2D3696_H
#ifndef XMLSCHEMAANNOTATED_T86F7843F771EEE4F6A372B3B46CC4497928C3741_H
#define XMLSCHEMAANNOTATED_T86F7843F771EEE4F6A372B3B46CC4497928C3741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnnotated
struct  XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741  : public XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747
{
public:
	// System.Xml.Schema.XmlSchemaAnnotation System.Xml.Schema.XmlSchemaAnnotated::annotation
	XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED * ___annotation_13;
	// System.String System.Xml.Schema.XmlSchemaAnnotated::id
	String_t* ___id_14;
	// System.Xml.XmlAttribute[] System.Xml.Schema.XmlSchemaAnnotated::unhandledAttributes
	XmlAttributeU5BU5D_t48E79142153B9C691C51308E60A01F22035CBD01* ___unhandledAttributes_15;

public:
	inline static int32_t get_offset_of_annotation_13() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741, ___annotation_13)); }
	inline XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED * get_annotation_13() const { return ___annotation_13; }
	inline XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED ** get_address_of_annotation_13() { return &___annotation_13; }
	inline void set_annotation_13(XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED * value)
	{
		___annotation_13 = value;
		Il2CppCodeGenWriteBarrier((&___annotation_13), value);
	}

	inline static int32_t get_offset_of_id_14() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741, ___id_14)); }
	inline String_t* get_id_14() const { return ___id_14; }
	inline String_t** get_address_of_id_14() { return &___id_14; }
	inline void set_id_14(String_t* value)
	{
		___id_14 = value;
		Il2CppCodeGenWriteBarrier((&___id_14), value);
	}

	inline static int32_t get_offset_of_unhandledAttributes_15() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741, ___unhandledAttributes_15)); }
	inline XmlAttributeU5BU5D_t48E79142153B9C691C51308E60A01F22035CBD01* get_unhandledAttributes_15() const { return ___unhandledAttributes_15; }
	inline XmlAttributeU5BU5D_t48E79142153B9C691C51308E60A01F22035CBD01** get_address_of_unhandledAttributes_15() { return &___unhandledAttributes_15; }
	inline void set_unhandledAttributes_15(XmlAttributeU5BU5D_t48E79142153B9C691C51308E60A01F22035CBD01* value)
	{
		___unhandledAttributes_15 = value;
		Il2CppCodeGenWriteBarrier((&___unhandledAttributes_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANNOTATED_T86F7843F771EEE4F6A372B3B46CC4497928C3741_H
#ifndef XMLSCHEMAANNOTATION_T67D83A065574ADCBDEDC1761C352289C91C639ED_H
#define XMLSCHEMAANNOTATION_T67D83A065574ADCBDEDC1761C352289C91C639ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnnotation
struct  XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED  : public XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747
{
public:
	// System.String System.Xml.Schema.XmlSchemaAnnotation::id
	String_t* ___id_13;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaAnnotation::items
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___items_14;

public:
	inline static int32_t get_offset_of_id_13() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED, ___id_13)); }
	inline String_t* get_id_13() const { return ___id_13; }
	inline String_t** get_address_of_id_13() { return &___id_13; }
	inline void set_id_13(String_t* value)
	{
		___id_13 = value;
		Il2CppCodeGenWriteBarrier((&___id_13), value);
	}

	inline static int32_t get_offset_of_items_14() { return static_cast<int32_t>(offsetof(XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED, ___items_14)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_items_14() const { return ___items_14; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_items_14() { return &___items_14; }
	inline void set_items_14(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___items_14 = value;
		Il2CppCodeGenWriteBarrier((&___items_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANNOTATION_T67D83A065574ADCBDEDC1761C352289C91C639ED_H
#ifndef XMLSCHEMAAPPINFO_T7312016110CCC70A45888C86986A3754523E5409_H
#define XMLSCHEMAAPPINFO_T7312016110CCC70A45888C86986A3754523E5409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAppInfo
struct  XmlSchemaAppInfo_t7312016110CCC70A45888C86986A3754523E5409  : public XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747
{
public:
	// System.Xml.XmlNode[] System.Xml.Schema.XmlSchemaAppInfo::markup
	XmlNodeU5BU5D_t880B356D14B59983F6FDD527C6B3C1B9BD673739* ___markup_13;
	// System.String System.Xml.Schema.XmlSchemaAppInfo::source
	String_t* ___source_14;

public:
	inline static int32_t get_offset_of_markup_13() { return static_cast<int32_t>(offsetof(XmlSchemaAppInfo_t7312016110CCC70A45888C86986A3754523E5409, ___markup_13)); }
	inline XmlNodeU5BU5D_t880B356D14B59983F6FDD527C6B3C1B9BD673739* get_markup_13() const { return ___markup_13; }
	inline XmlNodeU5BU5D_t880B356D14B59983F6FDD527C6B3C1B9BD673739** get_address_of_markup_13() { return &___markup_13; }
	inline void set_markup_13(XmlNodeU5BU5D_t880B356D14B59983F6FDD527C6B3C1B9BD673739* value)
	{
		___markup_13 = value;
		Il2CppCodeGenWriteBarrier((&___markup_13), value);
	}

	inline static int32_t get_offset_of_source_14() { return static_cast<int32_t>(offsetof(XmlSchemaAppInfo_t7312016110CCC70A45888C86986A3754523E5409, ___source_14)); }
	inline String_t* get_source_14() const { return ___source_14; }
	inline String_t** get_address_of_source_14() { return &___source_14; }
	inline void set_source_14(String_t* value)
	{
		___source_14 = value;
		Il2CppCodeGenWriteBarrier((&___source_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAAPPINFO_T7312016110CCC70A45888C86986A3754523E5409_H
#ifndef XMLSCHEMADATATYPE_T3384DFF75456A27E693C24453B91B2B7A0CC2A19_H
#define XMLSCHEMADATATYPE_T3384DFF75456A27E693C24453B91B2B7A0CC2A19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatype
struct  XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19  : public RuntimeObject
{
public:
	// Mono.Xml.Schema.XsdWhitespaceFacet System.Xml.Schema.XmlSchemaDatatype::WhitespaceValue
	int32_t ___WhitespaceValue_0;
	// System.Text.StringBuilder System.Xml.Schema.XmlSchemaDatatype::sb
	StringBuilder_t * ___sb_2;

public:
	inline static int32_t get_offset_of_WhitespaceValue_0() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19, ___WhitespaceValue_0)); }
	inline int32_t get_WhitespaceValue_0() const { return ___WhitespaceValue_0; }
	inline int32_t* get_address_of_WhitespaceValue_0() { return &___WhitespaceValue_0; }
	inline void set_WhitespaceValue_0(int32_t value)
	{
		___WhitespaceValue_0 = value;
	}

	inline static int32_t get_offset_of_sb_2() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19, ___sb_2)); }
	inline StringBuilder_t * get_sb_2() const { return ___sb_2; }
	inline StringBuilder_t ** get_address_of_sb_2() { return &___sb_2; }
	inline void set_sb_2(StringBuilder_t * value)
	{
		___sb_2 = value;
		Il2CppCodeGenWriteBarrier((&___sb_2), value);
	}
};

struct XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields
{
public:
	// System.Char[] System.Xml.Schema.XmlSchemaDatatype::wsChars
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___wsChars_1;
	// Mono.Xml.Schema.XsdAnySimpleType System.Xml.Schema.XmlSchemaDatatype::datatypeAnySimpleType
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * ___datatypeAnySimpleType_3;
	// Mono.Xml.Schema.XsdString System.Xml.Schema.XmlSchemaDatatype::datatypeString
	XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD * ___datatypeString_4;
	// Mono.Xml.Schema.XsdNormalizedString System.Xml.Schema.XmlSchemaDatatype::datatypeNormalizedString
	XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0 * ___datatypeNormalizedString_5;
	// Mono.Xml.Schema.XsdToken System.Xml.Schema.XmlSchemaDatatype::datatypeToken
	XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C * ___datatypeToken_6;
	// Mono.Xml.Schema.XsdLanguage System.Xml.Schema.XmlSchemaDatatype::datatypeLanguage
	XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85 * ___datatypeLanguage_7;
	// Mono.Xml.Schema.XsdNMToken System.Xml.Schema.XmlSchemaDatatype::datatypeNMToken
	XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91 * ___datatypeNMToken_8;
	// Mono.Xml.Schema.XsdNMTokens System.Xml.Schema.XmlSchemaDatatype::datatypeNMTokens
	XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095 * ___datatypeNMTokens_9;
	// Mono.Xml.Schema.XsdName System.Xml.Schema.XmlSchemaDatatype::datatypeName
	XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8 * ___datatypeName_10;
	// Mono.Xml.Schema.XsdNCName System.Xml.Schema.XmlSchemaDatatype::datatypeNCName
	XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7 * ___datatypeNCName_11;
	// Mono.Xml.Schema.XsdID System.Xml.Schema.XmlSchemaDatatype::datatypeID
	XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B * ___datatypeID_12;
	// Mono.Xml.Schema.XsdIDRef System.Xml.Schema.XmlSchemaDatatype::datatypeIDRef
	XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B * ___datatypeIDRef_13;
	// Mono.Xml.Schema.XsdIDRefs System.Xml.Schema.XmlSchemaDatatype::datatypeIDRefs
	XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F * ___datatypeIDRefs_14;
	// Mono.Xml.Schema.XsdEntity System.Xml.Schema.XmlSchemaDatatype::datatypeEntity
	XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371 * ___datatypeEntity_15;
	// Mono.Xml.Schema.XsdEntities System.Xml.Schema.XmlSchemaDatatype::datatypeEntities
	XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C * ___datatypeEntities_16;
	// Mono.Xml.Schema.XsdNotation System.Xml.Schema.XmlSchemaDatatype::datatypeNotation
	XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3 * ___datatypeNotation_17;
	// Mono.Xml.Schema.XsdDecimal System.Xml.Schema.XmlSchemaDatatype::datatypeDecimal
	XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE * ___datatypeDecimal_18;
	// Mono.Xml.Schema.XsdInteger System.Xml.Schema.XmlSchemaDatatype::datatypeInteger
	XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348 * ___datatypeInteger_19;
	// Mono.Xml.Schema.XsdLong System.Xml.Schema.XmlSchemaDatatype::datatypeLong
	XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC * ___datatypeLong_20;
	// Mono.Xml.Schema.XsdInt System.Xml.Schema.XmlSchemaDatatype::datatypeInt
	XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F * ___datatypeInt_21;
	// Mono.Xml.Schema.XsdShort System.Xml.Schema.XmlSchemaDatatype::datatypeShort
	XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332 * ___datatypeShort_22;
	// Mono.Xml.Schema.XsdByte System.Xml.Schema.XmlSchemaDatatype::datatypeByte
	XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351 * ___datatypeByte_23;
	// Mono.Xml.Schema.XsdNonNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonNegativeInteger
	XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30 * ___datatypeNonNegativeInteger_24;
	// Mono.Xml.Schema.XsdPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypePositiveInteger
	XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2 * ___datatypePositiveInteger_25;
	// Mono.Xml.Schema.XsdUnsignedLong System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedLong
	XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9 * ___datatypeUnsignedLong_26;
	// Mono.Xml.Schema.XsdUnsignedInt System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedInt
	XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32 * ___datatypeUnsignedInt_27;
	// Mono.Xml.Schema.XsdUnsignedShort System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedShort
	XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785 * ___datatypeUnsignedShort_28;
	// Mono.Xml.Schema.XsdUnsignedByte System.Xml.Schema.XmlSchemaDatatype::datatypeUnsignedByte
	XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256 * ___datatypeUnsignedByte_29;
	// Mono.Xml.Schema.XsdNonPositiveInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNonPositiveInteger
	XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62 * ___datatypeNonPositiveInteger_30;
	// Mono.Xml.Schema.XsdNegativeInteger System.Xml.Schema.XmlSchemaDatatype::datatypeNegativeInteger
	XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B * ___datatypeNegativeInteger_31;
	// Mono.Xml.Schema.XsdFloat System.Xml.Schema.XmlSchemaDatatype::datatypeFloat
	XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8 * ___datatypeFloat_32;
	// Mono.Xml.Schema.XsdDouble System.Xml.Schema.XmlSchemaDatatype::datatypeDouble
	XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269 * ___datatypeDouble_33;
	// Mono.Xml.Schema.XsdBase64Binary System.Xml.Schema.XmlSchemaDatatype::datatypeBase64Binary
	XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F * ___datatypeBase64Binary_34;
	// Mono.Xml.Schema.XsdBoolean System.Xml.Schema.XmlSchemaDatatype::datatypeBoolean
	XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959 * ___datatypeBoolean_35;
	// Mono.Xml.Schema.XsdAnyURI System.Xml.Schema.XmlSchemaDatatype::datatypeAnyURI
	XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A * ___datatypeAnyURI_36;
	// Mono.Xml.Schema.XsdDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDuration
	XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78 * ___datatypeDuration_37;
	// Mono.Xml.Schema.XsdDateTime System.Xml.Schema.XmlSchemaDatatype::datatypeDateTime
	XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D * ___datatypeDateTime_38;
	// Mono.Xml.Schema.XsdDate System.Xml.Schema.XmlSchemaDatatype::datatypeDate
	XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964 * ___datatypeDate_39;
	// Mono.Xml.Schema.XsdTime System.Xml.Schema.XmlSchemaDatatype::datatypeTime
	XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C * ___datatypeTime_40;
	// Mono.Xml.Schema.XsdHexBinary System.Xml.Schema.XmlSchemaDatatype::datatypeHexBinary
	XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229 * ___datatypeHexBinary_41;
	// Mono.Xml.Schema.XsdQName System.Xml.Schema.XmlSchemaDatatype::datatypeQName
	XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0 * ___datatypeQName_42;
	// Mono.Xml.Schema.XsdGYearMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGYearMonth
	XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385 * ___datatypeGYearMonth_43;
	// Mono.Xml.Schema.XsdGMonthDay System.Xml.Schema.XmlSchemaDatatype::datatypeGMonthDay
	XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669 * ___datatypeGMonthDay_44;
	// Mono.Xml.Schema.XsdGYear System.Xml.Schema.XmlSchemaDatatype::datatypeGYear
	XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2 * ___datatypeGYear_45;
	// Mono.Xml.Schema.XsdGMonth System.Xml.Schema.XmlSchemaDatatype::datatypeGMonth
	XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312 * ___datatypeGMonth_46;
	// Mono.Xml.Schema.XsdGDay System.Xml.Schema.XmlSchemaDatatype::datatypeGDay
	XsdGDay_tEC936DF85B2878EB240E093D4B56BBDDC75A463B * ___datatypeGDay_47;
	// Mono.Xml.Schema.XdtAnyAtomicType System.Xml.Schema.XmlSchemaDatatype::datatypeAnyAtomicType
	XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7 * ___datatypeAnyAtomicType_48;
	// Mono.Xml.Schema.XdtUntypedAtomic System.Xml.Schema.XmlSchemaDatatype::datatypeUntypedAtomic
	XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED * ___datatypeUntypedAtomic_49;
	// Mono.Xml.Schema.XdtDayTimeDuration System.Xml.Schema.XmlSchemaDatatype::datatypeDayTimeDuration
	XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F * ___datatypeDayTimeDuration_50;
	// Mono.Xml.Schema.XdtYearMonthDuration System.Xml.Schema.XmlSchemaDatatype::datatypeYearMonthDuration
	XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68 * ___datatypeYearMonthDuration_51;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switchU24map3E
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map3E_52;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switchU24map3F
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map3F_53;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaDatatype::<>f__switchU24map40
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map40_54;

public:
	inline static int32_t get_offset_of_wsChars_1() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___wsChars_1)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_wsChars_1() const { return ___wsChars_1; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_wsChars_1() { return &___wsChars_1; }
	inline void set_wsChars_1(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___wsChars_1 = value;
		Il2CppCodeGenWriteBarrier((&___wsChars_1), value);
	}

	inline static int32_t get_offset_of_datatypeAnySimpleType_3() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeAnySimpleType_3)); }
	inline XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * get_datatypeAnySimpleType_3() const { return ___datatypeAnySimpleType_3; }
	inline XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 ** get_address_of_datatypeAnySimpleType_3() { return &___datatypeAnySimpleType_3; }
	inline void set_datatypeAnySimpleType_3(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * value)
	{
		___datatypeAnySimpleType_3 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnySimpleType_3), value);
	}

	inline static int32_t get_offset_of_datatypeString_4() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeString_4)); }
	inline XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD * get_datatypeString_4() const { return ___datatypeString_4; }
	inline XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD ** get_address_of_datatypeString_4() { return &___datatypeString_4; }
	inline void set_datatypeString_4(XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD * value)
	{
		___datatypeString_4 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeString_4), value);
	}

	inline static int32_t get_offset_of_datatypeNormalizedString_5() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNormalizedString_5)); }
	inline XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0 * get_datatypeNormalizedString_5() const { return ___datatypeNormalizedString_5; }
	inline XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0 ** get_address_of_datatypeNormalizedString_5() { return &___datatypeNormalizedString_5; }
	inline void set_datatypeNormalizedString_5(XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0 * value)
	{
		___datatypeNormalizedString_5 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNormalizedString_5), value);
	}

	inline static int32_t get_offset_of_datatypeToken_6() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeToken_6)); }
	inline XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C * get_datatypeToken_6() const { return ___datatypeToken_6; }
	inline XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C ** get_address_of_datatypeToken_6() { return &___datatypeToken_6; }
	inline void set_datatypeToken_6(XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C * value)
	{
		___datatypeToken_6 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeToken_6), value);
	}

	inline static int32_t get_offset_of_datatypeLanguage_7() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeLanguage_7)); }
	inline XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85 * get_datatypeLanguage_7() const { return ___datatypeLanguage_7; }
	inline XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85 ** get_address_of_datatypeLanguage_7() { return &___datatypeLanguage_7; }
	inline void set_datatypeLanguage_7(XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85 * value)
	{
		___datatypeLanguage_7 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeLanguage_7), value);
	}

	inline static int32_t get_offset_of_datatypeNMToken_8() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNMToken_8)); }
	inline XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91 * get_datatypeNMToken_8() const { return ___datatypeNMToken_8; }
	inline XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91 ** get_address_of_datatypeNMToken_8() { return &___datatypeNMToken_8; }
	inline void set_datatypeNMToken_8(XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91 * value)
	{
		___datatypeNMToken_8 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNMToken_8), value);
	}

	inline static int32_t get_offset_of_datatypeNMTokens_9() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNMTokens_9)); }
	inline XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095 * get_datatypeNMTokens_9() const { return ___datatypeNMTokens_9; }
	inline XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095 ** get_address_of_datatypeNMTokens_9() { return &___datatypeNMTokens_9; }
	inline void set_datatypeNMTokens_9(XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095 * value)
	{
		___datatypeNMTokens_9 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNMTokens_9), value);
	}

	inline static int32_t get_offset_of_datatypeName_10() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeName_10)); }
	inline XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8 * get_datatypeName_10() const { return ___datatypeName_10; }
	inline XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8 ** get_address_of_datatypeName_10() { return &___datatypeName_10; }
	inline void set_datatypeName_10(XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8 * value)
	{
		___datatypeName_10 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeName_10), value);
	}

	inline static int32_t get_offset_of_datatypeNCName_11() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNCName_11)); }
	inline XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7 * get_datatypeNCName_11() const { return ___datatypeNCName_11; }
	inline XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7 ** get_address_of_datatypeNCName_11() { return &___datatypeNCName_11; }
	inline void set_datatypeNCName_11(XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7 * value)
	{
		___datatypeNCName_11 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNCName_11), value);
	}

	inline static int32_t get_offset_of_datatypeID_12() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeID_12)); }
	inline XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B * get_datatypeID_12() const { return ___datatypeID_12; }
	inline XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B ** get_address_of_datatypeID_12() { return &___datatypeID_12; }
	inline void set_datatypeID_12(XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B * value)
	{
		___datatypeID_12 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeID_12), value);
	}

	inline static int32_t get_offset_of_datatypeIDRef_13() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeIDRef_13)); }
	inline XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B * get_datatypeIDRef_13() const { return ___datatypeIDRef_13; }
	inline XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B ** get_address_of_datatypeIDRef_13() { return &___datatypeIDRef_13; }
	inline void set_datatypeIDRef_13(XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B * value)
	{
		___datatypeIDRef_13 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeIDRef_13), value);
	}

	inline static int32_t get_offset_of_datatypeIDRefs_14() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeIDRefs_14)); }
	inline XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F * get_datatypeIDRefs_14() const { return ___datatypeIDRefs_14; }
	inline XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F ** get_address_of_datatypeIDRefs_14() { return &___datatypeIDRefs_14; }
	inline void set_datatypeIDRefs_14(XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F * value)
	{
		___datatypeIDRefs_14 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeIDRefs_14), value);
	}

	inline static int32_t get_offset_of_datatypeEntity_15() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeEntity_15)); }
	inline XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371 * get_datatypeEntity_15() const { return ___datatypeEntity_15; }
	inline XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371 ** get_address_of_datatypeEntity_15() { return &___datatypeEntity_15; }
	inline void set_datatypeEntity_15(XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371 * value)
	{
		___datatypeEntity_15 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeEntity_15), value);
	}

	inline static int32_t get_offset_of_datatypeEntities_16() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeEntities_16)); }
	inline XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C * get_datatypeEntities_16() const { return ___datatypeEntities_16; }
	inline XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C ** get_address_of_datatypeEntities_16() { return &___datatypeEntities_16; }
	inline void set_datatypeEntities_16(XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C * value)
	{
		___datatypeEntities_16 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeEntities_16), value);
	}

	inline static int32_t get_offset_of_datatypeNotation_17() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNotation_17)); }
	inline XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3 * get_datatypeNotation_17() const { return ___datatypeNotation_17; }
	inline XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3 ** get_address_of_datatypeNotation_17() { return &___datatypeNotation_17; }
	inline void set_datatypeNotation_17(XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3 * value)
	{
		___datatypeNotation_17 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNotation_17), value);
	}

	inline static int32_t get_offset_of_datatypeDecimal_18() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeDecimal_18)); }
	inline XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE * get_datatypeDecimal_18() const { return ___datatypeDecimal_18; }
	inline XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE ** get_address_of_datatypeDecimal_18() { return &___datatypeDecimal_18; }
	inline void set_datatypeDecimal_18(XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE * value)
	{
		___datatypeDecimal_18 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDecimal_18), value);
	}

	inline static int32_t get_offset_of_datatypeInteger_19() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeInteger_19)); }
	inline XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348 * get_datatypeInteger_19() const { return ___datatypeInteger_19; }
	inline XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348 ** get_address_of_datatypeInteger_19() { return &___datatypeInteger_19; }
	inline void set_datatypeInteger_19(XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348 * value)
	{
		___datatypeInteger_19 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeInteger_19), value);
	}

	inline static int32_t get_offset_of_datatypeLong_20() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeLong_20)); }
	inline XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC * get_datatypeLong_20() const { return ___datatypeLong_20; }
	inline XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC ** get_address_of_datatypeLong_20() { return &___datatypeLong_20; }
	inline void set_datatypeLong_20(XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC * value)
	{
		___datatypeLong_20 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeLong_20), value);
	}

	inline static int32_t get_offset_of_datatypeInt_21() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeInt_21)); }
	inline XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F * get_datatypeInt_21() const { return ___datatypeInt_21; }
	inline XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F ** get_address_of_datatypeInt_21() { return &___datatypeInt_21; }
	inline void set_datatypeInt_21(XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F * value)
	{
		___datatypeInt_21 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeInt_21), value);
	}

	inline static int32_t get_offset_of_datatypeShort_22() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeShort_22)); }
	inline XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332 * get_datatypeShort_22() const { return ___datatypeShort_22; }
	inline XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332 ** get_address_of_datatypeShort_22() { return &___datatypeShort_22; }
	inline void set_datatypeShort_22(XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332 * value)
	{
		___datatypeShort_22 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeShort_22), value);
	}

	inline static int32_t get_offset_of_datatypeByte_23() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeByte_23)); }
	inline XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351 * get_datatypeByte_23() const { return ___datatypeByte_23; }
	inline XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351 ** get_address_of_datatypeByte_23() { return &___datatypeByte_23; }
	inline void set_datatypeByte_23(XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351 * value)
	{
		___datatypeByte_23 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeByte_23), value);
	}

	inline static int32_t get_offset_of_datatypeNonNegativeInteger_24() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNonNegativeInteger_24)); }
	inline XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30 * get_datatypeNonNegativeInteger_24() const { return ___datatypeNonNegativeInteger_24; }
	inline XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30 ** get_address_of_datatypeNonNegativeInteger_24() { return &___datatypeNonNegativeInteger_24; }
	inline void set_datatypeNonNegativeInteger_24(XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30 * value)
	{
		___datatypeNonNegativeInteger_24 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNonNegativeInteger_24), value);
	}

	inline static int32_t get_offset_of_datatypePositiveInteger_25() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypePositiveInteger_25)); }
	inline XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2 * get_datatypePositiveInteger_25() const { return ___datatypePositiveInteger_25; }
	inline XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2 ** get_address_of_datatypePositiveInteger_25() { return &___datatypePositiveInteger_25; }
	inline void set_datatypePositiveInteger_25(XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2 * value)
	{
		___datatypePositiveInteger_25 = value;
		Il2CppCodeGenWriteBarrier((&___datatypePositiveInteger_25), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedLong_26() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeUnsignedLong_26)); }
	inline XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9 * get_datatypeUnsignedLong_26() const { return ___datatypeUnsignedLong_26; }
	inline XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9 ** get_address_of_datatypeUnsignedLong_26() { return &___datatypeUnsignedLong_26; }
	inline void set_datatypeUnsignedLong_26(XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9 * value)
	{
		___datatypeUnsignedLong_26 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedLong_26), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedInt_27() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeUnsignedInt_27)); }
	inline XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32 * get_datatypeUnsignedInt_27() const { return ___datatypeUnsignedInt_27; }
	inline XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32 ** get_address_of_datatypeUnsignedInt_27() { return &___datatypeUnsignedInt_27; }
	inline void set_datatypeUnsignedInt_27(XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32 * value)
	{
		___datatypeUnsignedInt_27 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedInt_27), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedShort_28() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeUnsignedShort_28)); }
	inline XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785 * get_datatypeUnsignedShort_28() const { return ___datatypeUnsignedShort_28; }
	inline XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785 ** get_address_of_datatypeUnsignedShort_28() { return &___datatypeUnsignedShort_28; }
	inline void set_datatypeUnsignedShort_28(XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785 * value)
	{
		___datatypeUnsignedShort_28 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedShort_28), value);
	}

	inline static int32_t get_offset_of_datatypeUnsignedByte_29() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeUnsignedByte_29)); }
	inline XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256 * get_datatypeUnsignedByte_29() const { return ___datatypeUnsignedByte_29; }
	inline XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256 ** get_address_of_datatypeUnsignedByte_29() { return &___datatypeUnsignedByte_29; }
	inline void set_datatypeUnsignedByte_29(XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256 * value)
	{
		___datatypeUnsignedByte_29 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUnsignedByte_29), value);
	}

	inline static int32_t get_offset_of_datatypeNonPositiveInteger_30() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNonPositiveInteger_30)); }
	inline XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62 * get_datatypeNonPositiveInteger_30() const { return ___datatypeNonPositiveInteger_30; }
	inline XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62 ** get_address_of_datatypeNonPositiveInteger_30() { return &___datatypeNonPositiveInteger_30; }
	inline void set_datatypeNonPositiveInteger_30(XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62 * value)
	{
		___datatypeNonPositiveInteger_30 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNonPositiveInteger_30), value);
	}

	inline static int32_t get_offset_of_datatypeNegativeInteger_31() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeNegativeInteger_31)); }
	inline XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B * get_datatypeNegativeInteger_31() const { return ___datatypeNegativeInteger_31; }
	inline XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B ** get_address_of_datatypeNegativeInteger_31() { return &___datatypeNegativeInteger_31; }
	inline void set_datatypeNegativeInteger_31(XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B * value)
	{
		___datatypeNegativeInteger_31 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeNegativeInteger_31), value);
	}

	inline static int32_t get_offset_of_datatypeFloat_32() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeFloat_32)); }
	inline XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8 * get_datatypeFloat_32() const { return ___datatypeFloat_32; }
	inline XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8 ** get_address_of_datatypeFloat_32() { return &___datatypeFloat_32; }
	inline void set_datatypeFloat_32(XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8 * value)
	{
		___datatypeFloat_32 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeFloat_32), value);
	}

	inline static int32_t get_offset_of_datatypeDouble_33() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeDouble_33)); }
	inline XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269 * get_datatypeDouble_33() const { return ___datatypeDouble_33; }
	inline XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269 ** get_address_of_datatypeDouble_33() { return &___datatypeDouble_33; }
	inline void set_datatypeDouble_33(XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269 * value)
	{
		___datatypeDouble_33 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDouble_33), value);
	}

	inline static int32_t get_offset_of_datatypeBase64Binary_34() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeBase64Binary_34)); }
	inline XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F * get_datatypeBase64Binary_34() const { return ___datatypeBase64Binary_34; }
	inline XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F ** get_address_of_datatypeBase64Binary_34() { return &___datatypeBase64Binary_34; }
	inline void set_datatypeBase64Binary_34(XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F * value)
	{
		___datatypeBase64Binary_34 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeBase64Binary_34), value);
	}

	inline static int32_t get_offset_of_datatypeBoolean_35() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeBoolean_35)); }
	inline XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959 * get_datatypeBoolean_35() const { return ___datatypeBoolean_35; }
	inline XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959 ** get_address_of_datatypeBoolean_35() { return &___datatypeBoolean_35; }
	inline void set_datatypeBoolean_35(XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959 * value)
	{
		___datatypeBoolean_35 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeBoolean_35), value);
	}

	inline static int32_t get_offset_of_datatypeAnyURI_36() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeAnyURI_36)); }
	inline XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A * get_datatypeAnyURI_36() const { return ___datatypeAnyURI_36; }
	inline XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A ** get_address_of_datatypeAnyURI_36() { return &___datatypeAnyURI_36; }
	inline void set_datatypeAnyURI_36(XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A * value)
	{
		___datatypeAnyURI_36 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnyURI_36), value);
	}

	inline static int32_t get_offset_of_datatypeDuration_37() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeDuration_37)); }
	inline XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78 * get_datatypeDuration_37() const { return ___datatypeDuration_37; }
	inline XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78 ** get_address_of_datatypeDuration_37() { return &___datatypeDuration_37; }
	inline void set_datatypeDuration_37(XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78 * value)
	{
		___datatypeDuration_37 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDuration_37), value);
	}

	inline static int32_t get_offset_of_datatypeDateTime_38() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeDateTime_38)); }
	inline XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D * get_datatypeDateTime_38() const { return ___datatypeDateTime_38; }
	inline XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D ** get_address_of_datatypeDateTime_38() { return &___datatypeDateTime_38; }
	inline void set_datatypeDateTime_38(XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D * value)
	{
		___datatypeDateTime_38 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDateTime_38), value);
	}

	inline static int32_t get_offset_of_datatypeDate_39() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeDate_39)); }
	inline XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964 * get_datatypeDate_39() const { return ___datatypeDate_39; }
	inline XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964 ** get_address_of_datatypeDate_39() { return &___datatypeDate_39; }
	inline void set_datatypeDate_39(XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964 * value)
	{
		___datatypeDate_39 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDate_39), value);
	}

	inline static int32_t get_offset_of_datatypeTime_40() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeTime_40)); }
	inline XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C * get_datatypeTime_40() const { return ___datatypeTime_40; }
	inline XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C ** get_address_of_datatypeTime_40() { return &___datatypeTime_40; }
	inline void set_datatypeTime_40(XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C * value)
	{
		___datatypeTime_40 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeTime_40), value);
	}

	inline static int32_t get_offset_of_datatypeHexBinary_41() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeHexBinary_41)); }
	inline XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229 * get_datatypeHexBinary_41() const { return ___datatypeHexBinary_41; }
	inline XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229 ** get_address_of_datatypeHexBinary_41() { return &___datatypeHexBinary_41; }
	inline void set_datatypeHexBinary_41(XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229 * value)
	{
		___datatypeHexBinary_41 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeHexBinary_41), value);
	}

	inline static int32_t get_offset_of_datatypeQName_42() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeQName_42)); }
	inline XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0 * get_datatypeQName_42() const { return ___datatypeQName_42; }
	inline XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0 ** get_address_of_datatypeQName_42() { return &___datatypeQName_42; }
	inline void set_datatypeQName_42(XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0 * value)
	{
		___datatypeQName_42 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeQName_42), value);
	}

	inline static int32_t get_offset_of_datatypeGYearMonth_43() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeGYearMonth_43)); }
	inline XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385 * get_datatypeGYearMonth_43() const { return ___datatypeGYearMonth_43; }
	inline XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385 ** get_address_of_datatypeGYearMonth_43() { return &___datatypeGYearMonth_43; }
	inline void set_datatypeGYearMonth_43(XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385 * value)
	{
		___datatypeGYearMonth_43 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGYearMonth_43), value);
	}

	inline static int32_t get_offset_of_datatypeGMonthDay_44() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeGMonthDay_44)); }
	inline XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669 * get_datatypeGMonthDay_44() const { return ___datatypeGMonthDay_44; }
	inline XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669 ** get_address_of_datatypeGMonthDay_44() { return &___datatypeGMonthDay_44; }
	inline void set_datatypeGMonthDay_44(XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669 * value)
	{
		___datatypeGMonthDay_44 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGMonthDay_44), value);
	}

	inline static int32_t get_offset_of_datatypeGYear_45() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeGYear_45)); }
	inline XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2 * get_datatypeGYear_45() const { return ___datatypeGYear_45; }
	inline XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2 ** get_address_of_datatypeGYear_45() { return &___datatypeGYear_45; }
	inline void set_datatypeGYear_45(XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2 * value)
	{
		___datatypeGYear_45 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGYear_45), value);
	}

	inline static int32_t get_offset_of_datatypeGMonth_46() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeGMonth_46)); }
	inline XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312 * get_datatypeGMonth_46() const { return ___datatypeGMonth_46; }
	inline XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312 ** get_address_of_datatypeGMonth_46() { return &___datatypeGMonth_46; }
	inline void set_datatypeGMonth_46(XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312 * value)
	{
		___datatypeGMonth_46 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGMonth_46), value);
	}

	inline static int32_t get_offset_of_datatypeGDay_47() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeGDay_47)); }
	inline XsdGDay_tEC936DF85B2878EB240E093D4B56BBDDC75A463B * get_datatypeGDay_47() const { return ___datatypeGDay_47; }
	inline XsdGDay_tEC936DF85B2878EB240E093D4B56BBDDC75A463B ** get_address_of_datatypeGDay_47() { return &___datatypeGDay_47; }
	inline void set_datatypeGDay_47(XsdGDay_tEC936DF85B2878EB240E093D4B56BBDDC75A463B * value)
	{
		___datatypeGDay_47 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeGDay_47), value);
	}

	inline static int32_t get_offset_of_datatypeAnyAtomicType_48() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeAnyAtomicType_48)); }
	inline XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7 * get_datatypeAnyAtomicType_48() const { return ___datatypeAnyAtomicType_48; }
	inline XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7 ** get_address_of_datatypeAnyAtomicType_48() { return &___datatypeAnyAtomicType_48; }
	inline void set_datatypeAnyAtomicType_48(XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7 * value)
	{
		___datatypeAnyAtomicType_48 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeAnyAtomicType_48), value);
	}

	inline static int32_t get_offset_of_datatypeUntypedAtomic_49() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeUntypedAtomic_49)); }
	inline XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED * get_datatypeUntypedAtomic_49() const { return ___datatypeUntypedAtomic_49; }
	inline XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED ** get_address_of_datatypeUntypedAtomic_49() { return &___datatypeUntypedAtomic_49; }
	inline void set_datatypeUntypedAtomic_49(XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED * value)
	{
		___datatypeUntypedAtomic_49 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeUntypedAtomic_49), value);
	}

	inline static int32_t get_offset_of_datatypeDayTimeDuration_50() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeDayTimeDuration_50)); }
	inline XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F * get_datatypeDayTimeDuration_50() const { return ___datatypeDayTimeDuration_50; }
	inline XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F ** get_address_of_datatypeDayTimeDuration_50() { return &___datatypeDayTimeDuration_50; }
	inline void set_datatypeDayTimeDuration_50(XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F * value)
	{
		___datatypeDayTimeDuration_50 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeDayTimeDuration_50), value);
	}

	inline static int32_t get_offset_of_datatypeYearMonthDuration_51() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___datatypeYearMonthDuration_51)); }
	inline XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68 * get_datatypeYearMonthDuration_51() const { return ___datatypeYearMonthDuration_51; }
	inline XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68 ** get_address_of_datatypeYearMonthDuration_51() { return &___datatypeYearMonthDuration_51; }
	inline void set_datatypeYearMonthDuration_51(XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68 * value)
	{
		___datatypeYearMonthDuration_51 = value;
		Il2CppCodeGenWriteBarrier((&___datatypeYearMonthDuration_51), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3E_52() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___U3CU3Ef__switchU24map3E_52)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map3E_52() const { return ___U3CU3Ef__switchU24map3E_52; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map3E_52() { return &___U3CU3Ef__switchU24map3E_52; }
	inline void set_U3CU3Ef__switchU24map3E_52(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map3E_52 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3E_52), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3F_53() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___U3CU3Ef__switchU24map3F_53)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map3F_53() const { return ___U3CU3Ef__switchU24map3F_53; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map3F_53() { return &___U3CU3Ef__switchU24map3F_53; }
	inline void set_U3CU3Ef__switchU24map3F_53(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map3F_53 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3F_53), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map40_54() { return static_cast<int32_t>(offsetof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields, ___U3CU3Ef__switchU24map40_54)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map40_54() const { return ___U3CU3Ef__switchU24map40_54; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map40_54() { return &___U3CU3Ef__switchU24map40_54; }
	inline void set_U3CU3Ef__switchU24map40_54(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map40_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map40_54), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPE_T3384DFF75456A27E693C24453B91B2B7A0CC2A19_H
#ifndef XMLSCHEMADOCUMENTATION_TF5EE1377C73D6ADCDF6142903E593FB6CCFAE15E_H
#define XMLSCHEMADOCUMENTATION_TF5EE1377C73D6ADCDF6142903E593FB6CCFAE15E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDocumentation
struct  XmlSchemaDocumentation_tF5EE1377C73D6ADCDF6142903E593FB6CCFAE15E  : public XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747
{
public:
	// System.String System.Xml.Schema.XmlSchemaDocumentation::language
	String_t* ___language_13;
	// System.Xml.XmlNode[] System.Xml.Schema.XmlSchemaDocumentation::markup
	XmlNodeU5BU5D_t880B356D14B59983F6FDD527C6B3C1B9BD673739* ___markup_14;
	// System.String System.Xml.Schema.XmlSchemaDocumentation::source
	String_t* ___source_15;

public:
	inline static int32_t get_offset_of_language_13() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_tF5EE1377C73D6ADCDF6142903E593FB6CCFAE15E, ___language_13)); }
	inline String_t* get_language_13() const { return ___language_13; }
	inline String_t** get_address_of_language_13() { return &___language_13; }
	inline void set_language_13(String_t* value)
	{
		___language_13 = value;
		Il2CppCodeGenWriteBarrier((&___language_13), value);
	}

	inline static int32_t get_offset_of_markup_14() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_tF5EE1377C73D6ADCDF6142903E593FB6CCFAE15E, ___markup_14)); }
	inline XmlNodeU5BU5D_t880B356D14B59983F6FDD527C6B3C1B9BD673739* get_markup_14() const { return ___markup_14; }
	inline XmlNodeU5BU5D_t880B356D14B59983F6FDD527C6B3C1B9BD673739** get_address_of_markup_14() { return &___markup_14; }
	inline void set_markup_14(XmlNodeU5BU5D_t880B356D14B59983F6FDD527C6B3C1B9BD673739* value)
	{
		___markup_14 = value;
		Il2CppCodeGenWriteBarrier((&___markup_14), value);
	}

	inline static int32_t get_offset_of_source_15() { return static_cast<int32_t>(offsetof(XmlSchemaDocumentation_tF5EE1377C73D6ADCDF6142903E593FB6CCFAE15E, ___source_15)); }
	inline String_t* get_source_15() const { return ___source_15; }
	inline String_t** get_address_of_source_15() { return &___source_15; }
	inline void set_source_15(String_t* value)
	{
		___source_15 = value;
		Il2CppCodeGenWriteBarrier((&___source_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADOCUMENTATION_TF5EE1377C73D6ADCDF6142903E593FB6CCFAE15E_H
#ifndef XMLSCHEMAEXTERNAL_TF75596B2A753B85E30D6F9675F05E107D9A287D9_H
#define XMLSCHEMAEXTERNAL_TF75596B2A753B85E30D6F9675F05E107D9A287D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaExternal
struct  XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9  : public XmlSchemaObject_tDD61EB6519D1FF4E9B08ADD6728F9FA9EDCBC747
{
public:
	// System.String System.Xml.Schema.XmlSchemaExternal::id
	String_t* ___id_13;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaExternal::schema
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * ___schema_14;
	// System.String System.Xml.Schema.XmlSchemaExternal::location
	String_t* ___location_15;

public:
	inline static int32_t get_offset_of_id_13() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9, ___id_13)); }
	inline String_t* get_id_13() const { return ___id_13; }
	inline String_t** get_address_of_id_13() { return &___id_13; }
	inline void set_id_13(String_t* value)
	{
		___id_13 = value;
		Il2CppCodeGenWriteBarrier((&___id_13), value);
	}

	inline static int32_t get_offset_of_schema_14() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9, ___schema_14)); }
	inline XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * get_schema_14() const { return ___schema_14; }
	inline XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 ** get_address_of_schema_14() { return &___schema_14; }
	inline void set_schema_14(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * value)
	{
		___schema_14 = value;
		Il2CppCodeGenWriteBarrier((&___schema_14), value);
	}

	inline static int32_t get_offset_of_location_15() { return static_cast<int32_t>(offsetof(XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9, ___location_15)); }
	inline String_t* get_location_15() const { return ___location_15; }
	inline String_t** get_address_of_location_15() { return &___location_15; }
	inline void set_location_15(String_t* value)
	{
		___location_15 = value;
		Il2CppCodeGenWriteBarrier((&___location_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAEXTERNAL_TF75596B2A753B85E30D6F9675F05E107D9A287D9_H
#ifndef XMLSTREAMREADER_T86B59C766839F8959042F327B7D4A2A3DBF9E27F_H
#define XMLSTREAMREADER_T86B59C766839F8959042F327B7D4A2A3DBF9E27F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlStreamReader
struct  XmlStreamReader_t86B59C766839F8959042F327B7D4A2A3DBF9E27F  : public NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8
{
public:
	// System.Xml.XmlInputStream System.Xml.XmlStreamReader::input
	XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629 * ___input_13;

public:
	inline static int32_t get_offset_of_input_13() { return static_cast<int32_t>(offsetof(XmlStreamReader_t86B59C766839F8959042F327B7D4A2A3DBF9E27F, ___input_13)); }
	inline XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629 * get_input_13() const { return ___input_13; }
	inline XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629 ** get_address_of_input_13() { return &___input_13; }
	inline void set_input_13(XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629 * value)
	{
		___input_13 = value;
		Il2CppCodeGenWriteBarrier((&___input_13), value);
	}
};

struct XmlStreamReader_t86B59C766839F8959042F327B7D4A2A3DBF9E27F_StaticFields
{
public:
	// System.Xml.XmlException System.Xml.XmlStreamReader::invalidDataException
	XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1 * ___invalidDataException_14;

public:
	inline static int32_t get_offset_of_invalidDataException_14() { return static_cast<int32_t>(offsetof(XmlStreamReader_t86B59C766839F8959042F327B7D4A2A3DBF9E27F_StaticFields, ___invalidDataException_14)); }
	inline XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1 * get_invalidDataException_14() const { return ___invalidDataException_14; }
	inline XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1 ** get_address_of_invalidDataException_14() { return &___invalidDataException_14; }
	inline void set_invalidDataException_14(XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1 * value)
	{
		___invalidDataException_14 = value;
		Il2CppCodeGenWriteBarrier((&___invalidDataException_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSTREAMREADER_T86B59C766839F8959042F327B7D4A2A3DBF9E27F_H
#ifndef XMLTEXTWRITER_TBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_H
#define XMLTEXTWRITER_TBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter
struct  XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1  : public XmlWriter_tF67065A3B83E8B7E2B24D172D8223F4328F437B6
{
public:
	// System.IO.Stream System.Xml.XmlTextWriter::base_stream
	Stream_tCFD27E43C18381861212C0288CACF467FB602009 * ___base_stream_3;
	// System.IO.TextWriter System.Xml.XmlTextWriter::source
	TextWriter_t584EB24550495420EF2EC2E56D5404D0A0A79A45 * ___source_4;
	// System.IO.TextWriter System.Xml.XmlTextWriter::writer
	TextWriter_t584EB24550495420EF2EC2E56D5404D0A0A79A45 * ___writer_5;
	// System.IO.StringWriter System.Xml.XmlTextWriter::preserver
	StringWriter_t3C72AF79544098A9167A3748E65721DB7ED2960D * ___preserver_6;
	// System.String System.Xml.XmlTextWriter::preserved_name
	String_t* ___preserved_name_7;
	// System.Boolean System.Xml.XmlTextWriter::is_preserved_xmlns
	bool ___is_preserved_xmlns_8;
	// System.Boolean System.Xml.XmlTextWriter::allow_doc_fragment
	bool ___allow_doc_fragment_9;
	// System.Boolean System.Xml.XmlTextWriter::close_output_stream
	bool ___close_output_stream_10;
	// System.Boolean System.Xml.XmlTextWriter::ignore_encoding
	bool ___ignore_encoding_11;
	// System.Boolean System.Xml.XmlTextWriter::namespaces
	bool ___namespaces_12;
	// System.Xml.XmlTextWriter_XmlDeclState System.Xml.XmlTextWriter::xmldecl_state
	int32_t ___xmldecl_state_13;
	// System.Boolean System.Xml.XmlTextWriter::check_character_validity
	bool ___check_character_validity_14;
	// System.Xml.NewLineHandling System.Xml.XmlTextWriter::newline_handling
	int32_t ___newline_handling_15;
	// System.Boolean System.Xml.XmlTextWriter::is_document_entity
	bool ___is_document_entity_16;
	// System.Xml.WriteState System.Xml.XmlTextWriter::state
	int32_t ___state_17;
	// System.Xml.XmlNodeType System.Xml.XmlTextWriter::node_state
	int32_t ___node_state_18;
	// System.Xml.XmlNamespaceManager System.Xml.XmlTextWriter::nsmanager
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * ___nsmanager_19;
	// System.Int32 System.Xml.XmlTextWriter::open_count
	int32_t ___open_count_20;
	// System.Xml.XmlTextWriter_XmlNodeInfo[] System.Xml.XmlTextWriter::elements
	XmlNodeInfoU5BU5D_tCB7829A10CA8B41C2A87C0CFE790ADD724A1D80D* ___elements_21;
	// System.Collections.Stack System.Xml.XmlTextWriter::new_local_namespaces
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___new_local_namespaces_22;
	// System.Collections.ArrayList System.Xml.XmlTextWriter::explicit_nsdecls
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___explicit_nsdecls_23;
	// System.Xml.NamespaceHandling System.Xml.XmlTextWriter::namespace_handling
	int32_t ___namespace_handling_24;
	// System.Boolean System.Xml.XmlTextWriter::indent
	bool ___indent_25;
	// System.Int32 System.Xml.XmlTextWriter::indent_count
	int32_t ___indent_count_26;
	// System.Char System.Xml.XmlTextWriter::indent_char
	Il2CppChar ___indent_char_27;
	// System.String System.Xml.XmlTextWriter::indent_string
	String_t* ___indent_string_28;
	// System.String System.Xml.XmlTextWriter::newline
	String_t* ___newline_29;
	// System.Boolean System.Xml.XmlTextWriter::indent_attributes
	bool ___indent_attributes_30;
	// System.Char System.Xml.XmlTextWriter::quote_char
	Il2CppChar ___quote_char_31;
	// System.Boolean System.Xml.XmlTextWriter::v2
	bool ___v2_32;

public:
	inline static int32_t get_offset_of_base_stream_3() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___base_stream_3)); }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 * get_base_stream_3() const { return ___base_stream_3; }
	inline Stream_tCFD27E43C18381861212C0288CACF467FB602009 ** get_address_of_base_stream_3() { return &___base_stream_3; }
	inline void set_base_stream_3(Stream_tCFD27E43C18381861212C0288CACF467FB602009 * value)
	{
		___base_stream_3 = value;
		Il2CppCodeGenWriteBarrier((&___base_stream_3), value);
	}

	inline static int32_t get_offset_of_source_4() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___source_4)); }
	inline TextWriter_t584EB24550495420EF2EC2E56D5404D0A0A79A45 * get_source_4() const { return ___source_4; }
	inline TextWriter_t584EB24550495420EF2EC2E56D5404D0A0A79A45 ** get_address_of_source_4() { return &___source_4; }
	inline void set_source_4(TextWriter_t584EB24550495420EF2EC2E56D5404D0A0A79A45 * value)
	{
		___source_4 = value;
		Il2CppCodeGenWriteBarrier((&___source_4), value);
	}

	inline static int32_t get_offset_of_writer_5() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___writer_5)); }
	inline TextWriter_t584EB24550495420EF2EC2E56D5404D0A0A79A45 * get_writer_5() const { return ___writer_5; }
	inline TextWriter_t584EB24550495420EF2EC2E56D5404D0A0A79A45 ** get_address_of_writer_5() { return &___writer_5; }
	inline void set_writer_5(TextWriter_t584EB24550495420EF2EC2E56D5404D0A0A79A45 * value)
	{
		___writer_5 = value;
		Il2CppCodeGenWriteBarrier((&___writer_5), value);
	}

	inline static int32_t get_offset_of_preserver_6() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___preserver_6)); }
	inline StringWriter_t3C72AF79544098A9167A3748E65721DB7ED2960D * get_preserver_6() const { return ___preserver_6; }
	inline StringWriter_t3C72AF79544098A9167A3748E65721DB7ED2960D ** get_address_of_preserver_6() { return &___preserver_6; }
	inline void set_preserver_6(StringWriter_t3C72AF79544098A9167A3748E65721DB7ED2960D * value)
	{
		___preserver_6 = value;
		Il2CppCodeGenWriteBarrier((&___preserver_6), value);
	}

	inline static int32_t get_offset_of_preserved_name_7() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___preserved_name_7)); }
	inline String_t* get_preserved_name_7() const { return ___preserved_name_7; }
	inline String_t** get_address_of_preserved_name_7() { return &___preserved_name_7; }
	inline void set_preserved_name_7(String_t* value)
	{
		___preserved_name_7 = value;
		Il2CppCodeGenWriteBarrier((&___preserved_name_7), value);
	}

	inline static int32_t get_offset_of_is_preserved_xmlns_8() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___is_preserved_xmlns_8)); }
	inline bool get_is_preserved_xmlns_8() const { return ___is_preserved_xmlns_8; }
	inline bool* get_address_of_is_preserved_xmlns_8() { return &___is_preserved_xmlns_8; }
	inline void set_is_preserved_xmlns_8(bool value)
	{
		___is_preserved_xmlns_8 = value;
	}

	inline static int32_t get_offset_of_allow_doc_fragment_9() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___allow_doc_fragment_9)); }
	inline bool get_allow_doc_fragment_9() const { return ___allow_doc_fragment_9; }
	inline bool* get_address_of_allow_doc_fragment_9() { return &___allow_doc_fragment_9; }
	inline void set_allow_doc_fragment_9(bool value)
	{
		___allow_doc_fragment_9 = value;
	}

	inline static int32_t get_offset_of_close_output_stream_10() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___close_output_stream_10)); }
	inline bool get_close_output_stream_10() const { return ___close_output_stream_10; }
	inline bool* get_address_of_close_output_stream_10() { return &___close_output_stream_10; }
	inline void set_close_output_stream_10(bool value)
	{
		___close_output_stream_10 = value;
	}

	inline static int32_t get_offset_of_ignore_encoding_11() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___ignore_encoding_11)); }
	inline bool get_ignore_encoding_11() const { return ___ignore_encoding_11; }
	inline bool* get_address_of_ignore_encoding_11() { return &___ignore_encoding_11; }
	inline void set_ignore_encoding_11(bool value)
	{
		___ignore_encoding_11 = value;
	}

	inline static int32_t get_offset_of_namespaces_12() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___namespaces_12)); }
	inline bool get_namespaces_12() const { return ___namespaces_12; }
	inline bool* get_address_of_namespaces_12() { return &___namespaces_12; }
	inline void set_namespaces_12(bool value)
	{
		___namespaces_12 = value;
	}

	inline static int32_t get_offset_of_xmldecl_state_13() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___xmldecl_state_13)); }
	inline int32_t get_xmldecl_state_13() const { return ___xmldecl_state_13; }
	inline int32_t* get_address_of_xmldecl_state_13() { return &___xmldecl_state_13; }
	inline void set_xmldecl_state_13(int32_t value)
	{
		___xmldecl_state_13 = value;
	}

	inline static int32_t get_offset_of_check_character_validity_14() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___check_character_validity_14)); }
	inline bool get_check_character_validity_14() const { return ___check_character_validity_14; }
	inline bool* get_address_of_check_character_validity_14() { return &___check_character_validity_14; }
	inline void set_check_character_validity_14(bool value)
	{
		___check_character_validity_14 = value;
	}

	inline static int32_t get_offset_of_newline_handling_15() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___newline_handling_15)); }
	inline int32_t get_newline_handling_15() const { return ___newline_handling_15; }
	inline int32_t* get_address_of_newline_handling_15() { return &___newline_handling_15; }
	inline void set_newline_handling_15(int32_t value)
	{
		___newline_handling_15 = value;
	}

	inline static int32_t get_offset_of_is_document_entity_16() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___is_document_entity_16)); }
	inline bool get_is_document_entity_16() const { return ___is_document_entity_16; }
	inline bool* get_address_of_is_document_entity_16() { return &___is_document_entity_16; }
	inline void set_is_document_entity_16(bool value)
	{
		___is_document_entity_16 = value;
	}

	inline static int32_t get_offset_of_state_17() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___state_17)); }
	inline int32_t get_state_17() const { return ___state_17; }
	inline int32_t* get_address_of_state_17() { return &___state_17; }
	inline void set_state_17(int32_t value)
	{
		___state_17 = value;
	}

	inline static int32_t get_offset_of_node_state_18() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___node_state_18)); }
	inline int32_t get_node_state_18() const { return ___node_state_18; }
	inline int32_t* get_address_of_node_state_18() { return &___node_state_18; }
	inline void set_node_state_18(int32_t value)
	{
		___node_state_18 = value;
	}

	inline static int32_t get_offset_of_nsmanager_19() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___nsmanager_19)); }
	inline XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * get_nsmanager_19() const { return ___nsmanager_19; }
	inline XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 ** get_address_of_nsmanager_19() { return &___nsmanager_19; }
	inline void set_nsmanager_19(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * value)
	{
		___nsmanager_19 = value;
		Il2CppCodeGenWriteBarrier((&___nsmanager_19), value);
	}

	inline static int32_t get_offset_of_open_count_20() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___open_count_20)); }
	inline int32_t get_open_count_20() const { return ___open_count_20; }
	inline int32_t* get_address_of_open_count_20() { return &___open_count_20; }
	inline void set_open_count_20(int32_t value)
	{
		___open_count_20 = value;
	}

	inline static int32_t get_offset_of_elements_21() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___elements_21)); }
	inline XmlNodeInfoU5BU5D_tCB7829A10CA8B41C2A87C0CFE790ADD724A1D80D* get_elements_21() const { return ___elements_21; }
	inline XmlNodeInfoU5BU5D_tCB7829A10CA8B41C2A87C0CFE790ADD724A1D80D** get_address_of_elements_21() { return &___elements_21; }
	inline void set_elements_21(XmlNodeInfoU5BU5D_tCB7829A10CA8B41C2A87C0CFE790ADD724A1D80D* value)
	{
		___elements_21 = value;
		Il2CppCodeGenWriteBarrier((&___elements_21), value);
	}

	inline static int32_t get_offset_of_new_local_namespaces_22() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___new_local_namespaces_22)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_new_local_namespaces_22() const { return ___new_local_namespaces_22; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_new_local_namespaces_22() { return &___new_local_namespaces_22; }
	inline void set_new_local_namespaces_22(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___new_local_namespaces_22 = value;
		Il2CppCodeGenWriteBarrier((&___new_local_namespaces_22), value);
	}

	inline static int32_t get_offset_of_explicit_nsdecls_23() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___explicit_nsdecls_23)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_explicit_nsdecls_23() const { return ___explicit_nsdecls_23; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_explicit_nsdecls_23() { return &___explicit_nsdecls_23; }
	inline void set_explicit_nsdecls_23(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___explicit_nsdecls_23 = value;
		Il2CppCodeGenWriteBarrier((&___explicit_nsdecls_23), value);
	}

	inline static int32_t get_offset_of_namespace_handling_24() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___namespace_handling_24)); }
	inline int32_t get_namespace_handling_24() const { return ___namespace_handling_24; }
	inline int32_t* get_address_of_namespace_handling_24() { return &___namespace_handling_24; }
	inline void set_namespace_handling_24(int32_t value)
	{
		___namespace_handling_24 = value;
	}

	inline static int32_t get_offset_of_indent_25() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___indent_25)); }
	inline bool get_indent_25() const { return ___indent_25; }
	inline bool* get_address_of_indent_25() { return &___indent_25; }
	inline void set_indent_25(bool value)
	{
		___indent_25 = value;
	}

	inline static int32_t get_offset_of_indent_count_26() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___indent_count_26)); }
	inline int32_t get_indent_count_26() const { return ___indent_count_26; }
	inline int32_t* get_address_of_indent_count_26() { return &___indent_count_26; }
	inline void set_indent_count_26(int32_t value)
	{
		___indent_count_26 = value;
	}

	inline static int32_t get_offset_of_indent_char_27() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___indent_char_27)); }
	inline Il2CppChar get_indent_char_27() const { return ___indent_char_27; }
	inline Il2CppChar* get_address_of_indent_char_27() { return &___indent_char_27; }
	inline void set_indent_char_27(Il2CppChar value)
	{
		___indent_char_27 = value;
	}

	inline static int32_t get_offset_of_indent_string_28() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___indent_string_28)); }
	inline String_t* get_indent_string_28() const { return ___indent_string_28; }
	inline String_t** get_address_of_indent_string_28() { return &___indent_string_28; }
	inline void set_indent_string_28(String_t* value)
	{
		___indent_string_28 = value;
		Il2CppCodeGenWriteBarrier((&___indent_string_28), value);
	}

	inline static int32_t get_offset_of_newline_29() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___newline_29)); }
	inline String_t* get_newline_29() const { return ___newline_29; }
	inline String_t** get_address_of_newline_29() { return &___newline_29; }
	inline void set_newline_29(String_t* value)
	{
		___newline_29 = value;
		Il2CppCodeGenWriteBarrier((&___newline_29), value);
	}

	inline static int32_t get_offset_of_indent_attributes_30() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___indent_attributes_30)); }
	inline bool get_indent_attributes_30() const { return ___indent_attributes_30; }
	inline bool* get_address_of_indent_attributes_30() { return &___indent_attributes_30; }
	inline void set_indent_attributes_30(bool value)
	{
		___indent_attributes_30 = value;
	}

	inline static int32_t get_offset_of_quote_char_31() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___quote_char_31)); }
	inline Il2CppChar get_quote_char_31() const { return ___quote_char_31; }
	inline Il2CppChar* get_address_of_quote_char_31() { return &___quote_char_31; }
	inline void set_quote_char_31(Il2CppChar value)
	{
		___quote_char_31 = value;
	}

	inline static int32_t get_offset_of_v2_32() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1, ___v2_32)); }
	inline bool get_v2_32() const { return ___v2_32; }
	inline bool* get_address_of_v2_32() { return &___v2_32; }
	inline void set_v2_32(bool value)
	{
		___v2_32 = value;
	}
};

struct XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_StaticFields
{
public:
	// System.Text.Encoding System.Xml.XmlTextWriter::unmarked_utf8encoding
	Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * ___unmarked_utf8encoding_0;
	// System.Char[] System.Xml.XmlTextWriter::escaped_text_chars
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___escaped_text_chars_1;
	// System.Char[] System.Xml.XmlTextWriter::escaped_attr_chars
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___escaped_attr_chars_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlTextWriter::<>f__switchU24map3A
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map3A_33;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlTextWriter::<>f__switchU24map3B
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map3B_34;

public:
	inline static int32_t get_offset_of_unmarked_utf8encoding_0() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_StaticFields, ___unmarked_utf8encoding_0)); }
	inline Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * get_unmarked_utf8encoding_0() const { return ___unmarked_utf8encoding_0; }
	inline Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 ** get_address_of_unmarked_utf8encoding_0() { return &___unmarked_utf8encoding_0; }
	inline void set_unmarked_utf8encoding_0(Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * value)
	{
		___unmarked_utf8encoding_0 = value;
		Il2CppCodeGenWriteBarrier((&___unmarked_utf8encoding_0), value);
	}

	inline static int32_t get_offset_of_escaped_text_chars_1() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_StaticFields, ___escaped_text_chars_1)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_escaped_text_chars_1() const { return ___escaped_text_chars_1; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_escaped_text_chars_1() { return &___escaped_text_chars_1; }
	inline void set_escaped_text_chars_1(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___escaped_text_chars_1 = value;
		Il2CppCodeGenWriteBarrier((&___escaped_text_chars_1), value);
	}

	inline static int32_t get_offset_of_escaped_attr_chars_2() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_StaticFields, ___escaped_attr_chars_2)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_escaped_attr_chars_2() const { return ___escaped_attr_chars_2; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_escaped_attr_chars_2() { return &___escaped_attr_chars_2; }
	inline void set_escaped_attr_chars_2(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___escaped_attr_chars_2 = value;
		Il2CppCodeGenWriteBarrier((&___escaped_attr_chars_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3A_33() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_StaticFields, ___U3CU3Ef__switchU24map3A_33)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map3A_33() const { return ___U3CU3Ef__switchU24map3A_33; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map3A_33() { return &___U3CU3Ef__switchU24map3A_33; }
	inline void set_U3CU3Ef__switchU24map3A_33(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map3A_33 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3A_33), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map3B_34() { return static_cast<int32_t>(offsetof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_StaticFields, ___U3CU3Ef__switchU24map3B_34)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map3B_34() const { return ___U3CU3Ef__switchU24map3B_34; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map3B_34() { return &___U3CU3Ef__switchU24map3B_34; }
	inline void set_U3CU3Ef__switchU24map3B_34(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map3B_34 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map3B_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTWRITER_TBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_H
#ifndef XMLNODEINFO_TB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3_H
#define XMLNODEINFO_TB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter_XmlNodeInfo
struct  XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlTextWriter_XmlNodeInfo::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlTextWriter_XmlNodeInfo::LocalName
	String_t* ___LocalName_1;
	// System.String System.Xml.XmlTextWriter_XmlNodeInfo::NS
	String_t* ___NS_2;
	// System.Boolean System.Xml.XmlTextWriter_XmlNodeInfo::HasSimple
	bool ___HasSimple_3;
	// System.Boolean System.Xml.XmlTextWriter_XmlNodeInfo::HasElements
	bool ___HasElements_4;
	// System.String System.Xml.XmlTextWriter_XmlNodeInfo::XmlLang
	String_t* ___XmlLang_5;
	// System.Xml.XmlSpace System.Xml.XmlTextWriter_XmlNodeInfo::XmlSpace
	int32_t ___XmlSpace_6;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_NS_2() { return static_cast<int32_t>(offsetof(XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3, ___NS_2)); }
	inline String_t* get_NS_2() const { return ___NS_2; }
	inline String_t** get_address_of_NS_2() { return &___NS_2; }
	inline void set_NS_2(String_t* value)
	{
		___NS_2 = value;
		Il2CppCodeGenWriteBarrier((&___NS_2), value);
	}

	inline static int32_t get_offset_of_HasSimple_3() { return static_cast<int32_t>(offsetof(XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3, ___HasSimple_3)); }
	inline bool get_HasSimple_3() const { return ___HasSimple_3; }
	inline bool* get_address_of_HasSimple_3() { return &___HasSimple_3; }
	inline void set_HasSimple_3(bool value)
	{
		___HasSimple_3 = value;
	}

	inline static int32_t get_offset_of_HasElements_4() { return static_cast<int32_t>(offsetof(XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3, ___HasElements_4)); }
	inline bool get_HasElements_4() const { return ___HasElements_4; }
	inline bool* get_address_of_HasElements_4() { return &___HasElements_4; }
	inline void set_HasElements_4(bool value)
	{
		___HasElements_4 = value;
	}

	inline static int32_t get_offset_of_XmlLang_5() { return static_cast<int32_t>(offsetof(XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3, ___XmlLang_5)); }
	inline String_t* get_XmlLang_5() const { return ___XmlLang_5; }
	inline String_t** get_address_of_XmlLang_5() { return &___XmlLang_5; }
	inline void set_XmlLang_5(String_t* value)
	{
		___XmlLang_5 = value;
		Il2CppCodeGenWriteBarrier((&___XmlLang_5), value);
	}

	inline static int32_t get_offset_of_XmlSpace_6() { return static_cast<int32_t>(offsetof(XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3, ___XmlSpace_6)); }
	inline int32_t get_XmlSpace_6() const { return ___XmlSpace_6; }
	inline int32_t* get_address_of_XmlSpace_6() { return &___XmlSpace_6; }
	inline void set_XmlSpace_6(int32_t value)
	{
		___XmlSpace_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEINFO_TB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3_H
#ifndef XSDANYSIMPLETYPE_T457003328EC5C522A2396822982C317B8DC684F0_H
#define XSDANYSIMPLETYPE_T457003328EC5C522A2396822982C317B8DC684F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnySimpleType
struct  XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0  : public XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19
{
public:

public:
};

struct XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields
{
public:
	// Mono.Xml.Schema.XsdAnySimpleType Mono.Xml.Schema.XsdAnySimpleType::instance
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * ___instance_55;
	// System.Char[] Mono.Xml.Schema.XsdAnySimpleType::whitespaceArray
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___whitespaceArray_56;
	// System.Xml.Schema.XmlSchemaFacet_Facet Mono.Xml.Schema.XsdAnySimpleType::booleanAllowedFacets
	int32_t ___booleanAllowedFacets_57;
	// System.Xml.Schema.XmlSchemaFacet_Facet Mono.Xml.Schema.XsdAnySimpleType::decimalAllowedFacets
	int32_t ___decimalAllowedFacets_58;
	// System.Xml.Schema.XmlSchemaFacet_Facet Mono.Xml.Schema.XsdAnySimpleType::durationAllowedFacets
	int32_t ___durationAllowedFacets_59;
	// System.Xml.Schema.XmlSchemaFacet_Facet Mono.Xml.Schema.XsdAnySimpleType::stringAllowedFacets
	int32_t ___stringAllowedFacets_60;

public:
	inline static int32_t get_offset_of_instance_55() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields, ___instance_55)); }
	inline XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * get_instance_55() const { return ___instance_55; }
	inline XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 ** get_address_of_instance_55() { return &___instance_55; }
	inline void set_instance_55(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0 * value)
	{
		___instance_55 = value;
		Il2CppCodeGenWriteBarrier((&___instance_55), value);
	}

	inline static int32_t get_offset_of_whitespaceArray_56() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields, ___whitespaceArray_56)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_whitespaceArray_56() const { return ___whitespaceArray_56; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_whitespaceArray_56() { return &___whitespaceArray_56; }
	inline void set_whitespaceArray_56(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___whitespaceArray_56 = value;
		Il2CppCodeGenWriteBarrier((&___whitespaceArray_56), value);
	}

	inline static int32_t get_offset_of_booleanAllowedFacets_57() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields, ___booleanAllowedFacets_57)); }
	inline int32_t get_booleanAllowedFacets_57() const { return ___booleanAllowedFacets_57; }
	inline int32_t* get_address_of_booleanAllowedFacets_57() { return &___booleanAllowedFacets_57; }
	inline void set_booleanAllowedFacets_57(int32_t value)
	{
		___booleanAllowedFacets_57 = value;
	}

	inline static int32_t get_offset_of_decimalAllowedFacets_58() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields, ___decimalAllowedFacets_58)); }
	inline int32_t get_decimalAllowedFacets_58() const { return ___decimalAllowedFacets_58; }
	inline int32_t* get_address_of_decimalAllowedFacets_58() { return &___decimalAllowedFacets_58; }
	inline void set_decimalAllowedFacets_58(int32_t value)
	{
		___decimalAllowedFacets_58 = value;
	}

	inline static int32_t get_offset_of_durationAllowedFacets_59() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields, ___durationAllowedFacets_59)); }
	inline int32_t get_durationAllowedFacets_59() const { return ___durationAllowedFacets_59; }
	inline int32_t* get_address_of_durationAllowedFacets_59() { return &___durationAllowedFacets_59; }
	inline void set_durationAllowedFacets_59(int32_t value)
	{
		___durationAllowedFacets_59 = value;
	}

	inline static int32_t get_offset_of_stringAllowedFacets_60() { return static_cast<int32_t>(offsetof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields, ___stringAllowedFacets_60)); }
	inline int32_t get_stringAllowedFacets_60() const { return ___stringAllowedFacets_60; }
	inline int32_t* get_address_of_stringAllowedFacets_60() { return &___stringAllowedFacets_60; }
	inline void set_stringAllowedFacets_60(int32_t value)
	{
		___stringAllowedFacets_60 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYSIMPLETYPE_T457003328EC5C522A2396822982C317B8DC684F0_H
#ifndef XMLSCHEMAANYATTRIBUTE_T8471C64949072A71089D20A4E590B63FCCA58FB3_H
#define XMLSCHEMAANYATTRIBUTE_T8471C64949072A71089D20A4E590B63FCCA58FB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAnyAttribute
struct  XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.String System.Xml.Schema.XmlSchemaAnyAttribute::nameSpace
	String_t* ___nameSpace_16;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.XmlSchemaAnyAttribute::processing
	int32_t ___processing_17;
	// Mono.Xml.Schema.XsdWildcard System.Xml.Schema.XmlSchemaAnyAttribute::wildcard
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7 * ___wildcard_18;

public:
	inline static int32_t get_offset_of_nameSpace_16() { return static_cast<int32_t>(offsetof(XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3, ___nameSpace_16)); }
	inline String_t* get_nameSpace_16() const { return ___nameSpace_16; }
	inline String_t** get_address_of_nameSpace_16() { return &___nameSpace_16; }
	inline void set_nameSpace_16(String_t* value)
	{
		___nameSpace_16 = value;
		Il2CppCodeGenWriteBarrier((&___nameSpace_16), value);
	}

	inline static int32_t get_offset_of_processing_17() { return static_cast<int32_t>(offsetof(XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3, ___processing_17)); }
	inline int32_t get_processing_17() const { return ___processing_17; }
	inline int32_t* get_address_of_processing_17() { return &___processing_17; }
	inline void set_processing_17(int32_t value)
	{
		___processing_17 = value;
	}

	inline static int32_t get_offset_of_wildcard_18() { return static_cast<int32_t>(offsetof(XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3, ___wildcard_18)); }
	inline XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7 * get_wildcard_18() const { return ___wildcard_18; }
	inline XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7 ** get_address_of_wildcard_18() { return &___wildcard_18; }
	inline void set_wildcard_18(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7 * value)
	{
		___wildcard_18 = value;
		Il2CppCodeGenWriteBarrier((&___wildcard_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANYATTRIBUTE_T8471C64949072A71089D20A4E590B63FCCA58FB3_H
#ifndef XMLSCHEMAATTRIBUTE_T6D32797FC1FC699E9DC42825A31EF1F99DCD0A77_H
#define XMLSCHEMAATTRIBUTE_T6D32797FC1FC699E9DC42825A31EF1F99DCD0A77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAttribute
struct  XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Object System.Xml.Schema.XmlSchemaAttribute::attributeType
	RuntimeObject * ___attributeType_16;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaAttribute::attributeSchemaType
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___attributeSchemaType_17;
	// System.String System.Xml.Schema.XmlSchemaAttribute::defaultValue
	String_t* ___defaultValue_18;
	// System.String System.Xml.Schema.XmlSchemaAttribute::fixedValue
	String_t* ___fixedValue_19;
	// System.String System.Xml.Schema.XmlSchemaAttribute::validatedDefaultValue
	String_t* ___validatedDefaultValue_20;
	// System.String System.Xml.Schema.XmlSchemaAttribute::validatedFixedValue
	String_t* ___validatedFixedValue_21;
	// System.Object System.Xml.Schema.XmlSchemaAttribute::validatedFixedTypedValue
	RuntimeObject * ___validatedFixedTypedValue_22;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchemaAttribute::form
	int32_t ___form_23;
	// System.String System.Xml.Schema.XmlSchemaAttribute::name
	String_t* ___name_24;
	// System.String System.Xml.Schema.XmlSchemaAttribute::targetNamespace
	String_t* ___targetNamespace_25;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttribute::qualifiedName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___qualifiedName_26;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttribute::refName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___refName_27;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.XmlSchemaAttribute::schemaType
	XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * ___schemaType_28;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttribute::schemaTypeName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___schemaTypeName_29;
	// System.Xml.Schema.XmlSchemaUse System.Xml.Schema.XmlSchemaAttribute::use
	int32_t ___use_30;
	// System.Xml.Schema.XmlSchemaUse System.Xml.Schema.XmlSchemaAttribute::validatedUse
	int32_t ___validatedUse_31;
	// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.XmlSchemaAttribute::referencedAttribute
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77 * ___referencedAttribute_32;

public:
	inline static int32_t get_offset_of_attributeType_16() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___attributeType_16)); }
	inline RuntimeObject * get_attributeType_16() const { return ___attributeType_16; }
	inline RuntimeObject ** get_address_of_attributeType_16() { return &___attributeType_16; }
	inline void set_attributeType_16(RuntimeObject * value)
	{
		___attributeType_16 = value;
		Il2CppCodeGenWriteBarrier((&___attributeType_16), value);
	}

	inline static int32_t get_offset_of_attributeSchemaType_17() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___attributeSchemaType_17)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_attributeSchemaType_17() const { return ___attributeSchemaType_17; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_attributeSchemaType_17() { return &___attributeSchemaType_17; }
	inline void set_attributeSchemaType_17(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___attributeSchemaType_17 = value;
		Il2CppCodeGenWriteBarrier((&___attributeSchemaType_17), value);
	}

	inline static int32_t get_offset_of_defaultValue_18() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___defaultValue_18)); }
	inline String_t* get_defaultValue_18() const { return ___defaultValue_18; }
	inline String_t** get_address_of_defaultValue_18() { return &___defaultValue_18; }
	inline void set_defaultValue_18(String_t* value)
	{
		___defaultValue_18 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_18), value);
	}

	inline static int32_t get_offset_of_fixedValue_19() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___fixedValue_19)); }
	inline String_t* get_fixedValue_19() const { return ___fixedValue_19; }
	inline String_t** get_address_of_fixedValue_19() { return &___fixedValue_19; }
	inline void set_fixedValue_19(String_t* value)
	{
		___fixedValue_19 = value;
		Il2CppCodeGenWriteBarrier((&___fixedValue_19), value);
	}

	inline static int32_t get_offset_of_validatedDefaultValue_20() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___validatedDefaultValue_20)); }
	inline String_t* get_validatedDefaultValue_20() const { return ___validatedDefaultValue_20; }
	inline String_t** get_address_of_validatedDefaultValue_20() { return &___validatedDefaultValue_20; }
	inline void set_validatedDefaultValue_20(String_t* value)
	{
		___validatedDefaultValue_20 = value;
		Il2CppCodeGenWriteBarrier((&___validatedDefaultValue_20), value);
	}

	inline static int32_t get_offset_of_validatedFixedValue_21() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___validatedFixedValue_21)); }
	inline String_t* get_validatedFixedValue_21() const { return ___validatedFixedValue_21; }
	inline String_t** get_address_of_validatedFixedValue_21() { return &___validatedFixedValue_21; }
	inline void set_validatedFixedValue_21(String_t* value)
	{
		___validatedFixedValue_21 = value;
		Il2CppCodeGenWriteBarrier((&___validatedFixedValue_21), value);
	}

	inline static int32_t get_offset_of_validatedFixedTypedValue_22() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___validatedFixedTypedValue_22)); }
	inline RuntimeObject * get_validatedFixedTypedValue_22() const { return ___validatedFixedTypedValue_22; }
	inline RuntimeObject ** get_address_of_validatedFixedTypedValue_22() { return &___validatedFixedTypedValue_22; }
	inline void set_validatedFixedTypedValue_22(RuntimeObject * value)
	{
		___validatedFixedTypedValue_22 = value;
		Il2CppCodeGenWriteBarrier((&___validatedFixedTypedValue_22), value);
	}

	inline static int32_t get_offset_of_form_23() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___form_23)); }
	inline int32_t get_form_23() const { return ___form_23; }
	inline int32_t* get_address_of_form_23() { return &___form_23; }
	inline void set_form_23(int32_t value)
	{
		___form_23 = value;
	}

	inline static int32_t get_offset_of_name_24() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___name_24)); }
	inline String_t* get_name_24() const { return ___name_24; }
	inline String_t** get_address_of_name_24() { return &___name_24; }
	inline void set_name_24(String_t* value)
	{
		___name_24 = value;
		Il2CppCodeGenWriteBarrier((&___name_24), value);
	}

	inline static int32_t get_offset_of_targetNamespace_25() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___targetNamespace_25)); }
	inline String_t* get_targetNamespace_25() const { return ___targetNamespace_25; }
	inline String_t** get_address_of_targetNamespace_25() { return &___targetNamespace_25; }
	inline void set_targetNamespace_25(String_t* value)
	{
		___targetNamespace_25 = value;
		Il2CppCodeGenWriteBarrier((&___targetNamespace_25), value);
	}

	inline static int32_t get_offset_of_qualifiedName_26() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___qualifiedName_26)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_qualifiedName_26() const { return ___qualifiedName_26; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_qualifiedName_26() { return &___qualifiedName_26; }
	inline void set_qualifiedName_26(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___qualifiedName_26 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedName_26), value);
	}

	inline static int32_t get_offset_of_refName_27() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___refName_27)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_refName_27() const { return ___refName_27; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_refName_27() { return &___refName_27; }
	inline void set_refName_27(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___refName_27 = value;
		Il2CppCodeGenWriteBarrier((&___refName_27), value);
	}

	inline static int32_t get_offset_of_schemaType_28() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___schemaType_28)); }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * get_schemaType_28() const { return ___schemaType_28; }
	inline XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 ** get_address_of_schemaType_28() { return &___schemaType_28; }
	inline void set_schemaType_28(XmlSchemaSimpleType_t0750B9C370AB270CCE4C37FD04AC0F7809BAC908 * value)
	{
		___schemaType_28 = value;
		Il2CppCodeGenWriteBarrier((&___schemaType_28), value);
	}

	inline static int32_t get_offset_of_schemaTypeName_29() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___schemaTypeName_29)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_schemaTypeName_29() const { return ___schemaTypeName_29; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_schemaTypeName_29() { return &___schemaTypeName_29; }
	inline void set_schemaTypeName_29(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___schemaTypeName_29 = value;
		Il2CppCodeGenWriteBarrier((&___schemaTypeName_29), value);
	}

	inline static int32_t get_offset_of_use_30() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___use_30)); }
	inline int32_t get_use_30() const { return ___use_30; }
	inline int32_t* get_address_of_use_30() { return &___use_30; }
	inline void set_use_30(int32_t value)
	{
		___use_30 = value;
	}

	inline static int32_t get_offset_of_validatedUse_31() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___validatedUse_31)); }
	inline int32_t get_validatedUse_31() const { return ___validatedUse_31; }
	inline int32_t* get_address_of_validatedUse_31() { return &___validatedUse_31; }
	inline void set_validatedUse_31(int32_t value)
	{
		___validatedUse_31 = value;
	}

	inline static int32_t get_offset_of_referencedAttribute_32() { return static_cast<int32_t>(offsetof(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77, ___referencedAttribute_32)); }
	inline XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77 * get_referencedAttribute_32() const { return ___referencedAttribute_32; }
	inline XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77 ** get_address_of_referencedAttribute_32() { return &___referencedAttribute_32; }
	inline void set_referencedAttribute_32(XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77 * value)
	{
		___referencedAttribute_32 = value;
		Il2CppCodeGenWriteBarrier((&___referencedAttribute_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAATTRIBUTE_T6D32797FC1FC699E9DC42825A31EF1F99DCD0A77_H
#ifndef XMLSCHEMAATTRIBUTEGROUP_T86CC2C0DF0657C90F1F29230939E79B519879923_H
#define XMLSCHEMAATTRIBUTEGROUP_T86CC2C0DF0657C90F1F29230939E79B519879923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAttributeGroup
struct  XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaAttributeGroup::anyAttribute
	XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * ___anyAttribute_16;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaAttributeGroup::attributes
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___attributes_17;
	// System.String System.Xml.Schema.XmlSchemaAttributeGroup::name
	String_t* ___name_18;
	// System.Xml.Schema.XmlSchemaAttributeGroup System.Xml.Schema.XmlSchemaAttributeGroup::redefined
	XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923 * ___redefined_19;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttributeGroup::qualifiedName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___qualifiedName_20;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaAttributeGroup::attributeUses
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___attributeUses_21;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaAttributeGroup::anyAttributeUse
	XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * ___anyAttributeUse_22;
	// System.Boolean System.Xml.Schema.XmlSchemaAttributeGroup::AttributeGroupRecursionCheck
	bool ___AttributeGroupRecursionCheck_23;

public:
	inline static int32_t get_offset_of_anyAttribute_16() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923, ___anyAttribute_16)); }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * get_anyAttribute_16() const { return ___anyAttribute_16; }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 ** get_address_of_anyAttribute_16() { return &___anyAttribute_16; }
	inline void set_anyAttribute_16(XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * value)
	{
		___anyAttribute_16 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttribute_16), value);
	}

	inline static int32_t get_offset_of_attributes_17() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923, ___attributes_17)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_attributes_17() const { return ___attributes_17; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_attributes_17() { return &___attributes_17; }
	inline void set_attributes_17(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___attributes_17 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_17), value);
	}

	inline static int32_t get_offset_of_name_18() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923, ___name_18)); }
	inline String_t* get_name_18() const { return ___name_18; }
	inline String_t** get_address_of_name_18() { return &___name_18; }
	inline void set_name_18(String_t* value)
	{
		___name_18 = value;
		Il2CppCodeGenWriteBarrier((&___name_18), value);
	}

	inline static int32_t get_offset_of_redefined_19() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923, ___redefined_19)); }
	inline XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923 * get_redefined_19() const { return ___redefined_19; }
	inline XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923 ** get_address_of_redefined_19() { return &___redefined_19; }
	inline void set_redefined_19(XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923 * value)
	{
		___redefined_19 = value;
		Il2CppCodeGenWriteBarrier((&___redefined_19), value);
	}

	inline static int32_t get_offset_of_qualifiedName_20() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923, ___qualifiedName_20)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_qualifiedName_20() const { return ___qualifiedName_20; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_qualifiedName_20() { return &___qualifiedName_20; }
	inline void set_qualifiedName_20(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___qualifiedName_20 = value;
		Il2CppCodeGenWriteBarrier((&___qualifiedName_20), value);
	}

	inline static int32_t get_offset_of_attributeUses_21() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923, ___attributeUses_21)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_attributeUses_21() const { return ___attributeUses_21; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_attributeUses_21() { return &___attributeUses_21; }
	inline void set_attributeUses_21(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___attributeUses_21 = value;
		Il2CppCodeGenWriteBarrier((&___attributeUses_21), value);
	}

	inline static int32_t get_offset_of_anyAttributeUse_22() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923, ___anyAttributeUse_22)); }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * get_anyAttributeUse_22() const { return ___anyAttributeUse_22; }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 ** get_address_of_anyAttributeUse_22() { return &___anyAttributeUse_22; }
	inline void set_anyAttributeUse_22(XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * value)
	{
		___anyAttributeUse_22 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttributeUse_22), value);
	}

	inline static int32_t get_offset_of_AttributeGroupRecursionCheck_23() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923, ___AttributeGroupRecursionCheck_23)); }
	inline bool get_AttributeGroupRecursionCheck_23() const { return ___AttributeGroupRecursionCheck_23; }
	inline bool* get_address_of_AttributeGroupRecursionCheck_23() { return &___AttributeGroupRecursionCheck_23; }
	inline void set_AttributeGroupRecursionCheck_23(bool value)
	{
		___AttributeGroupRecursionCheck_23 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAATTRIBUTEGROUP_T86CC2C0DF0657C90F1F29230939E79B519879923_H
#ifndef XMLSCHEMAATTRIBUTEGROUPREF_TF5C13F6E20C66B54A7771EFB157C6A5F25A3491E_H
#define XMLSCHEMAATTRIBUTEGROUPREF_TF5C13F6E20C66B54A7771EFB157C6A5F25A3491E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAttributeGroupRef
struct  XmlSchemaAttributeGroupRef_tF5C13F6E20C66B54A7771EFB157C6A5F25A3491E  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaAttributeGroupRef::refName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___refName_16;

public:
	inline static int32_t get_offset_of_refName_16() { return static_cast<int32_t>(offsetof(XmlSchemaAttributeGroupRef_tF5C13F6E20C66B54A7771EFB157C6A5F25A3491E, ___refName_16)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_refName_16() const { return ___refName_16; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_refName_16() { return &___refName_16; }
	inline void set_refName_16(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___refName_16 = value;
		Il2CppCodeGenWriteBarrier((&___refName_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAATTRIBUTEGROUPREF_TF5C13F6E20C66B54A7771EFB157C6A5F25A3491E_H
#ifndef XMLSCHEMACONTENT_T809DC205F7F607C93C1B078FF8B77BDF4EAD31CA_H
#define XMLSCHEMACONTENT_T809DC205F7F607C93C1B078FF8B77BDF4EAD31CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContent
struct  XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Object System.Xml.Schema.XmlSchemaContent::actualBaseSchemaType
	RuntimeObject * ___actualBaseSchemaType_16;

public:
	inline static int32_t get_offset_of_actualBaseSchemaType_16() { return static_cast<int32_t>(offsetof(XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA, ___actualBaseSchemaType_16)); }
	inline RuntimeObject * get_actualBaseSchemaType_16() const { return ___actualBaseSchemaType_16; }
	inline RuntimeObject ** get_address_of_actualBaseSchemaType_16() { return &___actualBaseSchemaType_16; }
	inline void set_actualBaseSchemaType_16(RuntimeObject * value)
	{
		___actualBaseSchemaType_16 = value;
		Il2CppCodeGenWriteBarrier((&___actualBaseSchemaType_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENT_T809DC205F7F607C93C1B078FF8B77BDF4EAD31CA_H
#ifndef XMLSCHEMACONTENTMODEL_TB057D601009DC1517A2A538E001E592F465568D9_H
#define XMLSCHEMACONTENTMODEL_TB057D601009DC1517A2A538E001E592F465568D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentModel
struct  XmlSchemaContentModel_tB057D601009DC1517A2A538E001E592F465568D9  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTMODEL_TB057D601009DC1517A2A538E001E592F465568D9_H
#ifndef XMLSCHEMAFACET_T44B0D153A389F7A8C0769773496C6D041D8C8318_H
#define XMLSCHEMAFACET_T44B0D153A389F7A8C0769773496C6D041D8C8318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaFacet
struct  XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Boolean System.Xml.Schema.XmlSchemaFacet::isFixed
	bool ___isFixed_17;
	// System.String System.Xml.Schema.XmlSchemaFacet::val
	String_t* ___val_18;

public:
	inline static int32_t get_offset_of_isFixed_17() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318, ___isFixed_17)); }
	inline bool get_isFixed_17() const { return ___isFixed_17; }
	inline bool* get_address_of_isFixed_17() { return &___isFixed_17; }
	inline void set_isFixed_17(bool value)
	{
		___isFixed_17 = value;
	}

	inline static int32_t get_offset_of_val_18() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318, ___val_18)); }
	inline String_t* get_val_18() const { return ___val_18; }
	inline String_t** get_address_of_val_18() { return &___val_18; }
	inline void set_val_18(String_t* value)
	{
		___val_18 = value;
		Il2CppCodeGenWriteBarrier((&___val_18), value);
	}
};

struct XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaFacet_Facet System.Xml.Schema.XmlSchemaFacet::AllFacets
	int32_t ___AllFacets_16;

public:
	inline static int32_t get_offset_of_AllFacets_16() { return static_cast<int32_t>(offsetof(XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318_StaticFields, ___AllFacets_16)); }
	inline int32_t get_AllFacets_16() const { return ___AllFacets_16; }
	inline int32_t* get_address_of_AllFacets_16() { return &___AllFacets_16; }
	inline void set_AllFacets_16(int32_t value)
	{
		___AllFacets_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFACET_T44B0D153A389F7A8C0769773496C6D041D8C8318_H
#ifndef XMLSCHEMAPARTICLE_T7A09A84556A27FD42B9A6F7A933345158E7752BC_H
#define XMLSCHEMAPARTICLE_T7A09A84556A27FD42B9A6F7A933345158E7752BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaParticle
struct  XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::minOccurs
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___minOccurs_16;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::maxOccurs
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___maxOccurs_17;
	// System.String System.Xml.Schema.XmlSchemaParticle::minstr
	String_t* ___minstr_18;
	// System.String System.Xml.Schema.XmlSchemaParticle::maxstr
	String_t* ___maxstr_19;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::validatedMinOccurs
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___validatedMinOccurs_21;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::validatedMaxOccurs
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___validatedMaxOccurs_22;
	// System.Int32 System.Xml.Schema.XmlSchemaParticle::recursionDepth
	int32_t ___recursionDepth_23;
	// System.Decimal System.Xml.Schema.XmlSchemaParticle::minEffectiveTotalRange
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___minEffectiveTotalRange_24;
	// System.Boolean System.Xml.Schema.XmlSchemaParticle::parentIsGroupDefinition
	bool ___parentIsGroupDefinition_25;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaParticle::OptimizedParticle
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * ___OptimizedParticle_26;

public:
	inline static int32_t get_offset_of_minOccurs_16() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___minOccurs_16)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_minOccurs_16() const { return ___minOccurs_16; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_minOccurs_16() { return &___minOccurs_16; }
	inline void set_minOccurs_16(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___minOccurs_16 = value;
	}

	inline static int32_t get_offset_of_maxOccurs_17() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___maxOccurs_17)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_maxOccurs_17() const { return ___maxOccurs_17; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_maxOccurs_17() { return &___maxOccurs_17; }
	inline void set_maxOccurs_17(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___maxOccurs_17 = value;
	}

	inline static int32_t get_offset_of_minstr_18() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___minstr_18)); }
	inline String_t* get_minstr_18() const { return ___minstr_18; }
	inline String_t** get_address_of_minstr_18() { return &___minstr_18; }
	inline void set_minstr_18(String_t* value)
	{
		___minstr_18 = value;
		Il2CppCodeGenWriteBarrier((&___minstr_18), value);
	}

	inline static int32_t get_offset_of_maxstr_19() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___maxstr_19)); }
	inline String_t* get_maxstr_19() const { return ___maxstr_19; }
	inline String_t** get_address_of_maxstr_19() { return &___maxstr_19; }
	inline void set_maxstr_19(String_t* value)
	{
		___maxstr_19 = value;
		Il2CppCodeGenWriteBarrier((&___maxstr_19), value);
	}

	inline static int32_t get_offset_of_validatedMinOccurs_21() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___validatedMinOccurs_21)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_validatedMinOccurs_21() const { return ___validatedMinOccurs_21; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_validatedMinOccurs_21() { return &___validatedMinOccurs_21; }
	inline void set_validatedMinOccurs_21(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___validatedMinOccurs_21 = value;
	}

	inline static int32_t get_offset_of_validatedMaxOccurs_22() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___validatedMaxOccurs_22)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_validatedMaxOccurs_22() const { return ___validatedMaxOccurs_22; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_validatedMaxOccurs_22() { return &___validatedMaxOccurs_22; }
	inline void set_validatedMaxOccurs_22(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___validatedMaxOccurs_22 = value;
	}

	inline static int32_t get_offset_of_recursionDepth_23() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___recursionDepth_23)); }
	inline int32_t get_recursionDepth_23() const { return ___recursionDepth_23; }
	inline int32_t* get_address_of_recursionDepth_23() { return &___recursionDepth_23; }
	inline void set_recursionDepth_23(int32_t value)
	{
		___recursionDepth_23 = value;
	}

	inline static int32_t get_offset_of_minEffectiveTotalRange_24() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___minEffectiveTotalRange_24)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_minEffectiveTotalRange_24() const { return ___minEffectiveTotalRange_24; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_minEffectiveTotalRange_24() { return &___minEffectiveTotalRange_24; }
	inline void set_minEffectiveTotalRange_24(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___minEffectiveTotalRange_24 = value;
	}

	inline static int32_t get_offset_of_parentIsGroupDefinition_25() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___parentIsGroupDefinition_25)); }
	inline bool get_parentIsGroupDefinition_25() const { return ___parentIsGroupDefinition_25; }
	inline bool* get_address_of_parentIsGroupDefinition_25() { return &___parentIsGroupDefinition_25; }
	inline void set_parentIsGroupDefinition_25(bool value)
	{
		___parentIsGroupDefinition_25 = value;
	}

	inline static int32_t get_offset_of_OptimizedParticle_26() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC, ___OptimizedParticle_26)); }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * get_OptimizedParticle_26() const { return ___OptimizedParticle_26; }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC ** get_address_of_OptimizedParticle_26() { return &___OptimizedParticle_26; }
	inline void set_OptimizedParticle_26(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * value)
	{
		___OptimizedParticle_26 = value;
		Il2CppCodeGenWriteBarrier((&___OptimizedParticle_26), value);
	}
};

struct XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaParticle::empty
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * ___empty_20;

public:
	inline static int32_t get_offset_of_empty_20() { return static_cast<int32_t>(offsetof(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC_StaticFields, ___empty_20)); }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * get_empty_20() const { return ___empty_20; }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC ** get_address_of_empty_20() { return &___empty_20; }
	inline void set_empty_20(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * value)
	{
		___empty_20 = value;
		Il2CppCodeGenWriteBarrier((&___empty_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAPARTICLE_T7A09A84556A27FD42B9A6F7A933345158E7752BC_H
#ifndef XMLSCHEMATYPE_TDF0987DC710BC7BED17E5008774575FA2E6647AE_H
#define XMLSCHEMATYPE_TDF0987DC710BC7BED17E5008774575FA2E6647AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaType
struct  XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE  : public XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741
{
public:
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::final
	int32_t ___final_16;
	// System.Boolean System.Xml.Schema.XmlSchemaType::isMixed
	bool ___isMixed_17;
	// System.String System.Xml.Schema.XmlSchemaType::name
	String_t* ___name_18;
	// System.Boolean System.Xml.Schema.XmlSchemaType::recursed
	bool ___recursed_19;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaType::BaseSchemaTypeName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___BaseSchemaTypeName_20;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaType::BaseXmlSchemaTypeInternal
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * ___BaseXmlSchemaTypeInternal_21;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.XmlSchemaType::DatatypeInternal
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19 * ___DatatypeInternal_22;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::resolvedDerivedBy
	int32_t ___resolvedDerivedBy_23;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaType::finalResolved
	int32_t ___finalResolved_24;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaType::QNameInternal
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___QNameInternal_25;

public:
	inline static int32_t get_offset_of_final_16() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___final_16)); }
	inline int32_t get_final_16() const { return ___final_16; }
	inline int32_t* get_address_of_final_16() { return &___final_16; }
	inline void set_final_16(int32_t value)
	{
		___final_16 = value;
	}

	inline static int32_t get_offset_of_isMixed_17() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___isMixed_17)); }
	inline bool get_isMixed_17() const { return ___isMixed_17; }
	inline bool* get_address_of_isMixed_17() { return &___isMixed_17; }
	inline void set_isMixed_17(bool value)
	{
		___isMixed_17 = value;
	}

	inline static int32_t get_offset_of_name_18() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___name_18)); }
	inline String_t* get_name_18() const { return ___name_18; }
	inline String_t** get_address_of_name_18() { return &___name_18; }
	inline void set_name_18(String_t* value)
	{
		___name_18 = value;
		Il2CppCodeGenWriteBarrier((&___name_18), value);
	}

	inline static int32_t get_offset_of_recursed_19() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___recursed_19)); }
	inline bool get_recursed_19() const { return ___recursed_19; }
	inline bool* get_address_of_recursed_19() { return &___recursed_19; }
	inline void set_recursed_19(bool value)
	{
		___recursed_19 = value;
	}

	inline static int32_t get_offset_of_BaseSchemaTypeName_20() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___BaseSchemaTypeName_20)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_BaseSchemaTypeName_20() const { return ___BaseSchemaTypeName_20; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_BaseSchemaTypeName_20() { return &___BaseSchemaTypeName_20; }
	inline void set_BaseSchemaTypeName_20(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___BaseSchemaTypeName_20 = value;
		Il2CppCodeGenWriteBarrier((&___BaseSchemaTypeName_20), value);
	}

	inline static int32_t get_offset_of_BaseXmlSchemaTypeInternal_21() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___BaseXmlSchemaTypeInternal_21)); }
	inline XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * get_BaseXmlSchemaTypeInternal_21() const { return ___BaseXmlSchemaTypeInternal_21; }
	inline XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE ** get_address_of_BaseXmlSchemaTypeInternal_21() { return &___BaseXmlSchemaTypeInternal_21; }
	inline void set_BaseXmlSchemaTypeInternal_21(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * value)
	{
		___BaseXmlSchemaTypeInternal_21 = value;
		Il2CppCodeGenWriteBarrier((&___BaseXmlSchemaTypeInternal_21), value);
	}

	inline static int32_t get_offset_of_DatatypeInternal_22() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___DatatypeInternal_22)); }
	inline XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19 * get_DatatypeInternal_22() const { return ___DatatypeInternal_22; }
	inline XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19 ** get_address_of_DatatypeInternal_22() { return &___DatatypeInternal_22; }
	inline void set_DatatypeInternal_22(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19 * value)
	{
		___DatatypeInternal_22 = value;
		Il2CppCodeGenWriteBarrier((&___DatatypeInternal_22), value);
	}

	inline static int32_t get_offset_of_resolvedDerivedBy_23() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___resolvedDerivedBy_23)); }
	inline int32_t get_resolvedDerivedBy_23() const { return ___resolvedDerivedBy_23; }
	inline int32_t* get_address_of_resolvedDerivedBy_23() { return &___resolvedDerivedBy_23; }
	inline void set_resolvedDerivedBy_23(int32_t value)
	{
		___resolvedDerivedBy_23 = value;
	}

	inline static int32_t get_offset_of_finalResolved_24() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___finalResolved_24)); }
	inline int32_t get_finalResolved_24() const { return ___finalResolved_24; }
	inline int32_t* get_address_of_finalResolved_24() { return &___finalResolved_24; }
	inline void set_finalResolved_24(int32_t value)
	{
		___finalResolved_24 = value;
	}

	inline static int32_t get_offset_of_QNameInternal_25() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE, ___QNameInternal_25)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_QNameInternal_25() const { return ___QNameInternal_25; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_QNameInternal_25() { return &___QNameInternal_25; }
	inline void set_QNameInternal_25(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___QNameInternal_25 = value;
		Il2CppCodeGenWriteBarrier((&___QNameInternal_25), value);
	}
};

struct XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaType::<>f__switchU24map42
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map42_26;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.Schema.XmlSchemaType::<>f__switchU24map43
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map43_27;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map42_26() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE_StaticFields, ___U3CU3Ef__switchU24map42_26)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map42_26() const { return ___U3CU3Ef__switchU24map42_26; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map42_26() { return &___U3CU3Ef__switchU24map42_26; }
	inline void set_U3CU3Ef__switchU24map42_26(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map42_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map42_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map43_27() { return static_cast<int32_t>(offsetof(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE_StaticFields, ___U3CU3Ef__switchU24map43_27)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map43_27() const { return ___U3CU3Ef__switchU24map43_27; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map43_27() { return &___U3CU3Ef__switchU24map43_27; }
	inline void set_U3CU3Ef__switchU24map43_27(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map43_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map43_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMATYPE_TDF0987DC710BC7BED17E5008774575FA2E6647AE_H
#ifndef XDTANYATOMICTYPE_T922A677221DBA603ACF1F4D2024F9222102416D7_H
#define XDTANYATOMICTYPE_T922A677221DBA603ACF1F4D2024F9222102416D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtAnyAtomicType
struct  XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTANYATOMICTYPE_T922A677221DBA603ACF1F4D2024F9222102416D7_H
#ifndef XSDBOOLEAN_TAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959_H
#define XSDBOOLEAN_TAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdBoolean
struct  XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBOOLEAN_TAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959_H
#ifndef XSDDATE_T5536E073BDA86CBF86DE829550F870C6D7F89964_H
#define XSDDATE_T5536E073BDA86CBF86DE829550F870C6D7F89964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDate
struct  XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATE_T5536E073BDA86CBF86DE829550F870C6D7F89964_H
#ifndef XSDDATETIME_TC31D6EF56B47537535EEC097FEBF63553C479E5D_H
#define XSDDATETIME_TC31D6EF56B47537535EEC097FEBF63553C479E5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDateTime
struct  XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATETIME_TC31D6EF56B47537535EEC097FEBF63553C479E5D_H
#ifndef XSDDECIMAL_T1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE_H
#define XSDDECIMAL_T1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDecimal
struct  XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDECIMAL_T1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE_H
#ifndef XSDDOUBLE_TAF166F167258B898C01C97CE94720CA2EDB5E269_H
#define XSDDOUBLE_TAF166F167258B898C01C97CE94720CA2EDB5E269_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDouble
struct  XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDOUBLE_TAF166F167258B898C01C97CE94720CA2EDB5E269_H
#ifndef XSDDURATION_TA3253842BF902691A000BBFD87382247985D5E78_H
#define XSDDURATION_TA3253842BF902691A000BBFD87382247985D5E78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdDuration
struct  XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDURATION_TA3253842BF902691A000BBFD87382247985D5E78_H
#ifndef XSDFLOAT_T02419495B090187B99AC33E40CD6601F7608D9B8_H
#define XSDFLOAT_T02419495B090187B99AC33E40CD6601F7608D9B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdFloat
struct  XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDFLOAT_T02419495B090187B99AC33E40CD6601F7608D9B8_H
#ifndef XSDGDAY_TEC936DF85B2878EB240E093D4B56BBDDC75A463B_H
#define XSDGDAY_TEC936DF85B2878EB240E093D4B56BBDDC75A463B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGDay
struct  XsdGDay_tEC936DF85B2878EB240E093D4B56BBDDC75A463B  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGDAY_TEC936DF85B2878EB240E093D4B56BBDDC75A463B_H
#ifndef XSDGMONTH_T6E98934D7FAFFBCFA788D4068EB7DAE6706F4312_H
#define XSDGMONTH_T6E98934D7FAFFBCFA788D4068EB7DAE6706F4312_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGMonth
struct  XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGMONTH_T6E98934D7FAFFBCFA788D4068EB7DAE6706F4312_H
#ifndef XSDGMONTHDAY_T25E4C24A7DF2B6F4528C0271813BF8AF052F5669_H
#define XSDGMONTHDAY_T25E4C24A7DF2B6F4528C0271813BF8AF052F5669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGMonthDay
struct  XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGMONTHDAY_T25E4C24A7DF2B6F4528C0271813BF8AF052F5669_H
#ifndef XSDGYEAR_T3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2_H
#define XSDGYEAR_T3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGYear
struct  XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGYEAR_T3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2_H
#ifndef XSDGYEARMONTH_T775E38939959BAF7304B4212BD8406456781C385_H
#define XSDGYEARMONTH_T775E38939959BAF7304B4212BD8406456781C385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdGYearMonth
struct  XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDGYEARMONTH_T775E38939959BAF7304B4212BD8406456781C385_H
#ifndef XSDHEXBINARY_T542DD47AB586F148DAE0698D4FC956D0412C4229_H
#define XSDHEXBINARY_T542DD47AB586F148DAE0698D4FC956D0412C4229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdHexBinary
struct  XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDHEXBINARY_T542DD47AB586F148DAE0698D4FC956D0412C4229_H
#ifndef XSDNOTATION_T619CA8B98E6B358DAEE751C97934638D691DB5A3_H
#define XSDNOTATION_T619CA8B98E6B358DAEE751C97934638D691DB5A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNotation
struct  XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNOTATION_T619CA8B98E6B358DAEE751C97934638D691DB5A3_H
#ifndef XSDSTRING_T604253BF4F01F3A649275313E7AED2D8BCB854AD_H
#define XSDSTRING_T604253BF4F01F3A649275313E7AED2D8BCB854AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdString
struct  XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSTRING_T604253BF4F01F3A649275313E7AED2D8BCB854AD_H
#ifndef XSDTIME_TDB0973DB314925C984AC13CB795FD91589AB462C_H
#define XSDTIME_TDB0973DB314925C984AC13CB795FD91589AB462C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdTime
struct  XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C  : public XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0
{
public:

public:
};

struct XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C_StaticFields
{
public:
	// System.String[] Mono.Xml.Schema.XsdTime::timeFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___timeFormats_61;

public:
	inline static int32_t get_offset_of_timeFormats_61() { return static_cast<int32_t>(offsetof(XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C_StaticFields, ___timeFormats_61)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_timeFormats_61() const { return ___timeFormats_61; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_timeFormats_61() { return &___timeFormats_61; }
	inline void set_timeFormats_61(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___timeFormats_61 = value;
		Il2CppCodeGenWriteBarrier((&___timeFormats_61), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDTIME_TDB0973DB314925C984AC13CB795FD91589AB462C_H
#ifndef XMLSCHEMAANY_T57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF_H
#define XMLSCHEMAANY_T57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAny
struct  XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF  : public XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC
{
public:
	// System.String System.Xml.Schema.XmlSchemaAny::nameSpace
	String_t* ___nameSpace_28;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.XmlSchemaAny::processing
	int32_t ___processing_29;
	// Mono.Xml.Schema.XsdWildcard System.Xml.Schema.XmlSchemaAny::wildcard
	XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7 * ___wildcard_30;

public:
	inline static int32_t get_offset_of_nameSpace_28() { return static_cast<int32_t>(offsetof(XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF, ___nameSpace_28)); }
	inline String_t* get_nameSpace_28() const { return ___nameSpace_28; }
	inline String_t** get_address_of_nameSpace_28() { return &___nameSpace_28; }
	inline void set_nameSpace_28(String_t* value)
	{
		___nameSpace_28 = value;
		Il2CppCodeGenWriteBarrier((&___nameSpace_28), value);
	}

	inline static int32_t get_offset_of_processing_29() { return static_cast<int32_t>(offsetof(XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF, ___processing_29)); }
	inline int32_t get_processing_29() const { return ___processing_29; }
	inline int32_t* get_address_of_processing_29() { return &___processing_29; }
	inline void set_processing_29(int32_t value)
	{
		___processing_29 = value;
	}

	inline static int32_t get_offset_of_wildcard_30() { return static_cast<int32_t>(offsetof(XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF, ___wildcard_30)); }
	inline XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7 * get_wildcard_30() const { return ___wildcard_30; }
	inline XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7 ** get_address_of_wildcard_30() { return &___wildcard_30; }
	inline void set_wildcard_30(XsdWildcard_t320459A97A0A2CB47DCDB7E493CEFEFA2D375AC7 * value)
	{
		___wildcard_30 = value;
		Il2CppCodeGenWriteBarrier((&___wildcard_30), value);
	}
};

struct XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaAny System.Xml.Schema.XmlSchemaAny::anyTypeContent
	XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF * ___anyTypeContent_27;

public:
	inline static int32_t get_offset_of_anyTypeContent_27() { return static_cast<int32_t>(offsetof(XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF_StaticFields, ___anyTypeContent_27)); }
	inline XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF * get_anyTypeContent_27() const { return ___anyTypeContent_27; }
	inline XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF ** get_address_of_anyTypeContent_27() { return &___anyTypeContent_27; }
	inline void set_anyTypeContent_27(XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF * value)
	{
		___anyTypeContent_27 = value;
		Il2CppCodeGenWriteBarrier((&___anyTypeContent_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAANY_T57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF_H
#ifndef XMLSCHEMACOMPLEXCONTENT_TE95C6145AB86B2BE2E849E716CED6177BE851CDA_H
#define XMLSCHEMACOMPLEXCONTENT_TE95C6145AB86B2BE2E849E716CED6177BE851CDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexContent
struct  XmlSchemaComplexContent_tE95C6145AB86B2BE2E849E716CED6177BE851CDA  : public XmlSchemaContentModel_tB057D601009DC1517A2A538E001E592F465568D9
{
public:
	// System.Xml.Schema.XmlSchemaContent System.Xml.Schema.XmlSchemaComplexContent::content
	XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA * ___content_16;
	// System.Boolean System.Xml.Schema.XmlSchemaComplexContent::isMixed
	bool ___isMixed_17;

public:
	inline static int32_t get_offset_of_content_16() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContent_tE95C6145AB86B2BE2E849E716CED6177BE851CDA, ___content_16)); }
	inline XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA * get_content_16() const { return ___content_16; }
	inline XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA ** get_address_of_content_16() { return &___content_16; }
	inline void set_content_16(XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA * value)
	{
		___content_16 = value;
		Il2CppCodeGenWriteBarrier((&___content_16), value);
	}

	inline static int32_t get_offset_of_isMixed_17() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContent_tE95C6145AB86B2BE2E849E716CED6177BE851CDA, ___isMixed_17)); }
	inline bool get_isMixed_17() const { return ___isMixed_17; }
	inline bool* get_address_of_isMixed_17() { return &___isMixed_17; }
	inline void set_isMixed_17(bool value)
	{
		___isMixed_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPLEXCONTENT_TE95C6145AB86B2BE2E849E716CED6177BE851CDA_H
#ifndef XMLSCHEMACOMPLEXCONTENTEXTENSION_T53C28EA9FA57EC77E4542023E0CA1D7BA30C657A_H
#define XMLSCHEMACOMPLEXCONTENTEXTENSION_T53C28EA9FA57EC77E4542023E0CA1D7BA30C657A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexContentExtension
struct  XmlSchemaComplexContentExtension_t53C28EA9FA57EC77E4542023E0CA1D7BA30C657A  : public XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA
{
public:
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexContentExtension::any
	XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * ___any_17;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaComplexContentExtension::attributes
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___attributes_18;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaComplexContentExtension::baseTypeName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___baseTypeName_19;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexContentExtension::particle
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * ___particle_20;

public:
	inline static int32_t get_offset_of_any_17() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentExtension_t53C28EA9FA57EC77E4542023E0CA1D7BA30C657A, ___any_17)); }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * get_any_17() const { return ___any_17; }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 ** get_address_of_any_17() { return &___any_17; }
	inline void set_any_17(XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * value)
	{
		___any_17 = value;
		Il2CppCodeGenWriteBarrier((&___any_17), value);
	}

	inline static int32_t get_offset_of_attributes_18() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentExtension_t53C28EA9FA57EC77E4542023E0CA1D7BA30C657A, ___attributes_18)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_attributes_18() const { return ___attributes_18; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_attributes_18() { return &___attributes_18; }
	inline void set_attributes_18(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___attributes_18 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_18), value);
	}

	inline static int32_t get_offset_of_baseTypeName_19() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentExtension_t53C28EA9FA57EC77E4542023E0CA1D7BA30C657A, ___baseTypeName_19)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_baseTypeName_19() const { return ___baseTypeName_19; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_baseTypeName_19() { return &___baseTypeName_19; }
	inline void set_baseTypeName_19(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___baseTypeName_19 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_19), value);
	}

	inline static int32_t get_offset_of_particle_20() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentExtension_t53C28EA9FA57EC77E4542023E0CA1D7BA30C657A, ___particle_20)); }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * get_particle_20() const { return ___particle_20; }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC ** get_address_of_particle_20() { return &___particle_20; }
	inline void set_particle_20(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * value)
	{
		___particle_20 = value;
		Il2CppCodeGenWriteBarrier((&___particle_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPLEXCONTENTEXTENSION_T53C28EA9FA57EC77E4542023E0CA1D7BA30C657A_H
#ifndef XMLSCHEMACOMPLEXCONTENTRESTRICTION_TF14420FE7CBDFA987F5349F74557F16C1F78B2C7_H
#define XMLSCHEMACOMPLEXCONTENTRESTRICTION_TF14420FE7CBDFA987F5349F74557F16C1F78B2C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexContentRestriction
struct  XmlSchemaComplexContentRestriction_tF14420FE7CBDFA987F5349F74557F16C1F78B2C7  : public XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA
{
public:
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexContentRestriction::any
	XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * ___any_17;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaComplexContentRestriction::attributes
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___attributes_18;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaComplexContentRestriction::baseTypeName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___baseTypeName_19;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexContentRestriction::particle
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * ___particle_20;

public:
	inline static int32_t get_offset_of_any_17() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_tF14420FE7CBDFA987F5349F74557F16C1F78B2C7, ___any_17)); }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * get_any_17() const { return ___any_17; }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 ** get_address_of_any_17() { return &___any_17; }
	inline void set_any_17(XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * value)
	{
		___any_17 = value;
		Il2CppCodeGenWriteBarrier((&___any_17), value);
	}

	inline static int32_t get_offset_of_attributes_18() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_tF14420FE7CBDFA987F5349F74557F16C1F78B2C7, ___attributes_18)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_attributes_18() const { return ___attributes_18; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_attributes_18() { return &___attributes_18; }
	inline void set_attributes_18(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___attributes_18 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_18), value);
	}

	inline static int32_t get_offset_of_baseTypeName_19() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_tF14420FE7CBDFA987F5349F74557F16C1F78B2C7, ___baseTypeName_19)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_baseTypeName_19() const { return ___baseTypeName_19; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_baseTypeName_19() { return &___baseTypeName_19; }
	inline void set_baseTypeName_19(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___baseTypeName_19 = value;
		Il2CppCodeGenWriteBarrier((&___baseTypeName_19), value);
	}

	inline static int32_t get_offset_of_particle_20() { return static_cast<int32_t>(offsetof(XmlSchemaComplexContentRestriction_tF14420FE7CBDFA987F5349F74557F16C1F78B2C7, ___particle_20)); }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * get_particle_20() const { return ___particle_20; }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC ** get_address_of_particle_20() { return &___particle_20; }
	inline void set_particle_20(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * value)
	{
		___particle_20 = value;
		Il2CppCodeGenWriteBarrier((&___particle_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPLEXCONTENTRESTRICTION_TF14420FE7CBDFA987F5349F74557F16C1F78B2C7_H
#ifndef XMLSCHEMACOMPLEXTYPE_T524B1B0623DE77C8045B20C12BB58D17A8ADB0CB_H
#define XMLSCHEMACOMPLEXTYPE_T524B1B0623DE77C8045B20C12BB58D17A8ADB0CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaComplexType
struct  XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB  : public XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE
{
public:
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexType::anyAttribute
	XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * ___anyAttribute_28;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaComplexType::attributes
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___attributes_29;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.XmlSchemaComplexType::attributeUses
	XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * ___attributeUses_30;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.XmlSchemaComplexType::attributeWildcard
	XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * ___attributeWildcard_31;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaComplexType::block
	int32_t ___block_32;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaComplexType::blockResolved
	int32_t ___blockResolved_33;
	// System.Xml.Schema.XmlSchemaContentModel System.Xml.Schema.XmlSchemaComplexType::contentModel
	XmlSchemaContentModel_tB057D601009DC1517A2A538E001E592F465568D9 * ___contentModel_34;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexType::validatableParticle
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * ___validatableParticle_35;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexType::contentTypeParticle
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * ___contentTypeParticle_36;
	// System.Boolean System.Xml.Schema.XmlSchemaComplexType::isAbstract
	bool ___isAbstract_37;
	// System.Boolean System.Xml.Schema.XmlSchemaComplexType::isMixed
	bool ___isMixed_38;
	// System.Xml.Schema.XmlSchemaParticle System.Xml.Schema.XmlSchemaComplexType::particle
	XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * ___particle_39;
	// System.Xml.Schema.XmlSchemaContentType System.Xml.Schema.XmlSchemaComplexType::resolvedContentType
	int32_t ___resolvedContentType_40;
	// System.Boolean System.Xml.Schema.XmlSchemaComplexType::ValidatedIsAbstract
	bool ___ValidatedIsAbstract_41;
	// System.Guid System.Xml.Schema.XmlSchemaComplexType::CollectProcessId
	Guid_t  ___CollectProcessId_44;

public:
	inline static int32_t get_offset_of_anyAttribute_28() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___anyAttribute_28)); }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * get_anyAttribute_28() const { return ___anyAttribute_28; }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 ** get_address_of_anyAttribute_28() { return &___anyAttribute_28; }
	inline void set_anyAttribute_28(XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * value)
	{
		___anyAttribute_28 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttribute_28), value);
	}

	inline static int32_t get_offset_of_attributes_29() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___attributes_29)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_attributes_29() const { return ___attributes_29; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_attributes_29() { return &___attributes_29; }
	inline void set_attributes_29(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___attributes_29 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_29), value);
	}

	inline static int32_t get_offset_of_attributeUses_30() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___attributeUses_30)); }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * get_attributeUses_30() const { return ___attributeUses_30; }
	inline XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 ** get_address_of_attributeUses_30() { return &___attributeUses_30; }
	inline void set_attributeUses_30(XmlSchemaObjectTable_tA53C8BFD98C0E322B437AA48D3C89E01F6540D16 * value)
	{
		___attributeUses_30 = value;
		Il2CppCodeGenWriteBarrier((&___attributeUses_30), value);
	}

	inline static int32_t get_offset_of_attributeWildcard_31() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___attributeWildcard_31)); }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * get_attributeWildcard_31() const { return ___attributeWildcard_31; }
	inline XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 ** get_address_of_attributeWildcard_31() { return &___attributeWildcard_31; }
	inline void set_attributeWildcard_31(XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3 * value)
	{
		___attributeWildcard_31 = value;
		Il2CppCodeGenWriteBarrier((&___attributeWildcard_31), value);
	}

	inline static int32_t get_offset_of_block_32() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___block_32)); }
	inline int32_t get_block_32() const { return ___block_32; }
	inline int32_t* get_address_of_block_32() { return &___block_32; }
	inline void set_block_32(int32_t value)
	{
		___block_32 = value;
	}

	inline static int32_t get_offset_of_blockResolved_33() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___blockResolved_33)); }
	inline int32_t get_blockResolved_33() const { return ___blockResolved_33; }
	inline int32_t* get_address_of_blockResolved_33() { return &___blockResolved_33; }
	inline void set_blockResolved_33(int32_t value)
	{
		___blockResolved_33 = value;
	}

	inline static int32_t get_offset_of_contentModel_34() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___contentModel_34)); }
	inline XmlSchemaContentModel_tB057D601009DC1517A2A538E001E592F465568D9 * get_contentModel_34() const { return ___contentModel_34; }
	inline XmlSchemaContentModel_tB057D601009DC1517A2A538E001E592F465568D9 ** get_address_of_contentModel_34() { return &___contentModel_34; }
	inline void set_contentModel_34(XmlSchemaContentModel_tB057D601009DC1517A2A538E001E592F465568D9 * value)
	{
		___contentModel_34 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_34), value);
	}

	inline static int32_t get_offset_of_validatableParticle_35() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___validatableParticle_35)); }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * get_validatableParticle_35() const { return ___validatableParticle_35; }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC ** get_address_of_validatableParticle_35() { return &___validatableParticle_35; }
	inline void set_validatableParticle_35(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * value)
	{
		___validatableParticle_35 = value;
		Il2CppCodeGenWriteBarrier((&___validatableParticle_35), value);
	}

	inline static int32_t get_offset_of_contentTypeParticle_36() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___contentTypeParticle_36)); }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * get_contentTypeParticle_36() const { return ___contentTypeParticle_36; }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC ** get_address_of_contentTypeParticle_36() { return &___contentTypeParticle_36; }
	inline void set_contentTypeParticle_36(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * value)
	{
		___contentTypeParticle_36 = value;
		Il2CppCodeGenWriteBarrier((&___contentTypeParticle_36), value);
	}

	inline static int32_t get_offset_of_isAbstract_37() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___isAbstract_37)); }
	inline bool get_isAbstract_37() const { return ___isAbstract_37; }
	inline bool* get_address_of_isAbstract_37() { return &___isAbstract_37; }
	inline void set_isAbstract_37(bool value)
	{
		___isAbstract_37 = value;
	}

	inline static int32_t get_offset_of_isMixed_38() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___isMixed_38)); }
	inline bool get_isMixed_38() const { return ___isMixed_38; }
	inline bool* get_address_of_isMixed_38() { return &___isMixed_38; }
	inline void set_isMixed_38(bool value)
	{
		___isMixed_38 = value;
	}

	inline static int32_t get_offset_of_particle_39() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___particle_39)); }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * get_particle_39() const { return ___particle_39; }
	inline XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC ** get_address_of_particle_39() { return &___particle_39; }
	inline void set_particle_39(XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC * value)
	{
		___particle_39 = value;
		Il2CppCodeGenWriteBarrier((&___particle_39), value);
	}

	inline static int32_t get_offset_of_resolvedContentType_40() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___resolvedContentType_40)); }
	inline int32_t get_resolvedContentType_40() const { return ___resolvedContentType_40; }
	inline int32_t* get_address_of_resolvedContentType_40() { return &___resolvedContentType_40; }
	inline void set_resolvedContentType_40(int32_t value)
	{
		___resolvedContentType_40 = value;
	}

	inline static int32_t get_offset_of_ValidatedIsAbstract_41() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___ValidatedIsAbstract_41)); }
	inline bool get_ValidatedIsAbstract_41() const { return ___ValidatedIsAbstract_41; }
	inline bool* get_address_of_ValidatedIsAbstract_41() { return &___ValidatedIsAbstract_41; }
	inline void set_ValidatedIsAbstract_41(bool value)
	{
		___ValidatedIsAbstract_41 = value;
	}

	inline static int32_t get_offset_of_CollectProcessId_44() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB, ___CollectProcessId_44)); }
	inline Guid_t  get_CollectProcessId_44() const { return ___CollectProcessId_44; }
	inline Guid_t * get_address_of_CollectProcessId_44() { return &___CollectProcessId_44; }
	inline void set_CollectProcessId_44(Guid_t  value)
	{
		___CollectProcessId_44 = value;
	}
};

struct XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB_StaticFields
{
public:
	// System.Xml.Schema.XmlSchemaComplexType System.Xml.Schema.XmlSchemaComplexType::anyType
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB * ___anyType_42;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaComplexType::AnyTypeName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___AnyTypeName_43;

public:
	inline static int32_t get_offset_of_anyType_42() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB_StaticFields, ___anyType_42)); }
	inline XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB * get_anyType_42() const { return ___anyType_42; }
	inline XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB ** get_address_of_anyType_42() { return &___anyType_42; }
	inline void set_anyType_42(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB * value)
	{
		___anyType_42 = value;
		Il2CppCodeGenWriteBarrier((&___anyType_42), value);
	}

	inline static int32_t get_offset_of_AnyTypeName_43() { return static_cast<int32_t>(offsetof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB_StaticFields, ___AnyTypeName_43)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_AnyTypeName_43() const { return ___AnyTypeName_43; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_AnyTypeName_43() { return &___AnyTypeName_43; }
	inline void set_AnyTypeName_43(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___AnyTypeName_43 = value;
		Il2CppCodeGenWriteBarrier((&___AnyTypeName_43), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACOMPLEXTYPE_T524B1B0623DE77C8045B20C12BB58D17A8ADB0CB_H
#ifndef XMLSCHEMAELEMENT_T3FC59E33781324C566357FD7C739113242BAAD64_H
#define XMLSCHEMAELEMENT_T3FC59E33781324C566357FD7C739113242BAAD64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaElement
struct  XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64  : public XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC
{
public:
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::block
	int32_t ___block_27;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaElement::constraints
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___constraints_28;
	// System.String System.Xml.Schema.XmlSchemaElement::defaultValue
	String_t* ___defaultValue_29;
	// System.Object System.Xml.Schema.XmlSchemaElement::elementType
	RuntimeObject * ___elementType_30;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaElement::elementSchemaType
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * ___elementSchemaType_31;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::final
	int32_t ___final_32;
	// System.String System.Xml.Schema.XmlSchemaElement::fixedValue
	String_t* ___fixedValue_33;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.XmlSchemaElement::form
	int32_t ___form_34;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::isAbstract
	bool ___isAbstract_35;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::isNillable
	bool ___isNillable_36;
	// System.String System.Xml.Schema.XmlSchemaElement::name
	String_t* ___name_37;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::refName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___refName_38;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.XmlSchemaElement::schemaType
	XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * ___schemaType_39;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::schemaTypeName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___schemaTypeName_40;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::substitutionGroup
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___substitutionGroup_41;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaElement::schema
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * ___schema_42;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::parentIsSchema
	bool ___parentIsSchema_43;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaElement::qName
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___qName_44;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::blockResolved
	int32_t ___blockResolved_45;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.XmlSchemaElement::finalResolved
	int32_t ___finalResolved_46;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaElement::referencedElement
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * ___referencedElement_47;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaElement::substitutingElements
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___substitutingElements_48;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.XmlSchemaElement::substitutionGroupElement
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * ___substitutionGroupElement_49;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::actualIsAbstract
	bool ___actualIsAbstract_50;
	// System.Boolean System.Xml.Schema.XmlSchemaElement::actualIsNillable
	bool ___actualIsNillable_51;
	// System.String System.Xml.Schema.XmlSchemaElement::validatedDefaultValue
	String_t* ___validatedDefaultValue_52;
	// System.String System.Xml.Schema.XmlSchemaElement::validatedFixedValue
	String_t* ___validatedFixedValue_53;

public:
	inline static int32_t get_offset_of_block_27() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___block_27)); }
	inline int32_t get_block_27() const { return ___block_27; }
	inline int32_t* get_address_of_block_27() { return &___block_27; }
	inline void set_block_27(int32_t value)
	{
		___block_27 = value;
	}

	inline static int32_t get_offset_of_constraints_28() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___constraints_28)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_constraints_28() const { return ___constraints_28; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_constraints_28() { return &___constraints_28; }
	inline void set_constraints_28(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___constraints_28 = value;
		Il2CppCodeGenWriteBarrier((&___constraints_28), value);
	}

	inline static int32_t get_offset_of_defaultValue_29() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___defaultValue_29)); }
	inline String_t* get_defaultValue_29() const { return ___defaultValue_29; }
	inline String_t** get_address_of_defaultValue_29() { return &___defaultValue_29; }
	inline void set_defaultValue_29(String_t* value)
	{
		___defaultValue_29 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValue_29), value);
	}

	inline static int32_t get_offset_of_elementType_30() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___elementType_30)); }
	inline RuntimeObject * get_elementType_30() const { return ___elementType_30; }
	inline RuntimeObject ** get_address_of_elementType_30() { return &___elementType_30; }
	inline void set_elementType_30(RuntimeObject * value)
	{
		___elementType_30 = value;
		Il2CppCodeGenWriteBarrier((&___elementType_30), value);
	}

	inline static int32_t get_offset_of_elementSchemaType_31() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___elementSchemaType_31)); }
	inline XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * get_elementSchemaType_31() const { return ___elementSchemaType_31; }
	inline XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE ** get_address_of_elementSchemaType_31() { return &___elementSchemaType_31; }
	inline void set_elementSchemaType_31(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * value)
	{
		___elementSchemaType_31 = value;
		Il2CppCodeGenWriteBarrier((&___elementSchemaType_31), value);
	}

	inline static int32_t get_offset_of_final_32() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___final_32)); }
	inline int32_t get_final_32() const { return ___final_32; }
	inline int32_t* get_address_of_final_32() { return &___final_32; }
	inline void set_final_32(int32_t value)
	{
		___final_32 = value;
	}

	inline static int32_t get_offset_of_fixedValue_33() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___fixedValue_33)); }
	inline String_t* get_fixedValue_33() const { return ___fixedValue_33; }
	inline String_t** get_address_of_fixedValue_33() { return &___fixedValue_33; }
	inline void set_fixedValue_33(String_t* value)
	{
		___fixedValue_33 = value;
		Il2CppCodeGenWriteBarrier((&___fixedValue_33), value);
	}

	inline static int32_t get_offset_of_form_34() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___form_34)); }
	inline int32_t get_form_34() const { return ___form_34; }
	inline int32_t* get_address_of_form_34() { return &___form_34; }
	inline void set_form_34(int32_t value)
	{
		___form_34 = value;
	}

	inline static int32_t get_offset_of_isAbstract_35() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___isAbstract_35)); }
	inline bool get_isAbstract_35() const { return ___isAbstract_35; }
	inline bool* get_address_of_isAbstract_35() { return &___isAbstract_35; }
	inline void set_isAbstract_35(bool value)
	{
		___isAbstract_35 = value;
	}

	inline static int32_t get_offset_of_isNillable_36() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___isNillable_36)); }
	inline bool get_isNillable_36() const { return ___isNillable_36; }
	inline bool* get_address_of_isNillable_36() { return &___isNillable_36; }
	inline void set_isNillable_36(bool value)
	{
		___isNillable_36 = value;
	}

	inline static int32_t get_offset_of_name_37() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___name_37)); }
	inline String_t* get_name_37() const { return ___name_37; }
	inline String_t** get_address_of_name_37() { return &___name_37; }
	inline void set_name_37(String_t* value)
	{
		___name_37 = value;
		Il2CppCodeGenWriteBarrier((&___name_37), value);
	}

	inline static int32_t get_offset_of_refName_38() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___refName_38)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_refName_38() const { return ___refName_38; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_refName_38() { return &___refName_38; }
	inline void set_refName_38(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___refName_38 = value;
		Il2CppCodeGenWriteBarrier((&___refName_38), value);
	}

	inline static int32_t get_offset_of_schemaType_39() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___schemaType_39)); }
	inline XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * get_schemaType_39() const { return ___schemaType_39; }
	inline XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE ** get_address_of_schemaType_39() { return &___schemaType_39; }
	inline void set_schemaType_39(XmlSchemaType_tDF0987DC710BC7BED17E5008774575FA2E6647AE * value)
	{
		___schemaType_39 = value;
		Il2CppCodeGenWriteBarrier((&___schemaType_39), value);
	}

	inline static int32_t get_offset_of_schemaTypeName_40() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___schemaTypeName_40)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_schemaTypeName_40() const { return ___schemaTypeName_40; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_schemaTypeName_40() { return &___schemaTypeName_40; }
	inline void set_schemaTypeName_40(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___schemaTypeName_40 = value;
		Il2CppCodeGenWriteBarrier((&___schemaTypeName_40), value);
	}

	inline static int32_t get_offset_of_substitutionGroup_41() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___substitutionGroup_41)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_substitutionGroup_41() const { return ___substitutionGroup_41; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_substitutionGroup_41() { return &___substitutionGroup_41; }
	inline void set_substitutionGroup_41(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___substitutionGroup_41 = value;
		Il2CppCodeGenWriteBarrier((&___substitutionGroup_41), value);
	}

	inline static int32_t get_offset_of_schema_42() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___schema_42)); }
	inline XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * get_schema_42() const { return ___schema_42; }
	inline XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 ** get_address_of_schema_42() { return &___schema_42; }
	inline void set_schema_42(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * value)
	{
		___schema_42 = value;
		Il2CppCodeGenWriteBarrier((&___schema_42), value);
	}

	inline static int32_t get_offset_of_parentIsSchema_43() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___parentIsSchema_43)); }
	inline bool get_parentIsSchema_43() const { return ___parentIsSchema_43; }
	inline bool* get_address_of_parentIsSchema_43() { return &___parentIsSchema_43; }
	inline void set_parentIsSchema_43(bool value)
	{
		___parentIsSchema_43 = value;
	}

	inline static int32_t get_offset_of_qName_44() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___qName_44)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_qName_44() const { return ___qName_44; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_qName_44() { return &___qName_44; }
	inline void set_qName_44(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___qName_44 = value;
		Il2CppCodeGenWriteBarrier((&___qName_44), value);
	}

	inline static int32_t get_offset_of_blockResolved_45() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___blockResolved_45)); }
	inline int32_t get_blockResolved_45() const { return ___blockResolved_45; }
	inline int32_t* get_address_of_blockResolved_45() { return &___blockResolved_45; }
	inline void set_blockResolved_45(int32_t value)
	{
		___blockResolved_45 = value;
	}

	inline static int32_t get_offset_of_finalResolved_46() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___finalResolved_46)); }
	inline int32_t get_finalResolved_46() const { return ___finalResolved_46; }
	inline int32_t* get_address_of_finalResolved_46() { return &___finalResolved_46; }
	inline void set_finalResolved_46(int32_t value)
	{
		___finalResolved_46 = value;
	}

	inline static int32_t get_offset_of_referencedElement_47() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___referencedElement_47)); }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * get_referencedElement_47() const { return ___referencedElement_47; }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 ** get_address_of_referencedElement_47() { return &___referencedElement_47; }
	inline void set_referencedElement_47(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * value)
	{
		___referencedElement_47 = value;
		Il2CppCodeGenWriteBarrier((&___referencedElement_47), value);
	}

	inline static int32_t get_offset_of_substitutingElements_48() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___substitutingElements_48)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_substitutingElements_48() const { return ___substitutingElements_48; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_substitutingElements_48() { return &___substitutingElements_48; }
	inline void set_substitutingElements_48(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___substitutingElements_48 = value;
		Il2CppCodeGenWriteBarrier((&___substitutingElements_48), value);
	}

	inline static int32_t get_offset_of_substitutionGroupElement_49() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___substitutionGroupElement_49)); }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * get_substitutionGroupElement_49() const { return ___substitutionGroupElement_49; }
	inline XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 ** get_address_of_substitutionGroupElement_49() { return &___substitutionGroupElement_49; }
	inline void set_substitutionGroupElement_49(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64 * value)
	{
		___substitutionGroupElement_49 = value;
		Il2CppCodeGenWriteBarrier((&___substitutionGroupElement_49), value);
	}

	inline static int32_t get_offset_of_actualIsAbstract_50() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___actualIsAbstract_50)); }
	inline bool get_actualIsAbstract_50() const { return ___actualIsAbstract_50; }
	inline bool* get_address_of_actualIsAbstract_50() { return &___actualIsAbstract_50; }
	inline void set_actualIsAbstract_50(bool value)
	{
		___actualIsAbstract_50 = value;
	}

	inline static int32_t get_offset_of_actualIsNillable_51() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___actualIsNillable_51)); }
	inline bool get_actualIsNillable_51() const { return ___actualIsNillable_51; }
	inline bool* get_address_of_actualIsNillable_51() { return &___actualIsNillable_51; }
	inline void set_actualIsNillable_51(bool value)
	{
		___actualIsNillable_51 = value;
	}

	inline static int32_t get_offset_of_validatedDefaultValue_52() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___validatedDefaultValue_52)); }
	inline String_t* get_validatedDefaultValue_52() const { return ___validatedDefaultValue_52; }
	inline String_t** get_address_of_validatedDefaultValue_52() { return &___validatedDefaultValue_52; }
	inline void set_validatedDefaultValue_52(String_t* value)
	{
		___validatedDefaultValue_52 = value;
		Il2CppCodeGenWriteBarrier((&___validatedDefaultValue_52), value);
	}

	inline static int32_t get_offset_of_validatedFixedValue_53() { return static_cast<int32_t>(offsetof(XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64, ___validatedFixedValue_53)); }
	inline String_t* get_validatedFixedValue_53() const { return ___validatedFixedValue_53; }
	inline String_t** get_address_of_validatedFixedValue_53() { return &___validatedFixedValue_53; }
	inline void set_validatedFixedValue_53(String_t* value)
	{
		___validatedFixedValue_53 = value;
		Il2CppCodeGenWriteBarrier((&___validatedFixedValue_53), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAELEMENT_T3FC59E33781324C566357FD7C739113242BAAD64_H
#ifndef XMLSCHEMAENUMERATIONFACET_TB7AE5A8944818E8371CCD309658DCEF53595464F_H
#define XMLSCHEMAENUMERATIONFACET_TB7AE5A8944818E8371CCD309658DCEF53595464F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaEnumerationFacet
struct  XmlSchemaEnumerationFacet_tB7AE5A8944818E8371CCD309658DCEF53595464F  : public XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAENUMERATIONFACET_TB7AE5A8944818E8371CCD309658DCEF53595464F_H
#ifndef XMLSCHEMAGROUPBASE_TA64F460BB1ABB1F903265373A8216E35A19A9D9F_H
#define XMLSCHEMAGROUPBASE_TA64F460BB1ABB1F903265373A8216E35A19A9D9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaGroupBase
struct  XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F  : public XmlSchemaParticle_t7A09A84556A27FD42B9A6F7A933345158E7752BC
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaGroupBase::compiledItems
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___compiledItems_27;

public:
	inline static int32_t get_offset_of_compiledItems_27() { return static_cast<int32_t>(offsetof(XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F, ___compiledItems_27)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_compiledItems_27() const { return ___compiledItems_27; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_compiledItems_27() { return &___compiledItems_27; }
	inline void set_compiledItems_27(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___compiledItems_27 = value;
		Il2CppCodeGenWriteBarrier((&___compiledItems_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAGROUPBASE_TA64F460BB1ABB1F903265373A8216E35A19A9D9F_H
#ifndef XDTDAYTIMEDURATION_T14B50E7DF624721215FA0E49D3672BAAFF59027F_H
#define XDTDAYTIMEDURATION_T14B50E7DF624721215FA0E49D3672BAAFF59027F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtDayTimeDuration
struct  XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F  : public XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTDAYTIMEDURATION_T14B50E7DF624721215FA0E49D3672BAAFF59027F_H
#ifndef XDTUNTYPEDATOMIC_T8E0FD4A48C677AA7C2D946690BE85E92CE81CDED_H
#define XDTUNTYPEDATOMIC_T8E0FD4A48C677AA7C2D946690BE85E92CE81CDED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtUntypedAtomic
struct  XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED  : public XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTUNTYPEDATOMIC_T8E0FD4A48C677AA7C2D946690BE85E92CE81CDED_H
#ifndef XDTYEARMONTHDURATION_T940953B7258AE5DEC65A9F65D90EC70D9786EA68_H
#define XDTYEARMONTHDURATION_T940953B7258AE5DEC65A9F65D90EC70D9786EA68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XdtYearMonthDuration
struct  XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68  : public XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDTYEARMONTHDURATION_T940953B7258AE5DEC65A9F65D90EC70D9786EA68_H
#ifndef XSDANYURI_T049F3807506793286A90A0A279CADC8C39D4647A_H
#define XSDANYURI_T049F3807506793286A90A0A279CADC8C39D4647A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdAnyURI
struct  XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A  : public XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDANYURI_T049F3807506793286A90A0A279CADC8C39D4647A_H
#ifndef XSDBASE64BINARY_T2EBF4685DF4837949C9AA51C1DAA031B7259405F_H
#define XSDBASE64BINARY_T2EBF4685DF4837949C9AA51C1DAA031B7259405F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdBase64Binary
struct  XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F  : public XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD
{
public:

public:
};

struct XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F_StaticFields
{
public:
	// System.String Mono.Xml.Schema.XsdBase64Binary::ALPHABET
	String_t* ___ALPHABET_61;
	// System.Byte[] Mono.Xml.Schema.XsdBase64Binary::decodeTable
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___decodeTable_62;

public:
	inline static int32_t get_offset_of_ALPHABET_61() { return static_cast<int32_t>(offsetof(XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F_StaticFields, ___ALPHABET_61)); }
	inline String_t* get_ALPHABET_61() const { return ___ALPHABET_61; }
	inline String_t** get_address_of_ALPHABET_61() { return &___ALPHABET_61; }
	inline void set_ALPHABET_61(String_t* value)
	{
		___ALPHABET_61 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHABET_61), value);
	}

	inline static int32_t get_offset_of_decodeTable_62() { return static_cast<int32_t>(offsetof(XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F_StaticFields, ___decodeTable_62)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_decodeTable_62() const { return ___decodeTable_62; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_decodeTable_62() { return &___decodeTable_62; }
	inline void set_decodeTable_62(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___decodeTable_62 = value;
		Il2CppCodeGenWriteBarrier((&___decodeTable_62), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBASE64BINARY_T2EBF4685DF4837949C9AA51C1DAA031B7259405F_H
#ifndef XSDINTEGER_TE61DFB650D9B7BD3F6CA2C7DD4331667015CA348_H
#define XSDINTEGER_TE61DFB650D9B7BD3F6CA2C7DD4331667015CA348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInteger
struct  XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348  : public XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINTEGER_TE61DFB650D9B7BD3F6CA2C7DD4331667015CA348_H
#ifndef XSDNORMALIZEDSTRING_TEAAA641AA303B7A5227D53147AACC07A82EE13C0_H
#define XSDNORMALIZEDSTRING_TEAAA641AA303B7A5227D53147AACC07A82EE13C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNormalizedString
struct  XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0  : public XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNORMALIZEDSTRING_TEAAA641AA303B7A5227D53147AACC07A82EE13C0_H
#ifndef XMLSCHEMAALL_TD45D6713E3AF00578B100499CE5A37067F862EAB_H
#define XMLSCHEMAALL_TD45D6713E3AF00578B100499CE5A37067F862EAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaAll
struct  XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB  : public XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaAll::schema
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * ___schema_28;
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaAll::items
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___items_29;
	// System.Boolean System.Xml.Schema.XmlSchemaAll::emptiable
	bool ___emptiable_30;

public:
	inline static int32_t get_offset_of_schema_28() { return static_cast<int32_t>(offsetof(XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB, ___schema_28)); }
	inline XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * get_schema_28() const { return ___schema_28; }
	inline XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 ** get_address_of_schema_28() { return &___schema_28; }
	inline void set_schema_28(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696 * value)
	{
		___schema_28 = value;
		Il2CppCodeGenWriteBarrier((&___schema_28), value);
	}

	inline static int32_t get_offset_of_items_29() { return static_cast<int32_t>(offsetof(XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB, ___items_29)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_items_29() const { return ___items_29; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_items_29() { return &___items_29; }
	inline void set_items_29(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___items_29 = value;
		Il2CppCodeGenWriteBarrier((&___items_29), value);
	}

	inline static int32_t get_offset_of_emptiable_30() { return static_cast<int32_t>(offsetof(XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB, ___emptiable_30)); }
	inline bool get_emptiable_30() const { return ___emptiable_30; }
	inline bool* get_address_of_emptiable_30() { return &___emptiable_30; }
	inline void set_emptiable_30(bool value)
	{
		___emptiable_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAALL_TD45D6713E3AF00578B100499CE5A37067F862EAB_H
#ifndef XMLSCHEMACHOICE_T6872795EE02FB1510551BFC1F5657111504F33D5_H
#define XMLSCHEMACHOICE_T6872795EE02FB1510551BFC1F5657111504F33D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaChoice
struct  XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5  : public XmlSchemaGroupBase_tA64F460BB1ABB1F903265373A8216E35A19A9D9F
{
public:
	// System.Xml.Schema.XmlSchemaObjectCollection System.Xml.Schema.XmlSchemaChoice::items
	XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * ___items_28;
	// System.Decimal System.Xml.Schema.XmlSchemaChoice::minEffectiveTotalRange
	Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  ___minEffectiveTotalRange_29;

public:
	inline static int32_t get_offset_of_items_28() { return static_cast<int32_t>(offsetof(XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5, ___items_28)); }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * get_items_28() const { return ___items_28; }
	inline XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D ** get_address_of_items_28() { return &___items_28; }
	inline void set_items_28(XmlSchemaObjectCollection_t69E14FD8D1AD9A7E03D9982F62CC5E2E411A146D * value)
	{
		___items_28 = value;
		Il2CppCodeGenWriteBarrier((&___items_28), value);
	}

	inline static int32_t get_offset_of_minEffectiveTotalRange_29() { return static_cast<int32_t>(offsetof(XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5, ___minEffectiveTotalRange_29)); }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  get_minEffectiveTotalRange_29() const { return ___minEffectiveTotalRange_29; }
	inline Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3 * get_address_of_minEffectiveTotalRange_29() { return &___minEffectiveTotalRange_29; }
	inline void set_minEffectiveTotalRange_29(Decimal_t46BBDD161320E361BC57E00CD6C1F87256CA27F3  value)
	{
		___minEffectiveTotalRange_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACHOICE_T6872795EE02FB1510551BFC1F5657111504F33D5_H
#ifndef XSDLONG_T6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC_H
#define XSDLONG_T6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdLong
struct  XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC  : public XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDLONG_T6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC_H
#ifndef XSDNONNEGATIVEINTEGER_T0F4F93B0464B81E0BBF21CFC2F08343966734B30_H
#define XSDNONNEGATIVEINTEGER_T0F4F93B0464B81E0BBF21CFC2F08343966734B30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNonNegativeInteger
struct  XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30  : public XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNONNEGATIVEINTEGER_T0F4F93B0464B81E0BBF21CFC2F08343966734B30_H
#ifndef XSDNONPOSITIVEINTEGER_T960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62_H
#define XSDNONPOSITIVEINTEGER_T960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNonPositiveInteger
struct  XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62  : public XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNONPOSITIVEINTEGER_T960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62_H
#ifndef XSDTOKEN_T4A120CAA60CECDF6DCDC15717ABCA63187B0D60C_H
#define XSDTOKEN_T4A120CAA60CECDF6DCDC15717ABCA63187B0D60C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdToken
struct  XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C  : public XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDTOKEN_T4A120CAA60CECDF6DCDC15717ABCA63187B0D60C_H
#ifndef XSDINT_T6F6E692FF9941520F897A726ABDE415CCEFC984F_H
#define XSDINT_T6F6E692FF9941520F897A726ABDE415CCEFC984F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdInt
struct  XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F  : public XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDINT_T6F6E692FF9941520F897A726ABDE415CCEFC984F_H
#ifndef XSDLANGUAGE_T392B00FB85420B861413B7295BC68EA7C0982D85_H
#define XSDLANGUAGE_T392B00FB85420B861413B7295BC68EA7C0982D85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdLanguage
struct  XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85  : public XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDLANGUAGE_T392B00FB85420B861413B7295BC68EA7C0982D85_H
#ifndef XSDNMTOKEN_TED82C02272B75D49D76B0FC44858D09D8F06AB91_H
#define XSDNMTOKEN_TED82C02272B75D49D76B0FC44858D09D8F06AB91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNMToken
struct  XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91  : public XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNMTOKEN_TED82C02272B75D49D76B0FC44858D09D8F06AB91_H
#ifndef XSDNAME_T52D491BF6C35EDF8B16CE44E5677C37B75184CB8_H
#define XSDNAME_T52D491BF6C35EDF8B16CE44E5677C37B75184CB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdName
struct  XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8  : public XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNAME_T52D491BF6C35EDF8B16CE44E5677C37B75184CB8_H
#ifndef XSDNEGATIVEINTEGER_T5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B_H
#define XSDNEGATIVEINTEGER_T5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNegativeInteger
struct  XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B  : public XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNEGATIVEINTEGER_T5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B_H
#ifndef XSDPOSITIVEINTEGER_T8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2_H
#define XSDPOSITIVEINTEGER_T8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdPositiveInteger
struct  XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2  : public XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDPOSITIVEINTEGER_T8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2_H
#ifndef XSDUNSIGNEDLONG_T7B886E29EBA85082057263BFE0B9F644FF4D59B9_H
#define XSDUNSIGNEDLONG_T7B886E29EBA85082057263BFE0B9F644FF4D59B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedLong
struct  XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9  : public XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDLONG_T7B886E29EBA85082057263BFE0B9F644FF4D59B9_H
#ifndef XSDENTITIES_TE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C_H
#define XSDENTITIES_TE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdEntities
struct  XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDENTITIES_TE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C_H
#ifndef XSDENTITY_T426A9BE787702C55FF7FE5FE0FCDF582D5E50371_H
#define XSDENTITY_T426A9BE787702C55FF7FE5FE0FCDF582D5E50371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdEntity
struct  XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDENTITY_T426A9BE787702C55FF7FE5FE0FCDF582D5E50371_H
#ifndef XSDID_TFC394CE8EE4F16A40F0063659AB8C788C712E77B_H
#define XSDID_TFC394CE8EE4F16A40F0063659AB8C788C712E77B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdID
struct  XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDID_TFC394CE8EE4F16A40F0063659AB8C788C712E77B_H
#ifndef XSDIDREF_TC8B88E328BA8745548A89206641A6C113AE9F09B_H
#define XSDIDREF_TC8B88E328BA8745548A89206641A6C113AE9F09B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIDRef
struct  XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDREF_TC8B88E328BA8745548A89206641A6C113AE9F09B_H
#ifndef XSDIDREFS_T3598EB6F29A7BEC87962148242C3904C8FF5BB9F_H
#define XSDIDREFS_T3598EB6F29A7BEC87962148242C3904C8FF5BB9F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdIDRefs
struct  XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDIDREFS_T3598EB6F29A7BEC87962148242C3904C8FF5BB9F_H
#ifndef XSDNCNAME_T7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7_H
#define XSDNCNAME_T7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNCName
struct  XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNCNAME_T7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7_H
#ifndef XSDNMTOKENS_TA3A587C5F1FF49445F75C961B0280F58A57D1095_H
#define XSDNMTOKENS_TA3A587C5F1FF49445F75C961B0280F58A57D1095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdNMTokens
struct  XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095  : public XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDNMTOKENS_TA3A587C5F1FF49445F75C961B0280F58A57D1095_H
#ifndef XSDQNAME_T6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0_H
#define XSDQNAME_T6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdQName
struct  XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0  : public XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDQNAME_T6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0_H
#ifndef XSDSHORT_TAF41351D06667202EC3FDBC76501A2D091518332_H
#define XSDSHORT_TAF41351D06667202EC3FDBC76501A2D091518332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdShort
struct  XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332  : public XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDSHORT_TAF41351D06667202EC3FDBC76501A2D091518332_H
#ifndef XSDUNSIGNEDINT_T01075FD84457AE05E045BB23BD0E3A82AC30CE32_H
#define XSDUNSIGNEDINT_T01075FD84457AE05E045BB23BD0E3A82AC30CE32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedInt
struct  XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32  : public XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDINT_T01075FD84457AE05E045BB23BD0E3A82AC30CE32_H
#ifndef XSDBYTE_T048838D9FF5BFD18C9FC056F11C699E7E08FC351_H
#define XSDBYTE_T048838D9FF5BFD18C9FC056F11C699E7E08FC351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdByte
struct  XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351  : public XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDBYTE_T048838D9FF5BFD18C9FC056F11C699E7E08FC351_H
#ifndef XSDUNSIGNEDSHORT_TA832C283CB5937E31E78F3E4BBC85F90F92CD785_H
#define XSDUNSIGNEDSHORT_TA832C283CB5937E31E78F3E4BBC85F90F92CD785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedShort
struct  XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785  : public XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDSHORT_TA832C283CB5937E31E78F3E4BBC85F90F92CD785_H
#ifndef XSDUNSIGNEDBYTE_TA702674830BFF7B5B4ADF325F30A781366A45256_H
#define XSDUNSIGNEDBYTE_TA702674830BFF7B5B4ADF325F30A781366A45256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.Schema.XsdUnsignedByte
struct  XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256  : public XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDUNSIGNEDBYTE_TA702674830BFF7B5B4ADF325F30A781366A45256_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1), -1, sizeof(XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1600[35] = 
{
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_StaticFields::get_offset_of_unmarked_utf8encoding_0(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_StaticFields::get_offset_of_escaped_text_chars_1(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_StaticFields::get_offset_of_escaped_attr_chars_2(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_base_stream_3(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_source_4(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_writer_5(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_preserver_6(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_preserved_name_7(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_is_preserved_xmlns_8(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_allow_doc_fragment_9(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_close_output_stream_10(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_ignore_encoding_11(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_namespaces_12(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_xmldecl_state_13(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_check_character_validity_14(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_newline_handling_15(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_is_document_entity_16(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_state_17(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_node_state_18(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_nsmanager_19(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_open_count_20(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_elements_21(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_new_local_namespaces_22(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_explicit_nsdecls_23(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_namespace_handling_24(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_indent_25(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_indent_count_26(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_indent_char_27(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_indent_string_28(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_newline_29(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_indent_attributes_30(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_quote_char_31(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1::get_offset_of_v2_32(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_StaticFields::get_offset_of_U3CU3Ef__switchU24map3A_33(),
	XmlTextWriter_tBFB01CAEE59EB58F0B1DDDA1B96320C568FAECC1_StaticFields::get_offset_of_U3CU3Ef__switchU24map3B_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1601[7] = 
{
	XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3::get_offset_of_Prefix_0(),
	XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3::get_offset_of_LocalName_1(),
	XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3::get_offset_of_NS_2(),
	XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3::get_offset_of_HasSimple_3(),
	XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3::get_offset_of_HasElements_4(),
	XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3::get_offset_of_XmlLang_5(),
	XmlNodeInfo_tB8D848A4673F9F3EE6D7CAA6673A9446B8FFF5F3::get_offset_of_XmlSpace_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (StringUtil_t6B1C7DC3B724BD6FC51242BBACBCC786751CA110), -1, sizeof(StringUtil_t6B1C7DC3B724BD6FC51242BBACBCC786751CA110_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1602[2] = 
{
	StringUtil_t6B1C7DC3B724BD6FC51242BBACBCC786751CA110_StaticFields::get_offset_of_cul_0(),
	StringUtil_t6B1C7DC3B724BD6FC51242BBACBCC786751CA110_StaticFields::get_offset_of_cmp_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (XmlDeclState_t1D1721A2F7823A6484A591BC8F6775DFEF3A13E0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1603[5] = 
{
	XmlDeclState_t1D1721A2F7823A6484A591BC8F6775DFEF3A13E0::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (XmlStreamReader_t86B59C766839F8959042F327B7D4A2A3DBF9E27F), -1, sizeof(XmlStreamReader_t86B59C766839F8959042F327B7D4A2A3DBF9E27F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1604[2] = 
{
	XmlStreamReader_t86B59C766839F8959042F327B7D4A2A3DBF9E27F::get_offset_of_input_13(),
	XmlStreamReader_t86B59C766839F8959042F327B7D4A2A3DBF9E27F_StaticFields::get_offset_of_invalidDataException_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1605[11] = 
{
	NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8::get_offset_of_input_buffer_2(),
	NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8::get_offset_of_decoded_buffer_3(),
	NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8::get_offset_of_decoded_count_4(),
	NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8::get_offset_of_pos_5(),
	NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8::get_offset_of_buffer_size_6(),
	NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8::get_offset_of_encoding_7(),
	NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8::get_offset_of_decoder_8(),
	NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8::get_offset_of_base_stream_9(),
	NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8::get_offset_of_mayBlock_10(),
	NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8::get_offset_of_line_builder_11(),
	NonBlockingStreamReader_tCD1035F526FC01E59384659F6AF39B6DEA3365D8::get_offset_of_foundCR_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629), -1, sizeof(XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1606[7] = 
{
	XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629_StaticFields::get_offset_of_StrictUTF8_2(),
	XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629::get_offset_of_enc_3(),
	XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629::get_offset_of_stream_4(),
	XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629::get_offset_of_buffer_5(),
	XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629::get_offset_of_bufLength_6(),
	XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629::get_offset_of_bufPos_7(),
	XmlInputStream_tD179743E3D4482E2297EEC3A71D082F2F2B2F629_StaticFields::get_offset_of_encodingException_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1607[5] = 
{
	XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82::get_offset_of_sourceStack_0(),
	XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82::get_offset_of_source_1(),
	XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82::get_offset_of_has_peek_2(),
	XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82::get_offset_of_peek_char_3(),
	XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82::get_offset_of_allowTextDecl_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1608[6] = 
{
	XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC::get_offset_of_BaseURI_0(),
	XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC::get_offset_of_reader_1(),
	XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC::get_offset_of_state_2(),
	XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC::get_offset_of_isPE_3(),
	XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC::get_offset_of_line_4(),
	XmlParserInputSource_t91A9158D94809823091460776ED5C8688D6D54CC::get_offset_of_column_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (XsdWhitespaceFacet_tCADC752FD2332AC9C1304C86653374FF61C0B91A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1610[4] = 
{
	XsdWhitespaceFacet_tCADC752FD2332AC9C1304C86653374FF61C0B91A::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (XsdOrdering_t27E88D8D2082F10D686D9004720EBEA75EAA05E8)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1611[5] = 
{
	XsdOrdering_t27E88D8D2082F10D686D9004720EBEA75EAA05E8::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0), -1, sizeof(XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1612[6] = 
{
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields::get_offset_of_instance_55(),
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields::get_offset_of_whitespaceArray_56(),
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields::get_offset_of_booleanAllowedFacets_57(),
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields::get_offset_of_decimalAllowedFacets_58(),
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields::get_offset_of_durationAllowedFacets_59(),
	XsdAnySimpleType_t457003328EC5C522A2396822982C317B8DC684F0_StaticFields::get_offset_of_stringAllowedFacets_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (XdtAnyAtomicType_t922A677221DBA603ACF1F4D2024F9222102416D7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (XdtUntypedAtomic_t8E0FD4A48C677AA7C2D946690BE85E92CE81CDED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (XsdString_t604253BF4F01F3A649275313E7AED2D8BCB854AD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (XsdNormalizedString_tEAAA641AA303B7A5227D53147AACC07A82EE13C0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (XsdToken_t4A120CAA60CECDF6DCDC15717ABCA63187B0D60C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (XsdLanguage_t392B00FB85420B861413B7295BC68EA7C0982D85), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (XsdNMToken_tED82C02272B75D49D76B0FC44858D09D8F06AB91), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (XsdNMTokens_tA3A587C5F1FF49445F75C961B0280F58A57D1095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (XsdName_t52D491BF6C35EDF8B16CE44E5677C37B75184CB8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { sizeof (XsdNCName_t7C66E8DC7486C5D92AF7739B71EC88C1FD11E2F7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (XsdID_tFC394CE8EE4F16A40F0063659AB8C788C712E77B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (XsdIDRef_tC8B88E328BA8745548A89206641A6C113AE9F09B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (XsdIDRefs_t3598EB6F29A7BEC87962148242C3904C8FF5BB9F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (XsdEntity_t426A9BE787702C55FF7FE5FE0FCDF582D5E50371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (XsdEntities_tE41F357D8F259BBCCB58DF1E99D66720E8F2CF1C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (XsdNotation_t619CA8B98E6B358DAEE751C97934638D691DB5A3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (XsdDecimal_t1CB6398C98F43AF142671E2E9BDAB05D58AEE9EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (XsdInteger_tE61DFB650D9B7BD3F6CA2C7DD4331667015CA348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (XsdLong_t6FCDD94BFBBEC0874CD9166C160CF7B60D36B5FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (XsdInt_t6F6E692FF9941520F897A726ABDE415CCEFC984F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (XsdShort_tAF41351D06667202EC3FDBC76501A2D091518332), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (XsdByte_t048838D9FF5BFD18C9FC056F11C699E7E08FC351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (XsdNonNegativeInteger_t0F4F93B0464B81E0BBF21CFC2F08343966734B30), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { sizeof (XsdUnsignedLong_t7B886E29EBA85082057263BFE0B9F644FF4D59B9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { sizeof (XsdUnsignedInt_t01075FD84457AE05E045BB23BD0E3A82AC30CE32), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { sizeof (XsdUnsignedShort_tA832C283CB5937E31E78F3E4BBC85F90F92CD785), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (XsdUnsignedByte_tA702674830BFF7B5B4ADF325F30A781366A45256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (XsdPositiveInteger_t8089829DDE14DBD3EEAD11DD4EBA8F4658FA04B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (XsdNonPositiveInteger_t960302EB6DDE4ECE0961BC6BBE4FAFA3C3E5BE62), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (XsdNegativeInteger_t5BF8A2EE42840B88A3C798FC6704B5AA9EDA715B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (XsdFloat_t02419495B090187B99AC33E40CD6601F7608D9B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (XsdDouble_tAF166F167258B898C01C97CE94720CA2EDB5E269), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F), -1, sizeof(XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1645[2] = 
{
	XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F_StaticFields::get_offset_of_ALPHABET_61(),
	XsdBase64Binary_t2EBF4685DF4837949C9AA51C1DAA031B7259405F_StaticFields::get_offset_of_decodeTable_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (XsdHexBinary_t542DD47AB586F148DAE0698D4FC956D0412C4229), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { sizeof (XsdQName_t6E0BD13D8BFE16A59C49FAA8D331CA5FF5066DA0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { sizeof (XsdBoolean_tAA5FDC2ACA9F0A05A686F75BFE68CBB53A2F9959), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (XsdAnyURI_t049F3807506793286A90A0A279CADC8C39D4647A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1650[1] = 
{
	XmlSchemaUri_t93E9CFD7A5D337C08D82EE3A3D83BA447B760FE0::get_offset_of_value_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (XsdDuration_tA3253842BF902691A000BBFD87382247985D5E78), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (XdtDayTimeDuration_t14B50E7DF624721215FA0E49D3672BAAFF59027F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (XdtYearMonthDuration_t940953B7258AE5DEC65A9F65D90EC70D9786EA68), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (XsdDateTime_tC31D6EF56B47537535EEC097FEBF63553C479E5D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (XsdDate_t5536E073BDA86CBF86DE829550F870C6D7F89964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C), -1, sizeof(XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1656[1] = 
{
	XsdTime_tDB0973DB314925C984AC13CB795FD91589AB462C_StaticFields::get_offset_of_timeFormats_61(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (XsdGYearMonth_t775E38939959BAF7304B4212BD8406456781C385), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (XsdGMonthDay_t25E4C24A7DF2B6F4528C0271813BF8AF052F5669), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (XsdGYear_t3A3FECDF3BF0A8EDC0776CA5D478A2394CC894F2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (XsdGMonth_t6E98934D7FAFFBCFA788D4068EB7DAE6706F4312), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (XsdGDay_tEC936DF85B2878EB240E093D4B56BBDDC75A463B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (QNameValueType_t4CB12E2F7D0A45A5F96D22C26D4CA08ABBABC3D9)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1663[1] = 
{
	QNameValueType_t4CB12E2F7D0A45A5F96D22C26D4CA08ABBABC3D9::get_offset_of_value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (StringValueType_t20023BC77B65D51D249852CAC6D9DB267D469A01)+ sizeof (RuntimeObject), sizeof(StringValueType_t20023BC77B65D51D249852CAC6D9DB267D469A01_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1664[1] = 
{
	StringValueType_t20023BC77B65D51D249852CAC6D9DB267D469A01::get_offset_of_value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (UriValueType_tEEC6B4D97CBB2DE8DDAFD52DD400AA2FAF116A70)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1665[1] = 
{
	UriValueType_tEEC6B4D97CBB2DE8DDAFD52DD400AA2FAF116A70::get_offset_of_value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (StringArrayValueType_t2A9744C9A0B6AA9295574EF041B231EDD04510CD)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1666[1] = 
{
	StringArrayValueType_t2A9744C9A0B6AA9295574EF041B231EDD04510CD::get_offset_of_value_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (ValidationEventArgs_t509D590153253EAD1DC5DCEB4CDD56BE30AF3B1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[3] = 
{
	ValidationEventArgs_t509D590153253EAD1DC5DCEB4CDD56BE30AF3B1D::get_offset_of_exception_1(),
	ValidationEventArgs_t509D590153253EAD1DC5DCEB4CDD56BE30AF3B1D::get_offset_of_message_2(),
	ValidationEventArgs_t509D590153253EAD1DC5DCEB4CDD56BE30AF3B1D::get_offset_of_severity_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696), -1, sizeof(XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1668[20] = 
{
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_attributeFormDefault_13(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_attributeGroups_14(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_attributes_15(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_blockDefault_16(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_elementFormDefault_17(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_elements_18(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_finalDefault_19(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_groups_20(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_id_21(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_includes_22(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_items_23(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_notations_24(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_schemaTypes_25(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_targetNamespace_26(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_version_27(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_schemas_28(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_nameTable_29(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_missedSubComponents_30(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696::get_offset_of_compilationItems_31(),
	XmlSchema_t418D644D822F712374BE88CB3DBE2C010E2D3696_StaticFields::get_offset_of_U3CU3Ef__switchU24map41_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1669[3] = 
{
	XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB::get_offset_of_schema_28(),
	XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB::get_offset_of_items_29(),
	XmlSchemaAll_tD45D6713E3AF00578B100499CE5A37067F862EAB::get_offset_of_emptiable_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1670[3] = 
{
	XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741::get_offset_of_annotation_13(),
	XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741::get_offset_of_id_14(),
	XmlSchemaAnnotated_t86F7843F771EEE4F6A372B3B46CC4497928C3741::get_offset_of_unhandledAttributes_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1671[2] = 
{
	XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED::get_offset_of_id_13(),
	XmlSchemaAnnotation_t67D83A065574ADCBDEDC1761C352289C91C639ED::get_offset_of_items_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF), -1, sizeof(XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1672[4] = 
{
	XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF_StaticFields::get_offset_of_anyTypeContent_27(),
	XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF::get_offset_of_nameSpace_28(),
	XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF::get_offset_of_processing_29(),
	XmlSchemaAny_t57CC74A7F88BC0D2C1F6B224D4599D95CC9AB3CF::get_offset_of_wildcard_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[3] = 
{
	XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3::get_offset_of_nameSpace_16(),
	XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3::get_offset_of_processing_17(),
	XmlSchemaAnyAttribute_t8471C64949072A71089D20A4E590B63FCCA58FB3::get_offset_of_wildcard_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (XmlSchemaAppInfo_t7312016110CCC70A45888C86986A3754523E5409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1674[2] = 
{
	XmlSchemaAppInfo_t7312016110CCC70A45888C86986A3754523E5409::get_offset_of_markup_13(),
	XmlSchemaAppInfo_t7312016110CCC70A45888C86986A3754523E5409::get_offset_of_source_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1675[17] = 
{
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_attributeType_16(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_attributeSchemaType_17(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_defaultValue_18(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_fixedValue_19(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_validatedDefaultValue_20(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_validatedFixedValue_21(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_validatedFixedTypedValue_22(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_form_23(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_name_24(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_targetNamespace_25(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_qualifiedName_26(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_refName_27(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_schemaType_28(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_schemaTypeName_29(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_use_30(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_validatedUse_31(),
	XmlSchemaAttribute_t6D32797FC1FC699E9DC42825A31EF1F99DCD0A77::get_offset_of_referencedAttribute_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1676[8] = 
{
	XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923::get_offset_of_anyAttribute_16(),
	XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923::get_offset_of_attributes_17(),
	XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923::get_offset_of_name_18(),
	XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923::get_offset_of_redefined_19(),
	XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923::get_offset_of_qualifiedName_20(),
	XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923::get_offset_of_attributeUses_21(),
	XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923::get_offset_of_anyAttributeUse_22(),
	XmlSchemaAttributeGroup_t86CC2C0DF0657C90F1F29230939E79B519879923::get_offset_of_AttributeGroupRecursionCheck_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (XmlSchemaAttributeGroupRef_tF5C13F6E20C66B54A7771EFB157C6A5F25A3491E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1677[1] = 
{
	XmlSchemaAttributeGroupRef_tF5C13F6E20C66B54A7771EFB157C6A5F25A3491E::get_offset_of_refName_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1678[2] = 
{
	XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5::get_offset_of_items_28(),
	XmlSchemaChoice_t6872795EE02FB1510551BFC1F5657111504F33D5::get_offset_of_minEffectiveTotalRange_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (XmlSchemaCollection_t532C46B1FC9D97F471412FAC2FFCD7F7A93E3513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1679[2] = 
{
	XmlSchemaCollection_t532C46B1FC9D97F471412FAC2FFCD7F7A93E3513::get_offset_of_schemaSet_0(),
	XmlSchemaCollection_t532C46B1FC9D97F471412FAC2FFCD7F7A93E3513::get_offset_of_ValidationEventHandler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (XmlSchemaCollectionEnumerator_t7D03BFEE26AB5317F66B13E8FA91A8882FBC70E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1680[1] = 
{
	XmlSchemaCollectionEnumerator_t7D03BFEE26AB5317F66B13E8FA91A8882FBC70E8::get_offset_of_xenum_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (XmlSchemaCompilationSettings_t84466353D7B09B95C9D10D856332058F60FD31CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1681[1] = 
{
	XmlSchemaCompilationSettings_t84466353D7B09B95C9D10D856332058F60FD31CD::get_offset_of_enable_upa_check_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (XmlSchemaComplexContent_tE95C6145AB86B2BE2E849E716CED6177BE851CDA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1682[2] = 
{
	XmlSchemaComplexContent_tE95C6145AB86B2BE2E849E716CED6177BE851CDA::get_offset_of_content_16(),
	XmlSchemaComplexContent_tE95C6145AB86B2BE2E849E716CED6177BE851CDA::get_offset_of_isMixed_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { sizeof (XmlSchemaComplexContentExtension_t53C28EA9FA57EC77E4542023E0CA1D7BA30C657A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1683[4] = 
{
	XmlSchemaComplexContentExtension_t53C28EA9FA57EC77E4542023E0CA1D7BA30C657A::get_offset_of_any_17(),
	XmlSchemaComplexContentExtension_t53C28EA9FA57EC77E4542023E0CA1D7BA30C657A::get_offset_of_attributes_18(),
	XmlSchemaComplexContentExtension_t53C28EA9FA57EC77E4542023E0CA1D7BA30C657A::get_offset_of_baseTypeName_19(),
	XmlSchemaComplexContentExtension_t53C28EA9FA57EC77E4542023E0CA1D7BA30C657A::get_offset_of_particle_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (XmlSchemaComplexContentRestriction_tF14420FE7CBDFA987F5349F74557F16C1F78B2C7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[4] = 
{
	XmlSchemaComplexContentRestriction_tF14420FE7CBDFA987F5349F74557F16C1F78B2C7::get_offset_of_any_17(),
	XmlSchemaComplexContentRestriction_tF14420FE7CBDFA987F5349F74557F16C1F78B2C7::get_offset_of_attributes_18(),
	XmlSchemaComplexContentRestriction_tF14420FE7CBDFA987F5349F74557F16C1F78B2C7::get_offset_of_baseTypeName_19(),
	XmlSchemaComplexContentRestriction_tF14420FE7CBDFA987F5349F74557F16C1F78B2C7::get_offset_of_particle_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB), -1, sizeof(XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1685[17] = 
{
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_anyAttribute_28(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_attributes_29(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_attributeUses_30(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_attributeWildcard_31(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_block_32(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_blockResolved_33(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_contentModel_34(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_validatableParticle_35(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_contentTypeParticle_36(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_isAbstract_37(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_isMixed_38(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_particle_39(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_resolvedContentType_40(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_ValidatedIsAbstract_41(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB_StaticFields::get_offset_of_anyType_42(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB_StaticFields::get_offset_of_AnyTypeName_43(),
	XmlSchemaComplexType_t524B1B0623DE77C8045B20C12BB58D17A8ADB0CB::get_offset_of_CollectProcessId_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1686[1] = 
{
	XmlSchemaContent_t809DC205F7F607C93C1B078FF8B77BDF4EAD31CA::get_offset_of_actualBaseSchemaType_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (XmlSchemaContentModel_tB057D601009DC1517A2A538E001E592F465568D9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (XmlSchemaContentProcessing_t392DB8F98BDBDD400CD2370DE517690F99622330)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1688[5] = 
{
	XmlSchemaContentProcessing_t392DB8F98BDBDD400CD2370DE517690F99622330::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { sizeof (XmlSchemaContentType_tBCF71BAA43A0320A521F3B5E607A8EB2F57676A6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1689[5] = 
{
	XmlSchemaContentType_tBCF71BAA43A0320A521F3B5E607A8EB2F57676A6::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { sizeof (XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19), -1, sizeof(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1690[55] = 
{
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19::get_offset_of_WhitespaceValue_0(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_wsChars_1(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19::get_offset_of_sb_2(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeAnySimpleType_3(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeString_4(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeNormalizedString_5(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeToken_6(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeLanguage_7(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeNMToken_8(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeNMTokens_9(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeName_10(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeNCName_11(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeID_12(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeIDRef_13(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeIDRefs_14(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeEntity_15(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeEntities_16(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeNotation_17(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeDecimal_18(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeInteger_19(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeLong_20(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeInt_21(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeShort_22(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeByte_23(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeNonNegativeInteger_24(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypePositiveInteger_25(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeUnsignedLong_26(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeUnsignedInt_27(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeUnsignedShort_28(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeUnsignedByte_29(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeNonPositiveInteger_30(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeNegativeInteger_31(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeFloat_32(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeDouble_33(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeBase64Binary_34(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeBoolean_35(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeAnyURI_36(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeDuration_37(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeDateTime_38(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeDate_39(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeTime_40(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeHexBinary_41(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeQName_42(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeGYearMonth_43(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeGMonthDay_44(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeGYear_45(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeGMonth_46(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeGDay_47(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeAnyAtomicType_48(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeUntypedAtomic_49(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeDayTimeDuration_50(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_datatypeYearMonthDuration_51(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_U3CU3Ef__switchU24map3E_52(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_U3CU3Ef__switchU24map3F_53(),
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19_StaticFields::get_offset_of_U3CU3Ef__switchU24map40_54(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (XmlSchemaDerivationMethod_tCA0F660D1F9BAE9B164EC4393AA19EBA107F8C07)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1691[9] = 
{
	XmlSchemaDerivationMethod_tCA0F660D1F9BAE9B164EC4393AA19EBA107F8C07::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (XmlSchemaDocumentation_tF5EE1377C73D6ADCDF6142903E593FB6CCFAE15E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1692[3] = 
{
	XmlSchemaDocumentation_tF5EE1377C73D6ADCDF6142903E593FB6CCFAE15E::get_offset_of_language_13(),
	XmlSchemaDocumentation_tF5EE1377C73D6ADCDF6142903E593FB6CCFAE15E::get_offset_of_markup_14(),
	XmlSchemaDocumentation_tF5EE1377C73D6ADCDF6142903E593FB6CCFAE15E::get_offset_of_source_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1693[27] = 
{
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_block_27(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_constraints_28(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_defaultValue_29(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_elementType_30(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_elementSchemaType_31(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_final_32(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_fixedValue_33(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_form_34(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_isAbstract_35(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_isNillable_36(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_name_37(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_refName_38(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_schemaType_39(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_schemaTypeName_40(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_substitutionGroup_41(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_schema_42(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_parentIsSchema_43(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_qName_44(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_blockResolved_45(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_finalResolved_46(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_referencedElement_47(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_substitutingElements_48(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_substitutionGroupElement_49(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_actualIsAbstract_50(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_actualIsNillable_51(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_validatedDefaultValue_52(),
	XmlSchemaElement_t3FC59E33781324C566357FD7C739113242BAAD64::get_offset_of_validatedFixedValue_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (XmlSchemaEnumerationFacet_tB7AE5A8944818E8371CCD309658DCEF53595464F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1695[5] = 
{
	XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D::get_offset_of_hasLineInfo_11(),
	XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D::get_offset_of_lineNumber_12(),
	XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D::get_offset_of_linePosition_13(),
	XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D::get_offset_of_sourceObj_14(),
	XmlSchemaException_tCFA4C17B08E999ED8DF6B0AC8B6A9FE872401D7D::get_offset_of_sourceUri_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { sizeof (XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1696[3] = 
{
	XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9::get_offset_of_id_13(),
	XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9::get_offset_of_schema_14(),
	XmlSchemaExternal_tF75596B2A753B85E30D6F9675F05E107D9A287D9::get_offset_of_location_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318), -1, sizeof(XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1697[3] = 
{
	XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318_StaticFields::get_offset_of_AllFacets_16(),
	XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318::get_offset_of_isFixed_17(),
	XmlSchemaFacet_t44B0D153A389F7A8C0769773496C6D041D8C8318::get_offset_of_val_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (Facet_tC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1698[14] = 
{
	Facet_tC3A3E144DFA7F8AED31FB1073E83B5EF9F75A68B::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (XmlSchemaForm_t704AB79E1BD496D7F43D4687FBEC3B134884942E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1699[4] = 
{
	XmlSchemaForm_t704AB79E1BD496D7F43D4687FBEC3B134884942E::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif

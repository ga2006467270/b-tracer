﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BDSLiteSpeech
struct BDSLiteSpeech_t0080FF8C29E1F3373FB64E23EC48B7B6A3F78B24;
// BTracerBeacon
struct BTracerBeacon_tC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD;
// CardManager
struct CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874;
// CompleteImageSetter
struct CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82;
// DownloadImage
struct DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13;
// DownloadImage[]
struct DownloadImageU5BU5D_tF90EE40BF0A832B33C97DD9D009B2B83FF6FB7D0;
// Example
struct Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF;
// GameManage
struct GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965;
// Letters
struct Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478;
// LoadRank
struct LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52;
// LoginUI
struct LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A;
// MissionButtonData
struct MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9;
// MissionData
struct MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C;
// MissionManager
struct MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394;
// MissionRecordButtonData
struct MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A;
// ObjectPool/ObjectStore
struct ObjectStore_tB7EE0D6CBA7CBBBA830724C5792B309AF079DEF4;
// ObjectPool/SimpleObjectFactory
struct SimpleObjectFactory_tAAB121CEB0D927DEA868CFB01C52A50A1C7C7132;
// PlayerData
struct PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043;
// PlayerMode
struct PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97;
// RsourceStores
struct RsourceStores_t7DF0BF48D6C2C9A7496994994A0A55517E309385;
// SimpleJSON.JSONClass
struct JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F;
// SimpleJSON.JSONNode
struct JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889;
// SimpleResourceFactory
struct SimpleResourceFactory_tA31CC8DA5DB8C7190B9EE7E66AA4D570C1C71D14;
// System.AsyncCallback
struct AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.Generic.Dictionary`2<System.String,LetterController>
struct Dictionary_2_t0B02B7BF82B091F8E0E26832A55B0F39430FFDB7;
// System.Collections.Generic.Dictionary`2<System.String,SingleBeaconData>
struct Dictionary_2_t47BBAA0828324FDBA07443A67602C01FCF1F76B2;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84;
// System.Collections.Generic.List`1<BeaconLetter>
struct List_1_t0E3039C0F56CB4633248FB700B01E40BE529D0BA;
// System.Collections.Generic.List`1<LetterController>
struct List_1_t2B2E09A49BE9B02E03E870B244E8D605B1DA39CA;
// System.Collections.Generic.List`1<SingleBeaconData>
struct List_1_tDC5994943EC2B9DFB96A4B058CC95C00EC1D06E2;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t4A961510635950236678F1B3B2436ECCAF28713A;
// System.Collections.Generic.List`1<System.String>
struct List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955;
// System.DelegateData
struct DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3;
// System.IAsyncResult
struct IAsyncResult_tDA33C24465239FB383C4C2CDAAC43B9AD3CB7F05;
// System.Int32[]
struct Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067;
// UnityEngine.Gyroscope
struct Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t726E134F16701A2671D40BEBE22110DC57156353;
// UnityEngine.Object[]
struct ObjectU5BU5D_tE519E5BBCA48F8FEAE68926638261BD14A981AB9;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t06431062CF438D12908F0B93305795CB645DCCA8;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_tE9BB282384F0196211AD1A480477254188211F57;
// UnityEngine.RectOffset
struct RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017;
// UnityEngine.TextMesh[]
struct TextMeshU5BU5D_t6C46DABCE2E499BCAB13DDDEC1393FDE4A69DAAE;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>
struct ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.WWWForm
struct WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24;
// UnityEngine.WebCamTexture
struct WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73;
// cameraApp
struct cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7;




#ifndef U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#define U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T6CDDDF959E7E18A6744E43B613F41CDAC780256A_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef BTRACERBEACON_TC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD_H
#define BTRACERBEACON_TC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BTracerBeacon
struct  BTracerBeacon_tC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<SingleBeaconData> BTracerBeacon::BeaconName
	List_1_tDC5994943EC2B9DFB96A4B058CC95C00EC1D06E2 * ___BeaconName_0;
	// System.Collections.Generic.Dictionary`2<System.String,SingleBeaconData> BTracerBeacon::BeaconDic
	Dictionary_2_t47BBAA0828324FDBA07443A67602C01FCF1F76B2 * ___BeaconDic_1;

public:
	inline static int32_t get_offset_of_BeaconName_0() { return static_cast<int32_t>(offsetof(BTracerBeacon_tC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD, ___BeaconName_0)); }
	inline List_1_tDC5994943EC2B9DFB96A4B058CC95C00EC1D06E2 * get_BeaconName_0() const { return ___BeaconName_0; }
	inline List_1_tDC5994943EC2B9DFB96A4B058CC95C00EC1D06E2 ** get_address_of_BeaconName_0() { return &___BeaconName_0; }
	inline void set_BeaconName_0(List_1_tDC5994943EC2B9DFB96A4B058CC95C00EC1D06E2 * value)
	{
		___BeaconName_0 = value;
		Il2CppCodeGenWriteBarrier((&___BeaconName_0), value);
	}

	inline static int32_t get_offset_of_BeaconDic_1() { return static_cast<int32_t>(offsetof(BTracerBeacon_tC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD, ___BeaconDic_1)); }
	inline Dictionary_2_t47BBAA0828324FDBA07443A67602C01FCF1F76B2 * get_BeaconDic_1() const { return ___BeaconDic_1; }
	inline Dictionary_2_t47BBAA0828324FDBA07443A67602C01FCF1F76B2 ** get_address_of_BeaconDic_1() { return &___BeaconDic_1; }
	inline void set_BeaconDic_1(Dictionary_2_t47BBAA0828324FDBA07443A67602C01FCF1F76B2 * value)
	{
		___BeaconDic_1 = value;
		Il2CppCodeGenWriteBarrier((&___BeaconDic_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BTRACERBEACON_TC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD_H
#ifndef U3CCHECKCARDPANELDONEU3EC__ITERATOR1_TD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA_H
#define U3CCHECKCARDPANELDONEU3EC__ITERATOR1_TD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardManager_<CheckCardPanelDone>c__Iterator1
struct  U3CCheckCardPanelDoneU3Ec__Iterator1_tD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA  : public RuntimeObject
{
public:
	// CardManager CardManager_<CheckCardPanelDone>c__Iterator1::U24this
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874 * ___U24this_0;
	// System.Object CardManager_<CheckCardPanelDone>c__Iterator1::U24current
	RuntimeObject * ___U24current_1;
	// System.Boolean CardManager_<CheckCardPanelDone>c__Iterator1::U24disposing
	bool ___U24disposing_2;
	// System.Int32 CardManager_<CheckCardPanelDone>c__Iterator1::U24PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCheckCardPanelDoneU3Ec__Iterator1_tD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA, ___U24this_0)); }
	inline CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874 * get_U24this_0() const { return ___U24this_0; }
	inline CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCheckCardPanelDoneU3Ec__Iterator1_tD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCheckCardPanelDoneU3Ec__Iterator1_tD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCheckCardPanelDoneU3Ec__Iterator1_tD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKCARDPANELDONEU3EC__ITERATOR1_TD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA_H
#ifndef U3CSPEECHDELAYU3EC__ITERATOR0_T46456C049571B0F488EA7AA99B84EB797615E4FB_H
#define U3CSPEECHDELAYU3EC__ITERATOR0_T46456C049571B0F488EA7AA99B84EB797615E4FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardManager_<SpeechDelay>c__Iterator0
struct  U3CSpeechDelayU3Ec__Iterator0_t46456C049571B0F488EA7AA99B84EB797615E4FB  : public RuntimeObject
{
public:
	// CardManager CardManager_<SpeechDelay>c__Iterator0::U24this
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874 * ___U24this_0;
	// System.Object CardManager_<SpeechDelay>c__Iterator0::U24current
	RuntimeObject * ___U24current_1;
	// System.Boolean CardManager_<SpeechDelay>c__Iterator0::U24disposing
	bool ___U24disposing_2;
	// System.Int32 CardManager_<SpeechDelay>c__Iterator0::U24PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CSpeechDelayU3Ec__Iterator0_t46456C049571B0F488EA7AA99B84EB797615E4FB, ___U24this_0)); }
	inline CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874 * get_U24this_0() const { return ___U24this_0; }
	inline CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CSpeechDelayU3Ec__Iterator0_t46456C049571B0F488EA7AA99B84EB797615E4FB, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CSpeechDelayU3Ec__Iterator0_t46456C049571B0F488EA7AA99B84EB797615E4FB, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CSpeechDelayU3Ec__Iterator0_t46456C049571B0F488EA7AA99B84EB797615E4FB, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPEECHDELAYU3EC__ITERATOR0_T46456C049571B0F488EA7AA99B84EB797615E4FB_H
#ifndef U3CSTARTGAMEU3EC__ITERATOR0_T80F411FED94268ED4351291DE5D0EBC0BB64413D_H
#define U3CSTARTGAMEU3EC__ITERATOR0_T80F411FED94268ED4351291DE5D0EBC0BB64413D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManage_<StartGame>c__Iterator0
struct  U3CStartGameU3Ec__Iterator0_t80F411FED94268ED4351291DE5D0EBC0BB64413D  : public RuntimeObject
{
public:
	// GameManage GameManage_<StartGame>c__Iterator0::U24this
	GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965 * ___U24this_0;
	// System.Object GameManage_<StartGame>c__Iterator0::U24current
	RuntimeObject * ___U24current_1;
	// System.Boolean GameManage_<StartGame>c__Iterator0::U24disposing
	bool ___U24disposing_2;
	// System.Int32 GameManage_<StartGame>c__Iterator0::U24PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartGameU3Ec__Iterator0_t80F411FED94268ED4351291DE5D0EBC0BB64413D, ___U24this_0)); }
	inline GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965 * get_U24this_0() const { return ___U24this_0; }
	inline GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartGameU3Ec__Iterator0_t80F411FED94268ED4351291DE5D0EBC0BB64413D, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartGameU3Ec__Iterator0_t80F411FED94268ED4351291DE5D0EBC0BB64413D, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartGameU3Ec__Iterator0_t80F411FED94268ED4351291DE5D0EBC0BB64413D, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTGAMEU3EC__ITERATOR0_T80F411FED94268ED4351291DE5D0EBC0BB64413D_H
#ifndef INSTANCIATATEOBJECT_TFEE44005F6AE21D6C4674392F2C9BBB70F5214CA_H
#define INSTANCIATATEOBJECT_TFEE44005F6AE21D6C4674392F2C9BBB70F5214CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstanciatateObject
struct  InstanciatateObject_tFEE44005F6AE21D6C4674392F2C9BBB70F5214CA  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCIATATEOBJECT_TFEE44005F6AE21D6C4674392F2C9BBB70F5214CA_H
#ifndef LETTERS_T0A8693D7BA7607D25A2F070A9FE97809097A4478_H
#define LETTERS_T0A8693D7BA7607D25A2F070A9FE97809097A4478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Letters
struct  Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Letters::mTextDic
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___mTextDic_0;
	// System.Collections.Generic.Dictionary`2<System.String,LetterController> Letters::LetterControllerDic
	Dictionary_2_t0B02B7BF82B091F8E0E26832A55B0F39430FFDB7 * ___LetterControllerDic_1;
	// System.Collections.Generic.List`1<LetterController> Letters::LetterArray
	List_1_t2B2E09A49BE9B02E03E870B244E8D605B1DA39CA * ___LetterArray_2;

public:
	inline static int32_t get_offset_of_mTextDic_0() { return static_cast<int32_t>(offsetof(Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478, ___mTextDic_0)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_mTextDic_0() const { return ___mTextDic_0; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_mTextDic_0() { return &___mTextDic_0; }
	inline void set_mTextDic_0(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___mTextDic_0 = value;
		Il2CppCodeGenWriteBarrier((&___mTextDic_0), value);
	}

	inline static int32_t get_offset_of_LetterControllerDic_1() { return static_cast<int32_t>(offsetof(Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478, ___LetterControllerDic_1)); }
	inline Dictionary_2_t0B02B7BF82B091F8E0E26832A55B0F39430FFDB7 * get_LetterControllerDic_1() const { return ___LetterControllerDic_1; }
	inline Dictionary_2_t0B02B7BF82B091F8E0E26832A55B0F39430FFDB7 ** get_address_of_LetterControllerDic_1() { return &___LetterControllerDic_1; }
	inline void set_LetterControllerDic_1(Dictionary_2_t0B02B7BF82B091F8E0E26832A55B0F39430FFDB7 * value)
	{
		___LetterControllerDic_1 = value;
		Il2CppCodeGenWriteBarrier((&___LetterControllerDic_1), value);
	}

	inline static int32_t get_offset_of_LetterArray_2() { return static_cast<int32_t>(offsetof(Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478, ___LetterArray_2)); }
	inline List_1_t2B2E09A49BE9B02E03E870B244E8D605B1DA39CA * get_LetterArray_2() const { return ___LetterArray_2; }
	inline List_1_t2B2E09A49BE9B02E03E870B244E8D605B1DA39CA ** get_address_of_LetterArray_2() { return &___LetterArray_2; }
	inline void set_LetterArray_2(List_1_t2B2E09A49BE9B02E03E870B244E8D605B1DA39CA * value)
	{
		___LetterArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___LetterArray_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERS_T0A8693D7BA7607D25A2F070A9FE97809097A4478_H
#ifndef U3CFADELOGOU3EC__ITERATOR2_T8CCA0AFFF49E100E0505CECFEBFB840382A3471D_H
#define U3CFADELOGOU3EC__ITERATOR2_T8CCA0AFFF49E100E0505CECFEBFB840382A3471D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginUI_<FadeLogo>c__Iterator2
struct  U3CFadeLogoU3Ec__Iterator2_t8CCA0AFFF49E100E0505CECFEBFB840382A3471D  : public RuntimeObject
{
public:
	// LoginUI LoginUI_<FadeLogo>c__Iterator2::U24this
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A * ___U24this_0;
	// System.Object LoginUI_<FadeLogo>c__Iterator2::U24current
	RuntimeObject * ___U24current_1;
	// System.Boolean LoginUI_<FadeLogo>c__Iterator2::U24disposing
	bool ___U24disposing_2;
	// System.Int32 LoginUI_<FadeLogo>c__Iterator2::U24PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CFadeLogoU3Ec__Iterator2_t8CCA0AFFF49E100E0505CECFEBFB840382A3471D, ___U24this_0)); }
	inline LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A * get_U24this_0() const { return ___U24this_0; }
	inline LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CFadeLogoU3Ec__Iterator2_t8CCA0AFFF49E100E0505CECFEBFB840382A3471D, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CFadeLogoU3Ec__Iterator2_t8CCA0AFFF49E100E0505CECFEBFB840382A3471D, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CFadeLogoU3Ec__Iterator2_t8CCA0AFFF49E100E0505CECFEBFB840382A3471D, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADELOGOU3EC__ITERATOR2_T8CCA0AFFF49E100E0505CECFEBFB840382A3471D_H
#ifndef U3CLOGINU3EC__ITERATOR0_T349BFA2D6021C456F066AD617610D02F6793C228_H
#define U3CLOGINU3EC__ITERATOR0_T349BFA2D6021C456F066AD617610D02F6793C228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginUI_<Login>c__Iterator0
struct  U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm LoginUI_<Login>c__Iterator0::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass LoginUI_<Login>c__Iterator0::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// System.String LoginUI_<Login>c__Iterator0::<Platform>__0
	String_t* ___U3CPlatformU3E__0_2;
	// UnityEngine.Networking.UnityWebRequest LoginUI_<Login>c__Iterator0::<LoginWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CLoginWWWU3E__1_3;
	// System.Boolean LoginUI_<Login>c__Iterator0::IsLogin
	bool ___IsLogin_4;
	// SimpleJSON.JSONNode LoginUI_<Login>c__Iterator0::<Result>__2
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U3CResultU3E__2_5;
	// LoginUI LoginUI_<Login>c__Iterator0::U24this
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A * ___U24this_6;
	// System.Object LoginUI_<Login>c__Iterator0::U24current
	RuntimeObject * ___U24current_7;
	// System.Boolean LoginUI_<Login>c__Iterator0::U24disposing
	bool ___U24disposing_8;
	// System.Int32 LoginUI_<Login>c__Iterator0::U24PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CPlatformU3E__0_2() { return static_cast<int32_t>(offsetof(U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228, ___U3CPlatformU3E__0_2)); }
	inline String_t* get_U3CPlatformU3E__0_2() const { return ___U3CPlatformU3E__0_2; }
	inline String_t** get_address_of_U3CPlatformU3E__0_2() { return &___U3CPlatformU3E__0_2; }
	inline void set_U3CPlatformU3E__0_2(String_t* value)
	{
		___U3CPlatformU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPlatformU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CLoginWWWU3E__1_3() { return static_cast<int32_t>(offsetof(U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228, ___U3CLoginWWWU3E__1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CLoginWWWU3E__1_3() const { return ___U3CLoginWWWU3E__1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CLoginWWWU3E__1_3() { return &___U3CLoginWWWU3E__1_3; }
	inline void set_U3CLoginWWWU3E__1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CLoginWWWU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoginWWWU3E__1_3), value);
	}

	inline static int32_t get_offset_of_IsLogin_4() { return static_cast<int32_t>(offsetof(U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228, ___IsLogin_4)); }
	inline bool get_IsLogin_4() const { return ___IsLogin_4; }
	inline bool* get_address_of_IsLogin_4() { return &___IsLogin_4; }
	inline void set_IsLogin_4(bool value)
	{
		___IsLogin_4 = value;
	}

	inline static int32_t get_offset_of_U3CResultU3E__2_5() { return static_cast<int32_t>(offsetof(U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228, ___U3CResultU3E__2_5)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U3CResultU3E__2_5() const { return ___U3CResultU3E__2_5; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U3CResultU3E__2_5() { return &___U3CResultU3E__2_5; }
	inline void set_U3CResultU3E__2_5(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U3CResultU3E__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultU3E__2_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228, ___U24this_6)); }
	inline LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A * get_U24this_6() const { return ___U24this_6; }
	inline LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOGINU3EC__ITERATOR0_T349BFA2D6021C456F066AD617610D02F6793C228_H
#ifndef U3CSHOWLOGOU3EC__ITERATOR1_T09D132CBFC300F1D382716F620F65347F2CC168B_H
#define U3CSHOWLOGOU3EC__ITERATOR1_T09D132CBFC300F1D382716F620F65347F2CC168B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginUI_<ShowLogo>c__Iterator1
struct  U3CShowLogoU3Ec__Iterator1_t09D132CBFC300F1D382716F620F65347F2CC168B  : public RuntimeObject
{
public:
	// LoginUI LoginUI_<ShowLogo>c__Iterator1::U24this
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A * ___U24this_0;
	// System.Object LoginUI_<ShowLogo>c__Iterator1::U24current
	RuntimeObject * ___U24current_1;
	// System.Boolean LoginUI_<ShowLogo>c__Iterator1::U24disposing
	bool ___U24disposing_2;
	// System.Int32 LoginUI_<ShowLogo>c__Iterator1::U24PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CShowLogoU3Ec__Iterator1_t09D132CBFC300F1D382716F620F65347F2CC168B, ___U24this_0)); }
	inline LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A * get_U24this_0() const { return ___U24this_0; }
	inline LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CShowLogoU3Ec__Iterator1_t09D132CBFC300F1D382716F620F65347F2CC168B, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CShowLogoU3Ec__Iterator1_t09D132CBFC300F1D382716F620F65347F2CC168B, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CShowLogoU3Ec__Iterator1_t09D132CBFC300F1D382716F620F65347F2CC168B, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWLOGOU3EC__ITERATOR1_T09D132CBFC300F1D382716F620F65347F2CC168B_H
#ifndef MISSIONDATA_T1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C_H
#define MISSIONDATA_T1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionData
struct  MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C  : public RuntimeObject
{
public:
	// Letters MissionData::mLetters
	Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478 * ___mLetters_0;
	// System.Int32 MissionData::MissionNumber
	int32_t ___MissionNumber_1;
	// System.String MissionData::MissionName
	String_t* ___MissionName_2;
	// System.Boolean MissionData::IsDo
	bool ___IsDo_3;

public:
	inline static int32_t get_offset_of_mLetters_0() { return static_cast<int32_t>(offsetof(MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C, ___mLetters_0)); }
	inline Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478 * get_mLetters_0() const { return ___mLetters_0; }
	inline Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478 ** get_address_of_mLetters_0() { return &___mLetters_0; }
	inline void set_mLetters_0(Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478 * value)
	{
		___mLetters_0 = value;
		Il2CppCodeGenWriteBarrier((&___mLetters_0), value);
	}

	inline static int32_t get_offset_of_MissionNumber_1() { return static_cast<int32_t>(offsetof(MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C, ___MissionNumber_1)); }
	inline int32_t get_MissionNumber_1() const { return ___MissionNumber_1; }
	inline int32_t* get_address_of_MissionNumber_1() { return &___MissionNumber_1; }
	inline void set_MissionNumber_1(int32_t value)
	{
		___MissionNumber_1 = value;
	}

	inline static int32_t get_offset_of_MissionName_2() { return static_cast<int32_t>(offsetof(MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C, ___MissionName_2)); }
	inline String_t* get_MissionName_2() const { return ___MissionName_2; }
	inline String_t** get_address_of_MissionName_2() { return &___MissionName_2; }
	inline void set_MissionName_2(String_t* value)
	{
		___MissionName_2 = value;
		Il2CppCodeGenWriteBarrier((&___MissionName_2), value);
	}

	inline static int32_t get_offset_of_IsDo_3() { return static_cast<int32_t>(offsetof(MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C, ___IsDo_3)); }
	inline bool get_IsDo_3() const { return ___IsDo_3; }
	inline bool* get_address_of_IsDo_3() { return &___IsDo_3; }
	inline void set_IsDo_3(bool value)
	{
		___IsDo_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSIONDATA_T1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C_H
#ifndef U3CCHECKCANCLOSEMISSIONU3EC__ITERATOR3_T8A7FE58733D9CC1269B70A63BAED63ED5A9E335A_H
#define U3CCHECKCANCLOSEMISSIONU3EC__ITERATOR3_T8A7FE58733D9CC1269B70A63BAED63ED5A9E335A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager_<CheckCanCloseMission>c__Iterator3
struct  U3CCheckCanCloseMissionU3Ec__Iterator3_t8A7FE58733D9CC1269B70A63BAED63ED5A9E335A  : public RuntimeObject
{
public:
	// MissionManager MissionManager_<CheckCanCloseMission>c__Iterator3::U24this
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * ___U24this_0;
	// System.Object MissionManager_<CheckCanCloseMission>c__Iterator3::U24current
	RuntimeObject * ___U24current_1;
	// System.Boolean MissionManager_<CheckCanCloseMission>c__Iterator3::U24disposing
	bool ___U24disposing_2;
	// System.Int32 MissionManager_<CheckCanCloseMission>c__Iterator3::U24PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCheckCanCloseMissionU3Ec__Iterator3_t8A7FE58733D9CC1269B70A63BAED63ED5A9E335A, ___U24this_0)); }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * get_U24this_0() const { return ___U24this_0; }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCheckCanCloseMissionU3Ec__Iterator3_t8A7FE58733D9CC1269B70A63BAED63ED5A9E335A, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCheckCanCloseMissionU3Ec__Iterator3_t8A7FE58733D9CC1269B70A63BAED63ED5A9E335A, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCheckCanCloseMissionU3Ec__Iterator3_t8A7FE58733D9CC1269B70A63BAED63ED5A9E335A, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKCANCLOSEMISSIONU3EC__ITERATOR3_T8A7FE58733D9CC1269B70A63BAED63ED5A9E335A_H
#ifndef U3CPREPARESERILIZEIMGU3EC__ITERATOR9_T97394C2D06F5B89CC74FF4973765C20482922A5F_H
#define U3CPREPARESERILIZEIMGU3EC__ITERATOR9_T97394C2D06F5B89CC74FF4973765C20482922A5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager_<PrepareSerilizeImg>c__Iterator9
struct  U3CPrepareSerilizeImgU3Ec__Iterator9_t97394C2D06F5B89CC74FF4973765C20482922A5F  : public RuntimeObject
{
public:
	// System.Boolean MissionManager_<PrepareSerilizeImg>c__Iterator9::<checkcardImg>__0
	bool ___U3CcheckcardImgU3E__0_0;
	// MissionManager MissionManager_<PrepareSerilizeImg>c__Iterator9::U24this
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * ___U24this_1;
	// System.Object MissionManager_<PrepareSerilizeImg>c__Iterator9::U24current
	RuntimeObject * ___U24current_2;
	// System.Boolean MissionManager_<PrepareSerilizeImg>c__Iterator9::U24disposing
	bool ___U24disposing_3;
	// System.Int32 MissionManager_<PrepareSerilizeImg>c__Iterator9::U24PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcheckcardImgU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPrepareSerilizeImgU3Ec__Iterator9_t97394C2D06F5B89CC74FF4973765C20482922A5F, ___U3CcheckcardImgU3E__0_0)); }
	inline bool get_U3CcheckcardImgU3E__0_0() const { return ___U3CcheckcardImgU3E__0_0; }
	inline bool* get_address_of_U3CcheckcardImgU3E__0_0() { return &___U3CcheckcardImgU3E__0_0; }
	inline void set_U3CcheckcardImgU3E__0_0(bool value)
	{
		___U3CcheckcardImgU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CPrepareSerilizeImgU3Ec__Iterator9_t97394C2D06F5B89CC74FF4973765C20482922A5F, ___U24this_1)); }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * get_U24this_1() const { return ___U24this_1; }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CPrepareSerilizeImgU3Ec__Iterator9_t97394C2D06F5B89CC74FF4973765C20482922A5F, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CPrepareSerilizeImgU3Ec__Iterator9_t97394C2D06F5B89CC74FF4973765C20482922A5F, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CPrepareSerilizeImgU3Ec__Iterator9_t97394C2D06F5B89CC74FF4973765C20482922A5F, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPREPARESERILIZEIMGU3EC__ITERATOR9_T97394C2D06F5B89CC74FF4973765C20482922A5F_H
#ifndef U3CSAVEANSWERU3EC__ITERATOR7_T5C8AA9E1B3FE76AD060C7342CBAA29507BE86661_H
#define U3CSAVEANSWERU3EC__ITERATOR7_T5C8AA9E1B3FE76AD060C7342CBAA29507BE86661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager_<SaveAnswer>c__Iterator7
struct  U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm MissionManager_<SaveAnswer>c__Iterator7::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass MissionManager_<SaveAnswer>c__Iterator7::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// System.String MissionManager_<SaveAnswer>c__Iterator7::<savewording>__0
	String_t* ___U3CsavewordingU3E__0_2;
	// System.String MissionManager_<SaveAnswer>c__Iterator7::<playerOrder>__0
	String_t* ___U3CplayerOrderU3E__0_3;
	// System.String MissionManager_<SaveAnswer>c__Iterator7::<enableList>__0
	String_t* ___U3CenableListU3E__0_4;
	// UnityEngine.Networking.UnityWebRequest MissionManager_<SaveAnswer>c__Iterator7::<LoginWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CLoginWWWU3E__1_5;
	// MissionManager MissionManager_<SaveAnswer>c__Iterator7::U24this
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * ___U24this_6;
	// System.Object MissionManager_<SaveAnswer>c__Iterator7::U24current
	RuntimeObject * ___U24current_7;
	// System.Boolean MissionManager_<SaveAnswer>c__Iterator7::U24disposing
	bool ___U24disposing_8;
	// System.Int32 MissionManager_<SaveAnswer>c__Iterator7::U24PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CsavewordingU3E__0_2() { return static_cast<int32_t>(offsetof(U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661, ___U3CsavewordingU3E__0_2)); }
	inline String_t* get_U3CsavewordingU3E__0_2() const { return ___U3CsavewordingU3E__0_2; }
	inline String_t** get_address_of_U3CsavewordingU3E__0_2() { return &___U3CsavewordingU3E__0_2; }
	inline void set_U3CsavewordingU3E__0_2(String_t* value)
	{
		___U3CsavewordingU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsavewordingU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CplayerOrderU3E__0_3() { return static_cast<int32_t>(offsetof(U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661, ___U3CplayerOrderU3E__0_3)); }
	inline String_t* get_U3CplayerOrderU3E__0_3() const { return ___U3CplayerOrderU3E__0_3; }
	inline String_t** get_address_of_U3CplayerOrderU3E__0_3() { return &___U3CplayerOrderU3E__0_3; }
	inline void set_U3CplayerOrderU3E__0_3(String_t* value)
	{
		___U3CplayerOrderU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplayerOrderU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CenableListU3E__0_4() { return static_cast<int32_t>(offsetof(U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661, ___U3CenableListU3E__0_4)); }
	inline String_t* get_U3CenableListU3E__0_4() const { return ___U3CenableListU3E__0_4; }
	inline String_t** get_address_of_U3CenableListU3E__0_4() { return &___U3CenableListU3E__0_4; }
	inline void set_U3CenableListU3E__0_4(String_t* value)
	{
		___U3CenableListU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CenableListU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CLoginWWWU3E__1_5() { return static_cast<int32_t>(offsetof(U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661, ___U3CLoginWWWU3E__1_5)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CLoginWWWU3E__1_5() const { return ___U3CLoginWWWU3E__1_5; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CLoginWWWU3E__1_5() { return &___U3CLoginWWWU3E__1_5; }
	inline void set_U3CLoginWWWU3E__1_5(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CLoginWWWU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoginWWWU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661, ___U24this_6)); }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * get_U24this_6() const { return ___U24this_6; }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVEANSWERU3EC__ITERATOR7_T5C8AA9E1B3FE76AD060C7342CBAA29507BE86661_H
#ifndef U3CDOWNLOADMISSIONIMGU3EC__ITERATOR8_T19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7_H
#define U3CDOWNLOADMISSIONIMGU3EC__ITERATOR8_T19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager_<downloadMissionImg>c__Iterator8
struct  U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest MissionManager_<downloadMissionImg>c__Iterator8::<request>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CrequestU3E__1_0;
	// System.String MissionManager_<downloadMissionImg>c__Iterator8::url
	String_t* ___url_1;
	// UnityEngine.Networking.UnityWebRequestAsyncOperation MissionManager_<downloadMissionImg>c__Iterator8::<asyncOp>__2
	UnityWebRequestAsyncOperation_t726E134F16701A2671D40BEBE22110DC57156353 * ___U3CasyncOpU3E__2_2;
	// System.Int32 MissionManager_<downloadMissionImg>c__Iterator8::count
	int32_t ___count_3;
	// System.Boolean MissionManager_<downloadMissionImg>c__Iterator8::CardLoadDone
	bool ___CardLoadDone_4;
	// MissionManager MissionManager_<downloadMissionImg>c__Iterator8::U24this
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * ___U24this_5;
	// System.Object MissionManager_<downloadMissionImg>c__Iterator8::U24current
	RuntimeObject * ___U24current_6;
	// System.Boolean MissionManager_<downloadMissionImg>c__Iterator8::U24disposing
	bool ___U24disposing_7;
	// System.Int32 MissionManager_<downloadMissionImg>c__Iterator8::U24PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CrequestU3E__1_0() { return static_cast<int32_t>(offsetof(U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7, ___U3CrequestU3E__1_0)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CrequestU3E__1_0() const { return ___U3CrequestU3E__1_0; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CrequestU3E__1_0() { return &___U3CrequestU3E__1_0; }
	inline void set_U3CrequestU3E__1_0(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CrequestU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrequestU3E__1_0), value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier((&___url_1), value);
	}

	inline static int32_t get_offset_of_U3CasyncOpU3E__2_2() { return static_cast<int32_t>(offsetof(U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7, ___U3CasyncOpU3E__2_2)); }
	inline UnityWebRequestAsyncOperation_t726E134F16701A2671D40BEBE22110DC57156353 * get_U3CasyncOpU3E__2_2() const { return ___U3CasyncOpU3E__2_2; }
	inline UnityWebRequestAsyncOperation_t726E134F16701A2671D40BEBE22110DC57156353 ** get_address_of_U3CasyncOpU3E__2_2() { return &___U3CasyncOpU3E__2_2; }
	inline void set_U3CasyncOpU3E__2_2(UnityWebRequestAsyncOperation_t726E134F16701A2671D40BEBE22110DC57156353 * value)
	{
		___U3CasyncOpU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CasyncOpU3E__2_2), value);
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7, ___count_3)); }
	inline int32_t get_count_3() const { return ___count_3; }
	inline int32_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int32_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_CardLoadDone_4() { return static_cast<int32_t>(offsetof(U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7, ___CardLoadDone_4)); }
	inline bool get_CardLoadDone_4() const { return ___CardLoadDone_4; }
	inline bool* get_address_of_CardLoadDone_4() { return &___CardLoadDone_4; }
	inline void set_CardLoadDone_4(bool value)
	{
		___CardLoadDone_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7, ___U24this_5)); }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * get_U24this_5() const { return ___U24this_5; }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADMISSIONIMGU3EC__ITERATOR8_T19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7_H
#ifndef U3CLOADLETTERU3EC__ITERATOR2_T619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439_H
#define U3CLOADLETTERU3EC__ITERATOR2_T619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager_<loadLetter>c__Iterator2
struct  U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm MissionManager_<loadLetter>c__Iterator2::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass MissionManager_<loadLetter>c__Iterator2::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// System.Int32 MissionManager_<loadLetter>c__Iterator2::missionNumber
	int32_t ___missionNumber_2;
	// UnityEngine.Networking.UnityWebRequest MissionManager_<loadLetter>c__Iterator2::<LoginWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CLoginWWWU3E__1_3;
	// MissionManager MissionManager_<loadLetter>c__Iterator2::U24this
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * ___U24this_4;
	// System.Object MissionManager_<loadLetter>c__Iterator2::U24current
	RuntimeObject * ___U24current_5;
	// System.Boolean MissionManager_<loadLetter>c__Iterator2::U24disposing
	bool ___U24disposing_6;
	// System.Int32 MissionManager_<loadLetter>c__Iterator2::U24PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_missionNumber_2() { return static_cast<int32_t>(offsetof(U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439, ___missionNumber_2)); }
	inline int32_t get_missionNumber_2() const { return ___missionNumber_2; }
	inline int32_t* get_address_of_missionNumber_2() { return &___missionNumber_2; }
	inline void set_missionNumber_2(int32_t value)
	{
		___missionNumber_2 = value;
	}

	inline static int32_t get_offset_of_U3CLoginWWWU3E__1_3() { return static_cast<int32_t>(offsetof(U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439, ___U3CLoginWWWU3E__1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CLoginWWWU3E__1_3() const { return ___U3CLoginWWWU3E__1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CLoginWWWU3E__1_3() { return &___U3CLoginWWWU3E__1_3; }
	inline void set_U3CLoginWWWU3E__1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CLoginWWWU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoginWWWU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439, ___U24this_4)); }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * get_U24this_4() const { return ___U24this_4; }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADLETTERU3EC__ITERATOR2_T619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439_H
#ifndef U3CLOADRECORDSU3EC__ITERATOR1_T500DDD544F074F70A1AA1A273122A43EEB810741_H
#define U3CLOADRECORDSU3EC__ITERATOR1_T500DDD544F074F70A1AA1A273122A43EEB810741_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager_<loadRecords>c__Iterator1
struct  U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm MissionManager_<loadRecords>c__Iterator1::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass MissionManager_<loadRecords>c__Iterator1::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// System.Int32 MissionManager_<loadRecords>c__Iterator1::missionNumber
	int32_t ___missionNumber_2;
	// UnityEngine.Networking.UnityWebRequest MissionManager_<loadRecords>c__Iterator1::<LoginWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CLoginWWWU3E__1_3;
	// MissionManager MissionManager_<loadRecords>c__Iterator1::U24this
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * ___U24this_4;
	// System.Object MissionManager_<loadRecords>c__Iterator1::U24current
	RuntimeObject * ___U24current_5;
	// System.Boolean MissionManager_<loadRecords>c__Iterator1::U24disposing
	bool ___U24disposing_6;
	// System.Int32 MissionManager_<loadRecords>c__Iterator1::U24PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_missionNumber_2() { return static_cast<int32_t>(offsetof(U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741, ___missionNumber_2)); }
	inline int32_t get_missionNumber_2() const { return ___missionNumber_2; }
	inline int32_t* get_address_of_missionNumber_2() { return &___missionNumber_2; }
	inline void set_missionNumber_2(int32_t value)
	{
		___missionNumber_2 = value;
	}

	inline static int32_t get_offset_of_U3CLoginWWWU3E__1_3() { return static_cast<int32_t>(offsetof(U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741, ___U3CLoginWWWU3E__1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CLoginWWWU3E__1_3() const { return ___U3CLoginWWWU3E__1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CLoginWWWU3E__1_3() { return &___U3CLoginWWWU3E__1_3; }
	inline void set_U3CLoginWWWU3E__1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CLoginWWWU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoginWWWU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741, ___U24this_4)); }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * get_U24this_4() const { return ___U24this_4; }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADRECORDSU3EC__ITERATOR1_T500DDD544F074F70A1AA1A273122A43EEB810741_H
#ifndef U3CREADCHIBEACONDATAU3EC__ITERATORA_T106ABC44CA1E6B98B4E8BE70687C42D1052A3A66_H
#define U3CREADCHIBEACONDATAU3EC__ITERATORA_T106ABC44CA1E6B98B4E8BE70687C42D1052A3A66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager_<readChiBeaconData>c__IteratorA
struct  U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm MissionManager_<readChiBeaconData>c__IteratorA::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass MissionManager_<readChiBeaconData>c__IteratorA::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// UnityEngine.Networking.UnityWebRequest MissionManager_<readChiBeaconData>c__IteratorA::<LoginWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CLoginWWWU3E__1_2;
	// MissionManager MissionManager_<readChiBeaconData>c__IteratorA::U24this
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * ___U24this_3;
	// System.Object MissionManager_<readChiBeaconData>c__IteratorA::U24current
	RuntimeObject * ___U24current_4;
	// System.Boolean MissionManager_<readChiBeaconData>c__IteratorA::U24disposing
	bool ___U24disposing_5;
	// System.Int32 MissionManager_<readChiBeaconData>c__IteratorA::U24PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CLoginWWWU3E__1_2() { return static_cast<int32_t>(offsetof(U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66, ___U3CLoginWWWU3E__1_2)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CLoginWWWU3E__1_2() const { return ___U3CLoginWWWU3E__1_2; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CLoginWWWU3E__1_2() { return &___U3CLoginWWWU3E__1_2; }
	inline void set_U3CLoginWWWU3E__1_2(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CLoginWWWU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoginWWWU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66, ___U24this_3)); }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * get_U24this_3() const { return ___U24this_3; }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADCHIBEACONDATAU3EC__ITERATORA_T106ABC44CA1E6B98B4E8BE70687C42D1052A3A66_H
#ifndef U3CSAVELETTERSU3EC__ITERATOR6_T7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6_H
#define U3CSAVELETTERSU3EC__ITERATOR6_T7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager_<saveLetters>c__Iterator6
struct  U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm MissionManager_<saveLetters>c__Iterator6::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass MissionManager_<saveLetters>c__Iterator6::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// System.String MissionManager_<saveLetters>c__Iterator6::<chiLetterString>__0
	String_t* ___U3CchiLetterStringU3E__0_2;
	// System.String MissionManager_<saveLetters>c__Iterator6::<chiLetterNumString>__0
	String_t* ___U3CchiLetterNumStringU3E__0_3;
	// System.Collections.Generic.List`1<LetterController> MissionManager_<saveLetters>c__Iterator6::<letters>__0
	List_1_t2B2E09A49BE9B02E03E870B244E8D605B1DA39CA * ___U3ClettersU3E__0_4;
	// UnityEngine.Networking.UnityWebRequest MissionManager_<saveLetters>c__Iterator6::<LoginWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CLoginWWWU3E__1_5;
	// MissionManager MissionManager_<saveLetters>c__Iterator6::U24this
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * ___U24this_6;
	// System.Object MissionManager_<saveLetters>c__Iterator6::U24current
	RuntimeObject * ___U24current_7;
	// System.Boolean MissionManager_<saveLetters>c__Iterator6::U24disposing
	bool ___U24disposing_8;
	// System.Int32 MissionManager_<saveLetters>c__Iterator6::U24PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CchiLetterStringU3E__0_2() { return static_cast<int32_t>(offsetof(U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6, ___U3CchiLetterStringU3E__0_2)); }
	inline String_t* get_U3CchiLetterStringU3E__0_2() const { return ___U3CchiLetterStringU3E__0_2; }
	inline String_t** get_address_of_U3CchiLetterStringU3E__0_2() { return &___U3CchiLetterStringU3E__0_2; }
	inline void set_U3CchiLetterStringU3E__0_2(String_t* value)
	{
		___U3CchiLetterStringU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchiLetterStringU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CchiLetterNumStringU3E__0_3() { return static_cast<int32_t>(offsetof(U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6, ___U3CchiLetterNumStringU3E__0_3)); }
	inline String_t* get_U3CchiLetterNumStringU3E__0_3() const { return ___U3CchiLetterNumStringU3E__0_3; }
	inline String_t** get_address_of_U3CchiLetterNumStringU3E__0_3() { return &___U3CchiLetterNumStringU3E__0_3; }
	inline void set_U3CchiLetterNumStringU3E__0_3(String_t* value)
	{
		___U3CchiLetterNumStringU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CchiLetterNumStringU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3ClettersU3E__0_4() { return static_cast<int32_t>(offsetof(U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6, ___U3ClettersU3E__0_4)); }
	inline List_1_t2B2E09A49BE9B02E03E870B244E8D605B1DA39CA * get_U3ClettersU3E__0_4() const { return ___U3ClettersU3E__0_4; }
	inline List_1_t2B2E09A49BE9B02E03E870B244E8D605B1DA39CA ** get_address_of_U3ClettersU3E__0_4() { return &___U3ClettersU3E__0_4; }
	inline void set_U3ClettersU3E__0_4(List_1_t2B2E09A49BE9B02E03E870B244E8D605B1DA39CA * value)
	{
		___U3ClettersU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClettersU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CLoginWWWU3E__1_5() { return static_cast<int32_t>(offsetof(U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6, ___U3CLoginWWWU3E__1_5)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CLoginWWWU3E__1_5() const { return ___U3CLoginWWWU3E__1_5; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CLoginWWWU3E__1_5() { return &___U3CLoginWWWU3E__1_5; }
	inline void set_U3CLoginWWWU3E__1_5(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CLoginWWWU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoginWWWU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6, ___U24this_6)); }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * get_U24this_6() const { return ___U24this_6; }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSAVELETTERSU3EC__ITERATOR6_T7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6_H
#ifndef U3CUPLOADMOSTSTARU3EC__ITERATOR5_TCBA890EAFDC750477935DD4EC4DCCAB4F2997C09_H
#define U3CUPLOADMOSTSTARU3EC__ITERATOR5_TCBA890EAFDC750477935DD4EC4DCCAB4F2997C09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager_<upLoadMostStar>c__Iterator5
struct  U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm MissionManager_<upLoadMostStar>c__Iterator5::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass MissionManager_<upLoadMostStar>c__Iterator5::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// System.Int32 MissionManager_<upLoadMostStar>c__Iterator5::starAmount
	int32_t ___starAmount_2;
	// UnityEngine.Networking.UnityWebRequest MissionManager_<upLoadMostStar>c__Iterator5::<LoginWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CLoginWWWU3E__1_3;
	// MissionManager MissionManager_<upLoadMostStar>c__Iterator5::U24this
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * ___U24this_4;
	// System.Object MissionManager_<upLoadMostStar>c__Iterator5::U24current
	RuntimeObject * ___U24current_5;
	// System.Boolean MissionManager_<upLoadMostStar>c__Iterator5::U24disposing
	bool ___U24disposing_6;
	// System.Int32 MissionManager_<upLoadMostStar>c__Iterator5::U24PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_starAmount_2() { return static_cast<int32_t>(offsetof(U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09, ___starAmount_2)); }
	inline int32_t get_starAmount_2() const { return ___starAmount_2; }
	inline int32_t* get_address_of_starAmount_2() { return &___starAmount_2; }
	inline void set_starAmount_2(int32_t value)
	{
		___starAmount_2 = value;
	}

	inline static int32_t get_offset_of_U3CLoginWWWU3E__1_3() { return static_cast<int32_t>(offsetof(U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09, ___U3CLoginWWWU3E__1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CLoginWWWU3E__1_3() const { return ___U3CLoginWWWU3E__1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CLoginWWWU3E__1_3() { return &___U3CLoginWWWU3E__1_3; }
	inline void set_U3CLoginWWWU3E__1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CLoginWWWU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoginWWWU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09, ___U24this_4)); }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * get_U24this_4() const { return ___U24this_4; }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPLOADMOSTSTARU3EC__ITERATOR5_TCBA890EAFDC750477935DD4EC4DCCAB4F2997C09_H
#ifndef MYVAR_TFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_H
#define MYVAR_TFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MyVar
struct  MyVar_tFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA  : public RuntimeObject
{
public:

public:
};

struct MyVar_tFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_StaticFields
{
public:
	// System.String MyVar::Server
	String_t* ___Server_0;
	// System.String MyVar::PHPURL
	String_t* ___PHPURL_1;
	// System.Int32 MyVar::playerID
	int32_t ___playerID_2;
	// System.String MyVar::version
	String_t* ___version_3;

public:
	inline static int32_t get_offset_of_Server_0() { return static_cast<int32_t>(offsetof(MyVar_tFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_StaticFields, ___Server_0)); }
	inline String_t* get_Server_0() const { return ___Server_0; }
	inline String_t** get_address_of_Server_0() { return &___Server_0; }
	inline void set_Server_0(String_t* value)
	{
		___Server_0 = value;
		Il2CppCodeGenWriteBarrier((&___Server_0), value);
	}

	inline static int32_t get_offset_of_PHPURL_1() { return static_cast<int32_t>(offsetof(MyVar_tFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_StaticFields, ___PHPURL_1)); }
	inline String_t* get_PHPURL_1() const { return ___PHPURL_1; }
	inline String_t** get_address_of_PHPURL_1() { return &___PHPURL_1; }
	inline void set_PHPURL_1(String_t* value)
	{
		___PHPURL_1 = value;
		Il2CppCodeGenWriteBarrier((&___PHPURL_1), value);
	}

	inline static int32_t get_offset_of_playerID_2() { return static_cast<int32_t>(offsetof(MyVar_tFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_StaticFields, ___playerID_2)); }
	inline int32_t get_playerID_2() const { return ___playerID_2; }
	inline int32_t* get_address_of_playerID_2() { return &___playerID_2; }
	inline void set_playerID_2(int32_t value)
	{
		___playerID_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(MyVar_tFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_StaticFields, ___version_3)); }
	inline String_t* get_version_3() const { return ___version_3; }
	inline String_t** get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(String_t* value)
	{
		___version_3 = value;
		Il2CppCodeGenWriteBarrier((&___version_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MYVAR_TFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_H
#ifndef OBJECTLOADMOVE_T3BE2A335AE97C6B40CAB3E2CDD0639B7924110CF_H
#define OBJECTLOADMOVE_T3BE2A335AE97C6B40CAB3E2CDD0639B7924110CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectLoadMove
struct  ObjectLoadMove_t3BE2A335AE97C6B40CAB3E2CDD0639B7924110CF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTLOADMOVE_T3BE2A335AE97C6B40CAB3E2CDD0639B7924110CF_H
#ifndef OBJECTSTORE_TB7EE0D6CBA7CBBBA830724C5792B309AF079DEF4_H
#define OBJECTSTORE_TB7EE0D6CBA7CBBBA830724C5792B309AF079DEF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectPool_ObjectStore
struct  ObjectStore_tB7EE0D6CBA7CBBBA830724C5792B309AF079DEF4  : public RuntimeObject
{
public:
	// ObjectPool_SimpleObjectFactory ObjectPool_ObjectStore::mFactory
	SimpleObjectFactory_tAAB121CEB0D927DEA868CFB01C52A50A1C7C7132 * ___mFactory_0;

public:
	inline static int32_t get_offset_of_mFactory_0() { return static_cast<int32_t>(offsetof(ObjectStore_tB7EE0D6CBA7CBBBA830724C5792B309AF079DEF4, ___mFactory_0)); }
	inline SimpleObjectFactory_tAAB121CEB0D927DEA868CFB01C52A50A1C7C7132 * get_mFactory_0() const { return ___mFactory_0; }
	inline SimpleObjectFactory_tAAB121CEB0D927DEA868CFB01C52A50A1C7C7132 ** get_address_of_mFactory_0() { return &___mFactory_0; }
	inline void set_mFactory_0(SimpleObjectFactory_tAAB121CEB0D927DEA868CFB01C52A50A1C7C7132 * value)
	{
		___mFactory_0 = value;
		Il2CppCodeGenWriteBarrier((&___mFactory_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSTORE_TB7EE0D6CBA7CBBBA830724C5792B309AF079DEF4_H
#ifndef SIMPLEOBJECTFACTORY_TAAB121CEB0D927DEA868CFB01C52A50A1C7C7132_H
#define SIMPLEOBJECTFACTORY_TAAB121CEB0D927DEA868CFB01C52A50A1C7C7132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectPool_SimpleObjectFactory
struct  SimpleObjectFactory_tAAB121CEB0D927DEA868CFB01C52A50A1C7C7132  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEOBJECTFACTORY_TAAB121CEB0D927DEA868CFB01C52A50A1C7C7132_H
#ifndef PLAYERDATA_T884B4586F555C513CFA2CC9AE975F75C25538043_H
#define PLAYERDATA_T884B4586F555C513CFA2CC9AE975F75C25538043_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerData
struct  PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043  : public RuntimeObject
{
public:
	// System.String PlayerData::SaveAccount
	String_t* ___SaveAccount_0;
	// System.String PlayerData::SavePassword
	String_t* ___SavePassword_1;
	// System.Int32 PlayerData::PlayerID
	int32_t ___PlayerID_2;
	// System.Int32 PlayerData::Coins
	int32_t ___Coins_3;
	// System.Int32 PlayerData::WealthTracerRank
	int32_t ___WealthTracerRank_4;
	// System.Int32 PlayerData::WealthTracerMoney
	int32_t ___WealthTracerMoney_5;
	// System.Int32 PlayerData::BestTracerRank
	int32_t ___BestTracerRank_6;
	// System.Int32 PlayerData::BestTracerMoney
	int32_t ___BestTracerMoney_7;
	// System.Int32 PlayerData::finishedMission
	int32_t ___finishedMission_8;
	// System.String PlayerData::Name
	String_t* ___Name_9;
	// System.Int32 PlayerData::SchoolID
	int32_t ___SchoolID_10;

public:
	inline static int32_t get_offset_of_SaveAccount_0() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___SaveAccount_0)); }
	inline String_t* get_SaveAccount_0() const { return ___SaveAccount_0; }
	inline String_t** get_address_of_SaveAccount_0() { return &___SaveAccount_0; }
	inline void set_SaveAccount_0(String_t* value)
	{
		___SaveAccount_0 = value;
		Il2CppCodeGenWriteBarrier((&___SaveAccount_0), value);
	}

	inline static int32_t get_offset_of_SavePassword_1() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___SavePassword_1)); }
	inline String_t* get_SavePassword_1() const { return ___SavePassword_1; }
	inline String_t** get_address_of_SavePassword_1() { return &___SavePassword_1; }
	inline void set_SavePassword_1(String_t* value)
	{
		___SavePassword_1 = value;
		Il2CppCodeGenWriteBarrier((&___SavePassword_1), value);
	}

	inline static int32_t get_offset_of_PlayerID_2() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___PlayerID_2)); }
	inline int32_t get_PlayerID_2() const { return ___PlayerID_2; }
	inline int32_t* get_address_of_PlayerID_2() { return &___PlayerID_2; }
	inline void set_PlayerID_2(int32_t value)
	{
		___PlayerID_2 = value;
	}

	inline static int32_t get_offset_of_Coins_3() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___Coins_3)); }
	inline int32_t get_Coins_3() const { return ___Coins_3; }
	inline int32_t* get_address_of_Coins_3() { return &___Coins_3; }
	inline void set_Coins_3(int32_t value)
	{
		___Coins_3 = value;
	}

	inline static int32_t get_offset_of_WealthTracerRank_4() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___WealthTracerRank_4)); }
	inline int32_t get_WealthTracerRank_4() const { return ___WealthTracerRank_4; }
	inline int32_t* get_address_of_WealthTracerRank_4() { return &___WealthTracerRank_4; }
	inline void set_WealthTracerRank_4(int32_t value)
	{
		___WealthTracerRank_4 = value;
	}

	inline static int32_t get_offset_of_WealthTracerMoney_5() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___WealthTracerMoney_5)); }
	inline int32_t get_WealthTracerMoney_5() const { return ___WealthTracerMoney_5; }
	inline int32_t* get_address_of_WealthTracerMoney_5() { return &___WealthTracerMoney_5; }
	inline void set_WealthTracerMoney_5(int32_t value)
	{
		___WealthTracerMoney_5 = value;
	}

	inline static int32_t get_offset_of_BestTracerRank_6() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___BestTracerRank_6)); }
	inline int32_t get_BestTracerRank_6() const { return ___BestTracerRank_6; }
	inline int32_t* get_address_of_BestTracerRank_6() { return &___BestTracerRank_6; }
	inline void set_BestTracerRank_6(int32_t value)
	{
		___BestTracerRank_6 = value;
	}

	inline static int32_t get_offset_of_BestTracerMoney_7() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___BestTracerMoney_7)); }
	inline int32_t get_BestTracerMoney_7() const { return ___BestTracerMoney_7; }
	inline int32_t* get_address_of_BestTracerMoney_7() { return &___BestTracerMoney_7; }
	inline void set_BestTracerMoney_7(int32_t value)
	{
		___BestTracerMoney_7 = value;
	}

	inline static int32_t get_offset_of_finishedMission_8() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___finishedMission_8)); }
	inline int32_t get_finishedMission_8() const { return ___finishedMission_8; }
	inline int32_t* get_address_of_finishedMission_8() { return &___finishedMission_8; }
	inline void set_finishedMission_8(int32_t value)
	{
		___finishedMission_8 = value;
	}

	inline static int32_t get_offset_of_Name_9() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___Name_9)); }
	inline String_t* get_Name_9() const { return ___Name_9; }
	inline String_t** get_address_of_Name_9() { return &___Name_9; }
	inline void set_Name_9(String_t* value)
	{
		___Name_9 = value;
		Il2CppCodeGenWriteBarrier((&___Name_9), value);
	}

	inline static int32_t get_offset_of_SchoolID_10() { return static_cast<int32_t>(offsetof(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043, ___SchoolID_10)); }
	inline int32_t get_SchoolID_10() const { return ___SchoolID_10; }
	inline int32_t* get_address_of_SchoolID_10() { return &___SchoolID_10; }
	inline void set_SchoolID_10(int32_t value)
	{
		___SchoolID_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERDATA_T884B4586F555C513CFA2CC9AE975F75C25538043_H
#ifndef U3CCHECKCARDDOWNLOADU3EC__ITERATOR1_T9304B57662736604BA15C457E16630715C7FB901_H
#define U3CCHECKCARDDOWNLOADU3EC__ITERATOR1_T9304B57662736604BA15C457E16630715C7FB901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMode_<CheckCardDownload>c__Iterator1
struct  U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901  : public RuntimeObject
{
public:
	// System.Int32 PlayerMode_<CheckCardDownload>c__Iterator1::<cardscount>__0
	int32_t ___U3CcardscountU3E__0_0;
	// DownloadImage[] PlayerMode_<CheckCardDownload>c__Iterator1::cards
	DownloadImageU5BU5D_tF90EE40BF0A832B33C97DD9D009B2B83FF6FB7D0* ___cards_1;
	// DownloadImage[] PlayerMode_<CheckCardDownload>c__Iterator1::U24locvar0
	DownloadImageU5BU5D_tF90EE40BF0A832B33C97DD9D009B2B83FF6FB7D0* ___U24locvar0_2;
	// System.Int32 PlayerMode_<CheckCardDownload>c__Iterator1::U24locvar1
	int32_t ___U24locvar1_3;
	// DownloadImage PlayerMode_<CheckCardDownload>c__Iterator1::<img>__1
	DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13 * ___U3CimgU3E__1_4;
	// PlayerMode PlayerMode_<CheckCardDownload>c__Iterator1::U24this
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * ___U24this_5;
	// System.Object PlayerMode_<CheckCardDownload>c__Iterator1::U24current
	RuntimeObject * ___U24current_6;
	// System.Boolean PlayerMode_<CheckCardDownload>c__Iterator1::U24disposing
	bool ___U24disposing_7;
	// System.Int32 PlayerMode_<CheckCardDownload>c__Iterator1::U24PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CcardscountU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901, ___U3CcardscountU3E__0_0)); }
	inline int32_t get_U3CcardscountU3E__0_0() const { return ___U3CcardscountU3E__0_0; }
	inline int32_t* get_address_of_U3CcardscountU3E__0_0() { return &___U3CcardscountU3E__0_0; }
	inline void set_U3CcardscountU3E__0_0(int32_t value)
	{
		___U3CcardscountU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_cards_1() { return static_cast<int32_t>(offsetof(U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901, ___cards_1)); }
	inline DownloadImageU5BU5D_tF90EE40BF0A832B33C97DD9D009B2B83FF6FB7D0* get_cards_1() const { return ___cards_1; }
	inline DownloadImageU5BU5D_tF90EE40BF0A832B33C97DD9D009B2B83FF6FB7D0** get_address_of_cards_1() { return &___cards_1; }
	inline void set_cards_1(DownloadImageU5BU5D_tF90EE40BF0A832B33C97DD9D009B2B83FF6FB7D0* value)
	{
		___cards_1 = value;
		Il2CppCodeGenWriteBarrier((&___cards_1), value);
	}

	inline static int32_t get_offset_of_U24locvar0_2() { return static_cast<int32_t>(offsetof(U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901, ___U24locvar0_2)); }
	inline DownloadImageU5BU5D_tF90EE40BF0A832B33C97DD9D009B2B83FF6FB7D0* get_U24locvar0_2() const { return ___U24locvar0_2; }
	inline DownloadImageU5BU5D_tF90EE40BF0A832B33C97DD9D009B2B83FF6FB7D0** get_address_of_U24locvar0_2() { return &___U24locvar0_2; }
	inline void set_U24locvar0_2(DownloadImageU5BU5D_tF90EE40BF0A832B33C97DD9D009B2B83FF6FB7D0* value)
	{
		___U24locvar0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_2), value);
	}

	inline static int32_t get_offset_of_U24locvar1_3() { return static_cast<int32_t>(offsetof(U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901, ___U24locvar1_3)); }
	inline int32_t get_U24locvar1_3() const { return ___U24locvar1_3; }
	inline int32_t* get_address_of_U24locvar1_3() { return &___U24locvar1_3; }
	inline void set_U24locvar1_3(int32_t value)
	{
		___U24locvar1_3 = value;
	}

	inline static int32_t get_offset_of_U3CimgU3E__1_4() { return static_cast<int32_t>(offsetof(U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901, ___U3CimgU3E__1_4)); }
	inline DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13 * get_U3CimgU3E__1_4() const { return ___U3CimgU3E__1_4; }
	inline DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13 ** get_address_of_U3CimgU3E__1_4() { return &___U3CimgU3E__1_4; }
	inline void set_U3CimgU3E__1_4(DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13 * value)
	{
		___U3CimgU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CimgU3E__1_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901, ___U24this_5)); }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * get_U24this_5() const { return ___U24this_5; }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKCARDDOWNLOADU3EC__ITERATOR1_T9304B57662736604BA15C457E16630715C7FB901_H
#ifndef U3CCHECKLOADINGU3EC__ITERATOR6_TDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E_H
#define U3CCHECKLOADINGU3EC__ITERATOR6_TDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMode_<CheckLoading>c__Iterator6
struct  U3CCheckLoadingU3Ec__Iterator6_tDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E  : public RuntimeObject
{
public:
	// System.Int32 PlayerMode_<CheckLoading>c__Iterator6::<counter>__0
	int32_t ___U3CcounterU3E__0_0;
	// PlayerMode PlayerMode_<CheckLoading>c__Iterator6::U24this
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * ___U24this_1;
	// System.Object PlayerMode_<CheckLoading>c__Iterator6::U24current
	RuntimeObject * ___U24current_2;
	// System.Boolean PlayerMode_<CheckLoading>c__Iterator6::U24disposing
	bool ___U24disposing_3;
	// System.Int32 PlayerMode_<CheckLoading>c__Iterator6::U24PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CcounterU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCheckLoadingU3Ec__Iterator6_tDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E, ___U3CcounterU3E__0_0)); }
	inline int32_t get_U3CcounterU3E__0_0() const { return ___U3CcounterU3E__0_0; }
	inline int32_t* get_address_of_U3CcounterU3E__0_0() { return &___U3CcounterU3E__0_0; }
	inline void set_U3CcounterU3E__0_0(int32_t value)
	{
		___U3CcounterU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCheckLoadingU3Ec__Iterator6_tDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E, ___U24this_1)); }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * get_U24this_1() const { return ___U24this_1; }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCheckLoadingU3Ec__Iterator6_tDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCheckLoadingU3Ec__Iterator6_tDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCheckLoadingU3Ec__Iterator6_tDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKLOADINGU3EC__ITERATOR6_TDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E_H
#ifndef U3CDOWNLOADCOLLECTIONIMGU3EC__ITERATOR2_T6E2FD3738D2ECD6D50348CD4C6460A705015558A_H
#define U3CDOWNLOADCOLLECTIONIMGU3EC__ITERATOR2_T6E2FD3738D2ECD6D50348CD4C6460A705015558A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMode_<DownloadCollectionImg>c__Iterator2
struct  U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A  : public RuntimeObject
{
public:
	// System.String PlayerMode_<DownloadCollectionImg>c__Iterator2::cardurl
	String_t* ___cardurl_0;
	// System.String PlayerMode_<DownloadCollectionImg>c__Iterator2::<cardURL>__0
	String_t* ___U3CcardURLU3E__0_1;
	// System.String PlayerMode_<DownloadCollectionImg>c__Iterator2::<path>__0
	String_t* ___U3CpathU3E__0_2;
	// UnityEngine.Networking.UnityWebRequest PlayerMode_<DownloadCollectionImg>c__Iterator2::<www>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwwwU3E__1_3;
	// DownloadImage PlayerMode_<DownloadCollectionImg>c__Iterator2::img
	DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13 * ___img_4;
	// System.String PlayerMode_<DownloadCollectionImg>c__Iterator2::cardname
	String_t* ___cardname_5;
	// System.Object PlayerMode_<DownloadCollectionImg>c__Iterator2::U24current
	RuntimeObject * ___U24current_6;
	// System.Boolean PlayerMode_<DownloadCollectionImg>c__Iterator2::U24disposing
	bool ___U24disposing_7;
	// System.Int32 PlayerMode_<DownloadCollectionImg>c__Iterator2::U24PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_cardurl_0() { return static_cast<int32_t>(offsetof(U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A, ___cardurl_0)); }
	inline String_t* get_cardurl_0() const { return ___cardurl_0; }
	inline String_t** get_address_of_cardurl_0() { return &___cardurl_0; }
	inline void set_cardurl_0(String_t* value)
	{
		___cardurl_0 = value;
		Il2CppCodeGenWriteBarrier((&___cardurl_0), value);
	}

	inline static int32_t get_offset_of_U3CcardURLU3E__0_1() { return static_cast<int32_t>(offsetof(U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A, ___U3CcardURLU3E__0_1)); }
	inline String_t* get_U3CcardURLU3E__0_1() const { return ___U3CcardURLU3E__0_1; }
	inline String_t** get_address_of_U3CcardURLU3E__0_1() { return &___U3CcardURLU3E__0_1; }
	inline void set_U3CcardURLU3E__0_1(String_t* value)
	{
		___U3CcardURLU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcardURLU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CpathU3E__0_2() { return static_cast<int32_t>(offsetof(U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A, ___U3CpathU3E__0_2)); }
	inline String_t* get_U3CpathU3E__0_2() const { return ___U3CpathU3E__0_2; }
	inline String_t** get_address_of_U3CpathU3E__0_2() { return &___U3CpathU3E__0_2; }
	inline void set_U3CpathU3E__0_2(String_t* value)
	{
		___U3CpathU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__1_3() { return static_cast<int32_t>(offsetof(U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A, ___U3CwwwU3E__1_3)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwwwU3E__1_3() const { return ___U3CwwwU3E__1_3; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwwwU3E__1_3() { return &___U3CwwwU3E__1_3; }
	inline void set_U3CwwwU3E__1_3(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwwwU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__1_3), value);
	}

	inline static int32_t get_offset_of_img_4() { return static_cast<int32_t>(offsetof(U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A, ___img_4)); }
	inline DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13 * get_img_4() const { return ___img_4; }
	inline DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13 ** get_address_of_img_4() { return &___img_4; }
	inline void set_img_4(DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13 * value)
	{
		___img_4 = value;
		Il2CppCodeGenWriteBarrier((&___img_4), value);
	}

	inline static int32_t get_offset_of_cardname_5() { return static_cast<int32_t>(offsetof(U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A, ___cardname_5)); }
	inline String_t* get_cardname_5() const { return ___cardname_5; }
	inline String_t** get_address_of_cardname_5() { return &___cardname_5; }
	inline void set_cardname_5(String_t* value)
	{
		___cardname_5 = value;
		Il2CppCodeGenWriteBarrier((&___cardname_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOWNLOADCOLLECTIONIMGU3EC__ITERATOR2_T6E2FD3738D2ECD6D50348CD4C6460A705015558A_H
#ifndef U3CLOADCARDSU3EC__ITERATOR0_T7B75848B88C4DA25E037088C3EA4E73607E43470_H
#define U3CLOADCARDSU3EC__ITERATOR0_T7B75848B88C4DA25E037088C3EA4E73607E43470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMode_<LoadCards>c__Iterator0
struct  U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm PlayerMode_<LoadCards>c__Iterator0::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass PlayerMode_<LoadCards>c__Iterator0::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// UnityEngine.Networking.UnityWebRequest PlayerMode_<LoadCards>c__Iterator0::<sendWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CsendWWWU3E__1_2;
	// PlayerMode PlayerMode_<LoadCards>c__Iterator0::U24this
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * ___U24this_3;
	// System.Object PlayerMode_<LoadCards>c__Iterator0::U24current
	RuntimeObject * ___U24current_4;
	// System.Boolean PlayerMode_<LoadCards>c__Iterator0::U24disposing
	bool ___U24disposing_5;
	// System.Int32 PlayerMode_<LoadCards>c__Iterator0::U24PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CsendWWWU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470, ___U3CsendWWWU3E__1_2)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CsendWWWU3E__1_2() const { return ___U3CsendWWWU3E__1_2; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CsendWWWU3E__1_2() { return &___U3CsendWWWU3E__1_2; }
	inline void set_U3CsendWWWU3E__1_2(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CsendWWWU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsendWWWU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470, ___U24this_3)); }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * get_U24this_3() const { return ___U24this_3; }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCARDSU3EC__ITERATOR0_T7B75848B88C4DA25E037088C3EA4E73607E43470_H
#ifndef U3CLOADFINISHRECORDU3EC__ITERATOR4_TA98BA2EB1C824115C3665B7534F27A060A0A5E72_H
#define U3CLOADFINISHRECORDU3EC__ITERATOR4_TA98BA2EB1C824115C3665B7534F27A060A0A5E72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMode_<LoadFinishRecord>c__Iterator4
struct  U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm PlayerMode_<LoadFinishRecord>c__Iterator4::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass PlayerMode_<LoadFinishRecord>c__Iterator4::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// UnityEngine.Networking.UnityWebRequest PlayerMode_<LoadFinishRecord>c__Iterator4::<sendWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CsendWWWU3E__1_2;
	// PlayerMode PlayerMode_<LoadFinishRecord>c__Iterator4::U24this
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * ___U24this_3;
	// System.Object PlayerMode_<LoadFinishRecord>c__Iterator4::U24current
	RuntimeObject * ___U24current_4;
	// System.Boolean PlayerMode_<LoadFinishRecord>c__Iterator4::U24disposing
	bool ___U24disposing_5;
	// System.Int32 PlayerMode_<LoadFinishRecord>c__Iterator4::U24PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CsendWWWU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72, ___U3CsendWWWU3E__1_2)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CsendWWWU3E__1_2() const { return ___U3CsendWWWU3E__1_2; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CsendWWWU3E__1_2() { return &___U3CsendWWWU3E__1_2; }
	inline void set_U3CsendWWWU3E__1_2(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CsendWWWU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsendWWWU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72, ___U24this_3)); }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * get_U24this_3() const { return ___U24this_3; }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADFINISHRECORDU3EC__ITERATOR4_TA98BA2EB1C824115C3665B7534F27A060A0A5E72_H
#ifndef U3CLOADMISSIONLISTU3EC__ITERATOR3_T59CA83C041FF782FC9649342B1BACA25F9756582_H
#define U3CLOADMISSIONLISTU3EC__ITERATOR3_T59CA83C041FF782FC9649342B1BACA25F9756582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMode_<LoadMissionList>c__Iterator3
struct  U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm PlayerMode_<LoadMissionList>c__Iterator3::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass PlayerMode_<LoadMissionList>c__Iterator3::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// UnityEngine.Networking.UnityWebRequest PlayerMode_<LoadMissionList>c__Iterator3::<sendWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CsendWWWU3E__1_2;
	// PlayerMode PlayerMode_<LoadMissionList>c__Iterator3::U24this
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * ___U24this_3;
	// System.Object PlayerMode_<LoadMissionList>c__Iterator3::U24current
	RuntimeObject * ___U24current_4;
	// System.Boolean PlayerMode_<LoadMissionList>c__Iterator3::U24disposing
	bool ___U24disposing_5;
	// System.Int32 PlayerMode_<LoadMissionList>c__Iterator3::U24PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CsendWWWU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582, ___U3CsendWWWU3E__1_2)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CsendWWWU3E__1_2() const { return ___U3CsendWWWU3E__1_2; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CsendWWWU3E__1_2() { return &___U3CsendWWWU3E__1_2; }
	inline void set_U3CsendWWWU3E__1_2(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CsendWWWU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsendWWWU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582, ___U24this_3)); }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * get_U24this_3() const { return ___U24this_3; }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADMISSIONLISTU3EC__ITERATOR3_T59CA83C041FF782FC9649342B1BACA25F9756582_H
#ifndef U3CLOADRANKINGU3EC__ITERATOR5_T557E3D6752E1A099E53195859D49EDBE10801728_H
#define U3CLOADRANKINGU3EC__ITERATOR5_T557E3D6752E1A099E53195859D49EDBE10801728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMode_<loadRanking>c__Iterator5
struct  U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm PlayerMode_<loadRanking>c__Iterator5::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass PlayerMode_<loadRanking>c__Iterator5::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// UnityEngine.Networking.UnityWebRequest PlayerMode_<loadRanking>c__Iterator5::<sendWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CsendWWWU3E__1_2;
	// PlayerMode PlayerMode_<loadRanking>c__Iterator5::U24this
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * ___U24this_3;
	// System.Object PlayerMode_<loadRanking>c__Iterator5::U24current
	RuntimeObject * ___U24current_4;
	// System.Boolean PlayerMode_<loadRanking>c__Iterator5::U24disposing
	bool ___U24disposing_5;
	// System.Int32 PlayerMode_<loadRanking>c__Iterator5::U24PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CsendWWWU3E__1_2() { return static_cast<int32_t>(offsetof(U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728, ___U3CsendWWWU3E__1_2)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CsendWWWU3E__1_2() const { return ___U3CsendWWWU3E__1_2; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CsendWWWU3E__1_2() { return &___U3CsendWWWU3E__1_2; }
	inline void set_U3CsendWWWU3E__1_2(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CsendWWWU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsendWWWU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728, ___U24this_3)); }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * get_U24this_3() const { return ___U24this_3; }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADRANKINGU3EC__ITERATOR5_T557E3D6752E1A099E53195859D49EDBE10801728_H
#ifndef RSOURCESTORES_T7DF0BF48D6C2C9A7496994994A0A55517E309385_H
#define RSOURCESTORES_T7DF0BF48D6C2C9A7496994994A0A55517E309385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RsourceStores
struct  RsourceStores_t7DF0BF48D6C2C9A7496994994A0A55517E309385  : public RuntimeObject
{
public:
	// SimpleResourceFactory RsourceStores::mFactory
	SimpleResourceFactory_tA31CC8DA5DB8C7190B9EE7E66AA4D570C1C71D14 * ___mFactory_0;

public:
	inline static int32_t get_offset_of_mFactory_0() { return static_cast<int32_t>(offsetof(RsourceStores_t7DF0BF48D6C2C9A7496994994A0A55517E309385, ___mFactory_0)); }
	inline SimpleResourceFactory_tA31CC8DA5DB8C7190B9EE7E66AA4D570C1C71D14 * get_mFactory_0() const { return ___mFactory_0; }
	inline SimpleResourceFactory_tA31CC8DA5DB8C7190B9EE7E66AA4D570C1C71D14 ** get_address_of_mFactory_0() { return &___mFactory_0; }
	inline void set_mFactory_0(SimpleResourceFactory_tA31CC8DA5DB8C7190B9EE7E66AA4D570C1C71D14 * value)
	{
		___mFactory_0 = value;
		Il2CppCodeGenWriteBarrier((&___mFactory_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSOURCESTORES_T7DF0BF48D6C2C9A7496994994A0A55517E309385_H
#ifndef SINGLEBEACONDATA_T2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69_H
#define SINGLEBEACONDATA_T2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SingleBeaconData
struct  SingleBeaconData_t2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69  : public RuntimeObject
{
public:
	// System.String SingleBeaconData::UUID
	String_t* ___UUID_0;
	// System.Int32 SingleBeaconData::Major
	int32_t ___Major_1;
	// System.Int32 SingleBeaconData::Minor
	int32_t ___Minor_2;
	// System.Collections.Generic.List`1<System.String> SingleBeaconData::mContents
	List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 * ___mContents_3;

public:
	inline static int32_t get_offset_of_UUID_0() { return static_cast<int32_t>(offsetof(SingleBeaconData_t2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69, ___UUID_0)); }
	inline String_t* get_UUID_0() const { return ___UUID_0; }
	inline String_t** get_address_of_UUID_0() { return &___UUID_0; }
	inline void set_UUID_0(String_t* value)
	{
		___UUID_0 = value;
		Il2CppCodeGenWriteBarrier((&___UUID_0), value);
	}

	inline static int32_t get_offset_of_Major_1() { return static_cast<int32_t>(offsetof(SingleBeaconData_t2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69, ___Major_1)); }
	inline int32_t get_Major_1() const { return ___Major_1; }
	inline int32_t* get_address_of_Major_1() { return &___Major_1; }
	inline void set_Major_1(int32_t value)
	{
		___Major_1 = value;
	}

	inline static int32_t get_offset_of_Minor_2() { return static_cast<int32_t>(offsetof(SingleBeaconData_t2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69, ___Minor_2)); }
	inline int32_t get_Minor_2() const { return ___Minor_2; }
	inline int32_t* get_address_of_Minor_2() { return &___Minor_2; }
	inline void set_Minor_2(int32_t value)
	{
		___Minor_2 = value;
	}

	inline static int32_t get_offset_of_mContents_3() { return static_cast<int32_t>(offsetof(SingleBeaconData_t2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69, ___mContents_3)); }
	inline List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 * get_mContents_3() const { return ___mContents_3; }
	inline List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 ** get_address_of_mContents_3() { return &___mContents_3; }
	inline void set_mContents_3(List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 * value)
	{
		___mContents_3 = value;
		Il2CppCodeGenWriteBarrier((&___mContents_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLEBEACONDATA_T2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#define VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.UI.VertexHelperExtension
struct  VertexHelperExtension_t593AF80A941B7208D2CC88AE671873CE6E2AA8E3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPEREXTENSION_T593AF80A941B7208D2CC88AE671873CE6E2AA8E3_H
#ifndef BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#define BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T1EF95AB1FC33A027710E7DC86D19F700156C4F6A_H
#ifndef U3CDELAYEDSETDIRTYU3EC__ITERATOR0_TB8BB61C2C033D95240B4E2704971663E4DC08073_H
#define U3CDELAYEDSETDIRTYU3EC__ITERATOR0_TB8BB61C2C033D95240B4E2704971663E4DC08073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0
struct  U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::rectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rectTransform_0;
	// System.Object UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::U24current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::U24disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.LayoutGroup_<DelayedSetDirty>c__Iterator0::U24PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___rectTransform_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSETDIRTYU3EC__ITERATOR0_TB8BB61C2C033D95240B4E2704971663E4DC08073_H
#ifndef LAYOUTREBUILDER_T8D3501B43B1DE666140E2931FFA732B5B09EA5BD_H
#define LAYOUTREBUILDER_T8D3501B43B1DE666140E2931FFA732B5B09EA5BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;

public:
	inline static int32_t get_offset_of_m_ToRebuild_0() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD, ___m_ToRebuild_0)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_ToRebuild_0() const { return ___m_ToRebuild_0; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_ToRebuild_0() { return &___m_ToRebuild_0; }
	inline void set_m_ToRebuild_0(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_ToRebuild_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToRebuild_0), value);
	}

	inline static int32_t get_offset_of_m_CachedHashFromTransform_1() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD, ___m_CachedHashFromTransform_1)); }
	inline int32_t get_m_CachedHashFromTransform_1() const { return ___m_CachedHashFromTransform_1; }
	inline int32_t* get_address_of_m_CachedHashFromTransform_1() { return &___m_CachedHashFromTransform_1; }
	inline void set_m_CachedHashFromTransform_1(int32_t value)
	{
		___m_CachedHashFromTransform_1 = value;
	}
};

struct LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder> UnityEngine.UI.LayoutRebuilder::s_Rebuilders
	ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 * ___s_Rebuilders_2;
	// UnityEngine.RectTransform_ReapplyDrivenProperties UnityEngine.UI.LayoutRebuilder::<>f__mgU24cache0
	ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * ___U3CU3Ef__mgU24cache0_3;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache0
	Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * ___U3CU3Ef__amU24cache0_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache1
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache1_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache2
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache2_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache3
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache3_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__amU24cache4
	UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of_s_Rebuilders_2() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___s_Rebuilders_2)); }
	inline ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 * get_s_Rebuilders_2() const { return ___s_Rebuilders_2; }
	inline ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 ** get_address_of_s_Rebuilders_2() { return &___s_Rebuilders_2; }
	inline void set_s_Rebuilders_2(ObjectPool_1_tFA4F33849836CDB27432AE22249BB79D68619541 * value)
	{
		___s_Rebuilders_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Rebuilders_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ReapplyDrivenProperties_t431F4FBD9C59AE097FE33C4354CC6251B01B527D * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t1A9CE8ADB6E9328794CC409FD5BEAACA86D7D769 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(UnityAction_1_tD07424D822F8527D240A320A1866C70C835C7C99 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTREBUILDER_T8D3501B43B1DE666140E2931FFA732B5B09EA5BD_H
#ifndef LAYOUTUTILITY_T3B5074E34900DA384884FC50809EA395CB69E7D5_H
#define LAYOUTUTILITY_T3B5074E34900DA384884FC50809EA395CB69E7D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutUtility
struct  LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5  : public RuntimeObject
{
public:

public:
};

struct LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields
{
public:
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache0
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache1
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache2
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache3
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache4
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache5
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache6
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__amU24cache7
	Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_tCC31999C8C9743F42A2D59A3570B71FFA3DB09B3 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTUTILITY_T3B5074E34900DA384884FC50809EA395CB69E7D5_H
#ifndef REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#define REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache_Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache_RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache_Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3D_0)); }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___raycast2D_2)); }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_TBDADDC80D50C5F10BD00965217980B9A8D24BE8A_H
#ifndef U3CCAMERAONU3EC__ITERATOR0_TD3CB0D761CDCD91380D2D419E8225BE7B15F1E53_H
#define U3CCAMERAONU3EC__ITERATOR0_TD3CB0D761CDCD91380D2D419E8225BE7B15F1E53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// cameraApp_<CameraOn>c__Iterator0
struct  U3CCameraOnU3Ec__Iterator0_tD3CB0D761CDCD91380D2D419E8225BE7B15F1E53  : public RuntimeObject
{
public:
	// cameraApp cameraApp_<CameraOn>c__Iterator0::U24this
	cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7 * ___U24this_0;
	// System.Object cameraApp_<CameraOn>c__Iterator0::U24current
	RuntimeObject * ___U24current_1;
	// System.Boolean cameraApp_<CameraOn>c__Iterator0::U24disposing
	bool ___U24disposing_2;
	// System.Int32 cameraApp_<CameraOn>c__Iterator0::U24PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCameraOnU3Ec__Iterator0_tD3CB0D761CDCD91380D2D419E8225BE7B15F1E53, ___U24this_0)); }
	inline cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7 * get_U24this_0() const { return ___U24this_0; }
	inline cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCameraOnU3Ec__Iterator0_tD3CB0D761CDCD91380D2D419E8225BE7B15F1E53, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCameraOnU3Ec__Iterator0_tD3CB0D761CDCD91380D2D419E8225BE7B15F1E53, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCameraOnU3Ec__Iterator0_tD3CB0D761CDCD91380D2D419E8225BE7B15F1E53, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCAMERAONU3EC__ITERATOR0_TD3CB0D761CDCD91380D2D419E8225BE7B15F1E53_H
#ifndef U3CCHECKEDCAMERAU3EC__ITERATOR1_T066B15E9843DCAA7D8813EF8FBB746E3A00CA918_H
#define U3CCHECKEDCAMERAU3EC__ITERATOR1_T066B15E9843DCAA7D8813EF8FBB746E3A00CA918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// cameraApp_<CheckedCamera>c__Iterator1
struct  U3CCheckedCameraU3Ec__Iterator1_t066B15E9843DCAA7D8813EF8FBB746E3A00CA918  : public RuntimeObject
{
public:
	// cameraApp cameraApp_<CheckedCamera>c__Iterator1::U24this
	cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7 * ___U24this_0;
	// System.Object cameraApp_<CheckedCamera>c__Iterator1::U24current
	RuntimeObject * ___U24current_1;
	// System.Boolean cameraApp_<CheckedCamera>c__Iterator1::U24disposing
	bool ___U24disposing_2;
	// System.Int32 cameraApp_<CheckedCamera>c__Iterator1::U24PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCheckedCameraU3Ec__Iterator1_t066B15E9843DCAA7D8813EF8FBB746E3A00CA918, ___U24this_0)); }
	inline cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7 * get_U24this_0() const { return ___U24this_0; }
	inline cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCheckedCameraU3Ec__Iterator1_t066B15E9843DCAA7D8813EF8FBB746E3A00CA918, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCheckedCameraU3Ec__Iterator1_t066B15E9843DCAA7D8813EF8FBB746E3A00CA918, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCheckedCameraU3Ec__Iterator1_t066B15E9843DCAA7D8813EF8FBB746E3A00CA918, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKEDCAMERAU3EC__ITERATOR1_T066B15E9843DCAA7D8813EF8FBB746E3A00CA918_H
#ifndef U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#define U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>_U24ArrayTypeU3D12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#define INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_tC16F64335CE8B56D99229DE94BB3A876ED55FE87 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_tC16F64335CE8B56D99229DE94BB3A876ED55FE87, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_TF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB_H
#define SINGLE_TF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_tF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_tF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_TF2613CF3F3AF0E8BEBC3CE8AD41479C44E6075DB_H
#ifndef TIMESPAN_T26DE4373EA745C072A5FBDEAD8373348955FC374_H
#define TIMESPAN_T26DE4373EA745C072A5FBDEAD8373348955FC374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields, ___Zero_7)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T26DE4373EA745C072A5FBDEAD8373348955FC374_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#define DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_TB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#define VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___zeroVector_5)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___oneVector_6)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_TD148D6428C3F8FF6CD998F82090113C2B490B76E_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields
{
public:
	// <PrivateImplementationDetails>_U24ArrayTypeU3D12 <PrivateImplementationDetails>::U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_TC8332394FBFEEB4B73459A35E182942340DA3537_H
#ifndef RANKMODE_T80C951E8018D8B56A7CCC31715B45773E7E8F0A6_H
#define RANKMODE_T80C951E8018D8B56A7CCC31715B45773E7E8F0A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadRank_RankMode
struct  RankMode_t80C951E8018D8B56A7CCC31715B45773E7E8F0A6 
{
public:
	// System.Int32 LoadRank_RankMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RankMode_t80C951E8018D8B56A7CCC31715B45773E7E8F0A6, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANKMODE_T80C951E8018D8B56A7CCC31715B45773E7E8F0A6_H
#ifndef IMISSIONMODE_T76B792A5875AF2771F97F4A8EE3B22120268E9BC_H
#define IMISSIONMODE_T76B792A5875AF2771F97F4A8EE3B22120268E9BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager_IMissionMode
struct  IMissionMode_t76B792A5875AF2771F97F4A8EE3B22120268E9BC 
{
public:
	// System.Int32 MissionManager_IMissionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IMissionMode_t76B792A5875AF2771F97F4A8EE3B22120268E9BC, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMISSIONMODE_T76B792A5875AF2771F97F4A8EE3B22120268E9BC_H
#ifndef ESYSTEMTYPE_T549D39F80314FE96CDC5305DAD54AB1320FD37DE_H
#define ESYSTEMTYPE_T549D39F80314FE96CDC5305DAD54AB1320FD37DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectPool_eSystemType
struct  eSystemType_t549D39F80314FE96CDC5305DAD54AB1320FD37DE 
{
public:
	// System.Int32 ObjectPool_eSystemType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSystemType_t549D39F80314FE96CDC5305DAD54AB1320FD37DE, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESYSTEMTYPE_T549D39F80314FE96CDC5305DAD54AB1320FD37DE_H
#ifndef IPLAYERMODE_TD4C65A0449F8AA69EAA5391678F3112100B82465_H
#define IPLAYERMODE_TD4C65A0449F8AA69EAA5391678F3112100B82465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMode_IPlayerMode
struct  IPlayerMode_tD4C65A0449F8AA69EAA5391678F3112100B82465 
{
public:
	// System.Int32 PlayerMode_IPlayerMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IPlayerMode_tD4C65A0449F8AA69EAA5391678F3112100B82465, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPLAYERMODE_TD4C65A0449F8AA69EAA5391678F3112100B82465_H
#ifndef ESYSTEMTYPE_T2A5B48C62B3C5047427A89463E9FCE282C926C29_H
#define ESYSTEMTYPE_T2A5B48C62B3C5047427A89463E9FCE282C926C29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceLoad_eSystemType
struct  eSystemType_t2A5B48C62B3C5047427A89463E9FCE282C926C29 
{
public:
	// System.Int32 ResourceLoad_eSystemType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(eSystemType_t2A5B48C62B3C5047427A89463E9FCE282C926C29, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ESYSTEMTYPE_T2A5B48C62B3C5047427A89463E9FCE282C926C29_H
#ifndef DATETIMEKIND_T922DC40070B8AD59CB650E3FE023C4A58ED4568A_H
#define DATETIMEKIND_T922DC40070B8AD59CB650E3FE023C4A58ED4568A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t922DC40070B8AD59CB650E3FE023C4A58ED4568A 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t922DC40070B8AD59CB650E3FE023C4A58ED4568A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T922DC40070B8AD59CB650E3FE023C4A58ED4568A_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_8)); }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * get_data_8() const { return ___data_8; }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#define RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Origin_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_tE2163D4CB3E6B267E29F8ABE41684490E4A614B2, ___m_Direction_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_TE2163D4CB3E6B267E29F8ABE41684490E4A614B2_H
#ifndef RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#define RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifndef RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#define RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Centroid_0)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Point_1)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Normal_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t5E8A7F96317BAF2033362FC780F4D72DC72764BE, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT2D_T5E8A7F96317BAF2033362FC780F4D72DC72764BE_H
#ifndef TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#define TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_tEC19034D476659A5E05366C63564F34DD30E7C57, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_TEC19034D476659A5E05366C63564F34DD30E7C57_H
#ifndef VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#define VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t4A961510635950236678F1B3B2436ECCAF28713A * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Positions_0)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Colors_1)); }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv0S_2)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv1S_3)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv2S_4)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Uv3S_5)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Normals_6)); }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Tangents_7)); }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_Indices_8)); }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t4A961510635950236678F1B3B2436ECCAF28713A ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t4A961510635950236678F1B3B2436ECCAF28713A * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_H
#ifndef DATETIME_T816BBD9125EA02B42B87CB07643FC5F803391DA2_H
#define DATETIME_T816BBD9125EA02B42B87CB07643FC5F803391DA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2, ___ticks_10)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___MaxValue_12)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___MinValue_13)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T816BBD9125EA02B42B87CB07643FC5F803391DA2_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef U3CSUBMITU3EC__ITERATOR4_TC7520A7504F8F908B591A16517F699C17B01D61A_H
#define U3CSUBMITU3EC__ITERATOR4_TC7520A7504F8F908B591A16517F699C17B01D61A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager_<submit>c__Iterator4
struct  U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm MissionManager_<submit>c__Iterator4::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass MissionManager_<submit>c__Iterator4::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// System.DateTime MissionManager_<submit>c__Iterator4::<myDate>__0
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___U3CmyDateU3E__0_2;
	// System.String MissionManager_<submit>c__Iterator4::<myDateString>__0
	String_t* ___U3CmyDateStringU3E__0_3;
	// System.String MissionManager_<submit>c__Iterator4::<apm>__0
	String_t* ___U3CapmU3E__0_4;
	// System.Int32 MissionManager_<submit>c__Iterator4::<finishtime>__0
	int32_t ___U3CfinishtimeU3E__0_5;
	// System.String MissionManager_<submit>c__Iterator4::<saveCardID>__0
	String_t* ___U3CsaveCardIDU3E__0_6;
	// UnityEngine.Networking.UnityWebRequest MissionManager_<submit>c__Iterator4::<LoginWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CLoginWWWU3E__1_7;
	// MissionManager MissionManager_<submit>c__Iterator4::U24this
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * ___U24this_8;
	// System.Object MissionManager_<submit>c__Iterator4::U24current
	RuntimeObject * ___U24current_9;
	// System.Boolean MissionManager_<submit>c__Iterator4::U24disposing
	bool ___U24disposing_10;
	// System.Int32 MissionManager_<submit>c__Iterator4::U24PC
	int32_t ___U24PC_11;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CmyDateU3E__0_2() { return static_cast<int32_t>(offsetof(U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A, ___U3CmyDateU3E__0_2)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_U3CmyDateU3E__0_2() const { return ___U3CmyDateU3E__0_2; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_U3CmyDateU3E__0_2() { return &___U3CmyDateU3E__0_2; }
	inline void set_U3CmyDateU3E__0_2(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___U3CmyDateU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CmyDateStringU3E__0_3() { return static_cast<int32_t>(offsetof(U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A, ___U3CmyDateStringU3E__0_3)); }
	inline String_t* get_U3CmyDateStringU3E__0_3() const { return ___U3CmyDateStringU3E__0_3; }
	inline String_t** get_address_of_U3CmyDateStringU3E__0_3() { return &___U3CmyDateStringU3E__0_3; }
	inline void set_U3CmyDateStringU3E__0_3(String_t* value)
	{
		___U3CmyDateStringU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmyDateStringU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CapmU3E__0_4() { return static_cast<int32_t>(offsetof(U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A, ___U3CapmU3E__0_4)); }
	inline String_t* get_U3CapmU3E__0_4() const { return ___U3CapmU3E__0_4; }
	inline String_t** get_address_of_U3CapmU3E__0_4() { return &___U3CapmU3E__0_4; }
	inline void set_U3CapmU3E__0_4(String_t* value)
	{
		___U3CapmU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CapmU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U3CfinishtimeU3E__0_5() { return static_cast<int32_t>(offsetof(U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A, ___U3CfinishtimeU3E__0_5)); }
	inline int32_t get_U3CfinishtimeU3E__0_5() const { return ___U3CfinishtimeU3E__0_5; }
	inline int32_t* get_address_of_U3CfinishtimeU3E__0_5() { return &___U3CfinishtimeU3E__0_5; }
	inline void set_U3CfinishtimeU3E__0_5(int32_t value)
	{
		___U3CfinishtimeU3E__0_5 = value;
	}

	inline static int32_t get_offset_of_U3CsaveCardIDU3E__0_6() { return static_cast<int32_t>(offsetof(U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A, ___U3CsaveCardIDU3E__0_6)); }
	inline String_t* get_U3CsaveCardIDU3E__0_6() const { return ___U3CsaveCardIDU3E__0_6; }
	inline String_t** get_address_of_U3CsaveCardIDU3E__0_6() { return &___U3CsaveCardIDU3E__0_6; }
	inline void set_U3CsaveCardIDU3E__0_6(String_t* value)
	{
		___U3CsaveCardIDU3E__0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsaveCardIDU3E__0_6), value);
	}

	inline static int32_t get_offset_of_U3CLoginWWWU3E__1_7() { return static_cast<int32_t>(offsetof(U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A, ___U3CLoginWWWU3E__1_7)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CLoginWWWU3E__1_7() const { return ___U3CLoginWWWU3E__1_7; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CLoginWWWU3E__1_7() { return &___U3CLoginWWWU3E__1_7; }
	inline void set_U3CLoginWWWU3E__1_7(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CLoginWWWU3E__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoginWWWU3E__1_7), value);
	}

	inline static int32_t get_offset_of_U24this_8() { return static_cast<int32_t>(offsetof(U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A, ___U24this_8)); }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * get_U24this_8() const { return ___U24this_8; }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 ** get_address_of_U24this_8() { return &___U24this_8; }
	inline void set_U24this_8(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * value)
	{
		___U24this_8 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_8), value);
	}

	inline static int32_t get_offset_of_U24current_9() { return static_cast<int32_t>(offsetof(U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A, ___U24current_9)); }
	inline RuntimeObject * get_U24current_9() const { return ___U24current_9; }
	inline RuntimeObject ** get_address_of_U24current_9() { return &___U24current_9; }
	inline void set_U24current_9(RuntimeObject * value)
	{
		___U24current_9 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_9), value);
	}

	inline static int32_t get_offset_of_U24disposing_10() { return static_cast<int32_t>(offsetof(U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A, ___U24disposing_10)); }
	inline bool get_U24disposing_10() const { return ___U24disposing_10; }
	inline bool* get_address_of_U24disposing_10() { return &___U24disposing_10; }
	inline void set_U24disposing_10(bool value)
	{
		___U24disposing_10 = value;
	}

	inline static int32_t get_offset_of_U24PC_11() { return static_cast<int32_t>(offsetof(U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A, ___U24PC_11)); }
	inline int32_t get_U24PC_11() const { return ___U24PC_11; }
	inline int32_t* get_address_of_U24PC_11() { return &___U24PC_11; }
	inline void set_U24PC_11(int32_t value)
	{
		___U24PC_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSUBMITU3EC__ITERATOR4_TC7520A7504F8F908B591A16517F699C17B01D61A_H
#ifndef U3CUPLOADSTARTTIMEU3EC__ITERATOR0_TAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF_H
#define U3CUPLOADSTARTTIMEU3EC__ITERATOR0_TAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager_<upLoadStartTime>c__Iterator0
struct  U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF  : public RuntimeObject
{
public:
	// UnityEngine.WWWForm MissionManager_<upLoadStartTime>c__Iterator0::<form>__0
	WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * ___U3CformU3E__0_0;
	// SimpleJSON.JSONClass MissionManager_<upLoadStartTime>c__Iterator0::<JSon>__0
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U3CJSonU3E__0_1;
	// System.DateTime MissionManager_<upLoadStartTime>c__Iterator0::<myDate>__0
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___U3CmyDateU3E__0_2;
	// System.String MissionManager_<upLoadStartTime>c__Iterator0::<myDateString>__0
	String_t* ___U3CmyDateStringU3E__0_3;
	// UnityEngine.Networking.UnityWebRequest MissionManager_<upLoadStartTime>c__Iterator0::<LoginWWW>__1
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CLoginWWWU3E__1_4;
	// MissionManager MissionManager_<upLoadStartTime>c__Iterator0::U24this
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * ___U24this_5;
	// System.Object MissionManager_<upLoadStartTime>c__Iterator0::U24current
	RuntimeObject * ___U24current_6;
	// System.Boolean MissionManager_<upLoadStartTime>c__Iterator0::U24disposing
	bool ___U24disposing_7;
	// System.Int32 MissionManager_<upLoadStartTime>c__Iterator0::U24PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CformU3E__0_0() { return static_cast<int32_t>(offsetof(U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF, ___U3CformU3E__0_0)); }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * get_U3CformU3E__0_0() const { return ___U3CformU3E__0_0; }
	inline WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 ** get_address_of_U3CformU3E__0_0() { return &___U3CformU3E__0_0; }
	inline void set_U3CformU3E__0_0(WWWForm_t8D5ED7CAC180C102E377B21A70CC6A9AD5EAAD24 * value)
	{
		___U3CformU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CJSonU3E__0_1() { return static_cast<int32_t>(offsetof(U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF, ___U3CJSonU3E__0_1)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U3CJSonU3E__0_1() const { return ___U3CJSonU3E__0_1; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U3CJSonU3E__0_1() { return &___U3CJSonU3E__0_1; }
	inline void set_U3CJSonU3E__0_1(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U3CJSonU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJSonU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CmyDateU3E__0_2() { return static_cast<int32_t>(offsetof(U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF, ___U3CmyDateU3E__0_2)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_U3CmyDateU3E__0_2() const { return ___U3CmyDateU3E__0_2; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_U3CmyDateU3E__0_2() { return &___U3CmyDateU3E__0_2; }
	inline void set_U3CmyDateU3E__0_2(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___U3CmyDateU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CmyDateStringU3E__0_3() { return static_cast<int32_t>(offsetof(U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF, ___U3CmyDateStringU3E__0_3)); }
	inline String_t* get_U3CmyDateStringU3E__0_3() const { return ___U3CmyDateStringU3E__0_3; }
	inline String_t** get_address_of_U3CmyDateStringU3E__0_3() { return &___U3CmyDateStringU3E__0_3; }
	inline void set_U3CmyDateStringU3E__0_3(String_t* value)
	{
		___U3CmyDateStringU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmyDateStringU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CLoginWWWU3E__1_4() { return static_cast<int32_t>(offsetof(U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF, ___U3CLoginWWWU3E__1_4)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CLoginWWWU3E__1_4() const { return ___U3CLoginWWWU3E__1_4; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CLoginWWWU3E__1_4() { return &___U3CLoginWWWU3E__1_4; }
	inline void set_U3CLoginWWWU3E__1_4(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CLoginWWWU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLoginWWWU3E__1_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF, ___U24this_5)); }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * get_U24this_5() const { return ___U24this_5; }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPLOADSTARTTIMEU3EC__ITERATOR0_TAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#define GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T68C2581CCF05E868297EBD3F3361274954845095_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_TAD7508D45DB6679B6394983579AD18D967CC2AD4_H
#ifndef GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#define GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_TC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D_H
#ifndef RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#define RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_Raycast2DCallback
struct  Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_TE99ABF9ABC3A380677949E8C05A3E477889B82BE_H
#ifndef RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#define RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_Raycast3DCallback
struct  Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T83483916473C9710AEDB316A65CBE62C58935C5F_H
#ifndef RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#define RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache_RaycastAllCallback
struct  RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T751407A44270E02FAA43D0846A58EE6A8C4AE1CE_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef BEACONLETTER_TFEB971E49820F00AE3DFE623D77C737AB51070CD_H
#define BEACONLETTER_TFEB971E49820F00AE3DFE623D77C737AB51070CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BeaconLetter
struct  BeaconLetter_tFEB971E49820F00AE3DFE623D77C737AB51070CD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text BeaconLetter::mLetterText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mLetterText_4;
	// UnityEngine.UI.Text BeaconLetter::mLetterAmountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mLetterAmountText_5;

public:
	inline static int32_t get_offset_of_mLetterText_4() { return static_cast<int32_t>(offsetof(BeaconLetter_tFEB971E49820F00AE3DFE623D77C737AB51070CD, ___mLetterText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mLetterText_4() const { return ___mLetterText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mLetterText_4() { return &___mLetterText_4; }
	inline void set_mLetterText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mLetterText_4 = value;
		Il2CppCodeGenWriteBarrier((&___mLetterText_4), value);
	}

	inline static int32_t get_offset_of_mLetterAmountText_5() { return static_cast<int32_t>(offsetof(BeaconLetter_tFEB971E49820F00AE3DFE623D77C737AB51070CD, ___mLetterAmountText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mLetterAmountText_5() const { return ___mLetterAmountText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mLetterAmountText_5() { return &___mLetterAmountText_5; }
	inline void set_mLetterAmountText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mLetterAmountText_5 = value;
		Il2CppCodeGenWriteBarrier((&___mLetterAmountText_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEACONLETTER_TFEB971E49820F00AE3DFE623D77C737AB51070CD_H
#ifndef BEACONLETTERPANELCONTROLLER_TB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF_H
#define BEACONLETTERPANELCONTROLLER_TB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BeaconLetterPanelController
struct  BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject BeaconLetterPanelController::mBeaconLetterPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mBeaconLetterPanel_5;
	// UnityEngine.GameObject BeaconLetterPanelController::mBeaconLetter
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mBeaconLetter_6;
	// System.Collections.Generic.List`1<BeaconLetter> BeaconLetterPanelController::mBeaconLetterScripts
	List_1_t0E3039C0F56CB4633248FB700B01E40BE529D0BA * ___mBeaconLetterScripts_7;
	// System.Boolean BeaconLetterPanelController::mSerilize
	bool ___mSerilize_8;

public:
	inline static int32_t get_offset_of_mBeaconLetterPanel_5() { return static_cast<int32_t>(offsetof(BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF, ___mBeaconLetterPanel_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mBeaconLetterPanel_5() const { return ___mBeaconLetterPanel_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mBeaconLetterPanel_5() { return &___mBeaconLetterPanel_5; }
	inline void set_mBeaconLetterPanel_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mBeaconLetterPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___mBeaconLetterPanel_5), value);
	}

	inline static int32_t get_offset_of_mBeaconLetter_6() { return static_cast<int32_t>(offsetof(BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF, ___mBeaconLetter_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mBeaconLetter_6() const { return ___mBeaconLetter_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mBeaconLetter_6() { return &___mBeaconLetter_6; }
	inline void set_mBeaconLetter_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mBeaconLetter_6 = value;
		Il2CppCodeGenWriteBarrier((&___mBeaconLetter_6), value);
	}

	inline static int32_t get_offset_of_mBeaconLetterScripts_7() { return static_cast<int32_t>(offsetof(BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF, ___mBeaconLetterScripts_7)); }
	inline List_1_t0E3039C0F56CB4633248FB700B01E40BE529D0BA * get_mBeaconLetterScripts_7() const { return ___mBeaconLetterScripts_7; }
	inline List_1_t0E3039C0F56CB4633248FB700B01E40BE529D0BA ** get_address_of_mBeaconLetterScripts_7() { return &___mBeaconLetterScripts_7; }
	inline void set_mBeaconLetterScripts_7(List_1_t0E3039C0F56CB4633248FB700B01E40BE529D0BA * value)
	{
		___mBeaconLetterScripts_7 = value;
		Il2CppCodeGenWriteBarrier((&___mBeaconLetterScripts_7), value);
	}

	inline static int32_t get_offset_of_mSerilize_8() { return static_cast<int32_t>(offsetof(BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF, ___mSerilize_8)); }
	inline bool get_mSerilize_8() const { return ___mSerilize_8; }
	inline bool* get_address_of_mSerilize_8() { return &___mSerilize_8; }
	inline void set_mSerilize_8(bool value)
	{
		___mSerilize_8 = value;
	}
};

struct BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF_StaticFields
{
public:
	// BeaconLetterPanelController BeaconLetterPanelController::Instance
	BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF_StaticFields, ___Instance_4)); }
	inline BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF * get_Instance_4() const { return ___Instance_4; }
	inline BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEACONLETTERPANELCONTROLLER_TB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF_H
#ifndef CARDMANAGER_TCE6BDECC89C25A83B489E6478FD5F61998349874_H
#define CARDMANAGER_TCE6BDECC89C25A83B489E6478FD5F61998349874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CardManager
struct  CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Sprite[] CardManager::CardImgs
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___CardImgs_5;
	// System.Int32 CardManager::mCurrentCardCounter
	int32_t ___mCurrentCardCounter_6;
	// UnityEngine.UI.Image CardManager::mEmptyCardImg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___mEmptyCardImg_7;
	// UnityEngine.GameObject CardManager::mCardLetterPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mCardLetterPanel_8;
	// UnityEngine.GameObject CardManager::mLetterPrefab
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mLetterPrefab_9;
	// UnityEngine.GameObject CardManager::mFinishCardImage
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mFinishCardImage_10;
	// UnityEngine.GameObject CardManager::mWrongCardImage
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mWrongCardImage_11;
	// UnityEngine.GameObject CardManager::mCardPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mCardPanel_12;
	// BDSLiteSpeech CardManager::BDSSpeech
	BDSLiteSpeech_t0080FF8C29E1F3373FB64E23EC48B7B6A3F78B24 * ___BDSSpeech_13;
	// System.Boolean CardManager::mSpeechInDelay
	bool ___mSpeechInDelay_14;
	// System.Single CardManager::mSpeechDelayTime
	float ___mSpeechDelayTime_15;
	// System.Boolean CardManager::mLoadLetterDone
	bool ___mLoadLetterDone_16;
	// System.Boolean CardManager::mLoadCardDone
	bool ___mLoadCardDone_17;
	// System.Int32[] CardManager::HintedCardPosition
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___HintedCardPosition_18;

public:
	inline static int32_t get_offset_of_CardImgs_5() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___CardImgs_5)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_CardImgs_5() const { return ___CardImgs_5; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_CardImgs_5() { return &___CardImgs_5; }
	inline void set_CardImgs_5(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___CardImgs_5 = value;
		Il2CppCodeGenWriteBarrier((&___CardImgs_5), value);
	}

	inline static int32_t get_offset_of_mCurrentCardCounter_6() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___mCurrentCardCounter_6)); }
	inline int32_t get_mCurrentCardCounter_6() const { return ___mCurrentCardCounter_6; }
	inline int32_t* get_address_of_mCurrentCardCounter_6() { return &___mCurrentCardCounter_6; }
	inline void set_mCurrentCardCounter_6(int32_t value)
	{
		___mCurrentCardCounter_6 = value;
	}

	inline static int32_t get_offset_of_mEmptyCardImg_7() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___mEmptyCardImg_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_mEmptyCardImg_7() const { return ___mEmptyCardImg_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_mEmptyCardImg_7() { return &___mEmptyCardImg_7; }
	inline void set_mEmptyCardImg_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___mEmptyCardImg_7 = value;
		Il2CppCodeGenWriteBarrier((&___mEmptyCardImg_7), value);
	}

	inline static int32_t get_offset_of_mCardLetterPanel_8() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___mCardLetterPanel_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mCardLetterPanel_8() const { return ___mCardLetterPanel_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mCardLetterPanel_8() { return &___mCardLetterPanel_8; }
	inline void set_mCardLetterPanel_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mCardLetterPanel_8 = value;
		Il2CppCodeGenWriteBarrier((&___mCardLetterPanel_8), value);
	}

	inline static int32_t get_offset_of_mLetterPrefab_9() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___mLetterPrefab_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mLetterPrefab_9() const { return ___mLetterPrefab_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mLetterPrefab_9() { return &___mLetterPrefab_9; }
	inline void set_mLetterPrefab_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mLetterPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___mLetterPrefab_9), value);
	}

	inline static int32_t get_offset_of_mFinishCardImage_10() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___mFinishCardImage_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mFinishCardImage_10() const { return ___mFinishCardImage_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mFinishCardImage_10() { return &___mFinishCardImage_10; }
	inline void set_mFinishCardImage_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mFinishCardImage_10 = value;
		Il2CppCodeGenWriteBarrier((&___mFinishCardImage_10), value);
	}

	inline static int32_t get_offset_of_mWrongCardImage_11() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___mWrongCardImage_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mWrongCardImage_11() const { return ___mWrongCardImage_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mWrongCardImage_11() { return &___mWrongCardImage_11; }
	inline void set_mWrongCardImage_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mWrongCardImage_11 = value;
		Il2CppCodeGenWriteBarrier((&___mWrongCardImage_11), value);
	}

	inline static int32_t get_offset_of_mCardPanel_12() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___mCardPanel_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mCardPanel_12() const { return ___mCardPanel_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mCardPanel_12() { return &___mCardPanel_12; }
	inline void set_mCardPanel_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mCardPanel_12 = value;
		Il2CppCodeGenWriteBarrier((&___mCardPanel_12), value);
	}

	inline static int32_t get_offset_of_BDSSpeech_13() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___BDSSpeech_13)); }
	inline BDSLiteSpeech_t0080FF8C29E1F3373FB64E23EC48B7B6A3F78B24 * get_BDSSpeech_13() const { return ___BDSSpeech_13; }
	inline BDSLiteSpeech_t0080FF8C29E1F3373FB64E23EC48B7B6A3F78B24 ** get_address_of_BDSSpeech_13() { return &___BDSSpeech_13; }
	inline void set_BDSSpeech_13(BDSLiteSpeech_t0080FF8C29E1F3373FB64E23EC48B7B6A3F78B24 * value)
	{
		___BDSSpeech_13 = value;
		Il2CppCodeGenWriteBarrier((&___BDSSpeech_13), value);
	}

	inline static int32_t get_offset_of_mSpeechInDelay_14() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___mSpeechInDelay_14)); }
	inline bool get_mSpeechInDelay_14() const { return ___mSpeechInDelay_14; }
	inline bool* get_address_of_mSpeechInDelay_14() { return &___mSpeechInDelay_14; }
	inline void set_mSpeechInDelay_14(bool value)
	{
		___mSpeechInDelay_14 = value;
	}

	inline static int32_t get_offset_of_mSpeechDelayTime_15() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___mSpeechDelayTime_15)); }
	inline float get_mSpeechDelayTime_15() const { return ___mSpeechDelayTime_15; }
	inline float* get_address_of_mSpeechDelayTime_15() { return &___mSpeechDelayTime_15; }
	inline void set_mSpeechDelayTime_15(float value)
	{
		___mSpeechDelayTime_15 = value;
	}

	inline static int32_t get_offset_of_mLoadLetterDone_16() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___mLoadLetterDone_16)); }
	inline bool get_mLoadLetterDone_16() const { return ___mLoadLetterDone_16; }
	inline bool* get_address_of_mLoadLetterDone_16() { return &___mLoadLetterDone_16; }
	inline void set_mLoadLetterDone_16(bool value)
	{
		___mLoadLetterDone_16 = value;
	}

	inline static int32_t get_offset_of_mLoadCardDone_17() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___mLoadCardDone_17)); }
	inline bool get_mLoadCardDone_17() const { return ___mLoadCardDone_17; }
	inline bool* get_address_of_mLoadCardDone_17() { return &___mLoadCardDone_17; }
	inline void set_mLoadCardDone_17(bool value)
	{
		___mLoadCardDone_17 = value;
	}

	inline static int32_t get_offset_of_HintedCardPosition_18() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874, ___HintedCardPosition_18)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_HintedCardPosition_18() const { return ___HintedCardPosition_18; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_HintedCardPosition_18() { return &___HintedCardPosition_18; }
	inline void set_HintedCardPosition_18(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___HintedCardPosition_18 = value;
		Il2CppCodeGenWriteBarrier((&___HintedCardPosition_18), value);
	}
};

struct CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874_StaticFields
{
public:
	// CardManager CardManager::mInstance
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874 * ___mInstance_4;

public:
	inline static int32_t get_offset_of_mInstance_4() { return static_cast<int32_t>(offsetof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874_StaticFields, ___mInstance_4)); }
	inline CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874 * get_mInstance_4() const { return ___mInstance_4; }
	inline CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874 ** get_address_of_mInstance_4() { return &___mInstance_4; }
	inline void set_mInstance_4(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874 * value)
	{
		___mInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CARDMANAGER_TCE6BDECC89C25A83B489E6478FD5F61998349874_H
#ifndef COMPLETEIMAGESETTER_T93D129BA78346C5652563D4BAA8CAC3F255E2C82_H
#define COMPLETEIMAGESETTER_T93D129BA78346C5652563D4BAA8CAC3F255E2C82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CompleteImageSetter
struct  CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text CompleteImageSetter::mTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mTime_4;
	// UnityEngine.UI.Text CompleteImageSetter::mReward
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mReward_5;
	// UnityEngine.UI.Text CompleteImageSetter::mMissionName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mMissionName_6;
	// UnityEngine.UI.Text CompleteImageSetter::mCollectText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mCollectText_7;
	// UnityEngine.UI.GridLayoutGroup CompleteImageSetter::mCompleteCardLayout
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * ___mCompleteCardLayout_8;
	// UnityEngine.GameObject CompleteImageSetter::mRewardCardPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mRewardCardPanel_9;
	// UnityEngine.GameObject CompleteImageSetter::mCompleteCard
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mCompleteCard_10;
	// UnityEngine.RectTransform CompleteImageSetter::mCompleteCardRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___mCompleteCardRect_11;
	// UnityEngine.UI.Image CompleteImageSetter::mCompleteStar
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___mCompleteStar_12;
	// UnityEngine.Sprite[] CompleteImageSetter::mCompletePic
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___mCompletePic_13;

public:
	inline static int32_t get_offset_of_mTime_4() { return static_cast<int32_t>(offsetof(CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82, ___mTime_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mTime_4() const { return ___mTime_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mTime_4() { return &___mTime_4; }
	inline void set_mTime_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mTime_4 = value;
		Il2CppCodeGenWriteBarrier((&___mTime_4), value);
	}

	inline static int32_t get_offset_of_mReward_5() { return static_cast<int32_t>(offsetof(CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82, ___mReward_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mReward_5() const { return ___mReward_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mReward_5() { return &___mReward_5; }
	inline void set_mReward_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mReward_5 = value;
		Il2CppCodeGenWriteBarrier((&___mReward_5), value);
	}

	inline static int32_t get_offset_of_mMissionName_6() { return static_cast<int32_t>(offsetof(CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82, ___mMissionName_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mMissionName_6() const { return ___mMissionName_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mMissionName_6() { return &___mMissionName_6; }
	inline void set_mMissionName_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mMissionName_6 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionName_6), value);
	}

	inline static int32_t get_offset_of_mCollectText_7() { return static_cast<int32_t>(offsetof(CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82, ___mCollectText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mCollectText_7() const { return ___mCollectText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mCollectText_7() { return &___mCollectText_7; }
	inline void set_mCollectText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mCollectText_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCollectText_7), value);
	}

	inline static int32_t get_offset_of_mCompleteCardLayout_8() { return static_cast<int32_t>(offsetof(CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82, ___mCompleteCardLayout_8)); }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * get_mCompleteCardLayout_8() const { return ___mCompleteCardLayout_8; }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 ** get_address_of_mCompleteCardLayout_8() { return &___mCompleteCardLayout_8; }
	inline void set_mCompleteCardLayout_8(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * value)
	{
		___mCompleteCardLayout_8 = value;
		Il2CppCodeGenWriteBarrier((&___mCompleteCardLayout_8), value);
	}

	inline static int32_t get_offset_of_mRewardCardPanel_9() { return static_cast<int32_t>(offsetof(CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82, ___mRewardCardPanel_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mRewardCardPanel_9() const { return ___mRewardCardPanel_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mRewardCardPanel_9() { return &___mRewardCardPanel_9; }
	inline void set_mRewardCardPanel_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mRewardCardPanel_9 = value;
		Il2CppCodeGenWriteBarrier((&___mRewardCardPanel_9), value);
	}

	inline static int32_t get_offset_of_mCompleteCard_10() { return static_cast<int32_t>(offsetof(CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82, ___mCompleteCard_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mCompleteCard_10() const { return ___mCompleteCard_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mCompleteCard_10() { return &___mCompleteCard_10; }
	inline void set_mCompleteCard_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mCompleteCard_10 = value;
		Il2CppCodeGenWriteBarrier((&___mCompleteCard_10), value);
	}

	inline static int32_t get_offset_of_mCompleteCardRect_11() { return static_cast<int32_t>(offsetof(CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82, ___mCompleteCardRect_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_mCompleteCardRect_11() const { return ___mCompleteCardRect_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_mCompleteCardRect_11() { return &___mCompleteCardRect_11; }
	inline void set_mCompleteCardRect_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___mCompleteCardRect_11 = value;
		Il2CppCodeGenWriteBarrier((&___mCompleteCardRect_11), value);
	}

	inline static int32_t get_offset_of_mCompleteStar_12() { return static_cast<int32_t>(offsetof(CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82, ___mCompleteStar_12)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_mCompleteStar_12() const { return ___mCompleteStar_12; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_mCompleteStar_12() { return &___mCompleteStar_12; }
	inline void set_mCompleteStar_12(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___mCompleteStar_12 = value;
		Il2CppCodeGenWriteBarrier((&___mCompleteStar_12), value);
	}

	inline static int32_t get_offset_of_mCompletePic_13() { return static_cast<int32_t>(offsetof(CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82, ___mCompletePic_13)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_mCompletePic_13() const { return ___mCompletePic_13; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_mCompletePic_13() { return &___mCompletePic_13; }
	inline void set_mCompletePic_13(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___mCompletePic_13 = value;
		Il2CppCodeGenWriteBarrier((&___mCompletePic_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPLETEIMAGESETTER_T93D129BA78346C5652563D4BAA8CAC3F255E2C82_H
#ifndef DETECTLEAKS_TA86D73B553AC571DC63632E443A4797B48CE76B9_H
#define DETECTLEAKS_TA86D73B553AC571DC63632E443A4797B48CE76B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DetectLeaks
struct  DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Object[] DetectLeaks::originalObjs
	ObjectU5BU5D_tE519E5BBCA48F8FEAE68926638261BD14A981AB9* ___originalObjs_5;
	// System.Boolean DetectLeaks::isFirstSet
	bool ___isFirstSet_6;

public:
	inline static int32_t get_offset_of_originalObjs_5() { return static_cast<int32_t>(offsetof(DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9, ___originalObjs_5)); }
	inline ObjectU5BU5D_tE519E5BBCA48F8FEAE68926638261BD14A981AB9* get_originalObjs_5() const { return ___originalObjs_5; }
	inline ObjectU5BU5D_tE519E5BBCA48F8FEAE68926638261BD14A981AB9** get_address_of_originalObjs_5() { return &___originalObjs_5; }
	inline void set_originalObjs_5(ObjectU5BU5D_tE519E5BBCA48F8FEAE68926638261BD14A981AB9* value)
	{
		___originalObjs_5 = value;
		Il2CppCodeGenWriteBarrier((&___originalObjs_5), value);
	}

	inline static int32_t get_offset_of_isFirstSet_6() { return static_cast<int32_t>(offsetof(DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9, ___isFirstSet_6)); }
	inline bool get_isFirstSet_6() const { return ___isFirstSet_6; }
	inline bool* get_address_of_isFirstSet_6() { return &___isFirstSet_6; }
	inline void set_isFirstSet_6(bool value)
	{
		___isFirstSet_6 = value;
	}
};

struct DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9_StaticFields
{
public:
	// DetectLeaks DetectLeaks::instance
	DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9 * ___instance_4;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9_StaticFields, ___instance_4)); }
	inline DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9 * get_instance_4() const { return ___instance_4; }
	inline DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTLEAKS_TA86D73B553AC571DC63632E443A4797B48CE76B9_H
#ifndef DOWNLOADIMAGE_T18130CA1223AEF7B4DC12A624C828FEA1E6DDF13_H
#define DOWNLOADIMAGE_T18130CA1223AEF7B4DC12A624C828FEA1E6DDF13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DownloadImage
struct  DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image DownloadImage::mCardImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___mCardImage_4;
	// UnityEngine.RectTransform DownloadImage::mCardTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___mCardTransform_5;
	// UnityEngine.UI.Text DownloadImage::mCardText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mCardText_6;
	// System.Boolean DownloadImage::CardInstantiate
	bool ___CardInstantiate_7;

public:
	inline static int32_t get_offset_of_mCardImage_4() { return static_cast<int32_t>(offsetof(DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13, ___mCardImage_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_mCardImage_4() const { return ___mCardImage_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_mCardImage_4() { return &___mCardImage_4; }
	inline void set_mCardImage_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___mCardImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCardImage_4), value);
	}

	inline static int32_t get_offset_of_mCardTransform_5() { return static_cast<int32_t>(offsetof(DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13, ___mCardTransform_5)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_mCardTransform_5() const { return ___mCardTransform_5; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_mCardTransform_5() { return &___mCardTransform_5; }
	inline void set_mCardTransform_5(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___mCardTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___mCardTransform_5), value);
	}

	inline static int32_t get_offset_of_mCardText_6() { return static_cast<int32_t>(offsetof(DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13, ___mCardText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mCardText_6() const { return ___mCardText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mCardText_6() { return &___mCardText_6; }
	inline void set_mCardText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mCardText_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCardText_6), value);
	}

	inline static int32_t get_offset_of_CardInstantiate_7() { return static_cast<int32_t>(offsetof(DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13, ___CardInstantiate_7)); }
	inline bool get_CardInstantiate_7() const { return ___CardInstantiate_7; }
	inline bool* get_address_of_CardInstantiate_7() { return &___CardInstantiate_7; }
	inline void set_CardInstantiate_7(bool value)
	{
		___CardInstantiate_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWNLOADIMAGE_T18130CA1223AEF7B4DC12A624C828FEA1E6DDF13_H
#ifndef FILLINWORD_T3D157EE5A803EAC523500F3475D142B55D29E365_H
#define FILLINWORD_T3D157EE5A803EAC523500F3475D142B55D29E365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FillInWord
struct  FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text FillInWord::mWord
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mWord_4;
	// System.Single FillInWord::mIncreaceSize
	float ___mIncreaceSize_5;
	// UnityEngine.UI.Image FillInWord::ImgColor
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___ImgColor_6;
	// UnityEngine.Transform FillInWord::mStartParent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mStartParent_7;
	// UnityEngine.Vector3 FillInWord::mOriginalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___mOriginalPosition_9;
	// UnityEngine.Vector3 FillInWord::originalscale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___originalscale_10;

public:
	inline static int32_t get_offset_of_mWord_4() { return static_cast<int32_t>(offsetof(FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365, ___mWord_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mWord_4() const { return ___mWord_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mWord_4() { return &___mWord_4; }
	inline void set_mWord_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mWord_4 = value;
		Il2CppCodeGenWriteBarrier((&___mWord_4), value);
	}

	inline static int32_t get_offset_of_mIncreaceSize_5() { return static_cast<int32_t>(offsetof(FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365, ___mIncreaceSize_5)); }
	inline float get_mIncreaceSize_5() const { return ___mIncreaceSize_5; }
	inline float* get_address_of_mIncreaceSize_5() { return &___mIncreaceSize_5; }
	inline void set_mIncreaceSize_5(float value)
	{
		___mIncreaceSize_5 = value;
	}

	inline static int32_t get_offset_of_ImgColor_6() { return static_cast<int32_t>(offsetof(FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365, ___ImgColor_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_ImgColor_6() const { return ___ImgColor_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_ImgColor_6() { return &___ImgColor_6; }
	inline void set_ImgColor_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___ImgColor_6 = value;
		Il2CppCodeGenWriteBarrier((&___ImgColor_6), value);
	}

	inline static int32_t get_offset_of_mStartParent_7() { return static_cast<int32_t>(offsetof(FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365, ___mStartParent_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mStartParent_7() const { return ___mStartParent_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mStartParent_7() { return &___mStartParent_7; }
	inline void set_mStartParent_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mStartParent_7 = value;
		Il2CppCodeGenWriteBarrier((&___mStartParent_7), value);
	}

	inline static int32_t get_offset_of_mOriginalPosition_9() { return static_cast<int32_t>(offsetof(FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365, ___mOriginalPosition_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_mOriginalPosition_9() const { return ___mOriginalPosition_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_mOriginalPosition_9() { return &___mOriginalPosition_9; }
	inline void set_mOriginalPosition_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___mOriginalPosition_9 = value;
	}

	inline static int32_t get_offset_of_originalscale_10() { return static_cast<int32_t>(offsetof(FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365, ___originalscale_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_originalscale_10() const { return ___originalscale_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_originalscale_10() { return &___originalscale_10; }
	inline void set_originalscale_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___originalscale_10 = value;
	}
};

struct FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365_StaticFields
{
public:
	// UnityEngine.GameObject FillInWord::ItemBeginDrag
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ItemBeginDrag_8;

public:
	inline static int32_t get_offset_of_ItemBeginDrag_8() { return static_cast<int32_t>(offsetof(FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365_StaticFields, ___ItemBeginDrag_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ItemBeginDrag_8() const { return ___ItemBeginDrag_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ItemBeginDrag_8() { return &___ItemBeginDrag_8; }
	inline void set_ItemBeginDrag_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ItemBeginDrag_8 = value;
		Il2CppCodeGenWriteBarrier((&___ItemBeginDrag_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLINWORD_T3D157EE5A803EAC523500F3475D142B55D29E365_H
#ifndef GAMEMANAGE_T2EF28DD9376BDE665CDFFA3DF4087BA68DA83965_H
#define GAMEMANAGE_T2EF28DD9376BDE665CDFFA3DF4087BA68DA83965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManage
struct  GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// PlayerData GameManage::ThePlayerData
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * ___ThePlayerData_5;
	// UnityEngine.GameObject GameManage::mShowRewardCard
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mShowRewardCard_6;
	// UnityEngine.UI.Image GameManage::mRewardCardImg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___mRewardCardImg_7;
	// UnityEngine.UI.Text GameManage::mRewardCardText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mRewardCardText_8;

public:
	inline static int32_t get_offset_of_ThePlayerData_5() { return static_cast<int32_t>(offsetof(GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965, ___ThePlayerData_5)); }
	inline PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * get_ThePlayerData_5() const { return ___ThePlayerData_5; }
	inline PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 ** get_address_of_ThePlayerData_5() { return &___ThePlayerData_5; }
	inline void set_ThePlayerData_5(PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043 * value)
	{
		___ThePlayerData_5 = value;
		Il2CppCodeGenWriteBarrier((&___ThePlayerData_5), value);
	}

	inline static int32_t get_offset_of_mShowRewardCard_6() { return static_cast<int32_t>(offsetof(GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965, ___mShowRewardCard_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mShowRewardCard_6() const { return ___mShowRewardCard_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mShowRewardCard_6() { return &___mShowRewardCard_6; }
	inline void set_mShowRewardCard_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mShowRewardCard_6 = value;
		Il2CppCodeGenWriteBarrier((&___mShowRewardCard_6), value);
	}

	inline static int32_t get_offset_of_mRewardCardImg_7() { return static_cast<int32_t>(offsetof(GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965, ___mRewardCardImg_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_mRewardCardImg_7() const { return ___mRewardCardImg_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_mRewardCardImg_7() { return &___mRewardCardImg_7; }
	inline void set_mRewardCardImg_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___mRewardCardImg_7 = value;
		Il2CppCodeGenWriteBarrier((&___mRewardCardImg_7), value);
	}

	inline static int32_t get_offset_of_mRewardCardText_8() { return static_cast<int32_t>(offsetof(GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965, ___mRewardCardText_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mRewardCardText_8() const { return ___mRewardCardText_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mRewardCardText_8() { return &___mRewardCardText_8; }
	inline void set_mRewardCardText_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mRewardCardText_8 = value;
		Il2CppCodeGenWriteBarrier((&___mRewardCardText_8), value);
	}
};

struct GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965_StaticFields
{
public:
	// GameManage GameManage::mInsatnce
	GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965 * ___mInsatnce_4;

public:
	inline static int32_t get_offset_of_mInsatnce_4() { return static_cast<int32_t>(offsetof(GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965_StaticFields, ___mInsatnce_4)); }
	inline GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965 * get_mInsatnce_4() const { return ___mInsatnce_4; }
	inline GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965 ** get_address_of_mInsatnce_4() { return &___mInsatnce_4; }
	inline void set_mInsatnce_4(GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965 * value)
	{
		___mInsatnce_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInsatnce_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEMANAGE_T2EF28DD9376BDE665CDFFA3DF4087BA68DA83965_H
#ifndef LETTERCONTROLLER_TB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D_H
#define LETTERCONTROLLER_TB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LetterController
struct  LetterController_tB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text LetterController::mLetter
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mLetter_4;
	// UnityEngine.UI.Text LetterController::mLetterAmountText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mLetterAmountText_5;
	// System.String LetterController::Letter
	String_t* ___Letter_6;
	// System.Int32 LetterController::mLetterAmount
	int32_t ___mLetterAmount_7;
	// System.Boolean LetterController::mSerilize
	bool ___mSerilize_8;

public:
	inline static int32_t get_offset_of_mLetter_4() { return static_cast<int32_t>(offsetof(LetterController_tB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D, ___mLetter_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mLetter_4() const { return ___mLetter_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mLetter_4() { return &___mLetter_4; }
	inline void set_mLetter_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mLetter_4 = value;
		Il2CppCodeGenWriteBarrier((&___mLetter_4), value);
	}

	inline static int32_t get_offset_of_mLetterAmountText_5() { return static_cast<int32_t>(offsetof(LetterController_tB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D, ___mLetterAmountText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mLetterAmountText_5() const { return ___mLetterAmountText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mLetterAmountText_5() { return &___mLetterAmountText_5; }
	inline void set_mLetterAmountText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mLetterAmountText_5 = value;
		Il2CppCodeGenWriteBarrier((&___mLetterAmountText_5), value);
	}

	inline static int32_t get_offset_of_Letter_6() { return static_cast<int32_t>(offsetof(LetterController_tB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D, ___Letter_6)); }
	inline String_t* get_Letter_6() const { return ___Letter_6; }
	inline String_t** get_address_of_Letter_6() { return &___Letter_6; }
	inline void set_Letter_6(String_t* value)
	{
		___Letter_6 = value;
		Il2CppCodeGenWriteBarrier((&___Letter_6), value);
	}

	inline static int32_t get_offset_of_mLetterAmount_7() { return static_cast<int32_t>(offsetof(LetterController_tB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D, ___mLetterAmount_7)); }
	inline int32_t get_mLetterAmount_7() const { return ___mLetterAmount_7; }
	inline int32_t* get_address_of_mLetterAmount_7() { return &___mLetterAmount_7; }
	inline void set_mLetterAmount_7(int32_t value)
	{
		___mLetterAmount_7 = value;
	}

	inline static int32_t get_offset_of_mSerilize_8() { return static_cast<int32_t>(offsetof(LetterController_tB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D, ___mSerilize_8)); }
	inline bool get_mSerilize_8() const { return ___mSerilize_8; }
	inline bool* get_address_of_mSerilize_8() { return &___mSerilize_8; }
	inline void set_mSerilize_8(bool value)
	{
		___mSerilize_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERCONTROLLER_TB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D_H
#ifndef LETTERCUBECONTROLLER_TA8459A17CE4FDE57AE4BA3E75B435030F794AD7E_H
#define LETTERCUBECONTROLLER_TA8459A17CE4FDE57AE4BA3E75B435030F794AD7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LetterCubeController
struct  LetterCubeController_tA8459A17CE4FDE57AE4BA3E75B435030F794AD7E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String LetterCubeController::mLetter
	String_t* ___mLetter_4;
	// UnityEngine.TextMesh[] LetterCubeController::texts
	TextMeshU5BU5D_t6C46DABCE2E499BCAB13DDDEC1393FDE4A69DAAE* ___texts_5;
	// UnityEngine.Renderer LetterCubeController::dicemesh
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___dicemesh_6;

public:
	inline static int32_t get_offset_of_mLetter_4() { return static_cast<int32_t>(offsetof(LetterCubeController_tA8459A17CE4FDE57AE4BA3E75B435030F794AD7E, ___mLetter_4)); }
	inline String_t* get_mLetter_4() const { return ___mLetter_4; }
	inline String_t** get_address_of_mLetter_4() { return &___mLetter_4; }
	inline void set_mLetter_4(String_t* value)
	{
		___mLetter_4 = value;
		Il2CppCodeGenWriteBarrier((&___mLetter_4), value);
	}

	inline static int32_t get_offset_of_texts_5() { return static_cast<int32_t>(offsetof(LetterCubeController_tA8459A17CE4FDE57AE4BA3E75B435030F794AD7E, ___texts_5)); }
	inline TextMeshU5BU5D_t6C46DABCE2E499BCAB13DDDEC1393FDE4A69DAAE* get_texts_5() const { return ___texts_5; }
	inline TextMeshU5BU5D_t6C46DABCE2E499BCAB13DDDEC1393FDE4A69DAAE** get_address_of_texts_5() { return &___texts_5; }
	inline void set_texts_5(TextMeshU5BU5D_t6C46DABCE2E499BCAB13DDDEC1393FDE4A69DAAE* value)
	{
		___texts_5 = value;
		Il2CppCodeGenWriteBarrier((&___texts_5), value);
	}

	inline static int32_t get_offset_of_dicemesh_6() { return static_cast<int32_t>(offsetof(LetterCubeController_tA8459A17CE4FDE57AE4BA3E75B435030F794AD7E, ___dicemesh_6)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_dicemesh_6() const { return ___dicemesh_6; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_dicemesh_6() { return &___dicemesh_6; }
	inline void set_dicemesh_6(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___dicemesh_6 = value;
		Il2CppCodeGenWriteBarrier((&___dicemesh_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LETTERCUBECONTROLLER_TA8459A17CE4FDE57AE4BA3E75B435030F794AD7E_H
#ifndef LOADRANK_TDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52_H
#define LOADRANK_TDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadRank
struct  LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text LoadRank::mNameText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mNameText_4;
	// UnityEngine.UI.Text LoadRank::mMoneyText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mMoneyText_5;
	// UnityEngine.UI.Text LoadRank::mRankText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mRankText_6;
	// UnityEngine.UI.Image LoadRank::mRankImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___mRankImage_7;
	// UnityEngine.UI.Image LoadRank::mOtherRankImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___mOtherRankImage_8;
	// UnityEngine.Sprite[] LoadRank::mRankLevelImg
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___mRankLevelImg_9;
	// UnityEngine.RectTransform LoadRank::mMyTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___mMyTransform_10;
	// System.Int32 LoadRank::mRank
	int32_t ___mRank_11;
	// SimpleJSON.JSONNode LoadRank::mNode
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___mNode_12;
	// System.Boolean LoadRank::mLoadOnAwake
	bool ___mLoadOnAwake_13;
	// LoadRank_RankMode LoadRank::mRankMode
	int32_t ___mRankMode_14;

public:
	inline static int32_t get_offset_of_mNameText_4() { return static_cast<int32_t>(offsetof(LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52, ___mNameText_4)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mNameText_4() const { return ___mNameText_4; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mNameText_4() { return &___mNameText_4; }
	inline void set_mNameText_4(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mNameText_4 = value;
		Il2CppCodeGenWriteBarrier((&___mNameText_4), value);
	}

	inline static int32_t get_offset_of_mMoneyText_5() { return static_cast<int32_t>(offsetof(LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52, ___mMoneyText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mMoneyText_5() const { return ___mMoneyText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mMoneyText_5() { return &___mMoneyText_5; }
	inline void set_mMoneyText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mMoneyText_5 = value;
		Il2CppCodeGenWriteBarrier((&___mMoneyText_5), value);
	}

	inline static int32_t get_offset_of_mRankText_6() { return static_cast<int32_t>(offsetof(LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52, ___mRankText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mRankText_6() const { return ___mRankText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mRankText_6() { return &___mRankText_6; }
	inline void set_mRankText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mRankText_6 = value;
		Il2CppCodeGenWriteBarrier((&___mRankText_6), value);
	}

	inline static int32_t get_offset_of_mRankImage_7() { return static_cast<int32_t>(offsetof(LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52, ___mRankImage_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_mRankImage_7() const { return ___mRankImage_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_mRankImage_7() { return &___mRankImage_7; }
	inline void set_mRankImage_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___mRankImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___mRankImage_7), value);
	}

	inline static int32_t get_offset_of_mOtherRankImage_8() { return static_cast<int32_t>(offsetof(LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52, ___mOtherRankImage_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_mOtherRankImage_8() const { return ___mOtherRankImage_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_mOtherRankImage_8() { return &___mOtherRankImage_8; }
	inline void set_mOtherRankImage_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___mOtherRankImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___mOtherRankImage_8), value);
	}

	inline static int32_t get_offset_of_mRankLevelImg_9() { return static_cast<int32_t>(offsetof(LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52, ___mRankLevelImg_9)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_mRankLevelImg_9() const { return ___mRankLevelImg_9; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_mRankLevelImg_9() { return &___mRankLevelImg_9; }
	inline void set_mRankLevelImg_9(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___mRankLevelImg_9 = value;
		Il2CppCodeGenWriteBarrier((&___mRankLevelImg_9), value);
	}

	inline static int32_t get_offset_of_mMyTransform_10() { return static_cast<int32_t>(offsetof(LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52, ___mMyTransform_10)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_mMyTransform_10() const { return ___mMyTransform_10; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_mMyTransform_10() { return &___mMyTransform_10; }
	inline void set_mMyTransform_10(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___mMyTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___mMyTransform_10), value);
	}

	inline static int32_t get_offset_of_mRank_11() { return static_cast<int32_t>(offsetof(LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52, ___mRank_11)); }
	inline int32_t get_mRank_11() const { return ___mRank_11; }
	inline int32_t* get_address_of_mRank_11() { return &___mRank_11; }
	inline void set_mRank_11(int32_t value)
	{
		___mRank_11 = value;
	}

	inline static int32_t get_offset_of_mNode_12() { return static_cast<int32_t>(offsetof(LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52, ___mNode_12)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_mNode_12() const { return ___mNode_12; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_mNode_12() { return &___mNode_12; }
	inline void set_mNode_12(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___mNode_12 = value;
		Il2CppCodeGenWriteBarrier((&___mNode_12), value);
	}

	inline static int32_t get_offset_of_mLoadOnAwake_13() { return static_cast<int32_t>(offsetof(LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52, ___mLoadOnAwake_13)); }
	inline bool get_mLoadOnAwake_13() const { return ___mLoadOnAwake_13; }
	inline bool* get_address_of_mLoadOnAwake_13() { return &___mLoadOnAwake_13; }
	inline void set_mLoadOnAwake_13(bool value)
	{
		___mLoadOnAwake_13 = value;
	}

	inline static int32_t get_offset_of_mRankMode_14() { return static_cast<int32_t>(offsetof(LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52, ___mRankMode_14)); }
	inline int32_t get_mRankMode_14() const { return ___mRankMode_14; }
	inline int32_t* get_address_of_mRankMode_14() { return &___mRankMode_14; }
	inline void set_mRankMode_14(int32_t value)
	{
		___mRankMode_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADRANK_TDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52_H
#ifndef LOGINUI_T9275CF57F5DE81344789D50B0345F66BEEDA581A_H
#define LOGINUI_T9275CF57F5DE81344789D50B0345F66BEEDA581A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginUI
struct  LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.InputField LoginUI::mAccount
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___mAccount_4;
	// UnityEngine.UI.InputField LoginUI::mPassword
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___mPassword_5;
	// UnityEngine.UI.Image LoginUI::KeepLoginImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___KeepLoginImage_6;
	// UnityEngine.UI.Text LoginUI::mWrongIDText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mWrongIDText_7;
	// UnityEngine.UI.Image LoginUI::mBigDipperLogo
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___mBigDipperLogo_8;
	// System.Single LoginUI::mShowLogoSpeed
	float ___mShowLogoSpeed_9;
	// System.Single LoginUI::mShowLogoTime
	float ___mShowLogoTime_10;
	// System.Single LoginUI::mFadeLogoSpeed
	float ___mFadeLogoSpeed_11;
	// UnityEngine.GameObject LoginUI::mLoginPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mLoginPanel_12;
	// System.Boolean LoginUI::mLoadLoginData
	bool ___mLoadLoginData_14;

public:
	inline static int32_t get_offset_of_mAccount_4() { return static_cast<int32_t>(offsetof(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A, ___mAccount_4)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_mAccount_4() const { return ___mAccount_4; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_mAccount_4() { return &___mAccount_4; }
	inline void set_mAccount_4(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___mAccount_4 = value;
		Il2CppCodeGenWriteBarrier((&___mAccount_4), value);
	}

	inline static int32_t get_offset_of_mPassword_5() { return static_cast<int32_t>(offsetof(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A, ___mPassword_5)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_mPassword_5() const { return ___mPassword_5; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_mPassword_5() { return &___mPassword_5; }
	inline void set_mPassword_5(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___mPassword_5 = value;
		Il2CppCodeGenWriteBarrier((&___mPassword_5), value);
	}

	inline static int32_t get_offset_of_KeepLoginImage_6() { return static_cast<int32_t>(offsetof(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A, ___KeepLoginImage_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_KeepLoginImage_6() const { return ___KeepLoginImage_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_KeepLoginImage_6() { return &___KeepLoginImage_6; }
	inline void set_KeepLoginImage_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___KeepLoginImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___KeepLoginImage_6), value);
	}

	inline static int32_t get_offset_of_mWrongIDText_7() { return static_cast<int32_t>(offsetof(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A, ___mWrongIDText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mWrongIDText_7() const { return ___mWrongIDText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mWrongIDText_7() { return &___mWrongIDText_7; }
	inline void set_mWrongIDText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mWrongIDText_7 = value;
		Il2CppCodeGenWriteBarrier((&___mWrongIDText_7), value);
	}

	inline static int32_t get_offset_of_mBigDipperLogo_8() { return static_cast<int32_t>(offsetof(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A, ___mBigDipperLogo_8)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_mBigDipperLogo_8() const { return ___mBigDipperLogo_8; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_mBigDipperLogo_8() { return &___mBigDipperLogo_8; }
	inline void set_mBigDipperLogo_8(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___mBigDipperLogo_8 = value;
		Il2CppCodeGenWriteBarrier((&___mBigDipperLogo_8), value);
	}

	inline static int32_t get_offset_of_mShowLogoSpeed_9() { return static_cast<int32_t>(offsetof(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A, ___mShowLogoSpeed_9)); }
	inline float get_mShowLogoSpeed_9() const { return ___mShowLogoSpeed_9; }
	inline float* get_address_of_mShowLogoSpeed_9() { return &___mShowLogoSpeed_9; }
	inline void set_mShowLogoSpeed_9(float value)
	{
		___mShowLogoSpeed_9 = value;
	}

	inline static int32_t get_offset_of_mShowLogoTime_10() { return static_cast<int32_t>(offsetof(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A, ___mShowLogoTime_10)); }
	inline float get_mShowLogoTime_10() const { return ___mShowLogoTime_10; }
	inline float* get_address_of_mShowLogoTime_10() { return &___mShowLogoTime_10; }
	inline void set_mShowLogoTime_10(float value)
	{
		___mShowLogoTime_10 = value;
	}

	inline static int32_t get_offset_of_mFadeLogoSpeed_11() { return static_cast<int32_t>(offsetof(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A, ___mFadeLogoSpeed_11)); }
	inline float get_mFadeLogoSpeed_11() const { return ___mFadeLogoSpeed_11; }
	inline float* get_address_of_mFadeLogoSpeed_11() { return &___mFadeLogoSpeed_11; }
	inline void set_mFadeLogoSpeed_11(float value)
	{
		___mFadeLogoSpeed_11 = value;
	}

	inline static int32_t get_offset_of_mLoginPanel_12() { return static_cast<int32_t>(offsetof(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A, ___mLoginPanel_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mLoginPanel_12() const { return ___mLoginPanel_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mLoginPanel_12() { return &___mLoginPanel_12; }
	inline void set_mLoginPanel_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mLoginPanel_12 = value;
		Il2CppCodeGenWriteBarrier((&___mLoginPanel_12), value);
	}

	inline static int32_t get_offset_of_mLoadLoginData_14() { return static_cast<int32_t>(offsetof(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A, ___mLoadLoginData_14)); }
	inline bool get_mLoadLoginData_14() const { return ___mLoadLoginData_14; }
	inline bool* get_address_of_mLoadLoginData_14() { return &___mLoadLoginData_14; }
	inline void set_mLoadLoginData_14(bool value)
	{
		___mLoadLoginData_14 = value;
	}
};

struct LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A_StaticFields
{
public:
	// LoginUI LoginUI::mInstance
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A * ___mInstance_13;

public:
	inline static int32_t get_offset_of_mInstance_13() { return static_cast<int32_t>(offsetof(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A_StaticFields, ___mInstance_13)); }
	inline LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A * get_mInstance_13() const { return ___mInstance_13; }
	inline LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A ** get_address_of_mInstance_13() { return &___mInstance_13; }
	inline void set_mInstance_13(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A * value)
	{
		___mInstance_13 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINUI_T9275CF57F5DE81344789D50B0345F66BEEDA581A_H
#ifndef MISSIONBUTTONDATA_T7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9_H
#define MISSIONBUTTONDATA_T7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionButtonData
struct  MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 MissionButtonData::mMissionNumber
	int32_t ___mMissionNumber_4;
	// System.String MissionButtonData::mMissionName
	String_t* ___mMissionName_5;
	// System.Int32 MissionButtonData::IsDone
	int32_t ___IsDone_6;
	// System.Boolean MissionButtonData::IsDo
	bool ___IsDo_7;
	// UnityEngine.UI.Text MissionButtonData::mMissionNameText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mMissionNameText_8;
	// UnityEngine.UI.Image[] MissionButtonData::mStar
	ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* ___mStar_9;
	// UnityEngine.Sprite MissionButtonData::mStarImage
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___mStarImage_10;
	// UnityEngine.Sprite MissionButtonData::mStarGrayImage
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___mStarGrayImage_11;
	// UnityEngine.UI.Image MissionButtonData::mIsCompleteImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___mIsCompleteImage_12;
	// UnityEngine.Sprite[] MissionButtonData::mCompleteImage
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___mCompleteImage_13;

public:
	inline static int32_t get_offset_of_mMissionNumber_4() { return static_cast<int32_t>(offsetof(MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9, ___mMissionNumber_4)); }
	inline int32_t get_mMissionNumber_4() const { return ___mMissionNumber_4; }
	inline int32_t* get_address_of_mMissionNumber_4() { return &___mMissionNumber_4; }
	inline void set_mMissionNumber_4(int32_t value)
	{
		___mMissionNumber_4 = value;
	}

	inline static int32_t get_offset_of_mMissionName_5() { return static_cast<int32_t>(offsetof(MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9, ___mMissionName_5)); }
	inline String_t* get_mMissionName_5() const { return ___mMissionName_5; }
	inline String_t** get_address_of_mMissionName_5() { return &___mMissionName_5; }
	inline void set_mMissionName_5(String_t* value)
	{
		___mMissionName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionName_5), value);
	}

	inline static int32_t get_offset_of_IsDone_6() { return static_cast<int32_t>(offsetof(MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9, ___IsDone_6)); }
	inline int32_t get_IsDone_6() const { return ___IsDone_6; }
	inline int32_t* get_address_of_IsDone_6() { return &___IsDone_6; }
	inline void set_IsDone_6(int32_t value)
	{
		___IsDone_6 = value;
	}

	inline static int32_t get_offset_of_IsDo_7() { return static_cast<int32_t>(offsetof(MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9, ___IsDo_7)); }
	inline bool get_IsDo_7() const { return ___IsDo_7; }
	inline bool* get_address_of_IsDo_7() { return &___IsDo_7; }
	inline void set_IsDo_7(bool value)
	{
		___IsDo_7 = value;
	}

	inline static int32_t get_offset_of_mMissionNameText_8() { return static_cast<int32_t>(offsetof(MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9, ___mMissionNameText_8)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mMissionNameText_8() const { return ___mMissionNameText_8; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mMissionNameText_8() { return &___mMissionNameText_8; }
	inline void set_mMissionNameText_8(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mMissionNameText_8 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionNameText_8), value);
	}

	inline static int32_t get_offset_of_mStar_9() { return static_cast<int32_t>(offsetof(MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9, ___mStar_9)); }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* get_mStar_9() const { return ___mStar_9; }
	inline ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D** get_address_of_mStar_9() { return &___mStar_9; }
	inline void set_mStar_9(ImageU5BU5D_t3FC2D3F5D777CA546CA2314E6F5DC78FE8E3A37D* value)
	{
		___mStar_9 = value;
		Il2CppCodeGenWriteBarrier((&___mStar_9), value);
	}

	inline static int32_t get_offset_of_mStarImage_10() { return static_cast<int32_t>(offsetof(MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9, ___mStarImage_10)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_mStarImage_10() const { return ___mStarImage_10; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_mStarImage_10() { return &___mStarImage_10; }
	inline void set_mStarImage_10(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___mStarImage_10 = value;
		Il2CppCodeGenWriteBarrier((&___mStarImage_10), value);
	}

	inline static int32_t get_offset_of_mStarGrayImage_11() { return static_cast<int32_t>(offsetof(MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9, ___mStarGrayImage_11)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_mStarGrayImage_11() const { return ___mStarGrayImage_11; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_mStarGrayImage_11() { return &___mStarGrayImage_11; }
	inline void set_mStarGrayImage_11(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___mStarGrayImage_11 = value;
		Il2CppCodeGenWriteBarrier((&___mStarGrayImage_11), value);
	}

	inline static int32_t get_offset_of_mIsCompleteImage_12() { return static_cast<int32_t>(offsetof(MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9, ___mIsCompleteImage_12)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_mIsCompleteImage_12() const { return ___mIsCompleteImage_12; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_mIsCompleteImage_12() { return &___mIsCompleteImage_12; }
	inline void set_mIsCompleteImage_12(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___mIsCompleteImage_12 = value;
		Il2CppCodeGenWriteBarrier((&___mIsCompleteImage_12), value);
	}

	inline static int32_t get_offset_of_mCompleteImage_13() { return static_cast<int32_t>(offsetof(MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9, ___mCompleteImage_13)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_mCompleteImage_13() const { return ___mCompleteImage_13; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_mCompleteImage_13() { return &___mCompleteImage_13; }
	inline void set_mCompleteImage_13(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___mCompleteImage_13 = value;
		Il2CppCodeGenWriteBarrier((&___mCompleteImage_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSIONBUTTONDATA_T7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9_H
#ifndef MISSIONMANAGER_TA3C9C2F5BA139B63D123DCCEAA628F44F224C394_H
#define MISSIONMANAGER_TA3C9C2F5BA139B63D123DCCEAA628F44F224C394_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionManager
struct  MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MissionManager_IMissionMode MissionManager::mCurrentMissionMode
	int32_t ___mCurrentMissionMode_4;
	// UnityEngine.GameObject MissionManager::mMissionCanvasPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mMissionCanvasPanel_5;
	// UnityEngine.UI.Text MissionManager::mPassageAnswerText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mPassageAnswerText_6;
	// UnityEngine.GameObject MissionManager::mHintCardFieldPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mHintCardFieldPanel_7;
	// UnityEngine.GameObject MissionManager::mHintCardPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mHintCardPanel_8;
	// UnityEngine.GameObject MissionManager::mAnsCardObj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mAnsCardObj_9;
	// UnityEngine.GameObject MissionManager::mAnsFieldObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mAnsFieldObject_10;
	// UnityEngine.GameObject[] MissionManager::mMissionPanel
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___mMissionPanel_11;
	// UnityEngine.GameObject[] MissionManager::mMissionButtonPanel
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___mMissionButtonPanel_12;
	// UnityEngine.GameObject MissionManager::mSelectPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mSelectPanel_13;
	// UnityEngine.GameObject MissionManager::mBeaconPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mBeaconPanel_14;
	// UnityEngine.GameObject MissionManager::BeaconCollectedUIPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___BeaconCollectedUIPanel_15;
	// UnityEngine.GameObject MissionManager::mBeaconCamera
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mBeaconCamera_16;
	// CompleteImageSetter MissionManager::mCompletePanel
	CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82 * ___mCompletePanel_17;
	// UnityEngine.Sprite[] MissionManager::memoPic
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___memoPic_18;
	// UnityEngine.GameObject MissionManager::mUnCompletePanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mUnCompletePanel_19;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> MissionManager::mMissionPanelDic
	Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * ___mMissionPanelDic_20;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> MissionManager::mPanelButtonDic
	Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * ___mPanelButtonDic_21;
	// BTracerBeacon MissionManager::beaconDic
	BTracerBeacon_tC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD * ___beaconDic_22;
	// System.Collections.Generic.List`1<System.String> MissionManager::beaconUUIDList
	List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 * ___beaconUUIDList_23;
	// System.Collections.Generic.List`1<System.String> MissionManager::beaconContents
	List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 * ___beaconContents_24;
	// System.String[] MissionManager::TemporerLetter
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___TemporerLetter_26;
	// MissionData MissionManager::CurrentMissionData
	MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C * ___CurrentMissionData_27;
	// System.String[] MissionManager::Wordings
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___Wordings_28;
	// UnityEngine.UI.Text MissionManager::mPassageCardAnswerText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mPassageCardAnswerText_29;
	// UnityEngine.UI.Text MissionManager::mHintCardAnswerText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mHintCardAnswerText_30;
	// System.String[] MissionManager::mCurrentWording
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___mCurrentWording_31;
	// System.String[] MissionManager::mAnsOrder
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___mAnsOrder_32;
	// System.String[] MissionManager::mAnsField
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___mAnsField_33;
	// System.Int32 MissionManager::mSpentTime
	int32_t ___mSpentTime_34;
	// System.DateTime MissionManager::mStartTime
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___mStartTime_35;
	// System.Int32 MissionManager::mChecker
	int32_t ___mChecker_36;
	// System.Int32 MissionManager::mMostStarBefore
	int32_t ___mMostStarBefore_37;
	// System.Boolean MissionManager::IsUploading
	bool ___IsUploading_38;
	// System.Int32[] MissionManager::mCardFileID
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___mCardFileID_39;
	// System.Int32[] MissionManager::time
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___time_40;
	// System.Int32[] MissionManager::timeReward
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___timeReward_41;
	// System.Int32 MissionManager::mTime3RewardOver
	int32_t ___mTime3RewardOver_42;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> MissionManager::gestureDetector
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___gestureDetector_43;
	// System.Int32 MissionManager::gestureCount
	int32_t ___gestureCount_44;
	// System.Single MissionManager::gestureLength
	float ___gestureLength_45;
	// UnityEngine.Vector2 MissionManager::gestureSum
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___gestureSum_46;
	// System.Int32 MissionManager::numOfCircleToShow
	int32_t ___numOfCircleToShow_47;
	// System.Boolean MissionManager::saveletterDone
	bool ___saveletterDone_48;
	// System.Boolean MissionManager::SaveAnswerDone
	bool ___SaveAnswerDone_49;
	// UnityEngine.AudioClip MissionManager::CompleteSound
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___CompleteSound_50;
	// UnityEngine.AudioSource MissionManager::CompleteMissionSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___CompleteMissionSource_51;
	// System.String MissionManager::mAnsOrderFilled
	String_t* ___mAnsOrderFilled_52;

public:
	inline static int32_t get_offset_of_mCurrentMissionMode_4() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mCurrentMissionMode_4)); }
	inline int32_t get_mCurrentMissionMode_4() const { return ___mCurrentMissionMode_4; }
	inline int32_t* get_address_of_mCurrentMissionMode_4() { return &___mCurrentMissionMode_4; }
	inline void set_mCurrentMissionMode_4(int32_t value)
	{
		___mCurrentMissionMode_4 = value;
	}

	inline static int32_t get_offset_of_mMissionCanvasPanel_5() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mMissionCanvasPanel_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mMissionCanvasPanel_5() const { return ___mMissionCanvasPanel_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mMissionCanvasPanel_5() { return &___mMissionCanvasPanel_5; }
	inline void set_mMissionCanvasPanel_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mMissionCanvasPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionCanvasPanel_5), value);
	}

	inline static int32_t get_offset_of_mPassageAnswerText_6() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mPassageAnswerText_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mPassageAnswerText_6() const { return ___mPassageAnswerText_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mPassageAnswerText_6() { return &___mPassageAnswerText_6; }
	inline void set_mPassageAnswerText_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mPassageAnswerText_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPassageAnswerText_6), value);
	}

	inline static int32_t get_offset_of_mHintCardFieldPanel_7() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mHintCardFieldPanel_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mHintCardFieldPanel_7() const { return ___mHintCardFieldPanel_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mHintCardFieldPanel_7() { return &___mHintCardFieldPanel_7; }
	inline void set_mHintCardFieldPanel_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mHintCardFieldPanel_7 = value;
		Il2CppCodeGenWriteBarrier((&___mHintCardFieldPanel_7), value);
	}

	inline static int32_t get_offset_of_mHintCardPanel_8() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mHintCardPanel_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mHintCardPanel_8() const { return ___mHintCardPanel_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mHintCardPanel_8() { return &___mHintCardPanel_8; }
	inline void set_mHintCardPanel_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mHintCardPanel_8 = value;
		Il2CppCodeGenWriteBarrier((&___mHintCardPanel_8), value);
	}

	inline static int32_t get_offset_of_mAnsCardObj_9() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mAnsCardObj_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mAnsCardObj_9() const { return ___mAnsCardObj_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mAnsCardObj_9() { return &___mAnsCardObj_9; }
	inline void set_mAnsCardObj_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mAnsCardObj_9 = value;
		Il2CppCodeGenWriteBarrier((&___mAnsCardObj_9), value);
	}

	inline static int32_t get_offset_of_mAnsFieldObject_10() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mAnsFieldObject_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mAnsFieldObject_10() const { return ___mAnsFieldObject_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mAnsFieldObject_10() { return &___mAnsFieldObject_10; }
	inline void set_mAnsFieldObject_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mAnsFieldObject_10 = value;
		Il2CppCodeGenWriteBarrier((&___mAnsFieldObject_10), value);
	}

	inline static int32_t get_offset_of_mMissionPanel_11() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mMissionPanel_11)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_mMissionPanel_11() const { return ___mMissionPanel_11; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_mMissionPanel_11() { return &___mMissionPanel_11; }
	inline void set_mMissionPanel_11(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___mMissionPanel_11 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionPanel_11), value);
	}

	inline static int32_t get_offset_of_mMissionButtonPanel_12() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mMissionButtonPanel_12)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_mMissionButtonPanel_12() const { return ___mMissionButtonPanel_12; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_mMissionButtonPanel_12() { return &___mMissionButtonPanel_12; }
	inline void set_mMissionButtonPanel_12(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___mMissionButtonPanel_12 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionButtonPanel_12), value);
	}

	inline static int32_t get_offset_of_mSelectPanel_13() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mSelectPanel_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mSelectPanel_13() const { return ___mSelectPanel_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mSelectPanel_13() { return &___mSelectPanel_13; }
	inline void set_mSelectPanel_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mSelectPanel_13 = value;
		Il2CppCodeGenWriteBarrier((&___mSelectPanel_13), value);
	}

	inline static int32_t get_offset_of_mBeaconPanel_14() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mBeaconPanel_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mBeaconPanel_14() const { return ___mBeaconPanel_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mBeaconPanel_14() { return &___mBeaconPanel_14; }
	inline void set_mBeaconPanel_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mBeaconPanel_14 = value;
		Il2CppCodeGenWriteBarrier((&___mBeaconPanel_14), value);
	}

	inline static int32_t get_offset_of_BeaconCollectedUIPanel_15() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___BeaconCollectedUIPanel_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_BeaconCollectedUIPanel_15() const { return ___BeaconCollectedUIPanel_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_BeaconCollectedUIPanel_15() { return &___BeaconCollectedUIPanel_15; }
	inline void set_BeaconCollectedUIPanel_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___BeaconCollectedUIPanel_15 = value;
		Il2CppCodeGenWriteBarrier((&___BeaconCollectedUIPanel_15), value);
	}

	inline static int32_t get_offset_of_mBeaconCamera_16() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mBeaconCamera_16)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mBeaconCamera_16() const { return ___mBeaconCamera_16; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mBeaconCamera_16() { return &___mBeaconCamera_16; }
	inline void set_mBeaconCamera_16(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mBeaconCamera_16 = value;
		Il2CppCodeGenWriteBarrier((&___mBeaconCamera_16), value);
	}

	inline static int32_t get_offset_of_mCompletePanel_17() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mCompletePanel_17)); }
	inline CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82 * get_mCompletePanel_17() const { return ___mCompletePanel_17; }
	inline CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82 ** get_address_of_mCompletePanel_17() { return &___mCompletePanel_17; }
	inline void set_mCompletePanel_17(CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82 * value)
	{
		___mCompletePanel_17 = value;
		Il2CppCodeGenWriteBarrier((&___mCompletePanel_17), value);
	}

	inline static int32_t get_offset_of_memoPic_18() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___memoPic_18)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_memoPic_18() const { return ___memoPic_18; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_memoPic_18() { return &___memoPic_18; }
	inline void set_memoPic_18(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___memoPic_18 = value;
		Il2CppCodeGenWriteBarrier((&___memoPic_18), value);
	}

	inline static int32_t get_offset_of_mUnCompletePanel_19() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mUnCompletePanel_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mUnCompletePanel_19() const { return ___mUnCompletePanel_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mUnCompletePanel_19() { return &___mUnCompletePanel_19; }
	inline void set_mUnCompletePanel_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mUnCompletePanel_19 = value;
		Il2CppCodeGenWriteBarrier((&___mUnCompletePanel_19), value);
	}

	inline static int32_t get_offset_of_mMissionPanelDic_20() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mMissionPanelDic_20)); }
	inline Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * get_mMissionPanelDic_20() const { return ___mMissionPanelDic_20; }
	inline Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 ** get_address_of_mMissionPanelDic_20() { return &___mMissionPanelDic_20; }
	inline void set_mMissionPanelDic_20(Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * value)
	{
		___mMissionPanelDic_20 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionPanelDic_20), value);
	}

	inline static int32_t get_offset_of_mPanelButtonDic_21() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mPanelButtonDic_21)); }
	inline Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * get_mPanelButtonDic_21() const { return ___mPanelButtonDic_21; }
	inline Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 ** get_address_of_mPanelButtonDic_21() { return &___mPanelButtonDic_21; }
	inline void set_mPanelButtonDic_21(Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * value)
	{
		___mPanelButtonDic_21 = value;
		Il2CppCodeGenWriteBarrier((&___mPanelButtonDic_21), value);
	}

	inline static int32_t get_offset_of_beaconDic_22() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___beaconDic_22)); }
	inline BTracerBeacon_tC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD * get_beaconDic_22() const { return ___beaconDic_22; }
	inline BTracerBeacon_tC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD ** get_address_of_beaconDic_22() { return &___beaconDic_22; }
	inline void set_beaconDic_22(BTracerBeacon_tC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD * value)
	{
		___beaconDic_22 = value;
		Il2CppCodeGenWriteBarrier((&___beaconDic_22), value);
	}

	inline static int32_t get_offset_of_beaconUUIDList_23() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___beaconUUIDList_23)); }
	inline List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 * get_beaconUUIDList_23() const { return ___beaconUUIDList_23; }
	inline List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 ** get_address_of_beaconUUIDList_23() { return &___beaconUUIDList_23; }
	inline void set_beaconUUIDList_23(List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 * value)
	{
		___beaconUUIDList_23 = value;
		Il2CppCodeGenWriteBarrier((&___beaconUUIDList_23), value);
	}

	inline static int32_t get_offset_of_beaconContents_24() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___beaconContents_24)); }
	inline List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 * get_beaconContents_24() const { return ___beaconContents_24; }
	inline List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 ** get_address_of_beaconContents_24() { return &___beaconContents_24; }
	inline void set_beaconContents_24(List_1_t7600B3EA144F32C2120D420AB63C9E014D113CF1 * value)
	{
		___beaconContents_24 = value;
		Il2CppCodeGenWriteBarrier((&___beaconContents_24), value);
	}

	inline static int32_t get_offset_of_TemporerLetter_26() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___TemporerLetter_26)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_TemporerLetter_26() const { return ___TemporerLetter_26; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_TemporerLetter_26() { return &___TemporerLetter_26; }
	inline void set_TemporerLetter_26(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___TemporerLetter_26 = value;
		Il2CppCodeGenWriteBarrier((&___TemporerLetter_26), value);
	}

	inline static int32_t get_offset_of_CurrentMissionData_27() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___CurrentMissionData_27)); }
	inline MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C * get_CurrentMissionData_27() const { return ___CurrentMissionData_27; }
	inline MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C ** get_address_of_CurrentMissionData_27() { return &___CurrentMissionData_27; }
	inline void set_CurrentMissionData_27(MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C * value)
	{
		___CurrentMissionData_27 = value;
		Il2CppCodeGenWriteBarrier((&___CurrentMissionData_27), value);
	}

	inline static int32_t get_offset_of_Wordings_28() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___Wordings_28)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_Wordings_28() const { return ___Wordings_28; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_Wordings_28() { return &___Wordings_28; }
	inline void set_Wordings_28(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___Wordings_28 = value;
		Il2CppCodeGenWriteBarrier((&___Wordings_28), value);
	}

	inline static int32_t get_offset_of_mPassageCardAnswerText_29() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mPassageCardAnswerText_29)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mPassageCardAnswerText_29() const { return ___mPassageCardAnswerText_29; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mPassageCardAnswerText_29() { return &___mPassageCardAnswerText_29; }
	inline void set_mPassageCardAnswerText_29(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mPassageCardAnswerText_29 = value;
		Il2CppCodeGenWriteBarrier((&___mPassageCardAnswerText_29), value);
	}

	inline static int32_t get_offset_of_mHintCardAnswerText_30() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mHintCardAnswerText_30)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mHintCardAnswerText_30() const { return ___mHintCardAnswerText_30; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mHintCardAnswerText_30() { return &___mHintCardAnswerText_30; }
	inline void set_mHintCardAnswerText_30(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mHintCardAnswerText_30 = value;
		Il2CppCodeGenWriteBarrier((&___mHintCardAnswerText_30), value);
	}

	inline static int32_t get_offset_of_mCurrentWording_31() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mCurrentWording_31)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_mCurrentWording_31() const { return ___mCurrentWording_31; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_mCurrentWording_31() { return &___mCurrentWording_31; }
	inline void set_mCurrentWording_31(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___mCurrentWording_31 = value;
		Il2CppCodeGenWriteBarrier((&___mCurrentWording_31), value);
	}

	inline static int32_t get_offset_of_mAnsOrder_32() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mAnsOrder_32)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_mAnsOrder_32() const { return ___mAnsOrder_32; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_mAnsOrder_32() { return &___mAnsOrder_32; }
	inline void set_mAnsOrder_32(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___mAnsOrder_32 = value;
		Il2CppCodeGenWriteBarrier((&___mAnsOrder_32), value);
	}

	inline static int32_t get_offset_of_mAnsField_33() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mAnsField_33)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_mAnsField_33() const { return ___mAnsField_33; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_mAnsField_33() { return &___mAnsField_33; }
	inline void set_mAnsField_33(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___mAnsField_33 = value;
		Il2CppCodeGenWriteBarrier((&___mAnsField_33), value);
	}

	inline static int32_t get_offset_of_mSpentTime_34() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mSpentTime_34)); }
	inline int32_t get_mSpentTime_34() const { return ___mSpentTime_34; }
	inline int32_t* get_address_of_mSpentTime_34() { return &___mSpentTime_34; }
	inline void set_mSpentTime_34(int32_t value)
	{
		___mSpentTime_34 = value;
	}

	inline static int32_t get_offset_of_mStartTime_35() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mStartTime_35)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_mStartTime_35() const { return ___mStartTime_35; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_mStartTime_35() { return &___mStartTime_35; }
	inline void set_mStartTime_35(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___mStartTime_35 = value;
	}

	inline static int32_t get_offset_of_mChecker_36() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mChecker_36)); }
	inline int32_t get_mChecker_36() const { return ___mChecker_36; }
	inline int32_t* get_address_of_mChecker_36() { return &___mChecker_36; }
	inline void set_mChecker_36(int32_t value)
	{
		___mChecker_36 = value;
	}

	inline static int32_t get_offset_of_mMostStarBefore_37() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mMostStarBefore_37)); }
	inline int32_t get_mMostStarBefore_37() const { return ___mMostStarBefore_37; }
	inline int32_t* get_address_of_mMostStarBefore_37() { return &___mMostStarBefore_37; }
	inline void set_mMostStarBefore_37(int32_t value)
	{
		___mMostStarBefore_37 = value;
	}

	inline static int32_t get_offset_of_IsUploading_38() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___IsUploading_38)); }
	inline bool get_IsUploading_38() const { return ___IsUploading_38; }
	inline bool* get_address_of_IsUploading_38() { return &___IsUploading_38; }
	inline void set_IsUploading_38(bool value)
	{
		___IsUploading_38 = value;
	}

	inline static int32_t get_offset_of_mCardFileID_39() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mCardFileID_39)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_mCardFileID_39() const { return ___mCardFileID_39; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_mCardFileID_39() { return &___mCardFileID_39; }
	inline void set_mCardFileID_39(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___mCardFileID_39 = value;
		Il2CppCodeGenWriteBarrier((&___mCardFileID_39), value);
	}

	inline static int32_t get_offset_of_time_40() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___time_40)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_time_40() const { return ___time_40; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_time_40() { return &___time_40; }
	inline void set_time_40(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___time_40 = value;
		Il2CppCodeGenWriteBarrier((&___time_40), value);
	}

	inline static int32_t get_offset_of_timeReward_41() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___timeReward_41)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_timeReward_41() const { return ___timeReward_41; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_timeReward_41() { return &___timeReward_41; }
	inline void set_timeReward_41(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___timeReward_41 = value;
		Il2CppCodeGenWriteBarrier((&___timeReward_41), value);
	}

	inline static int32_t get_offset_of_mTime3RewardOver_42() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mTime3RewardOver_42)); }
	inline int32_t get_mTime3RewardOver_42() const { return ___mTime3RewardOver_42; }
	inline int32_t* get_address_of_mTime3RewardOver_42() { return &___mTime3RewardOver_42; }
	inline void set_mTime3RewardOver_42(int32_t value)
	{
		___mTime3RewardOver_42 = value;
	}

	inline static int32_t get_offset_of_gestureDetector_43() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___gestureDetector_43)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_gestureDetector_43() const { return ___gestureDetector_43; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_gestureDetector_43() { return &___gestureDetector_43; }
	inline void set_gestureDetector_43(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___gestureDetector_43 = value;
		Il2CppCodeGenWriteBarrier((&___gestureDetector_43), value);
	}

	inline static int32_t get_offset_of_gestureCount_44() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___gestureCount_44)); }
	inline int32_t get_gestureCount_44() const { return ___gestureCount_44; }
	inline int32_t* get_address_of_gestureCount_44() { return &___gestureCount_44; }
	inline void set_gestureCount_44(int32_t value)
	{
		___gestureCount_44 = value;
	}

	inline static int32_t get_offset_of_gestureLength_45() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___gestureLength_45)); }
	inline float get_gestureLength_45() const { return ___gestureLength_45; }
	inline float* get_address_of_gestureLength_45() { return &___gestureLength_45; }
	inline void set_gestureLength_45(float value)
	{
		___gestureLength_45 = value;
	}

	inline static int32_t get_offset_of_gestureSum_46() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___gestureSum_46)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_gestureSum_46() const { return ___gestureSum_46; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_gestureSum_46() { return &___gestureSum_46; }
	inline void set_gestureSum_46(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___gestureSum_46 = value;
	}

	inline static int32_t get_offset_of_numOfCircleToShow_47() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___numOfCircleToShow_47)); }
	inline int32_t get_numOfCircleToShow_47() const { return ___numOfCircleToShow_47; }
	inline int32_t* get_address_of_numOfCircleToShow_47() { return &___numOfCircleToShow_47; }
	inline void set_numOfCircleToShow_47(int32_t value)
	{
		___numOfCircleToShow_47 = value;
	}

	inline static int32_t get_offset_of_saveletterDone_48() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___saveletterDone_48)); }
	inline bool get_saveletterDone_48() const { return ___saveletterDone_48; }
	inline bool* get_address_of_saveletterDone_48() { return &___saveletterDone_48; }
	inline void set_saveletterDone_48(bool value)
	{
		___saveletterDone_48 = value;
	}

	inline static int32_t get_offset_of_SaveAnswerDone_49() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___SaveAnswerDone_49)); }
	inline bool get_SaveAnswerDone_49() const { return ___SaveAnswerDone_49; }
	inline bool* get_address_of_SaveAnswerDone_49() { return &___SaveAnswerDone_49; }
	inline void set_SaveAnswerDone_49(bool value)
	{
		___SaveAnswerDone_49 = value;
	}

	inline static int32_t get_offset_of_CompleteSound_50() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___CompleteSound_50)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_CompleteSound_50() const { return ___CompleteSound_50; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_CompleteSound_50() { return &___CompleteSound_50; }
	inline void set_CompleteSound_50(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___CompleteSound_50 = value;
		Il2CppCodeGenWriteBarrier((&___CompleteSound_50), value);
	}

	inline static int32_t get_offset_of_CompleteMissionSource_51() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___CompleteMissionSource_51)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_CompleteMissionSource_51() const { return ___CompleteMissionSource_51; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_CompleteMissionSource_51() { return &___CompleteMissionSource_51; }
	inline void set_CompleteMissionSource_51(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___CompleteMissionSource_51 = value;
		Il2CppCodeGenWriteBarrier((&___CompleteMissionSource_51), value);
	}

	inline static int32_t get_offset_of_mAnsOrderFilled_52() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394, ___mAnsOrderFilled_52)); }
	inline String_t* get_mAnsOrderFilled_52() const { return ___mAnsOrderFilled_52; }
	inline String_t** get_address_of_mAnsOrderFilled_52() { return &___mAnsOrderFilled_52; }
	inline void set_mAnsOrderFilled_52(String_t* value)
	{
		___mAnsOrderFilled_52 = value;
		Il2CppCodeGenWriteBarrier((&___mAnsOrderFilled_52), value);
	}
};

struct MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394_StaticFields
{
public:
	// MissionManager MissionManager::mInstance
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * ___mInstance_25;

public:
	inline static int32_t get_offset_of_mInstance_25() { return static_cast<int32_t>(offsetof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394_StaticFields, ___mInstance_25)); }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * get_mInstance_25() const { return ___mInstance_25; }
	inline MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 ** get_address_of_mInstance_25() { return &___mInstance_25; }
	inline void set_mInstance_25(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394 * value)
	{
		___mInstance_25 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSIONMANAGER_TA3C9C2F5BA139B63D123DCCEAA628F44F224C394_H
#ifndef MISSIONRECORDBUTTONDATA_T1A8F175F8C67F136FAA77DB0268B27C8158E974A_H
#define MISSIONRECORDBUTTONDATA_T1A8F175F8C67F136FAA77DB0268B27C8158E974A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionRecordButtonData
struct  MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 MissionRecordButtonData::mScore
	int32_t ___mScore_4;
	// UnityEngine.UI.Text MissionRecordButtonData::mMissionName
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mMissionName_5;
	// UnityEngine.UI.Text MissionRecordButtonData::mSpentTime
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mSpentTime_6;
	// UnityEngine.Sprite MissionRecordButtonData::mStarImage
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___mStarImage_7;

public:
	inline static int32_t get_offset_of_mScore_4() { return static_cast<int32_t>(offsetof(MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A, ___mScore_4)); }
	inline int32_t get_mScore_4() const { return ___mScore_4; }
	inline int32_t* get_address_of_mScore_4() { return &___mScore_4; }
	inline void set_mScore_4(int32_t value)
	{
		___mScore_4 = value;
	}

	inline static int32_t get_offset_of_mMissionName_5() { return static_cast<int32_t>(offsetof(MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A, ___mMissionName_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mMissionName_5() const { return ___mMissionName_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mMissionName_5() { return &___mMissionName_5; }
	inline void set_mMissionName_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mMissionName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionName_5), value);
	}

	inline static int32_t get_offset_of_mSpentTime_6() { return static_cast<int32_t>(offsetof(MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A, ___mSpentTime_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mSpentTime_6() const { return ___mSpentTime_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mSpentTime_6() { return &___mSpentTime_6; }
	inline void set_mSpentTime_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mSpentTime_6 = value;
		Il2CppCodeGenWriteBarrier((&___mSpentTime_6), value);
	}

	inline static int32_t get_offset_of_mStarImage_7() { return static_cast<int32_t>(offsetof(MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A, ___mStarImage_7)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_mStarImage_7() const { return ___mStarImage_7; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_mStarImage_7() { return &___mStarImage_7; }
	inline void set_mStarImage_7(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___mStarImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___mStarImage_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSIONRECORDBUTTONDATA_T1A8F175F8C67F136FAA77DB0268B27C8158E974A_H
#ifndef MISSIONUI_T82A18D2C7CBB423DC3A123916F334231B7F4B579_H
#define MISSIONUI_T82A18D2C7CBB423DC3A123916F334231B7F4B579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MissionUI
struct  MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject MissionUI::mHomePanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mHomePanel_5;
	// UnityEngine.GameObject MissionUI::mUnCompletePanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mUnCompletePanel_6;
	// Example MissionUI::example
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF * ___example_7;
	// UnityEngine.Sprite[] MissionUI::InstructionImage
	SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* ___InstructionImage_8;
	// UnityEngine.UI.Image MissionUI::currentInstuctImg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___currentInstuctImg_9;
	// UnityEngine.GameObject MissionUI::MainCam
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___MainCam_10;

public:
	inline static int32_t get_offset_of_mHomePanel_5() { return static_cast<int32_t>(offsetof(MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579, ___mHomePanel_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mHomePanel_5() const { return ___mHomePanel_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mHomePanel_5() { return &___mHomePanel_5; }
	inline void set_mHomePanel_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mHomePanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___mHomePanel_5), value);
	}

	inline static int32_t get_offset_of_mUnCompletePanel_6() { return static_cast<int32_t>(offsetof(MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579, ___mUnCompletePanel_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mUnCompletePanel_6() const { return ___mUnCompletePanel_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mUnCompletePanel_6() { return &___mUnCompletePanel_6; }
	inline void set_mUnCompletePanel_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mUnCompletePanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___mUnCompletePanel_6), value);
	}

	inline static int32_t get_offset_of_example_7() { return static_cast<int32_t>(offsetof(MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579, ___example_7)); }
	inline Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF * get_example_7() const { return ___example_7; }
	inline Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF ** get_address_of_example_7() { return &___example_7; }
	inline void set_example_7(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF * value)
	{
		___example_7 = value;
		Il2CppCodeGenWriteBarrier((&___example_7), value);
	}

	inline static int32_t get_offset_of_InstructionImage_8() { return static_cast<int32_t>(offsetof(MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579, ___InstructionImage_8)); }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* get_InstructionImage_8() const { return ___InstructionImage_8; }
	inline SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017** get_address_of_InstructionImage_8() { return &___InstructionImage_8; }
	inline void set_InstructionImage_8(SpriteU5BU5D_tC248C30BCF0EAA6D8BD69A89AF1FC5BA002CA017* value)
	{
		___InstructionImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___InstructionImage_8), value);
	}

	inline static int32_t get_offset_of_currentInstuctImg_9() { return static_cast<int32_t>(offsetof(MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579, ___currentInstuctImg_9)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_currentInstuctImg_9() const { return ___currentInstuctImg_9; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_currentInstuctImg_9() { return &___currentInstuctImg_9; }
	inline void set_currentInstuctImg_9(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___currentInstuctImg_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstuctImg_9), value);
	}

	inline static int32_t get_offset_of_MainCam_10() { return static_cast<int32_t>(offsetof(MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579, ___MainCam_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_MainCam_10() const { return ___MainCam_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_MainCam_10() { return &___MainCam_10; }
	inline void set_MainCam_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___MainCam_10 = value;
		Il2CppCodeGenWriteBarrier((&___MainCam_10), value);
	}
};

struct MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579_StaticFields
{
public:
	// MissionUI MissionUI::Instance
	MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579_StaticFields, ___Instance_4)); }
	inline MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579 * get_Instance_4() const { return ___Instance_4; }
	inline MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSIONUI_T82A18D2C7CBB423DC3A123916F334231B7F4B579_H
#ifndef OBJECTPOOL_T6972C6DD97FC383C5A6CFB727EAE1C2389375A34_H
#define OBJECTPOOL_T6972C6DD97FC383C5A6CFB727EAE1C2389375A34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectPool
struct  ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ObjectPool::mObjectPool
	List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * ___mObjectPool_5;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> ObjectPool::mObjectDic
	Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * ___mObjectDic_6;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> ObjectPool::mObjectInGame
	Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * ___mObjectInGame_7;
	// ObjectPool_eSystemType ObjectPool::mSystemType
	int32_t ___mSystemType_8;
	// ObjectPool_ObjectStore ObjectPool::mFactoryStore
	ObjectStore_tB7EE0D6CBA7CBBBA830724C5792B309AF079DEF4 * ___mFactoryStore_9;

public:
	inline static int32_t get_offset_of_mObjectPool_5() { return static_cast<int32_t>(offsetof(ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34, ___mObjectPool_5)); }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * get_mObjectPool_5() const { return ___mObjectPool_5; }
	inline List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 ** get_address_of_mObjectPool_5() { return &___mObjectPool_5; }
	inline void set_mObjectPool_5(List_1_tBA8D772D87B6502B2A4D0EFE166C846285F50650 * value)
	{
		___mObjectPool_5 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectPool_5), value);
	}

	inline static int32_t get_offset_of_mObjectDic_6() { return static_cast<int32_t>(offsetof(ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34, ___mObjectDic_6)); }
	inline Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * get_mObjectDic_6() const { return ___mObjectDic_6; }
	inline Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 ** get_address_of_mObjectDic_6() { return &___mObjectDic_6; }
	inline void set_mObjectDic_6(Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * value)
	{
		___mObjectDic_6 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectDic_6), value);
	}

	inline static int32_t get_offset_of_mObjectInGame_7() { return static_cast<int32_t>(offsetof(ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34, ___mObjectInGame_7)); }
	inline Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * get_mObjectInGame_7() const { return ___mObjectInGame_7; }
	inline Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 ** get_address_of_mObjectInGame_7() { return &___mObjectInGame_7; }
	inline void set_mObjectInGame_7(Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * value)
	{
		___mObjectInGame_7 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectInGame_7), value);
	}

	inline static int32_t get_offset_of_mSystemType_8() { return static_cast<int32_t>(offsetof(ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34, ___mSystemType_8)); }
	inline int32_t get_mSystemType_8() const { return ___mSystemType_8; }
	inline int32_t* get_address_of_mSystemType_8() { return &___mSystemType_8; }
	inline void set_mSystemType_8(int32_t value)
	{
		___mSystemType_8 = value;
	}

	inline static int32_t get_offset_of_mFactoryStore_9() { return static_cast<int32_t>(offsetof(ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34, ___mFactoryStore_9)); }
	inline ObjectStore_tB7EE0D6CBA7CBBBA830724C5792B309AF079DEF4 * get_mFactoryStore_9() const { return ___mFactoryStore_9; }
	inline ObjectStore_tB7EE0D6CBA7CBBBA830724C5792B309AF079DEF4 ** get_address_of_mFactoryStore_9() { return &___mFactoryStore_9; }
	inline void set_mFactoryStore_9(ObjectStore_tB7EE0D6CBA7CBBBA830724C5792B309AF079DEF4 * value)
	{
		___mFactoryStore_9 = value;
		Il2CppCodeGenWriteBarrier((&___mFactoryStore_9), value);
	}
};

struct ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34_StaticFields
{
public:
	// ObjectPool ObjectPool::mInsatnce
	ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34 * ___mInsatnce_4;

public:
	inline static int32_t get_offset_of_mInsatnce_4() { return static_cast<int32_t>(offsetof(ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34_StaticFields, ___mInsatnce_4)); }
	inline ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34 * get_mInsatnce_4() const { return ___mInsatnce_4; }
	inline ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34 ** get_address_of_mInsatnce_4() { return &___mInsatnce_4; }
	inline void set_mInsatnce_4(ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34 * value)
	{
		___mInsatnce_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInsatnce_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTPOOL_T6972C6DD97FC383C5A6CFB727EAE1C2389375A34_H
#ifndef PLAYERMODE_TC98090D1841F0D9D319F822FF716CA599235AC97_H
#define PLAYERMODE_TC98090D1841F0D9D319F822FF716CA599235AC97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayerMode
struct  PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// SimpleJSON.JSONNode PlayerMode::mCardResult
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___mCardResult_5;
	// System.Int32 PlayerMode::mCardUrlCount
	int32_t ___mCardUrlCount_6;
	// System.Int32 PlayerMode::mCardNameCount
	int32_t ___mCardNameCount_7;
	// System.Int32 PlayerMode::mCardDownloadCount
	int32_t ___mCardDownloadCount_8;
	// System.Boolean PlayerMode::mCollectionComplete
	bool ___mCollectionComplete_9;
	// System.Boolean PlayerMode::mRankingComplete
	bool ___mRankingComplete_10;
	// System.Boolean PlayerMode::mLoadPlayerDataComplete
	bool ___mLoadPlayerDataComplete_11;
	// SimpleJSON.JSONNode PlayerMode::mRankResult
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___mRankResult_12;
	// System.Int32 PlayerMode::mRankCount
	int32_t ___mRankCount_13;
	// System.Int32 PlayerMode::mBestTracerRankCount
	int32_t ___mBestTracerRankCount_14;
	// UnityEngine.GameObject PlayerMode::GameCanvasPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___GameCanvasPanel_15;
	// PlayerMode_IPlayerMode PlayerMode::mPlayerMode
	int32_t ___mPlayerMode_16;
	// UnityEngine.GameObject[] PlayerMode::mPanel
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___mPanel_17;
	// UnityEngine.GameObject[] PlayerMode::mPanelButton
	GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* ___mPanelButton_18;
	// UnityEngine.GameObject PlayerMode::mMissionButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mMissionButton_19;
	// MissionButtonData PlayerMode::mMissionButtonData
	MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9 * ___mMissionButtonData_20;
	// UnityEngine.GameObject PlayerMode::mResultButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mResultButton_21;
	// MissionRecordButtonData PlayerMode::recordData
	MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A * ___recordData_22;
	// UnityEngine.RectTransform PlayerMode::mMissionContent
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___mMissionContent_23;
	// UnityEngine.RectTransform PlayerMode::mMissionRecordContent
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___mMissionRecordContent_24;
	// UnityEngine.GameObject PlayerMode::mMissionView
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mMissionView_25;
	// UnityEngine.GameObject PlayerMode::mMissionRecordView
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mMissionRecordView_26;
	// UnityEngine.GameObject PlayerMode::mMissionButtonOn
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mMissionButtonOn_27;
	// UnityEngine.GameObject PlayerMode::mMissionButtonOff
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mMissionButtonOff_28;
	// UnityEngine.GameObject PlayerMode::mMissionRecordButtonOn
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mMissionRecordButtonOn_29;
	// UnityEngine.GameObject PlayerMode::mMissionRecordButtonOff
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mMissionRecordButtonOff_30;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> PlayerMode::mPanelDic
	Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * ___mPanelDic_31;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> PlayerMode::mPanelButtonDic
	Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * ___mPanelButtonDic_32;
	// UnityEngine.RectTransform PlayerMode::mCollectionContent
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___mCollectionContent_33;
	// UnityEngine.UI.GridLayoutGroup PlayerMode::mRecordLayout
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * ___mRecordLayout_34;
	// UnityEngine.RectTransform PlayerMode::mBestTracerRankContent
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___mBestTracerRankContent_35;
	// UnityEngine.RectTransform PlayerMode::mWealthTracerRankContent
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___mWealthTracerRankContent_36;
	// LoadRank PlayerMode::mPlayerRank
	LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52 * ___mPlayerRank_37;
	// UnityEngine.GameObject PlayerMode::EnumImage
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___EnumImage_38;
	// UnityEngine.RectTransform PlayerMode::mBestTracerView
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___mBestTracerView_39;
	// UnityEngine.RectTransform PlayerMode::mWealthTracerView
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___mWealthTracerView_40;
	// UnityEngine.GameObject PlayerMode::mBestTracerButtonOn
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mBestTracerButtonOn_41;
	// UnityEngine.GameObject PlayerMode::mBestTracerButtonOff
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mBestTracerButtonOff_42;
	// UnityEngine.GameObject PlayerMode::mWealthTracerButtonOn
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mWealthTracerButtonOn_43;
	// UnityEngine.GameObject PlayerMode::mWealthTracerButtonOff
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mWealthTracerButtonOff_44;
	// UnityEngine.UI.Text PlayerMode::Money
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___Money_45;
	// UnityEngine.UI.Text PlayerMode::Name
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___Name_46;
	// UnityEngine.UI.GridLayoutGroup PlayerMode::CollectLayout
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * ___CollectLayout_47;
	// UnityEngine.UI.GridLayoutGroup PlayerMode::BestTracerLayoutGroup
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * ___BestTracerLayoutGroup_48;
	// UnityEngine.UI.GridLayoutGroup PlayerMode::WealthTracerLayoutGroup
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * ___WealthTracerLayoutGroup_49;
	// UnityEngine.UI.GridLayoutGroup PlayerMode::MissionLayoutGroup
	GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * ___MissionLayoutGroup_50;
	// UnityEngine.Vector2 PlayerMode::CollectionContentORect
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___CollectionContentORect_51;
	// System.Boolean PlayerMode::SetBestTracer
	bool ___SetBestTracer_52;
	// System.Boolean PlayerMode::SetWealthTracer
	bool ___SetWealthTracer_53;

public:
	inline static int32_t get_offset_of_mCardResult_5() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mCardResult_5)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_mCardResult_5() const { return ___mCardResult_5; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_mCardResult_5() { return &___mCardResult_5; }
	inline void set_mCardResult_5(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___mCardResult_5 = value;
		Il2CppCodeGenWriteBarrier((&___mCardResult_5), value);
	}

	inline static int32_t get_offset_of_mCardUrlCount_6() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mCardUrlCount_6)); }
	inline int32_t get_mCardUrlCount_6() const { return ___mCardUrlCount_6; }
	inline int32_t* get_address_of_mCardUrlCount_6() { return &___mCardUrlCount_6; }
	inline void set_mCardUrlCount_6(int32_t value)
	{
		___mCardUrlCount_6 = value;
	}

	inline static int32_t get_offset_of_mCardNameCount_7() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mCardNameCount_7)); }
	inline int32_t get_mCardNameCount_7() const { return ___mCardNameCount_7; }
	inline int32_t* get_address_of_mCardNameCount_7() { return &___mCardNameCount_7; }
	inline void set_mCardNameCount_7(int32_t value)
	{
		___mCardNameCount_7 = value;
	}

	inline static int32_t get_offset_of_mCardDownloadCount_8() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mCardDownloadCount_8)); }
	inline int32_t get_mCardDownloadCount_8() const { return ___mCardDownloadCount_8; }
	inline int32_t* get_address_of_mCardDownloadCount_8() { return &___mCardDownloadCount_8; }
	inline void set_mCardDownloadCount_8(int32_t value)
	{
		___mCardDownloadCount_8 = value;
	}

	inline static int32_t get_offset_of_mCollectionComplete_9() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mCollectionComplete_9)); }
	inline bool get_mCollectionComplete_9() const { return ___mCollectionComplete_9; }
	inline bool* get_address_of_mCollectionComplete_9() { return &___mCollectionComplete_9; }
	inline void set_mCollectionComplete_9(bool value)
	{
		___mCollectionComplete_9 = value;
	}

	inline static int32_t get_offset_of_mRankingComplete_10() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mRankingComplete_10)); }
	inline bool get_mRankingComplete_10() const { return ___mRankingComplete_10; }
	inline bool* get_address_of_mRankingComplete_10() { return &___mRankingComplete_10; }
	inline void set_mRankingComplete_10(bool value)
	{
		___mRankingComplete_10 = value;
	}

	inline static int32_t get_offset_of_mLoadPlayerDataComplete_11() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mLoadPlayerDataComplete_11)); }
	inline bool get_mLoadPlayerDataComplete_11() const { return ___mLoadPlayerDataComplete_11; }
	inline bool* get_address_of_mLoadPlayerDataComplete_11() { return &___mLoadPlayerDataComplete_11; }
	inline void set_mLoadPlayerDataComplete_11(bool value)
	{
		___mLoadPlayerDataComplete_11 = value;
	}

	inline static int32_t get_offset_of_mRankResult_12() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mRankResult_12)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_mRankResult_12() const { return ___mRankResult_12; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_mRankResult_12() { return &___mRankResult_12; }
	inline void set_mRankResult_12(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___mRankResult_12 = value;
		Il2CppCodeGenWriteBarrier((&___mRankResult_12), value);
	}

	inline static int32_t get_offset_of_mRankCount_13() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mRankCount_13)); }
	inline int32_t get_mRankCount_13() const { return ___mRankCount_13; }
	inline int32_t* get_address_of_mRankCount_13() { return &___mRankCount_13; }
	inline void set_mRankCount_13(int32_t value)
	{
		___mRankCount_13 = value;
	}

	inline static int32_t get_offset_of_mBestTracerRankCount_14() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mBestTracerRankCount_14)); }
	inline int32_t get_mBestTracerRankCount_14() const { return ___mBestTracerRankCount_14; }
	inline int32_t* get_address_of_mBestTracerRankCount_14() { return &___mBestTracerRankCount_14; }
	inline void set_mBestTracerRankCount_14(int32_t value)
	{
		___mBestTracerRankCount_14 = value;
	}

	inline static int32_t get_offset_of_GameCanvasPanel_15() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___GameCanvasPanel_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_GameCanvasPanel_15() const { return ___GameCanvasPanel_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_GameCanvasPanel_15() { return &___GameCanvasPanel_15; }
	inline void set_GameCanvasPanel_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___GameCanvasPanel_15 = value;
		Il2CppCodeGenWriteBarrier((&___GameCanvasPanel_15), value);
	}

	inline static int32_t get_offset_of_mPlayerMode_16() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mPlayerMode_16)); }
	inline int32_t get_mPlayerMode_16() const { return ___mPlayerMode_16; }
	inline int32_t* get_address_of_mPlayerMode_16() { return &___mPlayerMode_16; }
	inline void set_mPlayerMode_16(int32_t value)
	{
		___mPlayerMode_16 = value;
	}

	inline static int32_t get_offset_of_mPanel_17() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mPanel_17)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_mPanel_17() const { return ___mPanel_17; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_mPanel_17() { return &___mPanel_17; }
	inline void set_mPanel_17(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___mPanel_17 = value;
		Il2CppCodeGenWriteBarrier((&___mPanel_17), value);
	}

	inline static int32_t get_offset_of_mPanelButton_18() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mPanelButton_18)); }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* get_mPanelButton_18() const { return ___mPanelButton_18; }
	inline GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067** get_address_of_mPanelButton_18() { return &___mPanelButton_18; }
	inline void set_mPanelButton_18(GameObjectU5BU5D_t30E337C5B5FBBCBAEDAF4093924D916CF2944067* value)
	{
		___mPanelButton_18 = value;
		Il2CppCodeGenWriteBarrier((&___mPanelButton_18), value);
	}

	inline static int32_t get_offset_of_mMissionButton_19() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mMissionButton_19)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mMissionButton_19() const { return ___mMissionButton_19; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mMissionButton_19() { return &___mMissionButton_19; }
	inline void set_mMissionButton_19(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mMissionButton_19 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionButton_19), value);
	}

	inline static int32_t get_offset_of_mMissionButtonData_20() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mMissionButtonData_20)); }
	inline MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9 * get_mMissionButtonData_20() const { return ___mMissionButtonData_20; }
	inline MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9 ** get_address_of_mMissionButtonData_20() { return &___mMissionButtonData_20; }
	inline void set_mMissionButtonData_20(MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9 * value)
	{
		___mMissionButtonData_20 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionButtonData_20), value);
	}

	inline static int32_t get_offset_of_mResultButton_21() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mResultButton_21)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mResultButton_21() const { return ___mResultButton_21; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mResultButton_21() { return &___mResultButton_21; }
	inline void set_mResultButton_21(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mResultButton_21 = value;
		Il2CppCodeGenWriteBarrier((&___mResultButton_21), value);
	}

	inline static int32_t get_offset_of_recordData_22() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___recordData_22)); }
	inline MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A * get_recordData_22() const { return ___recordData_22; }
	inline MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A ** get_address_of_recordData_22() { return &___recordData_22; }
	inline void set_recordData_22(MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A * value)
	{
		___recordData_22 = value;
		Il2CppCodeGenWriteBarrier((&___recordData_22), value);
	}

	inline static int32_t get_offset_of_mMissionContent_23() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mMissionContent_23)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_mMissionContent_23() const { return ___mMissionContent_23; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_mMissionContent_23() { return &___mMissionContent_23; }
	inline void set_mMissionContent_23(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___mMissionContent_23 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionContent_23), value);
	}

	inline static int32_t get_offset_of_mMissionRecordContent_24() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mMissionRecordContent_24)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_mMissionRecordContent_24() const { return ___mMissionRecordContent_24; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_mMissionRecordContent_24() { return &___mMissionRecordContent_24; }
	inline void set_mMissionRecordContent_24(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___mMissionRecordContent_24 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionRecordContent_24), value);
	}

	inline static int32_t get_offset_of_mMissionView_25() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mMissionView_25)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mMissionView_25() const { return ___mMissionView_25; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mMissionView_25() { return &___mMissionView_25; }
	inline void set_mMissionView_25(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mMissionView_25 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionView_25), value);
	}

	inline static int32_t get_offset_of_mMissionRecordView_26() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mMissionRecordView_26)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mMissionRecordView_26() const { return ___mMissionRecordView_26; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mMissionRecordView_26() { return &___mMissionRecordView_26; }
	inline void set_mMissionRecordView_26(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mMissionRecordView_26 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionRecordView_26), value);
	}

	inline static int32_t get_offset_of_mMissionButtonOn_27() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mMissionButtonOn_27)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mMissionButtonOn_27() const { return ___mMissionButtonOn_27; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mMissionButtonOn_27() { return &___mMissionButtonOn_27; }
	inline void set_mMissionButtonOn_27(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mMissionButtonOn_27 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionButtonOn_27), value);
	}

	inline static int32_t get_offset_of_mMissionButtonOff_28() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mMissionButtonOff_28)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mMissionButtonOff_28() const { return ___mMissionButtonOff_28; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mMissionButtonOff_28() { return &___mMissionButtonOff_28; }
	inline void set_mMissionButtonOff_28(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mMissionButtonOff_28 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionButtonOff_28), value);
	}

	inline static int32_t get_offset_of_mMissionRecordButtonOn_29() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mMissionRecordButtonOn_29)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mMissionRecordButtonOn_29() const { return ___mMissionRecordButtonOn_29; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mMissionRecordButtonOn_29() { return &___mMissionRecordButtonOn_29; }
	inline void set_mMissionRecordButtonOn_29(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mMissionRecordButtonOn_29 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionRecordButtonOn_29), value);
	}

	inline static int32_t get_offset_of_mMissionRecordButtonOff_30() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mMissionRecordButtonOff_30)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mMissionRecordButtonOff_30() const { return ___mMissionRecordButtonOff_30; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mMissionRecordButtonOff_30() { return &___mMissionRecordButtonOff_30; }
	inline void set_mMissionRecordButtonOff_30(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mMissionRecordButtonOff_30 = value;
		Il2CppCodeGenWriteBarrier((&___mMissionRecordButtonOff_30), value);
	}

	inline static int32_t get_offset_of_mPanelDic_31() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mPanelDic_31)); }
	inline Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * get_mPanelDic_31() const { return ___mPanelDic_31; }
	inline Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 ** get_address_of_mPanelDic_31() { return &___mPanelDic_31; }
	inline void set_mPanelDic_31(Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * value)
	{
		___mPanelDic_31 = value;
		Il2CppCodeGenWriteBarrier((&___mPanelDic_31), value);
	}

	inline static int32_t get_offset_of_mPanelButtonDic_32() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mPanelButtonDic_32)); }
	inline Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * get_mPanelButtonDic_32() const { return ___mPanelButtonDic_32; }
	inline Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 ** get_address_of_mPanelButtonDic_32() { return &___mPanelButtonDic_32; }
	inline void set_mPanelButtonDic_32(Dictionary_2_t8BB31FE280EDABC334545343234D0E2E47A71D84 * value)
	{
		___mPanelButtonDic_32 = value;
		Il2CppCodeGenWriteBarrier((&___mPanelButtonDic_32), value);
	}

	inline static int32_t get_offset_of_mCollectionContent_33() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mCollectionContent_33)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_mCollectionContent_33() const { return ___mCollectionContent_33; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_mCollectionContent_33() { return &___mCollectionContent_33; }
	inline void set_mCollectionContent_33(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___mCollectionContent_33 = value;
		Il2CppCodeGenWriteBarrier((&___mCollectionContent_33), value);
	}

	inline static int32_t get_offset_of_mRecordLayout_34() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mRecordLayout_34)); }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * get_mRecordLayout_34() const { return ___mRecordLayout_34; }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 ** get_address_of_mRecordLayout_34() { return &___mRecordLayout_34; }
	inline void set_mRecordLayout_34(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * value)
	{
		___mRecordLayout_34 = value;
		Il2CppCodeGenWriteBarrier((&___mRecordLayout_34), value);
	}

	inline static int32_t get_offset_of_mBestTracerRankContent_35() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mBestTracerRankContent_35)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_mBestTracerRankContent_35() const { return ___mBestTracerRankContent_35; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_mBestTracerRankContent_35() { return &___mBestTracerRankContent_35; }
	inline void set_mBestTracerRankContent_35(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___mBestTracerRankContent_35 = value;
		Il2CppCodeGenWriteBarrier((&___mBestTracerRankContent_35), value);
	}

	inline static int32_t get_offset_of_mWealthTracerRankContent_36() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mWealthTracerRankContent_36)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_mWealthTracerRankContent_36() const { return ___mWealthTracerRankContent_36; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_mWealthTracerRankContent_36() { return &___mWealthTracerRankContent_36; }
	inline void set_mWealthTracerRankContent_36(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___mWealthTracerRankContent_36 = value;
		Il2CppCodeGenWriteBarrier((&___mWealthTracerRankContent_36), value);
	}

	inline static int32_t get_offset_of_mPlayerRank_37() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mPlayerRank_37)); }
	inline LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52 * get_mPlayerRank_37() const { return ___mPlayerRank_37; }
	inline LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52 ** get_address_of_mPlayerRank_37() { return &___mPlayerRank_37; }
	inline void set_mPlayerRank_37(LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52 * value)
	{
		___mPlayerRank_37 = value;
		Il2CppCodeGenWriteBarrier((&___mPlayerRank_37), value);
	}

	inline static int32_t get_offset_of_EnumImage_38() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___EnumImage_38)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_EnumImage_38() const { return ___EnumImage_38; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_EnumImage_38() { return &___EnumImage_38; }
	inline void set_EnumImage_38(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___EnumImage_38 = value;
		Il2CppCodeGenWriteBarrier((&___EnumImage_38), value);
	}

	inline static int32_t get_offset_of_mBestTracerView_39() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mBestTracerView_39)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_mBestTracerView_39() const { return ___mBestTracerView_39; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_mBestTracerView_39() { return &___mBestTracerView_39; }
	inline void set_mBestTracerView_39(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___mBestTracerView_39 = value;
		Il2CppCodeGenWriteBarrier((&___mBestTracerView_39), value);
	}

	inline static int32_t get_offset_of_mWealthTracerView_40() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mWealthTracerView_40)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_mWealthTracerView_40() const { return ___mWealthTracerView_40; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_mWealthTracerView_40() { return &___mWealthTracerView_40; }
	inline void set_mWealthTracerView_40(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___mWealthTracerView_40 = value;
		Il2CppCodeGenWriteBarrier((&___mWealthTracerView_40), value);
	}

	inline static int32_t get_offset_of_mBestTracerButtonOn_41() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mBestTracerButtonOn_41)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mBestTracerButtonOn_41() const { return ___mBestTracerButtonOn_41; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mBestTracerButtonOn_41() { return &___mBestTracerButtonOn_41; }
	inline void set_mBestTracerButtonOn_41(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mBestTracerButtonOn_41 = value;
		Il2CppCodeGenWriteBarrier((&___mBestTracerButtonOn_41), value);
	}

	inline static int32_t get_offset_of_mBestTracerButtonOff_42() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mBestTracerButtonOff_42)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mBestTracerButtonOff_42() const { return ___mBestTracerButtonOff_42; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mBestTracerButtonOff_42() { return &___mBestTracerButtonOff_42; }
	inline void set_mBestTracerButtonOff_42(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mBestTracerButtonOff_42 = value;
		Il2CppCodeGenWriteBarrier((&___mBestTracerButtonOff_42), value);
	}

	inline static int32_t get_offset_of_mWealthTracerButtonOn_43() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mWealthTracerButtonOn_43)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mWealthTracerButtonOn_43() const { return ___mWealthTracerButtonOn_43; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mWealthTracerButtonOn_43() { return &___mWealthTracerButtonOn_43; }
	inline void set_mWealthTracerButtonOn_43(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mWealthTracerButtonOn_43 = value;
		Il2CppCodeGenWriteBarrier((&___mWealthTracerButtonOn_43), value);
	}

	inline static int32_t get_offset_of_mWealthTracerButtonOff_44() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___mWealthTracerButtonOff_44)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mWealthTracerButtonOff_44() const { return ___mWealthTracerButtonOff_44; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mWealthTracerButtonOff_44() { return &___mWealthTracerButtonOff_44; }
	inline void set_mWealthTracerButtonOff_44(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mWealthTracerButtonOff_44 = value;
		Il2CppCodeGenWriteBarrier((&___mWealthTracerButtonOff_44), value);
	}

	inline static int32_t get_offset_of_Money_45() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___Money_45)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_Money_45() const { return ___Money_45; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_Money_45() { return &___Money_45; }
	inline void set_Money_45(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___Money_45 = value;
		Il2CppCodeGenWriteBarrier((&___Money_45), value);
	}

	inline static int32_t get_offset_of_Name_46() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___Name_46)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_Name_46() const { return ___Name_46; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_Name_46() { return &___Name_46; }
	inline void set_Name_46(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___Name_46 = value;
		Il2CppCodeGenWriteBarrier((&___Name_46), value);
	}

	inline static int32_t get_offset_of_CollectLayout_47() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___CollectLayout_47)); }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * get_CollectLayout_47() const { return ___CollectLayout_47; }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 ** get_address_of_CollectLayout_47() { return &___CollectLayout_47; }
	inline void set_CollectLayout_47(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * value)
	{
		___CollectLayout_47 = value;
		Il2CppCodeGenWriteBarrier((&___CollectLayout_47), value);
	}

	inline static int32_t get_offset_of_BestTracerLayoutGroup_48() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___BestTracerLayoutGroup_48)); }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * get_BestTracerLayoutGroup_48() const { return ___BestTracerLayoutGroup_48; }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 ** get_address_of_BestTracerLayoutGroup_48() { return &___BestTracerLayoutGroup_48; }
	inline void set_BestTracerLayoutGroup_48(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * value)
	{
		___BestTracerLayoutGroup_48 = value;
		Il2CppCodeGenWriteBarrier((&___BestTracerLayoutGroup_48), value);
	}

	inline static int32_t get_offset_of_WealthTracerLayoutGroup_49() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___WealthTracerLayoutGroup_49)); }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * get_WealthTracerLayoutGroup_49() const { return ___WealthTracerLayoutGroup_49; }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 ** get_address_of_WealthTracerLayoutGroup_49() { return &___WealthTracerLayoutGroup_49; }
	inline void set_WealthTracerLayoutGroup_49(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * value)
	{
		___WealthTracerLayoutGroup_49 = value;
		Il2CppCodeGenWriteBarrier((&___WealthTracerLayoutGroup_49), value);
	}

	inline static int32_t get_offset_of_MissionLayoutGroup_50() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___MissionLayoutGroup_50)); }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * get_MissionLayoutGroup_50() const { return ___MissionLayoutGroup_50; }
	inline GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 ** get_address_of_MissionLayoutGroup_50() { return &___MissionLayoutGroup_50; }
	inline void set_MissionLayoutGroup_50(GridLayoutGroup_t1C70294BD2567FD584672222A8BFD5A0DF1CA2A8 * value)
	{
		___MissionLayoutGroup_50 = value;
		Il2CppCodeGenWriteBarrier((&___MissionLayoutGroup_50), value);
	}

	inline static int32_t get_offset_of_CollectionContentORect_51() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___CollectionContentORect_51)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_CollectionContentORect_51() const { return ___CollectionContentORect_51; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_CollectionContentORect_51() { return &___CollectionContentORect_51; }
	inline void set_CollectionContentORect_51(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___CollectionContentORect_51 = value;
	}

	inline static int32_t get_offset_of_SetBestTracer_52() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___SetBestTracer_52)); }
	inline bool get_SetBestTracer_52() const { return ___SetBestTracer_52; }
	inline bool* get_address_of_SetBestTracer_52() { return &___SetBestTracer_52; }
	inline void set_SetBestTracer_52(bool value)
	{
		___SetBestTracer_52 = value;
	}

	inline static int32_t get_offset_of_SetWealthTracer_53() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97, ___SetWealthTracer_53)); }
	inline bool get_SetWealthTracer_53() const { return ___SetWealthTracer_53; }
	inline bool* get_address_of_SetWealthTracer_53() { return &___SetWealthTracer_53; }
	inline void set_SetWealthTracer_53(bool value)
	{
		___SetWealthTracer_53 = value;
	}
};

struct PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97_StaticFields
{
public:
	// PlayerMode PlayerMode::mInstance
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * ___mInstance_4;

public:
	inline static int32_t get_offset_of_mInstance_4() { return static_cast<int32_t>(offsetof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97_StaticFields, ___mInstance_4)); }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * get_mInstance_4() const { return ___mInstance_4; }
	inline PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 ** get_address_of_mInstance_4() { return &___mInstance_4; }
	inline void set_mInstance_4(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97 * value)
	{
		___mInstance_4 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMODE_TC98090D1841F0D9D319F822FF716CA599235AC97_H
#ifndef RESOURCELOAD_TE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365_H
#define RESOURCELOAD_TE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceLoad
struct  ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// ResourceLoad_eSystemType ResourceLoad::mSystemType
	int32_t ___mSystemType_4;
	// RsourceStores ResourceLoad::mFactoryStore
	RsourceStores_t7DF0BF48D6C2C9A7496994994A0A55517E309385 * ___mFactoryStore_6;

public:
	inline static int32_t get_offset_of_mSystemType_4() { return static_cast<int32_t>(offsetof(ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365, ___mSystemType_4)); }
	inline int32_t get_mSystemType_4() const { return ___mSystemType_4; }
	inline int32_t* get_address_of_mSystemType_4() { return &___mSystemType_4; }
	inline void set_mSystemType_4(int32_t value)
	{
		___mSystemType_4 = value;
	}

	inline static int32_t get_offset_of_mFactoryStore_6() { return static_cast<int32_t>(offsetof(ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365, ___mFactoryStore_6)); }
	inline RsourceStores_t7DF0BF48D6C2C9A7496994994A0A55517E309385 * get_mFactoryStore_6() const { return ___mFactoryStore_6; }
	inline RsourceStores_t7DF0BF48D6C2C9A7496994994A0A55517E309385 ** get_address_of_mFactoryStore_6() { return &___mFactoryStore_6; }
	inline void set_mFactoryStore_6(RsourceStores_t7DF0BF48D6C2C9A7496994994A0A55517E309385 * value)
	{
		___mFactoryStore_6 = value;
		Il2CppCodeGenWriteBarrier((&___mFactoryStore_6), value);
	}
};

struct ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365_StaticFields
{
public:
	// ResourceLoad ResourceLoad::mInstance
	ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365 * ___mInstance_5;

public:
	inline static int32_t get_offset_of_mInstance_5() { return static_cast<int32_t>(offsetof(ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365_StaticFields, ___mInstance_5)); }
	inline ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365 * get_mInstance_5() const { return ___mInstance_5; }
	inline ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365 ** get_address_of_mInstance_5() { return &___mInstance_5; }
	inline void set_mInstance_5(ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365 * value)
	{
		___mInstance_5 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCELOAD_TE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365_H
#ifndef UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#define UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3C3C339CD5677BA7FC27C352FED8B78052A3FE70_H
#ifndef CAMERAAPP_T36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7_H
#define CAMERAAPP_T36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// cameraApp
struct  cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean cameraApp::cameraon
	bool ___cameraon_8;
	// System.Boolean cameraApp::checkcamera
	bool ___checkcamera_9;
	// System.Boolean cameraApp::camAvailable
	bool ___camAvailable_10;
	// UnityEngine.UI.RawImage cameraApp::backGround
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___backGround_11;
	// UnityEngine.Color32[] cameraApp::data
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ___data_12;
	// UnityEngine.GameObject cameraApp::mScaleImage
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___mScaleImage_13;

public:
	inline static int32_t get_offset_of_cameraon_8() { return static_cast<int32_t>(offsetof(cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7, ___cameraon_8)); }
	inline bool get_cameraon_8() const { return ___cameraon_8; }
	inline bool* get_address_of_cameraon_8() { return &___cameraon_8; }
	inline void set_cameraon_8(bool value)
	{
		___cameraon_8 = value;
	}

	inline static int32_t get_offset_of_checkcamera_9() { return static_cast<int32_t>(offsetof(cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7, ___checkcamera_9)); }
	inline bool get_checkcamera_9() const { return ___checkcamera_9; }
	inline bool* get_address_of_checkcamera_9() { return &___checkcamera_9; }
	inline void set_checkcamera_9(bool value)
	{
		___checkcamera_9 = value;
	}

	inline static int32_t get_offset_of_camAvailable_10() { return static_cast<int32_t>(offsetof(cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7, ___camAvailable_10)); }
	inline bool get_camAvailable_10() const { return ___camAvailable_10; }
	inline bool* get_address_of_camAvailable_10() { return &___camAvailable_10; }
	inline void set_camAvailable_10(bool value)
	{
		___camAvailable_10 = value;
	}

	inline static int32_t get_offset_of_backGround_11() { return static_cast<int32_t>(offsetof(cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7, ___backGround_11)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_backGround_11() const { return ___backGround_11; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_backGround_11() { return &___backGround_11; }
	inline void set_backGround_11(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___backGround_11 = value;
		Il2CppCodeGenWriteBarrier((&___backGround_11), value);
	}

	inline static int32_t get_offset_of_data_12() { return static_cast<int32_t>(offsetof(cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7, ___data_12)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get_data_12() const { return ___data_12; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of_data_12() { return &___data_12; }
	inline void set_data_12(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		___data_12 = value;
		Il2CppCodeGenWriteBarrier((&___data_12), value);
	}

	inline static int32_t get_offset_of_mScaleImage_13() { return static_cast<int32_t>(offsetof(cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7, ___mScaleImage_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_mScaleImage_13() const { return ___mScaleImage_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_mScaleImage_13() { return &___mScaleImage_13; }
	inline void set_mScaleImage_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___mScaleImage_13 = value;
		Il2CppCodeGenWriteBarrier((&___mScaleImage_13), value);
	}
};

struct cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7_StaticFields
{
public:
	// UnityEngine.WebCamTexture cameraApp::mytexture
	WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * ___mytexture_4;
	// cameraApp cameraApp::Instance
	cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7 * ___Instance_5;

public:
	inline static int32_t get_offset_of_mytexture_4() { return static_cast<int32_t>(offsetof(cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7_StaticFields, ___mytexture_4)); }
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * get_mytexture_4() const { return ___mytexture_4; }
	inline WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 ** get_address_of_mytexture_4() { return &___mytexture_4; }
	inline void set_mytexture_4(WebCamTexture_tEC97A7A0A101B159FEC9A3E95B14E41DD84EFD73 * value)
	{
		___mytexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___mytexture_4), value);
	}

	inline static int32_t get_offset_of_Instance_5() { return static_cast<int32_t>(offsetof(cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7_StaticFields, ___Instance_5)); }
	inline cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7 * get_Instance_5() const { return ___Instance_5; }
	inline cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7 ** get_address_of_Instance_5() { return &___Instance_5; }
	inline void set_Instance_5(cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7 * value)
	{
		___Instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAAPP_T36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7_H
#ifndef GYROCONTROL_TED692BF5B2DB536AAAFCFB695D38F4F6826807D0_H
#define GYROCONTROL_TED692BF5B2DB536AAAFCFB695D38F4F6826807D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// gyrocontrol
struct  gyrocontrol_tED692BF5B2DB536AAAFCFB695D38F4F6826807D0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean gyrocontrol::gyroEnable
	bool ___gyroEnable_4;
	// UnityEngine.Gyroscope gyrocontrol::gyro
	Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC * ___gyro_5;
	// UnityEngine.GameObject gyrocontrol::cameraContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___cameraContainer_6;
	// UnityEngine.Quaternion gyrocontrol::rot
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rot_7;

public:
	inline static int32_t get_offset_of_gyroEnable_4() { return static_cast<int32_t>(offsetof(gyrocontrol_tED692BF5B2DB536AAAFCFB695D38F4F6826807D0, ___gyroEnable_4)); }
	inline bool get_gyroEnable_4() const { return ___gyroEnable_4; }
	inline bool* get_address_of_gyroEnable_4() { return &___gyroEnable_4; }
	inline void set_gyroEnable_4(bool value)
	{
		___gyroEnable_4 = value;
	}

	inline static int32_t get_offset_of_gyro_5() { return static_cast<int32_t>(offsetof(gyrocontrol_tED692BF5B2DB536AAAFCFB695D38F4F6826807D0, ___gyro_5)); }
	inline Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC * get_gyro_5() const { return ___gyro_5; }
	inline Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC ** get_address_of_gyro_5() { return &___gyro_5; }
	inline void set_gyro_5(Gyroscope_t004CBC5FD2FC2B3E27EE405E16FBB27ACC9BB2DC * value)
	{
		___gyro_5 = value;
		Il2CppCodeGenWriteBarrier((&___gyro_5), value);
	}

	inline static int32_t get_offset_of_cameraContainer_6() { return static_cast<int32_t>(offsetof(gyrocontrol_tED692BF5B2DB536AAAFCFB695D38F4F6826807D0, ___cameraContainer_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_cameraContainer_6() const { return ___cameraContainer_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_cameraContainer_6() { return &___cameraContainer_6; }
	inline void set_cameraContainer_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___cameraContainer_6 = value;
		Il2CppCodeGenWriteBarrier((&___cameraContainer_6), value);
	}

	inline static int32_t get_offset_of_rot_7() { return static_cast<int32_t>(offsetof(gyrocontrol_tED692BF5B2DB536AAAFCFB695D38F4F6826807D0, ___rot_7)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rot_7() const { return ___rot_7; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rot_7() { return &___rot_7; }
	inline void set_rot_7(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rot_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GYROCONTROL_TED692BF5B2DB536AAAFCFB695D38F4F6826807D0_H
#ifndef BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#define BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5, ___m_Graphic_4)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T72759F31F9D204D7EFB6B45097873809D4524BA5_H
#ifndef LAYOUTELEMENT_TD503826DB41B6EA85AC689292F8B2661B3C1048B_H
#define LAYOUTELEMENT_TD503826DB41B6EA85AC689292F8B2661B3C1048B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutElement
struct  LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Boolean UnityEngine.UI.LayoutElement::m_IgnoreLayout
	bool ___m_IgnoreLayout_4;
	// System.Single UnityEngine.UI.LayoutElement::m_MinWidth
	float ___m_MinWidth_5;
	// System.Single UnityEngine.UI.LayoutElement::m_MinHeight
	float ___m_MinHeight_6;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredWidth
	float ___m_PreferredWidth_7;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredHeight
	float ___m_PreferredHeight_8;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleWidth
	float ___m_FlexibleWidth_9;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleHeight
	float ___m_FlexibleHeight_10;
	// System.Int32 UnityEngine.UI.LayoutElement::m_LayoutPriority
	int32_t ___m_LayoutPriority_11;

public:
	inline static int32_t get_offset_of_m_IgnoreLayout_4() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_IgnoreLayout_4)); }
	inline bool get_m_IgnoreLayout_4() const { return ___m_IgnoreLayout_4; }
	inline bool* get_address_of_m_IgnoreLayout_4() { return &___m_IgnoreLayout_4; }
	inline void set_m_IgnoreLayout_4(bool value)
	{
		___m_IgnoreLayout_4 = value;
	}

	inline static int32_t get_offset_of_m_MinWidth_5() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_MinWidth_5)); }
	inline float get_m_MinWidth_5() const { return ___m_MinWidth_5; }
	inline float* get_address_of_m_MinWidth_5() { return &___m_MinWidth_5; }
	inline void set_m_MinWidth_5(float value)
	{
		___m_MinWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_MinHeight_6() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_MinHeight_6)); }
	inline float get_m_MinHeight_6() const { return ___m_MinHeight_6; }
	inline float* get_address_of_m_MinHeight_6() { return &___m_MinHeight_6; }
	inline void set_m_MinHeight_6(float value)
	{
		___m_MinHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_PreferredWidth_7() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_PreferredWidth_7)); }
	inline float get_m_PreferredWidth_7() const { return ___m_PreferredWidth_7; }
	inline float* get_address_of_m_PreferredWidth_7() { return &___m_PreferredWidth_7; }
	inline void set_m_PreferredWidth_7(float value)
	{
		___m_PreferredWidth_7 = value;
	}

	inline static int32_t get_offset_of_m_PreferredHeight_8() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_PreferredHeight_8)); }
	inline float get_m_PreferredHeight_8() const { return ___m_PreferredHeight_8; }
	inline float* get_address_of_m_PreferredHeight_8() { return &___m_PreferredHeight_8; }
	inline void set_m_PreferredHeight_8(float value)
	{
		___m_PreferredHeight_8 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleWidth_9() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_FlexibleWidth_9)); }
	inline float get_m_FlexibleWidth_9() const { return ___m_FlexibleWidth_9; }
	inline float* get_address_of_m_FlexibleWidth_9() { return &___m_FlexibleWidth_9; }
	inline void set_m_FlexibleWidth_9(float value)
	{
		___m_FlexibleWidth_9 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleHeight_10() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_FlexibleHeight_10)); }
	inline float get_m_FlexibleHeight_10() const { return ___m_FlexibleHeight_10; }
	inline float* get_address_of_m_FlexibleHeight_10() { return &___m_FlexibleHeight_10; }
	inline void set_m_FlexibleHeight_10(float value)
	{
		___m_FlexibleHeight_10 = value;
	}

	inline static int32_t get_offset_of_m_LayoutPriority_11() { return static_cast<int32_t>(offsetof(LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B, ___m_LayoutPriority_11)); }
	inline int32_t get_m_LayoutPriority_11() const { return ___m_LayoutPriority_11; }
	inline int32_t* get_address_of_m_LayoutPriority_11() { return &___m_LayoutPriority_11; }
	inline void set_m_LayoutPriority_11(int32_t value)
	{
		___m_LayoutPriority_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTELEMENT_TD503826DB41B6EA85AC689292F8B2661B3C1048B_H
#ifndef LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#define LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * ___m_Padding_4;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_5;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_Rect_6;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  ___m_Tracker_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalMinSize_8;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalPreferredSize_9;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_TotalFlexibleSize_10;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * ___m_RectChildren_11;

public:
	inline static int32_t get_offset_of_m_Padding_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Padding_4)); }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * get_m_Padding_4() const { return ___m_Padding_4; }
	inline RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A ** get_address_of_m_Padding_4() { return &___m_Padding_4; }
	inline void set_m_Padding_4(RectOffset_tED44B1176E93501050480416699D1F11BAE8C87A * value)
	{
		___m_Padding_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_4), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_ChildAlignment_5)); }
	inline int32_t get_m_ChildAlignment_5() const { return ___m_ChildAlignment_5; }
	inline int32_t* get_address_of_m_ChildAlignment_5() { return &___m_ChildAlignment_5; }
	inline void set_m_ChildAlignment_5(int32_t value)
	{
		___m_ChildAlignment_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Rect_6)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_Tracker_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_Tracker_7)); }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  get_m_Tracker_7() const { return ___m_Tracker_7; }
	inline DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03 * get_address_of_m_Tracker_7() { return &___m_Tracker_7; }
	inline void set_m_Tracker_7(DrivenRectTransformTracker_tB8FBBE24EEE9618CA32E4B3CF52F4AD7FDDEBE03  value)
	{
		___m_Tracker_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalMinSize_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalMinSize_8() const { return ___m_TotalMinSize_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalMinSize_8() { return &___m_TotalMinSize_8; }
	inline void set_m_TotalMinSize_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalMinSize_8 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalPreferredSize_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalPreferredSize_9() const { return ___m_TotalPreferredSize_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalPreferredSize_9() { return &___m_TotalPreferredSize_9; }
	inline void set_m_TotalPreferredSize_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalPreferredSize_9 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_10() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_TotalFlexibleSize_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_TotalFlexibleSize_10() const { return ___m_TotalFlexibleSize_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_TotalFlexibleSize_10() { return &___m_TotalFlexibleSize_10; }
	inline void set_m_TotalFlexibleSize_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_TotalFlexibleSize_10 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_11() { return static_cast<int32_t>(offsetof(LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4, ___m_RectChildren_11)); }
	inline List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * get_m_RectChildren_11() const { return ___m_RectChildren_11; }
	inline List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C ** get_address_of_m_RectChildren_11() { return &___m_RectChildren_11; }
	inline void set_m_RectChildren_11(List_1_t0CD9761E1DF9817484CF4FB4253C6A626DC2311C * value)
	{
		___m_RectChildren_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T9E072B95DA6476C487C0B07A815291249025C0E4_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#define HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB  : public LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_14;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_15;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_16;

public:
	inline static int32_t get_offset_of_m_Spacing_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_Spacing_12)); }
	inline float get_m_Spacing_12() const { return ___m_Spacing_12; }
	inline float* get_address_of_m_Spacing_12() { return &___m_Spacing_12; }
	inline void set_m_Spacing_12(float value)
	{
		___m_Spacing_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildForceExpandWidth_13)); }
	inline bool get_m_ChildForceExpandWidth_13() const { return ___m_ChildForceExpandWidth_13; }
	inline bool* get_address_of_m_ChildForceExpandWidth_13() { return &___m_ChildForceExpandWidth_13; }
	inline void set_m_ChildForceExpandWidth_13(bool value)
	{
		___m_ChildForceExpandWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildForceExpandHeight_14)); }
	inline bool get_m_ChildForceExpandHeight_14() const { return ___m_ChildForceExpandHeight_14; }
	inline bool* get_address_of_m_ChildForceExpandHeight_14() { return &___m_ChildForceExpandHeight_14; }
	inline void set_m_ChildForceExpandHeight_14(bool value)
	{
		___m_ChildForceExpandHeight_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_15() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildControlWidth_15)); }
	inline bool get_m_ChildControlWidth_15() const { return ___m_ChildControlWidth_15; }
	inline bool* get_address_of_m_ChildControlWidth_15() { return &___m_ChildControlWidth_15; }
	inline void set_m_ChildControlWidth_15(bool value)
	{
		___m_ChildControlWidth_15 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_16() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB, ___m_ChildControlHeight_16)); }
	inline bool get_m_ChildControlHeight_16() const { return ___m_ChildControlHeight_16; }
	inline bool* get_address_of_m_ChildControlHeight_16() { return &___m_ChildControlHeight_16; }
	inline void set_m_ChildControlHeight_16(bool value)
	{
		___m_ChildControlHeight_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_TFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB_H
#ifndef POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#define POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T26F06E879E7B8DD2F93B8B3643053534D82F684A_H
#ifndef SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#define SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1  : public BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_EffectDistance_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_TA03D2493843CDF8E64569F985AEB3FEEEEB412E1_H
#ifndef HORIZONTALLAYOUTGROUP_TEFAFA0DDCCE4FC89CC2C0BE96E7C025D243CFE37_H
#define HORIZONTALLAYOUTGROUP_TEFAFA0DDCCE4FC89CC2C0BE96E7C025D243CFE37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalLayoutGroup
struct  HorizontalLayoutGroup_tEFAFA0DDCCE4FC89CC2C0BE96E7C025D243CFE37  : public HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALLAYOUTGROUP_TEFAFA0DDCCE4FC89CC2C0BE96E7C025D243CFE37_H
#ifndef OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#define OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_tB750E496976B072E79142D51C0A991AC20183095  : public Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_TB750E496976B072E79142D51C0A991AC20183095_H
#ifndef VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H
#define VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11  : public HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_TAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2800 = { sizeof (HorizontalLayoutGroup_tEFAFA0DDCCE4FC89CC2C0BE96E7C025D243CFE37), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2801 = { sizeof (HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2801[5] = 
{
	HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB::get_offset_of_m_Spacing_12(),
	HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB::get_offset_of_m_ChildForceExpandWidth_13(),
	HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB::get_offset_of_m_ChildForceExpandHeight_14(),
	HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB::get_offset_of_m_ChildControlWidth_15(),
	HorizontalOrVerticalLayoutGroup_tFE5C3DB19C2CC4906B3E5D5F4E1966CB585174AB::get_offset_of_m_ChildControlHeight_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2802 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2803 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2804 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2805 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2806 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2807 = { sizeof (LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2807[8] = 
{
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_IgnoreLayout_4(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_MinWidth_5(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_MinHeight_6(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_PreferredWidth_7(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_PreferredHeight_8(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_FlexibleWidth_9(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_FlexibleHeight_10(),
	LayoutElement_tD503826DB41B6EA85AC689292F8B2661B3C1048B::get_offset_of_m_LayoutPriority_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2808 = { sizeof (LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2808[8] = 
{
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_Padding_4(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_ChildAlignment_5(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_Rect_6(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_Tracker_7(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_TotalMinSize_8(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_TotalPreferredSize_9(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_TotalFlexibleSize_10(),
	LayoutGroup_t9E072B95DA6476C487C0B07A815291249025C0E4::get_offset_of_m_RectChildren_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2809 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2809[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_tB8BB61C2C033D95240B4E2704971663E4DC08073::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2810 = { sizeof (LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD), -1, sizeof(LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2810[9] = 
{
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t8D3501B43B1DE666140E2931FFA732B5B09EA5BD_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2811 = { sizeof (LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5), -1, sizeof(LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2811[8] = 
{
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t3B5074E34900DA384884FC50809EA395CB69E7D5_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2812 = { sizeof (VerticalLayoutGroup_tAAEE0BAA82E9A110591DEC9A3FFC25A01C2FFA11), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2813 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2814 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2814[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2815 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2815[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2816 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2816[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2817 = { sizeof (ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A), -1, sizeof(ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2817[7] = 
{
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_tBDADDC80D50C5F10BD00965217980B9A8D24BE8A_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2818 = { sizeof (Raycast3DCallback_t83483916473C9710AEDB316A65CBE62C58935C5F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2819 = { sizeof (Raycast2DCallback_tE99ABF9ABC3A380677949E8C05A3E477889B82BE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2820 = { sizeof (RaycastAllCallback_t751407A44270E02FAA43D0846A58EE6A8C4AE1CE), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2821 = { sizeof (GetRayIntersectionAllCallback_t68C2581CCF05E868297EBD3F3361274954845095), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2822 = { sizeof (GetRayIntersectionAllNonAllocCallback_tAD7508D45DB6679B6394983579AD18D967CC2AD4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2823 = { sizeof (GetRaycastNonAllocCallback_tC13D9767CFF00EAB26E9FCC4BDD505F0721A2B4D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2824 = { sizeof (VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F), -1, sizeof(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2824[12] = 
{
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Positions_0(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Colors_1(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv0S_2(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv1S_3(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv2S_4(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Uv3S_5(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Normals_6(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Tangents_7(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_Indices_8(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F_StaticFields::get_offset_of_s_DefaultNormal_10(),
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F::get_offset_of_m_ListsInitalized_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2825 = { sizeof (VertexHelperExtension_t593AF80A941B7208D2CC88AE671873CE6E2AA8E3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2826 = { sizeof (BaseVertexEffect_t1EF95AB1FC33A027710E7DC86D19F700156C4F6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2827 = { sizeof (BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2827[1] = 
{
	BaseMeshEffect_t72759F31F9D204D7EFB6B45097873809D4524BA5::get_offset_of_m_Graphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2828 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2829 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2830 = { sizeof (Outline_tB750E496976B072E79142D51C0A991AC20183095), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2831 = { sizeof (PositionAsUV1_t26F06E879E7B8DD2F93B8B3643053534D82F684A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2832 = { sizeof (Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2832[4] = 
{
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectColor_5(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_EffectDistance_6(),
	Shadow_tA03D2493843CDF8E64569F985AEB3FEEEEB412E1::get_offset_of_m_UseGraphicAlpha_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2833 = { sizeof (U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537), -1, sizeof(U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2833[1] = 
{
	U3CPrivateImplementationDetailsU3E_tC8332394FBFEEB4B73459A35E182942340DA3537_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2834 = { sizeof (U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t25F5D2FC4CFB01F181ED6F7A7F68C39C5D73E199 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2835 = { sizeof (U3CModuleU3E_t6CDDDF959E7E18A6744E43B613F41CDAC780256A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2836 = { sizeof (BeaconLetter_tFEB971E49820F00AE3DFE623D77C737AB51070CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2836[2] = 
{
	BeaconLetter_tFEB971E49820F00AE3DFE623D77C737AB51070CD::get_offset_of_mLetterText_4(),
	BeaconLetter_tFEB971E49820F00AE3DFE623D77C737AB51070CD::get_offset_of_mLetterAmountText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2837 = { sizeof (BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF), -1, sizeof(BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2837[5] = 
{
	BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF_StaticFields::get_offset_of_Instance_4(),
	BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF::get_offset_of_mBeaconLetterPanel_5(),
	BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF::get_offset_of_mBeaconLetter_6(),
	BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF::get_offset_of_mBeaconLetterScripts_7(),
	BeaconLetterPanelController_tB582DEC981B6FF63B5FE55F9CA7FEBB140D1D1DF::get_offset_of_mSerilize_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2838 = { sizeof (BTracerBeacon_tC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2838[2] = 
{
	BTracerBeacon_tC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD::get_offset_of_BeaconName_0(),
	BTracerBeacon_tC4F3F930F62DCC34D2AD42FBD6578BE8D2FDDBBD::get_offset_of_BeaconDic_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2839 = { sizeof (SingleBeaconData_t2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2839[4] = 
{
	SingleBeaconData_t2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69::get_offset_of_UUID_0(),
	SingleBeaconData_t2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69::get_offset_of_Major_1(),
	SingleBeaconData_t2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69::get_offset_of_Minor_2(),
	SingleBeaconData_t2CEB245DEAB4B0C73E1B93573A2EF598EDD05F69::get_offset_of_mContents_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2840 = { sizeof (cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7), -1, sizeof(cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2840[10] = 
{
	cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7_StaticFields::get_offset_of_mytexture_4(),
	cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7_StaticFields::get_offset_of_Instance_5(),
	0,
	0,
	cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7::get_offset_of_cameraon_8(),
	cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7::get_offset_of_checkcamera_9(),
	cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7::get_offset_of_camAvailable_10(),
	cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7::get_offset_of_backGround_11(),
	cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7::get_offset_of_data_12(),
	cameraApp_t36BF613F4AB21F3D5D9ACE69E44B1C7695F268F7::get_offset_of_mScaleImage_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2841 = { sizeof (U3CCameraOnU3Ec__Iterator0_tD3CB0D761CDCD91380D2D419E8225BE7B15F1E53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2841[4] = 
{
	U3CCameraOnU3Ec__Iterator0_tD3CB0D761CDCD91380D2D419E8225BE7B15F1E53::get_offset_of_U24this_0(),
	U3CCameraOnU3Ec__Iterator0_tD3CB0D761CDCD91380D2D419E8225BE7B15F1E53::get_offset_of_U24current_1(),
	U3CCameraOnU3Ec__Iterator0_tD3CB0D761CDCD91380D2D419E8225BE7B15F1E53::get_offset_of_U24disposing_2(),
	U3CCameraOnU3Ec__Iterator0_tD3CB0D761CDCD91380D2D419E8225BE7B15F1E53::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2842 = { sizeof (U3CCheckedCameraU3Ec__Iterator1_t066B15E9843DCAA7D8813EF8FBB746E3A00CA918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2842[4] = 
{
	U3CCheckedCameraU3Ec__Iterator1_t066B15E9843DCAA7D8813EF8FBB746E3A00CA918::get_offset_of_U24this_0(),
	U3CCheckedCameraU3Ec__Iterator1_t066B15E9843DCAA7D8813EF8FBB746E3A00CA918::get_offset_of_U24current_1(),
	U3CCheckedCameraU3Ec__Iterator1_t066B15E9843DCAA7D8813EF8FBB746E3A00CA918::get_offset_of_U24disposing_2(),
	U3CCheckedCameraU3Ec__Iterator1_t066B15E9843DCAA7D8813EF8FBB746E3A00CA918::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2843 = { sizeof (CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874), -1, sizeof(CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2843[15] = 
{
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874_StaticFields::get_offset_of_mInstance_4(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_CardImgs_5(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_mCurrentCardCounter_6(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_mEmptyCardImg_7(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_mCardLetterPanel_8(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_mLetterPrefab_9(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_mFinishCardImage_10(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_mWrongCardImage_11(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_mCardPanel_12(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_BDSSpeech_13(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_mSpeechInDelay_14(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_mSpeechDelayTime_15(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_mLoadLetterDone_16(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_mLoadCardDone_17(),
	CardManager_tCE6BDECC89C25A83B489E6478FD5F61998349874::get_offset_of_HintedCardPosition_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2844 = { sizeof (U3CSpeechDelayU3Ec__Iterator0_t46456C049571B0F488EA7AA99B84EB797615E4FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2844[4] = 
{
	U3CSpeechDelayU3Ec__Iterator0_t46456C049571B0F488EA7AA99B84EB797615E4FB::get_offset_of_U24this_0(),
	U3CSpeechDelayU3Ec__Iterator0_t46456C049571B0F488EA7AA99B84EB797615E4FB::get_offset_of_U24current_1(),
	U3CSpeechDelayU3Ec__Iterator0_t46456C049571B0F488EA7AA99B84EB797615E4FB::get_offset_of_U24disposing_2(),
	U3CSpeechDelayU3Ec__Iterator0_t46456C049571B0F488EA7AA99B84EB797615E4FB::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2845 = { sizeof (U3CCheckCardPanelDoneU3Ec__Iterator1_tD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2845[4] = 
{
	U3CCheckCardPanelDoneU3Ec__Iterator1_tD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA::get_offset_of_U24this_0(),
	U3CCheckCardPanelDoneU3Ec__Iterator1_tD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA::get_offset_of_U24current_1(),
	U3CCheckCardPanelDoneU3Ec__Iterator1_tD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA::get_offset_of_U24disposing_2(),
	U3CCheckCardPanelDoneU3Ec__Iterator1_tD33009F3E5FC7DDF9EED2B36A224A37F931AD8EA::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2846 = { sizeof (CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2846[10] = 
{
	CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82::get_offset_of_mTime_4(),
	CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82::get_offset_of_mReward_5(),
	CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82::get_offset_of_mMissionName_6(),
	CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82::get_offset_of_mCollectText_7(),
	CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82::get_offset_of_mCompleteCardLayout_8(),
	CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82::get_offset_of_mRewardCardPanel_9(),
	CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82::get_offset_of_mCompleteCard_10(),
	CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82::get_offset_of_mCompleteCardRect_11(),
	CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82::get_offset_of_mCompleteStar_12(),
	CompleteImageSetter_t93D129BA78346C5652563D4BAA8CAC3F255E2C82::get_offset_of_mCompletePic_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2847 = { sizeof (DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9), -1, sizeof(DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2847[3] = 
{
	DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9_StaticFields::get_offset_of_instance_4(),
	DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9::get_offset_of_originalObjs_5(),
	DetectLeaks_tA86D73B553AC571DC63632E443A4797B48CE76B9::get_offset_of_isFirstSet_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2848 = { sizeof (DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2848[4] = 
{
	DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13::get_offset_of_mCardImage_4(),
	DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13::get_offset_of_mCardTransform_5(),
	DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13::get_offset_of_mCardText_6(),
	DownloadImage_t18130CA1223AEF7B4DC12A624C828FEA1E6DDF13::get_offset_of_CardInstantiate_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2849 = { sizeof (FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365), -1, sizeof(FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2849[7] = 
{
	FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365::get_offset_of_mWord_4(),
	FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365::get_offset_of_mIncreaceSize_5(),
	FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365::get_offset_of_ImgColor_6(),
	FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365::get_offset_of_mStartParent_7(),
	FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365_StaticFields::get_offset_of_ItemBeginDrag_8(),
	FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365::get_offset_of_mOriginalPosition_9(),
	FillInWord_t3D157EE5A803EAC523500F3475D142B55D29E365::get_offset_of_originalscale_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2850 = { sizeof (GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965), -1, sizeof(GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2850[5] = 
{
	GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965_StaticFields::get_offset_of_mInsatnce_4(),
	GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965::get_offset_of_ThePlayerData_5(),
	GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965::get_offset_of_mShowRewardCard_6(),
	GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965::get_offset_of_mRewardCardImg_7(),
	GameManage_t2EF28DD9376BDE665CDFFA3DF4087BA68DA83965::get_offset_of_mRewardCardText_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2851 = { sizeof (U3CStartGameU3Ec__Iterator0_t80F411FED94268ED4351291DE5D0EBC0BB64413D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2851[4] = 
{
	U3CStartGameU3Ec__Iterator0_t80F411FED94268ED4351291DE5D0EBC0BB64413D::get_offset_of_U24this_0(),
	U3CStartGameU3Ec__Iterator0_t80F411FED94268ED4351291DE5D0EBC0BB64413D::get_offset_of_U24current_1(),
	U3CStartGameU3Ec__Iterator0_t80F411FED94268ED4351291DE5D0EBC0BB64413D::get_offset_of_U24disposing_2(),
	U3CStartGameU3Ec__Iterator0_t80F411FED94268ED4351291DE5D0EBC0BB64413D::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2852 = { sizeof (gyrocontrol_tED692BF5B2DB536AAAFCFB695D38F4F6826807D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2852[4] = 
{
	gyrocontrol_tED692BF5B2DB536AAAFCFB695D38F4F6826807D0::get_offset_of_gyroEnable_4(),
	gyrocontrol_tED692BF5B2DB536AAAFCFB695D38F4F6826807D0::get_offset_of_gyro_5(),
	gyrocontrol_tED692BF5B2DB536AAAFCFB695D38F4F6826807D0::get_offset_of_cameraContainer_6(),
	gyrocontrol_tED692BF5B2DB536AAAFCFB695D38F4F6826807D0::get_offset_of_rot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2853 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2854 = { sizeof (InstanciatateObject_tFEE44005F6AE21D6C4674392F2C9BBB70F5214CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2855 = { sizeof (ObjectLoadMove_t3BE2A335AE97C6B40CAB3E2CDD0639B7924110CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2856 = { sizeof (LetterController_tB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2856[5] = 
{
	LetterController_tB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D::get_offset_of_mLetter_4(),
	LetterController_tB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D::get_offset_of_mLetterAmountText_5(),
	LetterController_tB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D::get_offset_of_Letter_6(),
	LetterController_tB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D::get_offset_of_mLetterAmount_7(),
	LetterController_tB2E2B1C92B79A31FCA9F2F0E3B5F9C00C7A4CE3D::get_offset_of_mSerilize_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2857 = { sizeof (LetterCubeController_tA8459A17CE4FDE57AE4BA3E75B435030F794AD7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2857[3] = 
{
	LetterCubeController_tA8459A17CE4FDE57AE4BA3E75B435030F794AD7E::get_offset_of_mLetter_4(),
	LetterCubeController_tA8459A17CE4FDE57AE4BA3E75B435030F794AD7E::get_offset_of_texts_5(),
	LetterCubeController_tA8459A17CE4FDE57AE4BA3E75B435030F794AD7E::get_offset_of_dicemesh_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2858 = { sizeof (Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2858[3] = 
{
	Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478::get_offset_of_mTextDic_0(),
	Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478::get_offset_of_LetterControllerDic_1(),
	Letters_t0A8693D7BA7607D25A2F070A9FE97809097A4478::get_offset_of_LetterArray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2859 = { sizeof (LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2859[11] = 
{
	LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52::get_offset_of_mNameText_4(),
	LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52::get_offset_of_mMoneyText_5(),
	LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52::get_offset_of_mRankText_6(),
	LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52::get_offset_of_mRankImage_7(),
	LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52::get_offset_of_mOtherRankImage_8(),
	LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52::get_offset_of_mRankLevelImg_9(),
	LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52::get_offset_of_mMyTransform_10(),
	LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52::get_offset_of_mRank_11(),
	LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52::get_offset_of_mNode_12(),
	LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52::get_offset_of_mLoadOnAwake_13(),
	LoadRank_tDD4D3E731ADB4004FA20646E8EF5225C6CDD6D52::get_offset_of_mRankMode_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2860 = { sizeof (RankMode_t80C951E8018D8B56A7CCC31715B45773E7E8F0A6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2860[3] = 
{
	RankMode_t80C951E8018D8B56A7CCC31715B45773E7E8F0A6::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2861 = { sizeof (LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A), -1, sizeof(LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2861[11] = 
{
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A::get_offset_of_mAccount_4(),
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A::get_offset_of_mPassword_5(),
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A::get_offset_of_KeepLoginImage_6(),
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A::get_offset_of_mWrongIDText_7(),
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A::get_offset_of_mBigDipperLogo_8(),
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A::get_offset_of_mShowLogoSpeed_9(),
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A::get_offset_of_mShowLogoTime_10(),
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A::get_offset_of_mFadeLogoSpeed_11(),
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A::get_offset_of_mLoginPanel_12(),
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A_StaticFields::get_offset_of_mInstance_13(),
	LoginUI_t9275CF57F5DE81344789D50B0345F66BEEDA581A::get_offset_of_mLoadLoginData_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2862 = { sizeof (U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2862[10] = 
{
	U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228::get_offset_of_U3CformU3E__0_0(),
	U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228::get_offset_of_U3CJSonU3E__0_1(),
	U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228::get_offset_of_U3CPlatformU3E__0_2(),
	U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228::get_offset_of_U3CLoginWWWU3E__1_3(),
	U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228::get_offset_of_IsLogin_4(),
	U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228::get_offset_of_U3CResultU3E__2_5(),
	U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228::get_offset_of_U24this_6(),
	U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228::get_offset_of_U24current_7(),
	U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228::get_offset_of_U24disposing_8(),
	U3CLoginU3Ec__Iterator0_t349BFA2D6021C456F066AD617610D02F6793C228::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2863 = { sizeof (U3CShowLogoU3Ec__Iterator1_t09D132CBFC300F1D382716F620F65347F2CC168B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2863[4] = 
{
	U3CShowLogoU3Ec__Iterator1_t09D132CBFC300F1D382716F620F65347F2CC168B::get_offset_of_U24this_0(),
	U3CShowLogoU3Ec__Iterator1_t09D132CBFC300F1D382716F620F65347F2CC168B::get_offset_of_U24current_1(),
	U3CShowLogoU3Ec__Iterator1_t09D132CBFC300F1D382716F620F65347F2CC168B::get_offset_of_U24disposing_2(),
	U3CShowLogoU3Ec__Iterator1_t09D132CBFC300F1D382716F620F65347F2CC168B::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2864 = { sizeof (U3CFadeLogoU3Ec__Iterator2_t8CCA0AFFF49E100E0505CECFEBFB840382A3471D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2864[4] = 
{
	U3CFadeLogoU3Ec__Iterator2_t8CCA0AFFF49E100E0505CECFEBFB840382A3471D::get_offset_of_U24this_0(),
	U3CFadeLogoU3Ec__Iterator2_t8CCA0AFFF49E100E0505CECFEBFB840382A3471D::get_offset_of_U24current_1(),
	U3CFadeLogoU3Ec__Iterator2_t8CCA0AFFF49E100E0505CECFEBFB840382A3471D::get_offset_of_U24disposing_2(),
	U3CFadeLogoU3Ec__Iterator2_t8CCA0AFFF49E100E0505CECFEBFB840382A3471D::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2865 = { sizeof (MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2865[10] = 
{
	MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9::get_offset_of_mMissionNumber_4(),
	MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9::get_offset_of_mMissionName_5(),
	MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9::get_offset_of_IsDone_6(),
	MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9::get_offset_of_IsDo_7(),
	MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9::get_offset_of_mMissionNameText_8(),
	MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9::get_offset_of_mStar_9(),
	MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9::get_offset_of_mStarImage_10(),
	MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9::get_offset_of_mStarGrayImage_11(),
	MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9::get_offset_of_mIsCompleteImage_12(),
	MissionButtonData_t7E0CDDA77413DA2F9E8D3CF7BD2EA0470F9486E9::get_offset_of_mCompleteImage_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2866 = { sizeof (MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2866[4] = 
{
	MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C::get_offset_of_mLetters_0(),
	MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C::get_offset_of_MissionNumber_1(),
	MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C::get_offset_of_MissionName_2(),
	MissionData_t1ADCB3A063F40B4F86E1703F2D808C6F8E1FDA7C::get_offset_of_IsDo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2867 = { sizeof (MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394), -1, sizeof(MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2867[49] = 
{
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mCurrentMissionMode_4(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mMissionCanvasPanel_5(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mPassageAnswerText_6(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mHintCardFieldPanel_7(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mHintCardPanel_8(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mAnsCardObj_9(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mAnsFieldObject_10(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mMissionPanel_11(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mMissionButtonPanel_12(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mSelectPanel_13(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mBeaconPanel_14(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_BeaconCollectedUIPanel_15(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mBeaconCamera_16(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mCompletePanel_17(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_memoPic_18(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mUnCompletePanel_19(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mMissionPanelDic_20(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mPanelButtonDic_21(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_beaconDic_22(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_beaconUUIDList_23(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_beaconContents_24(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394_StaticFields::get_offset_of_mInstance_25(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_TemporerLetter_26(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_CurrentMissionData_27(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_Wordings_28(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mPassageCardAnswerText_29(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mHintCardAnswerText_30(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mCurrentWording_31(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mAnsOrder_32(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mAnsField_33(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mSpentTime_34(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mStartTime_35(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mChecker_36(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mMostStarBefore_37(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_IsUploading_38(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mCardFileID_39(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_time_40(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_timeReward_41(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mTime3RewardOver_42(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_gestureDetector_43(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_gestureCount_44(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_gestureLength_45(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_gestureSum_46(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_numOfCircleToShow_47(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_saveletterDone_48(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_SaveAnswerDone_49(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_CompleteSound_50(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_CompleteMissionSource_51(),
	MissionManager_tA3C9C2F5BA139B63D123DCCEAA628F44F224C394::get_offset_of_mAnsOrderFilled_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2868 = { sizeof (IMissionMode_t76B792A5875AF2771F97F4A8EE3B22120268E9BC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2868[4] = 
{
	IMissionMode_t76B792A5875AF2771F97F4A8EE3B22120268E9BC::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2869 = { sizeof (U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2869[9] = 
{
	U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF::get_offset_of_U3CformU3E__0_0(),
	U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF::get_offset_of_U3CJSonU3E__0_1(),
	U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF::get_offset_of_U3CmyDateU3E__0_2(),
	U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF::get_offset_of_U3CmyDateStringU3E__0_3(),
	U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF::get_offset_of_U3CLoginWWWU3E__1_4(),
	U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF::get_offset_of_U24this_5(),
	U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF::get_offset_of_U24current_6(),
	U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF::get_offset_of_U24disposing_7(),
	U3CupLoadStartTimeU3Ec__Iterator0_tAB175BA8DBD21FD4D8A8FEF01992A049F1D438CF::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2870 = { sizeof (U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2870[8] = 
{
	U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741::get_offset_of_U3CformU3E__0_0(),
	U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741::get_offset_of_U3CJSonU3E__0_1(),
	U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741::get_offset_of_missionNumber_2(),
	U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741::get_offset_of_U3CLoginWWWU3E__1_3(),
	U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741::get_offset_of_U24this_4(),
	U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741::get_offset_of_U24current_5(),
	U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741::get_offset_of_U24disposing_6(),
	U3CloadRecordsU3Ec__Iterator1_t500DDD544F074F70A1AA1A273122A43EEB810741::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2871 = { sizeof (U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2871[8] = 
{
	U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439::get_offset_of_U3CformU3E__0_0(),
	U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439::get_offset_of_U3CJSonU3E__0_1(),
	U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439::get_offset_of_missionNumber_2(),
	U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439::get_offset_of_U3CLoginWWWU3E__1_3(),
	U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439::get_offset_of_U24this_4(),
	U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439::get_offset_of_U24current_5(),
	U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439::get_offset_of_U24disposing_6(),
	U3CloadLetterU3Ec__Iterator2_t619B6D2D59AC5FC2EBF1674EDBFA0305DBC30439::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2872 = { sizeof (U3CCheckCanCloseMissionU3Ec__Iterator3_t8A7FE58733D9CC1269B70A63BAED63ED5A9E335A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2872[4] = 
{
	U3CCheckCanCloseMissionU3Ec__Iterator3_t8A7FE58733D9CC1269B70A63BAED63ED5A9E335A::get_offset_of_U24this_0(),
	U3CCheckCanCloseMissionU3Ec__Iterator3_t8A7FE58733D9CC1269B70A63BAED63ED5A9E335A::get_offset_of_U24current_1(),
	U3CCheckCanCloseMissionU3Ec__Iterator3_t8A7FE58733D9CC1269B70A63BAED63ED5A9E335A::get_offset_of_U24disposing_2(),
	U3CCheckCanCloseMissionU3Ec__Iterator3_t8A7FE58733D9CC1269B70A63BAED63ED5A9E335A::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2873 = { sizeof (U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2873[12] = 
{
	U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A::get_offset_of_U3CformU3E__0_0(),
	U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A::get_offset_of_U3CJSonU3E__0_1(),
	U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A::get_offset_of_U3CmyDateU3E__0_2(),
	U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A::get_offset_of_U3CmyDateStringU3E__0_3(),
	U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A::get_offset_of_U3CapmU3E__0_4(),
	U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A::get_offset_of_U3CfinishtimeU3E__0_5(),
	U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A::get_offset_of_U3CsaveCardIDU3E__0_6(),
	U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A::get_offset_of_U3CLoginWWWU3E__1_7(),
	U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A::get_offset_of_U24this_8(),
	U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A::get_offset_of_U24current_9(),
	U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A::get_offset_of_U24disposing_10(),
	U3CsubmitU3Ec__Iterator4_tC7520A7504F8F908B591A16517F699C17B01D61A::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2874 = { sizeof (U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2874[8] = 
{
	U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09::get_offset_of_U3CformU3E__0_0(),
	U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09::get_offset_of_U3CJSonU3E__0_1(),
	U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09::get_offset_of_starAmount_2(),
	U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09::get_offset_of_U3CLoginWWWU3E__1_3(),
	U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09::get_offset_of_U24this_4(),
	U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09::get_offset_of_U24current_5(),
	U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09::get_offset_of_U24disposing_6(),
	U3CupLoadMostStarU3Ec__Iterator5_tCBA890EAFDC750477935DD4EC4DCCAB4F2997C09::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2875 = { sizeof (U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2875[10] = 
{
	U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6::get_offset_of_U3CformU3E__0_0(),
	U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6::get_offset_of_U3CJSonU3E__0_1(),
	U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6::get_offset_of_U3CchiLetterStringU3E__0_2(),
	U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6::get_offset_of_U3CchiLetterNumStringU3E__0_3(),
	U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6::get_offset_of_U3ClettersU3E__0_4(),
	U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6::get_offset_of_U3CLoginWWWU3E__1_5(),
	U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6::get_offset_of_U24this_6(),
	U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6::get_offset_of_U24current_7(),
	U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6::get_offset_of_U24disposing_8(),
	U3CsaveLettersU3Ec__Iterator6_t7E8EE3E0FC2ABF524FDECD50187207DF18FC7FA6::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2876 = { sizeof (U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2876[10] = 
{
	U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661::get_offset_of_U3CformU3E__0_0(),
	U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661::get_offset_of_U3CJSonU3E__0_1(),
	U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661::get_offset_of_U3CsavewordingU3E__0_2(),
	U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661::get_offset_of_U3CplayerOrderU3E__0_3(),
	U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661::get_offset_of_U3CenableListU3E__0_4(),
	U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661::get_offset_of_U3CLoginWWWU3E__1_5(),
	U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661::get_offset_of_U24this_6(),
	U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661::get_offset_of_U24current_7(),
	U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661::get_offset_of_U24disposing_8(),
	U3CSaveAnswerU3Ec__Iterator7_t5C8AA9E1B3FE76AD060C7342CBAA29507BE86661::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2877 = { sizeof (U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2877[9] = 
{
	U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7::get_offset_of_U3CrequestU3E__1_0(),
	U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7::get_offset_of_url_1(),
	U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7::get_offset_of_U3CasyncOpU3E__2_2(),
	U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7::get_offset_of_count_3(),
	U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7::get_offset_of_CardLoadDone_4(),
	U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7::get_offset_of_U24this_5(),
	U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7::get_offset_of_U24current_6(),
	U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7::get_offset_of_U24disposing_7(),
	U3CdownloadMissionImgU3Ec__Iterator8_t19E450FC9CD970A8BDA2AC97F0ED28CFAF5681E7::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2878 = { sizeof (U3CPrepareSerilizeImgU3Ec__Iterator9_t97394C2D06F5B89CC74FF4973765C20482922A5F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2878[5] = 
{
	U3CPrepareSerilizeImgU3Ec__Iterator9_t97394C2D06F5B89CC74FF4973765C20482922A5F::get_offset_of_U3CcheckcardImgU3E__0_0(),
	U3CPrepareSerilizeImgU3Ec__Iterator9_t97394C2D06F5B89CC74FF4973765C20482922A5F::get_offset_of_U24this_1(),
	U3CPrepareSerilizeImgU3Ec__Iterator9_t97394C2D06F5B89CC74FF4973765C20482922A5F::get_offset_of_U24current_2(),
	U3CPrepareSerilizeImgU3Ec__Iterator9_t97394C2D06F5B89CC74FF4973765C20482922A5F::get_offset_of_U24disposing_3(),
	U3CPrepareSerilizeImgU3Ec__Iterator9_t97394C2D06F5B89CC74FF4973765C20482922A5F::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2879 = { sizeof (U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2879[7] = 
{
	U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66::get_offset_of_U3CformU3E__0_0(),
	U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66::get_offset_of_U3CJSonU3E__0_1(),
	U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66::get_offset_of_U3CLoginWWWU3E__1_2(),
	U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66::get_offset_of_U24this_3(),
	U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66::get_offset_of_U24current_4(),
	U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66::get_offset_of_U24disposing_5(),
	U3CreadChiBeaconDataU3Ec__IteratorA_t106ABC44CA1E6B98B4E8BE70687C42D1052A3A66::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2880 = { sizeof (MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2880[4] = 
{
	MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A::get_offset_of_mScore_4(),
	MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A::get_offset_of_mMissionName_5(),
	MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A::get_offset_of_mSpentTime_6(),
	MissionRecordButtonData_t1A8F175F8C67F136FAA77DB0268B27C8158E974A::get_offset_of_mStarImage_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2881 = { sizeof (MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579), -1, sizeof(MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2881[7] = 
{
	MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579_StaticFields::get_offset_of_Instance_4(),
	MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579::get_offset_of_mHomePanel_5(),
	MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579::get_offset_of_mUnCompletePanel_6(),
	MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579::get_offset_of_example_7(),
	MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579::get_offset_of_InstructionImage_8(),
	MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579::get_offset_of_currentInstuctImg_9(),
	MissionUI_t82A18D2C7CBB423DC3A123916F334231B7F4B579::get_offset_of_MainCam_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2882 = { sizeof (MyVar_tFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA), -1, sizeof(MyVar_tFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2882[4] = 
{
	MyVar_tFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_StaticFields::get_offset_of_Server_0(),
	MyVar_tFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_StaticFields::get_offset_of_PHPURL_1(),
	MyVar_tFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_StaticFields::get_offset_of_playerID_2(),
	MyVar_tFA3DE4F0330CDFEDAB0921541B85CBCDAA5257FA_StaticFields::get_offset_of_version_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2883 = { sizeof (ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34), -1, sizeof(ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2883[6] = 
{
	ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34_StaticFields::get_offset_of_mInsatnce_4(),
	ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34::get_offset_of_mObjectPool_5(),
	ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34::get_offset_of_mObjectDic_6(),
	ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34::get_offset_of_mObjectInGame_7(),
	ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34::get_offset_of_mSystemType_8(),
	ObjectPool_t6972C6DD97FC383C5A6CFB727EAE1C2389375A34::get_offset_of_mFactoryStore_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2884 = { sizeof (eSystemType_t549D39F80314FE96CDC5305DAD54AB1320FD37DE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2884[2] = 
{
	eSystemType_t549D39F80314FE96CDC5305DAD54AB1320FD37DE::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2885 = { sizeof (ObjectStore_tB7EE0D6CBA7CBBBA830724C5792B309AF079DEF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2885[1] = 
{
	ObjectStore_tB7EE0D6CBA7CBBBA830724C5792B309AF079DEF4::get_offset_of_mFactory_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2886 = { sizeof (SimpleObjectFactory_tAAB121CEB0D927DEA868CFB01C52A50A1C7C7132), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2887 = { sizeof (PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2887[11] = 
{
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_SaveAccount_0(),
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_SavePassword_1(),
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_PlayerID_2(),
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_Coins_3(),
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_WealthTracerRank_4(),
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_WealthTracerMoney_5(),
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_BestTracerRank_6(),
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_BestTracerMoney_7(),
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_finishedMission_8(),
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_Name_9(),
	PlayerData_t884B4586F555C513CFA2CC9AE975F75C25538043::get_offset_of_SchoolID_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2888 = { sizeof (PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97), -1, sizeof(PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2888[50] = 
{
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97_StaticFields::get_offset_of_mInstance_4(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mCardResult_5(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mCardUrlCount_6(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mCardNameCount_7(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mCardDownloadCount_8(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mCollectionComplete_9(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mRankingComplete_10(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mLoadPlayerDataComplete_11(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mRankResult_12(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mRankCount_13(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mBestTracerRankCount_14(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_GameCanvasPanel_15(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mPlayerMode_16(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mPanel_17(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mPanelButton_18(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mMissionButton_19(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mMissionButtonData_20(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mResultButton_21(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_recordData_22(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mMissionContent_23(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mMissionRecordContent_24(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mMissionView_25(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mMissionRecordView_26(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mMissionButtonOn_27(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mMissionButtonOff_28(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mMissionRecordButtonOn_29(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mMissionRecordButtonOff_30(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mPanelDic_31(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mPanelButtonDic_32(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mCollectionContent_33(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mRecordLayout_34(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mBestTracerRankContent_35(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mWealthTracerRankContent_36(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mPlayerRank_37(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_EnumImage_38(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mBestTracerView_39(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mWealthTracerView_40(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mBestTracerButtonOn_41(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mBestTracerButtonOff_42(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mWealthTracerButtonOn_43(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_mWealthTracerButtonOff_44(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_Money_45(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_Name_46(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_CollectLayout_47(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_BestTracerLayoutGroup_48(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_WealthTracerLayoutGroup_49(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_MissionLayoutGroup_50(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_CollectionContentORect_51(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_SetBestTracer_52(),
	PlayerMode_tC98090D1841F0D9D319F822FF716CA599235AC97::get_offset_of_SetWealthTracer_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2889 = { sizeof (IPlayerMode_tD4C65A0449F8AA69EAA5391678F3112100B82465)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2889[5] = 
{
	IPlayerMode_tD4C65A0449F8AA69EAA5391678F3112100B82465::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2890 = { sizeof (U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2890[7] = 
{
	U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470::get_offset_of_U3CformU3E__0_0(),
	U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470::get_offset_of_U3CJSonU3E__0_1(),
	U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470::get_offset_of_U3CsendWWWU3E__1_2(),
	U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470::get_offset_of_U24this_3(),
	U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470::get_offset_of_U24current_4(),
	U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470::get_offset_of_U24disposing_5(),
	U3CLoadCardsU3Ec__Iterator0_t7B75848B88C4DA25E037088C3EA4E73607E43470::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2891 = { sizeof (U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2891[9] = 
{
	U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901::get_offset_of_U3CcardscountU3E__0_0(),
	U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901::get_offset_of_cards_1(),
	U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901::get_offset_of_U24locvar0_2(),
	U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901::get_offset_of_U24locvar1_3(),
	U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901::get_offset_of_U3CimgU3E__1_4(),
	U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901::get_offset_of_U24this_5(),
	U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901::get_offset_of_U24current_6(),
	U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901::get_offset_of_U24disposing_7(),
	U3CCheckCardDownloadU3Ec__Iterator1_t9304B57662736604BA15C457E16630715C7FB901::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2892 = { sizeof (U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2892[9] = 
{
	U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A::get_offset_of_cardurl_0(),
	U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A::get_offset_of_U3CcardURLU3E__0_1(),
	U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A::get_offset_of_U3CpathU3E__0_2(),
	U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A::get_offset_of_U3CwwwU3E__1_3(),
	U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A::get_offset_of_img_4(),
	U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A::get_offset_of_cardname_5(),
	U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A::get_offset_of_U24current_6(),
	U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A::get_offset_of_U24disposing_7(),
	U3CDownloadCollectionImgU3Ec__Iterator2_t6E2FD3738D2ECD6D50348CD4C6460A705015558A::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2893 = { sizeof (U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2893[7] = 
{
	U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582::get_offset_of_U3CformU3E__0_0(),
	U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582::get_offset_of_U3CJSonU3E__0_1(),
	U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582::get_offset_of_U3CsendWWWU3E__1_2(),
	U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582::get_offset_of_U24this_3(),
	U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582::get_offset_of_U24current_4(),
	U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582::get_offset_of_U24disposing_5(),
	U3CLoadMissionListU3Ec__Iterator3_t59CA83C041FF782FC9649342B1BACA25F9756582::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2894 = { sizeof (U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2894[7] = 
{
	U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72::get_offset_of_U3CformU3E__0_0(),
	U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72::get_offset_of_U3CJSonU3E__0_1(),
	U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72::get_offset_of_U3CsendWWWU3E__1_2(),
	U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72::get_offset_of_U24this_3(),
	U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72::get_offset_of_U24current_4(),
	U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72::get_offset_of_U24disposing_5(),
	U3CLoadFinishRecordU3Ec__Iterator4_tA98BA2EB1C824115C3665B7534F27A060A0A5E72::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2895 = { sizeof (U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2895[7] = 
{
	U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728::get_offset_of_U3CformU3E__0_0(),
	U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728::get_offset_of_U3CJSonU3E__0_1(),
	U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728::get_offset_of_U3CsendWWWU3E__1_2(),
	U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728::get_offset_of_U24this_3(),
	U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728::get_offset_of_U24current_4(),
	U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728::get_offset_of_U24disposing_5(),
	U3CloadRankingU3Ec__Iterator5_t557E3D6752E1A099E53195859D49EDBE10801728::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2896 = { sizeof (U3CCheckLoadingU3Ec__Iterator6_tDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2896[5] = 
{
	U3CCheckLoadingU3Ec__Iterator6_tDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E::get_offset_of_U3CcounterU3E__0_0(),
	U3CCheckLoadingU3Ec__Iterator6_tDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E::get_offset_of_U24this_1(),
	U3CCheckLoadingU3Ec__Iterator6_tDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E::get_offset_of_U24current_2(),
	U3CCheckLoadingU3Ec__Iterator6_tDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E::get_offset_of_U24disposing_3(),
	U3CCheckLoadingU3Ec__Iterator6_tDA81C67BA05D6E2E82FB87D2D8F4EAAE91F5081E::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2897 = { sizeof (ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365), -1, sizeof(ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2897[3] = 
{
	ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365::get_offset_of_mSystemType_4(),
	ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365_StaticFields::get_offset_of_mInstance_5(),
	ResourceLoad_tE941FD3ED629D1E9DDFE937A95BEEDFAE79D1365::get_offset_of_mFactoryStore_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2898 = { sizeof (eSystemType_t2A5B48C62B3C5047427A89463E9FCE282C926C29)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2898[2] = 
{
	eSystemType_t2A5B48C62B3C5047427A89463E9FCE282C926C29::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2899 = { sizeof (RsourceStores_t7DF0BF48D6C2C9A7496994994A0A55517E309385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2899[1] = 
{
	RsourceStores_t7DF0BF48D6C2C9A7496994994A0A55517E309385::get_offset_of_mFactory_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif

﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mono.Xml.DTDAnyAutomata
struct DTDAnyAutomata_t7D15632FA965F7D26DBF45EAA3D3BA11B59E7A06;
// Mono.Xml.DTDAttListDeclarationCollection
struct DTDAttListDeclarationCollection_tCDEF4AACF366E8B2298CAB3070235D432B854F55;
// Mono.Xml.DTDAutomata
struct DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7;
// Mono.Xml.DTDAutomataFactory
struct DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340;
// Mono.Xml.DTDContentModel
struct DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF;
// Mono.Xml.DTDContentModelCollection
struct DTDContentModelCollection_t0382EF04D32FF6333D71752F3C4E9CC1EE5B2664;
// Mono.Xml.DTDElementAutomata
struct DTDElementAutomata_t4F31CD3B4096E7EA16D973937D76EB96F23B1BD2;
// Mono.Xml.DTDElementDeclarationCollection
struct DTDElementDeclarationCollection_t45A928E2967593285196499A173437F6AB3AE89D;
// Mono.Xml.DTDEmptyAutomata
struct DTDEmptyAutomata_t055216A06878AB4743EF9FC55703460B24117B41;
// Mono.Xml.DTDEntityDeclarationCollection
struct DTDEntityDeclarationCollection_t94CA8138E40F051734624B32D031268F326F1995;
// Mono.Xml.DTDInvalidAutomata
struct DTDInvalidAutomata_tDF68F8C0E639715110419CEA22B09F5F30CCE0DB;
// Mono.Xml.DTDNode
struct DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B;
// Mono.Xml.DTDNotationDeclarationCollection
struct DTDNotationDeclarationCollection_tF5A146D5CFBA10C2C7B82FDCD76FA19A301C936C;
// Mono.Xml.DTDObjectModel
struct DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E;
// Mono.Xml.DTDParameterEntityDeclarationCollection
struct DTDParameterEntityDeclarationCollection_tA4A3C5F34157BEF4F00F640C63D94D48ABA0773E;
// Mono.Xml.DTDValidatingReader
struct DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8;
// Mono.Xml.DTDValidatingReader/AttributeSlot[]
struct AttributeSlotU5BU5D_t87B6D6037EDB3BC424B29D613C23C7D57290C3B3;
// Mono.Xml.DictionaryBase
struct DictionaryBase_t415336F093F8CDFAF278AA55A0F4A0FD46780582;
// Mono.Xml.EntityResolvingXmlReader
struct EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F;
// Mono.Xml.IHasXmlSchemaInfo
struct IHasXmlSchemaInfo_tA3B2C478D5F5102CB79A687B2F03ED13BC529461;
// Mono.Xml2.XmlTextReader
struct XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD;
// Mono.Xml2.XmlTextReader/DtdInputStateStack
struct DtdInputStateStack_tD410944C4A9AE89256F67B1E85F27375A0ABA100;
// Mono.Xml2.XmlTextReader/TagName[]
struct TagNameU5BU5D_tDC731B271CA71CDD2CCAA81DD046275A56A60BAE;
// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo
struct XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2;
// Mono.Xml2.XmlTextReader/XmlAttributeTokenInfo[]
struct XmlAttributeTokenInfoU5BU5D_t775B2DAE0BCFA2CA1F941E56B78921A90F82EDF5;
// Mono.Xml2.XmlTextReader/XmlTokenInfo
struct XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8;
// Mono.Xml2.XmlTextReader/XmlTokenInfo[]
struct XmlTokenInfoU5BU5D_tAC00CA31D3E381773C0EA56FCAF3FFA3AD87546E;
// System.AsyncCallback
struct AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2;
// System.Byte[]
struct ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.ArrayList
struct ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1;
// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>[]
struct KeyValuePair_2U5BU5D_t4624FBF72088DBEBCC8176D700E55896784955C5;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct List_1_t341C4DA0AC3E19C291DAB1B40650E946D9B6728B;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t36D6DE3CB7FEC6EDF6EA749E72AD210F334D8F45;
// System.Collections.Hashtable
struct Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82;
// System.Collections.IDictionary
struct IDictionary_tD35B9437F08BE98D1E0B295CC73C48E168CAB316;
// System.Collections.IEnumerator
struct IEnumerator_t5F4AD85C6EA424A50584F741049EA645DBD8EEFC;
// System.Collections.Stack
struct Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5;
// System.DelegateData
struct DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9;
// System.IAsyncResult
struct IAsyncResult_tDA33C24465239FB383C4C2CDAAC43B9AD3CB7F05;
// System.IO.TextReader
struct TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520;
// System.IntPtr[]
struct IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659;
// System.Net.ICredentials
struct ICredentials_tD0AB83297BDF32EE92C413F575F62DD096386B58;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B;
// System.Text.Encoding
struct Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type[]
struct TypeU5BU5D_tEF927658123F6CD4274B971442504A42AB6DE532;
// System.UInt32[]
struct UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5;
// System.Uri
struct Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;
// System.Xml.IHasXmlChildNode
struct IHasXmlChildNode_t9DA3954D4DB002114DCAD19E8AC559502B9D11A1;
// System.Xml.NameTable
struct NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6;
// System.Xml.NameTable/Entry[]
struct EntryU5BU5D_t97618C7433A838405FA69D1932F53790DB93AEAE;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_tA1795EB62BAF4B2A85998D2E227BBE15EDF6DFE7;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_t532C46B1FC9D97F471412FAC2FFCD7F7A93E3513;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D;
// System.Xml.XmlAttributeCollection
struct XmlAttributeCollection_t0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9;
// System.Xml.XmlDocument
struct XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763;
// System.Xml.XmlElement
struct XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9;
// System.Xml.XmlImplementation
struct XmlImplementation_t941F4E3C7A3128487AD76CEEA212EDCC5484E64B;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23;
// System.Xml.XmlNameEntry
struct XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768;
// System.Xml.XmlNameEntryCache
struct XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B;
// System.Xml.XmlNameTable
struct XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B;
// System.Xml.XmlNamedNodeMap
struct XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86;
// System.Xml.XmlNamespaceManager/NsDecl[]
struct NsDeclU5BU5D_tBDD6A9B70A78098C87EE5FC7BBC47AEC966170DC;
// System.Xml.XmlNamespaceManager/NsScope[]
struct NsScopeU5BU5D_t27760A59C722CCA943743D29B23D0A1BDC0E226F;
// System.Xml.XmlNode
struct XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A;
// System.Xml.XmlNode/EmptyNodeList
struct EmptyNodeList_tABF5FD9F0A8DC7FBC7808B3BACC780547EB48735;
// System.Xml.XmlNodeChangedEventHandler
struct XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC;
// System.Xml.XmlNodeListChildren
struct XmlNodeListChildren_t0DFA6642EB0E0CA24CB3F5AF8DC38D09D5656B2A;
// System.Xml.XmlNodeReaderImpl
struct XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54;
// System.Xml.XmlParserContext
struct XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14;
// System.Xml.XmlParserInput
struct XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82;
// System.Xml.XmlReader
struct XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A;
// System.Xml.XmlReaderBinarySupport/CharGetter
struct CharGetter_t41F2B070679160162013D7D2B99DF5A72133136B;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB;
// System.Xml.XmlResolver
struct XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C;
// System.Xml.XmlTextReader
struct XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3;
// System.Xml.XmlValidatingReader
struct XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DTDAUTOMATA_T831152C2924A60A794E81D920F5504585B94BFE7_H
#define DTDAUTOMATA_T831152C2924A60A794E81D920F5504585B94BFE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAutomata
struct  DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDAutomata::root
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * ___root_0;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7, ___root_0)); }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDAUTOMATA_T831152C2924A60A794E81D920F5504585B94BFE7_H
#ifndef DTDCONTENTMODELCOLLECTION_T0382EF04D32FF6333D71752F3C4E9CC1EE5B2664_H
#define DTDCONTENTMODELCOLLECTION_T0382EF04D32FF6333D71752F3C4E9CC1EE5B2664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentModelCollection
struct  DTDContentModelCollection_t0382EF04D32FF6333D71752F3C4E9CC1EE5B2664  : public RuntimeObject
{
public:
	// System.Collections.ArrayList Mono.Xml.DTDContentModelCollection::contentModel
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___contentModel_0;

public:
	inline static int32_t get_offset_of_contentModel_0() { return static_cast<int32_t>(offsetof(DTDContentModelCollection_t0382EF04D32FF6333D71752F3C4E9CC1EE5B2664, ___contentModel_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_contentModel_0() const { return ___contentModel_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_contentModel_0() { return &___contentModel_0; }
	inline void set_contentModel_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___contentModel_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTMODELCOLLECTION_T0382EF04D32FF6333D71752F3C4E9CC1EE5B2664_H
#ifndef DTDNODE_T204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B_H
#define DTDNODE_T204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNode
struct  DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B  : public RuntimeObject
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDNode::root
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * ___root_0;
	// System.Boolean Mono.Xml.DTDNode::isInternalSubset
	bool ___isInternalSubset_1;
	// System.String Mono.Xml.DTDNode::baseURI
	String_t* ___baseURI_2;
	// System.Int32 Mono.Xml.DTDNode::lineNumber
	int32_t ___lineNumber_3;
	// System.Int32 Mono.Xml.DTDNode::linePosition
	int32_t ___linePosition_4;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B, ___root_0)); }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * get_root_0() const { return ___root_0; }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier((&___root_0), value);
	}

	inline static int32_t get_offset_of_isInternalSubset_1() { return static_cast<int32_t>(offsetof(DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B, ___isInternalSubset_1)); }
	inline bool get_isInternalSubset_1() const { return ___isInternalSubset_1; }
	inline bool* get_address_of_isInternalSubset_1() { return &___isInternalSubset_1; }
	inline void set_isInternalSubset_1(bool value)
	{
		___isInternalSubset_1 = value;
	}

	inline static int32_t get_offset_of_baseURI_2() { return static_cast<int32_t>(offsetof(DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B, ___baseURI_2)); }
	inline String_t* get_baseURI_2() const { return ___baseURI_2; }
	inline String_t** get_address_of_baseURI_2() { return &___baseURI_2; }
	inline void set_baseURI_2(String_t* value)
	{
		___baseURI_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_2), value);
	}

	inline static int32_t get_offset_of_lineNumber_3() { return static_cast<int32_t>(offsetof(DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B, ___lineNumber_3)); }
	inline int32_t get_lineNumber_3() const { return ___lineNumber_3; }
	inline int32_t* get_address_of_lineNumber_3() { return &___lineNumber_3; }
	inline void set_lineNumber_3(int32_t value)
	{
		___lineNumber_3 = value;
	}

	inline static int32_t get_offset_of_linePosition_4() { return static_cast<int32_t>(offsetof(DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B, ___linePosition_4)); }
	inline int32_t get_linePosition_4() const { return ___linePosition_4; }
	inline int32_t* get_address_of_linePosition_4() { return &___linePosition_4; }
	inline void set_linePosition_4(int32_t value)
	{
		___linePosition_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNODE_T204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B_H
#ifndef DTDOBJECTMODEL_T1F8566CF41C2964FF7D10C0F00255A1A2EA1682E_H
#define DTDOBJECTMODEL_T1F8566CF41C2964FF7D10C0F00255A1A2EA1682E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDObjectModel
struct  DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E  : public RuntimeObject
{
public:
	// Mono.Xml.DTDAutomataFactory Mono.Xml.DTDObjectModel::factory
	DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340 * ___factory_0;
	// Mono.Xml.DTDElementAutomata Mono.Xml.DTDObjectModel::rootAutomata
	DTDElementAutomata_t4F31CD3B4096E7EA16D973937D76EB96F23B1BD2 * ___rootAutomata_1;
	// Mono.Xml.DTDEmptyAutomata Mono.Xml.DTDObjectModel::emptyAutomata
	DTDEmptyAutomata_t055216A06878AB4743EF9FC55703460B24117B41 * ___emptyAutomata_2;
	// Mono.Xml.DTDAnyAutomata Mono.Xml.DTDObjectModel::anyAutomata
	DTDAnyAutomata_t7D15632FA965F7D26DBF45EAA3D3BA11B59E7A06 * ___anyAutomata_3;
	// Mono.Xml.DTDInvalidAutomata Mono.Xml.DTDObjectModel::invalidAutomata
	DTDInvalidAutomata_tDF68F8C0E639715110419CEA22B09F5F30CCE0DB * ___invalidAutomata_4;
	// Mono.Xml.DTDElementDeclarationCollection Mono.Xml.DTDObjectModel::elementDecls
	DTDElementDeclarationCollection_t45A928E2967593285196499A173437F6AB3AE89D * ___elementDecls_5;
	// Mono.Xml.DTDAttListDeclarationCollection Mono.Xml.DTDObjectModel::attListDecls
	DTDAttListDeclarationCollection_tCDEF4AACF366E8B2298CAB3070235D432B854F55 * ___attListDecls_6;
	// Mono.Xml.DTDParameterEntityDeclarationCollection Mono.Xml.DTDObjectModel::peDecls
	DTDParameterEntityDeclarationCollection_tA4A3C5F34157BEF4F00F640C63D94D48ABA0773E * ___peDecls_7;
	// Mono.Xml.DTDEntityDeclarationCollection Mono.Xml.DTDObjectModel::entityDecls
	DTDEntityDeclarationCollection_t94CA8138E40F051734624B32D031268F326F1995 * ___entityDecls_8;
	// Mono.Xml.DTDNotationDeclarationCollection Mono.Xml.DTDObjectModel::notationDecls
	DTDNotationDeclarationCollection_tF5A146D5CFBA10C2C7B82FDCD76FA19A301C936C * ___notationDecls_9;
	// System.Collections.ArrayList Mono.Xml.DTDObjectModel::validationErrors
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___validationErrors_10;
	// System.Xml.XmlResolver Mono.Xml.DTDObjectModel::resolver
	XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * ___resolver_11;
	// System.Xml.XmlNameTable Mono.Xml.DTDObjectModel::nameTable
	XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * ___nameTable_12;
	// System.Collections.Hashtable Mono.Xml.DTDObjectModel::externalResources
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___externalResources_13;
	// System.String Mono.Xml.DTDObjectModel::baseURI
	String_t* ___baseURI_14;
	// System.String Mono.Xml.DTDObjectModel::name
	String_t* ___name_15;
	// System.String Mono.Xml.DTDObjectModel::publicId
	String_t* ___publicId_16;
	// System.String Mono.Xml.DTDObjectModel::systemId
	String_t* ___systemId_17;
	// System.String Mono.Xml.DTDObjectModel::intSubset
	String_t* ___intSubset_18;
	// System.Boolean Mono.Xml.DTDObjectModel::intSubsetHasPERef
	bool ___intSubsetHasPERef_19;
	// System.Boolean Mono.Xml.DTDObjectModel::isStandalone
	bool ___isStandalone_20;
	// System.Int32 Mono.Xml.DTDObjectModel::lineNumber
	int32_t ___lineNumber_21;
	// System.Int32 Mono.Xml.DTDObjectModel::linePosition
	int32_t ___linePosition_22;

public:
	inline static int32_t get_offset_of_factory_0() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___factory_0)); }
	inline DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340 * get_factory_0() const { return ___factory_0; }
	inline DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340 ** get_address_of_factory_0() { return &___factory_0; }
	inline void set_factory_0(DTDAutomataFactory_t8081E28AEC61216CB0502E9C8584C09D7A13F340 * value)
	{
		___factory_0 = value;
		Il2CppCodeGenWriteBarrier((&___factory_0), value);
	}

	inline static int32_t get_offset_of_rootAutomata_1() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___rootAutomata_1)); }
	inline DTDElementAutomata_t4F31CD3B4096E7EA16D973937D76EB96F23B1BD2 * get_rootAutomata_1() const { return ___rootAutomata_1; }
	inline DTDElementAutomata_t4F31CD3B4096E7EA16D973937D76EB96F23B1BD2 ** get_address_of_rootAutomata_1() { return &___rootAutomata_1; }
	inline void set_rootAutomata_1(DTDElementAutomata_t4F31CD3B4096E7EA16D973937D76EB96F23B1BD2 * value)
	{
		___rootAutomata_1 = value;
		Il2CppCodeGenWriteBarrier((&___rootAutomata_1), value);
	}

	inline static int32_t get_offset_of_emptyAutomata_2() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___emptyAutomata_2)); }
	inline DTDEmptyAutomata_t055216A06878AB4743EF9FC55703460B24117B41 * get_emptyAutomata_2() const { return ___emptyAutomata_2; }
	inline DTDEmptyAutomata_t055216A06878AB4743EF9FC55703460B24117B41 ** get_address_of_emptyAutomata_2() { return &___emptyAutomata_2; }
	inline void set_emptyAutomata_2(DTDEmptyAutomata_t055216A06878AB4743EF9FC55703460B24117B41 * value)
	{
		___emptyAutomata_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptyAutomata_2), value);
	}

	inline static int32_t get_offset_of_anyAutomata_3() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___anyAutomata_3)); }
	inline DTDAnyAutomata_t7D15632FA965F7D26DBF45EAA3D3BA11B59E7A06 * get_anyAutomata_3() const { return ___anyAutomata_3; }
	inline DTDAnyAutomata_t7D15632FA965F7D26DBF45EAA3D3BA11B59E7A06 ** get_address_of_anyAutomata_3() { return &___anyAutomata_3; }
	inline void set_anyAutomata_3(DTDAnyAutomata_t7D15632FA965F7D26DBF45EAA3D3BA11B59E7A06 * value)
	{
		___anyAutomata_3 = value;
		Il2CppCodeGenWriteBarrier((&___anyAutomata_3), value);
	}

	inline static int32_t get_offset_of_invalidAutomata_4() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___invalidAutomata_4)); }
	inline DTDInvalidAutomata_tDF68F8C0E639715110419CEA22B09F5F30CCE0DB * get_invalidAutomata_4() const { return ___invalidAutomata_4; }
	inline DTDInvalidAutomata_tDF68F8C0E639715110419CEA22B09F5F30CCE0DB ** get_address_of_invalidAutomata_4() { return &___invalidAutomata_4; }
	inline void set_invalidAutomata_4(DTDInvalidAutomata_tDF68F8C0E639715110419CEA22B09F5F30CCE0DB * value)
	{
		___invalidAutomata_4 = value;
		Il2CppCodeGenWriteBarrier((&___invalidAutomata_4), value);
	}

	inline static int32_t get_offset_of_elementDecls_5() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___elementDecls_5)); }
	inline DTDElementDeclarationCollection_t45A928E2967593285196499A173437F6AB3AE89D * get_elementDecls_5() const { return ___elementDecls_5; }
	inline DTDElementDeclarationCollection_t45A928E2967593285196499A173437F6AB3AE89D ** get_address_of_elementDecls_5() { return &___elementDecls_5; }
	inline void set_elementDecls_5(DTDElementDeclarationCollection_t45A928E2967593285196499A173437F6AB3AE89D * value)
	{
		___elementDecls_5 = value;
		Il2CppCodeGenWriteBarrier((&___elementDecls_5), value);
	}

	inline static int32_t get_offset_of_attListDecls_6() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___attListDecls_6)); }
	inline DTDAttListDeclarationCollection_tCDEF4AACF366E8B2298CAB3070235D432B854F55 * get_attListDecls_6() const { return ___attListDecls_6; }
	inline DTDAttListDeclarationCollection_tCDEF4AACF366E8B2298CAB3070235D432B854F55 ** get_address_of_attListDecls_6() { return &___attListDecls_6; }
	inline void set_attListDecls_6(DTDAttListDeclarationCollection_tCDEF4AACF366E8B2298CAB3070235D432B854F55 * value)
	{
		___attListDecls_6 = value;
		Il2CppCodeGenWriteBarrier((&___attListDecls_6), value);
	}

	inline static int32_t get_offset_of_peDecls_7() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___peDecls_7)); }
	inline DTDParameterEntityDeclarationCollection_tA4A3C5F34157BEF4F00F640C63D94D48ABA0773E * get_peDecls_7() const { return ___peDecls_7; }
	inline DTDParameterEntityDeclarationCollection_tA4A3C5F34157BEF4F00F640C63D94D48ABA0773E ** get_address_of_peDecls_7() { return &___peDecls_7; }
	inline void set_peDecls_7(DTDParameterEntityDeclarationCollection_tA4A3C5F34157BEF4F00F640C63D94D48ABA0773E * value)
	{
		___peDecls_7 = value;
		Il2CppCodeGenWriteBarrier((&___peDecls_7), value);
	}

	inline static int32_t get_offset_of_entityDecls_8() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___entityDecls_8)); }
	inline DTDEntityDeclarationCollection_t94CA8138E40F051734624B32D031268F326F1995 * get_entityDecls_8() const { return ___entityDecls_8; }
	inline DTDEntityDeclarationCollection_t94CA8138E40F051734624B32D031268F326F1995 ** get_address_of_entityDecls_8() { return &___entityDecls_8; }
	inline void set_entityDecls_8(DTDEntityDeclarationCollection_t94CA8138E40F051734624B32D031268F326F1995 * value)
	{
		___entityDecls_8 = value;
		Il2CppCodeGenWriteBarrier((&___entityDecls_8), value);
	}

	inline static int32_t get_offset_of_notationDecls_9() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___notationDecls_9)); }
	inline DTDNotationDeclarationCollection_tF5A146D5CFBA10C2C7B82FDCD76FA19A301C936C * get_notationDecls_9() const { return ___notationDecls_9; }
	inline DTDNotationDeclarationCollection_tF5A146D5CFBA10C2C7B82FDCD76FA19A301C936C ** get_address_of_notationDecls_9() { return &___notationDecls_9; }
	inline void set_notationDecls_9(DTDNotationDeclarationCollection_tF5A146D5CFBA10C2C7B82FDCD76FA19A301C936C * value)
	{
		___notationDecls_9 = value;
		Il2CppCodeGenWriteBarrier((&___notationDecls_9), value);
	}

	inline static int32_t get_offset_of_validationErrors_10() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___validationErrors_10)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_validationErrors_10() const { return ___validationErrors_10; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_validationErrors_10() { return &___validationErrors_10; }
	inline void set_validationErrors_10(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___validationErrors_10 = value;
		Il2CppCodeGenWriteBarrier((&___validationErrors_10), value);
	}

	inline static int32_t get_offset_of_resolver_11() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___resolver_11)); }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * get_resolver_11() const { return ___resolver_11; }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C ** get_address_of_resolver_11() { return &___resolver_11; }
	inline void set_resolver_11(XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * value)
	{
		___resolver_11 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_11), value);
	}

	inline static int32_t get_offset_of_nameTable_12() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___nameTable_12)); }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * get_nameTable_12() const { return ___nameTable_12; }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B ** get_address_of_nameTable_12() { return &___nameTable_12; }
	inline void set_nameTable_12(XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * value)
	{
		___nameTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_12), value);
	}

	inline static int32_t get_offset_of_externalResources_13() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___externalResources_13)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_externalResources_13() const { return ___externalResources_13; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_externalResources_13() { return &___externalResources_13; }
	inline void set_externalResources_13(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___externalResources_13 = value;
		Il2CppCodeGenWriteBarrier((&___externalResources_13), value);
	}

	inline static int32_t get_offset_of_baseURI_14() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___baseURI_14)); }
	inline String_t* get_baseURI_14() const { return ___baseURI_14; }
	inline String_t** get_address_of_baseURI_14() { return &___baseURI_14; }
	inline void set_baseURI_14(String_t* value)
	{
		___baseURI_14 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_14), value);
	}

	inline static int32_t get_offset_of_name_15() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___name_15)); }
	inline String_t* get_name_15() const { return ___name_15; }
	inline String_t** get_address_of_name_15() { return &___name_15; }
	inline void set_name_15(String_t* value)
	{
		___name_15 = value;
		Il2CppCodeGenWriteBarrier((&___name_15), value);
	}

	inline static int32_t get_offset_of_publicId_16() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___publicId_16)); }
	inline String_t* get_publicId_16() const { return ___publicId_16; }
	inline String_t** get_address_of_publicId_16() { return &___publicId_16; }
	inline void set_publicId_16(String_t* value)
	{
		___publicId_16 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_16), value);
	}

	inline static int32_t get_offset_of_systemId_17() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___systemId_17)); }
	inline String_t* get_systemId_17() const { return ___systemId_17; }
	inline String_t** get_address_of_systemId_17() { return &___systemId_17; }
	inline void set_systemId_17(String_t* value)
	{
		___systemId_17 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_17), value);
	}

	inline static int32_t get_offset_of_intSubset_18() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___intSubset_18)); }
	inline String_t* get_intSubset_18() const { return ___intSubset_18; }
	inline String_t** get_address_of_intSubset_18() { return &___intSubset_18; }
	inline void set_intSubset_18(String_t* value)
	{
		___intSubset_18 = value;
		Il2CppCodeGenWriteBarrier((&___intSubset_18), value);
	}

	inline static int32_t get_offset_of_intSubsetHasPERef_19() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___intSubsetHasPERef_19)); }
	inline bool get_intSubsetHasPERef_19() const { return ___intSubsetHasPERef_19; }
	inline bool* get_address_of_intSubsetHasPERef_19() { return &___intSubsetHasPERef_19; }
	inline void set_intSubsetHasPERef_19(bool value)
	{
		___intSubsetHasPERef_19 = value;
	}

	inline static int32_t get_offset_of_isStandalone_20() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___isStandalone_20)); }
	inline bool get_isStandalone_20() const { return ___isStandalone_20; }
	inline bool* get_address_of_isStandalone_20() { return &___isStandalone_20; }
	inline void set_isStandalone_20(bool value)
	{
		___isStandalone_20 = value;
	}

	inline static int32_t get_offset_of_lineNumber_21() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___lineNumber_21)); }
	inline int32_t get_lineNumber_21() const { return ___lineNumber_21; }
	inline int32_t* get_address_of_lineNumber_21() { return &___lineNumber_21; }
	inline void set_lineNumber_21(int32_t value)
	{
		___lineNumber_21 = value;
	}

	inline static int32_t get_offset_of_linePosition_22() { return static_cast<int32_t>(offsetof(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E, ___linePosition_22)); }
	inline int32_t get_linePosition_22() const { return ___linePosition_22; }
	inline int32_t* get_address_of_linePosition_22() { return &___linePosition_22; }
	inline void set_linePosition_22(int32_t value)
	{
		___linePosition_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDOBJECTMODEL_T1F8566CF41C2964FF7D10C0F00255A1A2EA1682E_H
#ifndef DTDPARAMETERENTITYDECLARATIONCOLLECTION_TA4A3C5F34157BEF4F00F640C63D94D48ABA0773E_H
#define DTDPARAMETERENTITYDECLARATIONCOLLECTION_TA4A3C5F34157BEF4F00F640C63D94D48ABA0773E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDParameterEntityDeclarationCollection
struct  DTDParameterEntityDeclarationCollection_tA4A3C5F34157BEF4F00F640C63D94D48ABA0773E  : public RuntimeObject
{
public:
	// System.Collections.Hashtable Mono.Xml.DTDParameterEntityDeclarationCollection::peDecls
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___peDecls_0;
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDParameterEntityDeclarationCollection::root
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * ___root_1;

public:
	inline static int32_t get_offset_of_peDecls_0() { return static_cast<int32_t>(offsetof(DTDParameterEntityDeclarationCollection_tA4A3C5F34157BEF4F00F640C63D94D48ABA0773E, ___peDecls_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_peDecls_0() const { return ___peDecls_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_peDecls_0() { return &___peDecls_0; }
	inline void set_peDecls_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___peDecls_0 = value;
		Il2CppCodeGenWriteBarrier((&___peDecls_0), value);
	}

	inline static int32_t get_offset_of_root_1() { return static_cast<int32_t>(offsetof(DTDParameterEntityDeclarationCollection_tA4A3C5F34157BEF4F00F640C63D94D48ABA0773E, ___root_1)); }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * get_root_1() const { return ___root_1; }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E ** get_address_of_root_1() { return &___root_1; }
	inline void set_root_1(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * value)
	{
		___root_1 = value;
		Il2CppCodeGenWriteBarrier((&___root_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARAMETERENTITYDECLARATIONCOLLECTION_TA4A3C5F34157BEF4F00F640C63D94D48ABA0773E_H
#ifndef ATTRIBUTESLOT_T08F6F94BF6512EF49DD57A33043840256A01E438_H
#define ATTRIBUTESLOT_T08F6F94BF6512EF49DD57A33043840256A01E438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDValidatingReader_AttributeSlot
struct  AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438  : public RuntimeObject
{
public:
	// System.String Mono.Xml.DTDValidatingReader_AttributeSlot::Name
	String_t* ___Name_0;
	// System.String Mono.Xml.DTDValidatingReader_AttributeSlot::LocalName
	String_t* ___LocalName_1;
	// System.String Mono.Xml.DTDValidatingReader_AttributeSlot::NS
	String_t* ___NS_2;
	// System.String Mono.Xml.DTDValidatingReader_AttributeSlot::Prefix
	String_t* ___Prefix_3;
	// System.String Mono.Xml.DTDValidatingReader_AttributeSlot::Value
	String_t* ___Value_4;
	// System.Boolean Mono.Xml.DTDValidatingReader_AttributeSlot::IsDefault
	bool ___IsDefault_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_NS_2() { return static_cast<int32_t>(offsetof(AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438, ___NS_2)); }
	inline String_t* get_NS_2() const { return ___NS_2; }
	inline String_t** get_address_of_NS_2() { return &___NS_2; }
	inline void set_NS_2(String_t* value)
	{
		___NS_2 = value;
		Il2CppCodeGenWriteBarrier((&___NS_2), value);
	}

	inline static int32_t get_offset_of_Prefix_3() { return static_cast<int32_t>(offsetof(AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438, ___Prefix_3)); }
	inline String_t* get_Prefix_3() const { return ___Prefix_3; }
	inline String_t** get_address_of_Prefix_3() { return &___Prefix_3; }
	inline void set_Prefix_3(String_t* value)
	{
		___Prefix_3 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_3), value);
	}

	inline static int32_t get_offset_of_Value_4() { return static_cast<int32_t>(offsetof(AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438, ___Value_4)); }
	inline String_t* get_Value_4() const { return ___Value_4; }
	inline String_t** get_address_of_Value_4() { return &___Value_4; }
	inline void set_Value_4(String_t* value)
	{
		___Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___Value_4), value);
	}

	inline static int32_t get_offset_of_IsDefault_5() { return static_cast<int32_t>(offsetof(AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438, ___IsDefault_5)); }
	inline bool get_IsDefault_5() const { return ___IsDefault_5; }
	inline bool* get_address_of_IsDefault_5() { return &___IsDefault_5; }
	inline void set_IsDefault_5(bool value)
	{
		___IsDefault_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTESLOT_T08F6F94BF6512EF49DD57A33043840256A01E438_H
#ifndef DTDINPUTSTATESTACK_TD410944C4A9AE89256F67B1E85F27375A0ABA100_H
#define DTDINPUTSTATESTACK_TD410944C4A9AE89256F67B1E85F27375A0ABA100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader_DtdInputStateStack
struct  DtdInputStateStack_tD410944C4A9AE89256F67B1E85F27375A0ABA100  : public RuntimeObject
{
public:
	// System.Collections.Stack Mono.Xml2.XmlTextReader_DtdInputStateStack::intern
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___intern_0;

public:
	inline static int32_t get_offset_of_intern_0() { return static_cast<int32_t>(offsetof(DtdInputStateStack_tD410944C4A9AE89256F67B1E85F27375A0ABA100, ___intern_0)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_intern_0() const { return ___intern_0; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_intern_0() { return &___intern_0; }
	inline void set_intern_0(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___intern_0 = value;
		Il2CppCodeGenWriteBarrier((&___intern_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDINPUTSTATESTACK_TD410944C4A9AE89256F67B1E85F27375A0ABA100_H
#ifndef LIST_1_T341C4DA0AC3E19C291DAB1B40650E946D9B6728B_H
#define LIST_1_T341C4DA0AC3E19C291DAB1B40650E946D9B6728B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct  List_1_t341C4DA0AC3E19C291DAB1B40650E946D9B6728B  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t4624FBF72088DBEBCC8176D700E55896784955C5* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t341C4DA0AC3E19C291DAB1B40650E946D9B6728B, ____items_1)); }
	inline KeyValuePair_2U5BU5D_t4624FBF72088DBEBCC8176D700E55896784955C5* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_t4624FBF72088DBEBCC8176D700E55896784955C5** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_t4624FBF72088DBEBCC8176D700E55896784955C5* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t341C4DA0AC3E19C291DAB1B40650E946D9B6728B, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t341C4DA0AC3E19C291DAB1B40650E946D9B6728B, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t341C4DA0AC3E19C291DAB1B40650E946D9B6728B_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	KeyValuePair_2U5BU5D_t4624FBF72088DBEBCC8176D700E55896784955C5* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t341C4DA0AC3E19C291DAB1B40650E946D9B6728B_StaticFields, ___EmptyArray_4)); }
	inline KeyValuePair_2U5BU5D_t4624FBF72088DBEBCC8176D700E55896784955C5* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline KeyValuePair_2U5BU5D_t4624FBF72088DBEBCC8176D700E55896784955C5** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(KeyValuePair_2U5BU5D_t4624FBF72088DBEBCC8176D700E55896784955C5* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T341C4DA0AC3E19C291DAB1B40650E946D9B6728B_H
#ifndef EVENTARGS_TA4C15C1D2AB4B139169B1942C1477933E00DCA17_H
#define EVENTARGS_TA4C15C1D2AB4B139169B1942C1477933E00DCA17_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17  : public RuntimeObject
{
public:

public:
};

struct EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17_StaticFields, ___Empty_0)); }
	inline EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_TA4C15C1D2AB4B139169B1942C1477933E00DCA17_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef DTDREADER_T03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B_H
#define DTDREADER_T03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DTDReader
struct  DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B  : public RuntimeObject
{
public:
	// System.Xml.XmlParserInput System.Xml.DTDReader::currentInput
	XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82 * ___currentInput_0;
	// System.Collections.Stack System.Xml.DTDReader::parserInputStack
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___parserInputStack_1;
	// System.Char[] System.Xml.DTDReader::nameBuffer
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___nameBuffer_2;
	// System.Int32 System.Xml.DTDReader::nameLength
	int32_t ___nameLength_3;
	// System.Int32 System.Xml.DTDReader::nameCapacity
	int32_t ___nameCapacity_4;
	// System.Text.StringBuilder System.Xml.DTDReader::valueBuffer
	StringBuilder_t * ___valueBuffer_5;
	// System.Int32 System.Xml.DTDReader::currentLinkedNodeLineNumber
	int32_t ___currentLinkedNodeLineNumber_6;
	// System.Int32 System.Xml.DTDReader::currentLinkedNodeLinePosition
	int32_t ___currentLinkedNodeLinePosition_7;
	// System.Int32 System.Xml.DTDReader::dtdIncludeSect
	int32_t ___dtdIncludeSect_8;
	// System.Boolean System.Xml.DTDReader::normalization
	bool ___normalization_9;
	// System.Boolean System.Xml.DTDReader::processingInternalSubset
	bool ___processingInternalSubset_10;
	// System.String System.Xml.DTDReader::cachedPublicId
	String_t* ___cachedPublicId_11;
	// System.String System.Xml.DTDReader::cachedSystemId
	String_t* ___cachedSystemId_12;
	// Mono.Xml.DTDObjectModel System.Xml.DTDReader::DTD
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * ___DTD_13;

public:
	inline static int32_t get_offset_of_currentInput_0() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___currentInput_0)); }
	inline XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82 * get_currentInput_0() const { return ___currentInput_0; }
	inline XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82 ** get_address_of_currentInput_0() { return &___currentInput_0; }
	inline void set_currentInput_0(XmlParserInput_t61A048BD6169B3A13F2876710997B12CF93C5F82 * value)
	{
		___currentInput_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentInput_0), value);
	}

	inline static int32_t get_offset_of_parserInputStack_1() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___parserInputStack_1)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_parserInputStack_1() const { return ___parserInputStack_1; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_parserInputStack_1() { return &___parserInputStack_1; }
	inline void set_parserInputStack_1(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___parserInputStack_1 = value;
		Il2CppCodeGenWriteBarrier((&___parserInputStack_1), value);
	}

	inline static int32_t get_offset_of_nameBuffer_2() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___nameBuffer_2)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_nameBuffer_2() const { return ___nameBuffer_2; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_nameBuffer_2() { return &___nameBuffer_2; }
	inline void set_nameBuffer_2(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___nameBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameBuffer_2), value);
	}

	inline static int32_t get_offset_of_nameLength_3() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___nameLength_3)); }
	inline int32_t get_nameLength_3() const { return ___nameLength_3; }
	inline int32_t* get_address_of_nameLength_3() { return &___nameLength_3; }
	inline void set_nameLength_3(int32_t value)
	{
		___nameLength_3 = value;
	}

	inline static int32_t get_offset_of_nameCapacity_4() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___nameCapacity_4)); }
	inline int32_t get_nameCapacity_4() const { return ___nameCapacity_4; }
	inline int32_t* get_address_of_nameCapacity_4() { return &___nameCapacity_4; }
	inline void set_nameCapacity_4(int32_t value)
	{
		___nameCapacity_4 = value;
	}

	inline static int32_t get_offset_of_valueBuffer_5() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___valueBuffer_5)); }
	inline StringBuilder_t * get_valueBuffer_5() const { return ___valueBuffer_5; }
	inline StringBuilder_t ** get_address_of_valueBuffer_5() { return &___valueBuffer_5; }
	inline void set_valueBuffer_5(StringBuilder_t * value)
	{
		___valueBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___valueBuffer_5), value);
	}

	inline static int32_t get_offset_of_currentLinkedNodeLineNumber_6() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___currentLinkedNodeLineNumber_6)); }
	inline int32_t get_currentLinkedNodeLineNumber_6() const { return ___currentLinkedNodeLineNumber_6; }
	inline int32_t* get_address_of_currentLinkedNodeLineNumber_6() { return &___currentLinkedNodeLineNumber_6; }
	inline void set_currentLinkedNodeLineNumber_6(int32_t value)
	{
		___currentLinkedNodeLineNumber_6 = value;
	}

	inline static int32_t get_offset_of_currentLinkedNodeLinePosition_7() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___currentLinkedNodeLinePosition_7)); }
	inline int32_t get_currentLinkedNodeLinePosition_7() const { return ___currentLinkedNodeLinePosition_7; }
	inline int32_t* get_address_of_currentLinkedNodeLinePosition_7() { return &___currentLinkedNodeLinePosition_7; }
	inline void set_currentLinkedNodeLinePosition_7(int32_t value)
	{
		___currentLinkedNodeLinePosition_7 = value;
	}

	inline static int32_t get_offset_of_dtdIncludeSect_8() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___dtdIncludeSect_8)); }
	inline int32_t get_dtdIncludeSect_8() const { return ___dtdIncludeSect_8; }
	inline int32_t* get_address_of_dtdIncludeSect_8() { return &___dtdIncludeSect_8; }
	inline void set_dtdIncludeSect_8(int32_t value)
	{
		___dtdIncludeSect_8 = value;
	}

	inline static int32_t get_offset_of_normalization_9() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___normalization_9)); }
	inline bool get_normalization_9() const { return ___normalization_9; }
	inline bool* get_address_of_normalization_9() { return &___normalization_9; }
	inline void set_normalization_9(bool value)
	{
		___normalization_9 = value;
	}

	inline static int32_t get_offset_of_processingInternalSubset_10() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___processingInternalSubset_10)); }
	inline bool get_processingInternalSubset_10() const { return ___processingInternalSubset_10; }
	inline bool* get_address_of_processingInternalSubset_10() { return &___processingInternalSubset_10; }
	inline void set_processingInternalSubset_10(bool value)
	{
		___processingInternalSubset_10 = value;
	}

	inline static int32_t get_offset_of_cachedPublicId_11() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___cachedPublicId_11)); }
	inline String_t* get_cachedPublicId_11() const { return ___cachedPublicId_11; }
	inline String_t** get_address_of_cachedPublicId_11() { return &___cachedPublicId_11; }
	inline void set_cachedPublicId_11(String_t* value)
	{
		___cachedPublicId_11 = value;
		Il2CppCodeGenWriteBarrier((&___cachedPublicId_11), value);
	}

	inline static int32_t get_offset_of_cachedSystemId_12() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___cachedSystemId_12)); }
	inline String_t* get_cachedSystemId_12() const { return ___cachedSystemId_12; }
	inline String_t** get_address_of_cachedSystemId_12() { return &___cachedSystemId_12; }
	inline void set_cachedSystemId_12(String_t* value)
	{
		___cachedSystemId_12 = value;
		Il2CppCodeGenWriteBarrier((&___cachedSystemId_12), value);
	}

	inline static int32_t get_offset_of_DTD_13() { return static_cast<int32_t>(offsetof(DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B, ___DTD_13)); }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * get_DTD_13() const { return ___DTD_13; }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E ** get_address_of_DTD_13() { return &___DTD_13; }
	inline void set_DTD_13(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * value)
	{
		___DTD_13 = value;
		Il2CppCodeGenWriteBarrier((&___DTD_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDREADER_T03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B_H
#ifndef ENTRY_TB2500B931CE6347D59272793DA70C3264BF4CDFA_H
#define ENTRY_TB2500B931CE6347D59272793DA70C3264BF4CDFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable_Entry
struct  Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA  : public RuntimeObject
{
public:
	// System.String System.Xml.NameTable_Entry::str
	String_t* ___str_0;
	// System.Int32 System.Xml.NameTable_Entry::hash
	int32_t ___hash_1;
	// System.Int32 System.Xml.NameTable_Entry::len
	int32_t ___len_2;
	// System.Xml.NameTable_Entry System.Xml.NameTable_Entry::next
	Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA * ___next_3;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}

	inline static int32_t get_offset_of_hash_1() { return static_cast<int32_t>(offsetof(Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA, ___hash_1)); }
	inline int32_t get_hash_1() const { return ___hash_1; }
	inline int32_t* get_address_of_hash_1() { return &___hash_1; }
	inline void set_hash_1(int32_t value)
	{
		___hash_1 = value;
	}

	inline static int32_t get_offset_of_len_2() { return static_cast<int32_t>(offsetof(Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA, ___len_2)); }
	inline int32_t get_len_2() const { return ___len_2; }
	inline int32_t* get_address_of_len_2() { return &___len_2; }
	inline void set_len_2(int32_t value)
	{
		___len_2 = value;
	}

	inline static int32_t get_offset_of_next_3() { return static_cast<int32_t>(offsetof(Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA, ___next_3)); }
	inline Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA * get_next_3() const { return ___next_3; }
	inline Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA ** get_address_of_next_3() { return &___next_3; }
	inline void set_next_3(Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA * value)
	{
		___next_3 = value;
		Il2CppCodeGenWriteBarrier((&___next_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_TB2500B931CE6347D59272793DA70C3264BF4CDFA_H
#ifndef XMLCHAR_T987897166A4938284071DFD58FC4594BAB1128A5_H
#define XMLCHAR_T987897166A4938284071DFD58FC4594BAB1128A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlChar
struct  XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5  : public RuntimeObject
{
public:

public:
};

struct XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5_StaticFields
{
public:
	// System.Char[] System.Xml.XmlChar::WhitespaceChars
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___WhitespaceChars_0;
	// System.Byte[] System.Xml.XmlChar::firstNamePages
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___firstNamePages_1;
	// System.Byte[] System.Xml.XmlChar::namePages
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___namePages_2;
	// System.UInt32[] System.Xml.XmlChar::nameBitmap
	UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* ___nameBitmap_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlChar::<>f__switchU24map2F
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map2F_4;

public:
	inline static int32_t get_offset_of_WhitespaceChars_0() { return static_cast<int32_t>(offsetof(XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5_StaticFields, ___WhitespaceChars_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_WhitespaceChars_0() const { return ___WhitespaceChars_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_WhitespaceChars_0() { return &___WhitespaceChars_0; }
	inline void set_WhitespaceChars_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___WhitespaceChars_0 = value;
		Il2CppCodeGenWriteBarrier((&___WhitespaceChars_0), value);
	}

	inline static int32_t get_offset_of_firstNamePages_1() { return static_cast<int32_t>(offsetof(XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5_StaticFields, ___firstNamePages_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_firstNamePages_1() const { return ___firstNamePages_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_firstNamePages_1() { return &___firstNamePages_1; }
	inline void set_firstNamePages_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___firstNamePages_1 = value;
		Il2CppCodeGenWriteBarrier((&___firstNamePages_1), value);
	}

	inline static int32_t get_offset_of_namePages_2() { return static_cast<int32_t>(offsetof(XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5_StaticFields, ___namePages_2)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_namePages_2() const { return ___namePages_2; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_namePages_2() { return &___namePages_2; }
	inline void set_namePages_2(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___namePages_2 = value;
		Il2CppCodeGenWriteBarrier((&___namePages_2), value);
	}

	inline static int32_t get_offset_of_nameBitmap_3() { return static_cast<int32_t>(offsetof(XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5_StaticFields, ___nameBitmap_3)); }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* get_nameBitmap_3() const { return ___nameBitmap_3; }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5** get_address_of_nameBitmap_3() { return &___nameBitmap_3; }
	inline void set_nameBitmap_3(UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* value)
	{
		___nameBitmap_3 = value;
		Il2CppCodeGenWriteBarrier((&___nameBitmap_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2F_4() { return static_cast<int32_t>(offsetof(XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5_StaticFields, ___U3CU3Ef__switchU24map2F_4)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map2F_4() const { return ___U3CU3Ef__switchU24map2F_4; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map2F_4() { return &___U3CU3Ef__switchU24map2F_4; }
	inline void set_U3CU3Ef__switchU24map2F_4(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map2F_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2F_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHAR_T987897166A4938284071DFD58FC4594BAB1128A5_H
#ifndef XMLIMPLEMENTATION_T941F4E3C7A3128487AD76CEEA212EDCC5484E64B_H
#define XMLIMPLEMENTATION_T941F4E3C7A3128487AD76CEEA212EDCC5484E64B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlImplementation
struct  XmlImplementation_t941F4E3C7A3128487AD76CEEA212EDCC5484E64B  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.XmlImplementation::InternalNameTable
	XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * ___InternalNameTable_0;

public:
	inline static int32_t get_offset_of_InternalNameTable_0() { return static_cast<int32_t>(offsetof(XmlImplementation_t941F4E3C7A3128487AD76CEEA212EDCC5484E64B, ___InternalNameTable_0)); }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * get_InternalNameTable_0() const { return ___InternalNameTable_0; }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B ** get_address_of_InternalNameTable_0() { return &___InternalNameTable_0; }
	inline void set_InternalNameTable_0(XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * value)
	{
		___InternalNameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___InternalNameTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIMPLEMENTATION_T941F4E3C7A3128487AD76CEEA212EDCC5484E64B_H
#ifndef XMLNAMEENTRY_T52398CEC2377640341683A49A09093C610A65768_H
#define XMLNAMEENTRY_T52398CEC2377640341683A49A09093C610A65768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameEntry
struct  XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlNameEntry::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlNameEntry::LocalName
	String_t* ___LocalName_1;
	// System.String System.Xml.XmlNameEntry::NS
	String_t* ___NS_2;
	// System.Int32 System.Xml.XmlNameEntry::Hash
	int32_t ___Hash_3;
	// System.String System.Xml.XmlNameEntry::prefixed_name_cache
	String_t* ___prefixed_name_cache_4;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_NS_2() { return static_cast<int32_t>(offsetof(XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768, ___NS_2)); }
	inline String_t* get_NS_2() const { return ___NS_2; }
	inline String_t** get_address_of_NS_2() { return &___NS_2; }
	inline void set_NS_2(String_t* value)
	{
		___NS_2 = value;
		Il2CppCodeGenWriteBarrier((&___NS_2), value);
	}

	inline static int32_t get_offset_of_Hash_3() { return static_cast<int32_t>(offsetof(XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768, ___Hash_3)); }
	inline int32_t get_Hash_3() const { return ___Hash_3; }
	inline int32_t* get_address_of_Hash_3() { return &___Hash_3; }
	inline void set_Hash_3(int32_t value)
	{
		___Hash_3 = value;
	}

	inline static int32_t get_offset_of_prefixed_name_cache_4() { return static_cast<int32_t>(offsetof(XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768, ___prefixed_name_cache_4)); }
	inline String_t* get_prefixed_name_cache_4() const { return ___prefixed_name_cache_4; }
	inline String_t** get_address_of_prefixed_name_cache_4() { return &___prefixed_name_cache_4; }
	inline void set_prefixed_name_cache_4(String_t* value)
	{
		___prefixed_name_cache_4 = value;
		Il2CppCodeGenWriteBarrier((&___prefixed_name_cache_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEENTRY_T52398CEC2377640341683A49A09093C610A65768_H
#ifndef XMLNAMEENTRYCACHE_T69A1B77D4107BB4F1DEEA63F9436F43B53EA205B_H
#define XMLNAMEENTRYCACHE_T69A1B77D4107BB4F1DEEA63F9436F43B53EA205B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameEntryCache
struct  XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Xml.XmlNameEntryCache::table
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___table_0;
	// System.Xml.XmlNameTable System.Xml.XmlNameEntryCache::nameTable
	XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * ___nameTable_1;
	// System.Xml.XmlNameEntry System.Xml.XmlNameEntryCache::dummy
	XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768 * ___dummy_2;
	// System.Char[] System.Xml.XmlNameEntryCache::cacheBuffer
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___cacheBuffer_3;

public:
	inline static int32_t get_offset_of_table_0() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B, ___table_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_table_0() const { return ___table_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_table_0() { return &___table_0; }
	inline void set_table_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___table_0 = value;
		Il2CppCodeGenWriteBarrier((&___table_0), value);
	}

	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B, ___nameTable_1)); }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_1), value);
	}

	inline static int32_t get_offset_of_dummy_2() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B, ___dummy_2)); }
	inline XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768 * get_dummy_2() const { return ___dummy_2; }
	inline XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768 ** get_address_of_dummy_2() { return &___dummy_2; }
	inline void set_dummy_2(XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768 * value)
	{
		___dummy_2 = value;
		Il2CppCodeGenWriteBarrier((&___dummy_2), value);
	}

	inline static int32_t get_offset_of_cacheBuffer_3() { return static_cast<int32_t>(offsetof(XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B, ___cacheBuffer_3)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_cacheBuffer_3() const { return ___cacheBuffer_3; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_cacheBuffer_3() { return &___cacheBuffer_3; }
	inline void set_cacheBuffer_3(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___cacheBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___cacheBuffer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEENTRYCACHE_T69A1B77D4107BB4F1DEEA63F9436F43B53EA205B_H
#ifndef XMLNAMETABLE_TAA4F116720DEE4495552DD594D090390F342894B_H
#define XMLNAMETABLE_TAA4F116720DEE4495552DD594D090390F342894B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameTable
struct  XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMETABLE_TAA4F116720DEE4495552DD594D090390F342894B_H
#ifndef XMLNAMEDNODEMAP_TBFED42F937E9A504358A1DCA71C3447CBE685246_H
#define XMLNAMEDNODEMAP_TBFED42F937E9A504358A1DCA71C3447CBE685246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap
struct  XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::parent
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * ___parent_1;
	// System.Collections.ArrayList System.Xml.XmlNamedNodeMap::nodeList
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___nodeList_2;
	// System.Boolean System.Xml.XmlNamedNodeMap::readOnly
	bool ___readOnly_3;

public:
	inline static int32_t get_offset_of_parent_1() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246, ___parent_1)); }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * get_parent_1() const { return ___parent_1; }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A ** get_address_of_parent_1() { return &___parent_1; }
	inline void set_parent_1(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * value)
	{
		___parent_1 = value;
		Il2CppCodeGenWriteBarrier((&___parent_1), value);
	}

	inline static int32_t get_offset_of_nodeList_2() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246, ___nodeList_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_nodeList_2() const { return ___nodeList_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_nodeList_2() { return &___nodeList_2; }
	inline void set_nodeList_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___nodeList_2 = value;
		Il2CppCodeGenWriteBarrier((&___nodeList_2), value);
	}

	inline static int32_t get_offset_of_readOnly_3() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246, ___readOnly_3)); }
	inline bool get_readOnly_3() const { return ___readOnly_3; }
	inline bool* get_address_of_readOnly_3() { return &___readOnly_3; }
	inline void set_readOnly_3(bool value)
	{
		___readOnly_3 = value;
	}
};

struct XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246_StaticFields
{
public:
	// System.Collections.IEnumerator System.Xml.XmlNamedNodeMap::emptyEnumerator
	RuntimeObject* ___emptyEnumerator_0;

public:
	inline static int32_t get_offset_of_emptyEnumerator_0() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246_StaticFields, ___emptyEnumerator_0)); }
	inline RuntimeObject* get_emptyEnumerator_0() const { return ___emptyEnumerator_0; }
	inline RuntimeObject** get_address_of_emptyEnumerator_0() { return &___emptyEnumerator_0; }
	inline void set_emptyEnumerator_0(RuntimeObject* value)
	{
		___emptyEnumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyEnumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEDNODEMAP_TBFED42F937E9A504358A1DCA71C3447CBE685246_H
#ifndef XMLNAMESPACEMANAGER_T638B0C086B7716A1FE071BF9611C4CE780717F86_H
#define XMLNAMESPACEMANAGER_T638B0C086B7716A1FE071BF9611C4CE780717F86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager_NsDecl[] System.Xml.XmlNamespaceManager::decls
	NsDeclU5BU5D_tBDD6A9B70A78098C87EE5FC7BBC47AEC966170DC* ___decls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::declPos
	int32_t ___declPos_1;
	// System.Xml.XmlNamespaceManager_NsScope[] System.Xml.XmlNamespaceManager::scopes
	NsScopeU5BU5D_t27760A59C722CCA943743D29B23D0A1BDC0E226F* ___scopes_2;
	// System.Int32 System.Xml.XmlNamespaceManager::scopePos
	int32_t ___scopePos_3;
	// System.String System.Xml.XmlNamespaceManager::defaultNamespace
	String_t* ___defaultNamespace_4;
	// System.Int32 System.Xml.XmlNamespaceManager::count
	int32_t ___count_5;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * ___nameTable_6;
	// System.Boolean System.Xml.XmlNamespaceManager::internalAtomizedNames
	bool ___internalAtomizedNames_7;

public:
	inline static int32_t get_offset_of_decls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86, ___decls_0)); }
	inline NsDeclU5BU5D_tBDD6A9B70A78098C87EE5FC7BBC47AEC966170DC* get_decls_0() const { return ___decls_0; }
	inline NsDeclU5BU5D_tBDD6A9B70A78098C87EE5FC7BBC47AEC966170DC** get_address_of_decls_0() { return &___decls_0; }
	inline void set_decls_0(NsDeclU5BU5D_tBDD6A9B70A78098C87EE5FC7BBC47AEC966170DC* value)
	{
		___decls_0 = value;
		Il2CppCodeGenWriteBarrier((&___decls_0), value);
	}

	inline static int32_t get_offset_of_declPos_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86, ___declPos_1)); }
	inline int32_t get_declPos_1() const { return ___declPos_1; }
	inline int32_t* get_address_of_declPos_1() { return &___declPos_1; }
	inline void set_declPos_1(int32_t value)
	{
		___declPos_1 = value;
	}

	inline static int32_t get_offset_of_scopes_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86, ___scopes_2)); }
	inline NsScopeU5BU5D_t27760A59C722CCA943743D29B23D0A1BDC0E226F* get_scopes_2() const { return ___scopes_2; }
	inline NsScopeU5BU5D_t27760A59C722CCA943743D29B23D0A1BDC0E226F** get_address_of_scopes_2() { return &___scopes_2; }
	inline void set_scopes_2(NsScopeU5BU5D_t27760A59C722CCA943743D29B23D0A1BDC0E226F* value)
	{
		___scopes_2 = value;
		Il2CppCodeGenWriteBarrier((&___scopes_2), value);
	}

	inline static int32_t get_offset_of_scopePos_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86, ___scopePos_3)); }
	inline int32_t get_scopePos_3() const { return ___scopePos_3; }
	inline int32_t* get_address_of_scopePos_3() { return &___scopePos_3; }
	inline void set_scopePos_3(int32_t value)
	{
		___scopePos_3 = value;
	}

	inline static int32_t get_offset_of_defaultNamespace_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86, ___defaultNamespace_4)); }
	inline String_t* get_defaultNamespace_4() const { return ___defaultNamespace_4; }
	inline String_t** get_address_of_defaultNamespace_4() { return &___defaultNamespace_4; }
	inline void set_defaultNamespace_4(String_t* value)
	{
		___defaultNamespace_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultNamespace_4), value);
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}

	inline static int32_t get_offset_of_nameTable_6() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86, ___nameTable_6)); }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * get_nameTable_6() const { return ___nameTable_6; }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B ** get_address_of_nameTable_6() { return &___nameTable_6; }
	inline void set_nameTable_6(XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * value)
	{
		___nameTable_6 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_6), value);
	}

	inline static int32_t get_offset_of_internalAtomizedNames_7() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86, ___internalAtomizedNames_7)); }
	inline bool get_internalAtomizedNames_7() const { return ___internalAtomizedNames_7; }
	inline bool* get_address_of_internalAtomizedNames_7() { return &___internalAtomizedNames_7; }
	inline void set_internalAtomizedNames_7(bool value)
	{
		___internalAtomizedNames_7 = value;
	}
};

struct XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::<>f__switchU24map25
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map25_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map25_8() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86_StaticFields, ___U3CU3Ef__switchU24map25_8)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map25_8() const { return ___U3CU3Ef__switchU24map25_8; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map25_8() { return &___U3CU3Ef__switchU24map25_8; }
	inline void set_U3CU3Ef__switchU24map25_8(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map25_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map25_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEMANAGER_T638B0C086B7716A1FE071BF9611C4CE780717F86_H
#ifndef XMLNODE_T2B708D2776777AFF8C95DD2EFB651AA64E57632A_H
#define XMLNODE_T2B708D2776777AFF8C95DD2EFB651AA64E57632A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode
struct  XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A  : public RuntimeObject
{
public:
	// System.Xml.XmlDocument System.Xml.XmlNode::ownerDocument
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763 * ___ownerDocument_1;
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * ___parentNode_2;
	// System.Xml.XmlNodeListChildren System.Xml.XmlNode::childNodes
	XmlNodeListChildren_t0DFA6642EB0E0CA24CB3F5AF8DC38D09D5656B2A * ___childNodes_3;

public:
	inline static int32_t get_offset_of_ownerDocument_1() { return static_cast<int32_t>(offsetof(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A, ___ownerDocument_1)); }
	inline XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763 * get_ownerDocument_1() const { return ___ownerDocument_1; }
	inline XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763 ** get_address_of_ownerDocument_1() { return &___ownerDocument_1; }
	inline void set_ownerDocument_1(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763 * value)
	{
		___ownerDocument_1 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_1), value);
	}

	inline static int32_t get_offset_of_parentNode_2() { return static_cast<int32_t>(offsetof(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A, ___parentNode_2)); }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * get_parentNode_2() const { return ___parentNode_2; }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A ** get_address_of_parentNode_2() { return &___parentNode_2; }
	inline void set_parentNode_2(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * value)
	{
		___parentNode_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_2), value);
	}

	inline static int32_t get_offset_of_childNodes_3() { return static_cast<int32_t>(offsetof(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A, ___childNodes_3)); }
	inline XmlNodeListChildren_t0DFA6642EB0E0CA24CB3F5AF8DC38D09D5656B2A * get_childNodes_3() const { return ___childNodes_3; }
	inline XmlNodeListChildren_t0DFA6642EB0E0CA24CB3F5AF8DC38D09D5656B2A ** get_address_of_childNodes_3() { return &___childNodes_3; }
	inline void set_childNodes_3(XmlNodeListChildren_t0DFA6642EB0E0CA24CB3F5AF8DC38D09D5656B2A * value)
	{
		___childNodes_3 = value;
		Il2CppCodeGenWriteBarrier((&___childNodes_3), value);
	}
};

struct XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A_StaticFields
{
public:
	// System.Xml.XmlNode_EmptyNodeList System.Xml.XmlNode::emptyList
	EmptyNodeList_tABF5FD9F0A8DC7FBC7808B3BACC780547EB48735 * ___emptyList_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNode::<>f__switchU24map2B
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map2B_4;

public:
	inline static int32_t get_offset_of_emptyList_0() { return static_cast<int32_t>(offsetof(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A_StaticFields, ___emptyList_0)); }
	inline EmptyNodeList_tABF5FD9F0A8DC7FBC7808B3BACC780547EB48735 * get_emptyList_0() const { return ___emptyList_0; }
	inline EmptyNodeList_tABF5FD9F0A8DC7FBC7808B3BACC780547EB48735 ** get_address_of_emptyList_0() { return &___emptyList_0; }
	inline void set_emptyList_0(EmptyNodeList_tABF5FD9F0A8DC7FBC7808B3BACC780547EB48735 * value)
	{
		___emptyList_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyList_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2B_4() { return static_cast<int32_t>(offsetof(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A_StaticFields, ___U3CU3Ef__switchU24map2B_4)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map2B_4() const { return ___U3CU3Ef__switchU24map2B_4; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map2B_4() { return &___U3CU3Ef__switchU24map2B_4; }
	inline void set_U3CU3Ef__switchU24map2B_4(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map2B_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2B_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODE_T2B708D2776777AFF8C95DD2EFB651AA64E57632A_H
#ifndef XMLNODELIST_TD7AD9076C850FBF7CC2520D0B670B65037DB1E3B_H
#define XMLNODELIST_TD7AD9076C850FBF7CC2520D0B670B65037DB1E3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeList
struct  XmlNodeList_tD7AD9076C850FBF7CC2520D0B670B65037DB1E3B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODELIST_TD7AD9076C850FBF7CC2520D0B670B65037DB1E3B_H
#ifndef ENUMERATOR_T9C8166FB5C464A64F87C434D7EB8F8AADC5E8817_H
#define ENUMERATOR_T9C8166FB5C464A64F87C434D7EB8F8AADC5E8817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeListChildren_Enumerator
struct  Enumerator_t9C8166FB5C464A64F87C434D7EB8F8AADC5E8817  : public RuntimeObject
{
public:
	// System.Xml.IHasXmlChildNode System.Xml.XmlNodeListChildren_Enumerator::parent
	RuntimeObject* ___parent_0;
	// System.Xml.XmlLinkedNode System.Xml.XmlNodeListChildren_Enumerator::currentChild
	XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * ___currentChild_1;
	// System.Boolean System.Xml.XmlNodeListChildren_Enumerator::passedLastNode
	bool ___passedLastNode_2;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(Enumerator_t9C8166FB5C464A64F87C434D7EB8F8AADC5E8817, ___parent_0)); }
	inline RuntimeObject* get_parent_0() const { return ___parent_0; }
	inline RuntimeObject** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(RuntimeObject* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_currentChild_1() { return static_cast<int32_t>(offsetof(Enumerator_t9C8166FB5C464A64F87C434D7EB8F8AADC5E8817, ___currentChild_1)); }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * get_currentChild_1() const { return ___currentChild_1; }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 ** get_address_of_currentChild_1() { return &___currentChild_1; }
	inline void set_currentChild_1(XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * value)
	{
		___currentChild_1 = value;
		Il2CppCodeGenWriteBarrier((&___currentChild_1), value);
	}

	inline static int32_t get_offset_of_passedLastNode_2() { return static_cast<int32_t>(offsetof(Enumerator_t9C8166FB5C464A64F87C434D7EB8F8AADC5E8817, ___passedLastNode_2)); }
	inline bool get_passedLastNode_2() const { return ___passedLastNode_2; }
	inline bool* get_address_of_passedLastNode_2() { return &___passedLastNode_2; }
	inline void set_passedLastNode_2(bool value)
	{
		___passedLastNode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T9C8166FB5C464A64F87C434D7EB8F8AADC5E8817_H
#ifndef XMLQUALIFIEDNAME_T473CA6BDDCC80E24626C6741FC40EFE9EA9E7004_H
#define XMLQUALIFIEDNAME_T473CA6BDDCC80E24626C6741FC40EFE9EA9E7004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlQualifiedName
struct  XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlQualifiedName::name
	String_t* ___name_1;
	// System.String System.Xml.XmlQualifiedName::ns
	String_t* ___ns_2;
	// System.Int32 System.Xml.XmlQualifiedName::hash
	int32_t ___hash_3;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((&___ns_2), value);
	}

	inline static int32_t get_offset_of_hash_3() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004, ___hash_3)); }
	inline int32_t get_hash_3() const { return ___hash_3; }
	inline int32_t* get_address_of_hash_3() { return &___hash_3; }
	inline void set_hash_3(int32_t value)
	{
		___hash_3 = value;
	}
};

struct XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004_StaticFields
{
public:
	// System.Xml.XmlQualifiedName System.Xml.XmlQualifiedName::Empty
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004_StaticFields, ___Empty_0)); }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * get_Empty_0() const { return ___Empty_0; }
	inline XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLQUALIFIEDNAME_T473CA6BDDCC80E24626C6741FC40EFE9EA9E7004_H
#ifndef XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#define XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293, ___binary_0)); }
	inline XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293, ___settings_1)); }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#ifndef XMLRESOLVER_TF417EFBE696C3A5E7656C0FA477260AFF8AA252C_H
#define XMLRESOLVER_TF417EFBE696C3A5E7656C0FA477260AFF8AA252C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlResolver
struct  XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRESOLVER_TF417EFBE696C3A5E7656C0FA477260AFF8AA252C_H
#ifndef XMLWRITER_TF67065A3B83E8B7E2B24D172D8223F4328F437B6_H
#define XMLWRITER_TF67065A3B83E8B7E2B24D172D8223F4328F437B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWriter
struct  XmlWriter_tF67065A3B83E8B7E2B24D172D8223F4328F437B6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITER_TF67065A3B83E8B7E2B24D172D8223F4328F437B6_H
#ifndef DTDATTLISTDECLARATION_T8E0E85C9F6362A01A1B30700071A49A2B87EDBF4_H
#define DTDATTLISTDECLARATION_T8E0E85C9F6362A01A1B30700071A49A2B87EDBF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttListDeclaration
struct  DTDAttListDeclaration_t8E0E85C9F6362A01A1B30700071A49A2B87EDBF4  : public DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B
{
public:
	// System.String Mono.Xml.DTDAttListDeclaration::name
	String_t* ___name_5;
	// System.Collections.Hashtable Mono.Xml.DTDAttListDeclaration::attributeOrders
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___attributeOrders_6;
	// System.Collections.ArrayList Mono.Xml.DTDAttListDeclaration::attributes
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___attributes_7;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t8E0E85C9F6362A01A1B30700071A49A2B87EDBF4, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_attributeOrders_6() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t8E0E85C9F6362A01A1B30700071A49A2B87EDBF4, ___attributeOrders_6)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_attributeOrders_6() const { return ___attributeOrders_6; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_attributeOrders_6() { return &___attributeOrders_6; }
	inline void set_attributeOrders_6(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___attributeOrders_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributeOrders_6), value);
	}

	inline static int32_t get_offset_of_attributes_7() { return static_cast<int32_t>(offsetof(DTDAttListDeclaration_t8E0E85C9F6362A01A1B30700071A49A2B87EDBF4, ___attributes_7)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_attributes_7() const { return ___attributes_7; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_attributes_7() { return &___attributes_7; }
	inline void set_attributes_7(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___attributes_7 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTLISTDECLARATION_T8E0E85C9F6362A01A1B30700071A49A2B87EDBF4_H
#ifndef DTDELEMENTDECLARATION_T5D44E3F438BC3A6A945C7D896AFED56B7DC95245_H
#define DTDELEMENTDECLARATION_T5D44E3F438BC3A6A945C7D896AFED56B7DC95245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementDeclaration
struct  DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245  : public DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDElementDeclaration::root
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * ___root_5;
	// Mono.Xml.DTDContentModel Mono.Xml.DTDElementDeclaration::contentModel
	DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF * ___contentModel_6;
	// System.String Mono.Xml.DTDElementDeclaration::name
	String_t* ___name_7;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isEmpty
	bool ___isEmpty_8;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isAny
	bool ___isAny_9;
	// System.Boolean Mono.Xml.DTDElementDeclaration::isMixedContent
	bool ___isMixedContent_10;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245, ___root_5)); }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}

	inline static int32_t get_offset_of_contentModel_6() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245, ___contentModel_6)); }
	inline DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF * get_contentModel_6() const { return ___contentModel_6; }
	inline DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF ** get_address_of_contentModel_6() { return &___contentModel_6; }
	inline void set_contentModel_6(DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF * value)
	{
		___contentModel_6 = value;
		Il2CppCodeGenWriteBarrier((&___contentModel_6), value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_isEmpty_8() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245, ___isEmpty_8)); }
	inline bool get_isEmpty_8() const { return ___isEmpty_8; }
	inline bool* get_address_of_isEmpty_8() { return &___isEmpty_8; }
	inline void set_isEmpty_8(bool value)
	{
		___isEmpty_8 = value;
	}

	inline static int32_t get_offset_of_isAny_9() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245, ___isAny_9)); }
	inline bool get_isAny_9() const { return ___isAny_9; }
	inline bool* get_address_of_isAny_9() { return &___isAny_9; }
	inline void set_isAny_9(bool value)
	{
		___isAny_9 = value;
	}

	inline static int32_t get_offset_of_isMixedContent_10() { return static_cast<int32_t>(offsetof(DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245, ___isMixedContent_10)); }
	inline bool get_isMixedContent_10() const { return ___isMixedContent_10; }
	inline bool* get_address_of_isMixedContent_10() { return &___isMixedContent_10; }
	inline void set_isMixedContent_10(bool value)
	{
		___isMixedContent_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTDECLARATION_T5D44E3F438BC3A6A945C7D896AFED56B7DC95245_H
#ifndef DTDENTITYBASE_T9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0_H
#define DTDENTITYBASE_T9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityBase
struct  DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0  : public DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B
{
public:
	// System.String Mono.Xml.DTDEntityBase::name
	String_t* ___name_5;
	// System.String Mono.Xml.DTDEntityBase::publicId
	String_t* ___publicId_6;
	// System.String Mono.Xml.DTDEntityBase::systemId
	String_t* ___systemId_7;
	// System.String Mono.Xml.DTDEntityBase::literalValue
	String_t* ___literalValue_8;
	// System.String Mono.Xml.DTDEntityBase::replacementText
	String_t* ___replacementText_9;
	// System.String Mono.Xml.DTDEntityBase::uriString
	String_t* ___uriString_10;
	// System.Uri Mono.Xml.DTDEntityBase::absUri
	Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * ___absUri_11;
	// System.Boolean Mono.Xml.DTDEntityBase::isInvalid
	bool ___isInvalid_12;
	// System.Boolean Mono.Xml.DTDEntityBase::loadFailed
	bool ___loadFailed_13;
	// System.Xml.XmlResolver Mono.Xml.DTDEntityBase::resolver
	XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * ___resolver_14;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_publicId_6() { return static_cast<int32_t>(offsetof(DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0, ___publicId_6)); }
	inline String_t* get_publicId_6() const { return ___publicId_6; }
	inline String_t** get_address_of_publicId_6() { return &___publicId_6; }
	inline void set_publicId_6(String_t* value)
	{
		___publicId_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_6), value);
	}

	inline static int32_t get_offset_of_systemId_7() { return static_cast<int32_t>(offsetof(DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0, ___systemId_7)); }
	inline String_t* get_systemId_7() const { return ___systemId_7; }
	inline String_t** get_address_of_systemId_7() { return &___systemId_7; }
	inline void set_systemId_7(String_t* value)
	{
		___systemId_7 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_7), value);
	}

	inline static int32_t get_offset_of_literalValue_8() { return static_cast<int32_t>(offsetof(DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0, ___literalValue_8)); }
	inline String_t* get_literalValue_8() const { return ___literalValue_8; }
	inline String_t** get_address_of_literalValue_8() { return &___literalValue_8; }
	inline void set_literalValue_8(String_t* value)
	{
		___literalValue_8 = value;
		Il2CppCodeGenWriteBarrier((&___literalValue_8), value);
	}

	inline static int32_t get_offset_of_replacementText_9() { return static_cast<int32_t>(offsetof(DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0, ___replacementText_9)); }
	inline String_t* get_replacementText_9() const { return ___replacementText_9; }
	inline String_t** get_address_of_replacementText_9() { return &___replacementText_9; }
	inline void set_replacementText_9(String_t* value)
	{
		___replacementText_9 = value;
		Il2CppCodeGenWriteBarrier((&___replacementText_9), value);
	}

	inline static int32_t get_offset_of_uriString_10() { return static_cast<int32_t>(offsetof(DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0, ___uriString_10)); }
	inline String_t* get_uriString_10() const { return ___uriString_10; }
	inline String_t** get_address_of_uriString_10() { return &___uriString_10; }
	inline void set_uriString_10(String_t* value)
	{
		___uriString_10 = value;
		Il2CppCodeGenWriteBarrier((&___uriString_10), value);
	}

	inline static int32_t get_offset_of_absUri_11() { return static_cast<int32_t>(offsetof(DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0, ___absUri_11)); }
	inline Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * get_absUri_11() const { return ___absUri_11; }
	inline Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E ** get_address_of_absUri_11() { return &___absUri_11; }
	inline void set_absUri_11(Uri_tF93A2559917F312C27D9DEAC7CAE4400D3A40F1E * value)
	{
		___absUri_11 = value;
		Il2CppCodeGenWriteBarrier((&___absUri_11), value);
	}

	inline static int32_t get_offset_of_isInvalid_12() { return static_cast<int32_t>(offsetof(DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0, ___isInvalid_12)); }
	inline bool get_isInvalid_12() const { return ___isInvalid_12; }
	inline bool* get_address_of_isInvalid_12() { return &___isInvalid_12; }
	inline void set_isInvalid_12(bool value)
	{
		___isInvalid_12 = value;
	}

	inline static int32_t get_offset_of_loadFailed_13() { return static_cast<int32_t>(offsetof(DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0, ___loadFailed_13)); }
	inline bool get_loadFailed_13() const { return ___loadFailed_13; }
	inline bool* get_address_of_loadFailed_13() { return &___loadFailed_13; }
	inline void set_loadFailed_13(bool value)
	{
		___loadFailed_13 = value;
	}

	inline static int32_t get_offset_of_resolver_14() { return static_cast<int32_t>(offsetof(DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0, ___resolver_14)); }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * get_resolver_14() const { return ___resolver_14; }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C ** get_address_of_resolver_14() { return &___resolver_14; }
	inline void set_resolver_14(XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * value)
	{
		___resolver_14 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYBASE_T9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0_H
#ifndef DTDINVALIDAUTOMATA_TDF68F8C0E639715110419CEA22B09F5F30CCE0DB_H
#define DTDINVALIDAUTOMATA_TDF68F8C0E639715110419CEA22B09F5F30CCE0DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDInvalidAutomata
struct  DTDInvalidAutomata_tDF68F8C0E639715110419CEA22B09F5F30CCE0DB  : public DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDINVALIDAUTOMATA_TDF68F8C0E639715110419CEA22B09F5F30CCE0DB_H
#ifndef DTDNOTATIONDECLARATION_T4AE86A46E6453976D99438636B220B04F4870087_H
#define DTDNOTATIONDECLARATION_T4AE86A46E6453976D99438636B220B04F4870087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNotationDeclaration
struct  DTDNotationDeclaration_t4AE86A46E6453976D99438636B220B04F4870087  : public DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B
{
public:
	// System.String Mono.Xml.DTDNotationDeclaration::name
	String_t* ___name_5;
	// System.String Mono.Xml.DTDNotationDeclaration::localName
	String_t* ___localName_6;
	// System.String Mono.Xml.DTDNotationDeclaration::prefix
	String_t* ___prefix_7;
	// System.String Mono.Xml.DTDNotationDeclaration::publicId
	String_t* ___publicId_8;
	// System.String Mono.Xml.DTDNotationDeclaration::systemId
	String_t* ___systemId_9;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t4AE86A46E6453976D99438636B220B04F4870087, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_localName_6() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t4AE86A46E6453976D99438636B220B04F4870087, ___localName_6)); }
	inline String_t* get_localName_6() const { return ___localName_6; }
	inline String_t** get_address_of_localName_6() { return &___localName_6; }
	inline void set_localName_6(String_t* value)
	{
		___localName_6 = value;
		Il2CppCodeGenWriteBarrier((&___localName_6), value);
	}

	inline static int32_t get_offset_of_prefix_7() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t4AE86A46E6453976D99438636B220B04F4870087, ___prefix_7)); }
	inline String_t* get_prefix_7() const { return ___prefix_7; }
	inline String_t** get_address_of_prefix_7() { return &___prefix_7; }
	inline void set_prefix_7(String_t* value)
	{
		___prefix_7 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_7), value);
	}

	inline static int32_t get_offset_of_publicId_8() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t4AE86A46E6453976D99438636B220B04F4870087, ___publicId_8)); }
	inline String_t* get_publicId_8() const { return ___publicId_8; }
	inline String_t** get_address_of_publicId_8() { return &___publicId_8; }
	inline void set_publicId_8(String_t* value)
	{
		___publicId_8 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_8), value);
	}

	inline static int32_t get_offset_of_systemId_9() { return static_cast<int32_t>(offsetof(DTDNotationDeclaration_t4AE86A46E6453976D99438636B220B04F4870087, ___systemId_9)); }
	inline String_t* get_systemId_9() const { return ___systemId_9; }
	inline String_t** get_address_of_systemId_9() { return &___systemId_9; }
	inline void set_systemId_9(String_t* value)
	{
		___systemId_9 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNOTATIONDECLARATION_T4AE86A46E6453976D99438636B220B04F4870087_H
#ifndef DTDVALIDATINGREADER_T45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8_H
#define DTDVALIDATINGREADER_T45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDValidatingReader
struct  DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// Mono.Xml.EntityResolvingXmlReader Mono.Xml.DTDValidatingReader::reader
	EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F * ___reader_2;
	// System.Xml.XmlTextReader Mono.Xml.DTDValidatingReader::sourceTextReader
	XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 * ___sourceTextReader_3;
	// System.Xml.XmlValidatingReader Mono.Xml.DTDValidatingReader::validatingReader
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6 * ___validatingReader_4;
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDValidatingReader::dtd
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * ___dtd_5;
	// System.Xml.XmlResolver Mono.Xml.DTDValidatingReader::resolver
	XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * ___resolver_6;
	// System.String Mono.Xml.DTDValidatingReader::currentElement
	String_t* ___currentElement_7;
	// Mono.Xml.DTDValidatingReader_AttributeSlot[] Mono.Xml.DTDValidatingReader::attributes
	AttributeSlotU5BU5D_t87B6D6037EDB3BC424B29D613C23C7D57290C3B3* ___attributes_8;
	// System.Int32 Mono.Xml.DTDValidatingReader::attributeCount
	int32_t ___attributeCount_9;
	// System.Int32 Mono.Xml.DTDValidatingReader::currentAttribute
	int32_t ___currentAttribute_10;
	// System.Boolean Mono.Xml.DTDValidatingReader::consumedAttribute
	bool ___consumedAttribute_11;
	// System.Collections.Stack Mono.Xml.DTDValidatingReader::elementStack
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___elementStack_12;
	// System.Collections.Stack Mono.Xml.DTDValidatingReader::automataStack
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___automataStack_13;
	// System.Boolean Mono.Xml.DTDValidatingReader::popScope
	bool ___popScope_14;
	// System.Boolean Mono.Xml.DTDValidatingReader::isStandalone
	bool ___isStandalone_15;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDValidatingReader::currentAutomata
	DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * ___currentAutomata_16;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDValidatingReader::previousAutomata
	DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * ___previousAutomata_17;
	// System.Collections.ArrayList Mono.Xml.DTDValidatingReader::idList
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___idList_18;
	// System.Collections.ArrayList Mono.Xml.DTDValidatingReader::missingIDReferences
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___missingIDReferences_19;
	// System.Xml.XmlNamespaceManager Mono.Xml.DTDValidatingReader::nsmgr
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * ___nsmgr_20;
	// System.String Mono.Xml.DTDValidatingReader::currentTextValue
	String_t* ___currentTextValue_21;
	// System.String Mono.Xml.DTDValidatingReader::constructingTextValue
	String_t* ___constructingTextValue_22;
	// System.Boolean Mono.Xml.DTDValidatingReader::shouldResetCurrentTextValue
	bool ___shouldResetCurrentTextValue_23;
	// System.Boolean Mono.Xml.DTDValidatingReader::isSignificantWhitespace
	bool ___isSignificantWhitespace_24;
	// System.Boolean Mono.Xml.DTDValidatingReader::isWhitespace
	bool ___isWhitespace_25;
	// System.Boolean Mono.Xml.DTDValidatingReader::isText
	bool ___isText_26;
	// System.Collections.Stack Mono.Xml.DTDValidatingReader::attributeValueEntityStack
	Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * ___attributeValueEntityStack_27;
	// System.Text.StringBuilder Mono.Xml.DTDValidatingReader::valueBuilder
	StringBuilder_t * ___valueBuilder_28;
	// System.Char[] Mono.Xml.DTDValidatingReader::whitespaceChars
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___whitespaceChars_29;

public:
	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___reader_2)); }
	inline EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F * get_reader_2() const { return ___reader_2; }
	inline EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F ** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F * value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier((&___reader_2), value);
	}

	inline static int32_t get_offset_of_sourceTextReader_3() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___sourceTextReader_3)); }
	inline XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 * get_sourceTextReader_3() const { return ___sourceTextReader_3; }
	inline XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 ** get_address_of_sourceTextReader_3() { return &___sourceTextReader_3; }
	inline void set_sourceTextReader_3(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 * value)
	{
		___sourceTextReader_3 = value;
		Il2CppCodeGenWriteBarrier((&___sourceTextReader_3), value);
	}

	inline static int32_t get_offset_of_validatingReader_4() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___validatingReader_4)); }
	inline XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6 * get_validatingReader_4() const { return ___validatingReader_4; }
	inline XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6 ** get_address_of_validatingReader_4() { return &___validatingReader_4; }
	inline void set_validatingReader_4(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6 * value)
	{
		___validatingReader_4 = value;
		Il2CppCodeGenWriteBarrier((&___validatingReader_4), value);
	}

	inline static int32_t get_offset_of_dtd_5() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___dtd_5)); }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * get_dtd_5() const { return ___dtd_5; }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E ** get_address_of_dtd_5() { return &___dtd_5; }
	inline void set_dtd_5(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * value)
	{
		___dtd_5 = value;
		Il2CppCodeGenWriteBarrier((&___dtd_5), value);
	}

	inline static int32_t get_offset_of_resolver_6() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___resolver_6)); }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * get_resolver_6() const { return ___resolver_6; }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C ** get_address_of_resolver_6() { return &___resolver_6; }
	inline void set_resolver_6(XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * value)
	{
		___resolver_6 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_6), value);
	}

	inline static int32_t get_offset_of_currentElement_7() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___currentElement_7)); }
	inline String_t* get_currentElement_7() const { return ___currentElement_7; }
	inline String_t** get_address_of_currentElement_7() { return &___currentElement_7; }
	inline void set_currentElement_7(String_t* value)
	{
		___currentElement_7 = value;
		Il2CppCodeGenWriteBarrier((&___currentElement_7), value);
	}

	inline static int32_t get_offset_of_attributes_8() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___attributes_8)); }
	inline AttributeSlotU5BU5D_t87B6D6037EDB3BC424B29D613C23C7D57290C3B3* get_attributes_8() const { return ___attributes_8; }
	inline AttributeSlotU5BU5D_t87B6D6037EDB3BC424B29D613C23C7D57290C3B3** get_address_of_attributes_8() { return &___attributes_8; }
	inline void set_attributes_8(AttributeSlotU5BU5D_t87B6D6037EDB3BC424B29D613C23C7D57290C3B3* value)
	{
		___attributes_8 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_8), value);
	}

	inline static int32_t get_offset_of_attributeCount_9() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___attributeCount_9)); }
	inline int32_t get_attributeCount_9() const { return ___attributeCount_9; }
	inline int32_t* get_address_of_attributeCount_9() { return &___attributeCount_9; }
	inline void set_attributeCount_9(int32_t value)
	{
		___attributeCount_9 = value;
	}

	inline static int32_t get_offset_of_currentAttribute_10() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___currentAttribute_10)); }
	inline int32_t get_currentAttribute_10() const { return ___currentAttribute_10; }
	inline int32_t* get_address_of_currentAttribute_10() { return &___currentAttribute_10; }
	inline void set_currentAttribute_10(int32_t value)
	{
		___currentAttribute_10 = value;
	}

	inline static int32_t get_offset_of_consumedAttribute_11() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___consumedAttribute_11)); }
	inline bool get_consumedAttribute_11() const { return ___consumedAttribute_11; }
	inline bool* get_address_of_consumedAttribute_11() { return &___consumedAttribute_11; }
	inline void set_consumedAttribute_11(bool value)
	{
		___consumedAttribute_11 = value;
	}

	inline static int32_t get_offset_of_elementStack_12() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___elementStack_12)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_elementStack_12() const { return ___elementStack_12; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_elementStack_12() { return &___elementStack_12; }
	inline void set_elementStack_12(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___elementStack_12 = value;
		Il2CppCodeGenWriteBarrier((&___elementStack_12), value);
	}

	inline static int32_t get_offset_of_automataStack_13() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___automataStack_13)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_automataStack_13() const { return ___automataStack_13; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_automataStack_13() { return &___automataStack_13; }
	inline void set_automataStack_13(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___automataStack_13 = value;
		Il2CppCodeGenWriteBarrier((&___automataStack_13), value);
	}

	inline static int32_t get_offset_of_popScope_14() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___popScope_14)); }
	inline bool get_popScope_14() const { return ___popScope_14; }
	inline bool* get_address_of_popScope_14() { return &___popScope_14; }
	inline void set_popScope_14(bool value)
	{
		___popScope_14 = value;
	}

	inline static int32_t get_offset_of_isStandalone_15() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___isStandalone_15)); }
	inline bool get_isStandalone_15() const { return ___isStandalone_15; }
	inline bool* get_address_of_isStandalone_15() { return &___isStandalone_15; }
	inline void set_isStandalone_15(bool value)
	{
		___isStandalone_15 = value;
	}

	inline static int32_t get_offset_of_currentAutomata_16() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___currentAutomata_16)); }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * get_currentAutomata_16() const { return ___currentAutomata_16; }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 ** get_address_of_currentAutomata_16() { return &___currentAutomata_16; }
	inline void set_currentAutomata_16(DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * value)
	{
		___currentAutomata_16 = value;
		Il2CppCodeGenWriteBarrier((&___currentAutomata_16), value);
	}

	inline static int32_t get_offset_of_previousAutomata_17() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___previousAutomata_17)); }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * get_previousAutomata_17() const { return ___previousAutomata_17; }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 ** get_address_of_previousAutomata_17() { return &___previousAutomata_17; }
	inline void set_previousAutomata_17(DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * value)
	{
		___previousAutomata_17 = value;
		Il2CppCodeGenWriteBarrier((&___previousAutomata_17), value);
	}

	inline static int32_t get_offset_of_idList_18() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___idList_18)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_idList_18() const { return ___idList_18; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_idList_18() { return &___idList_18; }
	inline void set_idList_18(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___idList_18 = value;
		Il2CppCodeGenWriteBarrier((&___idList_18), value);
	}

	inline static int32_t get_offset_of_missingIDReferences_19() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___missingIDReferences_19)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_missingIDReferences_19() const { return ___missingIDReferences_19; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_missingIDReferences_19() { return &___missingIDReferences_19; }
	inline void set_missingIDReferences_19(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___missingIDReferences_19 = value;
		Il2CppCodeGenWriteBarrier((&___missingIDReferences_19), value);
	}

	inline static int32_t get_offset_of_nsmgr_20() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___nsmgr_20)); }
	inline XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * get_nsmgr_20() const { return ___nsmgr_20; }
	inline XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 ** get_address_of_nsmgr_20() { return &___nsmgr_20; }
	inline void set_nsmgr_20(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * value)
	{
		___nsmgr_20 = value;
		Il2CppCodeGenWriteBarrier((&___nsmgr_20), value);
	}

	inline static int32_t get_offset_of_currentTextValue_21() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___currentTextValue_21)); }
	inline String_t* get_currentTextValue_21() const { return ___currentTextValue_21; }
	inline String_t** get_address_of_currentTextValue_21() { return &___currentTextValue_21; }
	inline void set_currentTextValue_21(String_t* value)
	{
		___currentTextValue_21 = value;
		Il2CppCodeGenWriteBarrier((&___currentTextValue_21), value);
	}

	inline static int32_t get_offset_of_constructingTextValue_22() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___constructingTextValue_22)); }
	inline String_t* get_constructingTextValue_22() const { return ___constructingTextValue_22; }
	inline String_t** get_address_of_constructingTextValue_22() { return &___constructingTextValue_22; }
	inline void set_constructingTextValue_22(String_t* value)
	{
		___constructingTextValue_22 = value;
		Il2CppCodeGenWriteBarrier((&___constructingTextValue_22), value);
	}

	inline static int32_t get_offset_of_shouldResetCurrentTextValue_23() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___shouldResetCurrentTextValue_23)); }
	inline bool get_shouldResetCurrentTextValue_23() const { return ___shouldResetCurrentTextValue_23; }
	inline bool* get_address_of_shouldResetCurrentTextValue_23() { return &___shouldResetCurrentTextValue_23; }
	inline void set_shouldResetCurrentTextValue_23(bool value)
	{
		___shouldResetCurrentTextValue_23 = value;
	}

	inline static int32_t get_offset_of_isSignificantWhitespace_24() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___isSignificantWhitespace_24)); }
	inline bool get_isSignificantWhitespace_24() const { return ___isSignificantWhitespace_24; }
	inline bool* get_address_of_isSignificantWhitespace_24() { return &___isSignificantWhitespace_24; }
	inline void set_isSignificantWhitespace_24(bool value)
	{
		___isSignificantWhitespace_24 = value;
	}

	inline static int32_t get_offset_of_isWhitespace_25() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___isWhitespace_25)); }
	inline bool get_isWhitespace_25() const { return ___isWhitespace_25; }
	inline bool* get_address_of_isWhitespace_25() { return &___isWhitespace_25; }
	inline void set_isWhitespace_25(bool value)
	{
		___isWhitespace_25 = value;
	}

	inline static int32_t get_offset_of_isText_26() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___isText_26)); }
	inline bool get_isText_26() const { return ___isText_26; }
	inline bool* get_address_of_isText_26() { return &___isText_26; }
	inline void set_isText_26(bool value)
	{
		___isText_26 = value;
	}

	inline static int32_t get_offset_of_attributeValueEntityStack_27() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___attributeValueEntityStack_27)); }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * get_attributeValueEntityStack_27() const { return ___attributeValueEntityStack_27; }
	inline Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 ** get_address_of_attributeValueEntityStack_27() { return &___attributeValueEntityStack_27; }
	inline void set_attributeValueEntityStack_27(Stack_t7384B3BC16406A09DCD2C2D2EA591D34ACBFDBA5 * value)
	{
		___attributeValueEntityStack_27 = value;
		Il2CppCodeGenWriteBarrier((&___attributeValueEntityStack_27), value);
	}

	inline static int32_t get_offset_of_valueBuilder_28() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___valueBuilder_28)); }
	inline StringBuilder_t * get_valueBuilder_28() const { return ___valueBuilder_28; }
	inline StringBuilder_t ** get_address_of_valueBuilder_28() { return &___valueBuilder_28; }
	inline void set_valueBuilder_28(StringBuilder_t * value)
	{
		___valueBuilder_28 = value;
		Il2CppCodeGenWriteBarrier((&___valueBuilder_28), value);
	}

	inline static int32_t get_offset_of_whitespaceChars_29() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8, ___whitespaceChars_29)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_whitespaceChars_29() const { return ___whitespaceChars_29; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_whitespaceChars_29() { return &___whitespaceChars_29; }
	inline void set_whitespaceChars_29(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___whitespaceChars_29 = value;
		Il2CppCodeGenWriteBarrier((&___whitespaceChars_29), value);
	}
};

struct DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml.DTDValidatingReader::<>f__switchU24map2A
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map2A_30;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map2A_30() { return static_cast<int32_t>(offsetof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8_StaticFields, ___U3CU3Ef__switchU24map2A_30)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map2A_30() const { return ___U3CU3Ef__switchU24map2A_30; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map2A_30() { return &___U3CU3Ef__switchU24map2A_30; }
	inline void set_U3CU3Ef__switchU24map2A_30(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map2A_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map2A_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDVALIDATINGREADER_T45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8_H
#ifndef DICTIONARYBASE_T415336F093F8CDFAF278AA55A0F4A0FD46780582_H
#define DICTIONARYBASE_T415336F093F8CDFAF278AA55A0F4A0FD46780582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DictionaryBase
struct  DictionaryBase_t415336F093F8CDFAF278AA55A0F4A0FD46780582  : public List_1_t341C4DA0AC3E19C291DAB1B40650E946D9B6728B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARYBASE_T415336F093F8CDFAF278AA55A0F4A0FD46780582_H
#ifndef TAGNAME_TF4669CF39B6F11E1EB42EC941A67A73BB025D3D4_H
#define TAGNAME_TF4669CF39B6F11E1EB42EC941A67A73BB025D3D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader_TagName
struct  TagName_tF4669CF39B6F11E1EB42EC941A67A73BB025D3D4 
{
public:
	// System.String Mono.Xml2.XmlTextReader_TagName::Name
	String_t* ___Name_0;
	// System.String Mono.Xml2.XmlTextReader_TagName::LocalName
	String_t* ___LocalName_1;
	// System.String Mono.Xml2.XmlTextReader_TagName::Prefix
	String_t* ___Prefix_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(TagName_tF4669CF39B6F11E1EB42EC941A67A73BB025D3D4, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_LocalName_1() { return static_cast<int32_t>(offsetof(TagName_tF4669CF39B6F11E1EB42EC941A67A73BB025D3D4, ___LocalName_1)); }
	inline String_t* get_LocalName_1() const { return ___LocalName_1; }
	inline String_t** get_address_of_LocalName_1() { return &___LocalName_1; }
	inline void set_LocalName_1(String_t* value)
	{
		___LocalName_1 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_1), value);
	}

	inline static int32_t get_offset_of_Prefix_2() { return static_cast<int32_t>(offsetof(TagName_tF4669CF39B6F11E1EB42EC941A67A73BB025D3D4, ___Prefix_2)); }
	inline String_t* get_Prefix_2() const { return ___Prefix_2; }
	inline String_t** get_address_of_Prefix_2() { return &___Prefix_2; }
	inline void set_Prefix_2(String_t* value)
	{
		___Prefix_2 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Mono.Xml2.XmlTextReader/TagName
struct TagName_tF4669CF39B6F11E1EB42EC941A67A73BB025D3D4_marshaled_pinvoke
{
	char* ___Name_0;
	char* ___LocalName_1;
	char* ___Prefix_2;
};
// Native definition for COM marshalling of Mono.Xml2.XmlTextReader/TagName
struct TagName_tF4669CF39B6F11E1EB42EC941A67A73BB025D3D4_marshaled_com
{
	Il2CppChar* ___Name_0;
	Il2CppChar* ___LocalName_1;
	Il2CppChar* ___Prefix_2;
};
#endif // TAGNAME_TF4669CF39B6F11E1EB42EC941A67A73BB025D3D4_H
#ifndef KEYVALUEPAIR_2_T0067BC83D8F125202440DC6183F788BB0802D33D_H
#define KEYVALUEPAIR_2_T0067BC83D8F125202440DC6183F788BB0802D33D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>
struct  KeyValuePair_2_t0067BC83D8F125202440DC6183F788BB0802D33D 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t0067BC83D8F125202440DC6183F788BB0802D33D, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t0067BC83D8F125202440DC6183F788BB0802D33D, ___value_1)); }
	inline DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B * get_value_1() const { return ___value_1; }
	inline DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T0067BC83D8F125202440DC6183F788BB0802D33D_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#define INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_tC16F64335CE8B56D99229DE94BB3A876ED55FE87 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_tC16F64335CE8B56D99229DE94BB3A876ED55FE87, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_TC16F64335CE8B56D99229DE94BB3A876ED55FE87_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#define SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T81A06ADD519539D252788D75E44AFFBE6580E301_H
#ifndef NAMETABLE_TE47F4B558F0AA75A29E063658B7BCB3CE6E482E6_H
#define NAMETABLE_TE47F4B558F0AA75A29E063658B7BCB3CE6E482E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable
struct  NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6  : public XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B
{
public:
	// System.Int32 System.Xml.NameTable::count
	int32_t ___count_0;
	// System.Xml.NameTable_Entry[] System.Xml.NameTable::buckets
	EntryU5BU5D_t97618C7433A838405FA69D1932F53790DB93AEAE* ___buckets_1;
	// System.Int32 System.Xml.NameTable::size
	int32_t ___size_2;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_buckets_1() { return static_cast<int32_t>(offsetof(NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6, ___buckets_1)); }
	inline EntryU5BU5D_t97618C7433A838405FA69D1932F53790DB93AEAE* get_buckets_1() const { return ___buckets_1; }
	inline EntryU5BU5D_t97618C7433A838405FA69D1932F53790DB93AEAE** get_address_of_buckets_1() { return &___buckets_1; }
	inline void set_buckets_1(EntryU5BU5D_t97618C7433A838405FA69D1932F53790DB93AEAE* value)
	{
		___buckets_1 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_1), value);
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6, ___size_2)); }
	inline int32_t get_size_2() const { return ___size_2; }
	inline int32_t* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(int32_t value)
	{
		___size_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETABLE_TE47F4B558F0AA75A29E063658B7BCB3CE6E482E6_H
#ifndef XMLATTRIBUTE_T0D7115A390CCB0A788D00DE412E3AB5FE2C7A910_H
#define XMLATTRIBUTE_T0D7115A390CCB0A788D00DE412E3AB5FE2C7A910_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttribute
struct  XmlAttribute_t0D7115A390CCB0A788D00DE412E3AB5FE2C7A910  : public XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A
{
public:
	// System.Xml.XmlNameEntry System.Xml.XmlAttribute::name
	XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768 * ___name_5;
	// System.Boolean System.Xml.XmlAttribute::isDefault
	bool ___isDefault_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlAttribute::lastLinkedChild
	XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * ___lastLinkedChild_7;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlAttribute::schemaInfo
	RuntimeObject* ___schemaInfo_8;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(XmlAttribute_t0D7115A390CCB0A788D00DE412E3AB5FE2C7A910, ___name_5)); }
	inline XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768 * get_name_5() const { return ___name_5; }
	inline XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768 ** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768 * value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_isDefault_6() { return static_cast<int32_t>(offsetof(XmlAttribute_t0D7115A390CCB0A788D00DE412E3AB5FE2C7A910, ___isDefault_6)); }
	inline bool get_isDefault_6() const { return ___isDefault_6; }
	inline bool* get_address_of_isDefault_6() { return &___isDefault_6; }
	inline void set_isDefault_6(bool value)
	{
		___isDefault_6 = value;
	}

	inline static int32_t get_offset_of_lastLinkedChild_7() { return static_cast<int32_t>(offsetof(XmlAttribute_t0D7115A390CCB0A788D00DE412E3AB5FE2C7A910, ___lastLinkedChild_7)); }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * get_lastLinkedChild_7() const { return ___lastLinkedChild_7; }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 ** get_address_of_lastLinkedChild_7() { return &___lastLinkedChild_7; }
	inline void set_lastLinkedChild_7(XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * value)
	{
		___lastLinkedChild_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_7), value);
	}

	inline static int32_t get_offset_of_schemaInfo_8() { return static_cast<int32_t>(offsetof(XmlAttribute_t0D7115A390CCB0A788D00DE412E3AB5FE2C7A910, ___schemaInfo_8)); }
	inline RuntimeObject* get_schemaInfo_8() const { return ___schemaInfo_8; }
	inline RuntimeObject** get_address_of_schemaInfo_8() { return &___schemaInfo_8; }
	inline void set_schemaInfo_8(RuntimeObject* value)
	{
		___schemaInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTE_T0D7115A390CCB0A788D00DE412E3AB5FE2C7A910_H
#ifndef XMLATTRIBUTECOLLECTION_T0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9_H
#define XMLATTRIBUTECOLLECTION_T0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttributeCollection
struct  XmlAttributeCollection_t0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9  : public XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246
{
public:
	// System.Xml.XmlElement System.Xml.XmlAttributeCollection::ownerElement
	XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9 * ___ownerElement_4;
	// System.Xml.XmlDocument System.Xml.XmlAttributeCollection::ownerDocument
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763 * ___ownerDocument_5;

public:
	inline static int32_t get_offset_of_ownerElement_4() { return static_cast<int32_t>(offsetof(XmlAttributeCollection_t0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9, ___ownerElement_4)); }
	inline XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9 * get_ownerElement_4() const { return ___ownerElement_4; }
	inline XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9 ** get_address_of_ownerElement_4() { return &___ownerElement_4; }
	inline void set_ownerElement_4(XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9 * value)
	{
		___ownerElement_4 = value;
		Il2CppCodeGenWriteBarrier((&___ownerElement_4), value);
	}

	inline static int32_t get_offset_of_ownerDocument_5() { return static_cast<int32_t>(offsetof(XmlAttributeCollection_t0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9, ___ownerDocument_5)); }
	inline XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763 * get_ownerDocument_5() const { return ___ownerDocument_5; }
	inline XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763 ** get_address_of_ownerDocument_5() { return &___ownerDocument_5; }
	inline void set_ownerDocument_5(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763 * value)
	{
		___ownerDocument_5 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTECOLLECTION_T0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9_H
#ifndef XMLDOCUMENT_T4C411CC65B75933374C140FB5737CF2C261CC763_H
#define XMLDOCUMENT_T4C411CC65B75933374C140FB5737CF2C261CC763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocument
struct  XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763  : public XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A
{
public:
	// System.Boolean System.Xml.XmlDocument::optimal_create_element
	bool ___optimal_create_element_6;
	// System.Boolean System.Xml.XmlDocument::optimal_create_attribute
	bool ___optimal_create_attribute_7;
	// System.Xml.XmlNameTable System.Xml.XmlDocument::nameTable
	XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * ___nameTable_8;
	// System.String System.Xml.XmlDocument::baseURI
	String_t* ___baseURI_9;
	// System.Xml.XmlImplementation System.Xml.XmlDocument::implementation
	XmlImplementation_t941F4E3C7A3128487AD76CEEA212EDCC5484E64B * ___implementation_10;
	// System.Boolean System.Xml.XmlDocument::preserveWhitespace
	bool ___preserveWhitespace_11;
	// System.Xml.XmlResolver System.Xml.XmlDocument::resolver
	XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * ___resolver_12;
	// System.Collections.Hashtable System.Xml.XmlDocument::idTable
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___idTable_13;
	// System.Xml.XmlNameEntryCache System.Xml.XmlDocument::nameCache
	XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B * ___nameCache_14;
	// System.Xml.XmlLinkedNode System.Xml.XmlDocument::lastLinkedChild
	XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * ___lastLinkedChild_15;
	// System.Xml.Schema.XmlSchemaSet System.Xml.XmlDocument::schemas
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * ___schemas_16;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::schemaInfo
	RuntimeObject* ___schemaInfo_17;
	// System.Boolean System.Xml.XmlDocument::loadMode
	bool ___loadMode_18;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeChanged
	XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * ___NodeChanged_19;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeChanging
	XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * ___NodeChanging_20;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeInserted
	XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * ___NodeInserted_21;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeInserting
	XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * ___NodeInserting_22;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeRemoved
	XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * ___NodeRemoved_23;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::NodeRemoving
	XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * ___NodeRemoving_24;

public:
	inline static int32_t get_offset_of_optimal_create_element_6() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___optimal_create_element_6)); }
	inline bool get_optimal_create_element_6() const { return ___optimal_create_element_6; }
	inline bool* get_address_of_optimal_create_element_6() { return &___optimal_create_element_6; }
	inline void set_optimal_create_element_6(bool value)
	{
		___optimal_create_element_6 = value;
	}

	inline static int32_t get_offset_of_optimal_create_attribute_7() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___optimal_create_attribute_7)); }
	inline bool get_optimal_create_attribute_7() const { return ___optimal_create_attribute_7; }
	inline bool* get_address_of_optimal_create_attribute_7() { return &___optimal_create_attribute_7; }
	inline void set_optimal_create_attribute_7(bool value)
	{
		___optimal_create_attribute_7 = value;
	}

	inline static int32_t get_offset_of_nameTable_8() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___nameTable_8)); }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * get_nameTable_8() const { return ___nameTable_8; }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B ** get_address_of_nameTable_8() { return &___nameTable_8; }
	inline void set_nameTable_8(XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * value)
	{
		___nameTable_8 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_8), value);
	}

	inline static int32_t get_offset_of_baseURI_9() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___baseURI_9)); }
	inline String_t* get_baseURI_9() const { return ___baseURI_9; }
	inline String_t** get_address_of_baseURI_9() { return &___baseURI_9; }
	inline void set_baseURI_9(String_t* value)
	{
		___baseURI_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_9), value);
	}

	inline static int32_t get_offset_of_implementation_10() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___implementation_10)); }
	inline XmlImplementation_t941F4E3C7A3128487AD76CEEA212EDCC5484E64B * get_implementation_10() const { return ___implementation_10; }
	inline XmlImplementation_t941F4E3C7A3128487AD76CEEA212EDCC5484E64B ** get_address_of_implementation_10() { return &___implementation_10; }
	inline void set_implementation_10(XmlImplementation_t941F4E3C7A3128487AD76CEEA212EDCC5484E64B * value)
	{
		___implementation_10 = value;
		Il2CppCodeGenWriteBarrier((&___implementation_10), value);
	}

	inline static int32_t get_offset_of_preserveWhitespace_11() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___preserveWhitespace_11)); }
	inline bool get_preserveWhitespace_11() const { return ___preserveWhitespace_11; }
	inline bool* get_address_of_preserveWhitespace_11() { return &___preserveWhitespace_11; }
	inline void set_preserveWhitespace_11(bool value)
	{
		___preserveWhitespace_11 = value;
	}

	inline static int32_t get_offset_of_resolver_12() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___resolver_12)); }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * get_resolver_12() const { return ___resolver_12; }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C ** get_address_of_resolver_12() { return &___resolver_12; }
	inline void set_resolver_12(XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * value)
	{
		___resolver_12 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_12), value);
	}

	inline static int32_t get_offset_of_idTable_13() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___idTable_13)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_idTable_13() const { return ___idTable_13; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_idTable_13() { return &___idTable_13; }
	inline void set_idTable_13(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___idTable_13 = value;
		Il2CppCodeGenWriteBarrier((&___idTable_13), value);
	}

	inline static int32_t get_offset_of_nameCache_14() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___nameCache_14)); }
	inline XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B * get_nameCache_14() const { return ___nameCache_14; }
	inline XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B ** get_address_of_nameCache_14() { return &___nameCache_14; }
	inline void set_nameCache_14(XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B * value)
	{
		___nameCache_14 = value;
		Il2CppCodeGenWriteBarrier((&___nameCache_14), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_15() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___lastLinkedChild_15)); }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * get_lastLinkedChild_15() const { return ___lastLinkedChild_15; }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 ** get_address_of_lastLinkedChild_15() { return &___lastLinkedChild_15; }
	inline void set_lastLinkedChild_15(XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * value)
	{
		___lastLinkedChild_15 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_15), value);
	}

	inline static int32_t get_offset_of_schemas_16() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___schemas_16)); }
	inline XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * get_schemas_16() const { return ___schemas_16; }
	inline XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D ** get_address_of_schemas_16() { return &___schemas_16; }
	inline void set_schemas_16(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * value)
	{
		___schemas_16 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_16), value);
	}

	inline static int32_t get_offset_of_schemaInfo_17() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___schemaInfo_17)); }
	inline RuntimeObject* get_schemaInfo_17() const { return ___schemaInfo_17; }
	inline RuntimeObject** get_address_of_schemaInfo_17() { return &___schemaInfo_17; }
	inline void set_schemaInfo_17(RuntimeObject* value)
	{
		___schemaInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_17), value);
	}

	inline static int32_t get_offset_of_loadMode_18() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___loadMode_18)); }
	inline bool get_loadMode_18() const { return ___loadMode_18; }
	inline bool* get_address_of_loadMode_18() { return &___loadMode_18; }
	inline void set_loadMode_18(bool value)
	{
		___loadMode_18 = value;
	}

	inline static int32_t get_offset_of_NodeChanged_19() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___NodeChanged_19)); }
	inline XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * get_NodeChanged_19() const { return ___NodeChanged_19; }
	inline XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC ** get_address_of_NodeChanged_19() { return &___NodeChanged_19; }
	inline void set_NodeChanged_19(XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * value)
	{
		___NodeChanged_19 = value;
		Il2CppCodeGenWriteBarrier((&___NodeChanged_19), value);
	}

	inline static int32_t get_offset_of_NodeChanging_20() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___NodeChanging_20)); }
	inline XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * get_NodeChanging_20() const { return ___NodeChanging_20; }
	inline XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC ** get_address_of_NodeChanging_20() { return &___NodeChanging_20; }
	inline void set_NodeChanging_20(XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * value)
	{
		___NodeChanging_20 = value;
		Il2CppCodeGenWriteBarrier((&___NodeChanging_20), value);
	}

	inline static int32_t get_offset_of_NodeInserted_21() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___NodeInserted_21)); }
	inline XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * get_NodeInserted_21() const { return ___NodeInserted_21; }
	inline XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC ** get_address_of_NodeInserted_21() { return &___NodeInserted_21; }
	inline void set_NodeInserted_21(XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * value)
	{
		___NodeInserted_21 = value;
		Il2CppCodeGenWriteBarrier((&___NodeInserted_21), value);
	}

	inline static int32_t get_offset_of_NodeInserting_22() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___NodeInserting_22)); }
	inline XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * get_NodeInserting_22() const { return ___NodeInserting_22; }
	inline XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC ** get_address_of_NodeInserting_22() { return &___NodeInserting_22; }
	inline void set_NodeInserting_22(XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * value)
	{
		___NodeInserting_22 = value;
		Il2CppCodeGenWriteBarrier((&___NodeInserting_22), value);
	}

	inline static int32_t get_offset_of_NodeRemoved_23() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___NodeRemoved_23)); }
	inline XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * get_NodeRemoved_23() const { return ___NodeRemoved_23; }
	inline XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC ** get_address_of_NodeRemoved_23() { return &___NodeRemoved_23; }
	inline void set_NodeRemoved_23(XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * value)
	{
		___NodeRemoved_23 = value;
		Il2CppCodeGenWriteBarrier((&___NodeRemoved_23), value);
	}

	inline static int32_t get_offset_of_NodeRemoving_24() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763, ___NodeRemoving_24)); }
	inline XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * get_NodeRemoving_24() const { return ___NodeRemoving_24; }
	inline XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC ** get_address_of_NodeRemoving_24() { return &___NodeRemoving_24; }
	inline void set_NodeRemoving_24(XmlNodeChangedEventHandler_tDE269819AB9B5B4BE28B1EDC553BDE78F7F5D4FC * value)
	{
		___NodeRemoving_24 = value;
		Il2CppCodeGenWriteBarrier((&___NodeRemoving_24), value);
	}
};

struct XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763_StaticFields
{
public:
	// System.Type[] System.Xml.XmlDocument::optimal_create_types
	TypeU5BU5D_tEF927658123F6CD4274B971442504A42AB6DE532* ___optimal_create_types_5;

public:
	inline static int32_t get_offset_of_optimal_create_types_5() { return static_cast<int32_t>(offsetof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763_StaticFields, ___optimal_create_types_5)); }
	inline TypeU5BU5D_tEF927658123F6CD4274B971442504A42AB6DE532* get_optimal_create_types_5() const { return ___optimal_create_types_5; }
	inline TypeU5BU5D_tEF927658123F6CD4274B971442504A42AB6DE532** get_address_of_optimal_create_types_5() { return &___optimal_create_types_5; }
	inline void set_optimal_create_types_5(TypeU5BU5D_tEF927658123F6CD4274B971442504A42AB6DE532* value)
	{
		___optimal_create_types_5 = value;
		Il2CppCodeGenWriteBarrier((&___optimal_create_types_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENT_T4C411CC65B75933374C140FB5737CF2C261CC763_H
#ifndef XMLDOCUMENTFRAGMENT_T51FD3AA32D241AD502CCE212D05763C6E1C7219E_H
#define XMLDOCUMENTFRAGMENT_T51FD3AA32D241AD502CCE212D05763C6E1C7219E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentFragment
struct  XmlDocumentFragment_t51FD3AA32D241AD502CCE212D05763C6E1C7219E  : public XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlDocumentFragment::lastLinkedChild
	XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * ___lastLinkedChild_5;

public:
	inline static int32_t get_offset_of_lastLinkedChild_5() { return static_cast<int32_t>(offsetof(XmlDocumentFragment_t51FD3AA32D241AD502CCE212D05763C6E1C7219E, ___lastLinkedChild_5)); }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * get_lastLinkedChild_5() const { return ___lastLinkedChild_5; }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 ** get_address_of_lastLinkedChild_5() { return &___lastLinkedChild_5; }
	inline void set_lastLinkedChild_5(XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * value)
	{
		___lastLinkedChild_5 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTFRAGMENT_T51FD3AA32D241AD502CCE212D05763C6E1C7219E_H
#ifndef XMLENTITY_TAFE7D3FA37928F62D4E9B2AFC6D6309746E19862_H
#define XMLENTITY_TAFE7D3FA37928F62D4E9B2AFC6D6309746E19862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntity
struct  XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862  : public XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A
{
public:
	// System.String System.Xml.XmlEntity::name
	String_t* ___name_5;
	// System.String System.Xml.XmlEntity::NDATA
	String_t* ___NDATA_6;
	// System.String System.Xml.XmlEntity::publicId
	String_t* ___publicId_7;
	// System.String System.Xml.XmlEntity::systemId
	String_t* ___systemId_8;
	// System.String System.Xml.XmlEntity::baseUri
	String_t* ___baseUri_9;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntity::lastLinkedChild
	XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * ___lastLinkedChild_10;
	// System.Boolean System.Xml.XmlEntity::contentAlreadySet
	bool ___contentAlreadySet_11;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_NDATA_6() { return static_cast<int32_t>(offsetof(XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862, ___NDATA_6)); }
	inline String_t* get_NDATA_6() const { return ___NDATA_6; }
	inline String_t** get_address_of_NDATA_6() { return &___NDATA_6; }
	inline void set_NDATA_6(String_t* value)
	{
		___NDATA_6 = value;
		Il2CppCodeGenWriteBarrier((&___NDATA_6), value);
	}

	inline static int32_t get_offset_of_publicId_7() { return static_cast<int32_t>(offsetof(XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862, ___publicId_7)); }
	inline String_t* get_publicId_7() const { return ___publicId_7; }
	inline String_t** get_address_of_publicId_7() { return &___publicId_7; }
	inline void set_publicId_7(String_t* value)
	{
		___publicId_7 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_7), value);
	}

	inline static int32_t get_offset_of_systemId_8() { return static_cast<int32_t>(offsetof(XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862, ___systemId_8)); }
	inline String_t* get_systemId_8() const { return ___systemId_8; }
	inline String_t** get_address_of_systemId_8() { return &___systemId_8; }
	inline void set_systemId_8(String_t* value)
	{
		___systemId_8 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_8), value);
	}

	inline static int32_t get_offset_of_baseUri_9() { return static_cast<int32_t>(offsetof(XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862, ___baseUri_9)); }
	inline String_t* get_baseUri_9() const { return ___baseUri_9; }
	inline String_t** get_address_of_baseUri_9() { return &___baseUri_9; }
	inline void set_baseUri_9(String_t* value)
	{
		___baseUri_9 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_9), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_10() { return static_cast<int32_t>(offsetof(XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862, ___lastLinkedChild_10)); }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * get_lastLinkedChild_10() const { return ___lastLinkedChild_10; }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 ** get_address_of_lastLinkedChild_10() { return &___lastLinkedChild_10; }
	inline void set_lastLinkedChild_10(XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * value)
	{
		___lastLinkedChild_10 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_10), value);
	}

	inline static int32_t get_offset_of_contentAlreadySet_11() { return static_cast<int32_t>(offsetof(XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862, ___contentAlreadySet_11)); }
	inline bool get_contentAlreadySet_11() const { return ___contentAlreadySet_11; }
	inline bool* get_address_of_contentAlreadySet_11() { return &___contentAlreadySet_11; }
	inline void set_contentAlreadySet_11(bool value)
	{
		___contentAlreadySet_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITY_TAFE7D3FA37928F62D4E9B2AFC6D6309746E19862_H
#ifndef XMLLINKEDNODE_T8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23_H
#define XMLLINKEDNODE_T8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23  : public XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::nextSibling
	XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * ___nextSibling_5;

public:
	inline static int32_t get_offset_of_nextSibling_5() { return static_cast<int32_t>(offsetof(XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23, ___nextSibling_5)); }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * get_nextSibling_5() const { return ___nextSibling_5; }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 ** get_address_of_nextSibling_5() { return &___nextSibling_5; }
	inline void set_nextSibling_5(XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * value)
	{
		___nextSibling_5 = value;
		Il2CppCodeGenWriteBarrier((&___nextSibling_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLLINKEDNODE_T8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23_H
#ifndef NSDECL_TFBD4E75517EBC2F40B9055F7785B4A0BEA27B70C_H
#define NSDECL_TFBD4E75517EBC2F40B9055F7785B4A0BEA27B70C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager_NsDecl
struct  NsDecl_tFBD4E75517EBC2F40B9055F7785B4A0BEA27B70C 
{
public:
	// System.String System.Xml.XmlNamespaceManager_NsDecl::Prefix
	String_t* ___Prefix_0;
	// System.String System.Xml.XmlNamespaceManager_NsDecl::Uri
	String_t* ___Uri_1;

public:
	inline static int32_t get_offset_of_Prefix_0() { return static_cast<int32_t>(offsetof(NsDecl_tFBD4E75517EBC2F40B9055F7785B4A0BEA27B70C, ___Prefix_0)); }
	inline String_t* get_Prefix_0() const { return ___Prefix_0; }
	inline String_t** get_address_of_Prefix_0() { return &___Prefix_0; }
	inline void set_Prefix_0(String_t* value)
	{
		___Prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_0), value);
	}

	inline static int32_t get_offset_of_Uri_1() { return static_cast<int32_t>(offsetof(NsDecl_tFBD4E75517EBC2F40B9055F7785B4A0BEA27B70C, ___Uri_1)); }
	inline String_t* get_Uri_1() const { return ___Uri_1; }
	inline String_t** get_address_of_Uri_1() { return &___Uri_1; }
	inline void set_Uri_1(String_t* value)
	{
		___Uri_1 = value;
		Il2CppCodeGenWriteBarrier((&___Uri_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNamespaceManager/NsDecl
struct NsDecl_tFBD4E75517EBC2F40B9055F7785B4A0BEA27B70C_marshaled_pinvoke
{
	char* ___Prefix_0;
	char* ___Uri_1;
};
// Native definition for COM marshalling of System.Xml.XmlNamespaceManager/NsDecl
struct NsDecl_tFBD4E75517EBC2F40B9055F7785B4A0BEA27B70C_marshaled_com
{
	Il2CppChar* ___Prefix_0;
	Il2CppChar* ___Uri_1;
};
#endif // NSDECL_TFBD4E75517EBC2F40B9055F7785B4A0BEA27B70C_H
#ifndef NSSCOPE_TAE4D8382C99EA8EC689B83ABCB5A04FE6CDE41DB_H
#define NSSCOPE_TAE4D8382C99EA8EC689B83ABCB5A04FE6CDE41DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager_NsScope
struct  NsScope_tAE4D8382C99EA8EC689B83ABCB5A04FE6CDE41DB 
{
public:
	// System.Int32 System.Xml.XmlNamespaceManager_NsScope::DeclCount
	int32_t ___DeclCount_0;
	// System.String System.Xml.XmlNamespaceManager_NsScope::DefaultNamespace
	String_t* ___DefaultNamespace_1;

public:
	inline static int32_t get_offset_of_DeclCount_0() { return static_cast<int32_t>(offsetof(NsScope_tAE4D8382C99EA8EC689B83ABCB5A04FE6CDE41DB, ___DeclCount_0)); }
	inline int32_t get_DeclCount_0() const { return ___DeclCount_0; }
	inline int32_t* get_address_of_DeclCount_0() { return &___DeclCount_0; }
	inline void set_DeclCount_0(int32_t value)
	{
		___DeclCount_0 = value;
	}

	inline static int32_t get_offset_of_DefaultNamespace_1() { return static_cast<int32_t>(offsetof(NsScope_tAE4D8382C99EA8EC689B83ABCB5A04FE6CDE41DB, ___DefaultNamespace_1)); }
	inline String_t* get_DefaultNamespace_1() const { return ___DefaultNamespace_1; }
	inline String_t** get_address_of_DefaultNamespace_1() { return &___DefaultNamespace_1; }
	inline void set_DefaultNamespace_1(String_t* value)
	{
		___DefaultNamespace_1 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultNamespace_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNamespaceManager/NsScope
struct NsScope_tAE4D8382C99EA8EC689B83ABCB5A04FE6CDE41DB_marshaled_pinvoke
{
	int32_t ___DeclCount_0;
	char* ___DefaultNamespace_1;
};
// Native definition for COM marshalling of System.Xml.XmlNamespaceManager/NsScope
struct NsScope_tAE4D8382C99EA8EC689B83ABCB5A04FE6CDE41DB_marshaled_com
{
	int32_t ___DeclCount_0;
	Il2CppChar* ___DefaultNamespace_1;
};
#endif // NSSCOPE_TAE4D8382C99EA8EC689B83ABCB5A04FE6CDE41DB_H
#ifndef EMPTYNODELIST_TABF5FD9F0A8DC7FBC7808B3BACC780547EB48735_H
#define EMPTYNODELIST_TABF5FD9F0A8DC7FBC7808B3BACC780547EB48735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode_EmptyNodeList
struct  EmptyNodeList_tABF5FD9F0A8DC7FBC7808B3BACC780547EB48735  : public XmlNodeList_tD7AD9076C850FBF7CC2520D0B670B65037DB1E3B
{
public:

public:
};

struct EmptyNodeList_tABF5FD9F0A8DC7FBC7808B3BACC780547EB48735_StaticFields
{
public:
	// System.Collections.IEnumerator System.Xml.XmlNode_EmptyNodeList::emptyEnumerator
	RuntimeObject* ___emptyEnumerator_0;

public:
	inline static int32_t get_offset_of_emptyEnumerator_0() { return static_cast<int32_t>(offsetof(EmptyNodeList_tABF5FD9F0A8DC7FBC7808B3BACC780547EB48735_StaticFields, ___emptyEnumerator_0)); }
	inline RuntimeObject* get_emptyEnumerator_0() const { return ___emptyEnumerator_0; }
	inline RuntimeObject** get_address_of_emptyEnumerator_0() { return &___emptyEnumerator_0; }
	inline void set_emptyEnumerator_0(RuntimeObject* value)
	{
		___emptyEnumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptyEnumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYNODELIST_TABF5FD9F0A8DC7FBC7808B3BACC780547EB48735_H
#ifndef XMLNODELISTCHILDREN_T0DFA6642EB0E0CA24CB3F5AF8DC38D09D5656B2A_H
#define XMLNODELISTCHILDREN_T0DFA6642EB0E0CA24CB3F5AF8DC38D09D5656B2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeListChildren
struct  XmlNodeListChildren_t0DFA6642EB0E0CA24CB3F5AF8DC38D09D5656B2A  : public XmlNodeList_tD7AD9076C850FBF7CC2520D0B670B65037DB1E3B
{
public:
	// System.Xml.IHasXmlChildNode System.Xml.XmlNodeListChildren::parent
	RuntimeObject* ___parent_0;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(XmlNodeListChildren_t0DFA6642EB0E0CA24CB3F5AF8DC38D09D5656B2A, ___parent_0)); }
	inline RuntimeObject* get_parent_0() const { return ___parent_0; }
	inline RuntimeObject** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(RuntimeObject* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODELISTCHILDREN_T0DFA6642EB0E0CA24CB3F5AF8DC38D09D5656B2A_H
#ifndef XMLNODEREADER_TE00D164708D99621945F9F8A48BBB0BEA5FBA3BE_H
#define XMLNODEREADER_TE00D164708D99621945F9F8A48BBB0BEA5FBA3BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeReader
struct  XmlNodeReader_tE00D164708D99621945F9F8A48BBB0BEA5FBA3BE  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// System.Xml.XmlReader System.Xml.XmlNodeReader::entity
	XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * ___entity_2;
	// System.Xml.XmlNodeReaderImpl System.Xml.XmlNodeReader::source
	XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54 * ___source_3;
	// System.Boolean System.Xml.XmlNodeReader::entityInsideAttribute
	bool ___entityInsideAttribute_4;
	// System.Boolean System.Xml.XmlNodeReader::insideAttribute
	bool ___insideAttribute_5;

public:
	inline static int32_t get_offset_of_entity_2() { return static_cast<int32_t>(offsetof(XmlNodeReader_tE00D164708D99621945F9F8A48BBB0BEA5FBA3BE, ___entity_2)); }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * get_entity_2() const { return ___entity_2; }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 ** get_address_of_entity_2() { return &___entity_2; }
	inline void set_entity_2(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * value)
	{
		___entity_2 = value;
		Il2CppCodeGenWriteBarrier((&___entity_2), value);
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(XmlNodeReader_tE00D164708D99621945F9F8A48BBB0BEA5FBA3BE, ___source_3)); }
	inline XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54 * get_source_3() const { return ___source_3; }
	inline XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_entityInsideAttribute_4() { return static_cast<int32_t>(offsetof(XmlNodeReader_tE00D164708D99621945F9F8A48BBB0BEA5FBA3BE, ___entityInsideAttribute_4)); }
	inline bool get_entityInsideAttribute_4() const { return ___entityInsideAttribute_4; }
	inline bool* get_address_of_entityInsideAttribute_4() { return &___entityInsideAttribute_4; }
	inline void set_entityInsideAttribute_4(bool value)
	{
		___entityInsideAttribute_4 = value;
	}

	inline static int32_t get_offset_of_insideAttribute_5() { return static_cast<int32_t>(offsetof(XmlNodeReader_tE00D164708D99621945F9F8A48BBB0BEA5FBA3BE, ___insideAttribute_5)); }
	inline bool get_insideAttribute_5() const { return ___insideAttribute_5; }
	inline bool* get_address_of_insideAttribute_5() { return &___insideAttribute_5; }
	inline void set_insideAttribute_5(bool value)
	{
		___insideAttribute_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEREADER_TE00D164708D99621945F9F8A48BBB0BEA5FBA3BE_H
#ifndef XMLNOTATION_T38B042D026BD23802CEE72EAB2245B99F9FF54B1_H
#define XMLNOTATION_T38B042D026BD23802CEE72EAB2245B99F9FF54B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNotation
struct  XmlNotation_t38B042D026BD23802CEE72EAB2245B99F9FF54B1  : public XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A
{
public:
	// System.String System.Xml.XmlNotation::localName
	String_t* ___localName_5;
	// System.String System.Xml.XmlNotation::publicId
	String_t* ___publicId_6;
	// System.String System.Xml.XmlNotation::systemId
	String_t* ___systemId_7;
	// System.String System.Xml.XmlNotation::prefix
	String_t* ___prefix_8;

public:
	inline static int32_t get_offset_of_localName_5() { return static_cast<int32_t>(offsetof(XmlNotation_t38B042D026BD23802CEE72EAB2245B99F9FF54B1, ___localName_5)); }
	inline String_t* get_localName_5() const { return ___localName_5; }
	inline String_t** get_address_of_localName_5() { return &___localName_5; }
	inline void set_localName_5(String_t* value)
	{
		___localName_5 = value;
		Il2CppCodeGenWriteBarrier((&___localName_5), value);
	}

	inline static int32_t get_offset_of_publicId_6() { return static_cast<int32_t>(offsetof(XmlNotation_t38B042D026BD23802CEE72EAB2245B99F9FF54B1, ___publicId_6)); }
	inline String_t* get_publicId_6() const { return ___publicId_6; }
	inline String_t** get_address_of_publicId_6() { return &___publicId_6; }
	inline void set_publicId_6(String_t* value)
	{
		___publicId_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_6), value);
	}

	inline static int32_t get_offset_of_systemId_7() { return static_cast<int32_t>(offsetof(XmlNotation_t38B042D026BD23802CEE72EAB2245B99F9FF54B1, ___systemId_7)); }
	inline String_t* get_systemId_7() const { return ___systemId_7; }
	inline String_t** get_address_of_systemId_7() { return &___systemId_7; }
	inline void set_systemId_7(String_t* value)
	{
		___systemId_7 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_7), value);
	}

	inline static int32_t get_offset_of_prefix_8() { return static_cast<int32_t>(offsetof(XmlNotation_t38B042D026BD23802CEE72EAB2245B99F9FF54B1, ___prefix_8)); }
	inline String_t* get_prefix_8() const { return ___prefix_8; }
	inline String_t** get_address_of_prefix_8() { return &___prefix_8; }
	inline void set_prefix_8(String_t* value)
	{
		___prefix_8 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNOTATION_T38B042D026BD23802CEE72EAB2245B99F9FF54B1_H
#ifndef XMLTEXTREADER_T3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3_H
#define XMLTEXTREADER_T3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReader
struct  XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// System.Xml.XmlTextReader System.Xml.XmlTextReader::entity
	XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 * ___entity_2;
	// Mono.Xml2.XmlTextReader System.Xml.XmlTextReader::source
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD * ___source_3;
	// System.Boolean System.Xml.XmlTextReader::entityInsideAttribute
	bool ___entityInsideAttribute_4;
	// System.Boolean System.Xml.XmlTextReader::insideAttribute
	bool ___insideAttribute_5;
	// System.Collections.Generic.Stack`1<System.String> System.Xml.XmlTextReader::entityNameStack
	Stack_1_t36D6DE3CB7FEC6EDF6EA749E72AD210F334D8F45 * ___entityNameStack_6;

public:
	inline static int32_t get_offset_of_entity_2() { return static_cast<int32_t>(offsetof(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3, ___entity_2)); }
	inline XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 * get_entity_2() const { return ___entity_2; }
	inline XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 ** get_address_of_entity_2() { return &___entity_2; }
	inline void set_entity_2(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 * value)
	{
		___entity_2 = value;
		Il2CppCodeGenWriteBarrier((&___entity_2), value);
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3, ___source_3)); }
	inline XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD * get_source_3() const { return ___source_3; }
	inline XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_entityInsideAttribute_4() { return static_cast<int32_t>(offsetof(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3, ___entityInsideAttribute_4)); }
	inline bool get_entityInsideAttribute_4() const { return ___entityInsideAttribute_4; }
	inline bool* get_address_of_entityInsideAttribute_4() { return &___entityInsideAttribute_4; }
	inline void set_entityInsideAttribute_4(bool value)
	{
		___entityInsideAttribute_4 = value;
	}

	inline static int32_t get_offset_of_insideAttribute_5() { return static_cast<int32_t>(offsetof(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3, ___insideAttribute_5)); }
	inline bool get_insideAttribute_5() const { return ___insideAttribute_5; }
	inline bool* get_address_of_insideAttribute_5() { return &___insideAttribute_5; }
	inline void set_insideAttribute_5(bool value)
	{
		___insideAttribute_5 = value;
	}

	inline static int32_t get_offset_of_entityNameStack_6() { return static_cast<int32_t>(offsetof(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3, ___entityNameStack_6)); }
	inline Stack_1_t36D6DE3CB7FEC6EDF6EA749E72AD210F334D8F45 * get_entityNameStack_6() const { return ___entityNameStack_6; }
	inline Stack_1_t36D6DE3CB7FEC6EDF6EA749E72AD210F334D8F45 ** get_address_of_entityNameStack_6() { return &___entityNameStack_6; }
	inline void set_entityNameStack_6(Stack_1_t36D6DE3CB7FEC6EDF6EA749E72AD210F334D8F45 * value)
	{
		___entityNameStack_6 = value;
		Il2CppCodeGenWriteBarrier((&___entityNameStack_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTREADER_T3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3_H
#ifndef XMLURLRESOLVER_TD3E7BD8A4D69B96E8EB009EA459545CFE399FA83_H
#define XMLURLRESOLVER_TD3E7BD8A4D69B96E8EB009EA459545CFE399FA83_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUrlResolver
struct  XmlUrlResolver_tD3E7BD8A4D69B96E8EB009EA459545CFE399FA83  : public XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C
{
public:
	// System.Net.ICredentials System.Xml.XmlUrlResolver::credential
	RuntimeObject* ___credential_0;

public:
	inline static int32_t get_offset_of_credential_0() { return static_cast<int32_t>(offsetof(XmlUrlResolver_tD3E7BD8A4D69B96E8EB009EA459545CFE399FA83, ___credential_0)); }
	inline RuntimeObject* get_credential_0() const { return ___credential_0; }
	inline RuntimeObject** get_address_of_credential_0() { return &___credential_0; }
	inline void set_credential_0(RuntimeObject* value)
	{
		___credential_0 = value;
		Il2CppCodeGenWriteBarrier((&___credential_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLURLRESOLVER_TD3E7BD8A4D69B96E8EB009EA459545CFE399FA83_H
#ifndef DTDATTRIBUTEOCCURENCETYPE_T29080CCF7A7910C586B173D49EA08A534E25F5C1_H
#define DTDATTRIBUTEOCCURENCETYPE_T29080CCF7A7910C586B173D49EA08A534E25F5C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttributeOccurenceType
struct  DTDAttributeOccurenceType_t29080CCF7A7910C586B173D49EA08A534E25F5C1 
{
public:
	// System.Int32 Mono.Xml.DTDAttributeOccurenceType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDAttributeOccurenceType_t29080CCF7A7910C586B173D49EA08A534E25F5C1, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTRIBUTEOCCURENCETYPE_T29080CCF7A7910C586B173D49EA08A534E25F5C1_H
#ifndef DTDCOLLECTIONBASE_TBA57464C8928A0229E33734BCF832A90138D0E4F_H
#define DTDCOLLECTIONBASE_TBA57464C8928A0229E33734BCF832A90138D0E4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDCollectionBase
struct  DTDCollectionBase_tBA57464C8928A0229E33734BCF832A90138D0E4F  : public DictionaryBase_t415336F093F8CDFAF278AA55A0F4A0FD46780582
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDCollectionBase::root
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * ___root_5;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDCollectionBase_tBA57464C8928A0229E33734BCF832A90138D0E4F, ___root_5)); }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCOLLECTIONBASE_TBA57464C8928A0229E33734BCF832A90138D0E4F_H
#ifndef DTDCONTENTORDERTYPE_TA131422D00D6DE5AEDAACD0D6A47DA17943C7DD0_H
#define DTDCONTENTORDERTYPE_TA131422D00D6DE5AEDAACD0D6A47DA17943C7DD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentOrderType
struct  DTDContentOrderType_tA131422D00D6DE5AEDAACD0D6A47DA17943C7DD0 
{
public:
	// System.Int32 Mono.Xml.DTDContentOrderType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDContentOrderType_tA131422D00D6DE5AEDAACD0D6A47DA17943C7DD0, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTORDERTYPE_TA131422D00D6DE5AEDAACD0D6A47DA17943C7DD0_H
#ifndef DTDENTITYDECLARATION_T3264D7AD9136E2D78D76857FFB546C5733C507C4_H
#define DTDENTITYDECLARATION_T3264D7AD9136E2D78D76857FFB546C5733C507C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityDeclaration
struct  DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4  : public DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0
{
public:
	// System.String Mono.Xml.DTDEntityDeclaration::entityValue
	String_t* ___entityValue_15;
	// System.String Mono.Xml.DTDEntityDeclaration::notationName
	String_t* ___notationName_16;
	// System.Collections.ArrayList Mono.Xml.DTDEntityDeclaration::ReferencingEntities
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___ReferencingEntities_17;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::scanned
	bool ___scanned_18;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::recursed
	bool ___recursed_19;
	// System.Boolean Mono.Xml.DTDEntityDeclaration::hasExternalReference
	bool ___hasExternalReference_20;

public:
	inline static int32_t get_offset_of_entityValue_15() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4, ___entityValue_15)); }
	inline String_t* get_entityValue_15() const { return ___entityValue_15; }
	inline String_t** get_address_of_entityValue_15() { return &___entityValue_15; }
	inline void set_entityValue_15(String_t* value)
	{
		___entityValue_15 = value;
		Il2CppCodeGenWriteBarrier((&___entityValue_15), value);
	}

	inline static int32_t get_offset_of_notationName_16() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4, ___notationName_16)); }
	inline String_t* get_notationName_16() const { return ___notationName_16; }
	inline String_t** get_address_of_notationName_16() { return &___notationName_16; }
	inline void set_notationName_16(String_t* value)
	{
		___notationName_16 = value;
		Il2CppCodeGenWriteBarrier((&___notationName_16), value);
	}

	inline static int32_t get_offset_of_ReferencingEntities_17() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4, ___ReferencingEntities_17)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_ReferencingEntities_17() const { return ___ReferencingEntities_17; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_ReferencingEntities_17() { return &___ReferencingEntities_17; }
	inline void set_ReferencingEntities_17(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___ReferencingEntities_17 = value;
		Il2CppCodeGenWriteBarrier((&___ReferencingEntities_17), value);
	}

	inline static int32_t get_offset_of_scanned_18() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4, ___scanned_18)); }
	inline bool get_scanned_18() const { return ___scanned_18; }
	inline bool* get_address_of_scanned_18() { return &___scanned_18; }
	inline void set_scanned_18(bool value)
	{
		___scanned_18 = value;
	}

	inline static int32_t get_offset_of_recursed_19() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4, ___recursed_19)); }
	inline bool get_recursed_19() const { return ___recursed_19; }
	inline bool* get_address_of_recursed_19() { return &___recursed_19; }
	inline void set_recursed_19(bool value)
	{
		___recursed_19 = value;
	}

	inline static int32_t get_offset_of_hasExternalReference_20() { return static_cast<int32_t>(offsetof(DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4, ___hasExternalReference_20)); }
	inline bool get_hasExternalReference_20() const { return ___hasExternalReference_20; }
	inline bool* get_address_of_hasExternalReference_20() { return &___hasExternalReference_20; }
	inline void set_hasExternalReference_20(bool value)
	{
		___hasExternalReference_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYDECLARATION_T3264D7AD9136E2D78D76857FFB546C5733C507C4_H
#ifndef DTDOCCURENCE_TA32D6D2BDB3ACD58BADB3B855835C5C319851208_H
#define DTDOCCURENCE_TA32D6D2BDB3ACD58BADB3B855835C5C319851208_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDOccurence
struct  DTDOccurence_tA32D6D2BDB3ACD58BADB3B855835C5C319851208 
{
public:
	// System.Int32 Mono.Xml.DTDOccurence::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DTDOccurence_tA32D6D2BDB3ACD58BADB3B855835C5C319851208, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDOCCURENCE_TA32D6D2BDB3ACD58BADB3B855835C5C319851208_H
#ifndef DTDPARAMETERENTITYDECLARATION_TD34D6D3351BDB37AB92E73D4A58C2A03E5724D7B_H
#define DTDPARAMETERENTITYDECLARATION_TD34D6D3351BDB37AB92E73D4A58C2A03E5724D7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDParameterEntityDeclaration
struct  DTDParameterEntityDeclaration_tD34D6D3351BDB37AB92E73D4A58C2A03E5724D7B  : public DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARAMETERENTITYDECLARATION_TD34D6D3351BDB37AB92E73D4A58C2A03E5724D7B_H
#ifndef DTDINPUTSTATE_T5A3E4D094A604D4B3774B3E36AD681F304E565C3_H
#define DTDINPUTSTATE_T5A3E4D094A604D4B3774B3E36AD681F304E565C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader_DtdInputState
struct  DtdInputState_t5A3E4D094A604D4B3774B3E36AD681F304E565C3 
{
public:
	// System.Int32 Mono.Xml2.XmlTextReader_DtdInputState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DtdInputState_t5A3E4D094A604D4B3774B3E36AD681F304E565C3, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDINPUTSTATE_T5A3E4D094A604D4B3774B3E36AD681F304E565C3_H
#ifndef ENUMERATOR_T6396B5CBCF3FCE7769AB33B71210BFD48AAE9FB4_H
#define ENUMERATOR_T6396B5CBCF3FCE7769AB33B71210BFD48AAE9FB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>>
struct  Enumerator_t6396B5CBCF3FCE7769AB33B71210BFD48AAE9FB4 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::l
	List_1_t341C4DA0AC3E19C291DAB1B40650E946D9B6728B * ___l_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	KeyValuePair_2_t0067BC83D8F125202440DC6183F788BB0802D33D  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t6396B5CBCF3FCE7769AB33B71210BFD48AAE9FB4, ___l_0)); }
	inline List_1_t341C4DA0AC3E19C291DAB1B40650E946D9B6728B * get_l_0() const { return ___l_0; }
	inline List_1_t341C4DA0AC3E19C291DAB1B40650E946D9B6728B ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t341C4DA0AC3E19C291DAB1B40650E946D9B6728B * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t6396B5CBCF3FCE7769AB33B71210BFD48AAE9FB4, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t6396B5CBCF3FCE7769AB33B71210BFD48AAE9FB4, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t6396B5CBCF3FCE7769AB33B71210BFD48AAE9FB4, ___current_3)); }
	inline KeyValuePair_2_t0067BC83D8F125202440DC6183F788BB0802D33D  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t0067BC83D8F125202440DC6183F788BB0802D33D * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t0067BC83D8F125202440DC6183F788BB0802D33D  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T6396B5CBCF3FCE7769AB33B71210BFD48AAE9FB4_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_8)); }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * get_data_8() const { return ___data_8; }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T_H
#ifndef DATETIMESTYLES_T4FCC05296E35516E6FE83CE0FD0E5B1079A3B669_H
#define DATETIMESTYLES_T4FCC05296E35516E6FE83CE0FD0E5B1079A3B669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.DateTimeStyles
struct  DateTimeStyles_t4FCC05296E35516E6FE83CE0FD0E5B1079A3B669 
{
public:
	// System.Int32 System.Globalization.DateTimeStyles::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeStyles_t4FCC05296E35516E6FE83CE0FD0E5B1079A3B669, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMESTYLES_T4FCC05296E35516E6FE83CE0FD0E5B1079A3B669_H
#ifndef CONFORMANCELEVEL_TBA47CA81D3009DF2AD8DC7CE769E6982D1EA96CB_H
#define CONFORMANCELEVEL_TBA47CA81D3009DF2AD8DC7CE769E6982D1EA96CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_tBA47CA81D3009DF2AD8DC7CE769E6982D1EA96CB 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConformanceLevel_tBA47CA81D3009DF2AD8DC7CE769E6982D1EA96CB, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_TBA47CA81D3009DF2AD8DC7CE769E6982D1EA96CB_H
#ifndef ENTITYHANDLING_TF590E11CFBDA825293A1964A1234087114F4B98D_H
#define ENTITYHANDLING_TF590E11CFBDA825293A1964A1234087114F4B98D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.EntityHandling
struct  EntityHandling_tF590E11CFBDA825293A1964A1234087114F4B98D 
{
public:
	// System.Int32 System.Xml.EntityHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EntityHandling_tF590E11CFBDA825293A1964A1234087114F4B98D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYHANDLING_TF590E11CFBDA825293A1964A1234087114F4B98D_H
#ifndef FORMATTING_T2EC8ADDABC4B928B58EA5351103138C3579EC1E4_H
#define FORMATTING_T2EC8ADDABC4B928B58EA5351103138C3579EC1E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Formatting
struct  Formatting_t2EC8ADDABC4B928B58EA5351103138C3579EC1E4 
{
public:
	// System.Int32 System.Xml.Formatting::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Formatting_t2EC8ADDABC4B928B58EA5351103138C3579EC1E4, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_T2EC8ADDABC4B928B58EA5351103138C3579EC1E4_H
#ifndef NAMESPACEHANDLING_T2F02A279F33A1348B8E4AB835A10E0C06237939A_H
#define NAMESPACEHANDLING_T2F02A279F33A1348B8E4AB835A10E0C06237939A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NamespaceHandling
struct  NamespaceHandling_t2F02A279F33A1348B8E4AB835A10E0C06237939A 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NamespaceHandling_t2F02A279F33A1348B8E4AB835A10E0C06237939A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEHANDLING_T2F02A279F33A1348B8E4AB835A10E0C06237939A_H
#ifndef NEWLINEHANDLING_TC5B27F2FDF6CAA10BBBB61E1F03BAF891FF30FC9_H
#define NEWLINEHANDLING_TC5B27F2FDF6CAA10BBBB61E1F03BAF891FF30FC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_tC5B27F2FDF6CAA10BBBB61E1F03BAF891FF30FC9 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NewLineHandling_tC5B27F2FDF6CAA10BBBB61E1F03BAF891FF30FC9, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_TC5B27F2FDF6CAA10BBBB61E1F03BAF891FF30FC9_H
#ifndef READSTATE_TEB8765F42B4F591ECA14C88336750FF5E198AAF1_H
#define READSTATE_TEB8765F42B4F591ECA14C88336750FF5E198AAF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadState
struct  ReadState_tEB8765F42B4F591ECA14C88336750FF5E198AAF1 
{
public:
	// System.Int32 System.Xml.ReadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReadState_tEB8765F42B4F591ECA14C88336750FF5E198AAF1, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_TEB8765F42B4F591ECA14C88336750FF5E198AAF1_H
#ifndef XMLSCHEMAVALIDATIONFLAGS_TECE1A6138368A7455AC47A24FC5FD4920CDB8ACA_H
#define XMLSCHEMAVALIDATIONFLAGS_TECE1A6138368A7455AC47A24FC5FD4920CDB8ACA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidationFlags
struct  XmlSchemaValidationFlags_tECE1A6138368A7455AC47A24FC5FD4920CDB8ACA 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidationFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSchemaValidationFlags_tECE1A6138368A7455AC47A24FC5FD4920CDB8ACA, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDATIONFLAGS_TECE1A6138368A7455AC47A24FC5FD4920CDB8ACA_H
#ifndef VALIDATIONTYPE_T31525A080D050BCFADE54364812432A4C5BDBE63_H
#define VALIDATIONTYPE_T31525A080D050BCFADE54364812432A4C5BDBE63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidationType
struct  ValidationType_t31525A080D050BCFADE54364812432A4C5BDBE63 
{
public:
	// System.Int32 System.Xml.ValidationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ValidationType_t31525A080D050BCFADE54364812432A4C5BDBE63, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONTYPE_T31525A080D050BCFADE54364812432A4C5BDBE63_H
#ifndef WHITESPACEHANDLING_T49897E1FC16BB3483FAEE35AE0F396573EE124B5_H
#define WHITESPACEHANDLING_T49897E1FC16BB3483FAEE35AE0F396573EE124B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WhitespaceHandling
struct  WhitespaceHandling_t49897E1FC16BB3483FAEE35AE0F396573EE124B5 
{
public:
	// System.Int32 System.Xml.WhitespaceHandling::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WhitespaceHandling_t49897E1FC16BB3483FAEE35AE0F396573EE124B5, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WHITESPACEHANDLING_T49897E1FC16BB3483FAEE35AE0F396573EE124B5_H
#ifndef WRITESTATE_T77FFF7A78C6BEB469AF5904B89980997CDC8735F_H
#define WRITESTATE_T77FFF7A78C6BEB469AF5904B89980997CDC8735F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WriteState
struct  WriteState_t77FFF7A78C6BEB469AF5904B89980997CDC8735F 
{
public:
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WriteState_t77FFF7A78C6BEB469AF5904B89980997CDC8735F, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_T77FFF7A78C6BEB469AF5904B89980997CDC8735F_H
#ifndef XMLCHARACTERDATA_T1733676F46934DE88DE8DEC9CF47190ABAE32F07_H
#define XMLCHARACTERDATA_T1733676F46934DE88DE8DEC9CF47190ABAE32F07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharacterData
struct  XmlCharacterData_t1733676F46934DE88DE8DEC9CF47190ABAE32F07  : public XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23
{
public:
	// System.String System.Xml.XmlCharacterData::data
	String_t* ___data_6;

public:
	inline static int32_t get_offset_of_data_6() { return static_cast<int32_t>(offsetof(XmlCharacterData_t1733676F46934DE88DE8DEC9CF47190ABAE32F07, ___data_6)); }
	inline String_t* get_data_6() const { return ___data_6; }
	inline String_t** get_address_of_data_6() { return &___data_6; }
	inline void set_data_6(String_t* value)
	{
		___data_6 = value;
		Il2CppCodeGenWriteBarrier((&___data_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHARACTERDATA_T1733676F46934DE88DE8DEC9CF47190ABAE32F07_H
#ifndef XMLDECLARATION_T4A6A6879823F50E37FD51260AB9C67C540134C28_H
#define XMLDECLARATION_T4A6A6879823F50E37FD51260AB9C67C540134C28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDeclaration
struct  XmlDeclaration_t4A6A6879823F50E37FD51260AB9C67C540134C28  : public XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23
{
public:
	// System.String System.Xml.XmlDeclaration::encoding
	String_t* ___encoding_6;
	// System.String System.Xml.XmlDeclaration::standalone
	String_t* ___standalone_7;
	// System.String System.Xml.XmlDeclaration::version
	String_t* ___version_8;

public:
	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(XmlDeclaration_t4A6A6879823F50E37FD51260AB9C67C540134C28, ___encoding_6)); }
	inline String_t* get_encoding_6() const { return ___encoding_6; }
	inline String_t** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(String_t* value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_standalone_7() { return static_cast<int32_t>(offsetof(XmlDeclaration_t4A6A6879823F50E37FD51260AB9C67C540134C28, ___standalone_7)); }
	inline String_t* get_standalone_7() const { return ___standalone_7; }
	inline String_t** get_address_of_standalone_7() { return &___standalone_7; }
	inline void set_standalone_7(String_t* value)
	{
		___standalone_7 = value;
		Il2CppCodeGenWriteBarrier((&___standalone_7), value);
	}

	inline static int32_t get_offset_of_version_8() { return static_cast<int32_t>(offsetof(XmlDeclaration_t4A6A6879823F50E37FD51260AB9C67C540134C28, ___version_8)); }
	inline String_t* get_version_8() const { return ___version_8; }
	inline String_t** get_address_of_version_8() { return &___version_8; }
	inline void set_version_8(String_t* value)
	{
		___version_8 = value;
		Il2CppCodeGenWriteBarrier((&___version_8), value);
	}
};

struct XmlDeclaration_t4A6A6879823F50E37FD51260AB9C67C540134C28_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlDeclaration::<>f__switchU24map30
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map30_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map30_9() { return static_cast<int32_t>(offsetof(XmlDeclaration_t4A6A6879823F50E37FD51260AB9C67C540134C28_StaticFields, ___U3CU3Ef__switchU24map30_9)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map30_9() const { return ___U3CU3Ef__switchU24map30_9; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map30_9() { return &___U3CU3Ef__switchU24map30_9; }
	inline void set_U3CU3Ef__switchU24map30_9(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map30_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map30_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLARATION_T4A6A6879823F50E37FD51260AB9C67C540134C28_H
#ifndef XMLDOCUMENTTYPE_TF87D4D3AB760B0E3D65B139D0CA3605803F37093_H
#define XMLDOCUMENTTYPE_TF87D4D3AB760B0E3D65B139D0CA3605803F37093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentType
struct  XmlDocumentType_tF87D4D3AB760B0E3D65B139D0CA3605803F37093  : public XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23
{
public:
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::entities
	XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246 * ___entities_6;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::notations
	XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246 * ___notations_7;
	// Mono.Xml.DTDObjectModel System.Xml.XmlDocumentType::dtd
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * ___dtd_8;

public:
	inline static int32_t get_offset_of_entities_6() { return static_cast<int32_t>(offsetof(XmlDocumentType_tF87D4D3AB760B0E3D65B139D0CA3605803F37093, ___entities_6)); }
	inline XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246 * get_entities_6() const { return ___entities_6; }
	inline XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246 ** get_address_of_entities_6() { return &___entities_6; }
	inline void set_entities_6(XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246 * value)
	{
		___entities_6 = value;
		Il2CppCodeGenWriteBarrier((&___entities_6), value);
	}

	inline static int32_t get_offset_of_notations_7() { return static_cast<int32_t>(offsetof(XmlDocumentType_tF87D4D3AB760B0E3D65B139D0CA3605803F37093, ___notations_7)); }
	inline XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246 * get_notations_7() const { return ___notations_7; }
	inline XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246 ** get_address_of_notations_7() { return &___notations_7; }
	inline void set_notations_7(XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246 * value)
	{
		___notations_7 = value;
		Il2CppCodeGenWriteBarrier((&___notations_7), value);
	}

	inline static int32_t get_offset_of_dtd_8() { return static_cast<int32_t>(offsetof(XmlDocumentType_tF87D4D3AB760B0E3D65B139D0CA3605803F37093, ___dtd_8)); }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * get_dtd_8() const { return ___dtd_8; }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E ** get_address_of_dtd_8() { return &___dtd_8; }
	inline void set_dtd_8(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * value)
	{
		___dtd_8 = value;
		Il2CppCodeGenWriteBarrier((&___dtd_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTTYPE_TF87D4D3AB760B0E3D65B139D0CA3605803F37093_H
#ifndef XMLELEMENT_T1E0443656F6ED079E0C45153B25AE0A39E31CFF9_H
#define XMLELEMENT_T1E0443656F6ED079E0C45153B25AE0A39E31CFF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlElement
struct  XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9  : public XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23
{
public:
	// System.Xml.XmlAttributeCollection System.Xml.XmlElement::attributes
	XmlAttributeCollection_t0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9 * ___attributes_6;
	// System.Xml.XmlNameEntry System.Xml.XmlElement::name
	XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768 * ___name_7;
	// System.Xml.XmlLinkedNode System.Xml.XmlElement::lastLinkedChild
	XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * ___lastLinkedChild_8;
	// System.Boolean System.Xml.XmlElement::isNotEmpty
	bool ___isNotEmpty_9;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlElement::schemaInfo
	RuntimeObject* ___schemaInfo_10;

public:
	inline static int32_t get_offset_of_attributes_6() { return static_cast<int32_t>(offsetof(XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9, ___attributes_6)); }
	inline XmlAttributeCollection_t0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9 * get_attributes_6() const { return ___attributes_6; }
	inline XmlAttributeCollection_t0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9 ** get_address_of_attributes_6() { return &___attributes_6; }
	inline void set_attributes_6(XmlAttributeCollection_t0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9 * value)
	{
		___attributes_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_6), value);
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9, ___name_7)); }
	inline XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768 * get_name_7() const { return ___name_7; }
	inline XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768 ** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768 * value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_8() { return static_cast<int32_t>(offsetof(XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9, ___lastLinkedChild_8)); }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * get_lastLinkedChild_8() const { return ___lastLinkedChild_8; }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 ** get_address_of_lastLinkedChild_8() { return &___lastLinkedChild_8; }
	inline void set_lastLinkedChild_8(XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * value)
	{
		___lastLinkedChild_8 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_8), value);
	}

	inline static int32_t get_offset_of_isNotEmpty_9() { return static_cast<int32_t>(offsetof(XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9, ___isNotEmpty_9)); }
	inline bool get_isNotEmpty_9() const { return ___isNotEmpty_9; }
	inline bool* get_address_of_isNotEmpty_9() { return &___isNotEmpty_9; }
	inline void set_isNotEmpty_9(bool value)
	{
		___isNotEmpty_9 = value;
	}

	inline static int32_t get_offset_of_schemaInfo_10() { return static_cast<int32_t>(offsetof(XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9, ___schemaInfo_10)); }
	inline RuntimeObject* get_schemaInfo_10() const { return ___schemaInfo_10; }
	inline RuntimeObject** get_address_of_schemaInfo_10() { return &___schemaInfo_10; }
	inline void set_schemaInfo_10(RuntimeObject* value)
	{
		___schemaInfo_10 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENT_T1E0443656F6ED079E0C45153B25AE0A39E31CFF9_H
#ifndef XMLENTITYREFERENCE_TA8468650AA9BE800351BA5A6BE748953AA4E23F3_H
#define XMLENTITYREFERENCE_TA8468650AA9BE800351BA5A6BE748953AA4E23F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntityReference
struct  XmlEntityReference_tA8468650AA9BE800351BA5A6BE748953AA4E23F3  : public XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23
{
public:
	// System.String System.Xml.XmlEntityReference::entityName
	String_t* ___entityName_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntityReference::lastLinkedChild
	XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * ___lastLinkedChild_7;

public:
	inline static int32_t get_offset_of_entityName_6() { return static_cast<int32_t>(offsetof(XmlEntityReference_tA8468650AA9BE800351BA5A6BE748953AA4E23F3, ___entityName_6)); }
	inline String_t* get_entityName_6() const { return ___entityName_6; }
	inline String_t** get_address_of_entityName_6() { return &___entityName_6; }
	inline void set_entityName_6(String_t* value)
	{
		___entityName_6 = value;
		Il2CppCodeGenWriteBarrier((&___entityName_6), value);
	}

	inline static int32_t get_offset_of_lastLinkedChild_7() { return static_cast<int32_t>(offsetof(XmlEntityReference_tA8468650AA9BE800351BA5A6BE748953AA4E23F3, ___lastLinkedChild_7)); }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * get_lastLinkedChild_7() const { return ___lastLinkedChild_7; }
	inline XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 ** get_address_of_lastLinkedChild_7() { return &___lastLinkedChild_7; }
	inline void set_lastLinkedChild_7(XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23 * value)
	{
		___lastLinkedChild_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastLinkedChild_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITYREFERENCE_TA8468650AA9BE800351BA5A6BE748953AA4E23F3_H
#ifndef XMLEXCEPTION_TC3F8843762100626FC686CF0862B5545BCB261A1_H
#define XMLEXCEPTION_TC3F8843762100626FC686CF0862B5545BCB261A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlException
struct  XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1  : public SystemException_t81A06ADD519539D252788D75E44AFFBE6580E301
{
public:
	// System.Int32 System.Xml.XmlException::lineNumber
	int32_t ___lineNumber_11;
	// System.Int32 System.Xml.XmlException::linePosition
	int32_t ___linePosition_12;
	// System.String System.Xml.XmlException::sourceUri
	String_t* ___sourceUri_13;
	// System.String System.Xml.XmlException::res
	String_t* ___res_14;
	// System.String[] System.Xml.XmlException::messages
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___messages_15;

public:
	inline static int32_t get_offset_of_lineNumber_11() { return static_cast<int32_t>(offsetof(XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1, ___lineNumber_11)); }
	inline int32_t get_lineNumber_11() const { return ___lineNumber_11; }
	inline int32_t* get_address_of_lineNumber_11() { return &___lineNumber_11; }
	inline void set_lineNumber_11(int32_t value)
	{
		___lineNumber_11 = value;
	}

	inline static int32_t get_offset_of_linePosition_12() { return static_cast<int32_t>(offsetof(XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1, ___linePosition_12)); }
	inline int32_t get_linePosition_12() const { return ___linePosition_12; }
	inline int32_t* get_address_of_linePosition_12() { return &___linePosition_12; }
	inline void set_linePosition_12(int32_t value)
	{
		___linePosition_12 = value;
	}

	inline static int32_t get_offset_of_sourceUri_13() { return static_cast<int32_t>(offsetof(XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1, ___sourceUri_13)); }
	inline String_t* get_sourceUri_13() const { return ___sourceUri_13; }
	inline String_t** get_address_of_sourceUri_13() { return &___sourceUri_13; }
	inline void set_sourceUri_13(String_t* value)
	{
		___sourceUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_13), value);
	}

	inline static int32_t get_offset_of_res_14() { return static_cast<int32_t>(offsetof(XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1, ___res_14)); }
	inline String_t* get_res_14() const { return ___res_14; }
	inline String_t** get_address_of_res_14() { return &___res_14; }
	inline void set_res_14(String_t* value)
	{
		___res_14 = value;
		Il2CppCodeGenWriteBarrier((&___res_14), value);
	}

	inline static int32_t get_offset_of_messages_15() { return static_cast<int32_t>(offsetof(XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1, ___messages_15)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_messages_15() const { return ___messages_15; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_messages_15() { return &___messages_15; }
	inline void set_messages_15(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___messages_15 = value;
		Il2CppCodeGenWriteBarrier((&___messages_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLEXCEPTION_TC3F8843762100626FC686CF0862B5545BCB261A1_H
#ifndef XMLNODECHANGEDACTION_TAFD0821DB232A81EDA0AAB61B337E05F12FB3930_H
#define XMLNODECHANGEDACTION_TAFD0821DB232A81EDA0AAB61B337E05F12FB3930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedAction
struct  XmlNodeChangedAction_tAFD0821DB232A81EDA0AAB61B337E05F12FB3930 
{
public:
	// System.Int32 System.Xml.XmlNodeChangedAction::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeChangedAction_tAFD0821DB232A81EDA0AAB61B337E05F12FB3930, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDACTION_TAFD0821DB232A81EDA0AAB61B337E05F12FB3930_H
#ifndef XMLNODETYPE_TF9194E434ED56EA8A4CBE6EB7181252E20BB94FA_H
#define XMLNODETYPE_TF9194E434ED56EA8A4CBE6EB7181252E20BB94FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_tF9194E434ED56EA8A4CBE6EB7181252E20BB94FA 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlNodeType_tF9194E434ED56EA8A4CBE6EB7181252E20BB94FA, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_TF9194E434ED56EA8A4CBE6EB7181252E20BB94FA_H
#ifndef XMLPROCESSINGINSTRUCTION_TD4965C66FE3BF9B2AB08C8EE5AD339F1F51FBBEF_H
#define XMLPROCESSINGINSTRUCTION_TD4965C66FE3BF9B2AB08C8EE5AD339F1F51FBBEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlProcessingInstruction
struct  XmlProcessingInstruction_tD4965C66FE3BF9B2AB08C8EE5AD339F1F51FBBEF  : public XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23
{
public:
	// System.String System.Xml.XmlProcessingInstruction::target
	String_t* ___target_6;
	// System.String System.Xml.XmlProcessingInstruction::data
	String_t* ___data_7;

public:
	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(XmlProcessingInstruction_tD4965C66FE3BF9B2AB08C8EE5AD339F1F51FBBEF, ___target_6)); }
	inline String_t* get_target_6() const { return ___target_6; }
	inline String_t** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(String_t* value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_data_7() { return static_cast<int32_t>(offsetof(XmlProcessingInstruction_tD4965C66FE3BF9B2AB08C8EE5AD339F1F51FBBEF, ___data_7)); }
	inline String_t* get_data_7() const { return ___data_7; }
	inline String_t** get_address_of_data_7() { return &___data_7; }
	inline void set_data_7(String_t* value)
	{
		___data_7 = value;
		Il2CppCodeGenWriteBarrier((&___data_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPROCESSINGINSTRUCTION_TD4965C66FE3BF9B2AB08C8EE5AD339F1F51FBBEF_H
#ifndef COMMANDSTATE_T151B413EEEE8EFF4B4C087D57AE3D3F595D0DE72_H
#define COMMANDSTATE_T151B413EEEE8EFF4B4C087D57AE3D3F595D0DE72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderBinarySupport_CommandState
struct  CommandState_t151B413EEEE8EFF4B4C087D57AE3D3F595D0DE72 
{
public:
	// System.Int32 System.Xml.XmlReaderBinarySupport_CommandState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CommandState_t151B413EEEE8EFF4B4C087D57AE3D3F595D0DE72, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDSTATE_T151B413EEEE8EFF4B4C087D57AE3D3F595D0DE72_H
#ifndef XMLSPACE_T6A54DEF6D051D3E7B2509C3E101CE153C833F7E0_H
#define XMLSPACE_T6A54DEF6D051D3E7B2509C3E101CE153C833F7E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSpace
struct  XmlSpace_t6A54DEF6D051D3E7B2509C3E101CE153C833F7E0 
{
public:
	// System.Int32 System.Xml.XmlSpace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlSpace_t6A54DEF6D051D3E7B2509C3E101CE153C833F7E0, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSPACE_T6A54DEF6D051D3E7B2509C3E101CE153C833F7E0_H
#ifndef XMLTOKENIZEDTYPE_T8CDA56668C31147E44A370167F11C0F6ACE7E338_H
#define XMLTOKENIZEDTYPE_T8CDA56668C31147E44A370167F11C0F6ACE7E338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTokenizedType
struct  XmlTokenizedType_t8CDA56668C31147E44A370167F11C0F6ACE7E338 
{
public:
	// System.Int32 System.Xml.XmlTokenizedType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(XmlTokenizedType_t8CDA56668C31147E44A370167F11C0F6ACE7E338, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTOKENIZEDTYPE_T8CDA56668C31147E44A370167F11C0F6ACE7E338_H
#ifndef DTDATTLISTDECLARATIONCOLLECTION_TCDEF4AACF366E8B2298CAB3070235D432B854F55_H
#define DTDATTLISTDECLARATIONCOLLECTION_TCDEF4AACF366E8B2298CAB3070235D432B854F55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttListDeclarationCollection
struct  DTDAttListDeclarationCollection_tCDEF4AACF366E8B2298CAB3070235D432B854F55  : public DTDCollectionBase_tBA57464C8928A0229E33734BCF832A90138D0E4F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTLISTDECLARATIONCOLLECTION_TCDEF4AACF366E8B2298CAB3070235D432B854F55_H
#ifndef DTDATTRIBUTEDEFINITION_TF1F8AADA0445606A6837FA0CBA58A5237FC89854_H
#define DTDATTRIBUTEDEFINITION_TF1F8AADA0445606A6837FA0CBA58A5237FC89854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDAttributeDefinition
struct  DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854  : public DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B
{
public:
	// System.String Mono.Xml.DTDAttributeDefinition::name
	String_t* ___name_5;
	// System.Xml.Schema.XmlSchemaDatatype Mono.Xml.DTDAttributeDefinition::datatype
	XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19 * ___datatype_6;
	// System.Collections.ArrayList Mono.Xml.DTDAttributeDefinition::enumeratedLiterals
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___enumeratedLiterals_7;
	// System.String Mono.Xml.DTDAttributeDefinition::unresolvedDefault
	String_t* ___unresolvedDefault_8;
	// System.Collections.ArrayList Mono.Xml.DTDAttributeDefinition::enumeratedNotations
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___enumeratedNotations_9;
	// Mono.Xml.DTDAttributeOccurenceType Mono.Xml.DTDAttributeDefinition::occurenceType
	int32_t ___occurenceType_10;
	// System.String Mono.Xml.DTDAttributeDefinition::resolvedDefaultValue
	String_t* ___resolvedDefaultValue_11;

public:
	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854, ___name_5)); }
	inline String_t* get_name_5() const { return ___name_5; }
	inline String_t** get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(String_t* value)
	{
		___name_5 = value;
		Il2CppCodeGenWriteBarrier((&___name_5), value);
	}

	inline static int32_t get_offset_of_datatype_6() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854, ___datatype_6)); }
	inline XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19 * get_datatype_6() const { return ___datatype_6; }
	inline XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19 ** get_address_of_datatype_6() { return &___datatype_6; }
	inline void set_datatype_6(XmlSchemaDatatype_t3384DFF75456A27E693C24453B91B2B7A0CC2A19 * value)
	{
		___datatype_6 = value;
		Il2CppCodeGenWriteBarrier((&___datatype_6), value);
	}

	inline static int32_t get_offset_of_enumeratedLiterals_7() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854, ___enumeratedLiterals_7)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_enumeratedLiterals_7() const { return ___enumeratedLiterals_7; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_enumeratedLiterals_7() { return &___enumeratedLiterals_7; }
	inline void set_enumeratedLiterals_7(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___enumeratedLiterals_7 = value;
		Il2CppCodeGenWriteBarrier((&___enumeratedLiterals_7), value);
	}

	inline static int32_t get_offset_of_unresolvedDefault_8() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854, ___unresolvedDefault_8)); }
	inline String_t* get_unresolvedDefault_8() const { return ___unresolvedDefault_8; }
	inline String_t** get_address_of_unresolvedDefault_8() { return &___unresolvedDefault_8; }
	inline void set_unresolvedDefault_8(String_t* value)
	{
		___unresolvedDefault_8 = value;
		Il2CppCodeGenWriteBarrier((&___unresolvedDefault_8), value);
	}

	inline static int32_t get_offset_of_enumeratedNotations_9() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854, ___enumeratedNotations_9)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_enumeratedNotations_9() const { return ___enumeratedNotations_9; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_enumeratedNotations_9() { return &___enumeratedNotations_9; }
	inline void set_enumeratedNotations_9(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___enumeratedNotations_9 = value;
		Il2CppCodeGenWriteBarrier((&___enumeratedNotations_9), value);
	}

	inline static int32_t get_offset_of_occurenceType_10() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854, ___occurenceType_10)); }
	inline int32_t get_occurenceType_10() const { return ___occurenceType_10; }
	inline int32_t* get_address_of_occurenceType_10() { return &___occurenceType_10; }
	inline void set_occurenceType_10(int32_t value)
	{
		___occurenceType_10 = value;
	}

	inline static int32_t get_offset_of_resolvedDefaultValue_11() { return static_cast<int32_t>(offsetof(DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854, ___resolvedDefaultValue_11)); }
	inline String_t* get_resolvedDefaultValue_11() const { return ___resolvedDefaultValue_11; }
	inline String_t** get_address_of_resolvedDefaultValue_11() { return &___resolvedDefaultValue_11; }
	inline void set_resolvedDefaultValue_11(String_t* value)
	{
		___resolvedDefaultValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___resolvedDefaultValue_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDATTRIBUTEDEFINITION_TF1F8AADA0445606A6837FA0CBA58A5237FC89854_H
#ifndef DTDCONTENTMODEL_T4D28535870F690FE6FA369E5F22EA0C7411B2BFF_H
#define DTDCONTENTMODEL_T4D28535870F690FE6FA369E5F22EA0C7411B2BFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDContentModel
struct  DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF  : public DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B
{
public:
	// Mono.Xml.DTDObjectModel Mono.Xml.DTDContentModel::root
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * ___root_5;
	// Mono.Xml.DTDAutomata Mono.Xml.DTDContentModel::compiledAutomata
	DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * ___compiledAutomata_6;
	// System.String Mono.Xml.DTDContentModel::ownerElementName
	String_t* ___ownerElementName_7;
	// System.String Mono.Xml.DTDContentModel::elementName
	String_t* ___elementName_8;
	// Mono.Xml.DTDContentOrderType Mono.Xml.DTDContentModel::orderType
	int32_t ___orderType_9;
	// Mono.Xml.DTDContentModelCollection Mono.Xml.DTDContentModel::childModels
	DTDContentModelCollection_t0382EF04D32FF6333D71752F3C4E9CC1EE5B2664 * ___childModels_10;
	// Mono.Xml.DTDOccurence Mono.Xml.DTDContentModel::occurence
	int32_t ___occurence_11;

public:
	inline static int32_t get_offset_of_root_5() { return static_cast<int32_t>(offsetof(DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF, ___root_5)); }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * get_root_5() const { return ___root_5; }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E ** get_address_of_root_5() { return &___root_5; }
	inline void set_root_5(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * value)
	{
		___root_5 = value;
		Il2CppCodeGenWriteBarrier((&___root_5), value);
	}

	inline static int32_t get_offset_of_compiledAutomata_6() { return static_cast<int32_t>(offsetof(DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF, ___compiledAutomata_6)); }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * get_compiledAutomata_6() const { return ___compiledAutomata_6; }
	inline DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 ** get_address_of_compiledAutomata_6() { return &___compiledAutomata_6; }
	inline void set_compiledAutomata_6(DTDAutomata_t831152C2924A60A794E81D920F5504585B94BFE7 * value)
	{
		___compiledAutomata_6 = value;
		Il2CppCodeGenWriteBarrier((&___compiledAutomata_6), value);
	}

	inline static int32_t get_offset_of_ownerElementName_7() { return static_cast<int32_t>(offsetof(DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF, ___ownerElementName_7)); }
	inline String_t* get_ownerElementName_7() const { return ___ownerElementName_7; }
	inline String_t** get_address_of_ownerElementName_7() { return &___ownerElementName_7; }
	inline void set_ownerElementName_7(String_t* value)
	{
		___ownerElementName_7 = value;
		Il2CppCodeGenWriteBarrier((&___ownerElementName_7), value);
	}

	inline static int32_t get_offset_of_elementName_8() { return static_cast<int32_t>(offsetof(DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF, ___elementName_8)); }
	inline String_t* get_elementName_8() const { return ___elementName_8; }
	inline String_t** get_address_of_elementName_8() { return &___elementName_8; }
	inline void set_elementName_8(String_t* value)
	{
		___elementName_8 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_8), value);
	}

	inline static int32_t get_offset_of_orderType_9() { return static_cast<int32_t>(offsetof(DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF, ___orderType_9)); }
	inline int32_t get_orderType_9() const { return ___orderType_9; }
	inline int32_t* get_address_of_orderType_9() { return &___orderType_9; }
	inline void set_orderType_9(int32_t value)
	{
		___orderType_9 = value;
	}

	inline static int32_t get_offset_of_childModels_10() { return static_cast<int32_t>(offsetof(DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF, ___childModels_10)); }
	inline DTDContentModelCollection_t0382EF04D32FF6333D71752F3C4E9CC1EE5B2664 * get_childModels_10() const { return ___childModels_10; }
	inline DTDContentModelCollection_t0382EF04D32FF6333D71752F3C4E9CC1EE5B2664 ** get_address_of_childModels_10() { return &___childModels_10; }
	inline void set_childModels_10(DTDContentModelCollection_t0382EF04D32FF6333D71752F3C4E9CC1EE5B2664 * value)
	{
		___childModels_10 = value;
		Il2CppCodeGenWriteBarrier((&___childModels_10), value);
	}

	inline static int32_t get_offset_of_occurence_11() { return static_cast<int32_t>(offsetof(DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF, ___occurence_11)); }
	inline int32_t get_occurence_11() const { return ___occurence_11; }
	inline int32_t* get_address_of_occurence_11() { return &___occurence_11; }
	inline void set_occurence_11(int32_t value)
	{
		___occurence_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDCONTENTMODEL_T4D28535870F690FE6FA369E5F22EA0C7411B2BFF_H
#ifndef DTDELEMENTDECLARATIONCOLLECTION_T45A928E2967593285196499A173437F6AB3AE89D_H
#define DTDELEMENTDECLARATIONCOLLECTION_T45A928E2967593285196499A173437F6AB3AE89D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDElementDeclarationCollection
struct  DTDElementDeclarationCollection_t45A928E2967593285196499A173437F6AB3AE89D  : public DTDCollectionBase_tBA57464C8928A0229E33734BCF832A90138D0E4F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDELEMENTDECLARATIONCOLLECTION_T45A928E2967593285196499A173437F6AB3AE89D_H
#ifndef DTDENTITYDECLARATIONCOLLECTION_T94CA8138E40F051734624B32D031268F326F1995_H
#define DTDENTITYDECLARATIONCOLLECTION_T94CA8138E40F051734624B32D031268F326F1995_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDEntityDeclarationCollection
struct  DTDEntityDeclarationCollection_t94CA8138E40F051734624B32D031268F326F1995  : public DTDCollectionBase_tBA57464C8928A0229E33734BCF832A90138D0E4F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDENTITYDECLARATIONCOLLECTION_T94CA8138E40F051734624B32D031268F326F1995_H
#ifndef DTDNOTATIONDECLARATIONCOLLECTION_TF5A146D5CFBA10C2C7B82FDCD76FA19A301C936C_H
#define DTDNOTATIONDECLARATIONCOLLECTION_TF5A146D5CFBA10C2C7B82FDCD76FA19A301C936C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DTDNotationDeclarationCollection
struct  DTDNotationDeclarationCollection_tF5A146D5CFBA10C2C7B82FDCD76FA19A301C936C  : public DTDCollectionBase_tBA57464C8928A0229E33734BCF832A90138D0E4F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDNOTATIONDECLARATIONCOLLECTION_TF5A146D5CFBA10C2C7B82FDCD76FA19A301C936C_H
#ifndef U3CU3EC__ITERATOR3_TD90F8F0854A5877DB2152760FE7E79AC7CFC207A_H
#define U3CU3EC__ITERATOR3_TD90F8F0854A5877DB2152760FE7E79AC7CFC207A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.DictionaryBase_<>c__Iterator3
struct  U3CU3Ec__Iterator3_tD90F8F0854A5877DB2152760FE7E79AC7CFC207A  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1_Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode>> Mono.Xml.DictionaryBase_<>c__Iterator3::<U24s_50>__0
	Enumerator_t6396B5CBCF3FCE7769AB33B71210BFD48AAE9FB4  ___U3CU24s_50U3E__0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Mono.Xml.DTDNode> Mono.Xml.DictionaryBase_<>c__Iterator3::<p>__1
	KeyValuePair_2_t0067BC83D8F125202440DC6183F788BB0802D33D  ___U3CpU3E__1_1;
	// System.Int32 Mono.Xml.DictionaryBase_<>c__Iterator3::U24PC
	int32_t ___U24PC_2;
	// Mono.Xml.DTDNode Mono.Xml.DictionaryBase_<>c__Iterator3::U24current
	DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B * ___U24current_3;
	// Mono.Xml.DictionaryBase Mono.Xml.DictionaryBase_<>c__Iterator3::<>f__this
	DictionaryBase_t415336F093F8CDFAF278AA55A0F4A0FD46780582 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CU24s_50U3E__0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_tD90F8F0854A5877DB2152760FE7E79AC7CFC207A, ___U3CU24s_50U3E__0_0)); }
	inline Enumerator_t6396B5CBCF3FCE7769AB33B71210BFD48AAE9FB4  get_U3CU24s_50U3E__0_0() const { return ___U3CU24s_50U3E__0_0; }
	inline Enumerator_t6396B5CBCF3FCE7769AB33B71210BFD48AAE9FB4 * get_address_of_U3CU24s_50U3E__0_0() { return &___U3CU24s_50U3E__0_0; }
	inline void set_U3CU24s_50U3E__0_0(Enumerator_t6396B5CBCF3FCE7769AB33B71210BFD48AAE9FB4  value)
	{
		___U3CU24s_50U3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CpU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_tD90F8F0854A5877DB2152760FE7E79AC7CFC207A, ___U3CpU3E__1_1)); }
	inline KeyValuePair_2_t0067BC83D8F125202440DC6183F788BB0802D33D  get_U3CpU3E__1_1() const { return ___U3CpU3E__1_1; }
	inline KeyValuePair_2_t0067BC83D8F125202440DC6183F788BB0802D33D * get_address_of_U3CpU3E__1_1() { return &___U3CpU3E__1_1; }
	inline void set_U3CpU3E__1_1(KeyValuePair_2_t0067BC83D8F125202440DC6183F788BB0802D33D  value)
	{
		___U3CpU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_tD90F8F0854A5877DB2152760FE7E79AC7CFC207A, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_tD90F8F0854A5877DB2152760FE7E79AC7CFC207A, ___U24current_3)); }
	inline DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B * get_U24current_3() const { return ___U24current_3; }
	inline DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator3_tD90F8F0854A5877DB2152760FE7E79AC7CFC207A, ___U3CU3Ef__this_4)); }
	inline DictionaryBase_t415336F093F8CDFAF278AA55A0F4A0FD46780582 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline DictionaryBase_t415336F093F8CDFAF278AA55A0F4A0FD46780582 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(DictionaryBase_t415336F093F8CDFAF278AA55A0F4A0FD46780582 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR3_TD90F8F0854A5877DB2152760FE7E79AC7CFC207A_H
#ifndef ENTITYRESOLVINGXMLREADER_T37F12E545D4CEBDBE94AE87E3207C175BCBACC4F_H
#define ENTITYRESOLVINGXMLREADER_T37F12E545D4CEBDBE94AE87E3207C175BCBACC4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml.EntityResolvingXmlReader
struct  EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// Mono.Xml.EntityResolvingXmlReader Mono.Xml.EntityResolvingXmlReader::entity
	EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F * ___entity_2;
	// System.Xml.XmlReader Mono.Xml.EntityResolvingXmlReader::source
	XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * ___source_3;
	// System.Xml.XmlParserContext Mono.Xml.EntityResolvingXmlReader::context
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14 * ___context_4;
	// System.Xml.XmlResolver Mono.Xml.EntityResolvingXmlReader::resolver
	XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * ___resolver_5;
	// System.Xml.EntityHandling Mono.Xml.EntityResolvingXmlReader::entity_handling
	int32_t ___entity_handling_6;
	// System.Boolean Mono.Xml.EntityResolvingXmlReader::entity_inside_attr
	bool ___entity_inside_attr_7;
	// System.Boolean Mono.Xml.EntityResolvingXmlReader::inside_attr
	bool ___inside_attr_8;
	// System.Boolean Mono.Xml.EntityResolvingXmlReader::do_resolve
	bool ___do_resolve_9;

public:
	inline static int32_t get_offset_of_entity_2() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F, ___entity_2)); }
	inline EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F * get_entity_2() const { return ___entity_2; }
	inline EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F ** get_address_of_entity_2() { return &___entity_2; }
	inline void set_entity_2(EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F * value)
	{
		___entity_2 = value;
		Il2CppCodeGenWriteBarrier((&___entity_2), value);
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F, ___source_3)); }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * get_source_3() const { return ___source_3; }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_context_4() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F, ___context_4)); }
	inline XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14 * get_context_4() const { return ___context_4; }
	inline XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14 ** get_address_of_context_4() { return &___context_4; }
	inline void set_context_4(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14 * value)
	{
		___context_4 = value;
		Il2CppCodeGenWriteBarrier((&___context_4), value);
	}

	inline static int32_t get_offset_of_resolver_5() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F, ___resolver_5)); }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * get_resolver_5() const { return ___resolver_5; }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C ** get_address_of_resolver_5() { return &___resolver_5; }
	inline void set_resolver_5(XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * value)
	{
		___resolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_5), value);
	}

	inline static int32_t get_offset_of_entity_handling_6() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F, ___entity_handling_6)); }
	inline int32_t get_entity_handling_6() const { return ___entity_handling_6; }
	inline int32_t* get_address_of_entity_handling_6() { return &___entity_handling_6; }
	inline void set_entity_handling_6(int32_t value)
	{
		___entity_handling_6 = value;
	}

	inline static int32_t get_offset_of_entity_inside_attr_7() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F, ___entity_inside_attr_7)); }
	inline bool get_entity_inside_attr_7() const { return ___entity_inside_attr_7; }
	inline bool* get_address_of_entity_inside_attr_7() { return &___entity_inside_attr_7; }
	inline void set_entity_inside_attr_7(bool value)
	{
		___entity_inside_attr_7 = value;
	}

	inline static int32_t get_offset_of_inside_attr_8() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F, ___inside_attr_8)); }
	inline bool get_inside_attr_8() const { return ___inside_attr_8; }
	inline bool* get_address_of_inside_attr_8() { return &___inside_attr_8; }
	inline void set_inside_attr_8(bool value)
	{
		___inside_attr_8 = value;
	}

	inline static int32_t get_offset_of_do_resolve_9() { return static_cast<int32_t>(offsetof(EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F, ___do_resolve_9)); }
	inline bool get_do_resolve_9() const { return ___do_resolve_9; }
	inline bool* get_address_of_do_resolve_9() { return &___do_resolve_9; }
	inline void set_do_resolve_9(bool value)
	{
		___do_resolve_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYRESOLVINGXMLREADER_T37F12E545D4CEBDBE94AE87E3207C175BCBACC4F_H
#ifndef XMLTEXTREADER_TD979FF6B5FEF83373F4D669070FDD627F2447BCD_H
#define XMLTEXTREADER_TD979FF6B5FEF83373F4D669070FDD627F2447BCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader
struct  XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// Mono.Xml2.XmlTextReader_XmlTokenInfo Mono.Xml2.XmlTextReader::cursorToken
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8 * ___cursorToken_2;
	// Mono.Xml2.XmlTextReader_XmlTokenInfo Mono.Xml2.XmlTextReader::currentToken
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8 * ___currentToken_3;
	// Mono.Xml2.XmlTextReader_XmlAttributeTokenInfo Mono.Xml2.XmlTextReader::currentAttributeToken
	XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2 * ___currentAttributeToken_4;
	// Mono.Xml2.XmlTextReader_XmlTokenInfo Mono.Xml2.XmlTextReader::currentAttributeValueToken
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8 * ___currentAttributeValueToken_5;
	// Mono.Xml2.XmlTextReader_XmlAttributeTokenInfo[] Mono.Xml2.XmlTextReader::attributeTokens
	XmlAttributeTokenInfoU5BU5D_t775B2DAE0BCFA2CA1F941E56B78921A90F82EDF5* ___attributeTokens_6;
	// Mono.Xml2.XmlTextReader_XmlTokenInfo[] Mono.Xml2.XmlTextReader::attributeValueTokens
	XmlTokenInfoU5BU5D_tAC00CA31D3E381773C0EA56FCAF3FFA3AD87546E* ___attributeValueTokens_7;
	// System.Int32 Mono.Xml2.XmlTextReader::currentAttribute
	int32_t ___currentAttribute_8;
	// System.Int32 Mono.Xml2.XmlTextReader::currentAttributeValue
	int32_t ___currentAttributeValue_9;
	// System.Int32 Mono.Xml2.XmlTextReader::attributeCount
	int32_t ___attributeCount_10;
	// System.Xml.XmlParserContext Mono.Xml2.XmlTextReader::parserContext
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14 * ___parserContext_11;
	// System.Xml.XmlNameTable Mono.Xml2.XmlTextReader::nameTable
	XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * ___nameTable_12;
	// System.Xml.XmlNamespaceManager Mono.Xml2.XmlTextReader::nsmgr
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * ___nsmgr_13;
	// System.Xml.ReadState Mono.Xml2.XmlTextReader::readState
	int32_t ___readState_14;
	// System.Boolean Mono.Xml2.XmlTextReader::disallowReset
	bool ___disallowReset_15;
	// System.Int32 Mono.Xml2.XmlTextReader::depth
	int32_t ___depth_16;
	// System.Int32 Mono.Xml2.XmlTextReader::elementDepth
	int32_t ___elementDepth_17;
	// System.Boolean Mono.Xml2.XmlTextReader::depthUp
	bool ___depthUp_18;
	// System.Boolean Mono.Xml2.XmlTextReader::popScope
	bool ___popScope_19;
	// Mono.Xml2.XmlTextReader_TagName[] Mono.Xml2.XmlTextReader::elementNames
	TagNameU5BU5D_tDC731B271CA71CDD2CCAA81DD046275A56A60BAE* ___elementNames_20;
	// System.Int32 Mono.Xml2.XmlTextReader::elementNameStackPos
	int32_t ___elementNameStackPos_21;
	// System.Boolean Mono.Xml2.XmlTextReader::allowMultipleRoot
	bool ___allowMultipleRoot_22;
	// System.Boolean Mono.Xml2.XmlTextReader::isStandalone
	bool ___isStandalone_23;
	// System.Boolean Mono.Xml2.XmlTextReader::returnEntityReference
	bool ___returnEntityReference_24;
	// System.String Mono.Xml2.XmlTextReader::entityReferenceName
	String_t* ___entityReferenceName_25;
	// System.Text.StringBuilder Mono.Xml2.XmlTextReader::valueBuffer
	StringBuilder_t * ___valueBuffer_26;
	// System.IO.TextReader Mono.Xml2.XmlTextReader::reader
	TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520 * ___reader_27;
	// System.Char[] Mono.Xml2.XmlTextReader::peekChars
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___peekChars_28;
	// System.Int32 Mono.Xml2.XmlTextReader::peekCharsIndex
	int32_t ___peekCharsIndex_29;
	// System.Int32 Mono.Xml2.XmlTextReader::peekCharsLength
	int32_t ___peekCharsLength_30;
	// System.Int32 Mono.Xml2.XmlTextReader::curNodePeekIndex
	int32_t ___curNodePeekIndex_31;
	// System.Boolean Mono.Xml2.XmlTextReader::preserveCurrentTag
	bool ___preserveCurrentTag_32;
	// System.Int32 Mono.Xml2.XmlTextReader::line
	int32_t ___line_33;
	// System.Int32 Mono.Xml2.XmlTextReader::column
	int32_t ___column_34;
	// System.Int32 Mono.Xml2.XmlTextReader::currentLinkedNodeLineNumber
	int32_t ___currentLinkedNodeLineNumber_35;
	// System.Int32 Mono.Xml2.XmlTextReader::currentLinkedNodeLinePosition
	int32_t ___currentLinkedNodeLinePosition_36;
	// System.Boolean Mono.Xml2.XmlTextReader::useProceedingLineInfo
	bool ___useProceedingLineInfo_37;
	// System.Xml.XmlNodeType Mono.Xml2.XmlTextReader::startNodeType
	int32_t ___startNodeType_38;
	// System.Xml.XmlNodeType Mono.Xml2.XmlTextReader::currentState
	int32_t ___currentState_39;
	// System.Int32 Mono.Xml2.XmlTextReader::nestLevel
	int32_t ___nestLevel_40;
	// System.Boolean Mono.Xml2.XmlTextReader::readCharsInProgress
	bool ___readCharsInProgress_41;
	// System.Xml.XmlReaderBinarySupport_CharGetter Mono.Xml2.XmlTextReader::binaryCharGetter
	CharGetter_t41F2B070679160162013D7D2B99DF5A72133136B * ___binaryCharGetter_42;
	// System.Boolean Mono.Xml2.XmlTextReader::namespaces
	bool ___namespaces_43;
	// System.Xml.WhitespaceHandling Mono.Xml2.XmlTextReader::whitespaceHandling
	int32_t ___whitespaceHandling_44;
	// System.Xml.XmlResolver Mono.Xml2.XmlTextReader::resolver
	XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * ___resolver_45;
	// System.Boolean Mono.Xml2.XmlTextReader::normalization
	bool ___normalization_46;
	// System.Boolean Mono.Xml2.XmlTextReader::checkCharacters
	bool ___checkCharacters_47;
	// System.Boolean Mono.Xml2.XmlTextReader::prohibitDtd
	bool ___prohibitDtd_48;
	// System.Boolean Mono.Xml2.XmlTextReader::closeInput
	bool ___closeInput_49;
	// System.Xml.EntityHandling Mono.Xml2.XmlTextReader::entityHandling
	int32_t ___entityHandling_50;
	// System.Xml.NameTable Mono.Xml2.XmlTextReader::whitespacePool
	NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6 * ___whitespacePool_51;
	// System.Char[] Mono.Xml2.XmlTextReader::whitespaceCache
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___whitespaceCache_52;
	// Mono.Xml2.XmlTextReader_DtdInputStateStack Mono.Xml2.XmlTextReader::stateStack
	DtdInputStateStack_tD410944C4A9AE89256F67B1E85F27375A0ABA100 * ___stateStack_53;

public:
	inline static int32_t get_offset_of_cursorToken_2() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___cursorToken_2)); }
	inline XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8 * get_cursorToken_2() const { return ___cursorToken_2; }
	inline XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8 ** get_address_of_cursorToken_2() { return &___cursorToken_2; }
	inline void set_cursorToken_2(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8 * value)
	{
		___cursorToken_2 = value;
		Il2CppCodeGenWriteBarrier((&___cursorToken_2), value);
	}

	inline static int32_t get_offset_of_currentToken_3() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___currentToken_3)); }
	inline XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8 * get_currentToken_3() const { return ___currentToken_3; }
	inline XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8 ** get_address_of_currentToken_3() { return &___currentToken_3; }
	inline void set_currentToken_3(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8 * value)
	{
		___currentToken_3 = value;
		Il2CppCodeGenWriteBarrier((&___currentToken_3), value);
	}

	inline static int32_t get_offset_of_currentAttributeToken_4() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___currentAttributeToken_4)); }
	inline XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2 * get_currentAttributeToken_4() const { return ___currentAttributeToken_4; }
	inline XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2 ** get_address_of_currentAttributeToken_4() { return &___currentAttributeToken_4; }
	inline void set_currentAttributeToken_4(XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2 * value)
	{
		___currentAttributeToken_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttributeToken_4), value);
	}

	inline static int32_t get_offset_of_currentAttributeValueToken_5() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___currentAttributeValueToken_5)); }
	inline XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8 * get_currentAttributeValueToken_5() const { return ___currentAttributeValueToken_5; }
	inline XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8 ** get_address_of_currentAttributeValueToken_5() { return &___currentAttributeValueToken_5; }
	inline void set_currentAttributeValueToken_5(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8 * value)
	{
		___currentAttributeValueToken_5 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttributeValueToken_5), value);
	}

	inline static int32_t get_offset_of_attributeTokens_6() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___attributeTokens_6)); }
	inline XmlAttributeTokenInfoU5BU5D_t775B2DAE0BCFA2CA1F941E56B78921A90F82EDF5* get_attributeTokens_6() const { return ___attributeTokens_6; }
	inline XmlAttributeTokenInfoU5BU5D_t775B2DAE0BCFA2CA1F941E56B78921A90F82EDF5** get_address_of_attributeTokens_6() { return &___attributeTokens_6; }
	inline void set_attributeTokens_6(XmlAttributeTokenInfoU5BU5D_t775B2DAE0BCFA2CA1F941E56B78921A90F82EDF5* value)
	{
		___attributeTokens_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributeTokens_6), value);
	}

	inline static int32_t get_offset_of_attributeValueTokens_7() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___attributeValueTokens_7)); }
	inline XmlTokenInfoU5BU5D_tAC00CA31D3E381773C0EA56FCAF3FFA3AD87546E* get_attributeValueTokens_7() const { return ___attributeValueTokens_7; }
	inline XmlTokenInfoU5BU5D_tAC00CA31D3E381773C0EA56FCAF3FFA3AD87546E** get_address_of_attributeValueTokens_7() { return &___attributeValueTokens_7; }
	inline void set_attributeValueTokens_7(XmlTokenInfoU5BU5D_tAC00CA31D3E381773C0EA56FCAF3FFA3AD87546E* value)
	{
		___attributeValueTokens_7 = value;
		Il2CppCodeGenWriteBarrier((&___attributeValueTokens_7), value);
	}

	inline static int32_t get_offset_of_currentAttribute_8() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___currentAttribute_8)); }
	inline int32_t get_currentAttribute_8() const { return ___currentAttribute_8; }
	inline int32_t* get_address_of_currentAttribute_8() { return &___currentAttribute_8; }
	inline void set_currentAttribute_8(int32_t value)
	{
		___currentAttribute_8 = value;
	}

	inline static int32_t get_offset_of_currentAttributeValue_9() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___currentAttributeValue_9)); }
	inline int32_t get_currentAttributeValue_9() const { return ___currentAttributeValue_9; }
	inline int32_t* get_address_of_currentAttributeValue_9() { return &___currentAttributeValue_9; }
	inline void set_currentAttributeValue_9(int32_t value)
	{
		___currentAttributeValue_9 = value;
	}

	inline static int32_t get_offset_of_attributeCount_10() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___attributeCount_10)); }
	inline int32_t get_attributeCount_10() const { return ___attributeCount_10; }
	inline int32_t* get_address_of_attributeCount_10() { return &___attributeCount_10; }
	inline void set_attributeCount_10(int32_t value)
	{
		___attributeCount_10 = value;
	}

	inline static int32_t get_offset_of_parserContext_11() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___parserContext_11)); }
	inline XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14 * get_parserContext_11() const { return ___parserContext_11; }
	inline XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14 ** get_address_of_parserContext_11() { return &___parserContext_11; }
	inline void set_parserContext_11(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14 * value)
	{
		___parserContext_11 = value;
		Il2CppCodeGenWriteBarrier((&___parserContext_11), value);
	}

	inline static int32_t get_offset_of_nameTable_12() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___nameTable_12)); }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * get_nameTable_12() const { return ___nameTable_12; }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B ** get_address_of_nameTable_12() { return &___nameTable_12; }
	inline void set_nameTable_12(XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * value)
	{
		___nameTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_12), value);
	}

	inline static int32_t get_offset_of_nsmgr_13() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___nsmgr_13)); }
	inline XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * get_nsmgr_13() const { return ___nsmgr_13; }
	inline XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 ** get_address_of_nsmgr_13() { return &___nsmgr_13; }
	inline void set_nsmgr_13(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * value)
	{
		___nsmgr_13 = value;
		Il2CppCodeGenWriteBarrier((&___nsmgr_13), value);
	}

	inline static int32_t get_offset_of_readState_14() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___readState_14)); }
	inline int32_t get_readState_14() const { return ___readState_14; }
	inline int32_t* get_address_of_readState_14() { return &___readState_14; }
	inline void set_readState_14(int32_t value)
	{
		___readState_14 = value;
	}

	inline static int32_t get_offset_of_disallowReset_15() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___disallowReset_15)); }
	inline bool get_disallowReset_15() const { return ___disallowReset_15; }
	inline bool* get_address_of_disallowReset_15() { return &___disallowReset_15; }
	inline void set_disallowReset_15(bool value)
	{
		___disallowReset_15 = value;
	}

	inline static int32_t get_offset_of_depth_16() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___depth_16)); }
	inline int32_t get_depth_16() const { return ___depth_16; }
	inline int32_t* get_address_of_depth_16() { return &___depth_16; }
	inline void set_depth_16(int32_t value)
	{
		___depth_16 = value;
	}

	inline static int32_t get_offset_of_elementDepth_17() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___elementDepth_17)); }
	inline int32_t get_elementDepth_17() const { return ___elementDepth_17; }
	inline int32_t* get_address_of_elementDepth_17() { return &___elementDepth_17; }
	inline void set_elementDepth_17(int32_t value)
	{
		___elementDepth_17 = value;
	}

	inline static int32_t get_offset_of_depthUp_18() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___depthUp_18)); }
	inline bool get_depthUp_18() const { return ___depthUp_18; }
	inline bool* get_address_of_depthUp_18() { return &___depthUp_18; }
	inline void set_depthUp_18(bool value)
	{
		___depthUp_18 = value;
	}

	inline static int32_t get_offset_of_popScope_19() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___popScope_19)); }
	inline bool get_popScope_19() const { return ___popScope_19; }
	inline bool* get_address_of_popScope_19() { return &___popScope_19; }
	inline void set_popScope_19(bool value)
	{
		___popScope_19 = value;
	}

	inline static int32_t get_offset_of_elementNames_20() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___elementNames_20)); }
	inline TagNameU5BU5D_tDC731B271CA71CDD2CCAA81DD046275A56A60BAE* get_elementNames_20() const { return ___elementNames_20; }
	inline TagNameU5BU5D_tDC731B271CA71CDD2CCAA81DD046275A56A60BAE** get_address_of_elementNames_20() { return &___elementNames_20; }
	inline void set_elementNames_20(TagNameU5BU5D_tDC731B271CA71CDD2CCAA81DD046275A56A60BAE* value)
	{
		___elementNames_20 = value;
		Il2CppCodeGenWriteBarrier((&___elementNames_20), value);
	}

	inline static int32_t get_offset_of_elementNameStackPos_21() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___elementNameStackPos_21)); }
	inline int32_t get_elementNameStackPos_21() const { return ___elementNameStackPos_21; }
	inline int32_t* get_address_of_elementNameStackPos_21() { return &___elementNameStackPos_21; }
	inline void set_elementNameStackPos_21(int32_t value)
	{
		___elementNameStackPos_21 = value;
	}

	inline static int32_t get_offset_of_allowMultipleRoot_22() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___allowMultipleRoot_22)); }
	inline bool get_allowMultipleRoot_22() const { return ___allowMultipleRoot_22; }
	inline bool* get_address_of_allowMultipleRoot_22() { return &___allowMultipleRoot_22; }
	inline void set_allowMultipleRoot_22(bool value)
	{
		___allowMultipleRoot_22 = value;
	}

	inline static int32_t get_offset_of_isStandalone_23() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___isStandalone_23)); }
	inline bool get_isStandalone_23() const { return ___isStandalone_23; }
	inline bool* get_address_of_isStandalone_23() { return &___isStandalone_23; }
	inline void set_isStandalone_23(bool value)
	{
		___isStandalone_23 = value;
	}

	inline static int32_t get_offset_of_returnEntityReference_24() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___returnEntityReference_24)); }
	inline bool get_returnEntityReference_24() const { return ___returnEntityReference_24; }
	inline bool* get_address_of_returnEntityReference_24() { return &___returnEntityReference_24; }
	inline void set_returnEntityReference_24(bool value)
	{
		___returnEntityReference_24 = value;
	}

	inline static int32_t get_offset_of_entityReferenceName_25() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___entityReferenceName_25)); }
	inline String_t* get_entityReferenceName_25() const { return ___entityReferenceName_25; }
	inline String_t** get_address_of_entityReferenceName_25() { return &___entityReferenceName_25; }
	inline void set_entityReferenceName_25(String_t* value)
	{
		___entityReferenceName_25 = value;
		Il2CppCodeGenWriteBarrier((&___entityReferenceName_25), value);
	}

	inline static int32_t get_offset_of_valueBuffer_26() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___valueBuffer_26)); }
	inline StringBuilder_t * get_valueBuffer_26() const { return ___valueBuffer_26; }
	inline StringBuilder_t ** get_address_of_valueBuffer_26() { return &___valueBuffer_26; }
	inline void set_valueBuffer_26(StringBuilder_t * value)
	{
		___valueBuffer_26 = value;
		Il2CppCodeGenWriteBarrier((&___valueBuffer_26), value);
	}

	inline static int32_t get_offset_of_reader_27() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___reader_27)); }
	inline TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520 * get_reader_27() const { return ___reader_27; }
	inline TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520 ** get_address_of_reader_27() { return &___reader_27; }
	inline void set_reader_27(TextReader_tC6C39132CD1389E729FA893F06C34EE351D24520 * value)
	{
		___reader_27 = value;
		Il2CppCodeGenWriteBarrier((&___reader_27), value);
	}

	inline static int32_t get_offset_of_peekChars_28() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___peekChars_28)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_peekChars_28() const { return ___peekChars_28; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_peekChars_28() { return &___peekChars_28; }
	inline void set_peekChars_28(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___peekChars_28 = value;
		Il2CppCodeGenWriteBarrier((&___peekChars_28), value);
	}

	inline static int32_t get_offset_of_peekCharsIndex_29() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___peekCharsIndex_29)); }
	inline int32_t get_peekCharsIndex_29() const { return ___peekCharsIndex_29; }
	inline int32_t* get_address_of_peekCharsIndex_29() { return &___peekCharsIndex_29; }
	inline void set_peekCharsIndex_29(int32_t value)
	{
		___peekCharsIndex_29 = value;
	}

	inline static int32_t get_offset_of_peekCharsLength_30() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___peekCharsLength_30)); }
	inline int32_t get_peekCharsLength_30() const { return ___peekCharsLength_30; }
	inline int32_t* get_address_of_peekCharsLength_30() { return &___peekCharsLength_30; }
	inline void set_peekCharsLength_30(int32_t value)
	{
		___peekCharsLength_30 = value;
	}

	inline static int32_t get_offset_of_curNodePeekIndex_31() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___curNodePeekIndex_31)); }
	inline int32_t get_curNodePeekIndex_31() const { return ___curNodePeekIndex_31; }
	inline int32_t* get_address_of_curNodePeekIndex_31() { return &___curNodePeekIndex_31; }
	inline void set_curNodePeekIndex_31(int32_t value)
	{
		___curNodePeekIndex_31 = value;
	}

	inline static int32_t get_offset_of_preserveCurrentTag_32() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___preserveCurrentTag_32)); }
	inline bool get_preserveCurrentTag_32() const { return ___preserveCurrentTag_32; }
	inline bool* get_address_of_preserveCurrentTag_32() { return &___preserveCurrentTag_32; }
	inline void set_preserveCurrentTag_32(bool value)
	{
		___preserveCurrentTag_32 = value;
	}

	inline static int32_t get_offset_of_line_33() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___line_33)); }
	inline int32_t get_line_33() const { return ___line_33; }
	inline int32_t* get_address_of_line_33() { return &___line_33; }
	inline void set_line_33(int32_t value)
	{
		___line_33 = value;
	}

	inline static int32_t get_offset_of_column_34() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___column_34)); }
	inline int32_t get_column_34() const { return ___column_34; }
	inline int32_t* get_address_of_column_34() { return &___column_34; }
	inline void set_column_34(int32_t value)
	{
		___column_34 = value;
	}

	inline static int32_t get_offset_of_currentLinkedNodeLineNumber_35() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___currentLinkedNodeLineNumber_35)); }
	inline int32_t get_currentLinkedNodeLineNumber_35() const { return ___currentLinkedNodeLineNumber_35; }
	inline int32_t* get_address_of_currentLinkedNodeLineNumber_35() { return &___currentLinkedNodeLineNumber_35; }
	inline void set_currentLinkedNodeLineNumber_35(int32_t value)
	{
		___currentLinkedNodeLineNumber_35 = value;
	}

	inline static int32_t get_offset_of_currentLinkedNodeLinePosition_36() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___currentLinkedNodeLinePosition_36)); }
	inline int32_t get_currentLinkedNodeLinePosition_36() const { return ___currentLinkedNodeLinePosition_36; }
	inline int32_t* get_address_of_currentLinkedNodeLinePosition_36() { return &___currentLinkedNodeLinePosition_36; }
	inline void set_currentLinkedNodeLinePosition_36(int32_t value)
	{
		___currentLinkedNodeLinePosition_36 = value;
	}

	inline static int32_t get_offset_of_useProceedingLineInfo_37() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___useProceedingLineInfo_37)); }
	inline bool get_useProceedingLineInfo_37() const { return ___useProceedingLineInfo_37; }
	inline bool* get_address_of_useProceedingLineInfo_37() { return &___useProceedingLineInfo_37; }
	inline void set_useProceedingLineInfo_37(bool value)
	{
		___useProceedingLineInfo_37 = value;
	}

	inline static int32_t get_offset_of_startNodeType_38() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___startNodeType_38)); }
	inline int32_t get_startNodeType_38() const { return ___startNodeType_38; }
	inline int32_t* get_address_of_startNodeType_38() { return &___startNodeType_38; }
	inline void set_startNodeType_38(int32_t value)
	{
		___startNodeType_38 = value;
	}

	inline static int32_t get_offset_of_currentState_39() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___currentState_39)); }
	inline int32_t get_currentState_39() const { return ___currentState_39; }
	inline int32_t* get_address_of_currentState_39() { return &___currentState_39; }
	inline void set_currentState_39(int32_t value)
	{
		___currentState_39 = value;
	}

	inline static int32_t get_offset_of_nestLevel_40() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___nestLevel_40)); }
	inline int32_t get_nestLevel_40() const { return ___nestLevel_40; }
	inline int32_t* get_address_of_nestLevel_40() { return &___nestLevel_40; }
	inline void set_nestLevel_40(int32_t value)
	{
		___nestLevel_40 = value;
	}

	inline static int32_t get_offset_of_readCharsInProgress_41() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___readCharsInProgress_41)); }
	inline bool get_readCharsInProgress_41() const { return ___readCharsInProgress_41; }
	inline bool* get_address_of_readCharsInProgress_41() { return &___readCharsInProgress_41; }
	inline void set_readCharsInProgress_41(bool value)
	{
		___readCharsInProgress_41 = value;
	}

	inline static int32_t get_offset_of_binaryCharGetter_42() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___binaryCharGetter_42)); }
	inline CharGetter_t41F2B070679160162013D7D2B99DF5A72133136B * get_binaryCharGetter_42() const { return ___binaryCharGetter_42; }
	inline CharGetter_t41F2B070679160162013D7D2B99DF5A72133136B ** get_address_of_binaryCharGetter_42() { return &___binaryCharGetter_42; }
	inline void set_binaryCharGetter_42(CharGetter_t41F2B070679160162013D7D2B99DF5A72133136B * value)
	{
		___binaryCharGetter_42 = value;
		Il2CppCodeGenWriteBarrier((&___binaryCharGetter_42), value);
	}

	inline static int32_t get_offset_of_namespaces_43() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___namespaces_43)); }
	inline bool get_namespaces_43() const { return ___namespaces_43; }
	inline bool* get_address_of_namespaces_43() { return &___namespaces_43; }
	inline void set_namespaces_43(bool value)
	{
		___namespaces_43 = value;
	}

	inline static int32_t get_offset_of_whitespaceHandling_44() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___whitespaceHandling_44)); }
	inline int32_t get_whitespaceHandling_44() const { return ___whitespaceHandling_44; }
	inline int32_t* get_address_of_whitespaceHandling_44() { return &___whitespaceHandling_44; }
	inline void set_whitespaceHandling_44(int32_t value)
	{
		___whitespaceHandling_44 = value;
	}

	inline static int32_t get_offset_of_resolver_45() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___resolver_45)); }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * get_resolver_45() const { return ___resolver_45; }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C ** get_address_of_resolver_45() { return &___resolver_45; }
	inline void set_resolver_45(XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * value)
	{
		___resolver_45 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_45), value);
	}

	inline static int32_t get_offset_of_normalization_46() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___normalization_46)); }
	inline bool get_normalization_46() const { return ___normalization_46; }
	inline bool* get_address_of_normalization_46() { return &___normalization_46; }
	inline void set_normalization_46(bool value)
	{
		___normalization_46 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_47() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___checkCharacters_47)); }
	inline bool get_checkCharacters_47() const { return ___checkCharacters_47; }
	inline bool* get_address_of_checkCharacters_47() { return &___checkCharacters_47; }
	inline void set_checkCharacters_47(bool value)
	{
		___checkCharacters_47 = value;
	}

	inline static int32_t get_offset_of_prohibitDtd_48() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___prohibitDtd_48)); }
	inline bool get_prohibitDtd_48() const { return ___prohibitDtd_48; }
	inline bool* get_address_of_prohibitDtd_48() { return &___prohibitDtd_48; }
	inline void set_prohibitDtd_48(bool value)
	{
		___prohibitDtd_48 = value;
	}

	inline static int32_t get_offset_of_closeInput_49() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___closeInput_49)); }
	inline bool get_closeInput_49() const { return ___closeInput_49; }
	inline bool* get_address_of_closeInput_49() { return &___closeInput_49; }
	inline void set_closeInput_49(bool value)
	{
		___closeInput_49 = value;
	}

	inline static int32_t get_offset_of_entityHandling_50() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___entityHandling_50)); }
	inline int32_t get_entityHandling_50() const { return ___entityHandling_50; }
	inline int32_t* get_address_of_entityHandling_50() { return &___entityHandling_50; }
	inline void set_entityHandling_50(int32_t value)
	{
		___entityHandling_50 = value;
	}

	inline static int32_t get_offset_of_whitespacePool_51() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___whitespacePool_51)); }
	inline NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6 * get_whitespacePool_51() const { return ___whitespacePool_51; }
	inline NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6 ** get_address_of_whitespacePool_51() { return &___whitespacePool_51; }
	inline void set_whitespacePool_51(NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6 * value)
	{
		___whitespacePool_51 = value;
		Il2CppCodeGenWriteBarrier((&___whitespacePool_51), value);
	}

	inline static int32_t get_offset_of_whitespaceCache_52() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___whitespaceCache_52)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_whitespaceCache_52() const { return ___whitespaceCache_52; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_whitespaceCache_52() { return &___whitespaceCache_52; }
	inline void set_whitespaceCache_52(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___whitespaceCache_52 = value;
		Il2CppCodeGenWriteBarrier((&___whitespaceCache_52), value);
	}

	inline static int32_t get_offset_of_stateStack_53() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD, ___stateStack_53)); }
	inline DtdInputStateStack_tD410944C4A9AE89256F67B1E85F27375A0ABA100 * get_stateStack_53() const { return ___stateStack_53; }
	inline DtdInputStateStack_tD410944C4A9AE89256F67B1E85F27375A0ABA100 ** get_address_of_stateStack_53() { return &___stateStack_53; }
	inline void set_stateStack_53(DtdInputStateStack_tD410944C4A9AE89256F67B1E85F27375A0ABA100 * value)
	{
		___stateStack_53 = value;
		Il2CppCodeGenWriteBarrier((&___stateStack_53), value);
	}
};

struct XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml2.XmlTextReader::<>f__switchU24map38
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map38_54;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Xml2.XmlTextReader::<>f__switchU24map39
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map39_55;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map38_54() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD_StaticFields, ___U3CU3Ef__switchU24map38_54)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map38_54() const { return ___U3CU3Ef__switchU24map38_54; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map38_54() { return &___U3CU3Ef__switchU24map38_54; }
	inline void set_U3CU3Ef__switchU24map38_54(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map38_54 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map38_54), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map39_55() { return static_cast<int32_t>(offsetof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD_StaticFields, ___U3CU3Ef__switchU24map39_55)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map39_55() const { return ___U3CU3Ef__switchU24map39_55; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map39_55() { return &___U3CU3Ef__switchU24map39_55; }
	inline void set_U3CU3Ef__switchU24map39_55(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map39_55 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map39_55), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTREADER_TD979FF6B5FEF83373F4D669070FDD627F2447BCD_H
#ifndef XMLTOKENINFO_T2A1A819C64EA244AED28E17A84BA664B876EF3A8_H
#define XMLTOKENINFO_T2A1A819C64EA244AED28E17A84BA664B876EF3A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader_XmlTokenInfo
struct  XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8  : public RuntimeObject
{
public:
	// System.String Mono.Xml2.XmlTextReader_XmlTokenInfo::valueCache
	String_t* ___valueCache_0;
	// Mono.Xml2.XmlTextReader Mono.Xml2.XmlTextReader_XmlTokenInfo::Reader
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD * ___Reader_1;
	// System.String Mono.Xml2.XmlTextReader_XmlTokenInfo::Name
	String_t* ___Name_2;
	// System.String Mono.Xml2.XmlTextReader_XmlTokenInfo::LocalName
	String_t* ___LocalName_3;
	// System.String Mono.Xml2.XmlTextReader_XmlTokenInfo::Prefix
	String_t* ___Prefix_4;
	// System.String Mono.Xml2.XmlTextReader_XmlTokenInfo::NamespaceURI
	String_t* ___NamespaceURI_5;
	// System.Boolean Mono.Xml2.XmlTextReader_XmlTokenInfo::IsEmptyElement
	bool ___IsEmptyElement_6;
	// System.Char Mono.Xml2.XmlTextReader_XmlTokenInfo::QuoteChar
	Il2CppChar ___QuoteChar_7;
	// System.Int32 Mono.Xml2.XmlTextReader_XmlTokenInfo::LineNumber
	int32_t ___LineNumber_8;
	// System.Int32 Mono.Xml2.XmlTextReader_XmlTokenInfo::LinePosition
	int32_t ___LinePosition_9;
	// System.Int32 Mono.Xml2.XmlTextReader_XmlTokenInfo::ValueBufferStart
	int32_t ___ValueBufferStart_10;
	// System.Int32 Mono.Xml2.XmlTextReader_XmlTokenInfo::ValueBufferEnd
	int32_t ___ValueBufferEnd_11;
	// System.Xml.XmlNodeType Mono.Xml2.XmlTextReader_XmlTokenInfo::NodeType
	int32_t ___NodeType_12;

public:
	inline static int32_t get_offset_of_valueCache_0() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___valueCache_0)); }
	inline String_t* get_valueCache_0() const { return ___valueCache_0; }
	inline String_t** get_address_of_valueCache_0() { return &___valueCache_0; }
	inline void set_valueCache_0(String_t* value)
	{
		___valueCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___valueCache_0), value);
	}

	inline static int32_t get_offset_of_Reader_1() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___Reader_1)); }
	inline XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD * get_Reader_1() const { return ___Reader_1; }
	inline XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD ** get_address_of_Reader_1() { return &___Reader_1; }
	inline void set_Reader_1(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD * value)
	{
		___Reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___Reader_1), value);
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}

	inline static int32_t get_offset_of_LocalName_3() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___LocalName_3)); }
	inline String_t* get_LocalName_3() const { return ___LocalName_3; }
	inline String_t** get_address_of_LocalName_3() { return &___LocalName_3; }
	inline void set_LocalName_3(String_t* value)
	{
		___LocalName_3 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_3), value);
	}

	inline static int32_t get_offset_of_Prefix_4() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___Prefix_4)); }
	inline String_t* get_Prefix_4() const { return ___Prefix_4; }
	inline String_t** get_address_of_Prefix_4() { return &___Prefix_4; }
	inline void set_Prefix_4(String_t* value)
	{
		___Prefix_4 = value;
		Il2CppCodeGenWriteBarrier((&___Prefix_4), value);
	}

	inline static int32_t get_offset_of_NamespaceURI_5() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___NamespaceURI_5)); }
	inline String_t* get_NamespaceURI_5() const { return ___NamespaceURI_5; }
	inline String_t** get_address_of_NamespaceURI_5() { return &___NamespaceURI_5; }
	inline void set_NamespaceURI_5(String_t* value)
	{
		___NamespaceURI_5 = value;
		Il2CppCodeGenWriteBarrier((&___NamespaceURI_5), value);
	}

	inline static int32_t get_offset_of_IsEmptyElement_6() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___IsEmptyElement_6)); }
	inline bool get_IsEmptyElement_6() const { return ___IsEmptyElement_6; }
	inline bool* get_address_of_IsEmptyElement_6() { return &___IsEmptyElement_6; }
	inline void set_IsEmptyElement_6(bool value)
	{
		___IsEmptyElement_6 = value;
	}

	inline static int32_t get_offset_of_QuoteChar_7() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___QuoteChar_7)); }
	inline Il2CppChar get_QuoteChar_7() const { return ___QuoteChar_7; }
	inline Il2CppChar* get_address_of_QuoteChar_7() { return &___QuoteChar_7; }
	inline void set_QuoteChar_7(Il2CppChar value)
	{
		___QuoteChar_7 = value;
	}

	inline static int32_t get_offset_of_LineNumber_8() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___LineNumber_8)); }
	inline int32_t get_LineNumber_8() const { return ___LineNumber_8; }
	inline int32_t* get_address_of_LineNumber_8() { return &___LineNumber_8; }
	inline void set_LineNumber_8(int32_t value)
	{
		___LineNumber_8 = value;
	}

	inline static int32_t get_offset_of_LinePosition_9() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___LinePosition_9)); }
	inline int32_t get_LinePosition_9() const { return ___LinePosition_9; }
	inline int32_t* get_address_of_LinePosition_9() { return &___LinePosition_9; }
	inline void set_LinePosition_9(int32_t value)
	{
		___LinePosition_9 = value;
	}

	inline static int32_t get_offset_of_ValueBufferStart_10() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___ValueBufferStart_10)); }
	inline int32_t get_ValueBufferStart_10() const { return ___ValueBufferStart_10; }
	inline int32_t* get_address_of_ValueBufferStart_10() { return &___ValueBufferStart_10; }
	inline void set_ValueBufferStart_10(int32_t value)
	{
		___ValueBufferStart_10 = value;
	}

	inline static int32_t get_offset_of_ValueBufferEnd_11() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___ValueBufferEnd_11)); }
	inline int32_t get_ValueBufferEnd_11() const { return ___ValueBufferEnd_11; }
	inline int32_t* get_address_of_ValueBufferEnd_11() { return &___ValueBufferEnd_11; }
	inline void set_ValueBufferEnd_11(int32_t value)
	{
		___ValueBufferEnd_11 = value;
	}

	inline static int32_t get_offset_of_NodeType_12() { return static_cast<int32_t>(offsetof(XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8, ___NodeType_12)); }
	inline int32_t get_NodeType_12() const { return ___NodeType_12; }
	inline int32_t* get_address_of_NodeType_12() { return &___NodeType_12; }
	inline void set_NodeType_12(int32_t value)
	{
		___NodeType_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTOKENINFO_T2A1A819C64EA244AED28E17A84BA664B876EF3A8_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef XMLCDATASECTION_T9D9B34AE86A0567C22CF9E0A507E59876FC365F3_H
#define XMLCDATASECTION_T9D9B34AE86A0567C22CF9E0A507E59876FC365F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCDataSection
struct  XmlCDataSection_t9D9B34AE86A0567C22CF9E0A507E59876FC365F3  : public XmlCharacterData_t1733676F46934DE88DE8DEC9CF47190ABAE32F07
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCDATASECTION_T9D9B34AE86A0567C22CF9E0A507E59876FC365F3_H
#ifndef XMLCOMMENT_T00021AB0FA23A161ABEC4F774A4084E03FC8186D_H
#define XMLCOMMENT_T00021AB0FA23A161ABEC4F774A4084E03FC8186D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlComment
struct  XmlComment_t00021AB0FA23A161ABEC4F774A4084E03FC8186D  : public XmlCharacterData_t1733676F46934DE88DE8DEC9CF47190ABAE32F07
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCOMMENT_T00021AB0FA23A161ABEC4F774A4084E03FC8186D_H
#ifndef XMLCONVERT_T3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_H
#define XMLCONVERT_T3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlConvert
struct  XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84  : public RuntimeObject
{
public:

public:
};

struct XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields
{
public:
	// System.String[] System.Xml.XmlConvert::datetimeFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___datetimeFormats_0;
	// System.String[] System.Xml.XmlConvert::defaultDateTimeFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___defaultDateTimeFormats_1;
	// System.String[] System.Xml.XmlConvert::roundtripDateTimeFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___roundtripDateTimeFormats_2;
	// System.String[] System.Xml.XmlConvert::localDateTimeFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___localDateTimeFormats_3;
	// System.String[] System.Xml.XmlConvert::utcDateTimeFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___utcDateTimeFormats_4;
	// System.String[] System.Xml.XmlConvert::unspecifiedDateTimeFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___unspecifiedDateTimeFormats_5;
	// System.Globalization.DateTimeStyles System.Xml.XmlConvert::_defaultStyle
	int32_t ____defaultStyle_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlConvert::<>f__switchU24map33
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map33_7;

public:
	inline static int32_t get_offset_of_datetimeFormats_0() { return static_cast<int32_t>(offsetof(XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields, ___datetimeFormats_0)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_datetimeFormats_0() const { return ___datetimeFormats_0; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_datetimeFormats_0() { return &___datetimeFormats_0; }
	inline void set_datetimeFormats_0(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___datetimeFormats_0 = value;
		Il2CppCodeGenWriteBarrier((&___datetimeFormats_0), value);
	}

	inline static int32_t get_offset_of_defaultDateTimeFormats_1() { return static_cast<int32_t>(offsetof(XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields, ___defaultDateTimeFormats_1)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_defaultDateTimeFormats_1() const { return ___defaultDateTimeFormats_1; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_defaultDateTimeFormats_1() { return &___defaultDateTimeFormats_1; }
	inline void set_defaultDateTimeFormats_1(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___defaultDateTimeFormats_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultDateTimeFormats_1), value);
	}

	inline static int32_t get_offset_of_roundtripDateTimeFormats_2() { return static_cast<int32_t>(offsetof(XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields, ___roundtripDateTimeFormats_2)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_roundtripDateTimeFormats_2() const { return ___roundtripDateTimeFormats_2; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_roundtripDateTimeFormats_2() { return &___roundtripDateTimeFormats_2; }
	inline void set_roundtripDateTimeFormats_2(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___roundtripDateTimeFormats_2 = value;
		Il2CppCodeGenWriteBarrier((&___roundtripDateTimeFormats_2), value);
	}

	inline static int32_t get_offset_of_localDateTimeFormats_3() { return static_cast<int32_t>(offsetof(XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields, ___localDateTimeFormats_3)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_localDateTimeFormats_3() const { return ___localDateTimeFormats_3; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_localDateTimeFormats_3() { return &___localDateTimeFormats_3; }
	inline void set_localDateTimeFormats_3(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___localDateTimeFormats_3 = value;
		Il2CppCodeGenWriteBarrier((&___localDateTimeFormats_3), value);
	}

	inline static int32_t get_offset_of_utcDateTimeFormats_4() { return static_cast<int32_t>(offsetof(XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields, ___utcDateTimeFormats_4)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_utcDateTimeFormats_4() const { return ___utcDateTimeFormats_4; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_utcDateTimeFormats_4() { return &___utcDateTimeFormats_4; }
	inline void set_utcDateTimeFormats_4(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___utcDateTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___utcDateTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_unspecifiedDateTimeFormats_5() { return static_cast<int32_t>(offsetof(XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields, ___unspecifiedDateTimeFormats_5)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_unspecifiedDateTimeFormats_5() const { return ___unspecifiedDateTimeFormats_5; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_unspecifiedDateTimeFormats_5() { return &___unspecifiedDateTimeFormats_5; }
	inline void set_unspecifiedDateTimeFormats_5(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___unspecifiedDateTimeFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___unspecifiedDateTimeFormats_5), value);
	}

	inline static int32_t get_offset_of__defaultStyle_6() { return static_cast<int32_t>(offsetof(XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields, ____defaultStyle_6)); }
	inline int32_t get__defaultStyle_6() const { return ____defaultStyle_6; }
	inline int32_t* get_address_of__defaultStyle_6() { return &____defaultStyle_6; }
	inline void set__defaultStyle_6(int32_t value)
	{
		____defaultStyle_6 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map33_7() { return static_cast<int32_t>(offsetof(XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields, ___U3CU3Ef__switchU24map33_7)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map33_7() const { return ___U3CU3Ef__switchU24map33_7; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map33_7() { return &___U3CU3Ef__switchU24map33_7; }
	inline void set_U3CU3Ef__switchU24map33_7(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map33_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map33_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCONVERT_T3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_H
#ifndef XMLNODECHANGEDEVENTARGS_T11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C_H
#define XMLNODECHANGEDEVENTARGS_T11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedEventArgs
struct  XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C  : public EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17
{
public:
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::_oldParent
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * ____oldParent_1;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::_newParent
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * ____newParent_2;
	// System.Xml.XmlNodeChangedAction System.Xml.XmlNodeChangedEventArgs::_action
	int32_t ____action_3;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::_node
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * ____node_4;
	// System.String System.Xml.XmlNodeChangedEventArgs::_oldValue
	String_t* ____oldValue_5;
	// System.String System.Xml.XmlNodeChangedEventArgs::_newValue
	String_t* ____newValue_6;

public:
	inline static int32_t get_offset_of__oldParent_1() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C, ____oldParent_1)); }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * get__oldParent_1() const { return ____oldParent_1; }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A ** get_address_of__oldParent_1() { return &____oldParent_1; }
	inline void set__oldParent_1(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * value)
	{
		____oldParent_1 = value;
		Il2CppCodeGenWriteBarrier((&____oldParent_1), value);
	}

	inline static int32_t get_offset_of__newParent_2() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C, ____newParent_2)); }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * get__newParent_2() const { return ____newParent_2; }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A ** get_address_of__newParent_2() { return &____newParent_2; }
	inline void set__newParent_2(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * value)
	{
		____newParent_2 = value;
		Il2CppCodeGenWriteBarrier((&____newParent_2), value);
	}

	inline static int32_t get_offset_of__action_3() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C, ____action_3)); }
	inline int32_t get__action_3() const { return ____action_3; }
	inline int32_t* get_address_of__action_3() { return &____action_3; }
	inline void set__action_3(int32_t value)
	{
		____action_3 = value;
	}

	inline static int32_t get_offset_of__node_4() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C, ____node_4)); }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * get__node_4() const { return ____node_4; }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A ** get_address_of__node_4() { return &____node_4; }
	inline void set__node_4(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * value)
	{
		____node_4 = value;
		Il2CppCodeGenWriteBarrier((&____node_4), value);
	}

	inline static int32_t get_offset_of__oldValue_5() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C, ____oldValue_5)); }
	inline String_t* get__oldValue_5() const { return ____oldValue_5; }
	inline String_t** get_address_of__oldValue_5() { return &____oldValue_5; }
	inline void set__oldValue_5(String_t* value)
	{
		____oldValue_5 = value;
		Il2CppCodeGenWriteBarrier((&____oldValue_5), value);
	}

	inline static int32_t get_offset_of__newValue_6() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C, ____newValue_6)); }
	inline String_t* get__newValue_6() const { return ____newValue_6; }
	inline String_t** get_address_of__newValue_6() { return &____newValue_6; }
	inline void set__newValue_6(String_t* value)
	{
		____newValue_6 = value;
		Il2CppCodeGenWriteBarrier((&____newValue_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDEVENTARGS_T11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C_H
#ifndef XMLNODEREADERIMPL_T7996B6B5A9618AF6532A1436BFD3E48ADD281F54_H
#define XMLNODEREADERIMPL_T7996B6B5A9618AF6532A1436BFD3E48ADD281F54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeReaderImpl
struct  XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// System.Xml.XmlDocument System.Xml.XmlNodeReaderImpl::document
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763 * ___document_2;
	// System.Xml.XmlNode System.Xml.XmlNodeReaderImpl::startNode
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * ___startNode_3;
	// System.Xml.XmlNode System.Xml.XmlNodeReaderImpl::current
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * ___current_4;
	// System.Xml.XmlNode System.Xml.XmlNodeReaderImpl::ownerLinkedNode
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * ___ownerLinkedNode_5;
	// System.Xml.ReadState System.Xml.XmlNodeReaderImpl::state
	int32_t ___state_6;
	// System.Int32 System.Xml.XmlNodeReaderImpl::depth
	int32_t ___depth_7;
	// System.Boolean System.Xml.XmlNodeReaderImpl::isEndElement
	bool ___isEndElement_8;
	// System.Boolean System.Xml.XmlNodeReaderImpl::ignoreStartNode
	bool ___ignoreStartNode_9;

public:
	inline static int32_t get_offset_of_document_2() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54, ___document_2)); }
	inline XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763 * get_document_2() const { return ___document_2; }
	inline XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763 ** get_address_of_document_2() { return &___document_2; }
	inline void set_document_2(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763 * value)
	{
		___document_2 = value;
		Il2CppCodeGenWriteBarrier((&___document_2), value);
	}

	inline static int32_t get_offset_of_startNode_3() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54, ___startNode_3)); }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * get_startNode_3() const { return ___startNode_3; }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A ** get_address_of_startNode_3() { return &___startNode_3; }
	inline void set_startNode_3(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * value)
	{
		___startNode_3 = value;
		Il2CppCodeGenWriteBarrier((&___startNode_3), value);
	}

	inline static int32_t get_offset_of_current_4() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54, ___current_4)); }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * get_current_4() const { return ___current_4; }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A ** get_address_of_current_4() { return &___current_4; }
	inline void set_current_4(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * value)
	{
		___current_4 = value;
		Il2CppCodeGenWriteBarrier((&___current_4), value);
	}

	inline static int32_t get_offset_of_ownerLinkedNode_5() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54, ___ownerLinkedNode_5)); }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * get_ownerLinkedNode_5() const { return ___ownerLinkedNode_5; }
	inline XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A ** get_address_of_ownerLinkedNode_5() { return &___ownerLinkedNode_5; }
	inline void set_ownerLinkedNode_5(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A * value)
	{
		___ownerLinkedNode_5 = value;
		Il2CppCodeGenWriteBarrier((&___ownerLinkedNode_5), value);
	}

	inline static int32_t get_offset_of_state_6() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54, ___state_6)); }
	inline int32_t get_state_6() const { return ___state_6; }
	inline int32_t* get_address_of_state_6() { return &___state_6; }
	inline void set_state_6(int32_t value)
	{
		___state_6 = value;
	}

	inline static int32_t get_offset_of_depth_7() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54, ___depth_7)); }
	inline int32_t get_depth_7() const { return ___depth_7; }
	inline int32_t* get_address_of_depth_7() { return &___depth_7; }
	inline void set_depth_7(int32_t value)
	{
		___depth_7 = value;
	}

	inline static int32_t get_offset_of_isEndElement_8() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54, ___isEndElement_8)); }
	inline bool get_isEndElement_8() const { return ___isEndElement_8; }
	inline bool* get_address_of_isEndElement_8() { return &___isEndElement_8; }
	inline void set_isEndElement_8(bool value)
	{
		___isEndElement_8 = value;
	}

	inline static int32_t get_offset_of_ignoreStartNode_9() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54, ___ignoreStartNode_9)); }
	inline bool get_ignoreStartNode_9() const { return ___ignoreStartNode_9; }
	inline bool* get_address_of_ignoreStartNode_9() { return &___ignoreStartNode_9; }
	inline void set_ignoreStartNode_9(bool value)
	{
		___ignoreStartNode_9 = value;
	}
};

struct XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNodeReaderImpl::<>f__switchU24map34
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map34_10;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNodeReaderImpl::<>f__switchU24map35
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map35_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNodeReaderImpl::<>f__switchU24map36
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map36_12;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map34_10() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54_StaticFields, ___U3CU3Ef__switchU24map34_10)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map34_10() const { return ___U3CU3Ef__switchU24map34_10; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map34_10() { return &___U3CU3Ef__switchU24map34_10; }
	inline void set_U3CU3Ef__switchU24map34_10(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map34_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map34_10), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map35_11() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54_StaticFields, ___U3CU3Ef__switchU24map35_11)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map35_11() const { return ___U3CU3Ef__switchU24map35_11; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map35_11() { return &___U3CU3Ef__switchU24map35_11; }
	inline void set_U3CU3Ef__switchU24map35_11(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map35_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map35_11), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map36_12() { return static_cast<int32_t>(offsetof(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54_StaticFields, ___U3CU3Ef__switchU24map36_12)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map36_12() const { return ___U3CU3Ef__switchU24map36_12; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map36_12() { return &___U3CU3Ef__switchU24map36_12; }
	inline void set_U3CU3Ef__switchU24map36_12(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map36_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map36_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEREADERIMPL_T7996B6B5A9618AF6532A1436BFD3E48ADD281F54_H
#ifndef XMLPARSERCONTEXT_TFCFB3C705A296076573E3517AAD1F1C187DACB14_H
#define XMLPARSERCONTEXT_TFCFB3C705A296076573E3517AAD1F1C187DACB14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserContext
struct  XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlParserContext::baseURI
	String_t* ___baseURI_0;
	// System.String System.Xml.XmlParserContext::docTypeName
	String_t* ___docTypeName_1;
	// System.Text.Encoding System.Xml.XmlParserContext::encoding
	Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * ___encoding_2;
	// System.String System.Xml.XmlParserContext::internalSubset
	String_t* ___internalSubset_3;
	// System.Xml.XmlNamespaceManager System.Xml.XmlParserContext::namespaceManager
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * ___namespaceManager_4;
	// System.Xml.XmlNameTable System.Xml.XmlParserContext::nameTable
	XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * ___nameTable_5;
	// System.String System.Xml.XmlParserContext::publicID
	String_t* ___publicID_6;
	// System.String System.Xml.XmlParserContext::systemID
	String_t* ___systemID_7;
	// System.String System.Xml.XmlParserContext::xmlLang
	String_t* ___xmlLang_8;
	// System.Xml.XmlSpace System.Xml.XmlParserContext::xmlSpace
	int32_t ___xmlSpace_9;
	// System.Collections.ArrayList System.Xml.XmlParserContext::contextItems
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___contextItems_10;
	// System.Int32 System.Xml.XmlParserContext::contextItemCount
	int32_t ___contextItemCount_11;
	// Mono.Xml.DTDObjectModel System.Xml.XmlParserContext::dtd
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * ___dtd_12;

public:
	inline static int32_t get_offset_of_baseURI_0() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___baseURI_0)); }
	inline String_t* get_baseURI_0() const { return ___baseURI_0; }
	inline String_t** get_address_of_baseURI_0() { return &___baseURI_0; }
	inline void set_baseURI_0(String_t* value)
	{
		___baseURI_0 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_0), value);
	}

	inline static int32_t get_offset_of_docTypeName_1() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___docTypeName_1)); }
	inline String_t* get_docTypeName_1() const { return ___docTypeName_1; }
	inline String_t** get_address_of_docTypeName_1() { return &___docTypeName_1; }
	inline void set_docTypeName_1(String_t* value)
	{
		___docTypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___docTypeName_1), value);
	}

	inline static int32_t get_offset_of_encoding_2() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___encoding_2)); }
	inline Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * get_encoding_2() const { return ___encoding_2; }
	inline Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 ** get_address_of_encoding_2() { return &___encoding_2; }
	inline void set_encoding_2(Encoding_t07D1CA045FCD18F862F7308C8477B5C2E1A0CBE1 * value)
	{
		___encoding_2 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_2), value);
	}

	inline static int32_t get_offset_of_internalSubset_3() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___internalSubset_3)); }
	inline String_t* get_internalSubset_3() const { return ___internalSubset_3; }
	inline String_t** get_address_of_internalSubset_3() { return &___internalSubset_3; }
	inline void set_internalSubset_3(String_t* value)
	{
		___internalSubset_3 = value;
		Il2CppCodeGenWriteBarrier((&___internalSubset_3), value);
	}

	inline static int32_t get_offset_of_namespaceManager_4() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___namespaceManager_4)); }
	inline XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * get_namespaceManager_4() const { return ___namespaceManager_4; }
	inline XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 ** get_address_of_namespaceManager_4() { return &___namespaceManager_4; }
	inline void set_namespaceManager_4(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86 * value)
	{
		___namespaceManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceManager_4), value);
	}

	inline static int32_t get_offset_of_nameTable_5() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___nameTable_5)); }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * get_nameTable_5() const { return ___nameTable_5; }
	inline XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B ** get_address_of_nameTable_5() { return &___nameTable_5; }
	inline void set_nameTable_5(XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B * value)
	{
		___nameTable_5 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_5), value);
	}

	inline static int32_t get_offset_of_publicID_6() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___publicID_6)); }
	inline String_t* get_publicID_6() const { return ___publicID_6; }
	inline String_t** get_address_of_publicID_6() { return &___publicID_6; }
	inline void set_publicID_6(String_t* value)
	{
		___publicID_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicID_6), value);
	}

	inline static int32_t get_offset_of_systemID_7() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___systemID_7)); }
	inline String_t* get_systemID_7() const { return ___systemID_7; }
	inline String_t** get_address_of_systemID_7() { return &___systemID_7; }
	inline void set_systemID_7(String_t* value)
	{
		___systemID_7 = value;
		Il2CppCodeGenWriteBarrier((&___systemID_7), value);
	}

	inline static int32_t get_offset_of_xmlLang_8() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___xmlLang_8)); }
	inline String_t* get_xmlLang_8() const { return ___xmlLang_8; }
	inline String_t** get_address_of_xmlLang_8() { return &___xmlLang_8; }
	inline void set_xmlLang_8(String_t* value)
	{
		___xmlLang_8 = value;
		Il2CppCodeGenWriteBarrier((&___xmlLang_8), value);
	}

	inline static int32_t get_offset_of_xmlSpace_9() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___xmlSpace_9)); }
	inline int32_t get_xmlSpace_9() const { return ___xmlSpace_9; }
	inline int32_t* get_address_of_xmlSpace_9() { return &___xmlSpace_9; }
	inline void set_xmlSpace_9(int32_t value)
	{
		___xmlSpace_9 = value;
	}

	inline static int32_t get_offset_of_contextItems_10() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___contextItems_10)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_contextItems_10() const { return ___contextItems_10; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_contextItems_10() { return &___contextItems_10; }
	inline void set_contextItems_10(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___contextItems_10 = value;
		Il2CppCodeGenWriteBarrier((&___contextItems_10), value);
	}

	inline static int32_t get_offset_of_contextItemCount_11() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___contextItemCount_11)); }
	inline int32_t get_contextItemCount_11() const { return ___contextItemCount_11; }
	inline int32_t* get_address_of_contextItemCount_11() { return &___contextItemCount_11; }
	inline void set_contextItemCount_11(int32_t value)
	{
		___contextItemCount_11 = value;
	}

	inline static int32_t get_offset_of_dtd_12() { return static_cast<int32_t>(offsetof(XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14, ___dtd_12)); }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * get_dtd_12() const { return ___dtd_12; }
	inline DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E ** get_address_of_dtd_12() { return &___dtd_12; }
	inline void set_dtd_12(DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E * value)
	{
		___dtd_12 = value;
		Il2CppCodeGenWriteBarrier((&___dtd_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPARSERCONTEXT_TFCFB3C705A296076573E3517AAD1F1C187DACB14_H
#ifndef CONTEXTITEM_T826C1332DE94B9C4C391C335BC117442D86CBD25_H
#define CONTEXTITEM_T826C1332DE94B9C4C391C335BC117442D86CBD25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlParserContext_ContextItem
struct  ContextItem_t826C1332DE94B9C4C391C335BC117442D86CBD25  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlParserContext_ContextItem::BaseURI
	String_t* ___BaseURI_0;
	// System.String System.Xml.XmlParserContext_ContextItem::XmlLang
	String_t* ___XmlLang_1;
	// System.Xml.XmlSpace System.Xml.XmlParserContext_ContextItem::XmlSpace
	int32_t ___XmlSpace_2;

public:
	inline static int32_t get_offset_of_BaseURI_0() { return static_cast<int32_t>(offsetof(ContextItem_t826C1332DE94B9C4C391C335BC117442D86CBD25, ___BaseURI_0)); }
	inline String_t* get_BaseURI_0() const { return ___BaseURI_0; }
	inline String_t** get_address_of_BaseURI_0() { return &___BaseURI_0; }
	inline void set_BaseURI_0(String_t* value)
	{
		___BaseURI_0 = value;
		Il2CppCodeGenWriteBarrier((&___BaseURI_0), value);
	}

	inline static int32_t get_offset_of_XmlLang_1() { return static_cast<int32_t>(offsetof(ContextItem_t826C1332DE94B9C4C391C335BC117442D86CBD25, ___XmlLang_1)); }
	inline String_t* get_XmlLang_1() const { return ___XmlLang_1; }
	inline String_t** get_address_of_XmlLang_1() { return &___XmlLang_1; }
	inline void set_XmlLang_1(String_t* value)
	{
		___XmlLang_1 = value;
		Il2CppCodeGenWriteBarrier((&___XmlLang_1), value);
	}

	inline static int32_t get_offset_of_XmlSpace_2() { return static_cast<int32_t>(offsetof(ContextItem_t826C1332DE94B9C4C391C335BC117442D86CBD25, ___XmlSpace_2)); }
	inline int32_t get_XmlSpace_2() const { return ___XmlSpace_2; }
	inline int32_t* get_address_of_XmlSpace_2() { return &___XmlSpace_2; }
	inline void set_XmlSpace_2(int32_t value)
	{
		___XmlSpace_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXTITEM_T826C1332DE94B9C4C391C335BC117442D86CBD25_H
#ifndef XMLREADERBINARYSUPPORT_TACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A_H
#define XMLREADERBINARYSUPPORT_TACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderBinarySupport
struct  XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A  : public RuntimeObject
{
public:
	// System.Xml.XmlReader System.Xml.XmlReaderBinarySupport::reader
	XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * ___reader_0;
	// System.Int32 System.Xml.XmlReaderBinarySupport::base64CacheStartsAt
	int32_t ___base64CacheStartsAt_1;
	// System.Xml.XmlReaderBinarySupport_CommandState System.Xml.XmlReaderBinarySupport::state
	int32_t ___state_2;
	// System.Boolean System.Xml.XmlReaderBinarySupport::hasCache
	bool ___hasCache_3;
	// System.Boolean System.Xml.XmlReaderBinarySupport::dontReset
	bool ___dontReset_4;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A, ___reader_0)); }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * get_reader_0() const { return ___reader_0; }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_base64CacheStartsAt_1() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A, ___base64CacheStartsAt_1)); }
	inline int32_t get_base64CacheStartsAt_1() const { return ___base64CacheStartsAt_1; }
	inline int32_t* get_address_of_base64CacheStartsAt_1() { return &___base64CacheStartsAt_1; }
	inline void set_base64CacheStartsAt_1(int32_t value)
	{
		___base64CacheStartsAt_1 = value;
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A, ___state_2)); }
	inline int32_t get_state_2() const { return ___state_2; }
	inline int32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(int32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_hasCache_3() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A, ___hasCache_3)); }
	inline bool get_hasCache_3() const { return ___hasCache_3; }
	inline bool* get_address_of_hasCache_3() { return &___hasCache_3; }
	inline void set_hasCache_3(bool value)
	{
		___hasCache_3 = value;
	}

	inline static int32_t get_offset_of_dontReset_4() { return static_cast<int32_t>(offsetof(XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A, ___dontReset_4)); }
	inline bool get_dontReset_4() const { return ___dontReset_4; }
	inline bool* get_address_of_dontReset_4() { return &___dontReset_4; }
	inline void set_dontReset_4(bool value)
	{
		___dontReset_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADERBINARYSUPPORT_TACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A_H
#ifndef XMLREADERSETTINGS_TCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB_H
#define XMLREADERSETTINGS_TCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderSettings
struct  XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.XmlReaderSettings::checkCharacters
	bool ___checkCharacters_0;
	// System.Xml.ConformanceLevel System.Xml.XmlReaderSettings::conformance
	int32_t ___conformance_1;
	// System.Xml.Schema.XmlSchemaSet System.Xml.XmlReaderSettings::schemas
	XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * ___schemas_2;
	// System.Boolean System.Xml.XmlReaderSettings::schemasNeedsInitialization
	bool ___schemasNeedsInitialization_3;
	// System.Xml.Schema.XmlSchemaValidationFlags System.Xml.XmlReaderSettings::validationFlags
	int32_t ___validationFlags_4;

public:
	inline static int32_t get_offset_of_checkCharacters_0() { return static_cast<int32_t>(offsetof(XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB, ___checkCharacters_0)); }
	inline bool get_checkCharacters_0() const { return ___checkCharacters_0; }
	inline bool* get_address_of_checkCharacters_0() { return &___checkCharacters_0; }
	inline void set_checkCharacters_0(bool value)
	{
		___checkCharacters_0 = value;
	}

	inline static int32_t get_offset_of_conformance_1() { return static_cast<int32_t>(offsetof(XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB, ___conformance_1)); }
	inline int32_t get_conformance_1() const { return ___conformance_1; }
	inline int32_t* get_address_of_conformance_1() { return &___conformance_1; }
	inline void set_conformance_1(int32_t value)
	{
		___conformance_1 = value;
	}

	inline static int32_t get_offset_of_schemas_2() { return static_cast<int32_t>(offsetof(XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB, ___schemas_2)); }
	inline XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * get_schemas_2() const { return ___schemas_2; }
	inline XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D ** get_address_of_schemas_2() { return &___schemas_2; }
	inline void set_schemas_2(XmlSchemaSet_t57B9F8C94E8255E9EB78EB1F55CD69BB7EBFEC8D * value)
	{
		___schemas_2 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_2), value);
	}

	inline static int32_t get_offset_of_schemasNeedsInitialization_3() { return static_cast<int32_t>(offsetof(XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB, ___schemasNeedsInitialization_3)); }
	inline bool get_schemasNeedsInitialization_3() const { return ___schemasNeedsInitialization_3; }
	inline bool* get_address_of_schemasNeedsInitialization_3() { return &___schemasNeedsInitialization_3; }
	inline void set_schemasNeedsInitialization_3(bool value)
	{
		___schemasNeedsInitialization_3 = value;
	}

	inline static int32_t get_offset_of_validationFlags_4() { return static_cast<int32_t>(offsetof(XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB, ___validationFlags_4)); }
	inline int32_t get_validationFlags_4() const { return ___validationFlags_4; }
	inline int32_t* get_address_of_validationFlags_4() { return &___validationFlags_4; }
	inline void set_validationFlags_4(int32_t value)
	{
		___validationFlags_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADERSETTINGS_TCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB_H
#ifndef XMLSIGNIFICANTWHITESPACE_T7E151A744639F09EDE3057CA5B18C4E1F6A50103_H
#define XMLSIGNIFICANTWHITESPACE_T7E151A744639F09EDE3057CA5B18C4E1F6A50103_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSignificantWhitespace
struct  XmlSignificantWhitespace_t7E151A744639F09EDE3057CA5B18C4E1F6A50103  : public XmlCharacterData_t1733676F46934DE88DE8DEC9CF47190ABAE32F07
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSIGNIFICANTWHITESPACE_T7E151A744639F09EDE3057CA5B18C4E1F6A50103_H
#ifndef XMLTEXT_T5F596B3ED20FB449A77E07BFA11CCE56E36E9D89_H
#define XMLTEXT_T5F596B3ED20FB449A77E07BFA11CCE56E36E9D89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlText
struct  XmlText_t5F596B3ED20FB449A77E07BFA11CCE56E36E9D89  : public XmlCharacterData_t1733676F46934DE88DE8DEC9CF47190ABAE32F07
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXT_T5F596B3ED20FB449A77E07BFA11CCE56E36E9D89_H
#ifndef XMLVALIDATINGREADER_T524BE1B3546FA8855DD8C66D124AC456FB68D3E6_H
#define XMLVALIDATINGREADER_T524BE1B3546FA8855DD8C66D124AC456FB68D3E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlValidatingReader
struct  XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// System.Xml.EntityHandling System.Xml.XmlValidatingReader::entityHandling
	int32_t ___entityHandling_2;
	// System.Xml.XmlReader System.Xml.XmlValidatingReader::sourceReader
	XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * ___sourceReader_3;
	// System.Xml.XmlTextReader System.Xml.XmlValidatingReader::xmlTextReader
	XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 * ___xmlTextReader_4;
	// System.Xml.XmlReader System.Xml.XmlValidatingReader::validatingReader
	XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * ___validatingReader_5;
	// System.Xml.XmlResolver System.Xml.XmlValidatingReader::resolver
	XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * ___resolver_6;
	// System.Boolean System.Xml.XmlValidatingReader::resolverSpecified
	bool ___resolverSpecified_7;
	// System.Xml.ValidationType System.Xml.XmlValidatingReader::validationType
	int32_t ___validationType_8;
	// System.Xml.Schema.XmlSchemaCollection System.Xml.XmlValidatingReader::schemas
	XmlSchemaCollection_t532C46B1FC9D97F471412FAC2FFCD7F7A93E3513 * ___schemas_9;
	// Mono.Xml.DTDValidatingReader System.Xml.XmlValidatingReader::dtdReader
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8 * ___dtdReader_10;
	// Mono.Xml.IHasXmlSchemaInfo System.Xml.XmlValidatingReader::schemaInfo
	RuntimeObject* ___schemaInfo_11;
	// System.Text.StringBuilder System.Xml.XmlValidatingReader::storedCharacters
	StringBuilder_t * ___storedCharacters_12;
	// System.Xml.Schema.ValidationEventHandler System.Xml.XmlValidatingReader::ValidationEventHandler
	ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * ___ValidationEventHandler_13;

public:
	inline static int32_t get_offset_of_entityHandling_2() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6, ___entityHandling_2)); }
	inline int32_t get_entityHandling_2() const { return ___entityHandling_2; }
	inline int32_t* get_address_of_entityHandling_2() { return &___entityHandling_2; }
	inline void set_entityHandling_2(int32_t value)
	{
		___entityHandling_2 = value;
	}

	inline static int32_t get_offset_of_sourceReader_3() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6, ___sourceReader_3)); }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * get_sourceReader_3() const { return ___sourceReader_3; }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 ** get_address_of_sourceReader_3() { return &___sourceReader_3; }
	inline void set_sourceReader_3(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * value)
	{
		___sourceReader_3 = value;
		Il2CppCodeGenWriteBarrier((&___sourceReader_3), value);
	}

	inline static int32_t get_offset_of_xmlTextReader_4() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6, ___xmlTextReader_4)); }
	inline XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 * get_xmlTextReader_4() const { return ___xmlTextReader_4; }
	inline XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 ** get_address_of_xmlTextReader_4() { return &___xmlTextReader_4; }
	inline void set_xmlTextReader_4(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 * value)
	{
		___xmlTextReader_4 = value;
		Il2CppCodeGenWriteBarrier((&___xmlTextReader_4), value);
	}

	inline static int32_t get_offset_of_validatingReader_5() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6, ___validatingReader_5)); }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * get_validatingReader_5() const { return ___validatingReader_5; }
	inline XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 ** get_address_of_validatingReader_5() { return &___validatingReader_5; }
	inline void set_validatingReader_5(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293 * value)
	{
		___validatingReader_5 = value;
		Il2CppCodeGenWriteBarrier((&___validatingReader_5), value);
	}

	inline static int32_t get_offset_of_resolver_6() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6, ___resolver_6)); }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * get_resolver_6() const { return ___resolver_6; }
	inline XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C ** get_address_of_resolver_6() { return &___resolver_6; }
	inline void set_resolver_6(XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C * value)
	{
		___resolver_6 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_6), value);
	}

	inline static int32_t get_offset_of_resolverSpecified_7() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6, ___resolverSpecified_7)); }
	inline bool get_resolverSpecified_7() const { return ___resolverSpecified_7; }
	inline bool* get_address_of_resolverSpecified_7() { return &___resolverSpecified_7; }
	inline void set_resolverSpecified_7(bool value)
	{
		___resolverSpecified_7 = value;
	}

	inline static int32_t get_offset_of_validationType_8() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6, ___validationType_8)); }
	inline int32_t get_validationType_8() const { return ___validationType_8; }
	inline int32_t* get_address_of_validationType_8() { return &___validationType_8; }
	inline void set_validationType_8(int32_t value)
	{
		___validationType_8 = value;
	}

	inline static int32_t get_offset_of_schemas_9() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6, ___schemas_9)); }
	inline XmlSchemaCollection_t532C46B1FC9D97F471412FAC2FFCD7F7A93E3513 * get_schemas_9() const { return ___schemas_9; }
	inline XmlSchemaCollection_t532C46B1FC9D97F471412FAC2FFCD7F7A93E3513 ** get_address_of_schemas_9() { return &___schemas_9; }
	inline void set_schemas_9(XmlSchemaCollection_t532C46B1FC9D97F471412FAC2FFCD7F7A93E3513 * value)
	{
		___schemas_9 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_9), value);
	}

	inline static int32_t get_offset_of_dtdReader_10() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6, ___dtdReader_10)); }
	inline DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8 * get_dtdReader_10() const { return ___dtdReader_10; }
	inline DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8 ** get_address_of_dtdReader_10() { return &___dtdReader_10; }
	inline void set_dtdReader_10(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8 * value)
	{
		___dtdReader_10 = value;
		Il2CppCodeGenWriteBarrier((&___dtdReader_10), value);
	}

	inline static int32_t get_offset_of_schemaInfo_11() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6, ___schemaInfo_11)); }
	inline RuntimeObject* get_schemaInfo_11() const { return ___schemaInfo_11; }
	inline RuntimeObject** get_address_of_schemaInfo_11() { return &___schemaInfo_11; }
	inline void set_schemaInfo_11(RuntimeObject* value)
	{
		___schemaInfo_11 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_11), value);
	}

	inline static int32_t get_offset_of_storedCharacters_12() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6, ___storedCharacters_12)); }
	inline StringBuilder_t * get_storedCharacters_12() const { return ___storedCharacters_12; }
	inline StringBuilder_t ** get_address_of_storedCharacters_12() { return &___storedCharacters_12; }
	inline void set_storedCharacters_12(StringBuilder_t * value)
	{
		___storedCharacters_12 = value;
		Il2CppCodeGenWriteBarrier((&___storedCharacters_12), value);
	}

	inline static int32_t get_offset_of_ValidationEventHandler_13() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6, ___ValidationEventHandler_13)); }
	inline ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * get_ValidationEventHandler_13() const { return ___ValidationEventHandler_13; }
	inline ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B ** get_address_of_ValidationEventHandler_13() { return &___ValidationEventHandler_13; }
	inline void set_ValidationEventHandler_13(ValidationEventHandler_t4E1A0D7874804802BF369D671E548EE69B97AD6B * value)
	{
		___ValidationEventHandler_13 = value;
		Il2CppCodeGenWriteBarrier((&___ValidationEventHandler_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLVALIDATINGREADER_T524BE1B3546FA8855DD8C66D124AC456FB68D3E6_H
#ifndef XMLWHITESPACE_T94DE163BDCCE5B034799544EE082260E57B4D53C_H
#define XMLWHITESPACE_T94DE163BDCCE5B034799544EE082260E57B4D53C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWhitespace
struct  XmlWhitespace_t94DE163BDCCE5B034799544EE082260E57B4D53C  : public XmlCharacterData_t1733676F46934DE88DE8DEC9CF47190ABAE32F07
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWHITESPACE_T94DE163BDCCE5B034799544EE082260E57B4D53C_H
#ifndef XMLATTRIBUTETOKENINFO_TFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2_H
#define XMLATTRIBUTETOKENINFO_TFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Xml2.XmlTextReader_XmlAttributeTokenInfo
struct  XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2  : public XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8
{
public:
	// System.Int32 Mono.Xml2.XmlTextReader_XmlAttributeTokenInfo::ValueTokenStartIndex
	int32_t ___ValueTokenStartIndex_13;
	// System.Int32 Mono.Xml2.XmlTextReader_XmlAttributeTokenInfo::ValueTokenEndIndex
	int32_t ___ValueTokenEndIndex_14;
	// System.String Mono.Xml2.XmlTextReader_XmlAttributeTokenInfo::valueCache
	String_t* ___valueCache_15;
	// System.Text.StringBuilder Mono.Xml2.XmlTextReader_XmlAttributeTokenInfo::tmpBuilder
	StringBuilder_t * ___tmpBuilder_16;

public:
	inline static int32_t get_offset_of_ValueTokenStartIndex_13() { return static_cast<int32_t>(offsetof(XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2, ___ValueTokenStartIndex_13)); }
	inline int32_t get_ValueTokenStartIndex_13() const { return ___ValueTokenStartIndex_13; }
	inline int32_t* get_address_of_ValueTokenStartIndex_13() { return &___ValueTokenStartIndex_13; }
	inline void set_ValueTokenStartIndex_13(int32_t value)
	{
		___ValueTokenStartIndex_13 = value;
	}

	inline static int32_t get_offset_of_ValueTokenEndIndex_14() { return static_cast<int32_t>(offsetof(XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2, ___ValueTokenEndIndex_14)); }
	inline int32_t get_ValueTokenEndIndex_14() const { return ___ValueTokenEndIndex_14; }
	inline int32_t* get_address_of_ValueTokenEndIndex_14() { return &___ValueTokenEndIndex_14; }
	inline void set_ValueTokenEndIndex_14(int32_t value)
	{
		___ValueTokenEndIndex_14 = value;
	}

	inline static int32_t get_offset_of_valueCache_15() { return static_cast<int32_t>(offsetof(XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2, ___valueCache_15)); }
	inline String_t* get_valueCache_15() const { return ___valueCache_15; }
	inline String_t** get_address_of_valueCache_15() { return &___valueCache_15; }
	inline void set_valueCache_15(String_t* value)
	{
		___valueCache_15 = value;
		Il2CppCodeGenWriteBarrier((&___valueCache_15), value);
	}

	inline static int32_t get_offset_of_tmpBuilder_16() { return static_cast<int32_t>(offsetof(XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2, ___tmpBuilder_16)); }
	inline StringBuilder_t * get_tmpBuilder_16() const { return ___tmpBuilder_16; }
	inline StringBuilder_t ** get_address_of_tmpBuilder_16() { return &___tmpBuilder_16; }
	inline void set_tmpBuilder_16(StringBuilder_t * value)
	{
		___tmpBuilder_16 = value;
		Il2CppCodeGenWriteBarrier((&___tmpBuilder_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTETOKENINFO_TFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2_H
#ifndef CHARGETTER_T41F2B070679160162013D7D2B99DF5A72133136B_H
#define CHARGETTER_T41F2B070679160162013D7D2B99DF5A72133136B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReaderBinarySupport_CharGetter
struct  CharGetter_t41F2B070679160162013D7D2B99DF5A72133136B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHARGETTER_T41F2B070679160162013D7D2B99DF5A72133136B_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (DTDInvalidAutomata_tDF68F8C0E639715110419CEA22B09F5F30CCE0DB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1501[23] = 
{
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_factory_0(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_rootAutomata_1(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_emptyAutomata_2(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_anyAutomata_3(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_invalidAutomata_4(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_elementDecls_5(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_attListDecls_6(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_peDecls_7(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_entityDecls_8(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_notationDecls_9(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_validationErrors_10(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_resolver_11(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_nameTable_12(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_externalResources_13(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_baseURI_14(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_name_15(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_publicId_16(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_systemId_17(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_intSubset_18(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_intSubsetHasPERef_19(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_isStandalone_20(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_lineNumber_21(),
	DTDObjectModel_t1F8566CF41C2964FF7D10C0F00255A1A2EA1682E::get_offset_of_linePosition_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (DictionaryBase_t415336F093F8CDFAF278AA55A0F4A0FD46780582), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (U3CU3Ec__Iterator3_tD90F8F0854A5877DB2152760FE7E79AC7CFC207A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1503[5] = 
{
	U3CU3Ec__Iterator3_tD90F8F0854A5877DB2152760FE7E79AC7CFC207A::get_offset_of_U3CU24s_50U3E__0_0(),
	U3CU3Ec__Iterator3_tD90F8F0854A5877DB2152760FE7E79AC7CFC207A::get_offset_of_U3CpU3E__1_1(),
	U3CU3Ec__Iterator3_tD90F8F0854A5877DB2152760FE7E79AC7CFC207A::get_offset_of_U24PC_2(),
	U3CU3Ec__Iterator3_tD90F8F0854A5877DB2152760FE7E79AC7CFC207A::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator3_tD90F8F0854A5877DB2152760FE7E79AC7CFC207A::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { sizeof (DTDCollectionBase_tBA57464C8928A0229E33734BCF832A90138D0E4F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1504[1] = 
{
	DTDCollectionBase_tBA57464C8928A0229E33734BCF832A90138D0E4F::get_offset_of_root_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { sizeof (DTDElementDeclarationCollection_t45A928E2967593285196499A173437F6AB3AE89D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (DTDAttListDeclarationCollection_tCDEF4AACF366E8B2298CAB3070235D432B854F55), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { sizeof (DTDEntityDeclarationCollection_t94CA8138E40F051734624B32D031268F326F1995), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (DTDNotationDeclarationCollection_tF5A146D5CFBA10C2C7B82FDCD76FA19A301C936C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1509[7] = 
{
	DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF::get_offset_of_root_5(),
	DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF::get_offset_of_compiledAutomata_6(),
	DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF::get_offset_of_ownerElementName_7(),
	DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF::get_offset_of_elementName_8(),
	DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF::get_offset_of_orderType_9(),
	DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF::get_offset_of_childModels_10(),
	DTDContentModel_t4D28535870F690FE6FA369E5F22EA0C7411B2BFF::get_offset_of_occurence_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (DTDContentModelCollection_t0382EF04D32FF6333D71752F3C4E9CC1EE5B2664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1510[1] = 
{
	DTDContentModelCollection_t0382EF04D32FF6333D71752F3C4E9CC1EE5B2664::get_offset_of_contentModel_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1511[5] = 
{
	DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B::get_offset_of_root_0(),
	DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B::get_offset_of_isInternalSubset_1(),
	DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B::get_offset_of_baseURI_2(),
	DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B::get_offset_of_lineNumber_3(),
	DTDNode_t204A6AC80A2ECBA8ED3B3256E3AD7BA59AD4A27B::get_offset_of_linePosition_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1512[6] = 
{
	DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245::get_offset_of_root_5(),
	DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245::get_offset_of_contentModel_6(),
	DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245::get_offset_of_name_7(),
	DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245::get_offset_of_isEmpty_8(),
	DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245::get_offset_of_isAny_9(),
	DTDElementDeclaration_t5D44E3F438BC3A6A945C7D896AFED56B7DC95245::get_offset_of_isMixedContent_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1513[7] = 
{
	DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854::get_offset_of_name_5(),
	DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854::get_offset_of_datatype_6(),
	DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854::get_offset_of_enumeratedLiterals_7(),
	DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854::get_offset_of_unresolvedDefault_8(),
	DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854::get_offset_of_enumeratedNotations_9(),
	DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854::get_offset_of_occurenceType_10(),
	DTDAttributeDefinition_tF1F8AADA0445606A6837FA0CBA58A5237FC89854::get_offset_of_resolvedDefaultValue_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (DTDAttListDeclaration_t8E0E85C9F6362A01A1B30700071A49A2B87EDBF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1514[3] = 
{
	DTDAttListDeclaration_t8E0E85C9F6362A01A1B30700071A49A2B87EDBF4::get_offset_of_name_5(),
	DTDAttListDeclaration_t8E0E85C9F6362A01A1B30700071A49A2B87EDBF4::get_offset_of_attributeOrders_6(),
	DTDAttListDeclaration_t8E0E85C9F6362A01A1B30700071A49A2B87EDBF4::get_offset_of_attributes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1515[10] = 
{
	DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0::get_offset_of_name_5(),
	DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0::get_offset_of_publicId_6(),
	DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0::get_offset_of_systemId_7(),
	DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0::get_offset_of_literalValue_8(),
	DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0::get_offset_of_replacementText_9(),
	DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0::get_offset_of_uriString_10(),
	DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0::get_offset_of_absUri_11(),
	DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0::get_offset_of_isInvalid_12(),
	DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0::get_offset_of_loadFailed_13(),
	DTDEntityBase_t9C0EEBF5DD51045377C18DFACA72A60EA10DA4A0::get_offset_of_resolver_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1516[6] = 
{
	DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4::get_offset_of_entityValue_15(),
	DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4::get_offset_of_notationName_16(),
	DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4::get_offset_of_ReferencingEntities_17(),
	DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4::get_offset_of_scanned_18(),
	DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4::get_offset_of_recursed_19(),
	DTDEntityDeclaration_t3264D7AD9136E2D78D76857FFB546C5733C507C4::get_offset_of_hasExternalReference_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (DTDNotationDeclaration_t4AE86A46E6453976D99438636B220B04F4870087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1517[5] = 
{
	DTDNotationDeclaration_t4AE86A46E6453976D99438636B220B04F4870087::get_offset_of_name_5(),
	DTDNotationDeclaration_t4AE86A46E6453976D99438636B220B04F4870087::get_offset_of_localName_6(),
	DTDNotationDeclaration_t4AE86A46E6453976D99438636B220B04F4870087::get_offset_of_prefix_7(),
	DTDNotationDeclaration_t4AE86A46E6453976D99438636B220B04F4870087::get_offset_of_publicId_8(),
	DTDNotationDeclaration_t4AE86A46E6453976D99438636B220B04F4870087::get_offset_of_systemId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (DTDParameterEntityDeclarationCollection_tA4A3C5F34157BEF4F00F640C63D94D48ABA0773E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1518[2] = 
{
	DTDParameterEntityDeclarationCollection_tA4A3C5F34157BEF4F00F640C63D94D48ABA0773E::get_offset_of_peDecls_0(),
	DTDParameterEntityDeclarationCollection_tA4A3C5F34157BEF4F00F640C63D94D48ABA0773E::get_offset_of_root_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (DTDParameterEntityDeclaration_tD34D6D3351BDB37AB92E73D4A58C2A03E5724D7B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (DTDContentOrderType_tA131422D00D6DE5AEDAACD0D6A47DA17943C7DD0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1520[4] = 
{
	DTDContentOrderType_tA131422D00D6DE5AEDAACD0D6A47DA17943C7DD0::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (DTDAttributeOccurenceType_t29080CCF7A7910C586B173D49EA08A534E25F5C1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1521[5] = 
{
	DTDAttributeOccurenceType_t29080CCF7A7910C586B173D49EA08A534E25F5C1::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (DTDOccurence_tA32D6D2BDB3ACD58BADB3B855835C5C319851208)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1522[5] = 
{
	DTDOccurence_tA32D6D2BDB3ACD58BADB3B855835C5C319851208::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1523[14] = 
{
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_currentInput_0(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_parserInputStack_1(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_nameBuffer_2(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_nameLength_3(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_nameCapacity_4(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_valueBuffer_5(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_currentLinkedNodeLineNumber_6(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_currentLinkedNodeLinePosition_7(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_dtdIncludeSect_8(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_normalization_9(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_processingInternalSubset_10(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_cachedPublicId_11(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_cachedSystemId_12(),
	DTDReader_t03BE21B5FBC09423B45FCDD39D86F5C52D78AF9B::get_offset_of_DTD_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8), -1, sizeof(DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1524[29] = 
{
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_reader_2(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_sourceTextReader_3(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_validatingReader_4(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_dtd_5(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_resolver_6(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_currentElement_7(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_attributes_8(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_attributeCount_9(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_currentAttribute_10(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_consumedAttribute_11(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_elementStack_12(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_automataStack_13(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_popScope_14(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_isStandalone_15(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_currentAutomata_16(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_previousAutomata_17(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_idList_18(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_missingIDReferences_19(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_nsmgr_20(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_currentTextValue_21(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_constructingTextValue_22(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_shouldResetCurrentTextValue_23(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_isSignificantWhitespace_24(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_isWhitespace_25(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_isText_26(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_attributeValueEntityStack_27(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_valueBuilder_28(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8::get_offset_of_whitespaceChars_29(),
	DTDValidatingReader_t45B1D9125A5B1CF4DF112F19FC82C354D63B3DC8_StaticFields::get_offset_of_U3CU3Ef__switchU24map2A_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1525[6] = 
{
	AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438::get_offset_of_Name_0(),
	AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438::get_offset_of_LocalName_1(),
	AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438::get_offset_of_NS_2(),
	AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438::get_offset_of_Prefix_3(),
	AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438::get_offset_of_Value_4(),
	AttributeSlot_t08F6F94BF6512EF49DD57A33043840256A01E438::get_offset_of_IsDefault_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1526[8] = 
{
	EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F::get_offset_of_entity_2(),
	EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F::get_offset_of_source_3(),
	EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F::get_offset_of_context_4(),
	EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F::get_offset_of_resolver_5(),
	EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F::get_offset_of_entity_handling_6(),
	EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F::get_offset_of_entity_inside_attr_7(),
	EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F::get_offset_of_inside_attr_8(),
	EntityResolvingXmlReader_t37F12E545D4CEBDBE94AE87E3207C175BCBACC4F::get_offset_of_do_resolve_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (EntityHandling_tF590E11CFBDA825293A1964A1234087114F4B98D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1527[3] = 
{
	EntityHandling_tF590E11CFBDA825293A1964A1234087114F4B98D::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (Formatting_t2EC8ADDABC4B928B58EA5351103138C3579EC1E4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1528[3] = 
{
	Formatting_t2EC8ADDABC4B928B58EA5351103138C3579EC1E4::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1532[3] = 
{
	NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6::get_offset_of_count_0(),
	NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6::get_offset_of_buckets_1(),
	NameTable_tE47F4B558F0AA75A29E063658B7BCB3CE6E482E6::get_offset_of_size_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1533[4] = 
{
	Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA::get_offset_of_str_0(),
	Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA::get_offset_of_hash_1(),
	Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA::get_offset_of_len_2(),
	Entry_tB2500B931CE6347D59272793DA70C3264BF4CDFA::get_offset_of_next_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (NamespaceHandling_t2F02A279F33A1348B8E4AB835A10E0C06237939A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1534[3] = 
{
	NamespaceHandling_t2F02A279F33A1348B8E4AB835A10E0C06237939A::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (NewLineHandling_tC5B27F2FDF6CAA10BBBB61E1F03BAF891FF30FC9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1535[4] = 
{
	NewLineHandling_tC5B27F2FDF6CAA10BBBB61E1F03BAF891FF30FC9::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (ReadState_tEB8765F42B4F591ECA14C88336750FF5E198AAF1)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1536[6] = 
{
	ReadState_tEB8765F42B4F591ECA14C88336750FF5E198AAF1::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (ValidationType_t31525A080D050BCFADE54364812432A4C5BDBE63)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1537[6] = 
{
	ValidationType_t31525A080D050BCFADE54364812432A4C5BDBE63::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (WhitespaceHandling_t49897E1FC16BB3483FAEE35AE0F396573EE124B5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1538[4] = 
{
	WhitespaceHandling_t49897E1FC16BB3483FAEE35AE0F396573EE124B5::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (WriteState_t77FFF7A78C6BEB469AF5904B89980997CDC8735F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1539[8] = 
{
	WriteState_t77FFF7A78C6BEB469AF5904B89980997CDC8735F::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1540[7] = 
{
	XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862::get_offset_of_name_5(),
	XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862::get_offset_of_NDATA_6(),
	XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862::get_offset_of_publicId_7(),
	XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862::get_offset_of_systemId_8(),
	XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862::get_offset_of_baseUri_9(),
	XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862::get_offset_of_lastLinkedChild_10(),
	XmlEntity_tAFE7D3FA37928F62D4E9B2AFC6D6309746E19862::get_offset_of_contentAlreadySet_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (XmlAttribute_t0D7115A390CCB0A788D00DE412E3AB5FE2C7A910), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1541[4] = 
{
	XmlAttribute_t0D7115A390CCB0A788D00DE412E3AB5FE2C7A910::get_offset_of_name_5(),
	XmlAttribute_t0D7115A390CCB0A788D00DE412E3AB5FE2C7A910::get_offset_of_isDefault_6(),
	XmlAttribute_t0D7115A390CCB0A788D00DE412E3AB5FE2C7A910::get_offset_of_lastLinkedChild_7(),
	XmlAttribute_t0D7115A390CCB0A788D00DE412E3AB5FE2C7A910::get_offset_of_schemaInfo_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (XmlAttributeCollection_t0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1542[2] = 
{
	XmlAttributeCollection_t0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9::get_offset_of_ownerElement_4(),
	XmlAttributeCollection_t0C0B97C8BB233E94AD79C64F3FEF07CC7F3487C9::get_offset_of_ownerDocument_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (XmlCDataSection_t9D9B34AE86A0567C22CF9E0A507E59876FC365F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5), -1, sizeof(XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1544[5] = 
{
	XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5_StaticFields::get_offset_of_WhitespaceChars_0(),
	XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5_StaticFields::get_offset_of_firstNamePages_1(),
	XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5_StaticFields::get_offset_of_namePages_2(),
	XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5_StaticFields::get_offset_of_nameBitmap_3(),
	XmlChar_t987897166A4938284071DFD58FC4594BAB1128A5_StaticFields::get_offset_of_U3CU3Ef__switchU24map2F_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (XmlCharacterData_t1733676F46934DE88DE8DEC9CF47190ABAE32F07), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1545[1] = 
{
	XmlCharacterData_t1733676F46934DE88DE8DEC9CF47190ABAE32F07::get_offset_of_data_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (XmlComment_t00021AB0FA23A161ABEC4F774A4084E03FC8186D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (XmlNotation_t38B042D026BD23802CEE72EAB2245B99F9FF54B1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1547[4] = 
{
	XmlNotation_t38B042D026BD23802CEE72EAB2245B99F9FF54B1::get_offset_of_localName_5(),
	XmlNotation_t38B042D026BD23802CEE72EAB2245B99F9FF54B1::get_offset_of_publicId_6(),
	XmlNotation_t38B042D026BD23802CEE72EAB2245B99F9FF54B1::get_offset_of_systemId_7(),
	XmlNotation_t38B042D026BD23802CEE72EAB2245B99F9FF54B1::get_offset_of_prefix_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (XmlDeclaration_t4A6A6879823F50E37FD51260AB9C67C540134C28), -1, sizeof(XmlDeclaration_t4A6A6879823F50E37FD51260AB9C67C540134C28_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1548[4] = 
{
	XmlDeclaration_t4A6A6879823F50E37FD51260AB9C67C540134C28::get_offset_of_encoding_6(),
	XmlDeclaration_t4A6A6879823F50E37FD51260AB9C67C540134C28::get_offset_of_standalone_7(),
	XmlDeclaration_t4A6A6879823F50E37FD51260AB9C67C540134C28::get_offset_of_version_8(),
	XmlDeclaration_t4A6A6879823F50E37FD51260AB9C67C540134C28_StaticFields::get_offset_of_U3CU3Ef__switchU24map30_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763), -1, sizeof(XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1549[20] = 
{
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763_StaticFields::get_offset_of_optimal_create_types_5(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_optimal_create_element_6(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_optimal_create_attribute_7(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_nameTable_8(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_baseURI_9(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_implementation_10(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_preserveWhitespace_11(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_resolver_12(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_idTable_13(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_nameCache_14(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_lastLinkedChild_15(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_schemas_16(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_schemaInfo_17(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_loadMode_18(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_NodeChanged_19(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_NodeChanging_20(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_NodeInserted_21(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_NodeInserting_22(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_NodeRemoved_23(),
	XmlDocument_t4C411CC65B75933374C140FB5737CF2C261CC763::get_offset_of_NodeRemoving_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (XmlDocumentFragment_t51FD3AA32D241AD502CCE212D05763C6E1C7219E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1550[1] = 
{
	XmlDocumentFragment_t51FD3AA32D241AD502CCE212D05763C6E1C7219E::get_offset_of_lastLinkedChild_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (XmlDocumentType_tF87D4D3AB760B0E3D65B139D0CA3605803F37093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1551[3] = 
{
	XmlDocumentType_tF87D4D3AB760B0E3D65B139D0CA3605803F37093::get_offset_of_entities_6(),
	XmlDocumentType_tF87D4D3AB760B0E3D65B139D0CA3605803F37093::get_offset_of_notations_7(),
	XmlDocumentType_tF87D4D3AB760B0E3D65B139D0CA3605803F37093::get_offset_of_dtd_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1552[5] = 
{
	XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9::get_offset_of_attributes_6(),
	XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9::get_offset_of_name_7(),
	XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9::get_offset_of_lastLinkedChild_8(),
	XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9::get_offset_of_isNotEmpty_9(),
	XmlElement_t1E0443656F6ED079E0C45153B25AE0A39E31CFF9::get_offset_of_schemaInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (XmlEntityReference_tA8468650AA9BE800351BA5A6BE748953AA4E23F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1553[2] = 
{
	XmlEntityReference_tA8468650AA9BE800351BA5A6BE748953AA4E23F3::get_offset_of_entityName_6(),
	XmlEntityReference_tA8468650AA9BE800351BA5A6BE748953AA4E23F3::get_offset_of_lastLinkedChild_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1554[5] = 
{
	XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1::get_offset_of_lineNumber_11(),
	XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1::get_offset_of_linePosition_12(),
	XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1::get_offset_of_sourceUri_13(),
	XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1::get_offset_of_res_14(),
	XmlException_tC3F8843762100626FC686CF0862B5545BCB261A1::get_offset_of_messages_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (XmlImplementation_t941F4E3C7A3128487AD76CEEA212EDCC5484E64B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1555[1] = 
{
	XmlImplementation_t941F4E3C7A3128487AD76CEEA212EDCC5484E64B::get_offset_of_InternalNameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84), -1, sizeof(XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1556[8] = 
{
	XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields::get_offset_of_datetimeFormats_0(),
	XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields::get_offset_of_defaultDateTimeFormats_1(),
	XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields::get_offset_of_roundtripDateTimeFormats_2(),
	XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields::get_offset_of_localDateTimeFormats_3(),
	XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields::get_offset_of_utcDateTimeFormats_4(),
	XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields::get_offset_of_unspecifiedDateTimeFormats_5(),
	XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields::get_offset_of__defaultStyle_6(),
	XmlConvert_t3BB1C81D3A39888DBF9262B91B4AA4BF4BF44D84_StaticFields::get_offset_of_U3CU3Ef__switchU24map33_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1557[1] = 
{
	XmlLinkedNode_t8FF499D3DFBCA73D6F60F491BCC7C3D23435DF23::get_offset_of_nextSibling_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1558[5] = 
{
	XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768::get_offset_of_Prefix_0(),
	XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768::get_offset_of_LocalName_1(),
	XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768::get_offset_of_NS_2(),
	XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768::get_offset_of_Hash_3(),
	XmlNameEntry_t52398CEC2377640341683A49A09093C610A65768::get_offset_of_prefixed_name_cache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1559[4] = 
{
	XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B::get_offset_of_table_0(),
	XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B::get_offset_of_nameTable_1(),
	XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B::get_offset_of_dummy_2(),
	XmlNameEntryCache_t69A1B77D4107BB4F1DEEA63F9436F43B53EA205B::get_offset_of_cacheBuffer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (XmlNameTable_tAA4F116720DEE4495552DD594D090390F342894B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246), -1, sizeof(XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1561[4] = 
{
	XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246_StaticFields::get_offset_of_emptyEnumerator_0(),
	XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246::get_offset_of_parent_1(),
	XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246::get_offset_of_nodeList_2(),
	XmlNamedNodeMap_tBFED42F937E9A504358A1DCA71C3447CBE685246::get_offset_of_readOnly_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86), -1, sizeof(XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1562[9] = 
{
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86::get_offset_of_decls_0(),
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86::get_offset_of_declPos_1(),
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86::get_offset_of_scopes_2(),
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86::get_offset_of_scopePos_3(),
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86::get_offset_of_defaultNamespace_4(),
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86::get_offset_of_count_5(),
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86::get_offset_of_nameTable_6(),
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86::get_offset_of_internalAtomizedNames_7(),
	XmlNamespaceManager_t638B0C086B7716A1FE071BF9611C4CE780717F86_StaticFields::get_offset_of_U3CU3Ef__switchU24map25_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { sizeof (NsDecl_tFBD4E75517EBC2F40B9055F7785B4A0BEA27B70C)+ sizeof (RuntimeObject), sizeof(NsDecl_tFBD4E75517EBC2F40B9055F7785B4A0BEA27B70C_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1563[2] = 
{
	NsDecl_tFBD4E75517EBC2F40B9055F7785B4A0BEA27B70C::get_offset_of_Prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NsDecl_tFBD4E75517EBC2F40B9055F7785B4A0BEA27B70C::get_offset_of_Uri_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (NsScope_tAE4D8382C99EA8EC689B83ABCB5A04FE6CDE41DB)+ sizeof (RuntimeObject), sizeof(NsScope_tAE4D8382C99EA8EC689B83ABCB5A04FE6CDE41DB_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1564[2] = 
{
	NsScope_tAE4D8382C99EA8EC689B83ABCB5A04FE6CDE41DB::get_offset_of_DeclCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	NsScope_tAE4D8382C99EA8EC689B83ABCB5A04FE6CDE41DB::get_offset_of_DefaultNamespace_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A), -1, sizeof(XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1565[5] = 
{
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A_StaticFields::get_offset_of_emptyList_0(),
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A::get_offset_of_ownerDocument_1(),
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A::get_offset_of_parentNode_2(),
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A::get_offset_of_childNodes_3(),
	XmlNode_t2B708D2776777AFF8C95DD2EFB651AA64E57632A_StaticFields::get_offset_of_U3CU3Ef__switchU24map2B_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { sizeof (EmptyNodeList_tABF5FD9F0A8DC7FBC7808B3BACC780547EB48735), -1, sizeof(EmptyNodeList_tABF5FD9F0A8DC7FBC7808B3BACC780547EB48735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1566[1] = 
{
	EmptyNodeList_tABF5FD9F0A8DC7FBC7808B3BACC780547EB48735_StaticFields::get_offset_of_emptyEnumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { sizeof (XmlNodeChangedAction_tAFD0821DB232A81EDA0AAB61B337E05F12FB3930)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1567[4] = 
{
	XmlNodeChangedAction_tAFD0821DB232A81EDA0AAB61B337E05F12FB3930::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { sizeof (XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1568[6] = 
{
	XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C::get_offset_of__oldParent_1(),
	XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C::get_offset_of__newParent_2(),
	XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C::get_offset_of__action_3(),
	XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C::get_offset_of__node_4(),
	XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C::get_offset_of__oldValue_5(),
	XmlNodeChangedEventArgs_t11DDA0DEDD40A5B73CBEADAAC7872BB51C764D8C::get_offset_of__newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (XmlNodeList_tD7AD9076C850FBF7CC2520D0B670B65037DB1E3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (XmlNodeListChildren_t0DFA6642EB0E0CA24CB3F5AF8DC38D09D5656B2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1570[1] = 
{
	XmlNodeListChildren_t0DFA6642EB0E0CA24CB3F5AF8DC38D09D5656B2A::get_offset_of_parent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (Enumerator_t9C8166FB5C464A64F87C434D7EB8F8AADC5E8817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1571[3] = 
{
	Enumerator_t9C8166FB5C464A64F87C434D7EB8F8AADC5E8817::get_offset_of_parent_0(),
	Enumerator_t9C8166FB5C464A64F87C434D7EB8F8AADC5E8817::get_offset_of_currentChild_1(),
	Enumerator_t9C8166FB5C464A64F87C434D7EB8F8AADC5E8817::get_offset_of_passedLastNode_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { sizeof (XmlNodeReader_tE00D164708D99621945F9F8A48BBB0BEA5FBA3BE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1572[4] = 
{
	XmlNodeReader_tE00D164708D99621945F9F8A48BBB0BEA5FBA3BE::get_offset_of_entity_2(),
	XmlNodeReader_tE00D164708D99621945F9F8A48BBB0BEA5FBA3BE::get_offset_of_source_3(),
	XmlNodeReader_tE00D164708D99621945F9F8A48BBB0BEA5FBA3BE::get_offset_of_entityInsideAttribute_4(),
	XmlNodeReader_tE00D164708D99621945F9F8A48BBB0BEA5FBA3BE::get_offset_of_insideAttribute_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { sizeof (XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54), -1, sizeof(XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1573[11] = 
{
	XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54::get_offset_of_document_2(),
	XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54::get_offset_of_startNode_3(),
	XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54::get_offset_of_current_4(),
	XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54::get_offset_of_ownerLinkedNode_5(),
	XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54::get_offset_of_state_6(),
	XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54::get_offset_of_depth_7(),
	XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54::get_offset_of_isEndElement_8(),
	XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54::get_offset_of_ignoreStartNode_9(),
	XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54_StaticFields::get_offset_of_U3CU3Ef__switchU24map34_10(),
	XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54_StaticFields::get_offset_of_U3CU3Ef__switchU24map35_11(),
	XmlNodeReaderImpl_t7996B6B5A9618AF6532A1436BFD3E48ADD281F54_StaticFields::get_offset_of_U3CU3Ef__switchU24map36_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { sizeof (XmlNodeType_tF9194E434ED56EA8A4CBE6EB7181252E20BB94FA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1574[19] = 
{
	XmlNodeType_tF9194E434ED56EA8A4CBE6EB7181252E20BB94FA::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1575[13] = 
{
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_baseURI_0(),
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_docTypeName_1(),
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_encoding_2(),
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_internalSubset_3(),
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_namespaceManager_4(),
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_nameTable_5(),
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_publicID_6(),
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_systemID_7(),
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_xmlLang_8(),
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_xmlSpace_9(),
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_contextItems_10(),
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_contextItemCount_11(),
	XmlParserContext_tFCFB3C705A296076573E3517AAD1F1C187DACB14::get_offset_of_dtd_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { sizeof (ContextItem_t826C1332DE94B9C4C391C335BC117442D86CBD25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1576[3] = 
{
	ContextItem_t826C1332DE94B9C4C391C335BC117442D86CBD25::get_offset_of_BaseURI_0(),
	ContextItem_t826C1332DE94B9C4C391C335BC117442D86CBD25::get_offset_of_XmlLang_1(),
	ContextItem_t826C1332DE94B9C4C391C335BC117442D86CBD25::get_offset_of_XmlSpace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (XmlProcessingInstruction_tD4965C66FE3BF9B2AB08C8EE5AD339F1F51FBBEF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1577[2] = 
{
	XmlProcessingInstruction_tD4965C66FE3BF9B2AB08C8EE5AD339F1F51FBBEF::get_offset_of_target_6(),
	XmlProcessingInstruction_tD4965C66FE3BF9B2AB08C8EE5AD339F1F51FBBEF::get_offset_of_data_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004), -1, sizeof(XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1578[4] = 
{
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004_StaticFields::get_offset_of_Empty_0(),
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004::get_offset_of_name_1(),
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004::get_offset_of_ns_2(),
	XmlQualifiedName_t473CA6BDDCC80E24626C6741FC40EFE9EA9E7004::get_offset_of_hash_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1579[2] = 
{
	XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293::get_offset_of_binary_0(),
	XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293::get_offset_of_settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1580[5] = 
{
	XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A::get_offset_of_reader_0(),
	XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A::get_offset_of_base64CacheStartsAt_1(),
	XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A::get_offset_of_state_2(),
	XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A::get_offset_of_hasCache_3(),
	XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A::get_offset_of_dontReset_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (CommandState_t151B413EEEE8EFF4B4C087D57AE3D3F595D0DE72)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1581[6] = 
{
	CommandState_t151B413EEEE8EFF4B4C087D57AE3D3F595D0DE72::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (CharGetter_t41F2B070679160162013D7D2B99DF5A72133136B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1583[5] = 
{
	XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB::get_offset_of_checkCharacters_0(),
	XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB::get_offset_of_conformance_1(),
	XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB::get_offset_of_schemas_2(),
	XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB::get_offset_of_schemasNeedsInitialization_3(),
	XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB::get_offset_of_validationFlags_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (XmlResolver_tF417EFBE696C3A5E7656C0FA477260AFF8AA252C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (XmlSignificantWhitespace_t7E151A744639F09EDE3057CA5B18C4E1F6A50103), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (XmlSpace_t6A54DEF6D051D3E7B2509C3E101CE153C833F7E0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1586[4] = 
{
	XmlSpace_t6A54DEF6D051D3E7B2509C3E101CE153C833F7E0::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (XmlText_t5F596B3ED20FB449A77E07BFA11CCE56E36E9D89), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD), -1, sizeof(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1588[54] = 
{
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_cursorToken_2(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_currentToken_3(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_currentAttributeToken_4(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_currentAttributeValueToken_5(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_attributeTokens_6(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_attributeValueTokens_7(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_currentAttribute_8(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_currentAttributeValue_9(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_attributeCount_10(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_parserContext_11(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_nameTable_12(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_nsmgr_13(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_readState_14(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_disallowReset_15(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_depth_16(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_elementDepth_17(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_depthUp_18(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_popScope_19(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_elementNames_20(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_elementNameStackPos_21(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_allowMultipleRoot_22(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_isStandalone_23(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_returnEntityReference_24(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_entityReferenceName_25(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_valueBuffer_26(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_reader_27(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_peekChars_28(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_peekCharsIndex_29(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_peekCharsLength_30(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_curNodePeekIndex_31(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_preserveCurrentTag_32(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_line_33(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_column_34(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_currentLinkedNodeLineNumber_35(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_currentLinkedNodeLinePosition_36(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_useProceedingLineInfo_37(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_startNodeType_38(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_currentState_39(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_nestLevel_40(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_readCharsInProgress_41(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_binaryCharGetter_42(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_namespaces_43(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_whitespaceHandling_44(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_resolver_45(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_normalization_46(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_checkCharacters_47(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_prohibitDtd_48(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_closeInput_49(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_entityHandling_50(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_whitespacePool_51(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_whitespaceCache_52(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD::get_offset_of_stateStack_53(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD_StaticFields::get_offset_of_U3CU3Ef__switchU24map38_54(),
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD_StaticFields::get_offset_of_U3CU3Ef__switchU24map39_55(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { sizeof (XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1589[13] = 
{
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_valueCache_0(),
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_Reader_1(),
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_Name_2(),
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_LocalName_3(),
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_Prefix_4(),
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_NamespaceURI_5(),
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_IsEmptyElement_6(),
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_QuoteChar_7(),
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_LineNumber_8(),
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_LinePosition_9(),
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_ValueBufferStart_10(),
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_ValueBufferEnd_11(),
	XmlTokenInfo_t2A1A819C64EA244AED28E17A84BA664B876EF3A8::get_offset_of_NodeType_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1590[4] = 
{
	XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2::get_offset_of_ValueTokenStartIndex_13(),
	XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2::get_offset_of_ValueTokenEndIndex_14(),
	XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2::get_offset_of_valueCache_15(),
	XmlAttributeTokenInfo_tFAEEDCB34D0C2C4A330EA399EFF9E166D1C8B7F2::get_offset_of_tmpBuilder_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (TagName_tF4669CF39B6F11E1EB42EC941A67A73BB025D3D4)+ sizeof (RuntimeObject), sizeof(TagName_tF4669CF39B6F11E1EB42EC941A67A73BB025D3D4_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1591[3] = 
{
	TagName_tF4669CF39B6F11E1EB42EC941A67A73BB025D3D4::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagName_tF4669CF39B6F11E1EB42EC941A67A73BB025D3D4::get_offset_of_LocalName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagName_tF4669CF39B6F11E1EB42EC941A67A73BB025D3D4::get_offset_of_Prefix_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (DtdInputState_t5A3E4D094A604D4B3774B3E36AD681F304E565C3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1592[10] = 
{
	DtdInputState_t5A3E4D094A604D4B3774B3E36AD681F304E565C3::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (DtdInputStateStack_tD410944C4A9AE89256F67B1E85F27375A0ABA100), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1593[1] = 
{
	DtdInputStateStack_tD410944C4A9AE89256F67B1E85F27375A0ABA100::get_offset_of_intern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1594[5] = 
{
	XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3::get_offset_of_entity_2(),
	XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3::get_offset_of_source_3(),
	XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3::get_offset_of_entityInsideAttribute_4(),
	XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3::get_offset_of_insideAttribute_5(),
	XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3::get_offset_of_entityNameStack_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (XmlTokenizedType_t8CDA56668C31147E44A370167F11C0F6ACE7E338)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1595[14] = 
{
	XmlTokenizedType_t8CDA56668C31147E44A370167F11C0F6ACE7E338::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (XmlUrlResolver_tD3E7BD8A4D69B96E8EB009EA459545CFE399FA83), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1596[1] = 
{
	XmlUrlResolver_tD3E7BD8A4D69B96E8EB009EA459545CFE399FA83::get_offset_of_credential_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1597[12] = 
{
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6::get_offset_of_entityHandling_2(),
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6::get_offset_of_sourceReader_3(),
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6::get_offset_of_xmlTextReader_4(),
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6::get_offset_of_validatingReader_5(),
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6::get_offset_of_resolver_6(),
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6::get_offset_of_resolverSpecified_7(),
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6::get_offset_of_validationType_8(),
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6::get_offset_of_schemas_9(),
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6::get_offset_of_dtdReader_10(),
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6::get_offset_of_schemaInfo_11(),
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6::get_offset_of_storedCharacters_12(),
	XmlValidatingReader_t524BE1B3546FA8855DD8C66D124AC456FB68D3E6::get_offset_of_ValidationEventHandler_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (XmlWhitespace_t94DE163BDCCE5B034799544EE082260E57B4D53C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (XmlWriter_tF67065A3B83E8B7E2B24D172D8223F4328F437B6), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif

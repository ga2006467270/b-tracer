﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Beacon
struct Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD;
// BeaconType[]
struct BeaconTypeU5BU5D_t706B12A264072AC3C4259A7634D0E6FBD22C6D24;
// Beacon[]
struct BeaconU5BU5D_tF783DEEF66290B38A526E0376E980913DD58F286;
// BluetoothState/BluetoothStateChanged
struct BluetoothStateChanged_t795820F12512E5B80A7737D6836A41A7E0C59DEF;
// Example
struct Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF;
// Images
struct Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC;
// MultiKeyDictionary`3<System.String,System.String,Reporter/Log>
struct MultiKeyDictionary_3_t999B39286A71D85EB1F36E92538A09BA09791E27;
// Reporter
struct Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E;
// Reporter/Log
struct Log_t152F87153D078D3D99151CF14D91D518B21E3E22;
// SimpleJSON.JSONArray
struct JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8;
// SimpleJSON.JSONClass
struct JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F;
// SimpleJSON.JSONNode
struct JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889;
// System.AsyncCallback
struct AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2;
// System.Byte[]
struct ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode>
struct Dictionary_2_t685AE2501706A88ADC44D7B5DC9375A46511F357;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_tFDA27909C2695A2BBE12DBC4C8BCE6C3D8B82D61;
// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode>
struct IEnumerator_1_tDEFCE4834CBD426B3AA60F2B33FFBDE84259E830;
// System.Collections.Generic.List`1<Beacon>
struct List_1_t39992A124A89322ED608A124E78EB5AC0CAA8B7A;
// System.Collections.Generic.List`1<Reporter/Log>
struct List_1_t571010E716A1DF35D4D694A0FB92FCF347968814;
// System.Collections.Generic.List`1<Reporter/Sample>
struct List_1_t6B087B34678055970468DA4EC69ABFD3C66EC9CC;
// System.Collections.Generic.List`1<SimpleJSON.JSONNode>
struct List_1_t2260BF21306EA470AFD085642EAE0589F258F271;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.IDictionary
struct IDictionary_tD35B9437F08BE98D1E0B295CC73C48E168CAB316;
// System.DelegateData
struct DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9;
// System.Func`2<iBeaconRegion,System.Boolean>
struct Func_2_t2F03C7144AB252BC791273C6A5E26B7598EE87FD;
// System.IAsyncResult
struct IAsyncResult_tDA33C24465239FB383C4C2CDAAC43B9AD3CB7F05;
// System.Int32[]
struct Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074;
// System.IntPtr[]
struct IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B;
// System.Text.RegularExpressions.Regex
struct Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B;
// System.Threading.Thread
struct Thread_t83C301EC970792455F76D89E58140949B003EA50;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;
// Telemetry
struct Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E;
// UnityEngine.GUIContent
struct GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0;
// UnityEngine.GUISkin
struct GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7;
// UnityEngine.GUIStyle
struct GUIStyle_t671F175A201A19166385EE3392292A5F50070572;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.InputField
struct InputField_t533609195B110760BCFF00B746C87D81969CB005;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// iBeaconReceiver/BeaconRangeChanged
struct BeaconRangeChanged_tB250A609F6139101DE7538850A8524B29198C478;
// iBeaconRegion
struct iBeaconRegion_t5C666E55D6EB9CB88EC29B58B305A5C217BBC607;
// iBeaconRegion[]
struct iBeaconRegionU5BU5D_t9F3E57765B411A6495722FFA1DF5F15E1109A42D;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ADDRESOURCES_T3D4A1E7E86AC9B7C12E274F57E970A6A867929AD_H
#define ADDRESOURCES_T3D4A1E7E86AC9B7C12E274F57E970A6A867929AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AddResources
struct  AddResources_t3D4A1E7E86AC9B7C12E274F57E970A6A867929AD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDRESOURCES_T3D4A1E7E86AC9B7C12E274F57E970A6A867929AD_H
#ifndef BDSLITESPEECH_T0080FF8C29E1F3373FB64E23EC48B7B6A3F78B24_H
#define BDSLITESPEECH_T0080FF8C29E1F3373FB64E23EC48B7B6A3F78B24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BDSLiteSpeech
struct  BDSLiteSpeech_t0080FF8C29E1F3373FB64E23EC48B7B6A3F78B24  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BDSLITESPEECH_T0080FF8C29E1F3373FB64E23EC48B7B6A3F78B24_H
#ifndef IMAGES_T8B806A90CFA740B9FE6317F1F5ED416760C1B8DC_H
#define IMAGES_T8B806A90CFA740B9FE6317F1F5ED416760C1B8DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Images
struct  Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D Images::clearImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___clearImage_0;
	// UnityEngine.Texture2D Images::collapseImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___collapseImage_1;
	// UnityEngine.Texture2D Images::clearOnNewSceneImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___clearOnNewSceneImage_2;
	// UnityEngine.Texture2D Images::showTimeImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___showTimeImage_3;
	// UnityEngine.Texture2D Images::showSceneImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___showSceneImage_4;
	// UnityEngine.Texture2D Images::userImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___userImage_5;
	// UnityEngine.Texture2D Images::showMemoryImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___showMemoryImage_6;
	// UnityEngine.Texture2D Images::softwareImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___softwareImage_7;
	// UnityEngine.Texture2D Images::dateImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___dateImage_8;
	// UnityEngine.Texture2D Images::showFpsImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___showFpsImage_9;
	// UnityEngine.Texture2D Images::infoImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___infoImage_10;
	// UnityEngine.Texture2D Images::saveLogsImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___saveLogsImage_11;
	// UnityEngine.Texture2D Images::searchImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___searchImage_12;
	// UnityEngine.Texture2D Images::copyImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___copyImage_13;
	// UnityEngine.Texture2D Images::closeImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___closeImage_14;
	// UnityEngine.Texture2D Images::buildFromImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___buildFromImage_15;
	// UnityEngine.Texture2D Images::systemInfoImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___systemInfoImage_16;
	// UnityEngine.Texture2D Images::graphicsInfoImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___graphicsInfoImage_17;
	// UnityEngine.Texture2D Images::backImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___backImage_18;
	// UnityEngine.Texture2D Images::logImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___logImage_19;
	// UnityEngine.Texture2D Images::warningImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___warningImage_20;
	// UnityEngine.Texture2D Images::errorImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___errorImage_21;
	// UnityEngine.Texture2D Images::barImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___barImage_22;
	// UnityEngine.Texture2D Images::button_activeImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___button_activeImage_23;
	// UnityEngine.Texture2D Images::even_logImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___even_logImage_24;
	// UnityEngine.Texture2D Images::odd_logImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___odd_logImage_25;
	// UnityEngine.Texture2D Images::selectedImage
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___selectedImage_26;
	// UnityEngine.GUISkin Images::reporterScrollerSkin
	GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * ___reporterScrollerSkin_27;

public:
	inline static int32_t get_offset_of_clearImage_0() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___clearImage_0)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_clearImage_0() const { return ___clearImage_0; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_clearImage_0() { return &___clearImage_0; }
	inline void set_clearImage_0(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___clearImage_0 = value;
		Il2CppCodeGenWriteBarrier((&___clearImage_0), value);
	}

	inline static int32_t get_offset_of_collapseImage_1() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___collapseImage_1)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_collapseImage_1() const { return ___collapseImage_1; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_collapseImage_1() { return &___collapseImage_1; }
	inline void set_collapseImage_1(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___collapseImage_1 = value;
		Il2CppCodeGenWriteBarrier((&___collapseImage_1), value);
	}

	inline static int32_t get_offset_of_clearOnNewSceneImage_2() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___clearOnNewSceneImage_2)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_clearOnNewSceneImage_2() const { return ___clearOnNewSceneImage_2; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_clearOnNewSceneImage_2() { return &___clearOnNewSceneImage_2; }
	inline void set_clearOnNewSceneImage_2(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___clearOnNewSceneImage_2 = value;
		Il2CppCodeGenWriteBarrier((&___clearOnNewSceneImage_2), value);
	}

	inline static int32_t get_offset_of_showTimeImage_3() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___showTimeImage_3)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_showTimeImage_3() const { return ___showTimeImage_3; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_showTimeImage_3() { return &___showTimeImage_3; }
	inline void set_showTimeImage_3(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___showTimeImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___showTimeImage_3), value);
	}

	inline static int32_t get_offset_of_showSceneImage_4() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___showSceneImage_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_showSceneImage_4() const { return ___showSceneImage_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_showSceneImage_4() { return &___showSceneImage_4; }
	inline void set_showSceneImage_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___showSceneImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___showSceneImage_4), value);
	}

	inline static int32_t get_offset_of_userImage_5() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___userImage_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_userImage_5() const { return ___userImage_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_userImage_5() { return &___userImage_5; }
	inline void set_userImage_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___userImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___userImage_5), value);
	}

	inline static int32_t get_offset_of_showMemoryImage_6() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___showMemoryImage_6)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_showMemoryImage_6() const { return ___showMemoryImage_6; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_showMemoryImage_6() { return &___showMemoryImage_6; }
	inline void set_showMemoryImage_6(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___showMemoryImage_6 = value;
		Il2CppCodeGenWriteBarrier((&___showMemoryImage_6), value);
	}

	inline static int32_t get_offset_of_softwareImage_7() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___softwareImage_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_softwareImage_7() const { return ___softwareImage_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_softwareImage_7() { return &___softwareImage_7; }
	inline void set_softwareImage_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___softwareImage_7 = value;
		Il2CppCodeGenWriteBarrier((&___softwareImage_7), value);
	}

	inline static int32_t get_offset_of_dateImage_8() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___dateImage_8)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_dateImage_8() const { return ___dateImage_8; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_dateImage_8() { return &___dateImage_8; }
	inline void set_dateImage_8(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___dateImage_8 = value;
		Il2CppCodeGenWriteBarrier((&___dateImage_8), value);
	}

	inline static int32_t get_offset_of_showFpsImage_9() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___showFpsImage_9)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_showFpsImage_9() const { return ___showFpsImage_9; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_showFpsImage_9() { return &___showFpsImage_9; }
	inline void set_showFpsImage_9(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___showFpsImage_9 = value;
		Il2CppCodeGenWriteBarrier((&___showFpsImage_9), value);
	}

	inline static int32_t get_offset_of_infoImage_10() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___infoImage_10)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_infoImage_10() const { return ___infoImage_10; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_infoImage_10() { return &___infoImage_10; }
	inline void set_infoImage_10(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___infoImage_10 = value;
		Il2CppCodeGenWriteBarrier((&___infoImage_10), value);
	}

	inline static int32_t get_offset_of_saveLogsImage_11() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___saveLogsImage_11)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_saveLogsImage_11() const { return ___saveLogsImage_11; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_saveLogsImage_11() { return &___saveLogsImage_11; }
	inline void set_saveLogsImage_11(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___saveLogsImage_11 = value;
		Il2CppCodeGenWriteBarrier((&___saveLogsImage_11), value);
	}

	inline static int32_t get_offset_of_searchImage_12() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___searchImage_12)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_searchImage_12() const { return ___searchImage_12; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_searchImage_12() { return &___searchImage_12; }
	inline void set_searchImage_12(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___searchImage_12 = value;
		Il2CppCodeGenWriteBarrier((&___searchImage_12), value);
	}

	inline static int32_t get_offset_of_copyImage_13() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___copyImage_13)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_copyImage_13() const { return ___copyImage_13; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_copyImage_13() { return &___copyImage_13; }
	inline void set_copyImage_13(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___copyImage_13 = value;
		Il2CppCodeGenWriteBarrier((&___copyImage_13), value);
	}

	inline static int32_t get_offset_of_closeImage_14() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___closeImage_14)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_closeImage_14() const { return ___closeImage_14; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_closeImage_14() { return &___closeImage_14; }
	inline void set_closeImage_14(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___closeImage_14 = value;
		Il2CppCodeGenWriteBarrier((&___closeImage_14), value);
	}

	inline static int32_t get_offset_of_buildFromImage_15() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___buildFromImage_15)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_buildFromImage_15() const { return ___buildFromImage_15; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_buildFromImage_15() { return &___buildFromImage_15; }
	inline void set_buildFromImage_15(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___buildFromImage_15 = value;
		Il2CppCodeGenWriteBarrier((&___buildFromImage_15), value);
	}

	inline static int32_t get_offset_of_systemInfoImage_16() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___systemInfoImage_16)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_systemInfoImage_16() const { return ___systemInfoImage_16; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_systemInfoImage_16() { return &___systemInfoImage_16; }
	inline void set_systemInfoImage_16(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___systemInfoImage_16 = value;
		Il2CppCodeGenWriteBarrier((&___systemInfoImage_16), value);
	}

	inline static int32_t get_offset_of_graphicsInfoImage_17() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___graphicsInfoImage_17)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_graphicsInfoImage_17() const { return ___graphicsInfoImage_17; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_graphicsInfoImage_17() { return &___graphicsInfoImage_17; }
	inline void set_graphicsInfoImage_17(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___graphicsInfoImage_17 = value;
		Il2CppCodeGenWriteBarrier((&___graphicsInfoImage_17), value);
	}

	inline static int32_t get_offset_of_backImage_18() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___backImage_18)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_backImage_18() const { return ___backImage_18; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_backImage_18() { return &___backImage_18; }
	inline void set_backImage_18(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___backImage_18 = value;
		Il2CppCodeGenWriteBarrier((&___backImage_18), value);
	}

	inline static int32_t get_offset_of_logImage_19() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___logImage_19)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_logImage_19() const { return ___logImage_19; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_logImage_19() { return &___logImage_19; }
	inline void set_logImage_19(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___logImage_19 = value;
		Il2CppCodeGenWriteBarrier((&___logImage_19), value);
	}

	inline static int32_t get_offset_of_warningImage_20() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___warningImage_20)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_warningImage_20() const { return ___warningImage_20; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_warningImage_20() { return &___warningImage_20; }
	inline void set_warningImage_20(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___warningImage_20 = value;
		Il2CppCodeGenWriteBarrier((&___warningImage_20), value);
	}

	inline static int32_t get_offset_of_errorImage_21() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___errorImage_21)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_errorImage_21() const { return ___errorImage_21; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_errorImage_21() { return &___errorImage_21; }
	inline void set_errorImage_21(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___errorImage_21 = value;
		Il2CppCodeGenWriteBarrier((&___errorImage_21), value);
	}

	inline static int32_t get_offset_of_barImage_22() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___barImage_22)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_barImage_22() const { return ___barImage_22; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_barImage_22() { return &___barImage_22; }
	inline void set_barImage_22(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___barImage_22 = value;
		Il2CppCodeGenWriteBarrier((&___barImage_22), value);
	}

	inline static int32_t get_offset_of_button_activeImage_23() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___button_activeImage_23)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_button_activeImage_23() const { return ___button_activeImage_23; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_button_activeImage_23() { return &___button_activeImage_23; }
	inline void set_button_activeImage_23(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___button_activeImage_23 = value;
		Il2CppCodeGenWriteBarrier((&___button_activeImage_23), value);
	}

	inline static int32_t get_offset_of_even_logImage_24() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___even_logImage_24)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_even_logImage_24() const { return ___even_logImage_24; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_even_logImage_24() { return &___even_logImage_24; }
	inline void set_even_logImage_24(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___even_logImage_24 = value;
		Il2CppCodeGenWriteBarrier((&___even_logImage_24), value);
	}

	inline static int32_t get_offset_of_odd_logImage_25() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___odd_logImage_25)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_odd_logImage_25() const { return ___odd_logImage_25; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_odd_logImage_25() { return &___odd_logImage_25; }
	inline void set_odd_logImage_25(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___odd_logImage_25 = value;
		Il2CppCodeGenWriteBarrier((&___odd_logImage_25), value);
	}

	inline static int32_t get_offset_of_selectedImage_26() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___selectedImage_26)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_selectedImage_26() const { return ___selectedImage_26; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_selectedImage_26() { return &___selectedImage_26; }
	inline void set_selectedImage_26(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___selectedImage_26 = value;
		Il2CppCodeGenWriteBarrier((&___selectedImage_26), value);
	}

	inline static int32_t get_offset_of_reporterScrollerSkin_27() { return static_cast<int32_t>(offsetof(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC, ___reporterScrollerSkin_27)); }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * get_reporterScrollerSkin_27() const { return ___reporterScrollerSkin_27; }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 ** get_address_of_reporterScrollerSkin_27() { return &___reporterScrollerSkin_27; }
	inline void set_reporterScrollerSkin_27(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * value)
	{
		___reporterScrollerSkin_27 = value;
		Il2CppCodeGenWriteBarrier((&___reporterScrollerSkin_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGES_T8B806A90CFA740B9FE6317F1F5ED416760C1B8DC_H
#ifndef U3CREADINFOU3EC__ITERATOR0_TA0E58ECD00099B1743B472C294C557F46C462A60_H
#define U3CREADINFOU3EC__ITERATOR0_TA0E58ECD00099B1743B472C294C557F46C462A60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reporter_<readInfo>c__Iterator0
struct  U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60  : public RuntimeObject
{
public:
	// System.String Reporter_<readInfo>c__Iterator0::<prefFile>__0
	String_t* ___U3CprefFileU3E__0_0;
	// System.String Reporter_<readInfo>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_1;
	// UnityEngine.Networking.UnityWebRequest Reporter_<readInfo>c__Iterator0::<www>__0
	UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * ___U3CwwwU3E__0_2;
	// Reporter Reporter_<readInfo>c__Iterator0::U24this
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * ___U24this_3;
	// System.Object Reporter_<readInfo>c__Iterator0::U24current
	RuntimeObject * ___U24current_4;
	// System.Boolean Reporter_<readInfo>c__Iterator0::U24disposing
	bool ___U24disposing_5;
	// System.Int32 Reporter_<readInfo>c__Iterator0::U24PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CprefFileU3E__0_0() { return static_cast<int32_t>(offsetof(U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60, ___U3CprefFileU3E__0_0)); }
	inline String_t* get_U3CprefFileU3E__0_0() const { return ___U3CprefFileU3E__0_0; }
	inline String_t** get_address_of_U3CprefFileU3E__0_0() { return &___U3CprefFileU3E__0_0; }
	inline void set_U3CprefFileU3E__0_0(String_t* value)
	{
		___U3CprefFileU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CprefFileU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__0_1() { return static_cast<int32_t>(offsetof(U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60, ___U3CurlU3E__0_1)); }
	inline String_t* get_U3CurlU3E__0_1() const { return ___U3CurlU3E__0_1; }
	inline String_t** get_address_of_U3CurlU3E__0_1() { return &___U3CurlU3E__0_1; }
	inline void set_U3CurlU3E__0_1(String_t* value)
	{
		___U3CurlU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_2() { return static_cast<int32_t>(offsetof(U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60, ___U3CwwwU3E__0_2)); }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * get_U3CwwwU3E__0_2() const { return ___U3CwwwU3E__0_2; }
	inline UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 ** get_address_of_U3CwwwU3E__0_2() { return &___U3CwwwU3E__0_2; }
	inline void set_U3CwwwU3E__0_2(UnityWebRequest_t9120F5A2C7D43B936B49C0B7E4CA54C822689129 * value)
	{
		___U3CwwwU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60, ___U24this_3)); }
	inline Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * get_U24this_3() const { return ___U24this_3; }
	inline Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREADINFOU3EC__ITERATOR0_TA0E58ECD00099B1743B472C294C557F46C462A60_H
#ifndef SAMPLE_T88863152299A8F7A4FA9724DA3F943F4E0149689_H
#define SAMPLE_T88863152299A8F7A4FA9724DA3F943F4E0149689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reporter_Sample
struct  Sample_t88863152299A8F7A4FA9724DA3F943F4E0149689  : public RuntimeObject
{
public:
	// System.Single Reporter_Sample::time
	float ___time_0;
	// System.Byte Reporter_Sample::loadedScene
	uint8_t ___loadedScene_1;
	// System.Single Reporter_Sample::memory
	float ___memory_2;
	// System.Single Reporter_Sample::fps
	float ___fps_3;
	// System.String Reporter_Sample::fpsText
	String_t* ___fpsText_4;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(Sample_t88863152299A8F7A4FA9724DA3F943F4E0149689, ___time_0)); }
	inline float get_time_0() const { return ___time_0; }
	inline float* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(float value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_loadedScene_1() { return static_cast<int32_t>(offsetof(Sample_t88863152299A8F7A4FA9724DA3F943F4E0149689, ___loadedScene_1)); }
	inline uint8_t get_loadedScene_1() const { return ___loadedScene_1; }
	inline uint8_t* get_address_of_loadedScene_1() { return &___loadedScene_1; }
	inline void set_loadedScene_1(uint8_t value)
	{
		___loadedScene_1 = value;
	}

	inline static int32_t get_offset_of_memory_2() { return static_cast<int32_t>(offsetof(Sample_t88863152299A8F7A4FA9724DA3F943F4E0149689, ___memory_2)); }
	inline float get_memory_2() const { return ___memory_2; }
	inline float* get_address_of_memory_2() { return &___memory_2; }
	inline void set_memory_2(float value)
	{
		___memory_2 = value;
	}

	inline static int32_t get_offset_of_fps_3() { return static_cast<int32_t>(offsetof(Sample_t88863152299A8F7A4FA9724DA3F943F4E0149689, ___fps_3)); }
	inline float get_fps_3() const { return ___fps_3; }
	inline float* get_address_of_fps_3() { return &___fps_3; }
	inline void set_fps_3(float value)
	{
		___fps_3 = value;
	}

	inline static int32_t get_offset_of_fpsText_4() { return static_cast<int32_t>(offsetof(Sample_t88863152299A8F7A4FA9724DA3F943F4E0149689, ___fpsText_4)); }
	inline String_t* get_fpsText_4() const { return ___fpsText_4; }
	inline String_t** get_address_of_fpsText_4() { return &___fpsText_4; }
	inline void set_fpsText_4(String_t* value)
	{
		___fpsText_4 = value;
		Il2CppCodeGenWriteBarrier((&___fpsText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLE_T88863152299A8F7A4FA9724DA3F943F4E0149689_H
#ifndef RESOURCELOADBTRACE_TA3D930FE12A6F09A0F18935022C70B15F6CE588E_H
#define RESOURCELOADBTRACE_TA3D930FE12A6F09A0F18935022C70B15F6CE588E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResourceLoadBTrace
struct  ResourceLoadBTrace_tA3D930FE12A6F09A0F18935022C70B15F6CE588E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCELOADBTRACE_TA3D930FE12A6F09A0F18935022C70B15F6CE588E_H
#ifndef JSON_T23727CDEA55ED3C10DCEE3222EAC8D0EB7973C05_H
#define JSON_T23727CDEA55ED3C10DCEE3222EAC8D0EB7973C05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSON
struct  JSON_t23727CDEA55ED3C10DCEE3222EAC8D0EB7973C05  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T23727CDEA55ED3C10DCEE3222EAC8D0EB7973C05_H
#ifndef U3CREMOVEU3EC__ANONSTOREY2_T132776AAE263F0FCA30E3DAC8ACA78E95F92299E_H
#define U3CREMOVEU3EC__ANONSTOREY2_T132776AAE263F0FCA30E3DAC8ACA78E95F92299E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONClass_<Remove>c__AnonStorey2
struct  U3CRemoveU3Ec__AnonStorey2_t132776AAE263F0FCA30E3DAC8ACA78E95F92299E  : public RuntimeObject
{
public:
	// SimpleJSON.JSONNode SimpleJSON.JSONClass_<Remove>c__AnonStorey2::aNode
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___aNode_0;

public:
	inline static int32_t get_offset_of_aNode_0() { return static_cast<int32_t>(offsetof(U3CRemoveU3Ec__AnonStorey2_t132776AAE263F0FCA30E3DAC8ACA78E95F92299E, ___aNode_0)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_aNode_0() const { return ___aNode_0; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_aNode_0() { return &___aNode_0; }
	inline void set_aNode_0(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___aNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___aNode_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEU3EC__ANONSTOREY2_T132776AAE263F0FCA30E3DAC8ACA78E95F92299E_H
#ifndef JSONNODE_T0AA84017F5559ABABE7CB27656B1352F565B5889_H
#define JSONNODE_T0AA84017F5559ABABE7CB27656B1352F565B5889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNode
struct  JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONNODE_T0AA84017F5559ABABE7CB27656B1352F565B5889_H
#ifndef U3CU3EC__ITERATOR0_T7C332823620D33941DB25086B399A1280F6E3556_H
#define U3CU3EC__ITERATOR0_T7C332823620D33941DB25086B399A1280F6E3556_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNode_<>c__Iterator0
struct  U3CU3Ec__Iterator0_t7C332823620D33941DB25086B399A1280F6E3556  : public RuntimeObject
{
public:
	// SimpleJSON.JSONNode SimpleJSON.JSONNode_<>c__Iterator0::U24current
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U24current_0;
	// System.Boolean SimpleJSON.JSONNode_<>c__Iterator0::U24disposing
	bool ___U24disposing_1;
	// System.Int32 SimpleJSON.JSONNode_<>c__Iterator0::U24PC
	int32_t ___U24PC_2;

public:
	inline static int32_t get_offset_of_U24current_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t7C332823620D33941DB25086B399A1280F6E3556, ___U24current_0)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U24current_0() const { return ___U24current_0; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U24current_0() { return &___U24current_0; }
	inline void set_U24current_0(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U24current_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_0), value);
	}

	inline static int32_t get_offset_of_U24disposing_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t7C332823620D33941DB25086B399A1280F6E3556, ___U24disposing_1)); }
	inline bool get_U24disposing_1() const { return ___U24disposing_1; }
	inline bool* get_address_of_U24disposing_1() { return &___U24disposing_1; }
	inline void set_U24disposing_1(bool value)
	{
		___U24disposing_1 = value;
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t7C332823620D33941DB25086B399A1280F6E3556, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T7C332823620D33941DB25086B399A1280F6E3556_H
#ifndef U3CU3EC__ITERATOR1_TBCD33A6138D22501E073C18D3014899A28F00BC7_H
#define U3CU3EC__ITERATOR1_TBCD33A6138D22501E073C18D3014899A28F00BC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONNode_<>c__Iterator1
struct  U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode_<>c__Iterator1::U24locvar0
	RuntimeObject* ___U24locvar0_0;
	// SimpleJSON.JSONNode SimpleJSON.JSONNode_<>c__Iterator1::<C>__1
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U3CCU3E__1_1;
	// System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode_<>c__Iterator1::U24locvar1
	RuntimeObject* ___U24locvar1_2;
	// SimpleJSON.JSONNode SimpleJSON.JSONNode_<>c__Iterator1::<D>__2
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U3CDU3E__2_3;
	// SimpleJSON.JSONNode SimpleJSON.JSONNode_<>c__Iterator1::U24this
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U24this_4;
	// SimpleJSON.JSONNode SimpleJSON.JSONNode_<>c__Iterator1::U24current
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U24current_5;
	// System.Boolean SimpleJSON.JSONNode_<>c__Iterator1::U24disposing
	bool ___U24disposing_6;
	// System.Int32 SimpleJSON.JSONNode_<>c__Iterator1::U24PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7, ___U24locvar0_0)); }
	inline RuntimeObject* get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline RuntimeObject** get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(RuntimeObject* value)
	{
		___U24locvar0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_0), value);
	}

	inline static int32_t get_offset_of_U3CCU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7, ___U3CCU3E__1_1)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U3CCU3E__1_1() const { return ___U3CCU3E__1_1; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U3CCU3E__1_1() { return &___U3CCU3E__1_1; }
	inline void set_U3CCU3E__1_1(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U3CCU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7, ___U24locvar1_2)); }
	inline RuntimeObject* get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline RuntimeObject** get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(RuntimeObject* value)
	{
		___U24locvar1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar1_2), value);
	}

	inline static int32_t get_offset_of_U3CDU3E__2_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7, ___U3CDU3E__2_3)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U3CDU3E__2_3() const { return ___U3CDU3E__2_3; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U3CDU3E__2_3() { return &___U3CDU3E__2_3; }
	inline void set_U3CDU3E__2_3(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U3CDU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDU3E__2_3), value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7, ___U24this_4)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U24this_4() const { return ___U24this_4; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7, ___U24current_5)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U24current_5() const { return ___U24current_5; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR1_TBCD33A6138D22501E073C18D3014899A28F00BC7_H
#ifndef SIMPLERESOURCEFACTORY_TA31CC8DA5DB8C7190B9EE7E66AA4D570C1C71D14_H
#define SIMPLERESOURCEFACTORY_TA31CC8DA5DB8C7190B9EE7E66AA4D570C1C71D14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleResourceFactory
struct  SimpleResourceFactory_tA31CC8DA5DB8C7190B9EE7E66AA4D570C1C71D14  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLERESOURCEFACTORY_TA31CC8DA5DB8C7190B9EE7E66AA4D570C1C71D14_H
#ifndef ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#define ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_tB866BA24C91559CF299618E230043451CC7CF659* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef U3CTELEMETRYU3EC__ANONSTOREY0_T62793050C92FECD47DFA6FA1F97F9BBE653517FD_H
#define U3CTELEMETRYU3EC__ANONSTOREY0_T62793050C92FECD47DFA6FA1F97F9BBE653517FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Telemetry_<Telemetry>c__AnonStorey0
struct  U3CTelemetryU3Ec__AnonStorey0_t62793050C92FECD47DFA6FA1F97F9BBE653517FD  : public RuntimeObject
{
public:
	// System.String Telemetry_<Telemetry>c__AnonStorey0::hex
	String_t* ___hex_0;

public:
	inline static int32_t get_offset_of_hex_0() { return static_cast<int32_t>(offsetof(U3CTelemetryU3Ec__AnonStorey0_t62793050C92FECD47DFA6FA1F97F9BBE653517FD, ___hex_0)); }
	inline String_t* get_hex_0() const { return ___hex_0; }
	inline String_t** get_address_of_hex_0() { return &___hex_0; }
	inline void set_hex_0(String_t* value)
	{
		___hex_0 = value;
		Il2CppCodeGenWriteBarrier((&___hex_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTELEMETRYU3EC__ANONSTOREY0_T62793050C92FECD47DFA6FA1F97F9BBE653517FD_H
#ifndef IBEACONREGION_T5C666E55D6EB9CB88EC29B58B305A5C217BBC607_H
#define IBEACONREGION_T5C666E55D6EB9CB88EC29B58B305A5C217BBC607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iBeaconRegion
struct  iBeaconRegion_t5C666E55D6EB9CB88EC29B58B305A5C217BBC607  : public RuntimeObject
{
public:
	// System.String iBeaconRegion::_regionName
	String_t* ____regionName_0;
	// Beacon iBeaconRegion::_beacon
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD * ____beacon_1;

public:
	inline static int32_t get_offset_of__regionName_0() { return static_cast<int32_t>(offsetof(iBeaconRegion_t5C666E55D6EB9CB88EC29B58B305A5C217BBC607, ____regionName_0)); }
	inline String_t* get__regionName_0() const { return ____regionName_0; }
	inline String_t** get_address_of__regionName_0() { return &____regionName_0; }
	inline void set__regionName_0(String_t* value)
	{
		____regionName_0 = value;
		Il2CppCodeGenWriteBarrier((&____regionName_0), value);
	}

	inline static int32_t get_offset_of__beacon_1() { return static_cast<int32_t>(offsetof(iBeaconRegion_t5C666E55D6EB9CB88EC29B58B305A5C217BBC607, ____beacon_1)); }
	inline Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD * get__beacon_1() const { return ____beacon_1; }
	inline Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD ** get_address_of__beacon_1() { return &____beacon_1; }
	inline void set__beacon_1(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD * value)
	{
		____beacon_1 = value;
		Il2CppCodeGenWriteBarrier((&____beacon_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IBEACONREGION_T5C666E55D6EB9CB88EC29B58B305A5C217BBC607_H
#ifndef JSONARRAY_TA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8_H
#define JSONARRAY_TA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONArray
struct  JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8  : public JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889
{
public:
	// System.Collections.Generic.List`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::m_List
	List_1_t2260BF21306EA470AFD085642EAE0589F258F271 * ___m_List_0;

public:
	inline static int32_t get_offset_of_m_List_0() { return static_cast<int32_t>(offsetof(JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8, ___m_List_0)); }
	inline List_1_t2260BF21306EA470AFD085642EAE0589F258F271 * get_m_List_0() const { return ___m_List_0; }
	inline List_1_t2260BF21306EA470AFD085642EAE0589F258F271 ** get_address_of_m_List_0() { return &___m_List_0; }
	inline void set_m_List_0(List_1_t2260BF21306EA470AFD085642EAE0589F258F271 * value)
	{
		___m_List_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_List_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAY_TA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8_H
#ifndef JSONCLASS_T8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F_H
#define JSONCLASS_T8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONClass
struct  JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F  : public JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONClass::m_Dict
	Dictionary_2_t685AE2501706A88ADC44D7B5DC9375A46511F357 * ___m_Dict_0;

public:
	inline static int32_t get_offset_of_m_Dict_0() { return static_cast<int32_t>(offsetof(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F, ___m_Dict_0)); }
	inline Dictionary_2_t685AE2501706A88ADC44D7B5DC9375A46511F357 * get_m_Dict_0() const { return ___m_Dict_0; }
	inline Dictionary_2_t685AE2501706A88ADC44D7B5DC9375A46511F357 ** get_address_of_m_Dict_0() { return &___m_Dict_0; }
	inline void set_m_Dict_0(Dictionary_2_t685AE2501706A88ADC44D7B5DC9375A46511F357 * value)
	{
		___m_Dict_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dict_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCLASS_T8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F_H
#ifndef JSONDATA_T82E8EC24D2016E5CB36B4DE156906D9B57EEF020_H
#define JSONDATA_T82E8EC24D2016E5CB36B4DE156906D9B57EEF020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONData
struct  JSONData_t82E8EC24D2016E5CB36B4DE156906D9B57EEF020  : public JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889
{
public:
	// System.String SimpleJSON.JSONData::m_Data
	String_t* ___m_Data_0;

public:
	inline static int32_t get_offset_of_m_Data_0() { return static_cast<int32_t>(offsetof(JSONData_t82E8EC24D2016E5CB36B4DE156906D9B57EEF020, ___m_Data_0)); }
	inline String_t* get_m_Data_0() const { return ___m_Data_0; }
	inline String_t** get_address_of_m_Data_0() { return &___m_Data_0; }
	inline void set_m_Data_0(String_t* value)
	{
		___m_Data_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Data_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDATA_T82E8EC24D2016E5CB36B4DE156906D9B57EEF020_H
#ifndef JSONLAZYCREATOR_TC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C_H
#define JSONLAZYCREATOR_TC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONLazyCreator
struct  JSONLazyCreator_tC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C  : public JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889
{
public:
	// SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::m_Node
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___m_Node_0;
	// System.String SimpleJSON.JSONLazyCreator::m_Key
	String_t* ___m_Key_1;

public:
	inline static int32_t get_offset_of_m_Node_0() { return static_cast<int32_t>(offsetof(JSONLazyCreator_tC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C, ___m_Node_0)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_m_Node_0() const { return ___m_Node_0; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_m_Node_0() { return &___m_Node_0; }
	inline void set_m_Node_0(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___m_Node_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Node_0), value);
	}

	inline static int32_t get_offset_of_m_Key_1() { return static_cast<int32_t>(offsetof(JSONLazyCreator_tC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C, ___m_Key_1)); }
	inline String_t* get_m_Key_1() const { return ___m_Key_1; }
	inline String_t** get_address_of_m_Key_1() { return &___m_Key_1; }
	inline void set_m_Key_1(String_t* value)
	{
		___m_Key_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Key_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLAZYCREATOR_TC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C_H
#ifndef KEYVALUEPAIR_2_T7706074A794F097D501F1EE5C1615B12AE5D76ED_H
#define KEYVALUEPAIR_2_T7706074A794F097D501F1EE5C1615B12AE5D76ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>
struct  KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED, ___value_1)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_value_1() const { return ___value_1; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T7706074A794F097D501F1EE5C1615B12AE5D76ED_H
#ifndef ENUMERATOR_T90683FD1FBC40BDBECACB04A8150C3F2E9199A66_H
#define ENUMERATOR_T90683FD1FBC40BDBECACB04A8150C3F2E9199A66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>
struct  Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::l
	List_1_t2260BF21306EA470AFD085642EAE0589F258F271 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66, ___l_0)); }
	inline List_1_t2260BF21306EA470AFD085642EAE0589F258F271 * get_l_0() const { return ___l_0; }
	inline List_1_t2260BF21306EA470AFD085642EAE0589F258F271 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t2260BF21306EA470AFD085642EAE0589F258F271 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66, ___current_3)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_current_3() const { return ___current_3; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T90683FD1FBC40BDBECACB04A8150C3F2E9199A66_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TIMESPAN_T26DE4373EA745C072A5FBDEAD8373348955FC374_H
#define TIMESPAN_T26DE4373EA745C072A5FBDEAD8373348955FC374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields, ___Zero_7)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T26DE4373EA745C072A5FBDEAD8373348955FC374_H
#ifndef VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#define VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_tDB81A15FA2AB53E2401A76B745D215397B29F783 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifndef PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#define PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef IBEACONEXCEPTION_T711CE5299D51AD566A36AF6D5372E2BD45380779_H
#define IBEACONEXCEPTION_T711CE5299D51AD566A36AF6D5372E2BD45380779_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iBeaconException
struct  iBeaconException_t711CE5299D51AD566A36AF6D5372E2BD45380779  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IBEACONEXCEPTION_T711CE5299D51AD566A36AF6D5372E2BD45380779_H
#ifndef BEACONPROPERTYATTRIBUTE_TBDB24EC120F805FEBD33A90C594B1E3870CFAFE4_H
#define BEACONPROPERTYATTRIBUTE_TBDB24EC120F805FEBD33A90C594B1E3870CFAFE4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BeaconPropertyAttribute
struct  BeaconPropertyAttribute_tBDB24EC120F805FEBD33A90C594B1E3870CFAFE4  : public PropertyAttribute_t25BFFC093C9C96E3CCF4EAB36F5DC6F937B1FA54
{
public:
	// System.String BeaconPropertyAttribute::Field
	String_t* ___Field_0;
	// BeaconType[] BeaconPropertyAttribute::Types
	BeaconTypeU5BU5D_t706B12A264072AC3C4259A7634D0E6FBD22C6D24* ___Types_1;
	// System.Boolean BeaconPropertyAttribute::Exclude
	bool ___Exclude_2;

public:
	inline static int32_t get_offset_of_Field_0() { return static_cast<int32_t>(offsetof(BeaconPropertyAttribute_tBDB24EC120F805FEBD33A90C594B1E3870CFAFE4, ___Field_0)); }
	inline String_t* get_Field_0() const { return ___Field_0; }
	inline String_t** get_address_of_Field_0() { return &___Field_0; }
	inline void set_Field_0(String_t* value)
	{
		___Field_0 = value;
		Il2CppCodeGenWriteBarrier((&___Field_0), value);
	}

	inline static int32_t get_offset_of_Types_1() { return static_cast<int32_t>(offsetof(BeaconPropertyAttribute_tBDB24EC120F805FEBD33A90C594B1E3870CFAFE4, ___Types_1)); }
	inline BeaconTypeU5BU5D_t706B12A264072AC3C4259A7634D0E6FBD22C6D24* get_Types_1() const { return ___Types_1; }
	inline BeaconTypeU5BU5D_t706B12A264072AC3C4259A7634D0E6FBD22C6D24** get_address_of_Types_1() { return &___Types_1; }
	inline void set_Types_1(BeaconTypeU5BU5D_t706B12A264072AC3C4259A7634D0E6FBD22C6D24* value)
	{
		___Types_1 = value;
		Il2CppCodeGenWriteBarrier((&___Types_1), value);
	}

	inline static int32_t get_offset_of_Exclude_2() { return static_cast<int32_t>(offsetof(BeaconPropertyAttribute_tBDB24EC120F805FEBD33A90C594B1E3870CFAFE4, ___Exclude_2)); }
	inline bool get_Exclude_2() const { return ___Exclude_2; }
	inline bool* get_address_of_Exclude_2() { return &___Exclude_2; }
	inline void set_Exclude_2(bool value)
	{
		___Exclude_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEACONPROPERTYATTRIBUTE_TBDB24EC120F805FEBD33A90C594B1E3870CFAFE4_H
#ifndef BEACONRANGE_T9B0D359B31C878EE6039FF6601054ED135CC2C77_H
#define BEACONRANGE_T9B0D359B31C878EE6039FF6601054ED135CC2C77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BeaconRange
struct  BeaconRange_t9B0D359B31C878EE6039FF6601054ED135CC2C77 
{
public:
	// System.Int32 BeaconRange::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BeaconRange_t9B0D359B31C878EE6039FF6601054ED135CC2C77, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEACONRANGE_T9B0D359B31C878EE6039FF6601054ED135CC2C77_H
#ifndef BEACONTYPE_T6F651FD6AB6CA7EC3CA1498276CCE74212C6AC49_H
#define BEACONTYPE_T6F651FD6AB6CA7EC3CA1498276CCE74212C6AC49_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BeaconType
struct  BeaconType_t6F651FD6AB6CA7EC3CA1498276CCE74212C6AC49 
{
public:
	// System.Int32 BeaconType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BeaconType_t6F651FD6AB6CA7EC3CA1498276CCE74212C6AC49, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEACONTYPE_T6F651FD6AB6CA7EC3CA1498276CCE74212C6AC49_H
#ifndef BLUETOOTHLOWENERGYSTATE_TE4FE96513B91C829BDAFDE64CABB0F60B8F444ED_H
#define BLUETOOTHLOWENERGYSTATE_TE4FE96513B91C829BDAFDE64CABB0F60B8F444ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BluetoothLowEnergyState
struct  BluetoothLowEnergyState_tE4FE96513B91C829BDAFDE64CABB0F60B8F444ED 
{
public:
	// System.Int32 BluetoothLowEnergyState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BluetoothLowEnergyState_tE4FE96513B91C829BDAFDE64CABB0F60B8F444ED, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLUETOOTHLOWENERGYSTATE_TE4FE96513B91C829BDAFDE64CABB0F60B8F444ED_H
#ifndef BROADCASTMODE_T1914C940F1376C6B26EC5D6CFD6AE3516D47E9EF_H
#define BROADCASTMODE_T1914C940F1376C6B26EC5D6CFD6AE3516D47E9EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BroadcastMode
struct  BroadcastMode_t1914C940F1376C6B26EC5D6CFD6AE3516D47E9EF 
{
public:
	// System.Int32 BroadcastMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BroadcastMode_t1914C940F1376C6B26EC5D6CFD6AE3516D47E9EF, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BROADCASTMODE_T1914C940F1376C6B26EC5D6CFD6AE3516D47E9EF_H
#ifndef BROADCASTSTATE_T16D4A451D29E58B8181255E6A7D9DCBF48FCBF23_H
#define BROADCASTSTATE_T16D4A451D29E58B8181255E6A7D9DCBF48FCBF23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BroadcastState
struct  BroadcastState_t16D4A451D29E58B8181255E6A7D9DCBF48FCBF23 
{
public:
	// System.Int32 BroadcastState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BroadcastState_t16D4A451D29E58B8181255E6A7D9DCBF48FCBF23, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BROADCASTSTATE_T16D4A451D29E58B8181255E6A7D9DCBF48FCBF23_H
#ifndef DETAILVIEW_T5D04911DF2A860E2B5C82686669E9A56BA496850_H
#define DETAILVIEW_T5D04911DF2A860E2B5C82686669E9A56BA496850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reporter_DetailView
struct  DetailView_t5D04911DF2A860E2B5C82686669E9A56BA496850 
{
public:
	// System.Int32 Reporter_DetailView::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DetailView_t5D04911DF2A860E2B5C82686669E9A56BA496850, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETAILVIEW_T5D04911DF2A860E2B5C82686669E9A56BA496850_H
#ifndef REPORTVIEW_TAA3E3DFFECC2BBF6812E5FFB898CA0E1EA070428_H
#define REPORTVIEW_TAA3E3DFFECC2BBF6812E5FFB898CA0E1EA070428_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reporter_ReportView
struct  ReportView_tAA3E3DFFECC2BBF6812E5FFB898CA0E1EA070428 
{
public:
	// System.Int32 Reporter_ReportView::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReportView_tAA3E3DFFECC2BBF6812E5FFB898CA0E1EA070428, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPORTVIEW_TAA3E3DFFECC2BBF6812E5FFB898CA0E1EA070428_H
#ifndef _LOGTYPE_T40ACA5C5A8487DA4BE69F6B2FB6397C691DDBE7A_H
#define _LOGTYPE_T40ACA5C5A8487DA4BE69F6B2FB6397C691DDBE7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reporter__LogType
struct  _LogType_t40ACA5C5A8487DA4BE69F6B2FB6397C691DDBE7A 
{
public:
	// System.Int32 Reporter__LogType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(_LogType_t40ACA5C5A8487DA4BE69F6B2FB6397C691DDBE7A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // _LOGTYPE_T40ACA5C5A8487DA4BE69F6B2FB6397C691DDBE7A_H
#ifndef U3CU3EC__ITERATOR0_T65C86444E3E918F4F0190A2FB599E33297157D53_H
#define U3CU3EC__ITERATOR0_T65C86444E3E918F4F0190A2FB599E33297157D53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONArray_<>c__Iterator0
struct  U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode> SimpleJSON.JSONArray_<>c__Iterator0::U24locvar0
	Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66  ___U24locvar0_0;
	// SimpleJSON.JSONNode SimpleJSON.JSONArray_<>c__Iterator0::<N>__1
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U3CNU3E__1_1;
	// SimpleJSON.JSONArray SimpleJSON.JSONArray_<>c__Iterator0::U24this
	JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 * ___U24this_2;
	// SimpleJSON.JSONNode SimpleJSON.JSONArray_<>c__Iterator0::U24current
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U24current_3;
	// System.Boolean SimpleJSON.JSONArray_<>c__Iterator0::U24disposing
	bool ___U24disposing_4;
	// System.Int32 SimpleJSON.JSONArray_<>c__Iterator0::U24PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53, ___U24locvar0_0)); }
	inline Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CNU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53, ___U3CNU3E__1_1)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U3CNU3E__1_1() const { return ___U3CNU3E__1_1; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U3CNU3E__1_1() { return &___U3CNU3E__1_1; }
	inline void set_U3CNU3E__1_1(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U3CNU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53, ___U24this_2)); }
	inline JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 * get_U24this_2() const { return ___U24this_2; }
	inline JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53, ___U24current_3)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U24current_3() const { return ___U24current_3; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_T65C86444E3E918F4F0190A2FB599E33297157D53_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR1_TB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1_H
#define U3CGETENUMERATORU3EC__ITERATOR1_TB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONArray_<GetEnumerator>c__Iterator1
struct  U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode> SimpleJSON.JSONArray_<GetEnumerator>c__Iterator1::U24locvar0
	Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66  ___U24locvar0_0;
	// SimpleJSON.JSONNode SimpleJSON.JSONArray_<GetEnumerator>c__Iterator1::<N>__1
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U3CNU3E__1_1;
	// SimpleJSON.JSONArray SimpleJSON.JSONArray_<GetEnumerator>c__Iterator1::U24this
	JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 * ___U24this_2;
	// System.Object SimpleJSON.JSONArray_<GetEnumerator>c__Iterator1::U24current
	RuntimeObject * ___U24current_3;
	// System.Boolean SimpleJSON.JSONArray_<GetEnumerator>c__Iterator1::U24disposing
	bool ___U24disposing_4;
	// System.Int32 SimpleJSON.JSONArray_<GetEnumerator>c__Iterator1::U24PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1, ___U24locvar0_0)); }
	inline Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t90683FD1FBC40BDBECACB04A8150C3F2E9199A66  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CNU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1, ___U3CNU3E__1_1)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U3CNU3E__1_1() const { return ___U3CNU3E__1_1; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U3CNU3E__1_1() { return &___U3CNU3E__1_1; }
	inline void set_U3CNU3E__1_1(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U3CNU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1, ___U24this_2)); }
	inline JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 * get_U24this_2() const { return ___U24this_2; }
	inline JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR1_TB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1_H
#ifndef JSONBINARYTAG_TAC82A15D9AE639E8BA8ACF99387657998313FA99_H
#define JSONBINARYTAG_TAC82A15D9AE639E8BA8ACF99387657998313FA99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONBinaryTag
struct  JSONBinaryTag_tAC82A15D9AE639E8BA8ACF99387657998313FA99 
{
public:
	// System.Int32 SimpleJSON.JSONBinaryTag::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(JSONBinaryTag_tAC82A15D9AE639E8BA8ACF99387657998313FA99, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONBINARYTAG_TAC82A15D9AE639E8BA8ACF99387657998313FA99_H
#ifndef ENUMERATOR_T6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10_H
#define ENUMERATOR_T6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>
struct  Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::dictionary
	Dictionary_2_t685AE2501706A88ADC44D7B5DC9375A46511F357 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2_Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2_Enumerator::current
	KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10, ___dictionary_0)); }
	inline Dictionary_2_t685AE2501706A88ADC44D7B5DC9375A46511F357 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t685AE2501706A88ADC44D7B5DC9375A46511F357 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t685AE2501706A88ADC44D7B5DC9375A46511F357 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10, ___current_3)); }
	inline KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10_H
#ifndef DATETIMEKIND_T922DC40070B8AD59CB650E3FE023C4A58ED4568A_H
#define DATETIMEKIND_T922DC40070B8AD59CB650E3FE023C4A58ED4568A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t922DC40070B8AD59CB650E3FE023C4A58ED4568A 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t922DC40070B8AD59CB650E3FE023C4A58ED4568A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T922DC40070B8AD59CB650E3FE023C4A58ED4568A_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_8)); }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * get_data_8() const { return ___data_8; }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T_H
#ifndef TELEMETRY_T72A66C9CD10B6758A660F2C28ABE26CDA0834C6E_H
#define TELEMETRY_T72A66C9CD10B6758A660F2C28ABE26CDA0834C6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Telemetry
struct  Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E  : public RuntimeObject
{
public:
	// System.Single Telemetry::voltage
	float ___voltage_0;
	// System.Single Telemetry::temperature
	float ___temperature_1;
	// System.Int32 Telemetry::frameCounter
	int32_t ___frameCounter_2;
	// System.TimeSpan Telemetry::uptime
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___uptime_3;
	// System.Byte[] Telemetry::encryptedData
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___encryptedData_4;
	// System.Byte[] Telemetry::salt
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___salt_5;
	// System.Byte[] Telemetry::integrityCheck
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___integrityCheck_6;
	// System.Boolean Telemetry::encrypted
	bool ___encrypted_7;

public:
	inline static int32_t get_offset_of_voltage_0() { return static_cast<int32_t>(offsetof(Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E, ___voltage_0)); }
	inline float get_voltage_0() const { return ___voltage_0; }
	inline float* get_address_of_voltage_0() { return &___voltage_0; }
	inline void set_voltage_0(float value)
	{
		___voltage_0 = value;
	}

	inline static int32_t get_offset_of_temperature_1() { return static_cast<int32_t>(offsetof(Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E, ___temperature_1)); }
	inline float get_temperature_1() const { return ___temperature_1; }
	inline float* get_address_of_temperature_1() { return &___temperature_1; }
	inline void set_temperature_1(float value)
	{
		___temperature_1 = value;
	}

	inline static int32_t get_offset_of_frameCounter_2() { return static_cast<int32_t>(offsetof(Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E, ___frameCounter_2)); }
	inline int32_t get_frameCounter_2() const { return ___frameCounter_2; }
	inline int32_t* get_address_of_frameCounter_2() { return &___frameCounter_2; }
	inline void set_frameCounter_2(int32_t value)
	{
		___frameCounter_2 = value;
	}

	inline static int32_t get_offset_of_uptime_3() { return static_cast<int32_t>(offsetof(Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E, ___uptime_3)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_uptime_3() const { return ___uptime_3; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_uptime_3() { return &___uptime_3; }
	inline void set_uptime_3(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___uptime_3 = value;
	}

	inline static int32_t get_offset_of_encryptedData_4() { return static_cast<int32_t>(offsetof(Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E, ___encryptedData_4)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_encryptedData_4() const { return ___encryptedData_4; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_encryptedData_4() { return &___encryptedData_4; }
	inline void set_encryptedData_4(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___encryptedData_4 = value;
		Il2CppCodeGenWriteBarrier((&___encryptedData_4), value);
	}

	inline static int32_t get_offset_of_salt_5() { return static_cast<int32_t>(offsetof(Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E, ___salt_5)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_salt_5() const { return ___salt_5; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_salt_5() { return &___salt_5; }
	inline void set_salt_5(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___salt_5 = value;
		Il2CppCodeGenWriteBarrier((&___salt_5), value);
	}

	inline static int32_t get_offset_of_integrityCheck_6() { return static_cast<int32_t>(offsetof(Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E, ___integrityCheck_6)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_integrityCheck_6() const { return ___integrityCheck_6; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_integrityCheck_6() { return &___integrityCheck_6; }
	inline void set_integrityCheck_6(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___integrityCheck_6 = value;
		Il2CppCodeGenWriteBarrier((&___integrityCheck_6), value);
	}

	inline static int32_t get_offset_of_encrypted_7() { return static_cast<int32_t>(offsetof(Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E, ___encrypted_7)); }
	inline bool get_encrypted_7() const { return ___encrypted_7; }
	inline bool* get_address_of_encrypted_7() { return &___encrypted_7; }
	inline void set_encrypted_7(bool value)
	{
		___encrypted_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TELEMETRY_T72A66C9CD10B6758A660F2C28ABE26CDA0834C6E_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ANDROIDMODE_T3A67510C4A6775711A0966A18D3E8486797A5E2C_H
#define ANDROIDMODE_T3A67510C4A6775711A0966A18D3E8486797A5E2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iBeaconServer_AndroidMode
struct  AndroidMode_t3A67510C4A6775711A0966A18D3E8486797A5E2C 
{
public:
	// System.Int32 iBeaconServer_AndroidMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AndroidMode_t3A67510C4A6775711A0966A18D3E8486797A5E2C, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDMODE_T3A67510C4A6775711A0966A18D3E8486797A5E2C_H
#ifndef ANDROIDPOWERLEVEL_T00973029F90DD0BA7E7047C0A50540F84869E02B_H
#define ANDROIDPOWERLEVEL_T00973029F90DD0BA7E7047C0A50540F84869E02B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iBeaconServer_AndroidPowerLevel
struct  AndroidPowerLevel_t00973029F90DD0BA7E7047C0A50540F84869E02B 
{
public:
	// System.Int32 iBeaconServer_AndroidPowerLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AndroidPowerLevel_t00973029F90DD0BA7E7047C0A50540F84869E02B, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDPOWERLEVEL_T00973029F90DD0BA7E7047C0A50540F84869E02B_H
#ifndef U3CWAITBLUETOOTHONU3EC__ITERATOR0_TCF62F557E091FF29B4429D345B955C6DCAB2B8E9_H
#define U3CWAITBLUETOOTHONU3EC__ITERATOR0_TCF62F557E091FF29B4429D345B955C6DCAB2B8E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Example_<WaitBlueToothOn>c__Iterator0
struct  U3CWaitBlueToothOnU3Ec__Iterator0_tCF62F557E091FF29B4429D345B955C6DCAB2B8E9  : public RuntimeObject
{
public:
	// BluetoothLowEnergyState Example_<WaitBlueToothOn>c__Iterator0::state
	int32_t ___state_0;
	// Example Example_<WaitBlueToothOn>c__Iterator0::U24this
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF * ___U24this_1;
	// System.Object Example_<WaitBlueToothOn>c__Iterator0::U24current
	RuntimeObject * ___U24current_2;
	// System.Boolean Example_<WaitBlueToothOn>c__Iterator0::U24disposing
	bool ___U24disposing_3;
	// System.Int32 Example_<WaitBlueToothOn>c__Iterator0::U24PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(U3CWaitBlueToothOnU3Ec__Iterator0_tCF62F557E091FF29B4429D345B955C6DCAB2B8E9, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CWaitBlueToothOnU3Ec__Iterator0_tCF62F557E091FF29B4429D345B955C6DCAB2B8E9, ___U24this_1)); }
	inline Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF * get_U24this_1() const { return ___U24this_1; }
	inline Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitBlueToothOnU3Ec__Iterator0_tCF62F557E091FF29B4429D345B955C6DCAB2B8E9, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CWaitBlueToothOnU3Ec__Iterator0_tCF62F557E091FF29B4429D345B955C6DCAB2B8E9, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CWaitBlueToothOnU3Ec__Iterator0_tCF62F557E091FF29B4429D345B955C6DCAB2B8E9, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITBLUETOOTHONU3EC__ITERATOR0_TCF62F557E091FF29B4429D345B955C6DCAB2B8E9_H
#ifndef LOG_T152F87153D078D3D99151CF14D91D518B21E3E22_H
#define LOG_T152F87153D078D3D99151CF14D91D518B21E3E22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reporter_Log
struct  Log_t152F87153D078D3D99151CF14D91D518B21E3E22  : public RuntimeObject
{
public:
	// System.Int32 Reporter_Log::count
	int32_t ___count_0;
	// Reporter__LogType Reporter_Log::logType
	int32_t ___logType_1;
	// System.String Reporter_Log::condition
	String_t* ___condition_2;
	// System.String Reporter_Log::stacktrace
	String_t* ___stacktrace_3;
	// System.Int32 Reporter_Log::sampleId
	int32_t ___sampleId_4;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(Log_t152F87153D078D3D99151CF14D91D518B21E3E22, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_logType_1() { return static_cast<int32_t>(offsetof(Log_t152F87153D078D3D99151CF14D91D518B21E3E22, ___logType_1)); }
	inline int32_t get_logType_1() const { return ___logType_1; }
	inline int32_t* get_address_of_logType_1() { return &___logType_1; }
	inline void set_logType_1(int32_t value)
	{
		___logType_1 = value;
	}

	inline static int32_t get_offset_of_condition_2() { return static_cast<int32_t>(offsetof(Log_t152F87153D078D3D99151CF14D91D518B21E3E22, ___condition_2)); }
	inline String_t* get_condition_2() const { return ___condition_2; }
	inline String_t** get_address_of_condition_2() { return &___condition_2; }
	inline void set_condition_2(String_t* value)
	{
		___condition_2 = value;
		Il2CppCodeGenWriteBarrier((&___condition_2), value);
	}

	inline static int32_t get_offset_of_stacktrace_3() { return static_cast<int32_t>(offsetof(Log_t152F87153D078D3D99151CF14D91D518B21E3E22, ___stacktrace_3)); }
	inline String_t* get_stacktrace_3() const { return ___stacktrace_3; }
	inline String_t** get_address_of_stacktrace_3() { return &___stacktrace_3; }
	inline void set_stacktrace_3(String_t* value)
	{
		___stacktrace_3 = value;
		Il2CppCodeGenWriteBarrier((&___stacktrace_3), value);
	}

	inline static int32_t get_offset_of_sampleId_4() { return static_cast<int32_t>(offsetof(Log_t152F87153D078D3D99151CF14D91D518B21E3E22, ___sampleId_4)); }
	inline int32_t get_sampleId_4() const { return ___sampleId_4; }
	inline int32_t* get_address_of_sampleId_4() { return &___sampleId_4; }
	inline void set_sampleId_4(int32_t value)
	{
		___sampleId_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOG_T152F87153D078D3D99151CF14D91D518B21E3E22_H
#ifndef U3CU3EC__ITERATOR0_TF26315FF5214262587BC852C86D664123E470A05_H
#define U3CU3EC__ITERATOR0_TF26315FF5214262587BC852C86D664123E470A05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONClass_<>c__Iterator0
struct  U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONClass_<>c__Iterator0::U24locvar0
	Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10  ___U24locvar0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONClass_<>c__Iterator0::<N>__1
	KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED  ___U3CNU3E__1_1;
	// SimpleJSON.JSONClass SimpleJSON.JSONClass_<>c__Iterator0::U24this
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U24this_2;
	// SimpleJSON.JSONNode SimpleJSON.JSONClass_<>c__Iterator0::U24current
	JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * ___U24current_3;
	// System.Boolean SimpleJSON.JSONClass_<>c__Iterator0::U24disposing
	bool ___U24disposing_4;
	// System.Int32 SimpleJSON.JSONClass_<>c__Iterator0::U24PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05, ___U24locvar0_0)); }
	inline Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CNU3E__1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05, ___U3CNU3E__1_1)); }
	inline KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED  get_U3CNU3E__1_1() const { return ___U3CNU3E__1_1; }
	inline KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED * get_address_of_U3CNU3E__1_1() { return &___U3CNU3E__1_1; }
	inline void set_U3CNU3E__1_1(KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED  value)
	{
		___U3CNU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05, ___U24this_2)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U24this_2() const { return ___U24this_2; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05, ___U24current_3)); }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * get_U24current_3() const { return ___U24current_3; }
	inline JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889 * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ITERATOR0_TF26315FF5214262587BC852C86D664123E470A05_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR1_TCFF0D13470627674DB7875937E7DAC3901E64E4D_H
#define U3CGETENUMERATORU3EC__ITERATOR1_TCFF0D13470627674DB7875937E7DAC3901E64E4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleJSON.JSONClass_<GetEnumerator>c__Iterator1
struct  U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONClass_<GetEnumerator>c__Iterator1::U24locvar0
	Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10  ___U24locvar0_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONClass_<GetEnumerator>c__Iterator1::<N>__1
	KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED  ___U3CNU3E__1_1;
	// SimpleJSON.JSONClass SimpleJSON.JSONClass_<GetEnumerator>c__Iterator1::U24this
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * ___U24this_2;
	// System.Object SimpleJSON.JSONClass_<GetEnumerator>c__Iterator1::U24current
	RuntimeObject * ___U24current_3;
	// System.Boolean SimpleJSON.JSONClass_<GetEnumerator>c__Iterator1::U24disposing
	bool ___U24disposing_4;
	// System.Int32 SimpleJSON.JSONClass_<GetEnumerator>c__Iterator1::U24PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D, ___U24locvar0_0)); }
	inline Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t6B3469D0F9EA8D3E8ECC0EC3727E5F4AC3603F10  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CNU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D, ___U3CNU3E__1_1)); }
	inline KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED  get_U3CNU3E__1_1() const { return ___U3CNU3E__1_1; }
	inline KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED * get_address_of_U3CNU3E__1_1() { return &___U3CNU3E__1_1; }
	inline void set_U3CNU3E__1_1(KeyValuePair_2_t7706074A794F097D501F1EE5C1615B12AE5D76ED  value)
	{
		___U3CNU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D, ___U24this_2)); }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * get_U24this_2() const { return ___U24this_2; }
	inline JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR1_TCFF0D13470627674DB7875937E7DAC3901E64E4D_H
#ifndef DATETIME_T816BBD9125EA02B42B87CB07643FC5F803391DA2_H
#define DATETIME_T816BBD9125EA02B42B87CB07643FC5F803391DA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2, ___ticks_10)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___MaxValue_12)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___MinValue_13)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T816BBD9125EA02B42B87CB07643FC5F803391DA2_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEACON_T36C1666DE4D5755F5A474E47B04BBEDDAA908FFD_H
#define BEACON_T36C1666DE4D5755F5A474E47B04BBEDDAA908FFD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Beacon
struct  Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD  : public RuntimeObject
{
public:
	// BeaconType Beacon::_type
	int32_t ____type_0;
	// System.String Beacon::_UUID
	String_t* ____UUID_1;
	// System.Int32 Beacon::_major
	int32_t ____major_2;
	// System.Int32 Beacon::_minor
	int32_t ____minor_3;
	// System.String Beacon::_instance
	String_t* ____instance_4;
	// System.Int32 Beacon::_rssi
	int32_t ____rssi_5;
	// BeaconRange Beacon::_range
	int32_t ____range_6;
	// System.Int32 Beacon::_strength
	int32_t ____strength_7;
	// System.Double Beacon::_accuracy
	double ____accuracy_8;
	// System.String Beacon::_rawTelemetry
	String_t* ____rawTelemetry_9;
	// System.String Beacon::_regionName
	String_t* ____regionName_10;
	// System.DateTime Beacon::_lastSeen
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ____lastSeen_11;
	// Telemetry Beacon::_telemetry
	Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E * ____telemetry_12;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____type_0)); }
	inline int32_t get__type_0() const { return ____type_0; }
	inline int32_t* get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(int32_t value)
	{
		____type_0 = value;
	}

	inline static int32_t get_offset_of__UUID_1() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____UUID_1)); }
	inline String_t* get__UUID_1() const { return ____UUID_1; }
	inline String_t** get_address_of__UUID_1() { return &____UUID_1; }
	inline void set__UUID_1(String_t* value)
	{
		____UUID_1 = value;
		Il2CppCodeGenWriteBarrier((&____UUID_1), value);
	}

	inline static int32_t get_offset_of__major_2() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____major_2)); }
	inline int32_t get__major_2() const { return ____major_2; }
	inline int32_t* get_address_of__major_2() { return &____major_2; }
	inline void set__major_2(int32_t value)
	{
		____major_2 = value;
	}

	inline static int32_t get_offset_of__minor_3() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____minor_3)); }
	inline int32_t get__minor_3() const { return ____minor_3; }
	inline int32_t* get_address_of__minor_3() { return &____minor_3; }
	inline void set__minor_3(int32_t value)
	{
		____minor_3 = value;
	}

	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____instance_4)); }
	inline String_t* get__instance_4() const { return ____instance_4; }
	inline String_t** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(String_t* value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((&____instance_4), value);
	}

	inline static int32_t get_offset_of__rssi_5() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____rssi_5)); }
	inline int32_t get__rssi_5() const { return ____rssi_5; }
	inline int32_t* get_address_of__rssi_5() { return &____rssi_5; }
	inline void set__rssi_5(int32_t value)
	{
		____rssi_5 = value;
	}

	inline static int32_t get_offset_of__range_6() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____range_6)); }
	inline int32_t get__range_6() const { return ____range_6; }
	inline int32_t* get_address_of__range_6() { return &____range_6; }
	inline void set__range_6(int32_t value)
	{
		____range_6 = value;
	}

	inline static int32_t get_offset_of__strength_7() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____strength_7)); }
	inline int32_t get__strength_7() const { return ____strength_7; }
	inline int32_t* get_address_of__strength_7() { return &____strength_7; }
	inline void set__strength_7(int32_t value)
	{
		____strength_7 = value;
	}

	inline static int32_t get_offset_of__accuracy_8() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____accuracy_8)); }
	inline double get__accuracy_8() const { return ____accuracy_8; }
	inline double* get_address_of__accuracy_8() { return &____accuracy_8; }
	inline void set__accuracy_8(double value)
	{
		____accuracy_8 = value;
	}

	inline static int32_t get_offset_of__rawTelemetry_9() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____rawTelemetry_9)); }
	inline String_t* get__rawTelemetry_9() const { return ____rawTelemetry_9; }
	inline String_t** get_address_of__rawTelemetry_9() { return &____rawTelemetry_9; }
	inline void set__rawTelemetry_9(String_t* value)
	{
		____rawTelemetry_9 = value;
		Il2CppCodeGenWriteBarrier((&____rawTelemetry_9), value);
	}

	inline static int32_t get_offset_of__regionName_10() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____regionName_10)); }
	inline String_t* get__regionName_10() const { return ____regionName_10; }
	inline String_t** get_address_of__regionName_10() { return &____regionName_10; }
	inline void set__regionName_10(String_t* value)
	{
		____regionName_10 = value;
		Il2CppCodeGenWriteBarrier((&____regionName_10), value);
	}

	inline static int32_t get_offset_of__lastSeen_11() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____lastSeen_11)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get__lastSeen_11() const { return ____lastSeen_11; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of__lastSeen_11() { return &____lastSeen_11; }
	inline void set__lastSeen_11(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		____lastSeen_11 = value;
	}

	inline static int32_t get_offset_of__telemetry_12() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD, ____telemetry_12)); }
	inline Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E * get__telemetry_12() const { return ____telemetry_12; }
	inline Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E ** get_address_of__telemetry_12() { return &____telemetry_12; }
	inline void set__telemetry_12(Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E * value)
	{
		____telemetry_12 = value;
		Il2CppCodeGenWriteBarrier((&____telemetry_12), value);
	}
};

struct Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex Beacon::EddystoneNamespaceRegex
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * ___EddystoneNamespaceRegex_13;
	// System.Text.RegularExpressions.Regex Beacon::EddystoneInstanceRegex
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * ___EddystoneInstanceRegex_14;
	// System.Text.RegularExpressions.Regex Beacon::iBeaconUUIDRegex
	Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * ___iBeaconUUIDRegex_15;

public:
	inline static int32_t get_offset_of_EddystoneNamespaceRegex_13() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD_StaticFields, ___EddystoneNamespaceRegex_13)); }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * get_EddystoneNamespaceRegex_13() const { return ___EddystoneNamespaceRegex_13; }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B ** get_address_of_EddystoneNamespaceRegex_13() { return &___EddystoneNamespaceRegex_13; }
	inline void set_EddystoneNamespaceRegex_13(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * value)
	{
		___EddystoneNamespaceRegex_13 = value;
		Il2CppCodeGenWriteBarrier((&___EddystoneNamespaceRegex_13), value);
	}

	inline static int32_t get_offset_of_EddystoneInstanceRegex_14() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD_StaticFields, ___EddystoneInstanceRegex_14)); }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * get_EddystoneInstanceRegex_14() const { return ___EddystoneInstanceRegex_14; }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B ** get_address_of_EddystoneInstanceRegex_14() { return &___EddystoneInstanceRegex_14; }
	inline void set_EddystoneInstanceRegex_14(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * value)
	{
		___EddystoneInstanceRegex_14 = value;
		Il2CppCodeGenWriteBarrier((&___EddystoneInstanceRegex_14), value);
	}

	inline static int32_t get_offset_of_iBeaconUUIDRegex_15() { return static_cast<int32_t>(offsetof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD_StaticFields, ___iBeaconUUIDRegex_15)); }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * get_iBeaconUUIDRegex_15() const { return ___iBeaconUUIDRegex_15; }
	inline Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B ** get_address_of_iBeaconUUIDRegex_15() { return &___iBeaconUUIDRegex_15; }
	inline void set_iBeaconUUIDRegex_15(Regex_t829CB33C547AB824BFEAFF320BB8F59B3A76620B * value)
	{
		___iBeaconUUIDRegex_15 = value;
		Il2CppCodeGenWriteBarrier((&___iBeaconUUIDRegex_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEACON_T36C1666DE4D5755F5A474E47B04BBEDDAA908FFD_H
#ifndef BLUETOOTHSTATECHANGED_T795820F12512E5B80A7737D6836A41A7E0C59DEF_H
#define BLUETOOTHSTATECHANGED_T795820F12512E5B80A7737D6836A41A7E0C59DEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BluetoothState_BluetoothStateChanged
struct  BluetoothStateChanged_t795820F12512E5B80A7737D6836A41A7E0C59DEF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLUETOOTHSTATECHANGED_T795820F12512E5B80A7737D6836A41A7E0C59DEF_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef BEACONRANGECHANGED_TB250A609F6139101DE7538850A8524B29198C478_H
#define BEACONRANGECHANGED_TB250A609F6139101DE7538850A8524B29198C478_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iBeaconReceiver_BeaconRangeChanged
struct  BeaconRangeChanged_tB250A609F6139101DE7538850A8524B29198C478  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEACONRANGECHANGED_TB250A609F6139101DE7538850A8524B29198C478_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef BDSLITESPEECHSAMPLE_T24AD40B96AB93161F6023D690979AF719A70DADC_H
#define BDSLITESPEECHSAMPLE_T24AD40B96AB93161F6023D690979AF719A70DADC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BDSLiteSpeechSample
struct  BDSLiteSpeechSample_t24AD40B96AB93161F6023D690979AF719A70DADC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.InputField BDSLiteSpeechSample::SpeakText
	InputField_t533609195B110760BCFF00B746C87D81969CB005 * ___SpeakText_4;

public:
	inline static int32_t get_offset_of_SpeakText_4() { return static_cast<int32_t>(offsetof(BDSLiteSpeechSample_t24AD40B96AB93161F6023D690979AF719A70DADC, ___SpeakText_4)); }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 * get_SpeakText_4() const { return ___SpeakText_4; }
	inline InputField_t533609195B110760BCFF00B746C87D81969CB005 ** get_address_of_SpeakText_4() { return &___SpeakText_4; }
	inline void set_SpeakText_4(InputField_t533609195B110760BCFF00B746C87D81969CB005 * value)
	{
		___SpeakText_4 = value;
		Il2CppCodeGenWriteBarrier((&___SpeakText_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BDSLITESPEECHSAMPLE_T24AD40B96AB93161F6023D690979AF719A70DADC_H
#ifndef BLUETOOTHSTATE_T26E5F938742C65B27F708548BC5D06AD6856713F_H
#define BLUETOOTHSTATE_T26E5F938742C65B27F708548BC5D06AD6856713F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BluetoothState
struct  BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F_StaticFields
{
public:
	// BluetoothState_BluetoothStateChanged BluetoothState::BluetoothStateChangedEvent
	BluetoothStateChanged_t795820F12512E5B80A7737D6836A41A7E0C59DEF * ___BluetoothStateChangedEvent_4;
	// System.Boolean BluetoothState::initialized
	bool ___initialized_5;
	// BluetoothState BluetoothState::m_instance
	BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F * ___m_instance_7;

public:
	inline static int32_t get_offset_of_BluetoothStateChangedEvent_4() { return static_cast<int32_t>(offsetof(BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F_StaticFields, ___BluetoothStateChangedEvent_4)); }
	inline BluetoothStateChanged_t795820F12512E5B80A7737D6836A41A7E0C59DEF * get_BluetoothStateChangedEvent_4() const { return ___BluetoothStateChangedEvent_4; }
	inline BluetoothStateChanged_t795820F12512E5B80A7737D6836A41A7E0C59DEF ** get_address_of_BluetoothStateChangedEvent_4() { return &___BluetoothStateChangedEvent_4; }
	inline void set_BluetoothStateChangedEvent_4(BluetoothStateChanged_t795820F12512E5B80A7737D6836A41A7E0C59DEF * value)
	{
		___BluetoothStateChangedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___BluetoothStateChangedEvent_4), value);
	}

	inline static int32_t get_offset_of_initialized_5() { return static_cast<int32_t>(offsetof(BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F_StaticFields, ___initialized_5)); }
	inline bool get_initialized_5() const { return ___initialized_5; }
	inline bool* get_address_of_initialized_5() { return &___initialized_5; }
	inline void set_initialized_5(bool value)
	{
		___initialized_5 = value;
	}

	inline static int32_t get_offset_of_m_instance_7() { return static_cast<int32_t>(offsetof(BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F_StaticFields, ___m_instance_7)); }
	inline BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F * get_m_instance_7() const { return ___m_instance_7; }
	inline BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F ** get_address_of_m_instance_7() { return &___m_instance_7; }
	inline void set_m_instance_7(BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F * value)
	{
		___m_instance_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_instance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLUETOOTHSTATE_T26E5F938742C65B27F708548BC5D06AD6856713F_H
#ifndef DETECTCOLLISION_T9297CB9131627AEE604F88130B279A44B3ED71B7_H
#define DETECTCOLLISION_T9297CB9131627AEE604F88130B279A44B3ED71B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DetectCollision
struct  DetectCollision_t9297CB9131627AEE604F88130B279A44B3ED71B7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTCOLLISION_T9297CB9131627AEE604F88130B279A44B3ED71B7_H
#ifndef EXAMPLE_TB7AEFF417A3F2A1E30F8931285218D37BC4954CF_H
#define EXAMPLE_TB7AEFF417A3F2A1E30F8931285218D37BC4954CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Example
struct  Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// BeaconType Example::bt_Type
	int32_t ___bt_Type_4;
	// BroadcastMode Example::bm_Mode
	int32_t ___bm_Mode_5;
	// BroadcastState Example::bs_State
	int32_t ___bs_State_6;
	// UnityEngine.GameObject Example::ObjectCube
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ObjectCube_7;
	// UnityEngine.GameObject Example::BeaconCamera
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___BeaconCamera_8;
	// System.Single Example::currentTime
	float ___currentTime_9;
	// System.Single Example::spawnDelay
	float ___spawnDelay_10;
	// UnityEngine.GameObject Example::go_FoundBeaconClone
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___go_FoundBeaconClone_11;
	// System.Single Example::f_ScrollViewContentRectWidth
	float ___f_ScrollViewContentRectWidth_12;
	// System.Single Example::f_ScrollViewContentRectHeight
	float ___f_ScrollViewContentRectHeight_13;
	// System.Int32 Example::i_BeaconCounter
	int32_t ___i_BeaconCounter_14;
	// System.Int32 Example::mSpawnCount
	int32_t ___mSpawnCount_15;
	// System.Collections.Generic.List`1<Beacon> Example::mybeacons
	List_1_t39992A124A89322ED608A124E78EB5AC0CAA8B7A * ___mybeacons_16;

public:
	inline static int32_t get_offset_of_bt_Type_4() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___bt_Type_4)); }
	inline int32_t get_bt_Type_4() const { return ___bt_Type_4; }
	inline int32_t* get_address_of_bt_Type_4() { return &___bt_Type_4; }
	inline void set_bt_Type_4(int32_t value)
	{
		___bt_Type_4 = value;
	}

	inline static int32_t get_offset_of_bm_Mode_5() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___bm_Mode_5)); }
	inline int32_t get_bm_Mode_5() const { return ___bm_Mode_5; }
	inline int32_t* get_address_of_bm_Mode_5() { return &___bm_Mode_5; }
	inline void set_bm_Mode_5(int32_t value)
	{
		___bm_Mode_5 = value;
	}

	inline static int32_t get_offset_of_bs_State_6() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___bs_State_6)); }
	inline int32_t get_bs_State_6() const { return ___bs_State_6; }
	inline int32_t* get_address_of_bs_State_6() { return &___bs_State_6; }
	inline void set_bs_State_6(int32_t value)
	{
		___bs_State_6 = value;
	}

	inline static int32_t get_offset_of_ObjectCube_7() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___ObjectCube_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ObjectCube_7() const { return ___ObjectCube_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ObjectCube_7() { return &___ObjectCube_7; }
	inline void set_ObjectCube_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ObjectCube_7 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectCube_7), value);
	}

	inline static int32_t get_offset_of_BeaconCamera_8() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___BeaconCamera_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_BeaconCamera_8() const { return ___BeaconCamera_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_BeaconCamera_8() { return &___BeaconCamera_8; }
	inline void set_BeaconCamera_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___BeaconCamera_8 = value;
		Il2CppCodeGenWriteBarrier((&___BeaconCamera_8), value);
	}

	inline static int32_t get_offset_of_currentTime_9() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___currentTime_9)); }
	inline float get_currentTime_9() const { return ___currentTime_9; }
	inline float* get_address_of_currentTime_9() { return &___currentTime_9; }
	inline void set_currentTime_9(float value)
	{
		___currentTime_9 = value;
	}

	inline static int32_t get_offset_of_spawnDelay_10() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___spawnDelay_10)); }
	inline float get_spawnDelay_10() const { return ___spawnDelay_10; }
	inline float* get_address_of_spawnDelay_10() { return &___spawnDelay_10; }
	inline void set_spawnDelay_10(float value)
	{
		___spawnDelay_10 = value;
	}

	inline static int32_t get_offset_of_go_FoundBeaconClone_11() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___go_FoundBeaconClone_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_go_FoundBeaconClone_11() const { return ___go_FoundBeaconClone_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_go_FoundBeaconClone_11() { return &___go_FoundBeaconClone_11; }
	inline void set_go_FoundBeaconClone_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___go_FoundBeaconClone_11 = value;
		Il2CppCodeGenWriteBarrier((&___go_FoundBeaconClone_11), value);
	}

	inline static int32_t get_offset_of_f_ScrollViewContentRectWidth_12() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___f_ScrollViewContentRectWidth_12)); }
	inline float get_f_ScrollViewContentRectWidth_12() const { return ___f_ScrollViewContentRectWidth_12; }
	inline float* get_address_of_f_ScrollViewContentRectWidth_12() { return &___f_ScrollViewContentRectWidth_12; }
	inline void set_f_ScrollViewContentRectWidth_12(float value)
	{
		___f_ScrollViewContentRectWidth_12 = value;
	}

	inline static int32_t get_offset_of_f_ScrollViewContentRectHeight_13() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___f_ScrollViewContentRectHeight_13)); }
	inline float get_f_ScrollViewContentRectHeight_13() const { return ___f_ScrollViewContentRectHeight_13; }
	inline float* get_address_of_f_ScrollViewContentRectHeight_13() { return &___f_ScrollViewContentRectHeight_13; }
	inline void set_f_ScrollViewContentRectHeight_13(float value)
	{
		___f_ScrollViewContentRectHeight_13 = value;
	}

	inline static int32_t get_offset_of_i_BeaconCounter_14() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___i_BeaconCounter_14)); }
	inline int32_t get_i_BeaconCounter_14() const { return ___i_BeaconCounter_14; }
	inline int32_t* get_address_of_i_BeaconCounter_14() { return &___i_BeaconCounter_14; }
	inline void set_i_BeaconCounter_14(int32_t value)
	{
		___i_BeaconCounter_14 = value;
	}

	inline static int32_t get_offset_of_mSpawnCount_15() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___mSpawnCount_15)); }
	inline int32_t get_mSpawnCount_15() const { return ___mSpawnCount_15; }
	inline int32_t* get_address_of_mSpawnCount_15() { return &___mSpawnCount_15; }
	inline void set_mSpawnCount_15(int32_t value)
	{
		___mSpawnCount_15 = value;
	}

	inline static int32_t get_offset_of_mybeacons_16() { return static_cast<int32_t>(offsetof(Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF, ___mybeacons_16)); }
	inline List_1_t39992A124A89322ED608A124E78EB5AC0CAA8B7A * get_mybeacons_16() const { return ___mybeacons_16; }
	inline List_1_t39992A124A89322ED608A124E78EB5AC0CAA8B7A ** get_address_of_mybeacons_16() { return &___mybeacons_16; }
	inline void set_mybeacons_16(List_1_t39992A124A89322ED608A124E78EB5AC0CAA8B7A * value)
	{
		___mybeacons_16 = value;
		Il2CppCodeGenWriteBarrier((&___mybeacons_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLE_TB7AEFF417A3F2A1E30F8931285218D37BC4954CF_H
#ifndef REPORTER_TFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E_H
#define REPORTER_TFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Reporter
struct  Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Reporter_Sample> Reporter::samples
	List_1_t6B087B34678055970468DA4EC69ABFD3C66EC9CC * ___samples_5;
	// System.Collections.Generic.List`1<Reporter_Log> Reporter::logs
	List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 * ___logs_6;
	// System.Collections.Generic.List`1<Reporter_Log> Reporter::collapsedLogs
	List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 * ___collapsedLogs_7;
	// System.Collections.Generic.List`1<Reporter_Log> Reporter::currentLog
	List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 * ___currentLog_8;
	// MultiKeyDictionary`3<System.String,System.String,Reporter_Log> Reporter::logsDic
	MultiKeyDictionary_3_t999B39286A71D85EB1F36E92538A09BA09791E27 * ___logsDic_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Reporter::cachedString
	Dictionary_2_tFDA27909C2695A2BBE12DBC4C8BCE6C3D8B82D61 * ___cachedString_10;
	// System.Boolean Reporter::show
	bool ___show_11;
	// System.Boolean Reporter::collapse
	bool ___collapse_12;
	// System.Boolean Reporter::clearOnNewSceneLoaded
	bool ___clearOnNewSceneLoaded_13;
	// System.Boolean Reporter::showTime
	bool ___showTime_14;
	// System.Boolean Reporter::showScene
	bool ___showScene_15;
	// System.Boolean Reporter::showMemory
	bool ___showMemory_16;
	// System.Boolean Reporter::showFps
	bool ___showFps_17;
	// System.Boolean Reporter::showGraph
	bool ___showGraph_18;
	// System.Boolean Reporter::showLog
	bool ___showLog_19;
	// System.Boolean Reporter::showWarning
	bool ___showWarning_20;
	// System.Boolean Reporter::showError
	bool ___showError_21;
	// System.Int32 Reporter::numOfLogs
	int32_t ___numOfLogs_22;
	// System.Int32 Reporter::numOfLogsWarning
	int32_t ___numOfLogsWarning_23;
	// System.Int32 Reporter::numOfLogsError
	int32_t ___numOfLogsError_24;
	// System.Int32 Reporter::numOfCollapsedLogs
	int32_t ___numOfCollapsedLogs_25;
	// System.Int32 Reporter::numOfCollapsedLogsWarning
	int32_t ___numOfCollapsedLogsWarning_26;
	// System.Int32 Reporter::numOfCollapsedLogsError
	int32_t ___numOfCollapsedLogsError_27;
	// System.Boolean Reporter::showClearOnNewSceneLoadedButton
	bool ___showClearOnNewSceneLoadedButton_28;
	// System.Boolean Reporter::showTimeButton
	bool ___showTimeButton_29;
	// System.Boolean Reporter::showSceneButton
	bool ___showSceneButton_30;
	// System.Boolean Reporter::showMemButton
	bool ___showMemButton_31;
	// System.Boolean Reporter::showFpsButton
	bool ___showFpsButton_32;
	// System.Boolean Reporter::showSearchText
	bool ___showSearchText_33;
	// System.Boolean Reporter::showCopyButton
	bool ___showCopyButton_34;
	// System.Boolean Reporter::showSaveButton
	bool ___showSaveButton_35;
	// System.String Reporter::buildDate
	String_t* ___buildDate_36;
	// System.String Reporter::logDate
	String_t* ___logDate_37;
	// System.Single Reporter::logsMemUsage
	float ___logsMemUsage_38;
	// System.Single Reporter::graphMemUsage
	float ___graphMemUsage_39;
	// System.Single Reporter::gcTotalMemory
	float ___gcTotalMemory_40;
	// System.String Reporter::UserData
	String_t* ___UserData_41;
	// System.Single Reporter::fps
	float ___fps_42;
	// System.String Reporter::fpsText
	String_t* ___fpsText_43;
	// Reporter_ReportView Reporter::currentView
	int32_t ___currentView_44;
	// Images Reporter::images
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC * ___images_46;
	// UnityEngine.GUIContent Reporter::clearContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___clearContent_47;
	// UnityEngine.GUIContent Reporter::collapseContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___collapseContent_48;
	// UnityEngine.GUIContent Reporter::clearOnNewSceneContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___clearOnNewSceneContent_49;
	// UnityEngine.GUIContent Reporter::showTimeContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___showTimeContent_50;
	// UnityEngine.GUIContent Reporter::showSceneContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___showSceneContent_51;
	// UnityEngine.GUIContent Reporter::userContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___userContent_52;
	// UnityEngine.GUIContent Reporter::showMemoryContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___showMemoryContent_53;
	// UnityEngine.GUIContent Reporter::softwareContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___softwareContent_54;
	// UnityEngine.GUIContent Reporter::dateContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___dateContent_55;
	// UnityEngine.GUIContent Reporter::showFpsContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___showFpsContent_56;
	// UnityEngine.GUIContent Reporter::infoContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___infoContent_57;
	// UnityEngine.GUIContent Reporter::saveLogsContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___saveLogsContent_58;
	// UnityEngine.GUIContent Reporter::searchContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___searchContent_59;
	// UnityEngine.GUIContent Reporter::copyContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___copyContent_60;
	// UnityEngine.GUIContent Reporter::closeContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___closeContent_61;
	// UnityEngine.GUIContent Reporter::buildFromContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___buildFromContent_62;
	// UnityEngine.GUIContent Reporter::systemInfoContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___systemInfoContent_63;
	// UnityEngine.GUIContent Reporter::graphicsInfoContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___graphicsInfoContent_64;
	// UnityEngine.GUIContent Reporter::backContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___backContent_65;
	// UnityEngine.GUIContent Reporter::logContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___logContent_66;
	// UnityEngine.GUIContent Reporter::warningContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___warningContent_67;
	// UnityEngine.GUIContent Reporter::errorContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___errorContent_68;
	// UnityEngine.GUIStyle Reporter::barStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___barStyle_69;
	// UnityEngine.GUIStyle Reporter::buttonActiveStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___buttonActiveStyle_70;
	// UnityEngine.GUIStyle Reporter::nonStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___nonStyle_71;
	// UnityEngine.GUIStyle Reporter::lowerLeftFontStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___lowerLeftFontStyle_72;
	// UnityEngine.GUIStyle Reporter::backStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___backStyle_73;
	// UnityEngine.GUIStyle Reporter::evenLogStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___evenLogStyle_74;
	// UnityEngine.GUIStyle Reporter::oddLogStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___oddLogStyle_75;
	// UnityEngine.GUIStyle Reporter::logButtonStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___logButtonStyle_76;
	// UnityEngine.GUIStyle Reporter::selectedLogStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___selectedLogStyle_77;
	// UnityEngine.GUIStyle Reporter::selectedLogFontStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___selectedLogFontStyle_78;
	// UnityEngine.GUIStyle Reporter::stackLabelStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___stackLabelStyle_79;
	// UnityEngine.GUIStyle Reporter::scrollerStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___scrollerStyle_80;
	// UnityEngine.GUIStyle Reporter::searchStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___searchStyle_81;
	// UnityEngine.GUIStyle Reporter::sliderBackStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___sliderBackStyle_82;
	// UnityEngine.GUIStyle Reporter::sliderThumbStyle
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___sliderThumbStyle_83;
	// UnityEngine.GUISkin Reporter::toolbarScrollerSkin
	GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * ___toolbarScrollerSkin_84;
	// UnityEngine.GUISkin Reporter::logScrollerSkin
	GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * ___logScrollerSkin_85;
	// UnityEngine.GUISkin Reporter::graphScrollerSkin
	GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * ___graphScrollerSkin_86;
	// UnityEngine.Vector2 Reporter::size
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___size_87;
	// System.Single Reporter::maxSize
	float ___maxSize_88;
	// System.Int32 Reporter::numOfCircleToShow
	int32_t ___numOfCircleToShow_89;
	// System.String Reporter::currentScene
	String_t* ___currentScene_91;
	// System.String Reporter::filterText
	String_t* ___filterText_92;
	// System.String Reporter::deviceModel
	String_t* ___deviceModel_93;
	// System.String Reporter::deviceType
	String_t* ___deviceType_94;
	// System.String Reporter::deviceName
	String_t* ___deviceName_95;
	// System.String Reporter::graphicsMemorySize
	String_t* ___graphicsMemorySize_96;
	// System.String Reporter::maxTextureSize
	String_t* ___maxTextureSize_97;
	// System.String Reporter::systemMemorySize
	String_t* ___systemMemorySize_98;
	// System.Boolean Reporter::Initialized
	bool ___Initialized_99;
	// UnityEngine.Rect Reporter::screenRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___screenRect_100;
	// UnityEngine.Rect Reporter::toolBarRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___toolBarRect_101;
	// UnityEngine.Rect Reporter::logsRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___logsRect_102;
	// UnityEngine.Rect Reporter::stackRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___stackRect_103;
	// UnityEngine.Rect Reporter::graphRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___graphRect_104;
	// UnityEngine.Rect Reporter::graphMinRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___graphMinRect_105;
	// UnityEngine.Rect Reporter::graphMaxRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___graphMaxRect_106;
	// UnityEngine.Rect Reporter::buttomRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___buttomRect_107;
	// UnityEngine.Vector2 Reporter::stackRectTopLeft
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___stackRectTopLeft_108;
	// UnityEngine.Rect Reporter::detailRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___detailRect_109;
	// UnityEngine.Vector2 Reporter::scrollPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scrollPosition_110;
	// UnityEngine.Vector2 Reporter::scrollPosition2
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scrollPosition2_111;
	// UnityEngine.Vector2 Reporter::toolbarScrollPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___toolbarScrollPosition_112;
	// Reporter_Log Reporter::selectedLog
	Log_t152F87153D078D3D99151CF14D91D518B21E3E22 * ___selectedLog_113;
	// System.Single Reporter::toolbarOldDrag
	float ___toolbarOldDrag_114;
	// System.Single Reporter::oldDrag
	float ___oldDrag_115;
	// System.Single Reporter::oldDrag2
	float ___oldDrag2_116;
	// System.Single Reporter::oldDrag3
	float ___oldDrag3_117;
	// System.Int32 Reporter::startIndex
	int32_t ___startIndex_118;
	// UnityEngine.Rect Reporter::countRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___countRect_119;
	// UnityEngine.Rect Reporter::timeRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___timeRect_120;
	// UnityEngine.Rect Reporter::timeLabelRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___timeLabelRect_121;
	// UnityEngine.Rect Reporter::sceneRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___sceneRect_122;
	// UnityEngine.Rect Reporter::sceneLabelRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___sceneLabelRect_123;
	// UnityEngine.Rect Reporter::memoryRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___memoryRect_124;
	// UnityEngine.Rect Reporter::memoryLabelRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___memoryLabelRect_125;
	// UnityEngine.Rect Reporter::fpsRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___fpsRect_126;
	// UnityEngine.Rect Reporter::fpsLabelRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___fpsLabelRect_127;
	// UnityEngine.GUIContent Reporter::tempContent
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___tempContent_128;
	// UnityEngine.Vector2 Reporter::infoScrollPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___infoScrollPosition_129;
	// UnityEngine.Vector2 Reporter::oldInfoDrag
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oldInfoDrag_130;
	// UnityEngine.Rect Reporter::tempRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___tempRect_131;
	// System.Single Reporter::graphSize
	float ___graphSize_132;
	// System.Int32 Reporter::startFrame
	int32_t ___startFrame_133;
	// System.Int32 Reporter::currentFrame
	int32_t ___currentFrame_134;
	// UnityEngine.Vector3 Reporter::tempVector1
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___tempVector1_135;
	// UnityEngine.Vector3 Reporter::tempVector2
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___tempVector2_136;
	// UnityEngine.Vector2 Reporter::graphScrollerPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___graphScrollerPos_137;
	// System.Single Reporter::maxFpsValue
	float ___maxFpsValue_138;
	// System.Single Reporter::minFpsValue
	float ___minFpsValue_139;
	// System.Single Reporter::maxMemoryValue
	float ___maxMemoryValue_140;
	// System.Single Reporter::minMemoryValue
	float ___minMemoryValue_141;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Reporter::gestureDetector
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___gestureDetector_142;
	// UnityEngine.Vector2 Reporter::gestureSum
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___gestureSum_143;
	// System.Single Reporter::gestureLength
	float ___gestureLength_144;
	// System.Int32 Reporter::gestureCount
	int32_t ___gestureCount_145;
	// System.Single Reporter::lastClickTime
	float ___lastClickTime_146;
	// UnityEngine.Vector2 Reporter::startPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___startPos_147;
	// UnityEngine.Vector2 Reporter::downPos
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downPos_148;
	// UnityEngine.Vector2 Reporter::mousePosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___mousePosition_149;
	// System.Int32 Reporter::frames
	int32_t ___frames_150;
	// System.Boolean Reporter::firstTime
	bool ___firstTime_151;
	// System.Single Reporter::lastUpdate
	float ___lastUpdate_152;
	// System.Collections.Generic.List`1<Reporter_Log> Reporter::threadedLogs
	List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 * ___threadedLogs_155;

public:
	inline static int32_t get_offset_of_samples_5() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___samples_5)); }
	inline List_1_t6B087B34678055970468DA4EC69ABFD3C66EC9CC * get_samples_5() const { return ___samples_5; }
	inline List_1_t6B087B34678055970468DA4EC69ABFD3C66EC9CC ** get_address_of_samples_5() { return &___samples_5; }
	inline void set_samples_5(List_1_t6B087B34678055970468DA4EC69ABFD3C66EC9CC * value)
	{
		___samples_5 = value;
		Il2CppCodeGenWriteBarrier((&___samples_5), value);
	}

	inline static int32_t get_offset_of_logs_6() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___logs_6)); }
	inline List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 * get_logs_6() const { return ___logs_6; }
	inline List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 ** get_address_of_logs_6() { return &___logs_6; }
	inline void set_logs_6(List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 * value)
	{
		___logs_6 = value;
		Il2CppCodeGenWriteBarrier((&___logs_6), value);
	}

	inline static int32_t get_offset_of_collapsedLogs_7() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___collapsedLogs_7)); }
	inline List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 * get_collapsedLogs_7() const { return ___collapsedLogs_7; }
	inline List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 ** get_address_of_collapsedLogs_7() { return &___collapsedLogs_7; }
	inline void set_collapsedLogs_7(List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 * value)
	{
		___collapsedLogs_7 = value;
		Il2CppCodeGenWriteBarrier((&___collapsedLogs_7), value);
	}

	inline static int32_t get_offset_of_currentLog_8() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___currentLog_8)); }
	inline List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 * get_currentLog_8() const { return ___currentLog_8; }
	inline List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 ** get_address_of_currentLog_8() { return &___currentLog_8; }
	inline void set_currentLog_8(List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 * value)
	{
		___currentLog_8 = value;
		Il2CppCodeGenWriteBarrier((&___currentLog_8), value);
	}

	inline static int32_t get_offset_of_logsDic_9() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___logsDic_9)); }
	inline MultiKeyDictionary_3_t999B39286A71D85EB1F36E92538A09BA09791E27 * get_logsDic_9() const { return ___logsDic_9; }
	inline MultiKeyDictionary_3_t999B39286A71D85EB1F36E92538A09BA09791E27 ** get_address_of_logsDic_9() { return &___logsDic_9; }
	inline void set_logsDic_9(MultiKeyDictionary_3_t999B39286A71D85EB1F36E92538A09BA09791E27 * value)
	{
		___logsDic_9 = value;
		Il2CppCodeGenWriteBarrier((&___logsDic_9), value);
	}

	inline static int32_t get_offset_of_cachedString_10() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___cachedString_10)); }
	inline Dictionary_2_tFDA27909C2695A2BBE12DBC4C8BCE6C3D8B82D61 * get_cachedString_10() const { return ___cachedString_10; }
	inline Dictionary_2_tFDA27909C2695A2BBE12DBC4C8BCE6C3D8B82D61 ** get_address_of_cachedString_10() { return &___cachedString_10; }
	inline void set_cachedString_10(Dictionary_2_tFDA27909C2695A2BBE12DBC4C8BCE6C3D8B82D61 * value)
	{
		___cachedString_10 = value;
		Il2CppCodeGenWriteBarrier((&___cachedString_10), value);
	}

	inline static int32_t get_offset_of_show_11() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___show_11)); }
	inline bool get_show_11() const { return ___show_11; }
	inline bool* get_address_of_show_11() { return &___show_11; }
	inline void set_show_11(bool value)
	{
		___show_11 = value;
	}

	inline static int32_t get_offset_of_collapse_12() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___collapse_12)); }
	inline bool get_collapse_12() const { return ___collapse_12; }
	inline bool* get_address_of_collapse_12() { return &___collapse_12; }
	inline void set_collapse_12(bool value)
	{
		___collapse_12 = value;
	}

	inline static int32_t get_offset_of_clearOnNewSceneLoaded_13() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___clearOnNewSceneLoaded_13)); }
	inline bool get_clearOnNewSceneLoaded_13() const { return ___clearOnNewSceneLoaded_13; }
	inline bool* get_address_of_clearOnNewSceneLoaded_13() { return &___clearOnNewSceneLoaded_13; }
	inline void set_clearOnNewSceneLoaded_13(bool value)
	{
		___clearOnNewSceneLoaded_13 = value;
	}

	inline static int32_t get_offset_of_showTime_14() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showTime_14)); }
	inline bool get_showTime_14() const { return ___showTime_14; }
	inline bool* get_address_of_showTime_14() { return &___showTime_14; }
	inline void set_showTime_14(bool value)
	{
		___showTime_14 = value;
	}

	inline static int32_t get_offset_of_showScene_15() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showScene_15)); }
	inline bool get_showScene_15() const { return ___showScene_15; }
	inline bool* get_address_of_showScene_15() { return &___showScene_15; }
	inline void set_showScene_15(bool value)
	{
		___showScene_15 = value;
	}

	inline static int32_t get_offset_of_showMemory_16() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showMemory_16)); }
	inline bool get_showMemory_16() const { return ___showMemory_16; }
	inline bool* get_address_of_showMemory_16() { return &___showMemory_16; }
	inline void set_showMemory_16(bool value)
	{
		___showMemory_16 = value;
	}

	inline static int32_t get_offset_of_showFps_17() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showFps_17)); }
	inline bool get_showFps_17() const { return ___showFps_17; }
	inline bool* get_address_of_showFps_17() { return &___showFps_17; }
	inline void set_showFps_17(bool value)
	{
		___showFps_17 = value;
	}

	inline static int32_t get_offset_of_showGraph_18() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showGraph_18)); }
	inline bool get_showGraph_18() const { return ___showGraph_18; }
	inline bool* get_address_of_showGraph_18() { return &___showGraph_18; }
	inline void set_showGraph_18(bool value)
	{
		___showGraph_18 = value;
	}

	inline static int32_t get_offset_of_showLog_19() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showLog_19)); }
	inline bool get_showLog_19() const { return ___showLog_19; }
	inline bool* get_address_of_showLog_19() { return &___showLog_19; }
	inline void set_showLog_19(bool value)
	{
		___showLog_19 = value;
	}

	inline static int32_t get_offset_of_showWarning_20() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showWarning_20)); }
	inline bool get_showWarning_20() const { return ___showWarning_20; }
	inline bool* get_address_of_showWarning_20() { return &___showWarning_20; }
	inline void set_showWarning_20(bool value)
	{
		___showWarning_20 = value;
	}

	inline static int32_t get_offset_of_showError_21() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showError_21)); }
	inline bool get_showError_21() const { return ___showError_21; }
	inline bool* get_address_of_showError_21() { return &___showError_21; }
	inline void set_showError_21(bool value)
	{
		___showError_21 = value;
	}

	inline static int32_t get_offset_of_numOfLogs_22() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___numOfLogs_22)); }
	inline int32_t get_numOfLogs_22() const { return ___numOfLogs_22; }
	inline int32_t* get_address_of_numOfLogs_22() { return &___numOfLogs_22; }
	inline void set_numOfLogs_22(int32_t value)
	{
		___numOfLogs_22 = value;
	}

	inline static int32_t get_offset_of_numOfLogsWarning_23() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___numOfLogsWarning_23)); }
	inline int32_t get_numOfLogsWarning_23() const { return ___numOfLogsWarning_23; }
	inline int32_t* get_address_of_numOfLogsWarning_23() { return &___numOfLogsWarning_23; }
	inline void set_numOfLogsWarning_23(int32_t value)
	{
		___numOfLogsWarning_23 = value;
	}

	inline static int32_t get_offset_of_numOfLogsError_24() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___numOfLogsError_24)); }
	inline int32_t get_numOfLogsError_24() const { return ___numOfLogsError_24; }
	inline int32_t* get_address_of_numOfLogsError_24() { return &___numOfLogsError_24; }
	inline void set_numOfLogsError_24(int32_t value)
	{
		___numOfLogsError_24 = value;
	}

	inline static int32_t get_offset_of_numOfCollapsedLogs_25() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___numOfCollapsedLogs_25)); }
	inline int32_t get_numOfCollapsedLogs_25() const { return ___numOfCollapsedLogs_25; }
	inline int32_t* get_address_of_numOfCollapsedLogs_25() { return &___numOfCollapsedLogs_25; }
	inline void set_numOfCollapsedLogs_25(int32_t value)
	{
		___numOfCollapsedLogs_25 = value;
	}

	inline static int32_t get_offset_of_numOfCollapsedLogsWarning_26() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___numOfCollapsedLogsWarning_26)); }
	inline int32_t get_numOfCollapsedLogsWarning_26() const { return ___numOfCollapsedLogsWarning_26; }
	inline int32_t* get_address_of_numOfCollapsedLogsWarning_26() { return &___numOfCollapsedLogsWarning_26; }
	inline void set_numOfCollapsedLogsWarning_26(int32_t value)
	{
		___numOfCollapsedLogsWarning_26 = value;
	}

	inline static int32_t get_offset_of_numOfCollapsedLogsError_27() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___numOfCollapsedLogsError_27)); }
	inline int32_t get_numOfCollapsedLogsError_27() const { return ___numOfCollapsedLogsError_27; }
	inline int32_t* get_address_of_numOfCollapsedLogsError_27() { return &___numOfCollapsedLogsError_27; }
	inline void set_numOfCollapsedLogsError_27(int32_t value)
	{
		___numOfCollapsedLogsError_27 = value;
	}

	inline static int32_t get_offset_of_showClearOnNewSceneLoadedButton_28() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showClearOnNewSceneLoadedButton_28)); }
	inline bool get_showClearOnNewSceneLoadedButton_28() const { return ___showClearOnNewSceneLoadedButton_28; }
	inline bool* get_address_of_showClearOnNewSceneLoadedButton_28() { return &___showClearOnNewSceneLoadedButton_28; }
	inline void set_showClearOnNewSceneLoadedButton_28(bool value)
	{
		___showClearOnNewSceneLoadedButton_28 = value;
	}

	inline static int32_t get_offset_of_showTimeButton_29() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showTimeButton_29)); }
	inline bool get_showTimeButton_29() const { return ___showTimeButton_29; }
	inline bool* get_address_of_showTimeButton_29() { return &___showTimeButton_29; }
	inline void set_showTimeButton_29(bool value)
	{
		___showTimeButton_29 = value;
	}

	inline static int32_t get_offset_of_showSceneButton_30() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showSceneButton_30)); }
	inline bool get_showSceneButton_30() const { return ___showSceneButton_30; }
	inline bool* get_address_of_showSceneButton_30() { return &___showSceneButton_30; }
	inline void set_showSceneButton_30(bool value)
	{
		___showSceneButton_30 = value;
	}

	inline static int32_t get_offset_of_showMemButton_31() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showMemButton_31)); }
	inline bool get_showMemButton_31() const { return ___showMemButton_31; }
	inline bool* get_address_of_showMemButton_31() { return &___showMemButton_31; }
	inline void set_showMemButton_31(bool value)
	{
		___showMemButton_31 = value;
	}

	inline static int32_t get_offset_of_showFpsButton_32() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showFpsButton_32)); }
	inline bool get_showFpsButton_32() const { return ___showFpsButton_32; }
	inline bool* get_address_of_showFpsButton_32() { return &___showFpsButton_32; }
	inline void set_showFpsButton_32(bool value)
	{
		___showFpsButton_32 = value;
	}

	inline static int32_t get_offset_of_showSearchText_33() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showSearchText_33)); }
	inline bool get_showSearchText_33() const { return ___showSearchText_33; }
	inline bool* get_address_of_showSearchText_33() { return &___showSearchText_33; }
	inline void set_showSearchText_33(bool value)
	{
		___showSearchText_33 = value;
	}

	inline static int32_t get_offset_of_showCopyButton_34() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showCopyButton_34)); }
	inline bool get_showCopyButton_34() const { return ___showCopyButton_34; }
	inline bool* get_address_of_showCopyButton_34() { return &___showCopyButton_34; }
	inline void set_showCopyButton_34(bool value)
	{
		___showCopyButton_34 = value;
	}

	inline static int32_t get_offset_of_showSaveButton_35() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showSaveButton_35)); }
	inline bool get_showSaveButton_35() const { return ___showSaveButton_35; }
	inline bool* get_address_of_showSaveButton_35() { return &___showSaveButton_35; }
	inline void set_showSaveButton_35(bool value)
	{
		___showSaveButton_35 = value;
	}

	inline static int32_t get_offset_of_buildDate_36() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___buildDate_36)); }
	inline String_t* get_buildDate_36() const { return ___buildDate_36; }
	inline String_t** get_address_of_buildDate_36() { return &___buildDate_36; }
	inline void set_buildDate_36(String_t* value)
	{
		___buildDate_36 = value;
		Il2CppCodeGenWriteBarrier((&___buildDate_36), value);
	}

	inline static int32_t get_offset_of_logDate_37() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___logDate_37)); }
	inline String_t* get_logDate_37() const { return ___logDate_37; }
	inline String_t** get_address_of_logDate_37() { return &___logDate_37; }
	inline void set_logDate_37(String_t* value)
	{
		___logDate_37 = value;
		Il2CppCodeGenWriteBarrier((&___logDate_37), value);
	}

	inline static int32_t get_offset_of_logsMemUsage_38() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___logsMemUsage_38)); }
	inline float get_logsMemUsage_38() const { return ___logsMemUsage_38; }
	inline float* get_address_of_logsMemUsage_38() { return &___logsMemUsage_38; }
	inline void set_logsMemUsage_38(float value)
	{
		___logsMemUsage_38 = value;
	}

	inline static int32_t get_offset_of_graphMemUsage_39() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___graphMemUsage_39)); }
	inline float get_graphMemUsage_39() const { return ___graphMemUsage_39; }
	inline float* get_address_of_graphMemUsage_39() { return &___graphMemUsage_39; }
	inline void set_graphMemUsage_39(float value)
	{
		___graphMemUsage_39 = value;
	}

	inline static int32_t get_offset_of_gcTotalMemory_40() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___gcTotalMemory_40)); }
	inline float get_gcTotalMemory_40() const { return ___gcTotalMemory_40; }
	inline float* get_address_of_gcTotalMemory_40() { return &___gcTotalMemory_40; }
	inline void set_gcTotalMemory_40(float value)
	{
		___gcTotalMemory_40 = value;
	}

	inline static int32_t get_offset_of_UserData_41() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___UserData_41)); }
	inline String_t* get_UserData_41() const { return ___UserData_41; }
	inline String_t** get_address_of_UserData_41() { return &___UserData_41; }
	inline void set_UserData_41(String_t* value)
	{
		___UserData_41 = value;
		Il2CppCodeGenWriteBarrier((&___UserData_41), value);
	}

	inline static int32_t get_offset_of_fps_42() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___fps_42)); }
	inline float get_fps_42() const { return ___fps_42; }
	inline float* get_address_of_fps_42() { return &___fps_42; }
	inline void set_fps_42(float value)
	{
		___fps_42 = value;
	}

	inline static int32_t get_offset_of_fpsText_43() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___fpsText_43)); }
	inline String_t* get_fpsText_43() const { return ___fpsText_43; }
	inline String_t** get_address_of_fpsText_43() { return &___fpsText_43; }
	inline void set_fpsText_43(String_t* value)
	{
		___fpsText_43 = value;
		Il2CppCodeGenWriteBarrier((&___fpsText_43), value);
	}

	inline static int32_t get_offset_of_currentView_44() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___currentView_44)); }
	inline int32_t get_currentView_44() const { return ___currentView_44; }
	inline int32_t* get_address_of_currentView_44() { return &___currentView_44; }
	inline void set_currentView_44(int32_t value)
	{
		___currentView_44 = value;
	}

	inline static int32_t get_offset_of_images_46() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___images_46)); }
	inline Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC * get_images_46() const { return ___images_46; }
	inline Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC ** get_address_of_images_46() { return &___images_46; }
	inline void set_images_46(Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC * value)
	{
		___images_46 = value;
		Il2CppCodeGenWriteBarrier((&___images_46), value);
	}

	inline static int32_t get_offset_of_clearContent_47() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___clearContent_47)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_clearContent_47() const { return ___clearContent_47; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_clearContent_47() { return &___clearContent_47; }
	inline void set_clearContent_47(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___clearContent_47 = value;
		Il2CppCodeGenWriteBarrier((&___clearContent_47), value);
	}

	inline static int32_t get_offset_of_collapseContent_48() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___collapseContent_48)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_collapseContent_48() const { return ___collapseContent_48; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_collapseContent_48() { return &___collapseContent_48; }
	inline void set_collapseContent_48(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___collapseContent_48 = value;
		Il2CppCodeGenWriteBarrier((&___collapseContent_48), value);
	}

	inline static int32_t get_offset_of_clearOnNewSceneContent_49() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___clearOnNewSceneContent_49)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_clearOnNewSceneContent_49() const { return ___clearOnNewSceneContent_49; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_clearOnNewSceneContent_49() { return &___clearOnNewSceneContent_49; }
	inline void set_clearOnNewSceneContent_49(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___clearOnNewSceneContent_49 = value;
		Il2CppCodeGenWriteBarrier((&___clearOnNewSceneContent_49), value);
	}

	inline static int32_t get_offset_of_showTimeContent_50() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showTimeContent_50)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_showTimeContent_50() const { return ___showTimeContent_50; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_showTimeContent_50() { return &___showTimeContent_50; }
	inline void set_showTimeContent_50(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___showTimeContent_50 = value;
		Il2CppCodeGenWriteBarrier((&___showTimeContent_50), value);
	}

	inline static int32_t get_offset_of_showSceneContent_51() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showSceneContent_51)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_showSceneContent_51() const { return ___showSceneContent_51; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_showSceneContent_51() { return &___showSceneContent_51; }
	inline void set_showSceneContent_51(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___showSceneContent_51 = value;
		Il2CppCodeGenWriteBarrier((&___showSceneContent_51), value);
	}

	inline static int32_t get_offset_of_userContent_52() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___userContent_52)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_userContent_52() const { return ___userContent_52; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_userContent_52() { return &___userContent_52; }
	inline void set_userContent_52(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___userContent_52 = value;
		Il2CppCodeGenWriteBarrier((&___userContent_52), value);
	}

	inline static int32_t get_offset_of_showMemoryContent_53() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showMemoryContent_53)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_showMemoryContent_53() const { return ___showMemoryContent_53; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_showMemoryContent_53() { return &___showMemoryContent_53; }
	inline void set_showMemoryContent_53(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___showMemoryContent_53 = value;
		Il2CppCodeGenWriteBarrier((&___showMemoryContent_53), value);
	}

	inline static int32_t get_offset_of_softwareContent_54() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___softwareContent_54)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_softwareContent_54() const { return ___softwareContent_54; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_softwareContent_54() { return &___softwareContent_54; }
	inline void set_softwareContent_54(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___softwareContent_54 = value;
		Il2CppCodeGenWriteBarrier((&___softwareContent_54), value);
	}

	inline static int32_t get_offset_of_dateContent_55() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___dateContent_55)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_dateContent_55() const { return ___dateContent_55; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_dateContent_55() { return &___dateContent_55; }
	inline void set_dateContent_55(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___dateContent_55 = value;
		Il2CppCodeGenWriteBarrier((&___dateContent_55), value);
	}

	inline static int32_t get_offset_of_showFpsContent_56() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___showFpsContent_56)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_showFpsContent_56() const { return ___showFpsContent_56; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_showFpsContent_56() { return &___showFpsContent_56; }
	inline void set_showFpsContent_56(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___showFpsContent_56 = value;
		Il2CppCodeGenWriteBarrier((&___showFpsContent_56), value);
	}

	inline static int32_t get_offset_of_infoContent_57() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___infoContent_57)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_infoContent_57() const { return ___infoContent_57; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_infoContent_57() { return &___infoContent_57; }
	inline void set_infoContent_57(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___infoContent_57 = value;
		Il2CppCodeGenWriteBarrier((&___infoContent_57), value);
	}

	inline static int32_t get_offset_of_saveLogsContent_58() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___saveLogsContent_58)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_saveLogsContent_58() const { return ___saveLogsContent_58; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_saveLogsContent_58() { return &___saveLogsContent_58; }
	inline void set_saveLogsContent_58(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___saveLogsContent_58 = value;
		Il2CppCodeGenWriteBarrier((&___saveLogsContent_58), value);
	}

	inline static int32_t get_offset_of_searchContent_59() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___searchContent_59)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_searchContent_59() const { return ___searchContent_59; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_searchContent_59() { return &___searchContent_59; }
	inline void set_searchContent_59(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___searchContent_59 = value;
		Il2CppCodeGenWriteBarrier((&___searchContent_59), value);
	}

	inline static int32_t get_offset_of_copyContent_60() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___copyContent_60)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_copyContent_60() const { return ___copyContent_60; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_copyContent_60() { return &___copyContent_60; }
	inline void set_copyContent_60(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___copyContent_60 = value;
		Il2CppCodeGenWriteBarrier((&___copyContent_60), value);
	}

	inline static int32_t get_offset_of_closeContent_61() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___closeContent_61)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_closeContent_61() const { return ___closeContent_61; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_closeContent_61() { return &___closeContent_61; }
	inline void set_closeContent_61(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___closeContent_61 = value;
		Il2CppCodeGenWriteBarrier((&___closeContent_61), value);
	}

	inline static int32_t get_offset_of_buildFromContent_62() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___buildFromContent_62)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_buildFromContent_62() const { return ___buildFromContent_62; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_buildFromContent_62() { return &___buildFromContent_62; }
	inline void set_buildFromContent_62(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___buildFromContent_62 = value;
		Il2CppCodeGenWriteBarrier((&___buildFromContent_62), value);
	}

	inline static int32_t get_offset_of_systemInfoContent_63() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___systemInfoContent_63)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_systemInfoContent_63() const { return ___systemInfoContent_63; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_systemInfoContent_63() { return &___systemInfoContent_63; }
	inline void set_systemInfoContent_63(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___systemInfoContent_63 = value;
		Il2CppCodeGenWriteBarrier((&___systemInfoContent_63), value);
	}

	inline static int32_t get_offset_of_graphicsInfoContent_64() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___graphicsInfoContent_64)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_graphicsInfoContent_64() const { return ___graphicsInfoContent_64; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_graphicsInfoContent_64() { return &___graphicsInfoContent_64; }
	inline void set_graphicsInfoContent_64(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___graphicsInfoContent_64 = value;
		Il2CppCodeGenWriteBarrier((&___graphicsInfoContent_64), value);
	}

	inline static int32_t get_offset_of_backContent_65() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___backContent_65)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_backContent_65() const { return ___backContent_65; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_backContent_65() { return &___backContent_65; }
	inline void set_backContent_65(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___backContent_65 = value;
		Il2CppCodeGenWriteBarrier((&___backContent_65), value);
	}

	inline static int32_t get_offset_of_logContent_66() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___logContent_66)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_logContent_66() const { return ___logContent_66; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_logContent_66() { return &___logContent_66; }
	inline void set_logContent_66(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___logContent_66 = value;
		Il2CppCodeGenWriteBarrier((&___logContent_66), value);
	}

	inline static int32_t get_offset_of_warningContent_67() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___warningContent_67)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_warningContent_67() const { return ___warningContent_67; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_warningContent_67() { return &___warningContent_67; }
	inline void set_warningContent_67(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___warningContent_67 = value;
		Il2CppCodeGenWriteBarrier((&___warningContent_67), value);
	}

	inline static int32_t get_offset_of_errorContent_68() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___errorContent_68)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_errorContent_68() const { return ___errorContent_68; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_errorContent_68() { return &___errorContent_68; }
	inline void set_errorContent_68(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___errorContent_68 = value;
		Il2CppCodeGenWriteBarrier((&___errorContent_68), value);
	}

	inline static int32_t get_offset_of_barStyle_69() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___barStyle_69)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_barStyle_69() const { return ___barStyle_69; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_barStyle_69() { return &___barStyle_69; }
	inline void set_barStyle_69(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___barStyle_69 = value;
		Il2CppCodeGenWriteBarrier((&___barStyle_69), value);
	}

	inline static int32_t get_offset_of_buttonActiveStyle_70() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___buttonActiveStyle_70)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_buttonActiveStyle_70() const { return ___buttonActiveStyle_70; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_buttonActiveStyle_70() { return &___buttonActiveStyle_70; }
	inline void set_buttonActiveStyle_70(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___buttonActiveStyle_70 = value;
		Il2CppCodeGenWriteBarrier((&___buttonActiveStyle_70), value);
	}

	inline static int32_t get_offset_of_nonStyle_71() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___nonStyle_71)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_nonStyle_71() const { return ___nonStyle_71; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_nonStyle_71() { return &___nonStyle_71; }
	inline void set_nonStyle_71(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___nonStyle_71 = value;
		Il2CppCodeGenWriteBarrier((&___nonStyle_71), value);
	}

	inline static int32_t get_offset_of_lowerLeftFontStyle_72() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___lowerLeftFontStyle_72)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_lowerLeftFontStyle_72() const { return ___lowerLeftFontStyle_72; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_lowerLeftFontStyle_72() { return &___lowerLeftFontStyle_72; }
	inline void set_lowerLeftFontStyle_72(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___lowerLeftFontStyle_72 = value;
		Il2CppCodeGenWriteBarrier((&___lowerLeftFontStyle_72), value);
	}

	inline static int32_t get_offset_of_backStyle_73() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___backStyle_73)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_backStyle_73() const { return ___backStyle_73; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_backStyle_73() { return &___backStyle_73; }
	inline void set_backStyle_73(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___backStyle_73 = value;
		Il2CppCodeGenWriteBarrier((&___backStyle_73), value);
	}

	inline static int32_t get_offset_of_evenLogStyle_74() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___evenLogStyle_74)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_evenLogStyle_74() const { return ___evenLogStyle_74; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_evenLogStyle_74() { return &___evenLogStyle_74; }
	inline void set_evenLogStyle_74(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___evenLogStyle_74 = value;
		Il2CppCodeGenWriteBarrier((&___evenLogStyle_74), value);
	}

	inline static int32_t get_offset_of_oddLogStyle_75() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___oddLogStyle_75)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_oddLogStyle_75() const { return ___oddLogStyle_75; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_oddLogStyle_75() { return &___oddLogStyle_75; }
	inline void set_oddLogStyle_75(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___oddLogStyle_75 = value;
		Il2CppCodeGenWriteBarrier((&___oddLogStyle_75), value);
	}

	inline static int32_t get_offset_of_logButtonStyle_76() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___logButtonStyle_76)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_logButtonStyle_76() const { return ___logButtonStyle_76; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_logButtonStyle_76() { return &___logButtonStyle_76; }
	inline void set_logButtonStyle_76(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___logButtonStyle_76 = value;
		Il2CppCodeGenWriteBarrier((&___logButtonStyle_76), value);
	}

	inline static int32_t get_offset_of_selectedLogStyle_77() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___selectedLogStyle_77)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_selectedLogStyle_77() const { return ___selectedLogStyle_77; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_selectedLogStyle_77() { return &___selectedLogStyle_77; }
	inline void set_selectedLogStyle_77(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___selectedLogStyle_77 = value;
		Il2CppCodeGenWriteBarrier((&___selectedLogStyle_77), value);
	}

	inline static int32_t get_offset_of_selectedLogFontStyle_78() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___selectedLogFontStyle_78)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_selectedLogFontStyle_78() const { return ___selectedLogFontStyle_78; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_selectedLogFontStyle_78() { return &___selectedLogFontStyle_78; }
	inline void set_selectedLogFontStyle_78(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___selectedLogFontStyle_78 = value;
		Il2CppCodeGenWriteBarrier((&___selectedLogFontStyle_78), value);
	}

	inline static int32_t get_offset_of_stackLabelStyle_79() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___stackLabelStyle_79)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_stackLabelStyle_79() const { return ___stackLabelStyle_79; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_stackLabelStyle_79() { return &___stackLabelStyle_79; }
	inline void set_stackLabelStyle_79(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___stackLabelStyle_79 = value;
		Il2CppCodeGenWriteBarrier((&___stackLabelStyle_79), value);
	}

	inline static int32_t get_offset_of_scrollerStyle_80() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___scrollerStyle_80)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_scrollerStyle_80() const { return ___scrollerStyle_80; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_scrollerStyle_80() { return &___scrollerStyle_80; }
	inline void set_scrollerStyle_80(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___scrollerStyle_80 = value;
		Il2CppCodeGenWriteBarrier((&___scrollerStyle_80), value);
	}

	inline static int32_t get_offset_of_searchStyle_81() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___searchStyle_81)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_searchStyle_81() const { return ___searchStyle_81; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_searchStyle_81() { return &___searchStyle_81; }
	inline void set_searchStyle_81(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___searchStyle_81 = value;
		Il2CppCodeGenWriteBarrier((&___searchStyle_81), value);
	}

	inline static int32_t get_offset_of_sliderBackStyle_82() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___sliderBackStyle_82)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_sliderBackStyle_82() const { return ___sliderBackStyle_82; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_sliderBackStyle_82() { return &___sliderBackStyle_82; }
	inline void set_sliderBackStyle_82(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___sliderBackStyle_82 = value;
		Il2CppCodeGenWriteBarrier((&___sliderBackStyle_82), value);
	}

	inline static int32_t get_offset_of_sliderThumbStyle_83() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___sliderThumbStyle_83)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_sliderThumbStyle_83() const { return ___sliderThumbStyle_83; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_sliderThumbStyle_83() { return &___sliderThumbStyle_83; }
	inline void set_sliderThumbStyle_83(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___sliderThumbStyle_83 = value;
		Il2CppCodeGenWriteBarrier((&___sliderThumbStyle_83), value);
	}

	inline static int32_t get_offset_of_toolbarScrollerSkin_84() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___toolbarScrollerSkin_84)); }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * get_toolbarScrollerSkin_84() const { return ___toolbarScrollerSkin_84; }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 ** get_address_of_toolbarScrollerSkin_84() { return &___toolbarScrollerSkin_84; }
	inline void set_toolbarScrollerSkin_84(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * value)
	{
		___toolbarScrollerSkin_84 = value;
		Il2CppCodeGenWriteBarrier((&___toolbarScrollerSkin_84), value);
	}

	inline static int32_t get_offset_of_logScrollerSkin_85() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___logScrollerSkin_85)); }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * get_logScrollerSkin_85() const { return ___logScrollerSkin_85; }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 ** get_address_of_logScrollerSkin_85() { return &___logScrollerSkin_85; }
	inline void set_logScrollerSkin_85(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * value)
	{
		___logScrollerSkin_85 = value;
		Il2CppCodeGenWriteBarrier((&___logScrollerSkin_85), value);
	}

	inline static int32_t get_offset_of_graphScrollerSkin_86() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___graphScrollerSkin_86)); }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * get_graphScrollerSkin_86() const { return ___graphScrollerSkin_86; }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 ** get_address_of_graphScrollerSkin_86() { return &___graphScrollerSkin_86; }
	inline void set_graphScrollerSkin_86(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * value)
	{
		___graphScrollerSkin_86 = value;
		Il2CppCodeGenWriteBarrier((&___graphScrollerSkin_86), value);
	}

	inline static int32_t get_offset_of_size_87() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___size_87)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_size_87() const { return ___size_87; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_size_87() { return &___size_87; }
	inline void set_size_87(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___size_87 = value;
	}

	inline static int32_t get_offset_of_maxSize_88() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___maxSize_88)); }
	inline float get_maxSize_88() const { return ___maxSize_88; }
	inline float* get_address_of_maxSize_88() { return &___maxSize_88; }
	inline void set_maxSize_88(float value)
	{
		___maxSize_88 = value;
	}

	inline static int32_t get_offset_of_numOfCircleToShow_89() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___numOfCircleToShow_89)); }
	inline int32_t get_numOfCircleToShow_89() const { return ___numOfCircleToShow_89; }
	inline int32_t* get_address_of_numOfCircleToShow_89() { return &___numOfCircleToShow_89; }
	inline void set_numOfCircleToShow_89(int32_t value)
	{
		___numOfCircleToShow_89 = value;
	}

	inline static int32_t get_offset_of_currentScene_91() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___currentScene_91)); }
	inline String_t* get_currentScene_91() const { return ___currentScene_91; }
	inline String_t** get_address_of_currentScene_91() { return &___currentScene_91; }
	inline void set_currentScene_91(String_t* value)
	{
		___currentScene_91 = value;
		Il2CppCodeGenWriteBarrier((&___currentScene_91), value);
	}

	inline static int32_t get_offset_of_filterText_92() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___filterText_92)); }
	inline String_t* get_filterText_92() const { return ___filterText_92; }
	inline String_t** get_address_of_filterText_92() { return &___filterText_92; }
	inline void set_filterText_92(String_t* value)
	{
		___filterText_92 = value;
		Il2CppCodeGenWriteBarrier((&___filterText_92), value);
	}

	inline static int32_t get_offset_of_deviceModel_93() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___deviceModel_93)); }
	inline String_t* get_deviceModel_93() const { return ___deviceModel_93; }
	inline String_t** get_address_of_deviceModel_93() { return &___deviceModel_93; }
	inline void set_deviceModel_93(String_t* value)
	{
		___deviceModel_93 = value;
		Il2CppCodeGenWriteBarrier((&___deviceModel_93), value);
	}

	inline static int32_t get_offset_of_deviceType_94() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___deviceType_94)); }
	inline String_t* get_deviceType_94() const { return ___deviceType_94; }
	inline String_t** get_address_of_deviceType_94() { return &___deviceType_94; }
	inline void set_deviceType_94(String_t* value)
	{
		___deviceType_94 = value;
		Il2CppCodeGenWriteBarrier((&___deviceType_94), value);
	}

	inline static int32_t get_offset_of_deviceName_95() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___deviceName_95)); }
	inline String_t* get_deviceName_95() const { return ___deviceName_95; }
	inline String_t** get_address_of_deviceName_95() { return &___deviceName_95; }
	inline void set_deviceName_95(String_t* value)
	{
		___deviceName_95 = value;
		Il2CppCodeGenWriteBarrier((&___deviceName_95), value);
	}

	inline static int32_t get_offset_of_graphicsMemorySize_96() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___graphicsMemorySize_96)); }
	inline String_t* get_graphicsMemorySize_96() const { return ___graphicsMemorySize_96; }
	inline String_t** get_address_of_graphicsMemorySize_96() { return &___graphicsMemorySize_96; }
	inline void set_graphicsMemorySize_96(String_t* value)
	{
		___graphicsMemorySize_96 = value;
		Il2CppCodeGenWriteBarrier((&___graphicsMemorySize_96), value);
	}

	inline static int32_t get_offset_of_maxTextureSize_97() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___maxTextureSize_97)); }
	inline String_t* get_maxTextureSize_97() const { return ___maxTextureSize_97; }
	inline String_t** get_address_of_maxTextureSize_97() { return &___maxTextureSize_97; }
	inline void set_maxTextureSize_97(String_t* value)
	{
		___maxTextureSize_97 = value;
		Il2CppCodeGenWriteBarrier((&___maxTextureSize_97), value);
	}

	inline static int32_t get_offset_of_systemMemorySize_98() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___systemMemorySize_98)); }
	inline String_t* get_systemMemorySize_98() const { return ___systemMemorySize_98; }
	inline String_t** get_address_of_systemMemorySize_98() { return &___systemMemorySize_98; }
	inline void set_systemMemorySize_98(String_t* value)
	{
		___systemMemorySize_98 = value;
		Il2CppCodeGenWriteBarrier((&___systemMemorySize_98), value);
	}

	inline static int32_t get_offset_of_Initialized_99() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___Initialized_99)); }
	inline bool get_Initialized_99() const { return ___Initialized_99; }
	inline bool* get_address_of_Initialized_99() { return &___Initialized_99; }
	inline void set_Initialized_99(bool value)
	{
		___Initialized_99 = value;
	}

	inline static int32_t get_offset_of_screenRect_100() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___screenRect_100)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_screenRect_100() const { return ___screenRect_100; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_screenRect_100() { return &___screenRect_100; }
	inline void set_screenRect_100(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___screenRect_100 = value;
	}

	inline static int32_t get_offset_of_toolBarRect_101() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___toolBarRect_101)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_toolBarRect_101() const { return ___toolBarRect_101; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_toolBarRect_101() { return &___toolBarRect_101; }
	inline void set_toolBarRect_101(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___toolBarRect_101 = value;
	}

	inline static int32_t get_offset_of_logsRect_102() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___logsRect_102)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_logsRect_102() const { return ___logsRect_102; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_logsRect_102() { return &___logsRect_102; }
	inline void set_logsRect_102(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___logsRect_102 = value;
	}

	inline static int32_t get_offset_of_stackRect_103() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___stackRect_103)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_stackRect_103() const { return ___stackRect_103; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_stackRect_103() { return &___stackRect_103; }
	inline void set_stackRect_103(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___stackRect_103 = value;
	}

	inline static int32_t get_offset_of_graphRect_104() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___graphRect_104)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_graphRect_104() const { return ___graphRect_104; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_graphRect_104() { return &___graphRect_104; }
	inline void set_graphRect_104(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___graphRect_104 = value;
	}

	inline static int32_t get_offset_of_graphMinRect_105() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___graphMinRect_105)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_graphMinRect_105() const { return ___graphMinRect_105; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_graphMinRect_105() { return &___graphMinRect_105; }
	inline void set_graphMinRect_105(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___graphMinRect_105 = value;
	}

	inline static int32_t get_offset_of_graphMaxRect_106() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___graphMaxRect_106)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_graphMaxRect_106() const { return ___graphMaxRect_106; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_graphMaxRect_106() { return &___graphMaxRect_106; }
	inline void set_graphMaxRect_106(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___graphMaxRect_106 = value;
	}

	inline static int32_t get_offset_of_buttomRect_107() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___buttomRect_107)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_buttomRect_107() const { return ___buttomRect_107; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_buttomRect_107() { return &___buttomRect_107; }
	inline void set_buttomRect_107(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___buttomRect_107 = value;
	}

	inline static int32_t get_offset_of_stackRectTopLeft_108() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___stackRectTopLeft_108)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_stackRectTopLeft_108() const { return ___stackRectTopLeft_108; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_stackRectTopLeft_108() { return &___stackRectTopLeft_108; }
	inline void set_stackRectTopLeft_108(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___stackRectTopLeft_108 = value;
	}

	inline static int32_t get_offset_of_detailRect_109() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___detailRect_109)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_detailRect_109() const { return ___detailRect_109; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_detailRect_109() { return &___detailRect_109; }
	inline void set_detailRect_109(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___detailRect_109 = value;
	}

	inline static int32_t get_offset_of_scrollPosition_110() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___scrollPosition_110)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_scrollPosition_110() const { return ___scrollPosition_110; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_scrollPosition_110() { return &___scrollPosition_110; }
	inline void set_scrollPosition_110(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___scrollPosition_110 = value;
	}

	inline static int32_t get_offset_of_scrollPosition2_111() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___scrollPosition2_111)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_scrollPosition2_111() const { return ___scrollPosition2_111; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_scrollPosition2_111() { return &___scrollPosition2_111; }
	inline void set_scrollPosition2_111(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___scrollPosition2_111 = value;
	}

	inline static int32_t get_offset_of_toolbarScrollPosition_112() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___toolbarScrollPosition_112)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_toolbarScrollPosition_112() const { return ___toolbarScrollPosition_112; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_toolbarScrollPosition_112() { return &___toolbarScrollPosition_112; }
	inline void set_toolbarScrollPosition_112(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___toolbarScrollPosition_112 = value;
	}

	inline static int32_t get_offset_of_selectedLog_113() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___selectedLog_113)); }
	inline Log_t152F87153D078D3D99151CF14D91D518B21E3E22 * get_selectedLog_113() const { return ___selectedLog_113; }
	inline Log_t152F87153D078D3D99151CF14D91D518B21E3E22 ** get_address_of_selectedLog_113() { return &___selectedLog_113; }
	inline void set_selectedLog_113(Log_t152F87153D078D3D99151CF14D91D518B21E3E22 * value)
	{
		___selectedLog_113 = value;
		Il2CppCodeGenWriteBarrier((&___selectedLog_113), value);
	}

	inline static int32_t get_offset_of_toolbarOldDrag_114() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___toolbarOldDrag_114)); }
	inline float get_toolbarOldDrag_114() const { return ___toolbarOldDrag_114; }
	inline float* get_address_of_toolbarOldDrag_114() { return &___toolbarOldDrag_114; }
	inline void set_toolbarOldDrag_114(float value)
	{
		___toolbarOldDrag_114 = value;
	}

	inline static int32_t get_offset_of_oldDrag_115() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___oldDrag_115)); }
	inline float get_oldDrag_115() const { return ___oldDrag_115; }
	inline float* get_address_of_oldDrag_115() { return &___oldDrag_115; }
	inline void set_oldDrag_115(float value)
	{
		___oldDrag_115 = value;
	}

	inline static int32_t get_offset_of_oldDrag2_116() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___oldDrag2_116)); }
	inline float get_oldDrag2_116() const { return ___oldDrag2_116; }
	inline float* get_address_of_oldDrag2_116() { return &___oldDrag2_116; }
	inline void set_oldDrag2_116(float value)
	{
		___oldDrag2_116 = value;
	}

	inline static int32_t get_offset_of_oldDrag3_117() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___oldDrag3_117)); }
	inline float get_oldDrag3_117() const { return ___oldDrag3_117; }
	inline float* get_address_of_oldDrag3_117() { return &___oldDrag3_117; }
	inline void set_oldDrag3_117(float value)
	{
		___oldDrag3_117 = value;
	}

	inline static int32_t get_offset_of_startIndex_118() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___startIndex_118)); }
	inline int32_t get_startIndex_118() const { return ___startIndex_118; }
	inline int32_t* get_address_of_startIndex_118() { return &___startIndex_118; }
	inline void set_startIndex_118(int32_t value)
	{
		___startIndex_118 = value;
	}

	inline static int32_t get_offset_of_countRect_119() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___countRect_119)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_countRect_119() const { return ___countRect_119; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_countRect_119() { return &___countRect_119; }
	inline void set_countRect_119(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___countRect_119 = value;
	}

	inline static int32_t get_offset_of_timeRect_120() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___timeRect_120)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_timeRect_120() const { return ___timeRect_120; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_timeRect_120() { return &___timeRect_120; }
	inline void set_timeRect_120(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___timeRect_120 = value;
	}

	inline static int32_t get_offset_of_timeLabelRect_121() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___timeLabelRect_121)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_timeLabelRect_121() const { return ___timeLabelRect_121; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_timeLabelRect_121() { return &___timeLabelRect_121; }
	inline void set_timeLabelRect_121(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___timeLabelRect_121 = value;
	}

	inline static int32_t get_offset_of_sceneRect_122() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___sceneRect_122)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_sceneRect_122() const { return ___sceneRect_122; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_sceneRect_122() { return &___sceneRect_122; }
	inline void set_sceneRect_122(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___sceneRect_122 = value;
	}

	inline static int32_t get_offset_of_sceneLabelRect_123() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___sceneLabelRect_123)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_sceneLabelRect_123() const { return ___sceneLabelRect_123; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_sceneLabelRect_123() { return &___sceneLabelRect_123; }
	inline void set_sceneLabelRect_123(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___sceneLabelRect_123 = value;
	}

	inline static int32_t get_offset_of_memoryRect_124() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___memoryRect_124)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_memoryRect_124() const { return ___memoryRect_124; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_memoryRect_124() { return &___memoryRect_124; }
	inline void set_memoryRect_124(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___memoryRect_124 = value;
	}

	inline static int32_t get_offset_of_memoryLabelRect_125() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___memoryLabelRect_125)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_memoryLabelRect_125() const { return ___memoryLabelRect_125; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_memoryLabelRect_125() { return &___memoryLabelRect_125; }
	inline void set_memoryLabelRect_125(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___memoryLabelRect_125 = value;
	}

	inline static int32_t get_offset_of_fpsRect_126() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___fpsRect_126)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_fpsRect_126() const { return ___fpsRect_126; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_fpsRect_126() { return &___fpsRect_126; }
	inline void set_fpsRect_126(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___fpsRect_126 = value;
	}

	inline static int32_t get_offset_of_fpsLabelRect_127() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___fpsLabelRect_127)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_fpsLabelRect_127() const { return ___fpsLabelRect_127; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_fpsLabelRect_127() { return &___fpsLabelRect_127; }
	inline void set_fpsLabelRect_127(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___fpsLabelRect_127 = value;
	}

	inline static int32_t get_offset_of_tempContent_128() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___tempContent_128)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_tempContent_128() const { return ___tempContent_128; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_tempContent_128() { return &___tempContent_128; }
	inline void set_tempContent_128(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___tempContent_128 = value;
		Il2CppCodeGenWriteBarrier((&___tempContent_128), value);
	}

	inline static int32_t get_offset_of_infoScrollPosition_129() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___infoScrollPosition_129)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_infoScrollPosition_129() const { return ___infoScrollPosition_129; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_infoScrollPosition_129() { return &___infoScrollPosition_129; }
	inline void set_infoScrollPosition_129(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___infoScrollPosition_129 = value;
	}

	inline static int32_t get_offset_of_oldInfoDrag_130() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___oldInfoDrag_130)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oldInfoDrag_130() const { return ___oldInfoDrag_130; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oldInfoDrag_130() { return &___oldInfoDrag_130; }
	inline void set_oldInfoDrag_130(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oldInfoDrag_130 = value;
	}

	inline static int32_t get_offset_of_tempRect_131() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___tempRect_131)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_tempRect_131() const { return ___tempRect_131; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_tempRect_131() { return &___tempRect_131; }
	inline void set_tempRect_131(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___tempRect_131 = value;
	}

	inline static int32_t get_offset_of_graphSize_132() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___graphSize_132)); }
	inline float get_graphSize_132() const { return ___graphSize_132; }
	inline float* get_address_of_graphSize_132() { return &___graphSize_132; }
	inline void set_graphSize_132(float value)
	{
		___graphSize_132 = value;
	}

	inline static int32_t get_offset_of_startFrame_133() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___startFrame_133)); }
	inline int32_t get_startFrame_133() const { return ___startFrame_133; }
	inline int32_t* get_address_of_startFrame_133() { return &___startFrame_133; }
	inline void set_startFrame_133(int32_t value)
	{
		___startFrame_133 = value;
	}

	inline static int32_t get_offset_of_currentFrame_134() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___currentFrame_134)); }
	inline int32_t get_currentFrame_134() const { return ___currentFrame_134; }
	inline int32_t* get_address_of_currentFrame_134() { return &___currentFrame_134; }
	inline void set_currentFrame_134(int32_t value)
	{
		___currentFrame_134 = value;
	}

	inline static int32_t get_offset_of_tempVector1_135() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___tempVector1_135)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_tempVector1_135() const { return ___tempVector1_135; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_tempVector1_135() { return &___tempVector1_135; }
	inline void set_tempVector1_135(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___tempVector1_135 = value;
	}

	inline static int32_t get_offset_of_tempVector2_136() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___tempVector2_136)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_tempVector2_136() const { return ___tempVector2_136; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_tempVector2_136() { return &___tempVector2_136; }
	inline void set_tempVector2_136(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___tempVector2_136 = value;
	}

	inline static int32_t get_offset_of_graphScrollerPos_137() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___graphScrollerPos_137)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_graphScrollerPos_137() const { return ___graphScrollerPos_137; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_graphScrollerPos_137() { return &___graphScrollerPos_137; }
	inline void set_graphScrollerPos_137(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___graphScrollerPos_137 = value;
	}

	inline static int32_t get_offset_of_maxFpsValue_138() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___maxFpsValue_138)); }
	inline float get_maxFpsValue_138() const { return ___maxFpsValue_138; }
	inline float* get_address_of_maxFpsValue_138() { return &___maxFpsValue_138; }
	inline void set_maxFpsValue_138(float value)
	{
		___maxFpsValue_138 = value;
	}

	inline static int32_t get_offset_of_minFpsValue_139() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___minFpsValue_139)); }
	inline float get_minFpsValue_139() const { return ___minFpsValue_139; }
	inline float* get_address_of_minFpsValue_139() { return &___minFpsValue_139; }
	inline void set_minFpsValue_139(float value)
	{
		___minFpsValue_139 = value;
	}

	inline static int32_t get_offset_of_maxMemoryValue_140() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___maxMemoryValue_140)); }
	inline float get_maxMemoryValue_140() const { return ___maxMemoryValue_140; }
	inline float* get_address_of_maxMemoryValue_140() { return &___maxMemoryValue_140; }
	inline void set_maxMemoryValue_140(float value)
	{
		___maxMemoryValue_140 = value;
	}

	inline static int32_t get_offset_of_minMemoryValue_141() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___minMemoryValue_141)); }
	inline float get_minMemoryValue_141() const { return ___minMemoryValue_141; }
	inline float* get_address_of_minMemoryValue_141() { return &___minMemoryValue_141; }
	inline void set_minMemoryValue_141(float value)
	{
		___minMemoryValue_141 = value;
	}

	inline static int32_t get_offset_of_gestureDetector_142() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___gestureDetector_142)); }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * get_gestureDetector_142() const { return ___gestureDetector_142; }
	inline List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB ** get_address_of_gestureDetector_142() { return &___gestureDetector_142; }
	inline void set_gestureDetector_142(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * value)
	{
		___gestureDetector_142 = value;
		Il2CppCodeGenWriteBarrier((&___gestureDetector_142), value);
	}

	inline static int32_t get_offset_of_gestureSum_143() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___gestureSum_143)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_gestureSum_143() const { return ___gestureSum_143; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_gestureSum_143() { return &___gestureSum_143; }
	inline void set_gestureSum_143(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___gestureSum_143 = value;
	}

	inline static int32_t get_offset_of_gestureLength_144() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___gestureLength_144)); }
	inline float get_gestureLength_144() const { return ___gestureLength_144; }
	inline float* get_address_of_gestureLength_144() { return &___gestureLength_144; }
	inline void set_gestureLength_144(float value)
	{
		___gestureLength_144 = value;
	}

	inline static int32_t get_offset_of_gestureCount_145() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___gestureCount_145)); }
	inline int32_t get_gestureCount_145() const { return ___gestureCount_145; }
	inline int32_t* get_address_of_gestureCount_145() { return &___gestureCount_145; }
	inline void set_gestureCount_145(int32_t value)
	{
		___gestureCount_145 = value;
	}

	inline static int32_t get_offset_of_lastClickTime_146() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___lastClickTime_146)); }
	inline float get_lastClickTime_146() const { return ___lastClickTime_146; }
	inline float* get_address_of_lastClickTime_146() { return &___lastClickTime_146; }
	inline void set_lastClickTime_146(float value)
	{
		___lastClickTime_146 = value;
	}

	inline static int32_t get_offset_of_startPos_147() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___startPos_147)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_startPos_147() const { return ___startPos_147; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_startPos_147() { return &___startPos_147; }
	inline void set_startPos_147(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___startPos_147 = value;
	}

	inline static int32_t get_offset_of_downPos_148() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___downPos_148)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downPos_148() const { return ___downPos_148; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downPos_148() { return &___downPos_148; }
	inline void set_downPos_148(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downPos_148 = value;
	}

	inline static int32_t get_offset_of_mousePosition_149() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___mousePosition_149)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_mousePosition_149() const { return ___mousePosition_149; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_mousePosition_149() { return &___mousePosition_149; }
	inline void set_mousePosition_149(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___mousePosition_149 = value;
	}

	inline static int32_t get_offset_of_frames_150() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___frames_150)); }
	inline int32_t get_frames_150() const { return ___frames_150; }
	inline int32_t* get_address_of_frames_150() { return &___frames_150; }
	inline void set_frames_150(int32_t value)
	{
		___frames_150 = value;
	}

	inline static int32_t get_offset_of_firstTime_151() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___firstTime_151)); }
	inline bool get_firstTime_151() const { return ___firstTime_151; }
	inline bool* get_address_of_firstTime_151() { return &___firstTime_151; }
	inline void set_firstTime_151(bool value)
	{
		___firstTime_151 = value;
	}

	inline static int32_t get_offset_of_lastUpdate_152() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___lastUpdate_152)); }
	inline float get_lastUpdate_152() const { return ___lastUpdate_152; }
	inline float* get_address_of_lastUpdate_152() { return &___lastUpdate_152; }
	inline void set_lastUpdate_152(float value)
	{
		___lastUpdate_152 = value;
	}

	inline static int32_t get_offset_of_threadedLogs_155() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E, ___threadedLogs_155)); }
	inline List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 * get_threadedLogs_155() const { return ___threadedLogs_155; }
	inline List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 ** get_address_of_threadedLogs_155() { return &___threadedLogs_155; }
	inline void set_threadedLogs_155(List_1_t571010E716A1DF35D4D694A0FB92FCF347968814 * value)
	{
		___threadedLogs_155 = value;
		Il2CppCodeGenWriteBarrier((&___threadedLogs_155), value);
	}
};

struct Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E_StaticFields
{
public:
	// Reporter Reporter::Instance
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * ___Instance_4;
	// System.Boolean Reporter::created
	bool ___created_45;
	// System.String[] Reporter::scenes
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___scenes_90;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E_StaticFields, ___Instance_4)); }
	inline Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * get_Instance_4() const { return ___Instance_4; }
	inline Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}

	inline static int32_t get_offset_of_created_45() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E_StaticFields, ___created_45)); }
	inline bool get_created_45() const { return ___created_45; }
	inline bool* get_address_of_created_45() { return &___created_45; }
	inline void set_created_45(bool value)
	{
		___created_45 = value;
	}

	inline static int32_t get_offset_of_scenes_90() { return static_cast<int32_t>(offsetof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E_StaticFields, ___scenes_90)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_scenes_90() const { return ___scenes_90; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_scenes_90() { return &___scenes_90; }
	inline void set_scenes_90(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___scenes_90 = value;
		Il2CppCodeGenWriteBarrier((&___scenes_90), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPORTER_TFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E_H
#ifndef REPORTERGUI_T3748A1828F0F187695BA4A6B781D22E5DCBDF8DE_H
#define REPORTERGUI_T3748A1828F0F187695BA4A6B781D22E5DCBDF8DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReporterGUI
struct  ReporterGUI_t3748A1828F0F187695BA4A6B781D22E5DCBDF8DE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Reporter ReporterGUI::reporter
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * ___reporter_4;

public:
	inline static int32_t get_offset_of_reporter_4() { return static_cast<int32_t>(offsetof(ReporterGUI_t3748A1828F0F187695BA4A6B781D22E5DCBDF8DE, ___reporter_4)); }
	inline Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * get_reporter_4() const { return ___reporter_4; }
	inline Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E ** get_address_of_reporter_4() { return &___reporter_4; }
	inline void set_reporter_4(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * value)
	{
		___reporter_4 = value;
		Il2CppCodeGenWriteBarrier((&___reporter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPORTERGUI_T3748A1828F0F187695BA4A6B781D22E5DCBDF8DE_H
#ifndef REPORTERMESSAGERECEIVER_TABD7DD7411DF8F7B66D42A50DCD5FED1AE5EEED6_H
#define REPORTERMESSAGERECEIVER_TABD7DD7411DF8F7B66D42A50DCD5FED1AE5EEED6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReporterMessageReceiver
struct  ReporterMessageReceiver_tABD7DD7411DF8F7B66D42A50DCD5FED1AE5EEED6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Reporter ReporterMessageReceiver::reporter
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * ___reporter_4;

public:
	inline static int32_t get_offset_of_reporter_4() { return static_cast<int32_t>(offsetof(ReporterMessageReceiver_tABD7DD7411DF8F7B66D42A50DCD5FED1AE5EEED6, ___reporter_4)); }
	inline Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * get_reporter_4() const { return ___reporter_4; }
	inline Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E ** get_address_of_reporter_4() { return &___reporter_4; }
	inline void set_reporter_4(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * value)
	{
		___reporter_4 = value;
		Il2CppCodeGenWriteBarrier((&___reporter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPORTERMESSAGERECEIVER_TABD7DD7411DF8F7B66D42A50DCD5FED1AE5EEED6_H
#ifndef REWARDCARD_TE2469FF992CF638F044F91B93FFDD3850273C928_H
#define REWARDCARD_TE2469FF992CF638F044F91B93FFDD3850273C928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RewardCard
struct  RewardCard_tE2469FF992CF638F044F91B93FFDD3850273C928  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Sprite RewardCard::mCard
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___mCard_4;
	// System.String RewardCard::mName
	String_t* ___mName_5;
	// UnityEngine.UI.Image RewardCard::mCardImg
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___mCardImg_6;
	// UnityEngine.UI.Text RewardCard::mImgText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mImgText_7;

public:
	inline static int32_t get_offset_of_mCard_4() { return static_cast<int32_t>(offsetof(RewardCard_tE2469FF992CF638F044F91B93FFDD3850273C928, ___mCard_4)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_mCard_4() const { return ___mCard_4; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_mCard_4() { return &___mCard_4; }
	inline void set_mCard_4(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___mCard_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCard_4), value);
	}

	inline static int32_t get_offset_of_mName_5() { return static_cast<int32_t>(offsetof(RewardCard_tE2469FF992CF638F044F91B93FFDD3850273C928, ___mName_5)); }
	inline String_t* get_mName_5() const { return ___mName_5; }
	inline String_t** get_address_of_mName_5() { return &___mName_5; }
	inline void set_mName_5(String_t* value)
	{
		___mName_5 = value;
		Il2CppCodeGenWriteBarrier((&___mName_5), value);
	}

	inline static int32_t get_offset_of_mCardImg_6() { return static_cast<int32_t>(offsetof(RewardCard_tE2469FF992CF638F044F91B93FFDD3850273C928, ___mCardImg_6)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_mCardImg_6() const { return ___mCardImg_6; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_mCardImg_6() { return &___mCardImg_6; }
	inline void set_mCardImg_6(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___mCardImg_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCardImg_6), value);
	}

	inline static int32_t get_offset_of_mImgText_7() { return static_cast<int32_t>(offsetof(RewardCard_tE2469FF992CF638F044F91B93FFDD3850273C928, ___mImgText_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mImgText_7() const { return ___mImgText_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mImgText_7() { return &___mImgText_7; }
	inline void set_mImgText_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mImgText_7 = value;
		Il2CppCodeGenWriteBarrier((&___mImgText_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDCARD_TE2469FF992CF638F044F91B93FFDD3850273C928_H
#ifndef ROTATE_T763544D6383D5853BC2A73F921EB5C3A7A2F836C_H
#define ROTATE_T763544D6383D5853BC2A73F921EB5C3A7A2F836C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rotate
struct  Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 Rotate::angle
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___angle_4;

public:
	inline static int32_t get_offset_of_angle_4() { return static_cast<int32_t>(offsetof(Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C, ___angle_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_angle_4() const { return ___angle_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_angle_4() { return &___angle_4; }
	inline void set_angle_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___angle_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATE_T763544D6383D5853BC2A73F921EB5C3A7A2F836C_H
#ifndef SLOT_TA893CC5511A871B7F68FB1F20A049AE229FCB4FA_H
#define SLOT_TA893CC5511A871B7F68FB1F20A049AE229FCB4FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Slot
struct  Slot_tA893CC5511A871B7F68FB1F20A049AE229FCB4FA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image Slot::mImgColor
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___mImgColor_4;
	// UnityEngine.UI.Text Slot::mNumberText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mNumberText_5;

public:
	inline static int32_t get_offset_of_mImgColor_4() { return static_cast<int32_t>(offsetof(Slot_tA893CC5511A871B7F68FB1F20A049AE229FCB4FA, ___mImgColor_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_mImgColor_4() const { return ___mImgColor_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_mImgColor_4() { return &___mImgColor_4; }
	inline void set_mImgColor_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___mImgColor_4 = value;
		Il2CppCodeGenWriteBarrier((&___mImgColor_4), value);
	}

	inline static int32_t get_offset_of_mNumberText_5() { return static_cast<int32_t>(offsetof(Slot_tA893CC5511A871B7F68FB1F20A049AE229FCB4FA, ___mNumberText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mNumberText_5() const { return ___mNumberText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mNumberText_5() { return &___mNumberText_5; }
	inline void set_mNumberText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mNumberText_5 = value;
		Il2CppCodeGenWriteBarrier((&___mNumberText_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_TA893CC5511A871B7F68FB1F20A049AE229FCB4FA_H
#ifndef TESTREPORTER_TDA94F60FC70A78F5A8D976BA964F1C05CAB2A997_H
#define TESTREPORTER_TDA94F60FC70A78F5A8D976BA964F1C05CAB2A997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestReporter
struct  TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 TestReporter::logTestCount
	int32_t ___logTestCount_4;
	// System.Int32 TestReporter::threadLogTestCount
	int32_t ___threadLogTestCount_5;
	// System.Boolean TestReporter::logEverySecond
	bool ___logEverySecond_6;
	// System.Int32 TestReporter::currentLogTestCount
	int32_t ___currentLogTestCount_7;
	// Reporter TestReporter::reporter
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * ___reporter_8;
	// UnityEngine.GUIStyle TestReporter::style
	GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * ___style_9;
	// UnityEngine.Rect TestReporter::rect1
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect1_10;
	// UnityEngine.Rect TestReporter::rect2
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect2_11;
	// UnityEngine.Rect TestReporter::rect3
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect3_12;
	// UnityEngine.Rect TestReporter::rect4
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect4_13;
	// UnityEngine.Rect TestReporter::rect5
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect5_14;
	// UnityEngine.Rect TestReporter::rect6
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect6_15;
	// System.Threading.Thread TestReporter::thread
	Thread_t83C301EC970792455F76D89E58140949B003EA50 * ___thread_16;
	// System.Single TestReporter::elapsed
	float ___elapsed_17;

public:
	inline static int32_t get_offset_of_logTestCount_4() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___logTestCount_4)); }
	inline int32_t get_logTestCount_4() const { return ___logTestCount_4; }
	inline int32_t* get_address_of_logTestCount_4() { return &___logTestCount_4; }
	inline void set_logTestCount_4(int32_t value)
	{
		___logTestCount_4 = value;
	}

	inline static int32_t get_offset_of_threadLogTestCount_5() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___threadLogTestCount_5)); }
	inline int32_t get_threadLogTestCount_5() const { return ___threadLogTestCount_5; }
	inline int32_t* get_address_of_threadLogTestCount_5() { return &___threadLogTestCount_5; }
	inline void set_threadLogTestCount_5(int32_t value)
	{
		___threadLogTestCount_5 = value;
	}

	inline static int32_t get_offset_of_logEverySecond_6() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___logEverySecond_6)); }
	inline bool get_logEverySecond_6() const { return ___logEverySecond_6; }
	inline bool* get_address_of_logEverySecond_6() { return &___logEverySecond_6; }
	inline void set_logEverySecond_6(bool value)
	{
		___logEverySecond_6 = value;
	}

	inline static int32_t get_offset_of_currentLogTestCount_7() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___currentLogTestCount_7)); }
	inline int32_t get_currentLogTestCount_7() const { return ___currentLogTestCount_7; }
	inline int32_t* get_address_of_currentLogTestCount_7() { return &___currentLogTestCount_7; }
	inline void set_currentLogTestCount_7(int32_t value)
	{
		___currentLogTestCount_7 = value;
	}

	inline static int32_t get_offset_of_reporter_8() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___reporter_8)); }
	inline Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * get_reporter_8() const { return ___reporter_8; }
	inline Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E ** get_address_of_reporter_8() { return &___reporter_8; }
	inline void set_reporter_8(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E * value)
	{
		___reporter_8 = value;
		Il2CppCodeGenWriteBarrier((&___reporter_8), value);
	}

	inline static int32_t get_offset_of_style_9() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___style_9)); }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * get_style_9() const { return ___style_9; }
	inline GUIStyle_t671F175A201A19166385EE3392292A5F50070572 ** get_address_of_style_9() { return &___style_9; }
	inline void set_style_9(GUIStyle_t671F175A201A19166385EE3392292A5F50070572 * value)
	{
		___style_9 = value;
		Il2CppCodeGenWriteBarrier((&___style_9), value);
	}

	inline static int32_t get_offset_of_rect1_10() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___rect1_10)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_rect1_10() const { return ___rect1_10; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_rect1_10() { return &___rect1_10; }
	inline void set_rect1_10(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___rect1_10 = value;
	}

	inline static int32_t get_offset_of_rect2_11() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___rect2_11)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_rect2_11() const { return ___rect2_11; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_rect2_11() { return &___rect2_11; }
	inline void set_rect2_11(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___rect2_11 = value;
	}

	inline static int32_t get_offset_of_rect3_12() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___rect3_12)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_rect3_12() const { return ___rect3_12; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_rect3_12() { return &___rect3_12; }
	inline void set_rect3_12(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___rect3_12 = value;
	}

	inline static int32_t get_offset_of_rect4_13() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___rect4_13)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_rect4_13() const { return ___rect4_13; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_rect4_13() { return &___rect4_13; }
	inline void set_rect4_13(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___rect4_13 = value;
	}

	inline static int32_t get_offset_of_rect5_14() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___rect5_14)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_rect5_14() const { return ___rect5_14; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_rect5_14() { return &___rect5_14; }
	inline void set_rect5_14(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___rect5_14 = value;
	}

	inline static int32_t get_offset_of_rect6_15() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___rect6_15)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_rect6_15() const { return ___rect6_15; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_rect6_15() { return &___rect6_15; }
	inline void set_rect6_15(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___rect6_15 = value;
	}

	inline static int32_t get_offset_of_thread_16() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___thread_16)); }
	inline Thread_t83C301EC970792455F76D89E58140949B003EA50 * get_thread_16() const { return ___thread_16; }
	inline Thread_t83C301EC970792455F76D89E58140949B003EA50 ** get_address_of_thread_16() { return &___thread_16; }
	inline void set_thread_16(Thread_t83C301EC970792455F76D89E58140949B003EA50 * value)
	{
		___thread_16 = value;
		Il2CppCodeGenWriteBarrier((&___thread_16), value);
	}

	inline static int32_t get_offset_of_elapsed_17() { return static_cast<int32_t>(offsetof(TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997, ___elapsed_17)); }
	inline float get_elapsed_17() const { return ___elapsed_17; }
	inline float* get_address_of_elapsed_17() { return &___elapsed_17; }
	inline void set_elapsed_17(float value)
	{
		___elapsed_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTREPORTER_TDA94F60FC70A78F5A8D976BA964F1C05CAB2A997_H
#ifndef TIMECONTROLLER_TB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8_H
#define TIMECONTROLLER_TB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeController
struct  TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Text TimeController::mTimeText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___mTimeText_5;
	// System.Single TimeController::SpendTime
	float ___SpendTime_6;
	// System.Boolean TimeController::StartCount
	bool ___StartCount_7;

public:
	inline static int32_t get_offset_of_mTimeText_5() { return static_cast<int32_t>(offsetof(TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8, ___mTimeText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_mTimeText_5() const { return ___mTimeText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_mTimeText_5() { return &___mTimeText_5; }
	inline void set_mTimeText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___mTimeText_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTimeText_5), value);
	}

	inline static int32_t get_offset_of_SpendTime_6() { return static_cast<int32_t>(offsetof(TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8, ___SpendTime_6)); }
	inline float get_SpendTime_6() const { return ___SpendTime_6; }
	inline float* get_address_of_SpendTime_6() { return &___SpendTime_6; }
	inline void set_SpendTime_6(float value)
	{
		___SpendTime_6 = value;
	}

	inline static int32_t get_offset_of_StartCount_7() { return static_cast<int32_t>(offsetof(TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8, ___StartCount_7)); }
	inline bool get_StartCount_7() const { return ___StartCount_7; }
	inline bool* get_address_of_StartCount_7() { return &___StartCount_7; }
	inline void set_StartCount_7(bool value)
	{
		___StartCount_7 = value;
	}
};

struct TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8_StaticFields
{
public:
	// TimeController TimeController::Instance
	TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8 * ___Instance_4;

public:
	inline static int32_t get_offset_of_Instance_4() { return static_cast<int32_t>(offsetof(TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8_StaticFields, ___Instance_4)); }
	inline TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8 * get_Instance_4() const { return ___Instance_4; }
	inline TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8 ** get_address_of_Instance_4() { return &___Instance_4; }
	inline void set_Instance_4(TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8 * value)
	{
		___Instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMECONTROLLER_TB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8_H
#ifndef IBEACONRECEIVER_T43A0131A8BD99AFFE4033418979482BBCA876C0F_H
#define IBEACONRECEIVER_T43A0131A8BD99AFFE4033418979482BBCA876C0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iBeaconReceiver
struct  iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// iBeaconRegion[] iBeaconReceiver::_regions
	iBeaconRegionU5BU5D_t9F3E57765B411A6495722FFA1DF5F15E1109A42D* ____regions_5;
	// System.Single iBeaconReceiver::_detectionTimespan
	float ____detectionTimespan_6;
	// System.Single iBeaconReceiver::_scanPeriod
	float ____scanPeriod_7;
	// System.Single iBeaconReceiver::_betweenScanPeriod
	float ____betweenScanPeriod_8;

public:
	inline static int32_t get_offset_of__regions_5() { return static_cast<int32_t>(offsetof(iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F, ____regions_5)); }
	inline iBeaconRegionU5BU5D_t9F3E57765B411A6495722FFA1DF5F15E1109A42D* get__regions_5() const { return ____regions_5; }
	inline iBeaconRegionU5BU5D_t9F3E57765B411A6495722FFA1DF5F15E1109A42D** get_address_of__regions_5() { return &____regions_5; }
	inline void set__regions_5(iBeaconRegionU5BU5D_t9F3E57765B411A6495722FFA1DF5F15E1109A42D* value)
	{
		____regions_5 = value;
		Il2CppCodeGenWriteBarrier((&____regions_5), value);
	}

	inline static int32_t get_offset_of__detectionTimespan_6() { return static_cast<int32_t>(offsetof(iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F, ____detectionTimespan_6)); }
	inline float get__detectionTimespan_6() const { return ____detectionTimespan_6; }
	inline float* get_address_of__detectionTimespan_6() { return &____detectionTimespan_6; }
	inline void set__detectionTimespan_6(float value)
	{
		____detectionTimespan_6 = value;
	}

	inline static int32_t get_offset_of__scanPeriod_7() { return static_cast<int32_t>(offsetof(iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F, ____scanPeriod_7)); }
	inline float get__scanPeriod_7() const { return ____scanPeriod_7; }
	inline float* get_address_of__scanPeriod_7() { return &____scanPeriod_7; }
	inline void set__scanPeriod_7(float value)
	{
		____scanPeriod_7 = value;
	}

	inline static int32_t get_offset_of__betweenScanPeriod_8() { return static_cast<int32_t>(offsetof(iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F, ____betweenScanPeriod_8)); }
	inline float get__betweenScanPeriod_8() const { return ____betweenScanPeriod_8; }
	inline float* get_address_of__betweenScanPeriod_8() { return &____betweenScanPeriod_8; }
	inline void set__betweenScanPeriod_8(float value)
	{
		____betweenScanPeriod_8 = value;
	}
};

struct iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F_StaticFields
{
public:
	// iBeaconReceiver_BeaconRangeChanged iBeaconReceiver::BeaconRangeChangedEvent
	BeaconRangeChanged_tB250A609F6139101DE7538850A8524B29198C478 * ___BeaconRangeChangedEvent_4;
	// iBeaconReceiver iBeaconReceiver::m_instance
	iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F * ___m_instance_9;
	// System.Boolean iBeaconReceiver::initialized
	bool ___initialized_10;
	// System.Boolean iBeaconReceiver::receiving
	bool ___receiving_11;
	// System.Func`2<iBeaconRegion,System.Boolean> iBeaconReceiver::<>f__amU24cache0
	Func_2_t2F03C7144AB252BC791273C6A5E26B7598EE87FD * ___U3CU3Ef__amU24cache0_13;

public:
	inline static int32_t get_offset_of_BeaconRangeChangedEvent_4() { return static_cast<int32_t>(offsetof(iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F_StaticFields, ___BeaconRangeChangedEvent_4)); }
	inline BeaconRangeChanged_tB250A609F6139101DE7538850A8524B29198C478 * get_BeaconRangeChangedEvent_4() const { return ___BeaconRangeChangedEvent_4; }
	inline BeaconRangeChanged_tB250A609F6139101DE7538850A8524B29198C478 ** get_address_of_BeaconRangeChangedEvent_4() { return &___BeaconRangeChangedEvent_4; }
	inline void set_BeaconRangeChangedEvent_4(BeaconRangeChanged_tB250A609F6139101DE7538850A8524B29198C478 * value)
	{
		___BeaconRangeChangedEvent_4 = value;
		Il2CppCodeGenWriteBarrier((&___BeaconRangeChangedEvent_4), value);
	}

	inline static int32_t get_offset_of_m_instance_9() { return static_cast<int32_t>(offsetof(iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F_StaticFields, ___m_instance_9)); }
	inline iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F * get_m_instance_9() const { return ___m_instance_9; }
	inline iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F ** get_address_of_m_instance_9() { return &___m_instance_9; }
	inline void set_m_instance_9(iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F * value)
	{
		___m_instance_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_instance_9), value);
	}

	inline static int32_t get_offset_of_initialized_10() { return static_cast<int32_t>(offsetof(iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F_StaticFields, ___initialized_10)); }
	inline bool get_initialized_10() const { return ___initialized_10; }
	inline bool* get_address_of_initialized_10() { return &___initialized_10; }
	inline void set_initialized_10(bool value)
	{
		___initialized_10 = value;
	}

	inline static int32_t get_offset_of_receiving_11() { return static_cast<int32_t>(offsetof(iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F_StaticFields, ___receiving_11)); }
	inline bool get_receiving_11() const { return ___receiving_11; }
	inline bool* get_address_of_receiving_11() { return &___receiving_11; }
	inline void set_receiving_11(bool value)
	{
		___receiving_11 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_13() { return static_cast<int32_t>(offsetof(iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F_StaticFields, ___U3CU3Ef__amU24cache0_13)); }
	inline Func_2_t2F03C7144AB252BC791273C6A5E26B7598EE87FD * get_U3CU3Ef__amU24cache0_13() const { return ___U3CU3Ef__amU24cache0_13; }
	inline Func_2_t2F03C7144AB252BC791273C6A5E26B7598EE87FD ** get_address_of_U3CU3Ef__amU24cache0_13() { return &___U3CU3Ef__amU24cache0_13; }
	inline void set_U3CU3Ef__amU24cache0_13(Func_2_t2F03C7144AB252BC791273C6A5E26B7598EE87FD * value)
	{
		___U3CU3Ef__amU24cache0_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IBEACONRECEIVER_T43A0131A8BD99AFFE4033418979482BBCA876C0F_H
#ifndef IBEACONSERVER_TCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6_H
#define IBEACONSERVER_TCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// iBeaconServer
struct  iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// iBeaconRegion iBeaconServer::_region
	iBeaconRegion_t5C666E55D6EB9CB88EC29B58B305A5C217BBC607 * ____region_4;
	// System.Int32 iBeaconServer::_txPower
	int32_t ____txPower_5;
	// iBeaconServer_AndroidPowerLevel iBeaconServer::_powerLevel
	int32_t ____powerLevel_6;
	// iBeaconServer_AndroidMode iBeaconServer::_mode
	int32_t ____mode_7;

public:
	inline static int32_t get_offset_of__region_4() { return static_cast<int32_t>(offsetof(iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6, ____region_4)); }
	inline iBeaconRegion_t5C666E55D6EB9CB88EC29B58B305A5C217BBC607 * get__region_4() const { return ____region_4; }
	inline iBeaconRegion_t5C666E55D6EB9CB88EC29B58B305A5C217BBC607 ** get_address_of__region_4() { return &____region_4; }
	inline void set__region_4(iBeaconRegion_t5C666E55D6EB9CB88EC29B58B305A5C217BBC607 * value)
	{
		____region_4 = value;
		Il2CppCodeGenWriteBarrier((&____region_4), value);
	}

	inline static int32_t get_offset_of__txPower_5() { return static_cast<int32_t>(offsetof(iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6, ____txPower_5)); }
	inline int32_t get__txPower_5() const { return ____txPower_5; }
	inline int32_t* get_address_of__txPower_5() { return &____txPower_5; }
	inline void set__txPower_5(int32_t value)
	{
		____txPower_5 = value;
	}

	inline static int32_t get_offset_of__powerLevel_6() { return static_cast<int32_t>(offsetof(iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6, ____powerLevel_6)); }
	inline int32_t get__powerLevel_6() const { return ____powerLevel_6; }
	inline int32_t* get_address_of__powerLevel_6() { return &____powerLevel_6; }
	inline void set__powerLevel_6(int32_t value)
	{
		____powerLevel_6 = value;
	}

	inline static int32_t get_offset_of__mode_7() { return static_cast<int32_t>(offsetof(iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6, ____mode_7)); }
	inline int32_t get__mode_7() const { return ____mode_7; }
	inline int32_t* get_address_of__mode_7() { return &____mode_7; }
	inline void set__mode_7(int32_t value)
	{
		____mode_7 = value;
	}
};

struct iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6_StaticFields
{
public:
	// iBeaconServer iBeaconServer::m_instance
	iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6 * ___m_instance_8;
	// System.Boolean iBeaconServer::initialized
	bool ___initialized_9;
	// System.Boolean iBeaconServer::transmitting
	bool ___transmitting_10;

public:
	inline static int32_t get_offset_of_m_instance_8() { return static_cast<int32_t>(offsetof(iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6_StaticFields, ___m_instance_8)); }
	inline iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6 * get_m_instance_8() const { return ___m_instance_8; }
	inline iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6 ** get_address_of_m_instance_8() { return &___m_instance_8; }
	inline void set_m_instance_8(iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6 * value)
	{
		___m_instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_instance_8), value);
	}

	inline static int32_t get_offset_of_initialized_9() { return static_cast<int32_t>(offsetof(iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6_StaticFields, ___initialized_9)); }
	inline bool get_initialized_9() const { return ___initialized_9; }
	inline bool* get_address_of_initialized_9() { return &___initialized_9; }
	inline void set_initialized_9(bool value)
	{
		___initialized_9 = value;
	}

	inline static int32_t get_offset_of_transmitting_10() { return static_cast<int32_t>(offsetof(iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6_StaticFields, ___transmitting_10)); }
	inline bool get_transmitting_10() const { return ___transmitting_10; }
	inline bool* get_address_of_transmitting_10() { return &___transmitting_10; }
	inline void set_transmitting_10(bool value)
	{
		___transmitting_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IBEACONSERVER_TCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2900 = { sizeof (SimpleResourceFactory_tA31CC8DA5DB8C7190B9EE7E66AA4D570C1C71D14), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2901 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2902 = { sizeof (AddResources_t3D4A1E7E86AC9B7C12E274F57E970A6A867929AD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2903 = { sizeof (ResourceLoadBTrace_tA3D930FE12A6F09A0F18935022C70B15F6CE588E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2904 = { sizeof (RewardCard_tE2469FF992CF638F044F91B93FFDD3850273C928), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2904[4] = 
{
	RewardCard_tE2469FF992CF638F044F91B93FFDD3850273C928::get_offset_of_mCard_4(),
	RewardCard_tE2469FF992CF638F044F91B93FFDD3850273C928::get_offset_of_mName_5(),
	RewardCard_tE2469FF992CF638F044F91B93FFDD3850273C928::get_offset_of_mCardImg_6(),
	RewardCard_tE2469FF992CF638F044F91B93FFDD3850273C928::get_offset_of_mImgText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2905 = { sizeof (JSONBinaryTag_tAC82A15D9AE639E8BA8ACF99387657998313FA99)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2905[8] = 
{
	JSONBinaryTag_tAC82A15D9AE639E8BA8ACF99387657998313FA99::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2906 = { sizeof (JSONNode_t0AA84017F5559ABABE7CB27656B1352F565B5889), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2907 = { sizeof (U3CU3Ec__Iterator0_t7C332823620D33941DB25086B399A1280F6E3556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2907[3] = 
{
	U3CU3Ec__Iterator0_t7C332823620D33941DB25086B399A1280F6E3556::get_offset_of_U24current_0(),
	U3CU3Ec__Iterator0_t7C332823620D33941DB25086B399A1280F6E3556::get_offset_of_U24disposing_1(),
	U3CU3Ec__Iterator0_t7C332823620D33941DB25086B399A1280F6E3556::get_offset_of_U24PC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2908 = { sizeof (U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2908[8] = 
{
	U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7::get_offset_of_U3CCU3E__1_1(),
	U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7::get_offset_of_U24locvar1_2(),
	U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7::get_offset_of_U3CDU3E__2_3(),
	U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7::get_offset_of_U24this_4(),
	U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7::get_offset_of_U24current_5(),
	U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7::get_offset_of_U24disposing_6(),
	U3CU3Ec__Iterator1_tBCD33A6138D22501E073C18D3014899A28F00BC7::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2909 = { sizeof (JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2909[1] = 
{
	JSONArray_tA823D4BEAEF52C9EF2FCB3823F588177FCA35BC8::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2910 = { sizeof (U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2910[6] = 
{
	U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53::get_offset_of_U3CNU3E__1_1(),
	U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_t65C86444E3E918F4F0190A2FB599E33297157D53::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2911 = { sizeof (U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2911[6] = 
{
	U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1::get_offset_of_U3CNU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1::get_offset_of_U24this_2(),
	U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1::get_offset_of_U24disposing_4(),
	U3CGetEnumeratorU3Ec__Iterator1_tB9765384EAED9EFADEB3AFBC4E9DF099C4F27AC1::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2912 = { sizeof (JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2912[1] = 
{
	JSONClass_t8FEC65DD2AAEEC3876FE9A03371D9DB3A8653A4F::get_offset_of_m_Dict_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2913 = { sizeof (U3CRemoveU3Ec__AnonStorey2_t132776AAE263F0FCA30E3DAC8ACA78E95F92299E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2913[1] = 
{
	U3CRemoveU3Ec__AnonStorey2_t132776AAE263F0FCA30E3DAC8ACA78E95F92299E::get_offset_of_aNode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2914 = { sizeof (U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2914[6] = 
{
	U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05::get_offset_of_U24locvar0_0(),
	U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05::get_offset_of_U3CNU3E__1_1(),
	U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05::get_offset_of_U24this_2(),
	U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05::get_offset_of_U24current_3(),
	U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05::get_offset_of_U24disposing_4(),
	U3CU3Ec__Iterator0_tF26315FF5214262587BC852C86D664123E470A05::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2915 = { sizeof (U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2915[6] = 
{
	U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D::get_offset_of_U24locvar0_0(),
	U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D::get_offset_of_U3CNU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D::get_offset_of_U24this_2(),
	U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D::get_offset_of_U24disposing_4(),
	U3CGetEnumeratorU3Ec__Iterator1_tCFF0D13470627674DB7875937E7DAC3901E64E4D::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2916 = { sizeof (JSONData_t82E8EC24D2016E5CB36B4DE156906D9B57EEF020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2916[1] = 
{
	JSONData_t82E8EC24D2016E5CB36B4DE156906D9B57EEF020::get_offset_of_m_Data_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2917 = { sizeof (JSONLazyCreator_tC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2917[2] = 
{
	JSONLazyCreator_tC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C::get_offset_of_m_Node_0(),
	JSONLazyCreator_tC44F9D77A8274EBDF3699889A65E7FDFB22EFE5C::get_offset_of_m_Key_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2918 = { sizeof (JSON_t23727CDEA55ED3C10DCEE3222EAC8D0EB7973C05), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2919 = { sizeof (Slot_tA893CC5511A871B7F68FB1F20A049AE229FCB4FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2919[2] = 
{
	Slot_tA893CC5511A871B7F68FB1F20A049AE229FCB4FA::get_offset_of_mImgColor_4(),
	Slot_tA893CC5511A871B7F68FB1F20A049AE229FCB4FA::get_offset_of_mNumberText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2920 = { sizeof (TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8), -1, sizeof(TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2920[4] = 
{
	TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8_StaticFields::get_offset_of_Instance_4(),
	TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8::get_offset_of_mTimeText_5(),
	TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8::get_offset_of_SpendTime_6(),
	TimeController_tB89B0D556CE1C0A63D39D2BC1FC59BF6E45F72F8::get_offset_of_StartCount_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2921 = { sizeof (BDSLiteSpeech_t0080FF8C29E1F3373FB64E23EC48B7B6A3F78B24), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2922 = { sizeof (BDSLiteSpeechSample_t24AD40B96AB93161F6023D690979AF719A70DADC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2922[1] = 
{
	BDSLiteSpeechSample_t24AD40B96AB93161F6023D690979AF719A70DADC::get_offset_of_SpeakText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2923 = { sizeof (DetectCollision_t9297CB9131627AEE604F88130B279A44B3ED71B7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2924 = { sizeof (BroadcastMode_t1914C940F1376C6B26EC5D6CFD6AE3516D47E9EF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2924[4] = 
{
	BroadcastMode_t1914C940F1376C6B26EC5D6CFD6AE3516D47E9EF::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2925 = { sizeof (BroadcastState_t16D4A451D29E58B8181255E6A7D9DCBF48FCBF23)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2925[3] = 
{
	BroadcastState_t16D4A451D29E58B8181255E6A7D9DCBF48FCBF23::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2926 = { sizeof (Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2926[13] = 
{
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_bt_Type_4(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_bm_Mode_5(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_bs_State_6(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_ObjectCube_7(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_BeaconCamera_8(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_currentTime_9(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_spawnDelay_10(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_go_FoundBeaconClone_11(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_f_ScrollViewContentRectWidth_12(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_f_ScrollViewContentRectHeight_13(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_i_BeaconCounter_14(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_mSpawnCount_15(),
	Example_tB7AEFF417A3F2A1E30F8931285218D37BC4954CF::get_offset_of_mybeacons_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2927 = { sizeof (U3CWaitBlueToothOnU3Ec__Iterator0_tCF62F557E091FF29B4429D345B955C6DCAB2B8E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2927[5] = 
{
	U3CWaitBlueToothOnU3Ec__Iterator0_tCF62F557E091FF29B4429D345B955C6DCAB2B8E9::get_offset_of_state_0(),
	U3CWaitBlueToothOnU3Ec__Iterator0_tCF62F557E091FF29B4429D345B955C6DCAB2B8E9::get_offset_of_U24this_1(),
	U3CWaitBlueToothOnU3Ec__Iterator0_tCF62F557E091FF29B4429D345B955C6DCAB2B8E9::get_offset_of_U24current_2(),
	U3CWaitBlueToothOnU3Ec__Iterator0_tCF62F557E091FF29B4429D345B955C6DCAB2B8E9::get_offset_of_U24disposing_3(),
	U3CWaitBlueToothOnU3Ec__Iterator0_tCF62F557E091FF29B4429D345B955C6DCAB2B8E9::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2928 = { sizeof (BeaconRange_t9B0D359B31C878EE6039FF6601054ED135CC2C77)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2928[5] = 
{
	BeaconRange_t9B0D359B31C878EE6039FF6601054ED135CC2C77::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2929 = { sizeof (BeaconType_t6F651FD6AB6CA7EC3CA1498276CCE74212C6AC49)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2929[6] = 
{
	BeaconType_t6F651FD6AB6CA7EC3CA1498276CCE74212C6AC49::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2930 = { sizeof (Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD), -1, sizeof(Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2930[16] = 
{
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__type_0(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__UUID_1(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__major_2(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__minor_3(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__instance_4(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__rssi_5(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__range_6(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__strength_7(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__accuracy_8(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__rawTelemetry_9(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__regionName_10(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__lastSeen_11(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD::get_offset_of__telemetry_12(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD_StaticFields::get_offset_of_EddystoneNamespaceRegex_13(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD_StaticFields::get_offset_of_EddystoneInstanceRegex_14(),
	Beacon_t36C1666DE4D5755F5A474E47B04BBEDDAA908FFD_StaticFields::get_offset_of_iBeaconUUIDRegex_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2931 = { sizeof (BeaconPropertyAttribute_tBDB24EC120F805FEBD33A90C594B1E3870CFAFE4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2931[3] = 
{
	BeaconPropertyAttribute_tBDB24EC120F805FEBD33A90C594B1E3870CFAFE4::get_offset_of_Field_0(),
	BeaconPropertyAttribute_tBDB24EC120F805FEBD33A90C594B1E3870CFAFE4::get_offset_of_Types_1(),
	BeaconPropertyAttribute_tBDB24EC120F805FEBD33A90C594B1E3870CFAFE4::get_offset_of_Exclude_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2932 = { sizeof (BluetoothLowEnergyState_tE4FE96513B91C829BDAFDE64CABB0F60B8F444ED)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2932[10] = 
{
	BluetoothLowEnergyState_tE4FE96513B91C829BDAFDE64CABB0F60B8F444ED::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2933 = { sizeof (BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F), -1, sizeof(BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2933[4] = 
{
	BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F_StaticFields::get_offset_of_BluetoothStateChangedEvent_4(),
	BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F_StaticFields::get_offset_of_initialized_5(),
	0,
	BluetoothState_t26E5F938742C65B27F708548BC5D06AD6856713F_StaticFields::get_offset_of_m_instance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2934 = { sizeof (BluetoothStateChanged_t795820F12512E5B80A7737D6836A41A7E0C59DEF), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2935 = { sizeof (iBeaconException_t711CE5299D51AD566A36AF6D5372E2BD45380779), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2936 = { sizeof (iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F), -1, sizeof(iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2936[10] = 
{
	iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F_StaticFields::get_offset_of_BeaconRangeChangedEvent_4(),
	iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F::get_offset_of__regions_5(),
	iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F::get_offset_of__detectionTimespan_6(),
	iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F::get_offset_of__scanPeriod_7(),
	iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F::get_offset_of__betweenScanPeriod_8(),
	iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F_StaticFields::get_offset_of_m_instance_9(),
	iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F_StaticFields::get_offset_of_initialized_10(),
	iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F_StaticFields::get_offset_of_receiving_11(),
	0,
	iBeaconReceiver_t43A0131A8BD99AFFE4033418979482BBCA876C0F_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2937 = { sizeof (BeaconRangeChanged_tB250A609F6139101DE7538850A8524B29198C478), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2938 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2938[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2939 = { sizeof (iBeaconRegion_t5C666E55D6EB9CB88EC29B58B305A5C217BBC607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2939[2] = 
{
	iBeaconRegion_t5C666E55D6EB9CB88EC29B58B305A5C217BBC607::get_offset_of__regionName_0(),
	iBeaconRegion_t5C666E55D6EB9CB88EC29B58B305A5C217BBC607::get_offset_of__beacon_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2940 = { sizeof (iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6), -1, sizeof(iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2940[7] = 
{
	iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6::get_offset_of__region_4(),
	iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6::get_offset_of__txPower_5(),
	iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6::get_offset_of__powerLevel_6(),
	iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6::get_offset_of__mode_7(),
	iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6_StaticFields::get_offset_of_m_instance_8(),
	iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6_StaticFields::get_offset_of_initialized_9(),
	iBeaconServer_tCE8798B326663C2B84A4F0A23B4356EDE2F8ADD6_StaticFields::get_offset_of_transmitting_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2941 = { sizeof (AndroidPowerLevel_t00973029F90DD0BA7E7047C0A50540F84869E02B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2941[5] = 
{
	AndroidPowerLevel_t00973029F90DD0BA7E7047C0A50540F84869E02B::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2942 = { sizeof (AndroidMode_t3A67510C4A6775711A0966A18D3E8486797A5E2C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2942[4] = 
{
	AndroidMode_t3A67510C4A6775711A0966A18D3E8486797A5E2C::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2943 = { sizeof (Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2943[8] = 
{
	Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E::get_offset_of_voltage_0(),
	Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E::get_offset_of_temperature_1(),
	Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E::get_offset_of_frameCounter_2(),
	Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E::get_offset_of_uptime_3(),
	Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E::get_offset_of_encryptedData_4(),
	Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E::get_offset_of_salt_5(),
	Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E::get_offset_of_integrityCheck_6(),
	Telemetry_t72A66C9CD10B6758A660F2C28ABE26CDA0834C6E::get_offset_of_encrypted_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2944 = { sizeof (U3CTelemetryU3Ec__AnonStorey0_t62793050C92FECD47DFA6FA1F97F9BBE653517FD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2944[1] = 
{
	U3CTelemetryU3Ec__AnonStorey0_t62793050C92FECD47DFA6FA1F97F9BBE653517FD::get_offset_of_hex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2945 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2946 = { sizeof (Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2946[28] = 
{
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_clearImage_0(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_collapseImage_1(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_clearOnNewSceneImage_2(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_showTimeImage_3(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_showSceneImage_4(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_userImage_5(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_showMemoryImage_6(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_softwareImage_7(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_dateImage_8(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_showFpsImage_9(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_infoImage_10(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_saveLogsImage_11(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_searchImage_12(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_copyImage_13(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_closeImage_14(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_buildFromImage_15(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_systemInfoImage_16(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_graphicsInfoImage_17(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_backImage_18(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_logImage_19(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_warningImage_20(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_errorImage_21(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_barImage_22(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_button_activeImage_23(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_even_logImage_24(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_odd_logImage_25(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_selectedImage_26(),
	Images_t8B806A90CFA740B9FE6317F1F5ED416760C1B8DC::get_offset_of_reporterScrollerSkin_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2947 = { sizeof (Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E), -1, sizeof(Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2947[152] = 
{
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E_StaticFields::get_offset_of_Instance_4(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_samples_5(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_logs_6(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_collapsedLogs_7(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_currentLog_8(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_logsDic_9(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_cachedString_10(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_show_11(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_collapse_12(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_clearOnNewSceneLoaded_13(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showTime_14(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showScene_15(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showMemory_16(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showFps_17(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showGraph_18(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showLog_19(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showWarning_20(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showError_21(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_numOfLogs_22(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_numOfLogsWarning_23(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_numOfLogsError_24(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_numOfCollapsedLogs_25(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_numOfCollapsedLogsWarning_26(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_numOfCollapsedLogsError_27(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showClearOnNewSceneLoadedButton_28(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showTimeButton_29(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showSceneButton_30(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showMemButton_31(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showFpsButton_32(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showSearchText_33(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showCopyButton_34(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showSaveButton_35(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_buildDate_36(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_logDate_37(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_logsMemUsage_38(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_graphMemUsage_39(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_gcTotalMemory_40(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_UserData_41(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_fps_42(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_fpsText_43(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_currentView_44(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E_StaticFields::get_offset_of_created_45(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_images_46(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_clearContent_47(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_collapseContent_48(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_clearOnNewSceneContent_49(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showTimeContent_50(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showSceneContent_51(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_userContent_52(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showMemoryContent_53(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_softwareContent_54(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_dateContent_55(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_showFpsContent_56(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_infoContent_57(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_saveLogsContent_58(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_searchContent_59(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_copyContent_60(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_closeContent_61(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_buildFromContent_62(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_systemInfoContent_63(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_graphicsInfoContent_64(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_backContent_65(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_logContent_66(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_warningContent_67(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_errorContent_68(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_barStyle_69(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_buttonActiveStyle_70(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_nonStyle_71(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_lowerLeftFontStyle_72(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_backStyle_73(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_evenLogStyle_74(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_oddLogStyle_75(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_logButtonStyle_76(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_selectedLogStyle_77(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_selectedLogFontStyle_78(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_stackLabelStyle_79(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_scrollerStyle_80(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_searchStyle_81(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_sliderBackStyle_82(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_sliderThumbStyle_83(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_toolbarScrollerSkin_84(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_logScrollerSkin_85(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_graphScrollerSkin_86(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_size_87(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_maxSize_88(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_numOfCircleToShow_89(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E_StaticFields::get_offset_of_scenes_90(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_currentScene_91(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_filterText_92(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_deviceModel_93(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_deviceType_94(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_deviceName_95(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_graphicsMemorySize_96(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_maxTextureSize_97(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_systemMemorySize_98(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_Initialized_99(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_screenRect_100(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_toolBarRect_101(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_logsRect_102(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_stackRect_103(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_graphRect_104(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_graphMinRect_105(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_graphMaxRect_106(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_buttomRect_107(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_stackRectTopLeft_108(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_detailRect_109(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_scrollPosition_110(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_scrollPosition2_111(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_toolbarScrollPosition_112(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_selectedLog_113(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_toolbarOldDrag_114(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_oldDrag_115(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_oldDrag2_116(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_oldDrag3_117(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_startIndex_118(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_countRect_119(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_timeRect_120(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_timeLabelRect_121(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_sceneRect_122(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_sceneLabelRect_123(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_memoryRect_124(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_memoryLabelRect_125(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_fpsRect_126(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_fpsLabelRect_127(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_tempContent_128(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_infoScrollPosition_129(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_oldInfoDrag_130(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_tempRect_131(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_graphSize_132(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_startFrame_133(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_currentFrame_134(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_tempVector1_135(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_tempVector2_136(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_graphScrollerPos_137(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_maxFpsValue_138(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_minFpsValue_139(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_maxMemoryValue_140(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_minMemoryValue_141(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_gestureDetector_142(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_gestureSum_143(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_gestureLength_144(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_gestureCount_145(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_lastClickTime_146(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_startPos_147(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_downPos_148(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_mousePosition_149(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_frames_150(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_firstTime_151(),
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_lastUpdate_152(),
	0,
	0,
	Reporter_tFEAD3FE9ABBC033C2F5CEBFDB10B7430C6B6AF5E::get_offset_of_threadedLogs_155(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2948 = { sizeof (_LogType_t40ACA5C5A8487DA4BE69F6B2FB6397C691DDBE7A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2948[6] = 
{
	_LogType_t40ACA5C5A8487DA4BE69F6B2FB6397C691DDBE7A::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2949 = { sizeof (Sample_t88863152299A8F7A4FA9724DA3F943F4E0149689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2949[5] = 
{
	Sample_t88863152299A8F7A4FA9724DA3F943F4E0149689::get_offset_of_time_0(),
	Sample_t88863152299A8F7A4FA9724DA3F943F4E0149689::get_offset_of_loadedScene_1(),
	Sample_t88863152299A8F7A4FA9724DA3F943F4E0149689::get_offset_of_memory_2(),
	Sample_t88863152299A8F7A4FA9724DA3F943F4E0149689::get_offset_of_fps_3(),
	Sample_t88863152299A8F7A4FA9724DA3F943F4E0149689::get_offset_of_fpsText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2950 = { sizeof (Log_t152F87153D078D3D99151CF14D91D518B21E3E22), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2950[5] = 
{
	Log_t152F87153D078D3D99151CF14D91D518B21E3E22::get_offset_of_count_0(),
	Log_t152F87153D078D3D99151CF14D91D518B21E3E22::get_offset_of_logType_1(),
	Log_t152F87153D078D3D99151CF14D91D518B21E3E22::get_offset_of_condition_2(),
	Log_t152F87153D078D3D99151CF14D91D518B21E3E22::get_offset_of_stacktrace_3(),
	Log_t152F87153D078D3D99151CF14D91D518B21E3E22::get_offset_of_sampleId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2951 = { sizeof (ReportView_tAA3E3DFFECC2BBF6812E5FFB898CA0E1EA070428)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2951[5] = 
{
	ReportView_tAA3E3DFFECC2BBF6812E5FFB898CA0E1EA070428::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2952 = { sizeof (DetailView_t5D04911DF2A860E2B5C82686669E9A56BA496850)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2952[4] = 
{
	DetailView_t5D04911DF2A860E2B5C82686669E9A56BA496850::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2953 = { sizeof (U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2953[7] = 
{
	U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60::get_offset_of_U3CprefFileU3E__0_0(),
	U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60::get_offset_of_U3CurlU3E__0_1(),
	U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60::get_offset_of_U3CwwwU3E__0_2(),
	U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60::get_offset_of_U24this_3(),
	U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60::get_offset_of_U24current_4(),
	U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60::get_offset_of_U24disposing_5(),
	U3CreadInfoU3Ec__Iterator0_tA0E58ECD00099B1743B472C294C557F46C462A60::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2954 = { sizeof (ReporterGUI_t3748A1828F0F187695BA4A6B781D22E5DCBDF8DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2954[1] = 
{
	ReporterGUI_t3748A1828F0F187695BA4A6B781D22E5DCBDF8DE::get_offset_of_reporter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2955 = { sizeof (ReporterMessageReceiver_tABD7DD7411DF8F7B66D42A50DCD5FED1AE5EEED6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2955[1] = 
{
	ReporterMessageReceiver_tABD7DD7411DF8F7B66D42A50DCD5FED1AE5EEED6::get_offset_of_reporter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2956 = { sizeof (Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2956[1] = 
{
	Rotate_t763544D6383D5853BC2A73F921EB5C3A7A2F836C::get_offset_of_angle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2957 = { sizeof (TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2957[14] = 
{
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_logTestCount_4(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_threadLogTestCount_5(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_logEverySecond_6(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_currentLogTestCount_7(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_reporter_8(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_style_9(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_rect1_10(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_rect2_11(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_rect3_12(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_rect4_13(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_rect5_14(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_rect6_15(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_thread_16(),
	TestReporter_tDA94F60FC70A78F5A8D976BA964F1C05CAB2A997::get_offset_of_elapsed_17(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif

﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Mono.Math.BigInteger
struct BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E;
// Mono.Security.ASN1
struct ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA;
// Mono.Security.Cryptography.RSAManaged/KeyGeneratedEventHandler
struct KeyGeneratedEventHandler_t40F3B0E6400CF6FB02BED8AB74B8BFEC1DBA5042;
// Mono.Security.PKCS7/ContentInfo
struct ContentInfo_t51049CE410146B36AF37C7218807051D5996866F;
// Mono.Security.X509.Extensions.GeneralNames
struct GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45;
// Mono.Security.X509.X509Certificate
struct X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991;
// Mono.Security.X509.X509Store
struct X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302;
// Mono.Security.X509.X509Stores
struct X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28;
// Mono.Xml2.XmlTextReader
struct XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD;
// System.AsyncCallback
struct AsyncCallback_t74ABD1277F711E7FBDCB00E169A63DEFD39E86A2;
// System.Byte[]
struct ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8;
// System.Char[]
struct CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744;
// System.Collections.ArrayList
struct ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1;
// System.Collections.Generic.List`1<System.Configuration.ConfigurationProperty>
struct List_1_tF2126703F254B79E106F2178C60591A45C8E13D3;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t36D6DE3CB7FEC6EDF6EA749E72AD210F334D8F45;
// System.Collections.Hashtable
struct Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82;
// System.Collections.IComparer
struct IComparer_t971EE8726C43A8A086B35A11585E2AEFF2F7C2EB;
// System.Collections.IEnumerator
struct IEnumerator_t5F4AD85C6EA424A50584F741049EA645DBD8EEFC;
// System.Collections.IEqualityComparer
struct IEqualityComparer_tFBE0A1A09BAE01058D6DE65DD4FE16C50CB8E781;
// System.Collections.IHashCodeProvider
struct IHashCodeProvider_t2302C769B9686FD002965B00B8A3704D828517D5;
// System.Collections.Specialized.NameObjectCollectionBase/KeysCollection
struct KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB;
// System.Collections.Specialized.NameObjectCollectionBase/_Item
struct _Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7;
// System.ComponentModel.TypeConverter
struct TypeConverter_t5801C9F7100E1D849000ED0914E01E4CB2541B71;
// System.Configuration.ConfigInfoCollection
struct ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A;
// System.Configuration.ConfigNameValueCollection
struct ConfigNameValueCollection_t6BC2AF313208E6493F818D04E62F2C4ECE42A5CA;
// System.Configuration.Configuration
struct Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01;
// System.Configuration.ConfigurationCollectionAttribute
struct ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318;
// System.Configuration.ConfigurationElement
struct ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63;
// System.Configuration.ConfigurationElementCollection
struct ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E;
// System.Configuration.ConfigurationLockCollection
struct ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C;
// System.Configuration.ConfigurationProperty
struct ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400;
// System.Configuration.ConfigurationPropertyCollection
struct ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9;
// System.Configuration.ConfigurationSection
struct ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD;
// System.Configuration.ConfigurationSectionCollection
struct ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC;
// System.Configuration.ConfigurationSectionGroupCollection
struct ConfigurationSectionGroupCollection_t8FBD02E07771CB420A315A28E362FC9B48499052;
// System.Configuration.ConfigurationValidatorBase
struct ConfigurationValidatorBase_tD691540223153E9DEA98BDBD778BCB12A56248ED;
// System.Configuration.ElementInformation
struct ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26;
// System.Configuration.ElementMap
struct ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC;
// System.Configuration.ExeConfigurationFileMap
struct ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB;
// System.Configuration.IConfigurationSectionHandler
struct IConfigurationSectionHandler_t727B3154EE796E3224E5999FEA3F0B265095600F;
// System.Configuration.Internal.IInternalConfigHost
struct IInternalConfigHost_tF3B427C9E9DC4E866B1A69F1E347ABEC76BBCC8C;
// System.Configuration.Internal.IInternalConfigRoot
struct IInternalConfigRoot_tEE03567B39D4B468E1DA7A3F10DE367ED397FD56;
// System.Configuration.Internal.IInternalConfigSystem
struct IInternalConfigSystem_t6CAE984E794C32548107AF511D7DDB78B59B6131;
// System.Configuration.InternalConfigurationFactory
struct InternalConfigurationFactory_tDBD58CD148E84569F905006D415041A082593382;
// System.Configuration.PropertyInformation
struct PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF;
// System.Configuration.PropertyInformationCollection
struct PropertyInformationCollection_t6CBE051D205C8065A196AD13E4E5FA575B82AD52;
// System.Configuration.ProtectedConfigurationProvider
struct ProtectedConfigurationProvider_t6D1CFE3639AFF7A42A854B97BD575CFE05ED68A0;
// System.Configuration.ProtectedConfigurationProviderCollection
struct ProtectedConfigurationProviderCollection_t6F2A506BF598B7730852E9C57D207277687FBCDE;
// System.Configuration.SectionGroupInfo
struct SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61;
// System.Configuration.SectionInformation
struct SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A;
// System.DelegateData
struct DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9;
// System.EventArgs
struct EventArgs_tA4C15C1D2AB4B139169B1942C1477933E00DCA17;
// System.IAsyncResult
struct IAsyncResult_tDA33C24465239FB383C4C2CDAAC43B9AD3CB7F05;
// System.Int32[]
struct Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074;
// System.Object[]
struct ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2;
// System.Security.Cryptography.DSA
struct DSA_tF8DC81D58B7A574DF51517FD242D76B4FC225808;
// System.Security.Cryptography.HashAlgorithm
struct HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0;
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C;
// System.Security.Cryptography.RSA
struct RSA_t565B1E1F95589A0E37D0C66406CF730DC4A43051;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B;
// System.Type
struct Type_t;
// System.UInt32[]
struct UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5;
// System.Void
struct Void_tDB81A15FA2AB53E2401A76B745D215397B29F783;
// System.Xml.XmlReaderBinarySupport
struct XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB;




#ifndef U3CMODULEU3E_T2CD222707D881B4F268233C02FD6259FB0EB68C8_H
#define U3CMODULEU3E_T2CD222707D881B4F268233C02FD6259FB0EB68C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t2CD222707D881B4F268233C02FD6259FB0EB68C8 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T2CD222707D881B4F268233C02FD6259FB0EB68C8_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LOCALE_T98FA504E7F660E562AC58D271613184BD1DEF66F_H
#define LOCALE_T98FA504E7F660E562AC58D271613184BD1DEF66F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Locale
struct  Locale_t98FA504E7F660E562AC58D271613184BD1DEF66F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALE_T98FA504E7F660E562AC58D271613184BD1DEF66F_H
#ifndef BIGINTEGER_TC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E_H
#define BIGINTEGER_TC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger
struct  BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E  : public RuntimeObject
{
public:
	// System.UInt32 Mono.Math.BigInteger::length
	uint32_t ___length_0;
	// System.UInt32[] Mono.Math.BigInteger::data
	UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* ___data_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E, ___length_0)); }
	inline uint32_t get_length_0() const { return ___length_0; }
	inline uint32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(uint32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E, ___data_1)); }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* get_data_1() const { return ___data_1; }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

struct BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E_StaticFields
{
public:
	// System.UInt32[] Mono.Math.BigInteger::smallPrimes
	UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* ___smallPrimes_2;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Math.BigInteger::rng
	RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * ___rng_3;

public:
	inline static int32_t get_offset_of_smallPrimes_2() { return static_cast<int32_t>(offsetof(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E_StaticFields, ___smallPrimes_2)); }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* get_smallPrimes_2() const { return ___smallPrimes_2; }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5** get_address_of_smallPrimes_2() { return &___smallPrimes_2; }
	inline void set_smallPrimes_2(UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* value)
	{
		___smallPrimes_2 = value;
		Il2CppCodeGenWriteBarrier((&___smallPrimes_2), value);
	}

	inline static int32_t get_offset_of_rng_3() { return static_cast<int32_t>(offsetof(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E_StaticFields, ___rng_3)); }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * get_rng_3() const { return ___rng_3; }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 ** get_address_of_rng_3() { return &___rng_3; }
	inline void set_rng_3(RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * value)
	{
		___rng_3 = value;
		Il2CppCodeGenWriteBarrier((&___rng_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGER_TC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E_H
#ifndef KERNEL_TCDD9163849EF7FF83071E05F980FD8813EB50A37_H
#define KERNEL_TCDD9163849EF7FF83071E05F980FD8813EB50A37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger_Kernel
struct  Kernel_tCDD9163849EF7FF83071E05F980FD8813EB50A37  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNEL_TCDD9163849EF7FF83071E05F980FD8813EB50A37_H
#ifndef MODULUSRING_T5F514F6941DA17EC8D1FD6D170AAD9F9FA233541_H
#define MODULUSRING_T5F514F6941DA17EC8D1FD6D170AAD9F9FA233541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger_ModulusRing
struct  ModulusRing_t5F514F6941DA17EC8D1FD6D170AAD9F9FA233541  : public RuntimeObject
{
public:
	// Mono.Math.BigInteger Mono.Math.BigInteger_ModulusRing::mod
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * ___mod_0;
	// Mono.Math.BigInteger Mono.Math.BigInteger_ModulusRing::constant
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * ___constant_1;

public:
	inline static int32_t get_offset_of_mod_0() { return static_cast<int32_t>(offsetof(ModulusRing_t5F514F6941DA17EC8D1FD6D170AAD9F9FA233541, ___mod_0)); }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * get_mod_0() const { return ___mod_0; }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E ** get_address_of_mod_0() { return &___mod_0; }
	inline void set_mod_0(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * value)
	{
		___mod_0 = value;
		Il2CppCodeGenWriteBarrier((&___mod_0), value);
	}

	inline static int32_t get_offset_of_constant_1() { return static_cast<int32_t>(offsetof(ModulusRing_t5F514F6941DA17EC8D1FD6D170AAD9F9FA233541, ___constant_1)); }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * get_constant_1() const { return ___constant_1; }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E ** get_address_of_constant_1() { return &___constant_1; }
	inline void set_constant_1(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * value)
	{
		___constant_1 = value;
		Il2CppCodeGenWriteBarrier((&___constant_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULUSRING_T5F514F6941DA17EC8D1FD6D170AAD9F9FA233541_H
#ifndef PRIMEGENERATORBASE_TE8C14F85FCFF73ABFBC8B57DFE8F5A634DF477FE_H
#define PRIMEGENERATORBASE_TE8C14F85FCFF73ABFBC8B57DFE8F5A634DF477FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.Generator.PrimeGeneratorBase
struct  PrimeGeneratorBase_tE8C14F85FCFF73ABFBC8B57DFE8F5A634DF477FE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMEGENERATORBASE_TE8C14F85FCFF73ABFBC8B57DFE8F5A634DF477FE_H
#ifndef PRIMALITYTESTS_TE581B64657A89C85DBF15ED2C7C39C79AAFF7B70_H
#define PRIMALITYTESTS_TE581B64657A89C85DBF15ED2C7C39C79AAFF7B70_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.PrimalityTests
struct  PrimalityTests_tE581B64657A89C85DBF15ED2C7C39C79AAFF7B70  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMALITYTESTS_TE581B64657A89C85DBF15ED2C7C39C79AAFF7B70_H
#ifndef ASN1_T21F71D5E42416849A26C9ACE043F386E7F03D7FA_H
#define ASN1_T21F71D5E42416849A26C9ACE043F386E7F03D7FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.ASN1
struct  ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA  : public RuntimeObject
{
public:
	// System.Byte Mono.Security.ASN1::m_nTag
	uint8_t ___m_nTag_0;
	// System.Byte[] Mono.Security.ASN1::m_aValue
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___m_aValue_1;
	// System.Collections.ArrayList Mono.Security.ASN1::elist
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___elist_2;

public:
	inline static int32_t get_offset_of_m_nTag_0() { return static_cast<int32_t>(offsetof(ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA, ___m_nTag_0)); }
	inline uint8_t get_m_nTag_0() const { return ___m_nTag_0; }
	inline uint8_t* get_address_of_m_nTag_0() { return &___m_nTag_0; }
	inline void set_m_nTag_0(uint8_t value)
	{
		___m_nTag_0 = value;
	}

	inline static int32_t get_offset_of_m_aValue_1() { return static_cast<int32_t>(offsetof(ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA, ___m_aValue_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_m_aValue_1() const { return ___m_aValue_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_m_aValue_1() { return &___m_aValue_1; }
	inline void set_m_aValue_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___m_aValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_aValue_1), value);
	}

	inline static int32_t get_offset_of_elist_2() { return static_cast<int32_t>(offsetof(ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA, ___elist_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_elist_2() const { return ___elist_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_elist_2() { return &___elist_2; }
	inline void set_elist_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___elist_2 = value;
		Il2CppCodeGenWriteBarrier((&___elist_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1_T21F71D5E42416849A26C9ACE043F386E7F03D7FA_H
#ifndef ASN1CONVERT_TC746863213FE0090C01434A73EF88A69F48D093B_H
#define ASN1CONVERT_TC746863213FE0090C01434A73EF88A69F48D093B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.ASN1Convert
struct  ASN1Convert_tC746863213FE0090C01434A73EF88A69F48D093B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASN1CONVERT_TC746863213FE0090C01434A73EF88A69F48D093B_H
#ifndef BITCONVERTERLE_T7137470AF89208272C3686402978485B112F3F66_H
#define BITCONVERTERLE_T7137470AF89208272C3686402978485B112F3F66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.BitConverterLE
struct  BitConverterLE_t7137470AF89208272C3686402978485B112F3F66  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITCONVERTERLE_T7137470AF89208272C3686402978485B112F3F66_H
#ifndef CRYPTOCONVERT_T2C19EA750A34EFDF9DF6C017758C5C0E0191374C_H
#define CRYPTOCONVERT_T2C19EA750A34EFDF9DF6C017758C5C0E0191374C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.CryptoConvert
struct  CryptoConvert_t2C19EA750A34EFDF9DF6C017758C5C0E0191374C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRYPTOCONVERT_T2C19EA750A34EFDF9DF6C017758C5C0E0191374C_H
#ifndef KEYBUILDER_T3FFC7E782BE25F71226B048DAAFAA2CEB74C3C9C_H
#define KEYBUILDER_T3FFC7E782BE25F71226B048DAAFAA2CEB74C3C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.KeyBuilder
struct  KeyBuilder_t3FFC7E782BE25F71226B048DAAFAA2CEB74C3C9C  : public RuntimeObject
{
public:

public:
};

struct KeyBuilder_t3FFC7E782BE25F71226B048DAAFAA2CEB74C3C9C_StaticFields
{
public:
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.KeyBuilder::rng
	RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * ___rng_0;

public:
	inline static int32_t get_offset_of_rng_0() { return static_cast<int32_t>(offsetof(KeyBuilder_t3FFC7E782BE25F71226B048DAAFAA2CEB74C3C9C_StaticFields, ___rng_0)); }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * get_rng_0() const { return ___rng_0; }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 ** get_address_of_rng_0() { return &___rng_0; }
	inline void set_rng_0(RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * value)
	{
		___rng_0 = value;
		Il2CppCodeGenWriteBarrier((&___rng_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYBUILDER_T3FFC7E782BE25F71226B048DAAFAA2CEB74C3C9C_H
#ifndef PKCS1_TB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_H
#define PKCS1_TB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS1
struct  PKCS1_tB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B  : public RuntimeObject
{
public:

public:
};

struct PKCS1_tB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_StaticFields
{
public:
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA1
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___emptySHA1_0;
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA256
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___emptySHA256_1;
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA384
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___emptySHA384_2;
	// System.Byte[] Mono.Security.Cryptography.PKCS1::emptySHA512
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___emptySHA512_3;

public:
	inline static int32_t get_offset_of_emptySHA1_0() { return static_cast<int32_t>(offsetof(PKCS1_tB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_StaticFields, ___emptySHA1_0)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_emptySHA1_0() const { return ___emptySHA1_0; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_emptySHA1_0() { return &___emptySHA1_0; }
	inline void set_emptySHA1_0(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___emptySHA1_0 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA1_0), value);
	}

	inline static int32_t get_offset_of_emptySHA256_1() { return static_cast<int32_t>(offsetof(PKCS1_tB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_StaticFields, ___emptySHA256_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_emptySHA256_1() const { return ___emptySHA256_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_emptySHA256_1() { return &___emptySHA256_1; }
	inline void set_emptySHA256_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___emptySHA256_1 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA256_1), value);
	}

	inline static int32_t get_offset_of_emptySHA384_2() { return static_cast<int32_t>(offsetof(PKCS1_tB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_StaticFields, ___emptySHA384_2)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_emptySHA384_2() const { return ___emptySHA384_2; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_emptySHA384_2() { return &___emptySHA384_2; }
	inline void set_emptySHA384_2(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___emptySHA384_2 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA384_2), value);
	}

	inline static int32_t get_offset_of_emptySHA512_3() { return static_cast<int32_t>(offsetof(PKCS1_tB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_StaticFields, ___emptySHA512_3)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_emptySHA512_3() const { return ___emptySHA512_3; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_emptySHA512_3() { return &___emptySHA512_3; }
	inline void set_emptySHA512_3(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___emptySHA512_3 = value;
		Il2CppCodeGenWriteBarrier((&___emptySHA512_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS1_TB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_H
#ifndef PKCS8_TE374B294C7A9FC98A06529BA5AA4CB40800D35DF_H
#define PKCS8_TE374B294C7A9FC98A06529BA5AA4CB40800D35DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS8
struct  PKCS8_tE374B294C7A9FC98A06529BA5AA4CB40800D35DF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS8_TE374B294C7A9FC98A06529BA5AA4CB40800D35DF_H
#ifndef ENCRYPTEDPRIVATEKEYINFO_TEC05D1815F3B6911C2A72B36A0519FA958BFA86E_H
#define ENCRYPTEDPRIVATEKEYINFO_TEC05D1815F3B6911C2A72B36A0519FA958BFA86E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS8_EncryptedPrivateKeyInfo
struct  EncryptedPrivateKeyInfo_tEC05D1815F3B6911C2A72B36A0519FA958BFA86E  : public RuntimeObject
{
public:
	// System.String Mono.Security.Cryptography.PKCS8_EncryptedPrivateKeyInfo::_algorithm
	String_t* ____algorithm_0;
	// System.Byte[] Mono.Security.Cryptography.PKCS8_EncryptedPrivateKeyInfo::_salt
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ____salt_1;
	// System.Int32 Mono.Security.Cryptography.PKCS8_EncryptedPrivateKeyInfo::_iterations
	int32_t ____iterations_2;
	// System.Byte[] Mono.Security.Cryptography.PKCS8_EncryptedPrivateKeyInfo::_data
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ____data_3;

public:
	inline static int32_t get_offset_of__algorithm_0() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_tEC05D1815F3B6911C2A72B36A0519FA958BFA86E, ____algorithm_0)); }
	inline String_t* get__algorithm_0() const { return ____algorithm_0; }
	inline String_t** get_address_of__algorithm_0() { return &____algorithm_0; }
	inline void set__algorithm_0(String_t* value)
	{
		____algorithm_0 = value;
		Il2CppCodeGenWriteBarrier((&____algorithm_0), value);
	}

	inline static int32_t get_offset_of__salt_1() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_tEC05D1815F3B6911C2A72B36A0519FA958BFA86E, ____salt_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get__salt_1() const { return ____salt_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of__salt_1() { return &____salt_1; }
	inline void set__salt_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		____salt_1 = value;
		Il2CppCodeGenWriteBarrier((&____salt_1), value);
	}

	inline static int32_t get_offset_of__iterations_2() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_tEC05D1815F3B6911C2A72B36A0519FA958BFA86E, ____iterations_2)); }
	inline int32_t get__iterations_2() const { return ____iterations_2; }
	inline int32_t* get_address_of__iterations_2() { return &____iterations_2; }
	inline void set__iterations_2(int32_t value)
	{
		____iterations_2 = value;
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(EncryptedPrivateKeyInfo_tEC05D1815F3B6911C2A72B36A0519FA958BFA86E, ____data_3)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get__data_3() const { return ____data_3; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDPRIVATEKEYINFO_TEC05D1815F3B6911C2A72B36A0519FA958BFA86E_H
#ifndef PRIVATEKEYINFO_T9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D_H
#define PRIVATEKEYINFO_T9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.PKCS8_PrivateKeyInfo
struct  PrivateKeyInfo_t9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D  : public RuntimeObject
{
public:
	// System.Int32 Mono.Security.Cryptography.PKCS8_PrivateKeyInfo::_version
	int32_t ____version_0;
	// System.String Mono.Security.Cryptography.PKCS8_PrivateKeyInfo::_algorithm
	String_t* ____algorithm_1;
	// System.Byte[] Mono.Security.Cryptography.PKCS8_PrivateKeyInfo::_key
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ____key_2;
	// System.Collections.ArrayList Mono.Security.Cryptography.PKCS8_PrivateKeyInfo::_list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ____list_3;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D, ____version_0)); }
	inline int32_t get__version_0() const { return ____version_0; }
	inline int32_t* get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(int32_t value)
	{
		____version_0 = value;
	}

	inline static int32_t get_offset_of__algorithm_1() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D, ____algorithm_1)); }
	inline String_t* get__algorithm_1() const { return ____algorithm_1; }
	inline String_t** get_address_of__algorithm_1() { return &____algorithm_1; }
	inline void set__algorithm_1(String_t* value)
	{
		____algorithm_1 = value;
		Il2CppCodeGenWriteBarrier((&____algorithm_1), value);
	}

	inline static int32_t get_offset_of__key_2() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D, ____key_2)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get__key_2() const { return ____key_2; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of__key_2() { return &____key_2; }
	inline void set__key_2(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		____key_2 = value;
		Il2CppCodeGenWriteBarrier((&____key_2), value);
	}

	inline static int32_t get_offset_of__list_3() { return static_cast<int32_t>(offsetof(PrivateKeyInfo_t9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D, ____list_3)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get__list_3() const { return ____list_3; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of__list_3() { return &____list_3; }
	inline void set__list_3(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		____list_3 = value;
		Il2CppCodeGenWriteBarrier((&____list_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIVATEKEYINFO_T9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D_H
#ifndef PKCS7_T3D2A0269E93DED7E5D15D8C7FF375E7FD2D0AFA0_H
#define PKCS7_T3D2A0269E93DED7E5D15D8C7FF375E7FD2D0AFA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.PKCS7
struct  PKCS7_t3D2A0269E93DED7E5D15D8C7FF375E7FD2D0AFA0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS7_T3D2A0269E93DED7E5D15D8C7FF375E7FD2D0AFA0_H
#ifndef CONTENTINFO_T51049CE410146B36AF37C7218807051D5996866F_H
#define CONTENTINFO_T51049CE410146B36AF37C7218807051D5996866F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.PKCS7_ContentInfo
struct  ContentInfo_t51049CE410146B36AF37C7218807051D5996866F  : public RuntimeObject
{
public:
	// System.String Mono.Security.PKCS7_ContentInfo::contentType
	String_t* ___contentType_0;
	// Mono.Security.ASN1 Mono.Security.PKCS7_ContentInfo::content
	ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * ___content_1;

public:
	inline static int32_t get_offset_of_contentType_0() { return static_cast<int32_t>(offsetof(ContentInfo_t51049CE410146B36AF37C7218807051D5996866F, ___contentType_0)); }
	inline String_t* get_contentType_0() const { return ___contentType_0; }
	inline String_t** get_address_of_contentType_0() { return &___contentType_0; }
	inline void set_contentType_0(String_t* value)
	{
		___contentType_0 = value;
		Il2CppCodeGenWriteBarrier((&___contentType_0), value);
	}

	inline static int32_t get_offset_of_content_1() { return static_cast<int32_t>(offsetof(ContentInfo_t51049CE410146B36AF37C7218807051D5996866F, ___content_1)); }
	inline ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * get_content_1() const { return ___content_1; }
	inline ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA ** get_address_of_content_1() { return &___content_1; }
	inline void set_content_1(ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * value)
	{
		___content_1 = value;
		Il2CppCodeGenWriteBarrier((&___content_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTINFO_T51049CE410146B36AF37C7218807051D5996866F_H
#ifndef ENCRYPTEDDATA_T3170C44CDA01A6629A7A44AA069A720A5326EB2C_H
#define ENCRYPTEDDATA_T3170C44CDA01A6629A7A44AA069A720A5326EB2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.PKCS7_EncryptedData
struct  EncryptedData_t3170C44CDA01A6629A7A44AA069A720A5326EB2C  : public RuntimeObject
{
public:
	// System.Byte Mono.Security.PKCS7_EncryptedData::_version
	uint8_t ____version_0;
	// Mono.Security.PKCS7_ContentInfo Mono.Security.PKCS7_EncryptedData::_content
	ContentInfo_t51049CE410146B36AF37C7218807051D5996866F * ____content_1;
	// Mono.Security.PKCS7_ContentInfo Mono.Security.PKCS7_EncryptedData::_encryptionAlgorithm
	ContentInfo_t51049CE410146B36AF37C7218807051D5996866F * ____encryptionAlgorithm_2;
	// System.Byte[] Mono.Security.PKCS7_EncryptedData::_encrypted
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ____encrypted_3;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(EncryptedData_t3170C44CDA01A6629A7A44AA069A720A5326EB2C, ____version_0)); }
	inline uint8_t get__version_0() const { return ____version_0; }
	inline uint8_t* get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(uint8_t value)
	{
		____version_0 = value;
	}

	inline static int32_t get_offset_of__content_1() { return static_cast<int32_t>(offsetof(EncryptedData_t3170C44CDA01A6629A7A44AA069A720A5326EB2C, ____content_1)); }
	inline ContentInfo_t51049CE410146B36AF37C7218807051D5996866F * get__content_1() const { return ____content_1; }
	inline ContentInfo_t51049CE410146B36AF37C7218807051D5996866F ** get_address_of__content_1() { return &____content_1; }
	inline void set__content_1(ContentInfo_t51049CE410146B36AF37C7218807051D5996866F * value)
	{
		____content_1 = value;
		Il2CppCodeGenWriteBarrier((&____content_1), value);
	}

	inline static int32_t get_offset_of__encryptionAlgorithm_2() { return static_cast<int32_t>(offsetof(EncryptedData_t3170C44CDA01A6629A7A44AA069A720A5326EB2C, ____encryptionAlgorithm_2)); }
	inline ContentInfo_t51049CE410146B36AF37C7218807051D5996866F * get__encryptionAlgorithm_2() const { return ____encryptionAlgorithm_2; }
	inline ContentInfo_t51049CE410146B36AF37C7218807051D5996866F ** get_address_of__encryptionAlgorithm_2() { return &____encryptionAlgorithm_2; }
	inline void set__encryptionAlgorithm_2(ContentInfo_t51049CE410146B36AF37C7218807051D5996866F * value)
	{
		____encryptionAlgorithm_2 = value;
		Il2CppCodeGenWriteBarrier((&____encryptionAlgorithm_2), value);
	}

	inline static int32_t get_offset_of__encrypted_3() { return static_cast<int32_t>(offsetof(EncryptedData_t3170C44CDA01A6629A7A44AA069A720A5326EB2C, ____encrypted_3)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get__encrypted_3() const { return ____encrypted_3; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of__encrypted_3() { return &____encrypted_3; }
	inline void set__encrypted_3(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		____encrypted_3 = value;
		Il2CppCodeGenWriteBarrier((&____encrypted_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCRYPTEDDATA_T3170C44CDA01A6629A7A44AA069A720A5326EB2C_H
#ifndef GENERALNAMES_TADF51C0BC03F95F71335B31D2E417D8A6E1F9F45_H
#define GENERALNAMES_TADF51C0BC03F95F71335B31D2E417D8A6E1F9F45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.GeneralNames
struct  GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45  : public RuntimeObject
{
public:
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::rfc822Name
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___rfc822Name_0;
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::dnsName
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___dnsName_1;
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::directoryNames
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___directoryNames_2;
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::uris
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___uris_3;
	// System.Collections.ArrayList Mono.Security.X509.Extensions.GeneralNames::ipAddr
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___ipAddr_4;

public:
	inline static int32_t get_offset_of_rfc822Name_0() { return static_cast<int32_t>(offsetof(GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45, ___rfc822Name_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_rfc822Name_0() const { return ___rfc822Name_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_rfc822Name_0() { return &___rfc822Name_0; }
	inline void set_rfc822Name_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___rfc822Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___rfc822Name_0), value);
	}

	inline static int32_t get_offset_of_dnsName_1() { return static_cast<int32_t>(offsetof(GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45, ___dnsName_1)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_dnsName_1() const { return ___dnsName_1; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_dnsName_1() { return &___dnsName_1; }
	inline void set_dnsName_1(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___dnsName_1 = value;
		Il2CppCodeGenWriteBarrier((&___dnsName_1), value);
	}

	inline static int32_t get_offset_of_directoryNames_2() { return static_cast<int32_t>(offsetof(GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45, ___directoryNames_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_directoryNames_2() const { return ___directoryNames_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_directoryNames_2() { return &___directoryNames_2; }
	inline void set_directoryNames_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___directoryNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___directoryNames_2), value);
	}

	inline static int32_t get_offset_of_uris_3() { return static_cast<int32_t>(offsetof(GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45, ___uris_3)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_uris_3() const { return ___uris_3; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_uris_3() { return &___uris_3; }
	inline void set_uris_3(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___uris_3 = value;
		Il2CppCodeGenWriteBarrier((&___uris_3), value);
	}

	inline static int32_t get_offset_of_ipAddr_4() { return static_cast<int32_t>(offsetof(GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45, ___ipAddr_4)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_ipAddr_4() const { return ___ipAddr_4; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_ipAddr_4() { return &___ipAddr_4; }
	inline void set_ipAddr_4(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___ipAddr_4 = value;
		Il2CppCodeGenWriteBarrier((&___ipAddr_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERALNAMES_TADF51C0BC03F95F71335B31D2E417D8A6E1F9F45_H
#ifndef PKCS12_TED2078A34511432AD97B7B647986EF0858868441_H
#define PKCS12_TED2078A34511432AD97B7B647986EF0858868441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.PKCS12
struct  PKCS12_tED2078A34511432AD97B7B647986EF0858868441  : public RuntimeObject
{
public:
	// System.Byte[] Mono.Security.X509.PKCS12::_password
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ____password_1;
	// System.Collections.ArrayList Mono.Security.X509.PKCS12::_keyBags
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ____keyBags_2;
	// System.Collections.ArrayList Mono.Security.X509.PKCS12::_secretBags
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ____secretBags_3;
	// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.PKCS12::_certs
	X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * ____certs_4;
	// System.Boolean Mono.Security.X509.PKCS12::_keyBagsChanged
	bool ____keyBagsChanged_5;
	// System.Boolean Mono.Security.X509.PKCS12::_secretBagsChanged
	bool ____secretBagsChanged_6;
	// System.Boolean Mono.Security.X509.PKCS12::_certsChanged
	bool ____certsChanged_7;
	// System.Int32 Mono.Security.X509.PKCS12::_iterations
	int32_t ____iterations_8;
	// System.Collections.ArrayList Mono.Security.X509.PKCS12::_safeBags
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ____safeBags_9;
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.X509.PKCS12::_rng
	RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * ____rng_10;

public:
	inline static int32_t get_offset_of__password_1() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441, ____password_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get__password_1() const { return ____password_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of__password_1() { return &____password_1; }
	inline void set__password_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		____password_1 = value;
		Il2CppCodeGenWriteBarrier((&____password_1), value);
	}

	inline static int32_t get_offset_of__keyBags_2() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441, ____keyBags_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get__keyBags_2() const { return ____keyBags_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of__keyBags_2() { return &____keyBags_2; }
	inline void set__keyBags_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		____keyBags_2 = value;
		Il2CppCodeGenWriteBarrier((&____keyBags_2), value);
	}

	inline static int32_t get_offset_of__secretBags_3() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441, ____secretBags_3)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get__secretBags_3() const { return ____secretBags_3; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of__secretBags_3() { return &____secretBags_3; }
	inline void set__secretBags_3(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		____secretBags_3 = value;
		Il2CppCodeGenWriteBarrier((&____secretBags_3), value);
	}

	inline static int32_t get_offset_of__certs_4() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441, ____certs_4)); }
	inline X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * get__certs_4() const { return ____certs_4; }
	inline X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 ** get_address_of__certs_4() { return &____certs_4; }
	inline void set__certs_4(X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * value)
	{
		____certs_4 = value;
		Il2CppCodeGenWriteBarrier((&____certs_4), value);
	}

	inline static int32_t get_offset_of__keyBagsChanged_5() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441, ____keyBagsChanged_5)); }
	inline bool get__keyBagsChanged_5() const { return ____keyBagsChanged_5; }
	inline bool* get_address_of__keyBagsChanged_5() { return &____keyBagsChanged_5; }
	inline void set__keyBagsChanged_5(bool value)
	{
		____keyBagsChanged_5 = value;
	}

	inline static int32_t get_offset_of__secretBagsChanged_6() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441, ____secretBagsChanged_6)); }
	inline bool get__secretBagsChanged_6() const { return ____secretBagsChanged_6; }
	inline bool* get_address_of__secretBagsChanged_6() { return &____secretBagsChanged_6; }
	inline void set__secretBagsChanged_6(bool value)
	{
		____secretBagsChanged_6 = value;
	}

	inline static int32_t get_offset_of__certsChanged_7() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441, ____certsChanged_7)); }
	inline bool get__certsChanged_7() const { return ____certsChanged_7; }
	inline bool* get_address_of__certsChanged_7() { return &____certsChanged_7; }
	inline void set__certsChanged_7(bool value)
	{
		____certsChanged_7 = value;
	}

	inline static int32_t get_offset_of__iterations_8() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441, ____iterations_8)); }
	inline int32_t get__iterations_8() const { return ____iterations_8; }
	inline int32_t* get_address_of__iterations_8() { return &____iterations_8; }
	inline void set__iterations_8(int32_t value)
	{
		____iterations_8 = value;
	}

	inline static int32_t get_offset_of__safeBags_9() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441, ____safeBags_9)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get__safeBags_9() const { return ____safeBags_9; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of__safeBags_9() { return &____safeBags_9; }
	inline void set__safeBags_9(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		____safeBags_9 = value;
		Il2CppCodeGenWriteBarrier((&____safeBags_9), value);
	}

	inline static int32_t get_offset_of__rng_10() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441, ____rng_10)); }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * get__rng_10() const { return ____rng_10; }
	inline RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 ** get_address_of__rng_10() { return &____rng_10; }
	inline void set__rng_10(RandomNumberGenerator_t7D49CE0AB6123507423CD8702694F3D6CFD54DA0 * value)
	{
		____rng_10 = value;
		Il2CppCodeGenWriteBarrier((&____rng_10), value);
	}
};

struct PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields
{
public:
	// System.Int32 Mono.Security.X509.PKCS12::recommendedIterationCount
	int32_t ___recommendedIterationCount_0;
	// System.Int32 Mono.Security.X509.PKCS12::password_max_length
	int32_t ___password_max_length_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.PKCS12::<>f__switchU24map5
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map5_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.PKCS12::<>f__switchU24map6
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map6_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.PKCS12::<>f__switchU24map7
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map7_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.PKCS12::<>f__switchU24map8
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map8_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.PKCS12::<>f__switchU24mapC
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24mapC_16;

public:
	inline static int32_t get_offset_of_recommendedIterationCount_0() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields, ___recommendedIterationCount_0)); }
	inline int32_t get_recommendedIterationCount_0() const { return ___recommendedIterationCount_0; }
	inline int32_t* get_address_of_recommendedIterationCount_0() { return &___recommendedIterationCount_0; }
	inline void set_recommendedIterationCount_0(int32_t value)
	{
		___recommendedIterationCount_0 = value;
	}

	inline static int32_t get_offset_of_password_max_length_11() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields, ___password_max_length_11)); }
	inline int32_t get_password_max_length_11() const { return ___password_max_length_11; }
	inline int32_t* get_address_of_password_max_length_11() { return &___password_max_length_11; }
	inline void set_password_max_length_11(int32_t value)
	{
		___password_max_length_11 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_12() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields, ___U3CU3Ef__switchU24map5_12)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map5_12() const { return ___U3CU3Ef__switchU24map5_12; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map5_12() { return &___U3CU3Ef__switchU24map5_12; }
	inline void set_U3CU3Ef__switchU24map5_12(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map5_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map6_13() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields, ___U3CU3Ef__switchU24map6_13)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map6_13() const { return ___U3CU3Ef__switchU24map6_13; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map6_13() { return &___U3CU3Ef__switchU24map6_13; }
	inline void set_U3CU3Ef__switchU24map6_13(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map6_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map6_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map7_14() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields, ___U3CU3Ef__switchU24map7_14)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map7_14() const { return ___U3CU3Ef__switchU24map7_14; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map7_14() { return &___U3CU3Ef__switchU24map7_14; }
	inline void set_U3CU3Ef__switchU24map7_14(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map7_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map7_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map8_15() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields, ___U3CU3Ef__switchU24map8_15)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map8_15() const { return ___U3CU3Ef__switchU24map8_15; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map8_15() { return &___U3CU3Ef__switchU24map8_15; }
	inline void set_U3CU3Ef__switchU24map8_15(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map8_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map8_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapC_16() { return static_cast<int32_t>(offsetof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields, ___U3CU3Ef__switchU24mapC_16)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24mapC_16() const { return ___U3CU3Ef__switchU24mapC_16; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24mapC_16() { return &___U3CU3Ef__switchU24mapC_16; }
	inline void set_U3CU3Ef__switchU24mapC_16(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24mapC_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapC_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PKCS12_TED2078A34511432AD97B7B647986EF0858868441_H
#ifndef DERIVEBYTES_T9C20DD7277261025CA74878A4C04D066B5211460_H
#define DERIVEBYTES_T9C20DD7277261025CA74878A4C04D066B5211460_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.PKCS12_DeriveBytes
struct  DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.PKCS12_DeriveBytes::_hashName
	String_t* ____hashName_3;
	// System.Int32 Mono.Security.X509.PKCS12_DeriveBytes::_iterations
	int32_t ____iterations_4;
	// System.Byte[] Mono.Security.X509.PKCS12_DeriveBytes::_password
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ____password_5;
	// System.Byte[] Mono.Security.X509.PKCS12_DeriveBytes::_salt
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ____salt_6;

public:
	inline static int32_t get_offset_of__hashName_3() { return static_cast<int32_t>(offsetof(DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460, ____hashName_3)); }
	inline String_t* get__hashName_3() const { return ____hashName_3; }
	inline String_t** get_address_of__hashName_3() { return &____hashName_3; }
	inline void set__hashName_3(String_t* value)
	{
		____hashName_3 = value;
		Il2CppCodeGenWriteBarrier((&____hashName_3), value);
	}

	inline static int32_t get_offset_of__iterations_4() { return static_cast<int32_t>(offsetof(DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460, ____iterations_4)); }
	inline int32_t get__iterations_4() const { return ____iterations_4; }
	inline int32_t* get_address_of__iterations_4() { return &____iterations_4; }
	inline void set__iterations_4(int32_t value)
	{
		____iterations_4 = value;
	}

	inline static int32_t get_offset_of__password_5() { return static_cast<int32_t>(offsetof(DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460, ____password_5)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get__password_5() const { return ____password_5; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of__password_5() { return &____password_5; }
	inline void set__password_5(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		____password_5 = value;
		Il2CppCodeGenWriteBarrier((&____password_5), value);
	}

	inline static int32_t get_offset_of__salt_6() { return static_cast<int32_t>(offsetof(DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460, ____salt_6)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get__salt_6() const { return ____salt_6; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of__salt_6() { return &____salt_6; }
	inline void set__salt_6(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		____salt_6 = value;
		Il2CppCodeGenWriteBarrier((&____salt_6), value);
	}
};

struct DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460_StaticFields
{
public:
	// System.Byte[] Mono.Security.X509.PKCS12_DeriveBytes::keyDiversifier
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___keyDiversifier_0;
	// System.Byte[] Mono.Security.X509.PKCS12_DeriveBytes::ivDiversifier
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___ivDiversifier_1;
	// System.Byte[] Mono.Security.X509.PKCS12_DeriveBytes::macDiversifier
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___macDiversifier_2;

public:
	inline static int32_t get_offset_of_keyDiversifier_0() { return static_cast<int32_t>(offsetof(DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460_StaticFields, ___keyDiversifier_0)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_keyDiversifier_0() const { return ___keyDiversifier_0; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_keyDiversifier_0() { return &___keyDiversifier_0; }
	inline void set_keyDiversifier_0(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___keyDiversifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___keyDiversifier_0), value);
	}

	inline static int32_t get_offset_of_ivDiversifier_1() { return static_cast<int32_t>(offsetof(DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460_StaticFields, ___ivDiversifier_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_ivDiversifier_1() const { return ___ivDiversifier_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_ivDiversifier_1() { return &___ivDiversifier_1; }
	inline void set_ivDiversifier_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___ivDiversifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___ivDiversifier_1), value);
	}

	inline static int32_t get_offset_of_macDiversifier_2() { return static_cast<int32_t>(offsetof(DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460_StaticFields, ___macDiversifier_2)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_macDiversifier_2() const { return ___macDiversifier_2; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_macDiversifier_2() { return &___macDiversifier_2; }
	inline void set_macDiversifier_2(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___macDiversifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___macDiversifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DERIVEBYTES_T9C20DD7277261025CA74878A4C04D066B5211460_H
#ifndef SAFEBAG_TDDDE39DC5EA8E3F77F5E1502E73A97224B77AAC3_H
#define SAFEBAG_TDDDE39DC5EA8E3F77F5E1502E73A97224B77AAC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.SafeBag
struct  SafeBag_tDDDE39DC5EA8E3F77F5E1502E73A97224B77AAC3  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.SafeBag::_bagOID
	String_t* ____bagOID_0;
	// Mono.Security.ASN1 Mono.Security.X509.SafeBag::_asn1
	ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * ____asn1_1;

public:
	inline static int32_t get_offset_of__bagOID_0() { return static_cast<int32_t>(offsetof(SafeBag_tDDDE39DC5EA8E3F77F5E1502E73A97224B77AAC3, ____bagOID_0)); }
	inline String_t* get__bagOID_0() const { return ____bagOID_0; }
	inline String_t** get_address_of__bagOID_0() { return &____bagOID_0; }
	inline void set__bagOID_0(String_t* value)
	{
		____bagOID_0 = value;
		Il2CppCodeGenWriteBarrier((&____bagOID_0), value);
	}

	inline static int32_t get_offset_of__asn1_1() { return static_cast<int32_t>(offsetof(SafeBag_tDDDE39DC5EA8E3F77F5E1502E73A97224B77AAC3, ____asn1_1)); }
	inline ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * get__asn1_1() const { return ____asn1_1; }
	inline ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA ** get_address_of__asn1_1() { return &____asn1_1; }
	inline void set__asn1_1(ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * value)
	{
		____asn1_1 = value;
		Il2CppCodeGenWriteBarrier((&____asn1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAFEBAG_TDDDE39DC5EA8E3F77F5E1502E73A97224B77AAC3_H
#ifndef X501_TE168FA15DF59DEE465AFDD39E5B8B51B857605F4_H
#define X501_TE168FA15DF59DEE465AFDD39E5B8B51B857605F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X501
struct  X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4  : public RuntimeObject
{
public:

public:
};

struct X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields
{
public:
	// System.Byte[] Mono.Security.X509.X501::countryName
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___countryName_0;
	// System.Byte[] Mono.Security.X509.X501::organizationName
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___organizationName_1;
	// System.Byte[] Mono.Security.X509.X501::organizationalUnitName
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___organizationalUnitName_2;
	// System.Byte[] Mono.Security.X509.X501::commonName
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___commonName_3;
	// System.Byte[] Mono.Security.X509.X501::localityName
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___localityName_4;
	// System.Byte[] Mono.Security.X509.X501::stateOrProvinceName
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___stateOrProvinceName_5;
	// System.Byte[] Mono.Security.X509.X501::streetAddress
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___streetAddress_6;
	// System.Byte[] Mono.Security.X509.X501::domainComponent
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___domainComponent_7;
	// System.Byte[] Mono.Security.X509.X501::userid
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___userid_8;
	// System.Byte[] Mono.Security.X509.X501::email
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___email_9;
	// System.Byte[] Mono.Security.X509.X501::dnQualifier
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___dnQualifier_10;
	// System.Byte[] Mono.Security.X509.X501::title
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___title_11;
	// System.Byte[] Mono.Security.X509.X501::surname
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___surname_12;
	// System.Byte[] Mono.Security.X509.X501::givenName
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___givenName_13;
	// System.Byte[] Mono.Security.X509.X501::initial
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___initial_14;

public:
	inline static int32_t get_offset_of_countryName_0() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___countryName_0)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_countryName_0() const { return ___countryName_0; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_countryName_0() { return &___countryName_0; }
	inline void set_countryName_0(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___countryName_0 = value;
		Il2CppCodeGenWriteBarrier((&___countryName_0), value);
	}

	inline static int32_t get_offset_of_organizationName_1() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___organizationName_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_organizationName_1() const { return ___organizationName_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_organizationName_1() { return &___organizationName_1; }
	inline void set_organizationName_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___organizationName_1 = value;
		Il2CppCodeGenWriteBarrier((&___organizationName_1), value);
	}

	inline static int32_t get_offset_of_organizationalUnitName_2() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___organizationalUnitName_2)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_organizationalUnitName_2() const { return ___organizationalUnitName_2; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_organizationalUnitName_2() { return &___organizationalUnitName_2; }
	inline void set_organizationalUnitName_2(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___organizationalUnitName_2 = value;
		Il2CppCodeGenWriteBarrier((&___organizationalUnitName_2), value);
	}

	inline static int32_t get_offset_of_commonName_3() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___commonName_3)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_commonName_3() const { return ___commonName_3; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_commonName_3() { return &___commonName_3; }
	inline void set_commonName_3(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___commonName_3 = value;
		Il2CppCodeGenWriteBarrier((&___commonName_3), value);
	}

	inline static int32_t get_offset_of_localityName_4() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___localityName_4)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_localityName_4() const { return ___localityName_4; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_localityName_4() { return &___localityName_4; }
	inline void set_localityName_4(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___localityName_4 = value;
		Il2CppCodeGenWriteBarrier((&___localityName_4), value);
	}

	inline static int32_t get_offset_of_stateOrProvinceName_5() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___stateOrProvinceName_5)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_stateOrProvinceName_5() const { return ___stateOrProvinceName_5; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_stateOrProvinceName_5() { return &___stateOrProvinceName_5; }
	inline void set_stateOrProvinceName_5(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___stateOrProvinceName_5 = value;
		Il2CppCodeGenWriteBarrier((&___stateOrProvinceName_5), value);
	}

	inline static int32_t get_offset_of_streetAddress_6() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___streetAddress_6)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_streetAddress_6() const { return ___streetAddress_6; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_streetAddress_6() { return &___streetAddress_6; }
	inline void set_streetAddress_6(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___streetAddress_6 = value;
		Il2CppCodeGenWriteBarrier((&___streetAddress_6), value);
	}

	inline static int32_t get_offset_of_domainComponent_7() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___domainComponent_7)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_domainComponent_7() const { return ___domainComponent_7; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_domainComponent_7() { return &___domainComponent_7; }
	inline void set_domainComponent_7(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___domainComponent_7 = value;
		Il2CppCodeGenWriteBarrier((&___domainComponent_7), value);
	}

	inline static int32_t get_offset_of_userid_8() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___userid_8)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_userid_8() const { return ___userid_8; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_userid_8() { return &___userid_8; }
	inline void set_userid_8(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___userid_8 = value;
		Il2CppCodeGenWriteBarrier((&___userid_8), value);
	}

	inline static int32_t get_offset_of_email_9() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___email_9)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_email_9() const { return ___email_9; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_email_9() { return &___email_9; }
	inline void set_email_9(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___email_9 = value;
		Il2CppCodeGenWriteBarrier((&___email_9), value);
	}

	inline static int32_t get_offset_of_dnQualifier_10() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___dnQualifier_10)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_dnQualifier_10() const { return ___dnQualifier_10; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_dnQualifier_10() { return &___dnQualifier_10; }
	inline void set_dnQualifier_10(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___dnQualifier_10 = value;
		Il2CppCodeGenWriteBarrier((&___dnQualifier_10), value);
	}

	inline static int32_t get_offset_of_title_11() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___title_11)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_title_11() const { return ___title_11; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_title_11() { return &___title_11; }
	inline void set_title_11(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___title_11 = value;
		Il2CppCodeGenWriteBarrier((&___title_11), value);
	}

	inline static int32_t get_offset_of_surname_12() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___surname_12)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_surname_12() const { return ___surname_12; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_surname_12() { return &___surname_12; }
	inline void set_surname_12(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___surname_12 = value;
		Il2CppCodeGenWriteBarrier((&___surname_12), value);
	}

	inline static int32_t get_offset_of_givenName_13() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___givenName_13)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_givenName_13() const { return ___givenName_13; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_givenName_13() { return &___givenName_13; }
	inline void set_givenName_13(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___givenName_13 = value;
		Il2CppCodeGenWriteBarrier((&___givenName_13), value);
	}

	inline static int32_t get_offset_of_initial_14() { return static_cast<int32_t>(offsetof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields, ___initial_14)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_initial_14() const { return ___initial_14; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_initial_14() { return &___initial_14; }
	inline void set_initial_14(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___initial_14 = value;
		Il2CppCodeGenWriteBarrier((&___initial_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X501_TE168FA15DF59DEE465AFDD39E5B8B51B857605F4_H
#ifndef X509CERTIFICATEENUMERATOR_T73F2B8AEDEF6F187870E8A2750847EEE931E16D3_H
#define X509CERTIFICATEENUMERATOR_T73F2B8AEDEF6F187870E8A2750847EEE931E16D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509CertificateCollection_X509CertificateEnumerator
struct  X509CertificateEnumerator_t73F2B8AEDEF6F187870E8A2750847EEE931E16D3  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator Mono.Security.X509.X509CertificateCollection_X509CertificateEnumerator::enumerator
	RuntimeObject* ___enumerator_0;

public:
	inline static int32_t get_offset_of_enumerator_0() { return static_cast<int32_t>(offsetof(X509CertificateEnumerator_t73F2B8AEDEF6F187870E8A2750847EEE931E16D3, ___enumerator_0)); }
	inline RuntimeObject* get_enumerator_0() const { return ___enumerator_0; }
	inline RuntimeObject** get_address_of_enumerator_0() { return &___enumerator_0; }
	inline void set_enumerator_0(RuntimeObject* value)
	{
		___enumerator_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumerator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATEENUMERATOR_T73F2B8AEDEF6F187870E8A2750847EEE931E16D3_H
#ifndef X509EXTENSION_TE4755045C6BD26F3323152F1E6F86FCDC72D6698_H
#define X509EXTENSION_TE4755045C6BD26F3323152F1E6F86FCDC72D6698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Extension
struct  X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X509Extension::extnOid
	String_t* ___extnOid_0;
	// System.Boolean Mono.Security.X509.X509Extension::extnCritical
	bool ___extnCritical_1;
	// Mono.Security.ASN1 Mono.Security.X509.X509Extension::extnValue
	ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * ___extnValue_2;

public:
	inline static int32_t get_offset_of_extnOid_0() { return static_cast<int32_t>(offsetof(X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698, ___extnOid_0)); }
	inline String_t* get_extnOid_0() const { return ___extnOid_0; }
	inline String_t** get_address_of_extnOid_0() { return &___extnOid_0; }
	inline void set_extnOid_0(String_t* value)
	{
		___extnOid_0 = value;
		Il2CppCodeGenWriteBarrier((&___extnOid_0), value);
	}

	inline static int32_t get_offset_of_extnCritical_1() { return static_cast<int32_t>(offsetof(X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698, ___extnCritical_1)); }
	inline bool get_extnCritical_1() const { return ___extnCritical_1; }
	inline bool* get_address_of_extnCritical_1() { return &___extnCritical_1; }
	inline void set_extnCritical_1(bool value)
	{
		___extnCritical_1 = value;
	}

	inline static int32_t get_offset_of_extnValue_2() { return static_cast<int32_t>(offsetof(X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698, ___extnValue_2)); }
	inline ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * get_extnValue_2() const { return ___extnValue_2; }
	inline ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA ** get_address_of_extnValue_2() { return &___extnValue_2; }
	inline void set_extnValue_2(ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * value)
	{
		___extnValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___extnValue_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSION_TE4755045C6BD26F3323152F1E6F86FCDC72D6698_H
#ifndef X509STORE_T754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302_H
#define X509STORE_T754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Store
struct  X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X509Store::_storePath
	String_t* ____storePath_0;
	// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Store::_certificates
	X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * ____certificates_1;
	// System.Collections.ArrayList Mono.Security.X509.X509Store::_crls
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ____crls_2;
	// System.Boolean Mono.Security.X509.X509Store::_crl
	bool ____crl_3;

public:
	inline static int32_t get_offset_of__storePath_0() { return static_cast<int32_t>(offsetof(X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302, ____storePath_0)); }
	inline String_t* get__storePath_0() const { return ____storePath_0; }
	inline String_t** get_address_of__storePath_0() { return &____storePath_0; }
	inline void set__storePath_0(String_t* value)
	{
		____storePath_0 = value;
		Il2CppCodeGenWriteBarrier((&____storePath_0), value);
	}

	inline static int32_t get_offset_of__certificates_1() { return static_cast<int32_t>(offsetof(X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302, ____certificates_1)); }
	inline X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * get__certificates_1() const { return ____certificates_1; }
	inline X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 ** get_address_of__certificates_1() { return &____certificates_1; }
	inline void set__certificates_1(X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * value)
	{
		____certificates_1 = value;
		Il2CppCodeGenWriteBarrier((&____certificates_1), value);
	}

	inline static int32_t get_offset_of__crls_2() { return static_cast<int32_t>(offsetof(X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302, ____crls_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get__crls_2() const { return ____crls_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of__crls_2() { return &____crls_2; }
	inline void set__crls_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		____crls_2 = value;
		Il2CppCodeGenWriteBarrier((&____crls_2), value);
	}

	inline static int32_t get_offset_of__crl_3() { return static_cast<int32_t>(offsetof(X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302, ____crl_3)); }
	inline bool get__crl_3() const { return ____crl_3; }
	inline bool* get_address_of__crl_3() { return &____crl_3; }
	inline void set__crl_3(bool value)
	{
		____crl_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STORE_T754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302_H
#ifndef X509STOREMANAGER_T84D6FAF6F4DB5D367306FC494991364A0752BDB7_H
#define X509STOREMANAGER_T84D6FAF6F4DB5D367306FC494991364A0752BDB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509StoreManager
struct  X509StoreManager_t84D6FAF6F4DB5D367306FC494991364A0752BDB7  : public RuntimeObject
{
public:

public:
};

struct X509StoreManager_t84D6FAF6F4DB5D367306FC494991364A0752BDB7_StaticFields
{
public:
	// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::_userStore
	X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28 * ____userStore_0;
	// Mono.Security.X509.X509Stores Mono.Security.X509.X509StoreManager::_machineStore
	X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28 * ____machineStore_1;

public:
	inline static int32_t get_offset_of__userStore_0() { return static_cast<int32_t>(offsetof(X509StoreManager_t84D6FAF6F4DB5D367306FC494991364A0752BDB7_StaticFields, ____userStore_0)); }
	inline X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28 * get__userStore_0() const { return ____userStore_0; }
	inline X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28 ** get_address_of__userStore_0() { return &____userStore_0; }
	inline void set__userStore_0(X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28 * value)
	{
		____userStore_0 = value;
		Il2CppCodeGenWriteBarrier((&____userStore_0), value);
	}

	inline static int32_t get_offset_of__machineStore_1() { return static_cast<int32_t>(offsetof(X509StoreManager_t84D6FAF6F4DB5D367306FC494991364A0752BDB7_StaticFields, ____machineStore_1)); }
	inline X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28 * get__machineStore_1() const { return ____machineStore_1; }
	inline X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28 ** get_address_of__machineStore_1() { return &____machineStore_1; }
	inline void set__machineStore_1(X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28 * value)
	{
		____machineStore_1 = value;
		Il2CppCodeGenWriteBarrier((&____machineStore_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STOREMANAGER_T84D6FAF6F4DB5D367306FC494991364A0752BDB7_H
#ifndef X509STORES_T2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28_H
#define X509STORES_T2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Stores
struct  X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X509Stores::_storePath
	String_t* ____storePath_0;
	// Mono.Security.X509.X509Store Mono.Security.X509.X509Stores::_trusted
	X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302 * ____trusted_1;

public:
	inline static int32_t get_offset_of__storePath_0() { return static_cast<int32_t>(offsetof(X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28, ____storePath_0)); }
	inline String_t* get__storePath_0() const { return ____storePath_0; }
	inline String_t** get_address_of__storePath_0() { return &____storePath_0; }
	inline void set__storePath_0(String_t* value)
	{
		____storePath_0 = value;
		Il2CppCodeGenWriteBarrier((&____storePath_0), value);
	}

	inline static int32_t get_offset_of__trusted_1() { return static_cast<int32_t>(offsetof(X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28, ____trusted_1)); }
	inline X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302 * get__trusted_1() const { return ____trusted_1; }
	inline X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302 ** get_address_of__trusted_1() { return &____trusted_1; }
	inline void set__trusted_1(X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302 * value)
	{
		____trusted_1 = value;
		Il2CppCodeGenWriteBarrier((&____trusted_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509STORES_T2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28_H
#ifndef ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#define ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T60F25EB48D5935E4C6C2BAF7F90F57A43528E469_H
#ifndef COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#define COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.CollectionBase
struct  CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Collections.CollectionBase::list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F, ___list_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_list_0() const { return ___list_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONBASE_TC3F24120B23471E7FDEE72107D1D541E6456744F_H
#ifndef NAMEOBJECTCOLLECTIONBASE_TC2EE4FB130214FAE7365D519959A2A34DE56E070_H
#define NAMEOBJECTCOLLECTIONBASE_TC2EE4FB130214FAE7365D519959A2A34DE56E070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Specialized.NameObjectCollectionBase
struct  NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Collections.Specialized.NameObjectCollectionBase::m_ItemsContainer
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___m_ItemsContainer_0;
	// System.Collections.Specialized.NameObjectCollectionBase__Item System.Collections.Specialized.NameObjectCollectionBase::m_NullKeyItem
	_Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7 * ___m_NullKeyItem_1;
	// System.Collections.ArrayList System.Collections.Specialized.NameObjectCollectionBase::m_ItemsArray
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___m_ItemsArray_2;
	// System.Collections.IHashCodeProvider System.Collections.Specialized.NameObjectCollectionBase::m_hashprovider
	RuntimeObject* ___m_hashprovider_3;
	// System.Collections.IComparer System.Collections.Specialized.NameObjectCollectionBase::m_comparer
	RuntimeObject* ___m_comparer_4;
	// System.Int32 System.Collections.Specialized.NameObjectCollectionBase::m_defCapacity
	int32_t ___m_defCapacity_5;
	// System.Boolean System.Collections.Specialized.NameObjectCollectionBase::m_readonly
	bool ___m_readonly_6;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Specialized.NameObjectCollectionBase::infoCopy
	SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * ___infoCopy_7;
	// System.Collections.Specialized.NameObjectCollectionBase_KeysCollection System.Collections.Specialized.NameObjectCollectionBase::keyscoll
	KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB * ___keyscoll_8;
	// System.Collections.IEqualityComparer System.Collections.Specialized.NameObjectCollectionBase::equality_comparer
	RuntimeObject* ___equality_comparer_9;

public:
	inline static int32_t get_offset_of_m_ItemsContainer_0() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_ItemsContainer_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_m_ItemsContainer_0() const { return ___m_ItemsContainer_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_m_ItemsContainer_0() { return &___m_ItemsContainer_0; }
	inline void set_m_ItemsContainer_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___m_ItemsContainer_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsContainer_0), value);
	}

	inline static int32_t get_offset_of_m_NullKeyItem_1() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_NullKeyItem_1)); }
	inline _Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7 * get_m_NullKeyItem_1() const { return ___m_NullKeyItem_1; }
	inline _Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7 ** get_address_of_m_NullKeyItem_1() { return &___m_NullKeyItem_1; }
	inline void set_m_NullKeyItem_1(_Item_tFD4BAC90D8E7FF5A3713C2052052BAC018E7CEF7 * value)
	{
		___m_NullKeyItem_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_NullKeyItem_1), value);
	}

	inline static int32_t get_offset_of_m_ItemsArray_2() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_ItemsArray_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_m_ItemsArray_2() const { return ___m_ItemsArray_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_m_ItemsArray_2() { return &___m_ItemsArray_2; }
	inline void set_m_ItemsArray_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___m_ItemsArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ItemsArray_2), value);
	}

	inline static int32_t get_offset_of_m_hashprovider_3() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_hashprovider_3)); }
	inline RuntimeObject* get_m_hashprovider_3() const { return ___m_hashprovider_3; }
	inline RuntimeObject** get_address_of_m_hashprovider_3() { return &___m_hashprovider_3; }
	inline void set_m_hashprovider_3(RuntimeObject* value)
	{
		___m_hashprovider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashprovider_3), value);
	}

	inline static int32_t get_offset_of_m_comparer_4() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_comparer_4)); }
	inline RuntimeObject* get_m_comparer_4() const { return ___m_comparer_4; }
	inline RuntimeObject** get_address_of_m_comparer_4() { return &___m_comparer_4; }
	inline void set_m_comparer_4(RuntimeObject* value)
	{
		___m_comparer_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_comparer_4), value);
	}

	inline static int32_t get_offset_of_m_defCapacity_5() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_defCapacity_5)); }
	inline int32_t get_m_defCapacity_5() const { return ___m_defCapacity_5; }
	inline int32_t* get_address_of_m_defCapacity_5() { return &___m_defCapacity_5; }
	inline void set_m_defCapacity_5(int32_t value)
	{
		___m_defCapacity_5 = value;
	}

	inline static int32_t get_offset_of_m_readonly_6() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___m_readonly_6)); }
	inline bool get_m_readonly_6() const { return ___m_readonly_6; }
	inline bool* get_address_of_m_readonly_6() { return &___m_readonly_6; }
	inline void set_m_readonly_6(bool value)
	{
		___m_readonly_6 = value;
	}

	inline static int32_t get_offset_of_infoCopy_7() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___infoCopy_7)); }
	inline SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * get_infoCopy_7() const { return ___infoCopy_7; }
	inline SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 ** get_address_of_infoCopy_7() { return &___infoCopy_7; }
	inline void set_infoCopy_7(SerializationInfo_t778922F6A5AACC38C8F326D3338A91D6D72B11E2 * value)
	{
		___infoCopy_7 = value;
		Il2CppCodeGenWriteBarrier((&___infoCopy_7), value);
	}

	inline static int32_t get_offset_of_keyscoll_8() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___keyscoll_8)); }
	inline KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB * get_keyscoll_8() const { return ___keyscoll_8; }
	inline KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB ** get_address_of_keyscoll_8() { return &___keyscoll_8; }
	inline void set_keyscoll_8(KeysCollection_t3A1987CE62992069C60E21F2F157579EFBB7FDDB * value)
	{
		___keyscoll_8 = value;
		Il2CppCodeGenWriteBarrier((&___keyscoll_8), value);
	}

	inline static int32_t get_offset_of_equality_comparer_9() { return static_cast<int32_t>(offsetof(NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070, ___equality_comparer_9)); }
	inline RuntimeObject* get_equality_comparer_9() const { return ___equality_comparer_9; }
	inline RuntimeObject** get_address_of_equality_comparer_9() { return &___equality_comparer_9; }
	inline void set_equality_comparer_9(RuntimeObject* value)
	{
		___equality_comparer_9 = value;
		Il2CppCodeGenWriteBarrier((&___equality_comparer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEOBJECTCOLLECTIONBASE_TC2EE4FB130214FAE7365D519959A2A34DE56E070_H
#ifndef CONFIGINFO_TCCB111FBFC8E13282C1FACD3B69A2709D96EA698_H
#define CONFIGINFO_TCCB111FBFC8E13282C1FACD3B69A2709D96EA698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigInfo
struct  ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigInfo::Name
	String_t* ___Name_0;
	// System.String System.Configuration.ConfigInfo::TypeName
	String_t* ___TypeName_1;
	// System.Type System.Configuration.ConfigInfo::Type
	Type_t * ___Type_2;
	// System.String System.Configuration.ConfigInfo::streamName
	String_t* ___streamName_3;
	// System.Configuration.ConfigInfo System.Configuration.ConfigInfo::Parent
	ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698 * ___Parent_4;
	// System.Configuration.Internal.IInternalConfigHost System.Configuration.ConfigInfo::ConfigHost
	RuntimeObject* ___ConfigHost_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698, ___Type_2)); }
	inline Type_t * get_Type_2() const { return ___Type_2; }
	inline Type_t ** get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(Type_t * value)
	{
		___Type_2 = value;
		Il2CppCodeGenWriteBarrier((&___Type_2), value);
	}

	inline static int32_t get_offset_of_streamName_3() { return static_cast<int32_t>(offsetof(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698, ___streamName_3)); }
	inline String_t* get_streamName_3() const { return ___streamName_3; }
	inline String_t** get_address_of_streamName_3() { return &___streamName_3; }
	inline void set_streamName_3(String_t* value)
	{
		___streamName_3 = value;
		Il2CppCodeGenWriteBarrier((&___streamName_3), value);
	}

	inline static int32_t get_offset_of_Parent_4() { return static_cast<int32_t>(offsetof(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698, ___Parent_4)); }
	inline ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698 * get_Parent_4() const { return ___Parent_4; }
	inline ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698 ** get_address_of_Parent_4() { return &___Parent_4; }
	inline void set_Parent_4(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698 * value)
	{
		___Parent_4 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_4), value);
	}

	inline static int32_t get_offset_of_ConfigHost_5() { return static_cast<int32_t>(offsetof(ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698, ___ConfigHost_5)); }
	inline RuntimeObject* get_ConfigHost_5() const { return ___ConfigHost_5; }
	inline RuntimeObject** get_address_of_ConfigHost_5() { return &___ConfigHost_5; }
	inline void set_ConfigHost_5(RuntimeObject* value)
	{
		___ConfigHost_5 = value;
		Il2CppCodeGenWriteBarrier((&___ConfigHost_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGINFO_TCCB111FBFC8E13282C1FACD3B69A2709D96EA698_H
#ifndef CONFIGURATIONELEMENT_T0102651B4A826E7C1A21DB9AF0C7565C66A53D63_H
#define CONFIGURATIONELEMENT_T0102651B4A826E7C1A21DB9AF0C7565C66A53D63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElement
struct  ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationElement::rawXml
	String_t* ___rawXml_0;
	// System.Boolean System.Configuration.ConfigurationElement::modified
	bool ___modified_1;
	// System.Configuration.ElementMap System.Configuration.ConfigurationElement::map
	ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC * ___map_2;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ConfigurationElement::keyProps
	ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * ___keyProps_3;
	// System.Configuration.ConfigurationElementCollection System.Configuration.ConfigurationElement::defaultCollection
	ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E * ___defaultCollection_4;
	// System.Boolean System.Configuration.ConfigurationElement::readOnly
	bool ___readOnly_5;
	// System.Configuration.ElementInformation System.Configuration.ConfigurationElement::elementInfo
	ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26 * ___elementInfo_6;
	// System.Configuration.Configuration System.Configuration.ConfigurationElement::_configuration
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * ____configuration_7;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAllAttributesExcept
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * ___lockAllAttributesExcept_8;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAllElementsExcept
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * ___lockAllElementsExcept_9;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockAttributes
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * ___lockAttributes_10;
	// System.Configuration.ConfigurationLockCollection System.Configuration.ConfigurationElement::lockElements
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * ___lockElements_11;
	// System.Boolean System.Configuration.ConfigurationElement::lockItem
	bool ___lockItem_12;

public:
	inline static int32_t get_offset_of_rawXml_0() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___rawXml_0)); }
	inline String_t* get_rawXml_0() const { return ___rawXml_0; }
	inline String_t** get_address_of_rawXml_0() { return &___rawXml_0; }
	inline void set_rawXml_0(String_t* value)
	{
		___rawXml_0 = value;
		Il2CppCodeGenWriteBarrier((&___rawXml_0), value);
	}

	inline static int32_t get_offset_of_modified_1() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___modified_1)); }
	inline bool get_modified_1() const { return ___modified_1; }
	inline bool* get_address_of_modified_1() { return &___modified_1; }
	inline void set_modified_1(bool value)
	{
		___modified_1 = value;
	}

	inline static int32_t get_offset_of_map_2() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___map_2)); }
	inline ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC * get_map_2() const { return ___map_2; }
	inline ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC ** get_address_of_map_2() { return &___map_2; }
	inline void set_map_2(ElementMap_t6891F6CAB535C88620E528446A2766C300236EEC * value)
	{
		___map_2 = value;
		Il2CppCodeGenWriteBarrier((&___map_2), value);
	}

	inline static int32_t get_offset_of_keyProps_3() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___keyProps_3)); }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * get_keyProps_3() const { return ___keyProps_3; }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 ** get_address_of_keyProps_3() { return &___keyProps_3; }
	inline void set_keyProps_3(ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * value)
	{
		___keyProps_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyProps_3), value);
	}

	inline static int32_t get_offset_of_defaultCollection_4() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___defaultCollection_4)); }
	inline ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E * get_defaultCollection_4() const { return ___defaultCollection_4; }
	inline ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E ** get_address_of_defaultCollection_4() { return &___defaultCollection_4; }
	inline void set_defaultCollection_4(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E * value)
	{
		___defaultCollection_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultCollection_4), value);
	}

	inline static int32_t get_offset_of_readOnly_5() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___readOnly_5)); }
	inline bool get_readOnly_5() const { return ___readOnly_5; }
	inline bool* get_address_of_readOnly_5() { return &___readOnly_5; }
	inline void set_readOnly_5(bool value)
	{
		___readOnly_5 = value;
	}

	inline static int32_t get_offset_of_elementInfo_6() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___elementInfo_6)); }
	inline ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26 * get_elementInfo_6() const { return ___elementInfo_6; }
	inline ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26 ** get_address_of_elementInfo_6() { return &___elementInfo_6; }
	inline void set_elementInfo_6(ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26 * value)
	{
		___elementInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___elementInfo_6), value);
	}

	inline static int32_t get_offset_of__configuration_7() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ____configuration_7)); }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * get__configuration_7() const { return ____configuration_7; }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 ** get_address_of__configuration_7() { return &____configuration_7; }
	inline void set__configuration_7(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * value)
	{
		____configuration_7 = value;
		Il2CppCodeGenWriteBarrier((&____configuration_7), value);
	}

	inline static int32_t get_offset_of_lockAllAttributesExcept_8() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___lockAllAttributesExcept_8)); }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * get_lockAllAttributesExcept_8() const { return ___lockAllAttributesExcept_8; }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C ** get_address_of_lockAllAttributesExcept_8() { return &___lockAllAttributesExcept_8; }
	inline void set_lockAllAttributesExcept_8(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * value)
	{
		___lockAllAttributesExcept_8 = value;
		Il2CppCodeGenWriteBarrier((&___lockAllAttributesExcept_8), value);
	}

	inline static int32_t get_offset_of_lockAllElementsExcept_9() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___lockAllElementsExcept_9)); }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * get_lockAllElementsExcept_9() const { return ___lockAllElementsExcept_9; }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C ** get_address_of_lockAllElementsExcept_9() { return &___lockAllElementsExcept_9; }
	inline void set_lockAllElementsExcept_9(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * value)
	{
		___lockAllElementsExcept_9 = value;
		Il2CppCodeGenWriteBarrier((&___lockAllElementsExcept_9), value);
	}

	inline static int32_t get_offset_of_lockAttributes_10() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___lockAttributes_10)); }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * get_lockAttributes_10() const { return ___lockAttributes_10; }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C ** get_address_of_lockAttributes_10() { return &___lockAttributes_10; }
	inline void set_lockAttributes_10(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * value)
	{
		___lockAttributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___lockAttributes_10), value);
	}

	inline static int32_t get_offset_of_lockElements_11() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___lockElements_11)); }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * get_lockElements_11() const { return ___lockElements_11; }
	inline ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C ** get_address_of_lockElements_11() { return &___lockElements_11; }
	inline void set_lockElements_11(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C * value)
	{
		___lockElements_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockElements_11), value);
	}

	inline static int32_t get_offset_of_lockItem_12() { return static_cast<int32_t>(offsetof(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63, ___lockItem_12)); }
	inline bool get_lockItem_12() const { return ___lockItem_12; }
	inline bool* get_address_of_lockItem_12() { return &___lockItem_12; }
	inline void set_lockItem_12(bool value)
	{
		___lockItem_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENT_T0102651B4A826E7C1A21DB9AF0C7565C66A53D63_H
#ifndef CONFIGURATIONFILEMAP_TE596BD02408620847DC8AA9D93423B610AB224F0_H
#define CONFIGURATIONFILEMAP_TE596BD02408620847DC8AA9D93423B610AB224F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationFileMap
struct  ConfigurationFileMap_tE596BD02408620847DC8AA9D93423B610AB224F0  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationFileMap::machineConfigFilename
	String_t* ___machineConfigFilename_0;

public:
	inline static int32_t get_offset_of_machineConfigFilename_0() { return static_cast<int32_t>(offsetof(ConfigurationFileMap_tE596BD02408620847DC8AA9D93423B610AB224F0, ___machineConfigFilename_0)); }
	inline String_t* get_machineConfigFilename_0() const { return ___machineConfigFilename_0; }
	inline String_t** get_address_of_machineConfigFilename_0() { return &___machineConfigFilename_0; }
	inline void set_machineConfigFilename_0(String_t* value)
	{
		___machineConfigFilename_0 = value;
		Il2CppCodeGenWriteBarrier((&___machineConfigFilename_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONFILEMAP_TE596BD02408620847DC8AA9D93423B610AB224F0_H
#ifndef CONFIGURATIONMANAGER_T78E461FD29B1306CEC46889C728A32A2828AAC1C_H
#define CONFIGURATIONMANAGER_T78E461FD29B1306CEC46889C728A32A2828AAC1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationManager
struct  ConfigurationManager_t78E461FD29B1306CEC46889C728A32A2828AAC1C  : public RuntimeObject
{
public:

public:
};

struct ConfigurationManager_t78E461FD29B1306CEC46889C728A32A2828AAC1C_StaticFields
{
public:
	// System.Configuration.InternalConfigurationFactory System.Configuration.ConfigurationManager::configFactory
	InternalConfigurationFactory_tDBD58CD148E84569F905006D415041A082593382 * ___configFactory_0;
	// System.Configuration.Internal.IInternalConfigSystem System.Configuration.ConfigurationManager::configSystem
	RuntimeObject* ___configSystem_1;
	// System.Object System.Configuration.ConfigurationManager::lockobj
	RuntimeObject * ___lockobj_2;

public:
	inline static int32_t get_offset_of_configFactory_0() { return static_cast<int32_t>(offsetof(ConfigurationManager_t78E461FD29B1306CEC46889C728A32A2828AAC1C_StaticFields, ___configFactory_0)); }
	inline InternalConfigurationFactory_tDBD58CD148E84569F905006D415041A082593382 * get_configFactory_0() const { return ___configFactory_0; }
	inline InternalConfigurationFactory_tDBD58CD148E84569F905006D415041A082593382 ** get_address_of_configFactory_0() { return &___configFactory_0; }
	inline void set_configFactory_0(InternalConfigurationFactory_tDBD58CD148E84569F905006D415041A082593382 * value)
	{
		___configFactory_0 = value;
		Il2CppCodeGenWriteBarrier((&___configFactory_0), value);
	}

	inline static int32_t get_offset_of_configSystem_1() { return static_cast<int32_t>(offsetof(ConfigurationManager_t78E461FD29B1306CEC46889C728A32A2828AAC1C_StaticFields, ___configSystem_1)); }
	inline RuntimeObject* get_configSystem_1() const { return ___configSystem_1; }
	inline RuntimeObject** get_address_of_configSystem_1() { return &___configSystem_1; }
	inline void set_configSystem_1(RuntimeObject* value)
	{
		___configSystem_1 = value;
		Il2CppCodeGenWriteBarrier((&___configSystem_1), value);
	}

	inline static int32_t get_offset_of_lockobj_2() { return static_cast<int32_t>(offsetof(ConfigurationManager_t78E461FD29B1306CEC46889C728A32A2828AAC1C_StaticFields, ___lockobj_2)); }
	inline RuntimeObject * get_lockobj_2() const { return ___lockobj_2; }
	inline RuntimeObject ** get_address_of_lockobj_2() { return &___lockobj_2; }
	inline void set_lockobj_2(RuntimeObject * value)
	{
		___lockobj_2 = value;
		Il2CppCodeGenWriteBarrier((&___lockobj_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONMANAGER_T78E461FD29B1306CEC46889C728A32A2828AAC1C_H
#ifndef CONFIGURATIONPROPERTYCOLLECTION_T3704E32C4A8B0F542BBCBC51FD42167C200961E9_H
#define CONFIGURATIONPROPERTYCOLLECTION_T3704E32C4A8B0F542BBCBC51FD42167C200961E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationPropertyCollection
struct  ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Configuration.ConfigurationProperty> System.Configuration.ConfigurationPropertyCollection::collection
	List_1_tF2126703F254B79E106F2178C60591A45C8E13D3 * ___collection_0;

public:
	inline static int32_t get_offset_of_collection_0() { return static_cast<int32_t>(offsetof(ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9, ___collection_0)); }
	inline List_1_tF2126703F254B79E106F2178C60591A45C8E13D3 * get_collection_0() const { return ___collection_0; }
	inline List_1_tF2126703F254B79E106F2178C60591A45C8E13D3 ** get_address_of_collection_0() { return &___collection_0; }
	inline void set_collection_0(List_1_tF2126703F254B79E106F2178C60591A45C8E13D3 * value)
	{
		___collection_0 = value;
		Il2CppCodeGenWriteBarrier((&___collection_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONPROPERTYCOLLECTION_T3704E32C4A8B0F542BBCBC51FD42167C200961E9_H
#ifndef U3CGETENUMERATORU3EC__ITERATOR0_T02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82_H
#define U3CGETENUMERATORU3EC__ITERATOR0_T02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionCollection_<GetEnumerator>c__Iterator0
struct  U3CGetEnumeratorU3Ec__Iterator0_t02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator System.Configuration.ConfigurationSectionCollection_<GetEnumerator>c__Iterator0::<U24s_32>__0
	RuntimeObject* ___U3CU24s_32U3E__0_0;
	// System.String System.Configuration.ConfigurationSectionCollection_<GetEnumerator>c__Iterator0::<key>__1
	String_t* ___U3CkeyU3E__1_1;
	// System.Int32 System.Configuration.ConfigurationSectionCollection_<GetEnumerator>c__Iterator0::U24PC
	int32_t ___U24PC_2;
	// System.Object System.Configuration.ConfigurationSectionCollection_<GetEnumerator>c__Iterator0::U24current
	RuntimeObject * ___U24current_3;
	// System.Configuration.ConfigurationSectionCollection System.Configuration.ConfigurationSectionCollection_<GetEnumerator>c__Iterator0::<>f__this
	ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_U3CU24s_32U3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82, ___U3CU24s_32U3E__0_0)); }
	inline RuntimeObject* get_U3CU24s_32U3E__0_0() const { return ___U3CU24s_32U3E__0_0; }
	inline RuntimeObject** get_address_of_U3CU24s_32U3E__0_0() { return &___U3CU24s_32U3E__0_0; }
	inline void set_U3CU24s_32U3E__0_0(RuntimeObject* value)
	{
		___U3CU24s_32U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU24s_32U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CkeyU3E__1_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82, ___U3CkeyU3E__1_1)); }
	inline String_t* get_U3CkeyU3E__1_1() const { return ___U3CkeyU3E__1_1; }
	inline String_t** get_address_of_U3CkeyU3E__1_1() { return &___U3CkeyU3E__1_1; }
	inline void set_U3CkeyU3E__1_1(String_t* value)
	{
		___U3CkeyU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CkeyU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ec__Iterator0_t02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82, ___U3CU3Ef__this_4)); }
	inline ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3EC__ITERATOR0_T02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82_H
#ifndef CONFIGURATIONSECTIONGROUP_TFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79_H
#define CONFIGURATIONSECTIONGROUP_TFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionGroup
struct  ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79  : public RuntimeObject
{
public:
	// System.Configuration.ConfigurationSectionCollection System.Configuration.ConfigurationSectionGroup::sections
	ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC * ___sections_0;
	// System.Configuration.ConfigurationSectionGroupCollection System.Configuration.ConfigurationSectionGroup::groups
	ConfigurationSectionGroupCollection_t8FBD02E07771CB420A315A28E362FC9B48499052 * ___groups_1;
	// System.Configuration.Configuration System.Configuration.ConfigurationSectionGroup::config
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * ___config_2;
	// System.Configuration.SectionGroupInfo System.Configuration.ConfigurationSectionGroup::group
	SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 * ___group_3;
	// System.Boolean System.Configuration.ConfigurationSectionGroup::initialized
	bool ___initialized_4;

public:
	inline static int32_t get_offset_of_sections_0() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79, ___sections_0)); }
	inline ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC * get_sections_0() const { return ___sections_0; }
	inline ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC ** get_address_of_sections_0() { return &___sections_0; }
	inline void set_sections_0(ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC * value)
	{
		___sections_0 = value;
		Il2CppCodeGenWriteBarrier((&___sections_0), value);
	}

	inline static int32_t get_offset_of_groups_1() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79, ___groups_1)); }
	inline ConfigurationSectionGroupCollection_t8FBD02E07771CB420A315A28E362FC9B48499052 * get_groups_1() const { return ___groups_1; }
	inline ConfigurationSectionGroupCollection_t8FBD02E07771CB420A315A28E362FC9B48499052 ** get_address_of_groups_1() { return &___groups_1; }
	inline void set_groups_1(ConfigurationSectionGroupCollection_t8FBD02E07771CB420A315A28E362FC9B48499052 * value)
	{
		___groups_1 = value;
		Il2CppCodeGenWriteBarrier((&___groups_1), value);
	}

	inline static int32_t get_offset_of_config_2() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79, ___config_2)); }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * get_config_2() const { return ___config_2; }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 ** get_address_of_config_2() { return &___config_2; }
	inline void set_config_2(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * value)
	{
		___config_2 = value;
		Il2CppCodeGenWriteBarrier((&___config_2), value);
	}

	inline static int32_t get_offset_of_group_3() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79, ___group_3)); }
	inline SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 * get_group_3() const { return ___group_3; }
	inline SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 ** get_address_of_group_3() { return &___group_3; }
	inline void set_group_3(SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 * value)
	{
		___group_3 = value;
		Il2CppCodeGenWriteBarrier((&___group_3), value);
	}

	inline static int32_t get_offset_of_initialized_4() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79, ___initialized_4)); }
	inline bool get_initialized_4() const { return ___initialized_4; }
	inline bool* get_address_of_initialized_4() { return &___initialized_4; }
	inline void set_initialized_4(bool value)
	{
		___initialized_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTIONGROUP_TFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79_H
#ifndef CONFIGURATIONVALIDATORBASE_TD691540223153E9DEA98BDBD778BCB12A56248ED_H
#define CONFIGURATIONVALIDATORBASE_TD691540223153E9DEA98BDBD778BCB12A56248ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationValidatorBase
struct  ConfigurationValidatorBase_tD691540223153E9DEA98BDBD778BCB12A56248ED  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONVALIDATORBASE_TD691540223153E9DEA98BDBD778BCB12A56248ED_H
#ifndef ELEMENTINFORMATION_TE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26_H
#define ELEMENTINFORMATION_TE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ElementInformation
struct  ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26  : public RuntimeObject
{
public:
	// System.Configuration.PropertyInformation System.Configuration.ElementInformation::propertyInfo
	PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF * ___propertyInfo_0;
	// System.Configuration.ConfigurationElement System.Configuration.ElementInformation::owner
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 * ___owner_1;
	// System.Configuration.PropertyInformationCollection System.Configuration.ElementInformation::properties
	PropertyInformationCollection_t6CBE051D205C8065A196AD13E4E5FA575B82AD52 * ___properties_2;

public:
	inline static int32_t get_offset_of_propertyInfo_0() { return static_cast<int32_t>(offsetof(ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26, ___propertyInfo_0)); }
	inline PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF * get_propertyInfo_0() const { return ___propertyInfo_0; }
	inline PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF ** get_address_of_propertyInfo_0() { return &___propertyInfo_0; }
	inline void set_propertyInfo_0(PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF * value)
	{
		___propertyInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyInfo_0), value);
	}

	inline static int32_t get_offset_of_owner_1() { return static_cast<int32_t>(offsetof(ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26, ___owner_1)); }
	inline ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 * get_owner_1() const { return ___owner_1; }
	inline ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 ** get_address_of_owner_1() { return &___owner_1; }
	inline void set_owner_1(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 * value)
	{
		___owner_1 = value;
		Il2CppCodeGenWriteBarrier((&___owner_1), value);
	}

	inline static int32_t get_offset_of_properties_2() { return static_cast<int32_t>(offsetof(ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26, ___properties_2)); }
	inline PropertyInformationCollection_t6CBE051D205C8065A196AD13E4E5FA575B82AD52 * get_properties_2() const { return ___properties_2; }
	inline PropertyInformationCollection_t6CBE051D205C8065A196AD13E4E5FA575B82AD52 ** get_address_of_properties_2() { return &___properties_2; }
	inline void set_properties_2(PropertyInformationCollection_t6CBE051D205C8065A196AD13E4E5FA575B82AD52 * value)
	{
		___properties_2 = value;
		Il2CppCodeGenWriteBarrier((&___properties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ELEMENTINFORMATION_TE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26_H
#ifndef INTERNALCONFIGURATIONFACTORY_TDBD58CD148E84569F905006D415041A082593382_H
#define INTERNALCONFIGURATIONFACTORY_TDBD58CD148E84569F905006D415041A082593382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.InternalConfigurationFactory
struct  InternalConfigurationFactory_tDBD58CD148E84569F905006D415041A082593382  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONFIGURATIONFACTORY_TDBD58CD148E84569F905006D415041A082593382_H
#ifndef INTERNALCONFIGURATIONHOST_T89A94135BBC91E89941129D05624642760575086_H
#define INTERNALCONFIGURATIONHOST_T89A94135BBC91E89941129D05624642760575086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.InternalConfigurationHost
struct  InternalConfigurationHost_t89A94135BBC91E89941129D05624642760575086  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONFIGURATIONHOST_T89A94135BBC91E89941129D05624642760575086_H
#ifndef INTERNALCONFIGURATIONROOT_TA6A1CDE68BFB644A6D68A1B07D5D805A81F9EF20_H
#define INTERNALCONFIGURATIONROOT_TA6A1CDE68BFB644A6D68A1B07D5D805A81F9EF20_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.InternalConfigurationRoot
struct  InternalConfigurationRoot_tA6A1CDE68BFB644A6D68A1B07D5D805A81F9EF20  : public RuntimeObject
{
public:
	// System.Configuration.Internal.IInternalConfigHost System.Configuration.InternalConfigurationRoot::host
	RuntimeObject* ___host_0;
	// System.Boolean System.Configuration.InternalConfigurationRoot::isDesignTime
	bool ___isDesignTime_1;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(InternalConfigurationRoot_tA6A1CDE68BFB644A6D68A1B07D5D805A81F9EF20, ___host_0)); }
	inline RuntimeObject* get_host_0() const { return ___host_0; }
	inline RuntimeObject** get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(RuntimeObject* value)
	{
		___host_0 = value;
		Il2CppCodeGenWriteBarrier((&___host_0), value);
	}

	inline static int32_t get_offset_of_isDesignTime_1() { return static_cast<int32_t>(offsetof(InternalConfigurationRoot_tA6A1CDE68BFB644A6D68A1B07D5D805A81F9EF20, ___isDesignTime_1)); }
	inline bool get_isDesignTime_1() const { return ___isDesignTime_1; }
	inline bool* get_address_of_isDesignTime_1() { return &___isDesignTime_1; }
	inline void set_isDesignTime_1(bool value)
	{
		___isDesignTime_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONFIGURATIONROOT_TA6A1CDE68BFB644A6D68A1B07D5D805A81F9EF20_H
#ifndef INTERNALCONFIGURATIONSYSTEM_TD42591E3647EAD9A1CD177FE17AE59324A0B8BB7_H
#define INTERNALCONFIGURATIONSYSTEM_TD42591E3647EAD9A1CD177FE17AE59324A0B8BB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.InternalConfigurationSystem
struct  InternalConfigurationSystem_tD42591E3647EAD9A1CD177FE17AE59324A0B8BB7  : public RuntimeObject
{
public:
	// System.Configuration.Internal.IInternalConfigHost System.Configuration.InternalConfigurationSystem::host
	RuntimeObject* ___host_0;
	// System.Configuration.Internal.IInternalConfigRoot System.Configuration.InternalConfigurationSystem::root
	RuntimeObject* ___root_1;
	// System.Object[] System.Configuration.InternalConfigurationSystem::hostInitParams
	ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* ___hostInitParams_2;

public:
	inline static int32_t get_offset_of_host_0() { return static_cast<int32_t>(offsetof(InternalConfigurationSystem_tD42591E3647EAD9A1CD177FE17AE59324A0B8BB7, ___host_0)); }
	inline RuntimeObject* get_host_0() const { return ___host_0; }
	inline RuntimeObject** get_address_of_host_0() { return &___host_0; }
	inline void set_host_0(RuntimeObject* value)
	{
		___host_0 = value;
		Il2CppCodeGenWriteBarrier((&___host_0), value);
	}

	inline static int32_t get_offset_of_root_1() { return static_cast<int32_t>(offsetof(InternalConfigurationSystem_tD42591E3647EAD9A1CD177FE17AE59324A0B8BB7, ___root_1)); }
	inline RuntimeObject* get_root_1() const { return ___root_1; }
	inline RuntimeObject** get_address_of_root_1() { return &___root_1; }
	inline void set_root_1(RuntimeObject* value)
	{
		___root_1 = value;
		Il2CppCodeGenWriteBarrier((&___root_1), value);
	}

	inline static int32_t get_offset_of_hostInitParams_2() { return static_cast<int32_t>(offsetof(InternalConfigurationSystem_tD42591E3647EAD9A1CD177FE17AE59324A0B8BB7, ___hostInitParams_2)); }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* get_hostInitParams_2() const { return ___hostInitParams_2; }
	inline ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D** get_address_of_hostInitParams_2() { return &___hostInitParams_2; }
	inline void set_hostInitParams_2(ObjectU5BU5D_t8D571697F3A1B33B696E2F80500C21F1A1748C5D* value)
	{
		___hostInitParams_2 = value;
		Il2CppCodeGenWriteBarrier((&___hostInitParams_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERNALCONFIGURATIONSYSTEM_TD42591E3647EAD9A1CD177FE17AE59324A0B8BB7_H
#ifndef PROPERTYINFORMATIONENUMERATOR_T8A4F098D93D66DFF14EEB4B536ABF022CC0DF231_H
#define PROPERTYINFORMATIONENUMERATOR_T8A4F098D93D66DFF14EEB4B536ABF022CC0DF231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.PropertyInformationCollection_PropertyInformationEnumerator
struct  PropertyInformationEnumerator_t8A4F098D93D66DFF14EEB4B536ABF022CC0DF231  : public RuntimeObject
{
public:
	// System.Configuration.PropertyInformationCollection System.Configuration.PropertyInformationCollection_PropertyInformationEnumerator::collection
	PropertyInformationCollection_t6CBE051D205C8065A196AD13E4E5FA575B82AD52 * ___collection_0;
	// System.Int32 System.Configuration.PropertyInformationCollection_PropertyInformationEnumerator::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_collection_0() { return static_cast<int32_t>(offsetof(PropertyInformationEnumerator_t8A4F098D93D66DFF14EEB4B536ABF022CC0DF231, ___collection_0)); }
	inline PropertyInformationCollection_t6CBE051D205C8065A196AD13E4E5FA575B82AD52 * get_collection_0() const { return ___collection_0; }
	inline PropertyInformationCollection_t6CBE051D205C8065A196AD13E4E5FA575B82AD52 ** get_address_of_collection_0() { return &___collection_0; }
	inline void set_collection_0(PropertyInformationCollection_t6CBE051D205C8065A196AD13E4E5FA575B82AD52 * value)
	{
		___collection_0 = value;
		Il2CppCodeGenWriteBarrier((&___collection_0), value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(PropertyInformationEnumerator_t8A4F098D93D66DFF14EEB4B536ABF022CC0DF231, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFORMATIONENUMERATOR_T8A4F098D93D66DFF14EEB4B536ABF022CC0DF231_H
#ifndef PROTECTEDCONFIGURATION_T6CA812ECE2C605D4FDCB495584DB7D34E804428C_H
#define PROTECTEDCONFIGURATION_T6CA812ECE2C605D4FDCB495584DB7D34E804428C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfiguration
struct  ProtectedConfiguration_t6CA812ECE2C605D4FDCB495584DB7D34E804428C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDCONFIGURATION_T6CA812ECE2C605D4FDCB495584DB7D34E804428C_H
#ifndef PROVIDERBASE_T92D989F50324ADBD9423D8B2E92FEE752477D380_H
#define PROVIDERBASE_T92D989F50324ADBD9423D8B2E92FEE752477D380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.Provider.ProviderBase
struct  ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380  : public RuntimeObject
{
public:
	// System.Boolean System.Configuration.Provider.ProviderBase::alreadyInitialized
	bool ___alreadyInitialized_0;
	// System.String System.Configuration.Provider.ProviderBase::_description
	String_t* ____description_1;
	// System.String System.Configuration.Provider.ProviderBase::_name
	String_t* ____name_2;

public:
	inline static int32_t get_offset_of_alreadyInitialized_0() { return static_cast<int32_t>(offsetof(ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380, ___alreadyInitialized_0)); }
	inline bool get_alreadyInitialized_0() const { return ___alreadyInitialized_0; }
	inline bool* get_address_of_alreadyInitialized_0() { return &___alreadyInitialized_0; }
	inline void set_alreadyInitialized_0(bool value)
	{
		___alreadyInitialized_0 = value;
	}

	inline static int32_t get_offset_of__description_1() { return static_cast<int32_t>(offsetof(ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380, ____description_1)); }
	inline String_t* get__description_1() const { return ____description_1; }
	inline String_t** get_address_of__description_1() { return &____description_1; }
	inline void set__description_1(String_t* value)
	{
		____description_1 = value;
		Il2CppCodeGenWriteBarrier((&____description_1), value);
	}

	inline static int32_t get_offset_of__name_2() { return static_cast<int32_t>(offsetof(ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380, ____name_2)); }
	inline String_t* get__name_2() const { return ____name_2; }
	inline String_t** get_address_of__name_2() { return &____name_2; }
	inline void set__name_2(String_t* value)
	{
		____name_2 = value;
		Il2CppCodeGenWriteBarrier((&____name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERBASE_T92D989F50324ADBD9423D8B2E92FEE752477D380_H
#ifndef PROVIDERCOLLECTION_TF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386_H
#define PROVIDERCOLLECTION_TF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.Provider.ProviderCollection
struct  ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Configuration.Provider.ProviderCollection::lookup
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___lookup_0;
	// System.Boolean System.Configuration.Provider.ProviderCollection::readOnly
	bool ___readOnly_1;
	// System.Collections.ArrayList System.Configuration.Provider.ProviderCollection::values
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___values_2;

public:
	inline static int32_t get_offset_of_lookup_0() { return static_cast<int32_t>(offsetof(ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386, ___lookup_0)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_lookup_0() const { return ___lookup_0; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_lookup_0() { return &___lookup_0; }
	inline void set_lookup_0(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___lookup_0 = value;
		Il2CppCodeGenWriteBarrier((&___lookup_0), value);
	}

	inline static int32_t get_offset_of_readOnly_1() { return static_cast<int32_t>(offsetof(ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386, ___readOnly_1)); }
	inline bool get_readOnly_1() const { return ___readOnly_1; }
	inline bool* get_address_of_readOnly_1() { return &___readOnly_1; }
	inline void set_readOnly_1(bool value)
	{
		___readOnly_1 = value;
	}

	inline static int32_t get_offset_of_values_2() { return static_cast<int32_t>(offsetof(ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386, ___values_2)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_values_2() const { return ___values_2; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_values_2() { return &___values_2; }
	inline void set_values_2(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___values_2 = value;
		Il2CppCodeGenWriteBarrier((&___values_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERCOLLECTION_TF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386_H
#ifndef ASYMMETRICALGORITHM_TCE2A08658EFEA671D977BC39FA246B4FC5092BB2_H
#define ASYMMETRICALGORITHM_TCE2A08658EFEA671D977BC39FA246B4FC5092BB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.AsymmetricAlgorithm
struct  AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.AsymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_0;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.AsymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* ___LegalKeySizesValue_1;

public:
	inline static int32_t get_offset_of_KeySizeValue_0() { return static_cast<int32_t>(offsetof(AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2, ___KeySizeValue_0)); }
	inline int32_t get_KeySizeValue_0() const { return ___KeySizeValue_0; }
	inline int32_t* get_address_of_KeySizeValue_0() { return &___KeySizeValue_0; }
	inline void set_KeySizeValue_0(int32_t value)
	{
		___KeySizeValue_0 = value;
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_1() { return static_cast<int32_t>(offsetof(AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2, ___LegalKeySizesValue_1)); }
	inline KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* get_LegalKeySizesValue_1() const { return ___LegalKeySizesValue_1; }
	inline KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C** get_address_of_LegalKeySizesValue_1() { return &___LegalKeySizesValue_1; }
	inline void set_LegalKeySizesValue_1(KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* value)
	{
		___LegalKeySizesValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYMMETRICALGORITHM_TCE2A08658EFEA671D977BC39FA246B4FC5092BB2_H
#ifndef HASHALGORITHM_T6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0_H
#define HASHALGORITHM_T6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.HashAlgorithm
struct  HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0  : public RuntimeObject
{
public:
	// System.Byte[] System.Security.Cryptography.HashAlgorithm::HashValue
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___HashValue_0;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::HashSizeValue
	int32_t ___HashSizeValue_1;
	// System.Int32 System.Security.Cryptography.HashAlgorithm::State
	int32_t ___State_2;
	// System.Boolean System.Security.Cryptography.HashAlgorithm::disposed
	bool ___disposed_3;

public:
	inline static int32_t get_offset_of_HashValue_0() { return static_cast<int32_t>(offsetof(HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0, ___HashValue_0)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_HashValue_0() const { return ___HashValue_0; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_HashValue_0() { return &___HashValue_0; }
	inline void set_HashValue_0(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___HashValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___HashValue_0), value);
	}

	inline static int32_t get_offset_of_HashSizeValue_1() { return static_cast<int32_t>(offsetof(HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0, ___HashSizeValue_1)); }
	inline int32_t get_HashSizeValue_1() const { return ___HashSizeValue_1; }
	inline int32_t* get_address_of_HashSizeValue_1() { return &___HashSizeValue_1; }
	inline void set_HashSizeValue_1(int32_t value)
	{
		___HashSizeValue_1 = value;
	}

	inline static int32_t get_offset_of_State_2() { return static_cast<int32_t>(offsetof(HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0, ___State_2)); }
	inline int32_t get_State_2() const { return ___State_2; }
	inline int32_t* get_address_of_State_2() { return &___State_2; }
	inline void set_State_2(int32_t value)
	{
		___State_2 = value;
	}

	inline static int32_t get_offset_of_disposed_3() { return static_cast<int32_t>(offsetof(HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0, ___disposed_3)); }
	inline bool get_disposed_3() const { return ___disposed_3; }
	inline bool* get_address_of_disposed_3() { return &___disposed_3; }
	inline void set_disposed_3(bool value)
	{
		___disposed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHALGORITHM_T6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0_H
#ifndef VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#define VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_marshaled_com
{
};
#endif // VALUETYPE_T1810BD84E0FDB5D3A7CD34286A5B22F343995C9C_H
#ifndef XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#define XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293  : public RuntimeObject
{
public:
	// System.Xml.XmlReaderBinarySupport System.Xml.XmlReader::binary
	XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * ___binary_0;
	// System.Xml.XmlReaderSettings System.Xml.XmlReader::settings
	XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * ___settings_1;

public:
	inline static int32_t get_offset_of_binary_0() { return static_cast<int32_t>(offsetof(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293, ___binary_0)); }
	inline XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * get_binary_0() const { return ___binary_0; }
	inline XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A ** get_address_of_binary_0() { return &___binary_0; }
	inline void set_binary_0(XmlReaderBinarySupport_tACAE9D001BCEE8E7C0DCA0FE1D378CCE44D18F3A * value)
	{
		___binary_0 = value;
		Il2CppCodeGenWriteBarrier((&___binary_0), value);
	}

	inline static int32_t get_offset_of_settings_1() { return static_cast<int32_t>(offsetof(XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293, ___settings_1)); }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * get_settings_1() const { return ___settings_1; }
	inline XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB ** get_address_of_settings_1() { return &___settings_1; }
	inline void set_settings_1(XmlReaderSettings_tCBE994FD28559C5C6CD166EF7CAF43C9E8CC65AB * value)
	{
		___settings_1 = value;
		Il2CppCodeGenWriteBarrier((&___settings_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_TC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293_H
#ifndef SEQUENTIALSEARCHPRIMEGENERATORBASE_T3E8A2759CFF3EBCCA4D5F2B5514FE449D8AEBD08_H
#define SEQUENTIALSEARCHPRIMEGENERATORBASE_T3E8A2759CFF3EBCCA4D5F2B5514FE449D8AEBD08_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.Generator.SequentialSearchPrimeGeneratorBase
struct  SequentialSearchPrimeGeneratorBase_t3E8A2759CFF3EBCCA4D5F2B5514FE449D8AEBD08  : public PrimeGeneratorBase_tE8C14F85FCFF73ABFBC8B57DFE8F5A634DF477FE
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENTIALSEARCHPRIMEGENERATORBASE_T3E8A2759CFF3EBCCA4D5F2B5514FE449D8AEBD08_H
#ifndef MD2_TD12D2F4F3A46581687DEC22F27262968546E1DC7_H
#define MD2_TD12D2F4F3A46581687DEC22F27262968546E1DC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD2
struct  MD2_tD12D2F4F3A46581687DEC22F27262968546E1DC7  : public HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD2_TD12D2F4F3A46581687DEC22F27262968546E1DC7_H
#ifndef MD4_T089B8663437920C4CA21F863DD9E87FB3B95E023_H
#define MD4_T089B8663437920C4CA21F863DD9E87FB3B95E023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD4
struct  MD4_t089B8663437920C4CA21F863DD9E87FB3B95E023  : public HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD4_T089B8663437920C4CA21F863DD9E87FB3B95E023_H
#ifndef MD5SHA1_TC00142BACF5232E18AF953932BB12EB25ED58539_H
#define MD5SHA1_TC00142BACF5232E18AF953932BB12EB25ED58539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD5SHA1
struct  MD5SHA1_tC00142BACF5232E18AF953932BB12EB25ED58539  : public HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0
{
public:
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.MD5SHA1::md5
	HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0 * ___md5_4;
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.MD5SHA1::sha
	HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0 * ___sha_5;
	// System.Boolean Mono.Security.Cryptography.MD5SHA1::hashing
	bool ___hashing_6;

public:
	inline static int32_t get_offset_of_md5_4() { return static_cast<int32_t>(offsetof(MD5SHA1_tC00142BACF5232E18AF953932BB12EB25ED58539, ___md5_4)); }
	inline HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0 * get_md5_4() const { return ___md5_4; }
	inline HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0 ** get_address_of_md5_4() { return &___md5_4; }
	inline void set_md5_4(HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0 * value)
	{
		___md5_4 = value;
		Il2CppCodeGenWriteBarrier((&___md5_4), value);
	}

	inline static int32_t get_offset_of_sha_5() { return static_cast<int32_t>(offsetof(MD5SHA1_tC00142BACF5232E18AF953932BB12EB25ED58539, ___sha_5)); }
	inline HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0 * get_sha_5() const { return ___sha_5; }
	inline HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0 ** get_address_of_sha_5() { return &___sha_5; }
	inline void set_sha_5(HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0 * value)
	{
		___sha_5 = value;
		Il2CppCodeGenWriteBarrier((&___sha_5), value);
	}

	inline static int32_t get_offset_of_hashing_6() { return static_cast<int32_t>(offsetof(MD5SHA1_tC00142BACF5232E18AF953932BB12EB25ED58539, ___hashing_6)); }
	inline bool get_hashing_6() const { return ___hashing_6; }
	inline bool* get_address_of_hashing_6() { return &___hashing_6; }
	inline void set_hashing_6(bool value)
	{
		___hashing_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5SHA1_TC00142BACF5232E18AF953932BB12EB25ED58539_H
#ifndef AUTHORITYKEYIDENTIFIEREXTENSION_T76714D3A7DEB58D0D703AC8AD0F7D17F4430FDB3_H
#define AUTHORITYKEYIDENTIFIEREXTENSION_T76714D3A7DEB58D0D703AC8AD0F7D17F4430FDB3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension
struct  AuthorityKeyIdentifierExtension_t76714D3A7DEB58D0D703AC8AD0F7D17F4430FDB3  : public X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698
{
public:
	// System.Byte[] Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::aki
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___aki_3;

public:
	inline static int32_t get_offset_of_aki_3() { return static_cast<int32_t>(offsetof(AuthorityKeyIdentifierExtension_t76714D3A7DEB58D0D703AC8AD0F7D17F4430FDB3, ___aki_3)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_aki_3() const { return ___aki_3; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_aki_3() { return &___aki_3; }
	inline void set_aki_3(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___aki_3 = value;
		Il2CppCodeGenWriteBarrier((&___aki_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHORITYKEYIDENTIFIEREXTENSION_T76714D3A7DEB58D0D703AC8AD0F7D17F4430FDB3_H
#ifndef BASICCONSTRAINTSEXTENSION_T519E68578F3BFB9B3588596B24BF4B729D26494E_H
#define BASICCONSTRAINTSEXTENSION_T519E68578F3BFB9B3588596B24BF4B729D26494E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.BasicConstraintsExtension
struct  BasicConstraintsExtension_t519E68578F3BFB9B3588596B24BF4B729D26494E  : public X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698
{
public:
	// System.Boolean Mono.Security.X509.Extensions.BasicConstraintsExtension::cA
	bool ___cA_3;
	// System.Int32 Mono.Security.X509.Extensions.BasicConstraintsExtension::pathLenConstraint
	int32_t ___pathLenConstraint_4;

public:
	inline static int32_t get_offset_of_cA_3() { return static_cast<int32_t>(offsetof(BasicConstraintsExtension_t519E68578F3BFB9B3588596B24BF4B729D26494E, ___cA_3)); }
	inline bool get_cA_3() const { return ___cA_3; }
	inline bool* get_address_of_cA_3() { return &___cA_3; }
	inline void set_cA_3(bool value)
	{
		___cA_3 = value;
	}

	inline static int32_t get_offset_of_pathLenConstraint_4() { return static_cast<int32_t>(offsetof(BasicConstraintsExtension_t519E68578F3BFB9B3588596B24BF4B729D26494E, ___pathLenConstraint_4)); }
	inline int32_t get_pathLenConstraint_4() const { return ___pathLenConstraint_4; }
	inline int32_t* get_address_of_pathLenConstraint_4() { return &___pathLenConstraint_4; }
	inline void set_pathLenConstraint_4(int32_t value)
	{
		___pathLenConstraint_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICCONSTRAINTSEXTENSION_T519E68578F3BFB9B3588596B24BF4B729D26494E_H
#ifndef EXTENDEDKEYUSAGEEXTENSION_TC9B7CEC7328C06886E6F95EC50C8FE821ECCD8A1_H
#define EXTENDEDKEYUSAGEEXTENSION_TC9B7CEC7328C06886E6F95EC50C8FE821ECCD8A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.ExtendedKeyUsageExtension
struct  ExtendedKeyUsageExtension_tC9B7CEC7328C06886E6F95EC50C8FE821ECCD8A1  : public X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698
{
public:
	// System.Collections.ArrayList Mono.Security.X509.Extensions.ExtendedKeyUsageExtension::keyPurpose
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___keyPurpose_3;

public:
	inline static int32_t get_offset_of_keyPurpose_3() { return static_cast<int32_t>(offsetof(ExtendedKeyUsageExtension_tC9B7CEC7328C06886E6F95EC50C8FE821ECCD8A1, ___keyPurpose_3)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_keyPurpose_3() const { return ___keyPurpose_3; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_keyPurpose_3() { return &___keyPurpose_3; }
	inline void set_keyPurpose_3(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___keyPurpose_3 = value;
		Il2CppCodeGenWriteBarrier((&___keyPurpose_3), value);
	}
};

struct ExtendedKeyUsageExtension_tC9B7CEC7328C06886E6F95EC50C8FE821ECCD8A1_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.Extensions.ExtendedKeyUsageExtension::<>f__switchU24map14
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map14_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map14_4() { return static_cast<int32_t>(offsetof(ExtendedKeyUsageExtension_tC9B7CEC7328C06886E6F95EC50C8FE821ECCD8A1_StaticFields, ___U3CU3Ef__switchU24map14_4)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map14_4() const { return ___U3CU3Ef__switchU24map14_4; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map14_4() { return &___U3CU3Ef__switchU24map14_4; }
	inline void set_U3CU3Ef__switchU24map14_4(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map14_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map14_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENDEDKEYUSAGEEXTENSION_TC9B7CEC7328C06886E6F95EC50C8FE821ECCD8A1_H
#ifndef KEYUSAGEEXTENSION_TA1D7257D3C3251C4199CD4B7DAEBB97E15CA895C_H
#define KEYUSAGEEXTENSION_TA1D7257D3C3251C4199CD4B7DAEBB97E15CA895C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.KeyUsageExtension
struct  KeyUsageExtension_tA1D7257D3C3251C4199CD4B7DAEBB97E15CA895C  : public X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698
{
public:
	// System.Int32 Mono.Security.X509.Extensions.KeyUsageExtension::kubits
	int32_t ___kubits_3;

public:
	inline static int32_t get_offset_of_kubits_3() { return static_cast<int32_t>(offsetof(KeyUsageExtension_tA1D7257D3C3251C4199CD4B7DAEBB97E15CA895C, ___kubits_3)); }
	inline int32_t get_kubits_3() const { return ___kubits_3; }
	inline int32_t* get_address_of_kubits_3() { return &___kubits_3; }
	inline void set_kubits_3(int32_t value)
	{
		___kubits_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYUSAGEEXTENSION_TA1D7257D3C3251C4199CD4B7DAEBB97E15CA895C_H
#ifndef NETSCAPECERTTYPEEXTENSION_T8C5A13576C3C530FB3D477C716AA0E2D89868973_H
#define NETSCAPECERTTYPEEXTENSION_T8C5A13576C3C530FB3D477C716AA0E2D89868973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.NetscapeCertTypeExtension
struct  NetscapeCertTypeExtension_t8C5A13576C3C530FB3D477C716AA0E2D89868973  : public X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698
{
public:
	// System.Int32 Mono.Security.X509.Extensions.NetscapeCertTypeExtension::ctbits
	int32_t ___ctbits_3;

public:
	inline static int32_t get_offset_of_ctbits_3() { return static_cast<int32_t>(offsetof(NetscapeCertTypeExtension_t8C5A13576C3C530FB3D477C716AA0E2D89868973, ___ctbits_3)); }
	inline int32_t get_ctbits_3() const { return ___ctbits_3; }
	inline int32_t* get_address_of_ctbits_3() { return &___ctbits_3; }
	inline void set_ctbits_3(int32_t value)
	{
		___ctbits_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETSCAPECERTTYPEEXTENSION_T8C5A13576C3C530FB3D477C716AA0E2D89868973_H
#ifndef SUBJECTALTNAMEEXTENSION_TAA251D1FFC8AC88B0F91F3188F5F7F0A5CDA864D_H
#define SUBJECTALTNAMEEXTENSION_TAA251D1FFC8AC88B0F91F3188F5F7F0A5CDA864D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.SubjectAltNameExtension
struct  SubjectAltNameExtension_tAA251D1FFC8AC88B0F91F3188F5F7F0A5CDA864D  : public X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698
{
public:
	// Mono.Security.X509.Extensions.GeneralNames Mono.Security.X509.Extensions.SubjectAltNameExtension::_names
	GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45 * ____names_3;

public:
	inline static int32_t get_offset_of__names_3() { return static_cast<int32_t>(offsetof(SubjectAltNameExtension_tAA251D1FFC8AC88B0F91F3188F5F7F0A5CDA864D, ____names_3)); }
	inline GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45 * get__names_3() const { return ____names_3; }
	inline GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45 ** get_address_of__names_3() { return &____names_3; }
	inline void set__names_3(GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45 * value)
	{
		____names_3 = value;
		Il2CppCodeGenWriteBarrier((&____names_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBJECTALTNAMEEXTENSION_TAA251D1FFC8AC88B0F91F3188F5F7F0A5CDA864D_H
#ifndef X509CERTIFICATECOLLECTION_T59402ED1601796E9D33AA78F60D998BC0DDA12F4_H
#define X509CERTIFICATECOLLECTION_T59402ED1601796E9D33AA78F60D998BC0DDA12F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509CertificateCollection
struct  X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4  : public CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATECOLLECTION_T59402ED1601796E9D33AA78F60D998BC0DDA12F4_H
#ifndef X509EXTENSIONCOLLECTION_T755DFCBDA8823D949578ED886BAFA4BF87706991_H
#define X509EXTENSIONCOLLECTION_T755DFCBDA8823D949578ED886BAFA4BF87706991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509ExtensionCollection
struct  X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991  : public CollectionBase_tC3F24120B23471E7FDEE72107D1D541E6456744F
{
public:
	// System.Boolean Mono.Security.X509.X509ExtensionCollection::readOnly
	bool ___readOnly_1;

public:
	inline static int32_t get_offset_of_readOnly_1() { return static_cast<int32_t>(offsetof(X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991, ___readOnly_1)); }
	inline bool get_readOnly_1() const { return ___readOnly_1; }
	inline bool* get_address_of_readOnly_1() { return &___readOnly_1; }
	inline void set_readOnly_1(bool value)
	{
		___readOnly_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509EXTENSIONCOLLECTION_T755DFCBDA8823D949578ED886BAFA4BF87706991_H
#ifndef CONFIGINFOCOLLECTION_T7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A_H
#define CONFIGINFOCOLLECTION_T7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigInfoCollection
struct  ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A  : public NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGINFOCOLLECTION_T7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A_H
#ifndef CONFIGURATIONELEMENTCOLLECTION_T5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E_H
#define CONFIGURATIONELEMENTCOLLECTION_T5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationElementCollection
struct  ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E  : public ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63
{
public:
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::list
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___list_13;
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::removed
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___removed_14;
	// System.Collections.ArrayList System.Configuration.ConfigurationElementCollection::inherited
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___inherited_15;
	// System.Boolean System.Configuration.ConfigurationElementCollection::emitClear
	bool ___emitClear_16;
	// System.Boolean System.Configuration.ConfigurationElementCollection::modified
	bool ___modified_17;
	// System.Collections.IComparer System.Configuration.ConfigurationElementCollection::comparer
	RuntimeObject* ___comparer_18;
	// System.Int32 System.Configuration.ConfigurationElementCollection::inheritedLimitIndex
	int32_t ___inheritedLimitIndex_19;
	// System.String System.Configuration.ConfigurationElementCollection::addElementName
	String_t* ___addElementName_20;
	// System.String System.Configuration.ConfigurationElementCollection::clearElementName
	String_t* ___clearElementName_21;
	// System.String System.Configuration.ConfigurationElementCollection::removeElementName
	String_t* ___removeElementName_22;

public:
	inline static int32_t get_offset_of_list_13() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___list_13)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_list_13() const { return ___list_13; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_list_13() { return &___list_13; }
	inline void set_list_13(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___list_13 = value;
		Il2CppCodeGenWriteBarrier((&___list_13), value);
	}

	inline static int32_t get_offset_of_removed_14() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___removed_14)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_removed_14() const { return ___removed_14; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_removed_14() { return &___removed_14; }
	inline void set_removed_14(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___removed_14 = value;
		Il2CppCodeGenWriteBarrier((&___removed_14), value);
	}

	inline static int32_t get_offset_of_inherited_15() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___inherited_15)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_inherited_15() const { return ___inherited_15; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_inherited_15() { return &___inherited_15; }
	inline void set_inherited_15(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___inherited_15 = value;
		Il2CppCodeGenWriteBarrier((&___inherited_15), value);
	}

	inline static int32_t get_offset_of_emitClear_16() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___emitClear_16)); }
	inline bool get_emitClear_16() const { return ___emitClear_16; }
	inline bool* get_address_of_emitClear_16() { return &___emitClear_16; }
	inline void set_emitClear_16(bool value)
	{
		___emitClear_16 = value;
	}

	inline static int32_t get_offset_of_modified_17() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___modified_17)); }
	inline bool get_modified_17() const { return ___modified_17; }
	inline bool* get_address_of_modified_17() { return &___modified_17; }
	inline void set_modified_17(bool value)
	{
		___modified_17 = value;
	}

	inline static int32_t get_offset_of_comparer_18() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___comparer_18)); }
	inline RuntimeObject* get_comparer_18() const { return ___comparer_18; }
	inline RuntimeObject** get_address_of_comparer_18() { return &___comparer_18; }
	inline void set_comparer_18(RuntimeObject* value)
	{
		___comparer_18 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_18), value);
	}

	inline static int32_t get_offset_of_inheritedLimitIndex_19() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___inheritedLimitIndex_19)); }
	inline int32_t get_inheritedLimitIndex_19() const { return ___inheritedLimitIndex_19; }
	inline int32_t* get_address_of_inheritedLimitIndex_19() { return &___inheritedLimitIndex_19; }
	inline void set_inheritedLimitIndex_19(int32_t value)
	{
		___inheritedLimitIndex_19 = value;
	}

	inline static int32_t get_offset_of_addElementName_20() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___addElementName_20)); }
	inline String_t* get_addElementName_20() const { return ___addElementName_20; }
	inline String_t** get_address_of_addElementName_20() { return &___addElementName_20; }
	inline void set_addElementName_20(String_t* value)
	{
		___addElementName_20 = value;
		Il2CppCodeGenWriteBarrier((&___addElementName_20), value);
	}

	inline static int32_t get_offset_of_clearElementName_21() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___clearElementName_21)); }
	inline String_t* get_clearElementName_21() const { return ___clearElementName_21; }
	inline String_t** get_address_of_clearElementName_21() { return &___clearElementName_21; }
	inline void set_clearElementName_21(String_t* value)
	{
		___clearElementName_21 = value;
		Il2CppCodeGenWriteBarrier((&___clearElementName_21), value);
	}

	inline static int32_t get_offset_of_removeElementName_22() { return static_cast<int32_t>(offsetof(ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E, ___removeElementName_22)); }
	inline String_t* get_removeElementName_22() const { return ___removeElementName_22; }
	inline String_t** get_address_of_removeElementName_22() { return &___removeElementName_22; }
	inline void set_removeElementName_22(String_t* value)
	{
		___removeElementName_22 = value;
		Il2CppCodeGenWriteBarrier((&___removeElementName_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONELEMENTCOLLECTION_T5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E_H
#ifndef CONFIGURATIONSECTION_TEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD_H
#define CONFIGURATIONSECTION_TEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSection
struct  ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD  : public ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63
{
public:
	// System.Configuration.SectionInformation System.Configuration.ConfigurationSection::sectionInformation
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A * ___sectionInformation_13;
	// System.Configuration.IConfigurationSectionHandler System.Configuration.ConfigurationSection::section_handler
	RuntimeObject* ___section_handler_14;
	// System.String System.Configuration.ConfigurationSection::externalDataXml
	String_t* ___externalDataXml_15;
	// System.Object System.Configuration.ConfigurationSection::_configContext
	RuntimeObject * ____configContext_16;

public:
	inline static int32_t get_offset_of_sectionInformation_13() { return static_cast<int32_t>(offsetof(ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD, ___sectionInformation_13)); }
	inline SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A * get_sectionInformation_13() const { return ___sectionInformation_13; }
	inline SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A ** get_address_of_sectionInformation_13() { return &___sectionInformation_13; }
	inline void set_sectionInformation_13(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A * value)
	{
		___sectionInformation_13 = value;
		Il2CppCodeGenWriteBarrier((&___sectionInformation_13), value);
	}

	inline static int32_t get_offset_of_section_handler_14() { return static_cast<int32_t>(offsetof(ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD, ___section_handler_14)); }
	inline RuntimeObject* get_section_handler_14() const { return ___section_handler_14; }
	inline RuntimeObject** get_address_of_section_handler_14() { return &___section_handler_14; }
	inline void set_section_handler_14(RuntimeObject* value)
	{
		___section_handler_14 = value;
		Il2CppCodeGenWriteBarrier((&___section_handler_14), value);
	}

	inline static int32_t get_offset_of_externalDataXml_15() { return static_cast<int32_t>(offsetof(ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD, ___externalDataXml_15)); }
	inline String_t* get_externalDataXml_15() const { return ___externalDataXml_15; }
	inline String_t** get_address_of_externalDataXml_15() { return &___externalDataXml_15; }
	inline void set_externalDataXml_15(String_t* value)
	{
		___externalDataXml_15 = value;
		Il2CppCodeGenWriteBarrier((&___externalDataXml_15), value);
	}

	inline static int32_t get_offset_of__configContext_16() { return static_cast<int32_t>(offsetof(ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD, ____configContext_16)); }
	inline RuntimeObject * get__configContext_16() const { return ____configContext_16; }
	inline RuntimeObject ** get_address_of__configContext_16() { return &____configContext_16; }
	inline void set__configContext_16(RuntimeObject * value)
	{
		____configContext_16 = value;
		Il2CppCodeGenWriteBarrier((&____configContext_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTION_TEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD_H
#ifndef CONFIGURATIONSECTIONCOLLECTION_T2BACE8691335F63E842B656C3688BDAFA4AC58EC_H
#define CONFIGURATIONSECTIONCOLLECTION_T2BACE8691335F63E842B656C3688BDAFA4AC58EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionCollection
struct  ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC  : public NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070
{
public:
	// System.Configuration.SectionGroupInfo System.Configuration.ConfigurationSectionCollection::group
	SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 * ___group_10;
	// System.Configuration.Configuration System.Configuration.ConfigurationSectionCollection::config
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * ___config_11;

public:
	inline static int32_t get_offset_of_group_10() { return static_cast<int32_t>(offsetof(ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC, ___group_10)); }
	inline SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 * get_group_10() const { return ___group_10; }
	inline SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 ** get_address_of_group_10() { return &___group_10; }
	inline void set_group_10(SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 * value)
	{
		___group_10 = value;
		Il2CppCodeGenWriteBarrier((&___group_10), value);
	}

	inline static int32_t get_offset_of_config_11() { return static_cast<int32_t>(offsetof(ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC, ___config_11)); }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * get_config_11() const { return ___config_11; }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 ** get_address_of_config_11() { return &___config_11; }
	inline void set_config_11(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * value)
	{
		___config_11 = value;
		Il2CppCodeGenWriteBarrier((&___config_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTIONCOLLECTION_T2BACE8691335F63E842B656C3688BDAFA4AC58EC_H
#ifndef CONFIGURATIONSECTIONGROUPCOLLECTION_T8FBD02E07771CB420A315A28E362FC9B48499052_H
#define CONFIGURATIONSECTIONGROUPCOLLECTION_T8FBD02E07771CB420A315A28E362FC9B48499052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSectionGroupCollection
struct  ConfigurationSectionGroupCollection_t8FBD02E07771CB420A315A28E362FC9B48499052  : public NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070
{
public:
	// System.Configuration.SectionGroupInfo System.Configuration.ConfigurationSectionGroupCollection::group
	SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 * ___group_10;
	// System.Configuration.Configuration System.Configuration.ConfigurationSectionGroupCollection::config
	Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * ___config_11;

public:
	inline static int32_t get_offset_of_group_10() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroupCollection_t8FBD02E07771CB420A315A28E362FC9B48499052, ___group_10)); }
	inline SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 * get_group_10() const { return ___group_10; }
	inline SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 ** get_address_of_group_10() { return &___group_10; }
	inline void set_group_10(SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61 * value)
	{
		___group_10 = value;
		Il2CppCodeGenWriteBarrier((&___group_10), value);
	}

	inline static int32_t get_offset_of_config_11() { return static_cast<int32_t>(offsetof(ConfigurationSectionGroupCollection_t8FBD02E07771CB420A315A28E362FC9B48499052, ___config_11)); }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * get_config_11() const { return ___config_11; }
	inline Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 ** get_address_of_config_11() { return &___config_11; }
	inline void set_config_11(Configuration_t29470A3F7814259C2078D6C6229F221EE569FC01 * value)
	{
		___config_11 = value;
		Il2CppCodeGenWriteBarrier((&___config_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSECTIONGROUPCOLLECTION_T8FBD02E07771CB420A315A28E362FC9B48499052_H
#ifndef CONFIGURATIONVALIDATORATTRIBUTE_T76B8E3B6ADAF51BE9EB82BBA4A5C7213DDBEC724_H
#define CONFIGURATIONVALIDATORATTRIBUTE_T76B8E3B6ADAF51BE9EB82BBA4A5C7213DDBEC724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationValidatorAttribute
struct  ConfigurationValidatorAttribute_t76B8E3B6ADAF51BE9EB82BBA4A5C7213DDBEC724  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:
	// System.Type System.Configuration.ConfigurationValidatorAttribute::validatorType
	Type_t * ___validatorType_0;
	// System.Configuration.ConfigurationValidatorBase System.Configuration.ConfigurationValidatorAttribute::instance
	ConfigurationValidatorBase_tD691540223153E9DEA98BDBD778BCB12A56248ED * ___instance_1;

public:
	inline static int32_t get_offset_of_validatorType_0() { return static_cast<int32_t>(offsetof(ConfigurationValidatorAttribute_t76B8E3B6ADAF51BE9EB82BBA4A5C7213DDBEC724, ___validatorType_0)); }
	inline Type_t * get_validatorType_0() const { return ___validatorType_0; }
	inline Type_t ** get_address_of_validatorType_0() { return &___validatorType_0; }
	inline void set_validatorType_0(Type_t * value)
	{
		___validatorType_0 = value;
		Il2CppCodeGenWriteBarrier((&___validatorType_0), value);
	}

	inline static int32_t get_offset_of_instance_1() { return static_cast<int32_t>(offsetof(ConfigurationValidatorAttribute_t76B8E3B6ADAF51BE9EB82BBA4A5C7213DDBEC724, ___instance_1)); }
	inline ConfigurationValidatorBase_tD691540223153E9DEA98BDBD778BCB12A56248ED * get_instance_1() const { return ___instance_1; }
	inline ConfigurationValidatorBase_tD691540223153E9DEA98BDBD778BCB12A56248ED ** get_address_of_instance_1() { return &___instance_1; }
	inline void set_instance_1(ConfigurationValidatorBase_tD691540223153E9DEA98BDBD778BCB12A56248ED * value)
	{
		___instance_1 = value;
		Il2CppCodeGenWriteBarrier((&___instance_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONVALIDATORATTRIBUTE_T76B8E3B6ADAF51BE9EB82BBA4A5C7213DDBEC724_H
#ifndef DEFAULTVALIDATOR_T6B6B01DC537A705EB34A6757737ED4287BF4CC7A_H
#define DEFAULTVALIDATOR_T6B6B01DC537A705EB34A6757737ED4287BF4CC7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.DefaultValidator
struct  DefaultValidator_t6B6B01DC537A705EB34A6757737ED4287BF4CC7A  : public ConfigurationValidatorBase_tD691540223153E9DEA98BDBD778BCB12A56248ED
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALIDATOR_T6B6B01DC537A705EB34A6757737ED4287BF4CC7A_H
#ifndef EXECONFIGURATIONFILEMAP_T07633C2760732AD65BCFFCFB0D2F0D6C05F527AB_H
#define EXECONFIGURATIONFILEMAP_T07633C2760732AD65BCFFCFB0D2F0D6C05F527AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ExeConfigurationFileMap
struct  ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB  : public ConfigurationFileMap_tE596BD02408620847DC8AA9D93423B610AB224F0
{
public:
	// System.String System.Configuration.ExeConfigurationFileMap::exeConfigFilename
	String_t* ___exeConfigFilename_1;
	// System.String System.Configuration.ExeConfigurationFileMap::localUserConfigFilename
	String_t* ___localUserConfigFilename_2;
	// System.String System.Configuration.ExeConfigurationFileMap::roamingUserConfigFilename
	String_t* ___roamingUserConfigFilename_3;

public:
	inline static int32_t get_offset_of_exeConfigFilename_1() { return static_cast<int32_t>(offsetof(ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB, ___exeConfigFilename_1)); }
	inline String_t* get_exeConfigFilename_1() const { return ___exeConfigFilename_1; }
	inline String_t** get_address_of_exeConfigFilename_1() { return &___exeConfigFilename_1; }
	inline void set_exeConfigFilename_1(String_t* value)
	{
		___exeConfigFilename_1 = value;
		Il2CppCodeGenWriteBarrier((&___exeConfigFilename_1), value);
	}

	inline static int32_t get_offset_of_localUserConfigFilename_2() { return static_cast<int32_t>(offsetof(ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB, ___localUserConfigFilename_2)); }
	inline String_t* get_localUserConfigFilename_2() const { return ___localUserConfigFilename_2; }
	inline String_t** get_address_of_localUserConfigFilename_2() { return &___localUserConfigFilename_2; }
	inline void set_localUserConfigFilename_2(String_t* value)
	{
		___localUserConfigFilename_2 = value;
		Il2CppCodeGenWriteBarrier((&___localUserConfigFilename_2), value);
	}

	inline static int32_t get_offset_of_roamingUserConfigFilename_3() { return static_cast<int32_t>(offsetof(ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB, ___roamingUserConfigFilename_3)); }
	inline String_t* get_roamingUserConfigFilename_3() const { return ___roamingUserConfigFilename_3; }
	inline String_t** get_address_of_roamingUserConfigFilename_3() { return &___roamingUserConfigFilename_3; }
	inline void set_roamingUserConfigFilename_3(String_t* value)
	{
		___roamingUserConfigFilename_3 = value;
		Il2CppCodeGenWriteBarrier((&___roamingUserConfigFilename_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECONFIGURATIONFILEMAP_T07633C2760732AD65BCFFCFB0D2F0D6C05F527AB_H
#ifndef PROPERTYINFORMATIONCOLLECTION_T6CBE051D205C8065A196AD13E4E5FA575B82AD52_H
#define PROPERTYINFORMATIONCOLLECTION_T6CBE051D205C8065A196AD13E4E5FA575B82AD52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.PropertyInformationCollection
struct  PropertyInformationCollection_t6CBE051D205C8065A196AD13E4E5FA575B82AD52  : public NameObjectCollectionBase_tC2EE4FB130214FAE7365D519959A2A34DE56E070
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFORMATIONCOLLECTION_T6CBE051D205C8065A196AD13E4E5FA575B82AD52_H
#ifndef PROTECTEDCONFIGURATIONPROVIDER_T6D1CFE3639AFF7A42A854B97BD575CFE05ED68A0_H
#define PROTECTEDCONFIGURATIONPROVIDER_T6D1CFE3639AFF7A42A854B97BD575CFE05ED68A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfigurationProvider
struct  ProtectedConfigurationProvider_t6D1CFE3639AFF7A42A854B97BD575CFE05ED68A0  : public ProviderBase_t92D989F50324ADBD9423D8B2E92FEE752477D380
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDCONFIGURATIONPROVIDER_T6D1CFE3639AFF7A42A854B97BD575CFE05ED68A0_H
#ifndef PROTECTEDCONFIGURATIONPROVIDERCOLLECTION_T6F2A506BF598B7730852E9C57D207277687FBCDE_H
#define PROTECTEDCONFIGURATIONPROVIDERCOLLECTION_T6F2A506BF598B7730852E9C57D207277687FBCDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfigurationProviderCollection
struct  ProtectedConfigurationProviderCollection_t6F2A506BF598B7730852E9C57D207277687FBCDE  : public ProviderCollection_tF8C99700C404E5F24DA5BCB672AA7FBBE4BB1386
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDCONFIGURATIONPROVIDERCOLLECTION_T6F2A506BF598B7730852E9C57D207277687FBCDE_H
#ifndef PROVIDERSETTINGS_T99D7F6BF7553F7E9365222895DCB944F66F282B0_H
#define PROVIDERSETTINGS_T99D7F6BF7553F7E9365222895DCB944F66F282B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProviderSettings
struct  ProviderSettings_t99D7F6BF7553F7E9365222895DCB944F66F282B0  : public ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63
{
public:
	// System.Configuration.ConfigNameValueCollection System.Configuration.ProviderSettings::parameters
	ConfigNameValueCollection_t6BC2AF313208E6493F818D04E62F2C4ECE42A5CA * ___parameters_13;

public:
	inline static int32_t get_offset_of_parameters_13() { return static_cast<int32_t>(offsetof(ProviderSettings_t99D7F6BF7553F7E9365222895DCB944F66F282B0, ___parameters_13)); }
	inline ConfigNameValueCollection_t6BC2AF313208E6493F818D04E62F2C4ECE42A5CA * get_parameters_13() const { return ___parameters_13; }
	inline ConfigNameValueCollection_t6BC2AF313208E6493F818D04E62F2C4ECE42A5CA ** get_address_of_parameters_13() { return &___parameters_13; }
	inline void set_parameters_13(ConfigNameValueCollection_t6BC2AF313208E6493F818D04E62F2C4ECE42A5CA * value)
	{
		___parameters_13 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_13), value);
	}
};

struct ProviderSettings_t99D7F6BF7553F7E9365222895DCB944F66F282B0_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Configuration.ProviderSettings::nameProp
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * ___nameProp_14;
	// System.Configuration.ConfigurationProperty System.Configuration.ProviderSettings::typeProp
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * ___typeProp_15;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProviderSettings::properties
	ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * ___properties_16;

public:
	inline static int32_t get_offset_of_nameProp_14() { return static_cast<int32_t>(offsetof(ProviderSettings_t99D7F6BF7553F7E9365222895DCB944F66F282B0_StaticFields, ___nameProp_14)); }
	inline ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * get_nameProp_14() const { return ___nameProp_14; }
	inline ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 ** get_address_of_nameProp_14() { return &___nameProp_14; }
	inline void set_nameProp_14(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * value)
	{
		___nameProp_14 = value;
		Il2CppCodeGenWriteBarrier((&___nameProp_14), value);
	}

	inline static int32_t get_offset_of_typeProp_15() { return static_cast<int32_t>(offsetof(ProviderSettings_t99D7F6BF7553F7E9365222895DCB944F66F282B0_StaticFields, ___typeProp_15)); }
	inline ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * get_typeProp_15() const { return ___typeProp_15; }
	inline ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 ** get_address_of_typeProp_15() { return &___typeProp_15; }
	inline void set_typeProp_15(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * value)
	{
		___typeProp_15 = value;
		Il2CppCodeGenWriteBarrier((&___typeProp_15), value);
	}

	inline static int32_t get_offset_of_properties_16() { return static_cast<int32_t>(offsetof(ProviderSettings_t99D7F6BF7553F7E9365222895DCB944F66F282B0_StaticFields, ___properties_16)); }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * get_properties_16() const { return ___properties_16; }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 ** get_address_of_properties_16() { return &___properties_16; }
	inline void set_properties_16(ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * value)
	{
		___properties_16 = value;
		Il2CppCodeGenWriteBarrier((&___properties_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERSETTINGS_T99D7F6BF7553F7E9365222895DCB944F66F282B0_H
#ifndef SECTIONGROUPINFO_T5B331E18DED09B455B66D186D9F9CBF2D5B2FE61_H
#define SECTIONGROUPINFO_T5B331E18DED09B455B66D186D9F9CBF2D5B2FE61_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.SectionGroupInfo
struct  SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61  : public ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698
{
public:
	// System.Configuration.ConfigInfoCollection System.Configuration.SectionGroupInfo::sections
	ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A * ___sections_6;
	// System.Configuration.ConfigInfoCollection System.Configuration.SectionGroupInfo::groups
	ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A * ___groups_7;

public:
	inline static int32_t get_offset_of_sections_6() { return static_cast<int32_t>(offsetof(SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61, ___sections_6)); }
	inline ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A * get_sections_6() const { return ___sections_6; }
	inline ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A ** get_address_of_sections_6() { return &___sections_6; }
	inline void set_sections_6(ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A * value)
	{
		___sections_6 = value;
		Il2CppCodeGenWriteBarrier((&___sections_6), value);
	}

	inline static int32_t get_offset_of_groups_7() { return static_cast<int32_t>(offsetof(SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61, ___groups_7)); }
	inline ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A * get_groups_7() const { return ___groups_7; }
	inline ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A ** get_address_of_groups_7() { return &___groups_7; }
	inline void set_groups_7(ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A * value)
	{
		___groups_7 = value;
		Il2CppCodeGenWriteBarrier((&___groups_7), value);
	}
};

struct SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61_StaticFields
{
public:
	// System.Configuration.ConfigInfoCollection System.Configuration.SectionGroupInfo::emptyList
	ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A * ___emptyList_8;

public:
	inline static int32_t get_offset_of_emptyList_8() { return static_cast<int32_t>(offsetof(SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61_StaticFields, ___emptyList_8)); }
	inline ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A * get_emptyList_8() const { return ___emptyList_8; }
	inline ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A ** get_address_of_emptyList_8() { return &___emptyList_8; }
	inline void set_emptyList_8(ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A * value)
	{
		___emptyList_8 = value;
		Il2CppCodeGenWriteBarrier((&___emptyList_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTIONGROUPINFO_T5B331E18DED09B455B66D186D9F9CBF2D5B2FE61_H
#ifndef ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#define ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF  : public ValueType_t1810BD84E0FDB5D3A7CD34286A5B22F343995C9C
{
public:

public:
};

struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t79D85CE93255C78D04436552445C364ED409B744* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t5AAC444DFCAA78411386665A25FE3CD3169879EF_marshaled_com
{
};
#endif // ENUM_T5AAC444DFCAA78411386665A25FE3CD3169879EF_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MONOTODOATTRIBUTE_TC7E5E9F0CD72D820EAE8779D8457C93F3E5A8333_H
#define MONOTODOATTRIBUTE_TC7E5E9F0CD72D820EAE8779D8457C93F3E5A8333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoTODOAttribute
struct  MonoTODOAttribute_tC7E5E9F0CD72D820EAE8779D8457C93F3E5A8333  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:
	// System.String System.MonoTODOAttribute::comment
	String_t* ___comment_0;

public:
	inline static int32_t get_offset_of_comment_0() { return static_cast<int32_t>(offsetof(MonoTODOAttribute_tC7E5E9F0CD72D820EAE8779D8457C93F3E5A8333, ___comment_0)); }
	inline String_t* get_comment_0() const { return ___comment_0; }
	inline String_t** get_address_of_comment_0() { return &___comment_0; }
	inline void set_comment_0(String_t* value)
	{
		___comment_0 = value;
		Il2CppCodeGenWriteBarrier((&___comment_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOTODOATTRIBUTE_TC7E5E9F0CD72D820EAE8779D8457C93F3E5A8333_H
#ifndef KEYEDHASHALGORITHM_TEC8DB9B03E3086C7943F706BC5FB42FB0423E31C_H
#define KEYEDHASHALGORITHM_TEC8DB9B03E3086C7943F706BC5FB42FB0423E31C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.KeyedHashAlgorithm
struct  KeyedHashAlgorithm_tEC8DB9B03E3086C7943F706BC5FB42FB0423E31C  : public HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0
{
public:
	// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::KeyValue
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___KeyValue_4;

public:
	inline static int32_t get_offset_of_KeyValue_4() { return static_cast<int32_t>(offsetof(KeyedHashAlgorithm_tEC8DB9B03E3086C7943F706BC5FB42FB0423E31C, ___KeyValue_4)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_KeyValue_4() const { return ___KeyValue_4; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_KeyValue_4() { return &___KeyValue_4; }
	inline void set_KeyValue_4(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___KeyValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEDHASHALGORITHM_TEC8DB9B03E3086C7943F706BC5FB42FB0423E31C_H
#ifndef RSA_T565B1E1F95589A0E37D0C66406CF730DC4A43051_H
#define RSA_T565B1E1F95589A0E37D0C66406CF730DC4A43051_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.RSA
struct  RSA_t565B1E1F95589A0E37D0C66406CF730DC4A43051  : public AsymmetricAlgorithm_tCE2A08658EFEA671D977BC39FA246B4FC5092BB2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSA_T565B1E1F95589A0E37D0C66406CF730DC4A43051_H
#ifndef TIMESPAN_T26DE4373EA745C072A5FBDEAD8373348955FC374_H
#define TIMESPAN_T26DE4373EA745C072A5FBDEAD8373348955FC374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374_StaticFields, ___Zero_7)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T26DE4373EA745C072A5FBDEAD8373348955FC374_H
#ifndef VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#define VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_tDB81A15FA2AB53E2401A76B745D215397B29F783 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_TDB81A15FA2AB53E2401A76B745D215397B29F783_H
#ifndef XMLTEXTREADER_T3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3_H
#define XMLTEXTREADER_T3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReader
struct  XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3  : public XmlReader_tC30823FFBFDF75F5ECDE7FA4EB1DBE8358E06293
{
public:
	// System.Xml.XmlTextReader System.Xml.XmlTextReader::entity
	XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 * ___entity_2;
	// Mono.Xml2.XmlTextReader System.Xml.XmlTextReader::source
	XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD * ___source_3;
	// System.Boolean System.Xml.XmlTextReader::entityInsideAttribute
	bool ___entityInsideAttribute_4;
	// System.Boolean System.Xml.XmlTextReader::insideAttribute
	bool ___insideAttribute_5;
	// System.Collections.Generic.Stack`1<System.String> System.Xml.XmlTextReader::entityNameStack
	Stack_1_t36D6DE3CB7FEC6EDF6EA749E72AD210F334D8F45 * ___entityNameStack_6;

public:
	inline static int32_t get_offset_of_entity_2() { return static_cast<int32_t>(offsetof(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3, ___entity_2)); }
	inline XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 * get_entity_2() const { return ___entity_2; }
	inline XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 ** get_address_of_entity_2() { return &___entity_2; }
	inline void set_entity_2(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3 * value)
	{
		___entity_2 = value;
		Il2CppCodeGenWriteBarrier((&___entity_2), value);
	}

	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3, ___source_3)); }
	inline XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD * get_source_3() const { return ___source_3; }
	inline XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(XmlTextReader_tD979FF6B5FEF83373F4D669070FDD627F2447BCD * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((&___source_3), value);
	}

	inline static int32_t get_offset_of_entityInsideAttribute_4() { return static_cast<int32_t>(offsetof(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3, ___entityInsideAttribute_4)); }
	inline bool get_entityInsideAttribute_4() const { return ___entityInsideAttribute_4; }
	inline bool* get_address_of_entityInsideAttribute_4() { return &___entityInsideAttribute_4; }
	inline void set_entityInsideAttribute_4(bool value)
	{
		___entityInsideAttribute_4 = value;
	}

	inline static int32_t get_offset_of_insideAttribute_5() { return static_cast<int32_t>(offsetof(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3, ___insideAttribute_5)); }
	inline bool get_insideAttribute_5() const { return ___insideAttribute_5; }
	inline bool* get_address_of_insideAttribute_5() { return &___insideAttribute_5; }
	inline void set_insideAttribute_5(bool value)
	{
		___insideAttribute_5 = value;
	}

	inline static int32_t get_offset_of_entityNameStack_6() { return static_cast<int32_t>(offsetof(XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3, ___entityNameStack_6)); }
	inline Stack_1_t36D6DE3CB7FEC6EDF6EA749E72AD210F334D8F45 * get_entityNameStack_6() const { return ___entityNameStack_6; }
	inline Stack_1_t36D6DE3CB7FEC6EDF6EA749E72AD210F334D8F45 ** get_address_of_entityNameStack_6() { return &___entityNameStack_6; }
	inline void set_entityNameStack_6(Stack_1_t36D6DE3CB7FEC6EDF6EA749E72AD210F334D8F45 * value)
	{
		___entityNameStack_6 = value;
		Il2CppCodeGenWriteBarrier((&___entityNameStack_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTREADER_T3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3_H
#ifndef CONFIGXMLTEXTREADER_T8DBF4B876F941205E972027A42FC799AA145780A_H
#define CONFIGXMLTEXTREADER_T8DBF4B876F941205E972027A42FC799AA145780A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfigXmlTextReader
struct  ConfigXmlTextReader_t8DBF4B876F941205E972027A42FC799AA145780A  : public XmlTextReader_t3A388ABDDBD41FA8B4266A49D00CB7A8C5DC4BE3
{
public:
	// System.String ConfigXmlTextReader::fileName
	String_t* ___fileName_7;

public:
	inline static int32_t get_offset_of_fileName_7() { return static_cast<int32_t>(offsetof(ConfigXmlTextReader_t8DBF4B876F941205E972027A42FC799AA145780A, ___fileName_7)); }
	inline String_t* get_fileName_7() const { return ___fileName_7; }
	inline String_t** get_address_of_fileName_7() { return &___fileName_7; }
	inline void set_fileName_7(String_t* value)
	{
		___fileName_7 = value;
		Il2CppCodeGenWriteBarrier((&___fileName_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGXMLTEXTREADER_T8DBF4B876F941205E972027A42FC799AA145780A_H
#ifndef SIGN_TDBA68F5BB24DC260357E23BAC3ADF8B8640AD541_H
#define SIGN_TDBA68F5BB24DC260357E23BAC3ADF8B8640AD541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.BigInteger_Sign
struct  Sign_tDBA68F5BB24DC260357E23BAC3ADF8B8640AD541 
{
public:
	// System.Int32 Mono.Math.BigInteger_Sign::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Sign_tDBA68F5BB24DC260357E23BAC3ADF8B8640AD541, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGN_TDBA68F5BB24DC260357E23BAC3ADF8B8640AD541_H
#ifndef CONFIDENCEFACTOR_T09F5717EE806C44975FD23B3ED7DB48019FFD84E_H
#define CONFIDENCEFACTOR_T09F5717EE806C44975FD23B3ED7DB48019FFD84E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Math.Prime.ConfidenceFactor
struct  ConfidenceFactor_t09F5717EE806C44975FD23B3ED7DB48019FFD84E 
{
public:
	// System.Int32 Mono.Math.Prime.ConfidenceFactor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfidenceFactor_t09F5717EE806C44975FD23B3ED7DB48019FFD84E, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIDENCEFACTOR_T09F5717EE806C44975FD23B3ED7DB48019FFD84E_H
#ifndef HMAC_TEEBB2925FEAD31F6F5009153CF0056C327490FA1_H
#define HMAC_TEEBB2925FEAD31F6F5009153CF0056C327490FA1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.HMAC
struct  HMAC_tEEBB2925FEAD31F6F5009153CF0056C327490FA1  : public KeyedHashAlgorithm_tEC8DB9B03E3086C7943F706BC5FB42FB0423E31C
{
public:
	// System.Security.Cryptography.HashAlgorithm Mono.Security.Cryptography.HMAC::hash
	HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0 * ___hash_5;
	// System.Boolean Mono.Security.Cryptography.HMAC::hashing
	bool ___hashing_6;
	// System.Byte[] Mono.Security.Cryptography.HMAC::innerPad
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___innerPad_7;
	// System.Byte[] Mono.Security.Cryptography.HMAC::outerPad
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___outerPad_8;

public:
	inline static int32_t get_offset_of_hash_5() { return static_cast<int32_t>(offsetof(HMAC_tEEBB2925FEAD31F6F5009153CF0056C327490FA1, ___hash_5)); }
	inline HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0 * get_hash_5() const { return ___hash_5; }
	inline HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0 ** get_address_of_hash_5() { return &___hash_5; }
	inline void set_hash_5(HashAlgorithm_t6A31A9B4CBC494EA9286D2A1097C6DC9BCE96CA0 * value)
	{
		___hash_5 = value;
		Il2CppCodeGenWriteBarrier((&___hash_5), value);
	}

	inline static int32_t get_offset_of_hashing_6() { return static_cast<int32_t>(offsetof(HMAC_tEEBB2925FEAD31F6F5009153CF0056C327490FA1, ___hashing_6)); }
	inline bool get_hashing_6() const { return ___hashing_6; }
	inline bool* get_address_of_hashing_6() { return &___hashing_6; }
	inline void set_hashing_6(bool value)
	{
		___hashing_6 = value;
	}

	inline static int32_t get_offset_of_innerPad_7() { return static_cast<int32_t>(offsetof(HMAC_tEEBB2925FEAD31F6F5009153CF0056C327490FA1, ___innerPad_7)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_innerPad_7() const { return ___innerPad_7; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_innerPad_7() { return &___innerPad_7; }
	inline void set_innerPad_7(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___innerPad_7 = value;
		Il2CppCodeGenWriteBarrier((&___innerPad_7), value);
	}

	inline static int32_t get_offset_of_outerPad_8() { return static_cast<int32_t>(offsetof(HMAC_tEEBB2925FEAD31F6F5009153CF0056C327490FA1, ___outerPad_8)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_outerPad_8() const { return ___outerPad_8; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_outerPad_8() { return &___outerPad_8; }
	inline void set_outerPad_8(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___outerPad_8 = value;
		Il2CppCodeGenWriteBarrier((&___outerPad_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HMAC_TEEBB2925FEAD31F6F5009153CF0056C327490FA1_H
#ifndef MD2MANAGED_T90C3C60E33C09EB91A59C28E21E858A3CC56BAA3_H
#define MD2MANAGED_T90C3C60E33C09EB91A59C28E21E858A3CC56BAA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD2Managed
struct  MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3  : public MD2_tD12D2F4F3A46581687DEC22F27262968546E1DC7
{
public:
	// System.Byte[] Mono.Security.Cryptography.MD2Managed::state
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___state_4;
	// System.Byte[] Mono.Security.Cryptography.MD2Managed::checksum
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___checksum_5;
	// System.Byte[] Mono.Security.Cryptography.MD2Managed::buffer
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___buffer_6;
	// System.Int32 Mono.Security.Cryptography.MD2Managed::count
	int32_t ___count_7;
	// System.Byte[] Mono.Security.Cryptography.MD2Managed::x
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___x_8;

public:
	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3, ___state_4)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_state_4() const { return ___state_4; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_checksum_5() { return static_cast<int32_t>(offsetof(MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3, ___checksum_5)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_checksum_5() const { return ___checksum_5; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_checksum_5() { return &___checksum_5; }
	inline void set_checksum_5(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___checksum_5 = value;
		Il2CppCodeGenWriteBarrier((&___checksum_5), value);
	}

	inline static int32_t get_offset_of_buffer_6() { return static_cast<int32_t>(offsetof(MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3, ___buffer_6)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_buffer_6() const { return ___buffer_6; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_buffer_6() { return &___buffer_6; }
	inline void set_buffer_6(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___buffer_6 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_6), value);
	}

	inline static int32_t get_offset_of_count_7() { return static_cast<int32_t>(offsetof(MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3, ___count_7)); }
	inline int32_t get_count_7() const { return ___count_7; }
	inline int32_t* get_address_of_count_7() { return &___count_7; }
	inline void set_count_7(int32_t value)
	{
		___count_7 = value;
	}

	inline static int32_t get_offset_of_x_8() { return static_cast<int32_t>(offsetof(MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3, ___x_8)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_x_8() const { return ___x_8; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_x_8() { return &___x_8; }
	inline void set_x_8(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___x_8 = value;
		Il2CppCodeGenWriteBarrier((&___x_8), value);
	}
};

struct MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3_StaticFields
{
public:
	// System.Byte[] Mono.Security.Cryptography.MD2Managed::PI_SUBST
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___PI_SUBST_9;

public:
	inline static int32_t get_offset_of_PI_SUBST_9() { return static_cast<int32_t>(offsetof(MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3_StaticFields, ___PI_SUBST_9)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_PI_SUBST_9() const { return ___PI_SUBST_9; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_PI_SUBST_9() { return &___PI_SUBST_9; }
	inline void set_PI_SUBST_9(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___PI_SUBST_9 = value;
		Il2CppCodeGenWriteBarrier((&___PI_SUBST_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD2MANAGED_T90C3C60E33C09EB91A59C28E21E858A3CC56BAA3_H
#ifndef MD4MANAGED_TD87CADDB8B09C3BE9F77AEC14BA1E56765908F47_H
#define MD4MANAGED_TD87CADDB8B09C3BE9F77AEC14BA1E56765908F47_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.MD4Managed
struct  MD4Managed_tD87CADDB8B09C3BE9F77AEC14BA1E56765908F47  : public MD4_t089B8663437920C4CA21F863DD9E87FB3B95E023
{
public:
	// System.UInt32[] Mono.Security.Cryptography.MD4Managed::state
	UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* ___state_4;
	// System.Byte[] Mono.Security.Cryptography.MD4Managed::buffer
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___buffer_5;
	// System.UInt32[] Mono.Security.Cryptography.MD4Managed::count
	UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* ___count_6;
	// System.UInt32[] Mono.Security.Cryptography.MD4Managed::x
	UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* ___x_7;
	// System.Byte[] Mono.Security.Cryptography.MD4Managed::digest
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___digest_8;

public:
	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(MD4Managed_tD87CADDB8B09C3BE9F77AEC14BA1E56765908F47, ___state_4)); }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* get_state_4() const { return ___state_4; }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5** get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* value)
	{
		___state_4 = value;
		Il2CppCodeGenWriteBarrier((&___state_4), value);
	}

	inline static int32_t get_offset_of_buffer_5() { return static_cast<int32_t>(offsetof(MD4Managed_tD87CADDB8B09C3BE9F77AEC14BA1E56765908F47, ___buffer_5)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_buffer_5() const { return ___buffer_5; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_buffer_5() { return &___buffer_5; }
	inline void set_buffer_5(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___buffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_5), value);
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(MD4Managed_tD87CADDB8B09C3BE9F77AEC14BA1E56765908F47, ___count_6)); }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* get_count_6() const { return ___count_6; }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5** get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* value)
	{
		___count_6 = value;
		Il2CppCodeGenWriteBarrier((&___count_6), value);
	}

	inline static int32_t get_offset_of_x_7() { return static_cast<int32_t>(offsetof(MD4Managed_tD87CADDB8B09C3BE9F77AEC14BA1E56765908F47, ___x_7)); }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* get_x_7() const { return ___x_7; }
	inline UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5** get_address_of_x_7() { return &___x_7; }
	inline void set_x_7(UInt32U5BU5D_tEEB193BE31C017A3312DB0BD88771471F92391F5* value)
	{
		___x_7 = value;
		Il2CppCodeGenWriteBarrier((&___x_7), value);
	}

	inline static int32_t get_offset_of_digest_8() { return static_cast<int32_t>(offsetof(MD4Managed_tD87CADDB8B09C3BE9F77AEC14BA1E56765908F47, ___digest_8)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_digest_8() const { return ___digest_8; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_digest_8() { return &___digest_8; }
	inline void set_digest_8(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___digest_8 = value;
		Il2CppCodeGenWriteBarrier((&___digest_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD4MANAGED_TD87CADDB8B09C3BE9F77AEC14BA1E56765908F47_H
#ifndef RSAMANAGED_TDCF904147E249A455F681A10FE7CCBA6250E44EA_H
#define RSAMANAGED_TDCF904147E249A455F681A10FE7CCBA6250E44EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.RSAManaged
struct  RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA  : public RSA_t565B1E1F95589A0E37D0C66406CF730DC4A43051
{
public:
	// System.Boolean Mono.Security.Cryptography.RSAManaged::isCRTpossible
	bool ___isCRTpossible_2;
	// System.Boolean Mono.Security.Cryptography.RSAManaged::keyBlinding
	bool ___keyBlinding_3;
	// System.Boolean Mono.Security.Cryptography.RSAManaged::keypairGenerated
	bool ___keypairGenerated_4;
	// System.Boolean Mono.Security.Cryptography.RSAManaged::m_disposed
	bool ___m_disposed_5;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::d
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * ___d_6;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::p
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * ___p_7;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::q
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * ___q_8;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::dp
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * ___dp_9;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::dq
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * ___dq_10;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::qInv
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * ___qInv_11;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::n
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * ___n_12;
	// Mono.Math.BigInteger Mono.Security.Cryptography.RSAManaged::e
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * ___e_13;
	// Mono.Security.Cryptography.RSAManaged_KeyGeneratedEventHandler Mono.Security.Cryptography.RSAManaged::KeyGenerated
	KeyGeneratedEventHandler_t40F3B0E6400CF6FB02BED8AB74B8BFEC1DBA5042 * ___KeyGenerated_14;

public:
	inline static int32_t get_offset_of_isCRTpossible_2() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___isCRTpossible_2)); }
	inline bool get_isCRTpossible_2() const { return ___isCRTpossible_2; }
	inline bool* get_address_of_isCRTpossible_2() { return &___isCRTpossible_2; }
	inline void set_isCRTpossible_2(bool value)
	{
		___isCRTpossible_2 = value;
	}

	inline static int32_t get_offset_of_keyBlinding_3() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___keyBlinding_3)); }
	inline bool get_keyBlinding_3() const { return ___keyBlinding_3; }
	inline bool* get_address_of_keyBlinding_3() { return &___keyBlinding_3; }
	inline void set_keyBlinding_3(bool value)
	{
		___keyBlinding_3 = value;
	}

	inline static int32_t get_offset_of_keypairGenerated_4() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___keypairGenerated_4)); }
	inline bool get_keypairGenerated_4() const { return ___keypairGenerated_4; }
	inline bool* get_address_of_keypairGenerated_4() { return &___keypairGenerated_4; }
	inline void set_keypairGenerated_4(bool value)
	{
		___keypairGenerated_4 = value;
	}

	inline static int32_t get_offset_of_m_disposed_5() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___m_disposed_5)); }
	inline bool get_m_disposed_5() const { return ___m_disposed_5; }
	inline bool* get_address_of_m_disposed_5() { return &___m_disposed_5; }
	inline void set_m_disposed_5(bool value)
	{
		___m_disposed_5 = value;
	}

	inline static int32_t get_offset_of_d_6() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___d_6)); }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * get_d_6() const { return ___d_6; }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E ** get_address_of_d_6() { return &___d_6; }
	inline void set_d_6(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * value)
	{
		___d_6 = value;
		Il2CppCodeGenWriteBarrier((&___d_6), value);
	}

	inline static int32_t get_offset_of_p_7() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___p_7)); }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * get_p_7() const { return ___p_7; }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E ** get_address_of_p_7() { return &___p_7; }
	inline void set_p_7(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * value)
	{
		___p_7 = value;
		Il2CppCodeGenWriteBarrier((&___p_7), value);
	}

	inline static int32_t get_offset_of_q_8() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___q_8)); }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * get_q_8() const { return ___q_8; }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E ** get_address_of_q_8() { return &___q_8; }
	inline void set_q_8(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * value)
	{
		___q_8 = value;
		Il2CppCodeGenWriteBarrier((&___q_8), value);
	}

	inline static int32_t get_offset_of_dp_9() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___dp_9)); }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * get_dp_9() const { return ___dp_9; }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E ** get_address_of_dp_9() { return &___dp_9; }
	inline void set_dp_9(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * value)
	{
		___dp_9 = value;
		Il2CppCodeGenWriteBarrier((&___dp_9), value);
	}

	inline static int32_t get_offset_of_dq_10() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___dq_10)); }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * get_dq_10() const { return ___dq_10; }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E ** get_address_of_dq_10() { return &___dq_10; }
	inline void set_dq_10(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * value)
	{
		___dq_10 = value;
		Il2CppCodeGenWriteBarrier((&___dq_10), value);
	}

	inline static int32_t get_offset_of_qInv_11() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___qInv_11)); }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * get_qInv_11() const { return ___qInv_11; }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E ** get_address_of_qInv_11() { return &___qInv_11; }
	inline void set_qInv_11(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * value)
	{
		___qInv_11 = value;
		Il2CppCodeGenWriteBarrier((&___qInv_11), value);
	}

	inline static int32_t get_offset_of_n_12() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___n_12)); }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * get_n_12() const { return ___n_12; }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E ** get_address_of_n_12() { return &___n_12; }
	inline void set_n_12(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * value)
	{
		___n_12 = value;
		Il2CppCodeGenWriteBarrier((&___n_12), value);
	}

	inline static int32_t get_offset_of_e_13() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___e_13)); }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * get_e_13() const { return ___e_13; }
	inline BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E ** get_address_of_e_13() { return &___e_13; }
	inline void set_e_13(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E * value)
	{
		___e_13 = value;
		Il2CppCodeGenWriteBarrier((&___e_13), value);
	}

	inline static int32_t get_offset_of_KeyGenerated_14() { return static_cast<int32_t>(offsetof(RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA, ___KeyGenerated_14)); }
	inline KeyGeneratedEventHandler_t40F3B0E6400CF6FB02BED8AB74B8BFEC1DBA5042 * get_KeyGenerated_14() const { return ___KeyGenerated_14; }
	inline KeyGeneratedEventHandler_t40F3B0E6400CF6FB02BED8AB74B8BFEC1DBA5042 ** get_address_of_KeyGenerated_14() { return &___KeyGenerated_14; }
	inline void set_KeyGenerated_14(KeyGeneratedEventHandler_t40F3B0E6400CF6FB02BED8AB74B8BFEC1DBA5042 * value)
	{
		___KeyGenerated_14 = value;
		Il2CppCodeGenWriteBarrier((&___KeyGenerated_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RSAMANAGED_TDCF904147E249A455F681A10FE7CCBA6250E44EA_H
#ifndef ALERTLEVEL_T9C9CDF74FA43002F88644710D78D35D7C1BDE8DF_H
#define ALERTLEVEL_T9C9CDF74FA43002F88644710D78D35D7C1BDE8DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Protocol.Tls.AlertLevel
struct  AlertLevel_t9C9CDF74FA43002F88644710D78D35D7C1BDE8DF 
{
public:
	// System.Byte Mono.Security.Protocol.Tls.AlertLevel::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AlertLevel_t9C9CDF74FA43002F88644710D78D35D7C1BDE8DF, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALERTLEVEL_T9C9CDF74FA43002F88644710D78D35D7C1BDE8DF_H
#ifndef KEYUSAGES_TB76B0C5F4E8A5C3C1ADD8E5036836F9513550A76_H
#define KEYUSAGES_TB76B0C5F4E8A5C3C1ADD8E5036836F9513550A76_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.KeyUsages
struct  KeyUsages_tB76B0C5F4E8A5C3C1ADD8E5036836F9513550A76 
{
public:
	// System.Int32 Mono.Security.X509.Extensions.KeyUsages::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyUsages_tB76B0C5F4E8A5C3C1ADD8E5036836F9513550A76, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYUSAGES_TB76B0C5F4E8A5C3C1ADD8E5036836F9513550A76_H
#ifndef CERTTYPES_T3C0F8B516A7FA9EFAD20B3AAC9BF91F97B674825_H
#define CERTTYPES_T3C0F8B516A7FA9EFAD20B3AAC9BF91F97B674825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.Extensions.NetscapeCertTypeExtension_CertTypes
struct  CertTypes_t3C0F8B516A7FA9EFAD20B3AAC9BF91F97B674825 
{
public:
	// System.Int32 Mono.Security.X509.Extensions.NetscapeCertTypeExtension_CertTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CertTypes_t3C0F8B516A7FA9EFAD20B3AAC9BF91F97B674825, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CERTTYPES_T3C0F8B516A7FA9EFAD20B3AAC9BF91F97B674825_H
#ifndef X509CHAINSTATUSFLAGS_T1256D2CC5C46DA044CAF8A56776A978115A2B79D_H
#define X509CHAINSTATUSFLAGS_T1256D2CC5C46DA044CAF8A56776A978115A2B79D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509ChainStatusFlags
struct  X509ChainStatusFlags_t1256D2CC5C46DA044CAF8A56776A978115A2B79D 
{
public:
	// System.Int32 Mono.Security.X509.X509ChainStatusFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(X509ChainStatusFlags_t1256D2CC5C46DA044CAF8A56776A978115A2B79D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAINSTATUSFLAGS_T1256D2CC5C46DA044CAF8A56776A978115A2B79D_H
#ifndef CONFIGURATIONALLOWDEFINITION_T18B6E25650B50173DD5DFB01BCF332724756329D_H
#define CONFIGURATIONALLOWDEFINITION_T18B6E25650B50173DD5DFB01BCF332724756329D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationAllowDefinition
struct  ConfigurationAllowDefinition_t18B6E25650B50173DD5DFB01BCF332724756329D 
{
public:
	// System.Int32 System.Configuration.ConfigurationAllowDefinition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationAllowDefinition_t18B6E25650B50173DD5DFB01BCF332724756329D, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONALLOWDEFINITION_T18B6E25650B50173DD5DFB01BCF332724756329D_H
#ifndef CONFIGURATIONALLOWEXEDEFINITION_T66ED2E212F6E3FCB79A1A28752B982FA68AED8BC_H
#define CONFIGURATIONALLOWEXEDEFINITION_T66ED2E212F6E3FCB79A1A28752B982FA68AED8BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationAllowExeDefinition
struct  ConfigurationAllowExeDefinition_t66ED2E212F6E3FCB79A1A28752B982FA68AED8BC 
{
public:
	// System.Int32 System.Configuration.ConfigurationAllowExeDefinition::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationAllowExeDefinition_t66ED2E212F6E3FCB79A1A28752B982FA68AED8BC, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONALLOWEXEDEFINITION_T66ED2E212F6E3FCB79A1A28752B982FA68AED8BC_H
#ifndef CONFIGURATIONLOCKTYPE_TFAC681894C188BA192647A4170910E9D1F078534_H
#define CONFIGURATIONLOCKTYPE_TFAC681894C188BA192647A4170910E9D1F078534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationLockType
struct  ConfigurationLockType_tFAC681894C188BA192647A4170910E9D1F078534 
{
public:
	// System.Int32 System.Configuration.ConfigurationLockType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationLockType_tFAC681894C188BA192647A4170910E9D1F078534, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONLOCKTYPE_TFAC681894C188BA192647A4170910E9D1F078534_H
#ifndef CONFIGURATIONPROPERTYOPTIONS_T64C3CDD6F3EAD472075436034225B6AF610468A9_H
#define CONFIGURATIONPROPERTYOPTIONS_T64C3CDD6F3EAD472075436034225B6AF610468A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationPropertyOptions
struct  ConfigurationPropertyOptions_t64C3CDD6F3EAD472075436034225B6AF610468A9 
{
public:
	// System.Int32 System.Configuration.ConfigurationPropertyOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationPropertyOptions_t64C3CDD6F3EAD472075436034225B6AF610468A9, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONPROPERTYOPTIONS_T64C3CDD6F3EAD472075436034225B6AF610468A9_H
#ifndef CONFIGURATIONSAVEMODE_TC56A23C692BDA4E51A37965E1B4B3B088076EDD5_H
#define CONFIGURATIONSAVEMODE_TC56A23C692BDA4E51A37965E1B4B3B088076EDD5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationSaveMode
struct  ConfigurationSaveMode_tC56A23C692BDA4E51A37965E1B4B3B088076EDD5 
{
public:
	// System.Int32 System.Configuration.ConfigurationSaveMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationSaveMode_tC56A23C692BDA4E51A37965E1B4B3B088076EDD5, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONSAVEMODE_TC56A23C692BDA4E51A37965E1B4B3B088076EDD5_H
#ifndef CONFIGURATIONUSERLEVEL_TEC7EA3B13C8E11D592EBA37FEED1A1CC10BEC7D5_H
#define CONFIGURATIONUSERLEVEL_TEC7EA3B13C8E11D592EBA37FEED1A1CC10BEC7D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationUserLevel
struct  ConfigurationUserLevel_tEC7EA3B13C8E11D592EBA37FEED1A1CC10BEC7D5 
{
public:
	// System.Int32 System.Configuration.ConfigurationUserLevel::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ConfigurationUserLevel_tEC7EA3B13C8E11D592EBA37FEED1A1CC10BEC7D5, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONUSERLEVEL_TEC7EA3B13C8E11D592EBA37FEED1A1CC10BEC7D5_H
#ifndef DEFAULTSECTION_TE9AB28D010EDC291C1BCA7552C69C901BE76643D_H
#define DEFAULTSECTION_TE9AB28D010EDC291C1BCA7552C69C901BE76643D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.DefaultSection
struct  DefaultSection_tE9AB28D010EDC291C1BCA7552C69C901BE76643D  : public ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD
{
public:

public:
};

struct DefaultSection_tE9AB28D010EDC291C1BCA7552C69C901BE76643D_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.DefaultSection::properties
	ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * ___properties_17;

public:
	inline static int32_t get_offset_of_properties_17() { return static_cast<int32_t>(offsetof(DefaultSection_tE9AB28D010EDC291C1BCA7552C69C901BE76643D_StaticFields, ___properties_17)); }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * get_properties_17() const { return ___properties_17; }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 ** get_address_of_properties_17() { return &___properties_17; }
	inline void set_properties_17(ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * value)
	{
		___properties_17 = value;
		Il2CppCodeGenWriteBarrier((&___properties_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSECTION_TE9AB28D010EDC291C1BCA7552C69C901BE76643D_H
#ifndef IGNORESECTION_T9052979AAF08694DF92570F6DB85067EE68BC901_H
#define IGNORESECTION_T9052979AAF08694DF92570F6DB85067EE68BC901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.IgnoreSection
struct  IgnoreSection_t9052979AAF08694DF92570F6DB85067EE68BC901  : public ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD
{
public:
	// System.String System.Configuration.IgnoreSection::xml
	String_t* ___xml_17;

public:
	inline static int32_t get_offset_of_xml_17() { return static_cast<int32_t>(offsetof(IgnoreSection_t9052979AAF08694DF92570F6DB85067EE68BC901, ___xml_17)); }
	inline String_t* get_xml_17() const { return ___xml_17; }
	inline String_t** get_address_of_xml_17() { return &___xml_17; }
	inline void set_xml_17(String_t* value)
	{
		___xml_17 = value;
		Il2CppCodeGenWriteBarrier((&___xml_17), value);
	}
};

struct IgnoreSection_t9052979AAF08694DF92570F6DB85067EE68BC901_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.IgnoreSection::properties
	ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * ___properties_18;

public:
	inline static int32_t get_offset_of_properties_18() { return static_cast<int32_t>(offsetof(IgnoreSection_t9052979AAF08694DF92570F6DB85067EE68BC901_StaticFields, ___properties_18)); }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * get_properties_18() const { return ___properties_18; }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 ** get_address_of_properties_18() { return &___properties_18; }
	inline void set_properties_18(ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * value)
	{
		___properties_18 = value;
		Il2CppCodeGenWriteBarrier((&___properties_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IGNORESECTION_T9052979AAF08694DF92570F6DB85067EE68BC901_H
#ifndef PROPERTYVALUEORIGIN_T5FFE6EAB47694726F7A3E9B0A5D9DF1C0035A11C_H
#define PROPERTYVALUEORIGIN_T5FFE6EAB47694726F7A3E9B0A5D9DF1C0035A11C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.PropertyValueOrigin
struct  PropertyValueOrigin_t5FFE6EAB47694726F7A3E9B0A5D9DF1C0035A11C 
{
public:
	// System.Int32 System.Configuration.PropertyValueOrigin::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PropertyValueOrigin_t5FFE6EAB47694726F7A3E9B0A5D9DF1C0035A11C, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYVALUEORIGIN_T5FFE6EAB47694726F7A3E9B0A5D9DF1C0035A11C_H
#ifndef PROTECTEDCONFIGURATIONSECTION_TD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0_H
#define PROTECTEDCONFIGURATIONSECTION_TD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProtectedConfigurationSection
struct  ProtectedConfigurationSection_tD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0  : public ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD
{
public:
	// System.Configuration.ProtectedConfigurationProviderCollection System.Configuration.ProtectedConfigurationSection::providers
	ProtectedConfigurationProviderCollection_t6F2A506BF598B7730852E9C57D207277687FBCDE * ___providers_20;

public:
	inline static int32_t get_offset_of_providers_20() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_tD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0, ___providers_20)); }
	inline ProtectedConfigurationProviderCollection_t6F2A506BF598B7730852E9C57D207277687FBCDE * get_providers_20() const { return ___providers_20; }
	inline ProtectedConfigurationProviderCollection_t6F2A506BF598B7730852E9C57D207277687FBCDE ** get_address_of_providers_20() { return &___providers_20; }
	inline void set_providers_20(ProtectedConfigurationProviderCollection_t6F2A506BF598B7730852E9C57D207277687FBCDE * value)
	{
		___providers_20 = value;
		Il2CppCodeGenWriteBarrier((&___providers_20), value);
	}
};

struct ProtectedConfigurationSection_tD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0_StaticFields
{
public:
	// System.Configuration.ConfigurationProperty System.Configuration.ProtectedConfigurationSection::defaultProviderProp
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * ___defaultProviderProp_17;
	// System.Configuration.ConfigurationProperty System.Configuration.ProtectedConfigurationSection::providersProp
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * ___providersProp_18;
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProtectedConfigurationSection::properties
	ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * ___properties_19;

public:
	inline static int32_t get_offset_of_defaultProviderProp_17() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_tD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0_StaticFields, ___defaultProviderProp_17)); }
	inline ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * get_defaultProviderProp_17() const { return ___defaultProviderProp_17; }
	inline ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 ** get_address_of_defaultProviderProp_17() { return &___defaultProviderProp_17; }
	inline void set_defaultProviderProp_17(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * value)
	{
		___defaultProviderProp_17 = value;
		Il2CppCodeGenWriteBarrier((&___defaultProviderProp_17), value);
	}

	inline static int32_t get_offset_of_providersProp_18() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_tD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0_StaticFields, ___providersProp_18)); }
	inline ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * get_providersProp_18() const { return ___providersProp_18; }
	inline ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 ** get_address_of_providersProp_18() { return &___providersProp_18; }
	inline void set_providersProp_18(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * value)
	{
		___providersProp_18 = value;
		Il2CppCodeGenWriteBarrier((&___providersProp_18), value);
	}

	inline static int32_t get_offset_of_properties_19() { return static_cast<int32_t>(offsetof(ProtectedConfigurationSection_tD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0_StaticFields, ___properties_19)); }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * get_properties_19() const { return ___properties_19; }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 ** get_address_of_properties_19() { return &___properties_19; }
	inline void set_properties_19(ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * value)
	{
		___properties_19 = value;
		Il2CppCodeGenWriteBarrier((&___properties_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROTECTEDCONFIGURATIONSECTION_TD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0_H
#ifndef PROVIDERSETTINGSCOLLECTION_T3955D5E0D34A2639D9BD61A7E7C4A375496B5CEB_H
#define PROVIDERSETTINGSCOLLECTION_T3955D5E0D34A2639D9BD61A7E7C4A375496B5CEB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ProviderSettingsCollection
struct  ProviderSettingsCollection_t3955D5E0D34A2639D9BD61A7E7C4A375496B5CEB  : public ConfigurationElementCollection_t5222278B0F197F4F1CA7C6F79A2447A1FA64AE3E
{
public:

public:
};

struct ProviderSettingsCollection_t3955D5E0D34A2639D9BD61A7E7C4A375496B5CEB_StaticFields
{
public:
	// System.Configuration.ConfigurationPropertyCollection System.Configuration.ProviderSettingsCollection::props
	ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * ___props_23;

public:
	inline static int32_t get_offset_of_props_23() { return static_cast<int32_t>(offsetof(ProviderSettingsCollection_t3955D5E0D34A2639D9BD61A7E7C4A375496B5CEB_StaticFields, ___props_23)); }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * get_props_23() const { return ___props_23; }
	inline ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 ** get_address_of_props_23() { return &___props_23; }
	inline void set_props_23(ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9 * value)
	{
		___props_23 = value;
		Il2CppCodeGenWriteBarrier((&___props_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERSETTINGSCOLLECTION_T3955D5E0D34A2639D9BD61A7E7C4A375496B5CEB_H
#ifndef DATETIMEKIND_T922DC40070B8AD59CB650E3FE023C4A58ED4568A_H
#define DATETIMEKIND_T922DC40070B8AD59CB650E3FE023C4A58ED4568A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t922DC40070B8AD59CB650E3FE023C4A58ED4568A 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t922DC40070B8AD59CB650E3FE023C4A58ED4568A, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T922DC40070B8AD59CB650E3FE023C4A58ED4568A_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_8)); }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * get_data_8() const { return ___data_8; }
	inline DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_tF588FE8D395F9A38FC7D222940F9B218441D21A9 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T_H
#ifndef MONOINTERNALNOTEATTRIBUTE_TFAF383C49F7DE1A5AB26457F9B9CFAC8B1B03728_H
#define MONOINTERNALNOTEATTRIBUTE_TFAF383C49F7DE1A5AB26457F9B9CFAC8B1B03728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MonoInternalNoteAttribute
struct  MonoInternalNoteAttribute_tFAF383C49F7DE1A5AB26457F9B9CFAC8B1B03728  : public MonoTODOAttribute_tC7E5E9F0CD72D820EAE8779D8457C93F3E5A8333
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOINTERNALNOTEATTRIBUTE_TFAF383C49F7DE1A5AB26457F9B9CFAC8B1B03728_H
#ifndef CIPHERMODE_TED50339A3462EF858D0805247E0EA327FF0EF758_H
#define CIPHERMODE_TED50339A3462EF858D0805247E0EA327FF0EF758_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.CipherMode
struct  CipherMode_tED50339A3462EF858D0805247E0EA327FF0EF758 
{
public:
	// System.Int32 System.Security.Cryptography.CipherMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CipherMode_tED50339A3462EF858D0805247E0EA327FF0EF758, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIPHERMODE_TED50339A3462EF858D0805247E0EA327FF0EF758_H
#ifndef PADDINGMODE_T9D48D98A07E2FCB856722BDB4C943E00FAAB3142_H
#define PADDINGMODE_T9D48D98A07E2FCB856722BDB4C943E00FAAB3142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.PaddingMode
struct  PaddingMode_t9D48D98A07E2FCB856722BDB4C943E00FAAB3142 
{
public:
	// System.Int32 System.Security.Cryptography.PaddingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PaddingMode_t9D48D98A07E2FCB856722BDB4C943E00FAAB3142, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PADDINGMODE_T9D48D98A07E2FCB856722BDB4C943E00FAAB3142_H
#ifndef X509CHAIN_T4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67_H
#define X509CHAIN_T4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Chain
struct  X509Chain_t4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67  : public RuntimeObject
{
public:
	// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::roots
	X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * ___roots_0;
	// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::certs
	X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * ___certs_1;
	// Mono.Security.X509.X509Certificate Mono.Security.X509.X509Chain::_root
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7 * ____root_2;
	// Mono.Security.X509.X509CertificateCollection Mono.Security.X509.X509Chain::_chain
	X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * ____chain_3;
	// Mono.Security.X509.X509ChainStatusFlags Mono.Security.X509.X509Chain::_status
	int32_t ____status_4;

public:
	inline static int32_t get_offset_of_roots_0() { return static_cast<int32_t>(offsetof(X509Chain_t4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67, ___roots_0)); }
	inline X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * get_roots_0() const { return ___roots_0; }
	inline X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 ** get_address_of_roots_0() { return &___roots_0; }
	inline void set_roots_0(X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * value)
	{
		___roots_0 = value;
		Il2CppCodeGenWriteBarrier((&___roots_0), value);
	}

	inline static int32_t get_offset_of_certs_1() { return static_cast<int32_t>(offsetof(X509Chain_t4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67, ___certs_1)); }
	inline X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * get_certs_1() const { return ___certs_1; }
	inline X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 ** get_address_of_certs_1() { return &___certs_1; }
	inline void set_certs_1(X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * value)
	{
		___certs_1 = value;
		Il2CppCodeGenWriteBarrier((&___certs_1), value);
	}

	inline static int32_t get_offset_of__root_2() { return static_cast<int32_t>(offsetof(X509Chain_t4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67, ____root_2)); }
	inline X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7 * get__root_2() const { return ____root_2; }
	inline X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7 ** get_address_of__root_2() { return &____root_2; }
	inline void set__root_2(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7 * value)
	{
		____root_2 = value;
		Il2CppCodeGenWriteBarrier((&____root_2), value);
	}

	inline static int32_t get_offset_of__chain_3() { return static_cast<int32_t>(offsetof(X509Chain_t4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67, ____chain_3)); }
	inline X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * get__chain_3() const { return ____chain_3; }
	inline X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 ** get_address_of__chain_3() { return &____chain_3; }
	inline void set__chain_3(X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4 * value)
	{
		____chain_3 = value;
		Il2CppCodeGenWriteBarrier((&____chain_3), value);
	}

	inline static int32_t get_offset_of__status_4() { return static_cast<int32_t>(offsetof(X509Chain_t4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67, ____status_4)); }
	inline int32_t get__status_4() const { return ____status_4; }
	inline int32_t* get_address_of__status_4() { return &____status_4; }
	inline void set__status_4(int32_t value)
	{
		____status_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CHAIN_T4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67_H
#ifndef CONFIGURATIONLOCKCOLLECTION_T2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C_H
#define CONFIGURATIONLOCKCOLLECTION_T2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationLockCollection
struct  ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C  : public RuntimeObject
{
public:
	// System.Collections.ArrayList System.Configuration.ConfigurationLockCollection::names
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___names_0;
	// System.Configuration.ConfigurationElement System.Configuration.ConfigurationLockCollection::element
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 * ___element_1;
	// System.Configuration.ConfigurationLockType System.Configuration.ConfigurationLockCollection::lockType
	int32_t ___lockType_2;
	// System.Boolean System.Configuration.ConfigurationLockCollection::is_modified
	bool ___is_modified_3;
	// System.Collections.Hashtable System.Configuration.ConfigurationLockCollection::valid_name_hash
	Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * ___valid_name_hash_4;
	// System.String System.Configuration.ConfigurationLockCollection::valid_names
	String_t* ___valid_names_5;

public:
	inline static int32_t get_offset_of_names_0() { return static_cast<int32_t>(offsetof(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C, ___names_0)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_names_0() const { return ___names_0; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_names_0() { return &___names_0; }
	inline void set_names_0(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___names_0 = value;
		Il2CppCodeGenWriteBarrier((&___names_0), value);
	}

	inline static int32_t get_offset_of_element_1() { return static_cast<int32_t>(offsetof(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C, ___element_1)); }
	inline ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 * get_element_1() const { return ___element_1; }
	inline ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 ** get_address_of_element_1() { return &___element_1; }
	inline void set_element_1(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 * value)
	{
		___element_1 = value;
		Il2CppCodeGenWriteBarrier((&___element_1), value);
	}

	inline static int32_t get_offset_of_lockType_2() { return static_cast<int32_t>(offsetof(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C, ___lockType_2)); }
	inline int32_t get_lockType_2() const { return ___lockType_2; }
	inline int32_t* get_address_of_lockType_2() { return &___lockType_2; }
	inline void set_lockType_2(int32_t value)
	{
		___lockType_2 = value;
	}

	inline static int32_t get_offset_of_is_modified_3() { return static_cast<int32_t>(offsetof(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C, ___is_modified_3)); }
	inline bool get_is_modified_3() const { return ___is_modified_3; }
	inline bool* get_address_of_is_modified_3() { return &___is_modified_3; }
	inline void set_is_modified_3(bool value)
	{
		___is_modified_3 = value;
	}

	inline static int32_t get_offset_of_valid_name_hash_4() { return static_cast<int32_t>(offsetof(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C, ___valid_name_hash_4)); }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * get_valid_name_hash_4() const { return ___valid_name_hash_4; }
	inline Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 ** get_address_of_valid_name_hash_4() { return &___valid_name_hash_4; }
	inline void set_valid_name_hash_4(Hashtable_tA746260C9064A8C1FC071FF85C11C8EBAEB51B82 * value)
	{
		___valid_name_hash_4 = value;
		Il2CppCodeGenWriteBarrier((&___valid_name_hash_4), value);
	}

	inline static int32_t get_offset_of_valid_names_5() { return static_cast<int32_t>(offsetof(ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C, ___valid_names_5)); }
	inline String_t* get_valid_names_5() const { return ___valid_names_5; }
	inline String_t** get_address_of_valid_names_5() { return &___valid_names_5; }
	inline void set_valid_names_5(String_t* value)
	{
		___valid_names_5 = value;
		Il2CppCodeGenWriteBarrier((&___valid_names_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONLOCKCOLLECTION_T2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C_H
#ifndef CONFIGURATIONPROPERTY_T4AE3E848FDE59333B2B83BF7BBD40F0A66837400_H
#define CONFIGURATIONPROPERTY_T4AE3E848FDE59333B2B83BF7BBD40F0A66837400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationProperty
struct  ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400  : public RuntimeObject
{
public:
	// System.String System.Configuration.ConfigurationProperty::name
	String_t* ___name_1;
	// System.Type System.Configuration.ConfigurationProperty::type
	Type_t * ___type_2;
	// System.Object System.Configuration.ConfigurationProperty::default_value
	RuntimeObject * ___default_value_3;
	// System.ComponentModel.TypeConverter System.Configuration.ConfigurationProperty::converter
	TypeConverter_t5801C9F7100E1D849000ED0914E01E4CB2541B71 * ___converter_4;
	// System.Configuration.ConfigurationValidatorBase System.Configuration.ConfigurationProperty::validation
	ConfigurationValidatorBase_tD691540223153E9DEA98BDBD778BCB12A56248ED * ___validation_5;
	// System.Configuration.ConfigurationPropertyOptions System.Configuration.ConfigurationProperty::flags
	int32_t ___flags_6;
	// System.String System.Configuration.ConfigurationProperty::description
	String_t* ___description_7;
	// System.Configuration.ConfigurationCollectionAttribute System.Configuration.ConfigurationProperty::collectionAttribute
	ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318 * ___collectionAttribute_8;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400, ___type_2)); }
	inline Type_t * get_type_2() const { return ___type_2; }
	inline Type_t ** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(Type_t * value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier((&___type_2), value);
	}

	inline static int32_t get_offset_of_default_value_3() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400, ___default_value_3)); }
	inline RuntimeObject * get_default_value_3() const { return ___default_value_3; }
	inline RuntimeObject ** get_address_of_default_value_3() { return &___default_value_3; }
	inline void set_default_value_3(RuntimeObject * value)
	{
		___default_value_3 = value;
		Il2CppCodeGenWriteBarrier((&___default_value_3), value);
	}

	inline static int32_t get_offset_of_converter_4() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400, ___converter_4)); }
	inline TypeConverter_t5801C9F7100E1D849000ED0914E01E4CB2541B71 * get_converter_4() const { return ___converter_4; }
	inline TypeConverter_t5801C9F7100E1D849000ED0914E01E4CB2541B71 ** get_address_of_converter_4() { return &___converter_4; }
	inline void set_converter_4(TypeConverter_t5801C9F7100E1D849000ED0914E01E4CB2541B71 * value)
	{
		___converter_4 = value;
		Il2CppCodeGenWriteBarrier((&___converter_4), value);
	}

	inline static int32_t get_offset_of_validation_5() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400, ___validation_5)); }
	inline ConfigurationValidatorBase_tD691540223153E9DEA98BDBD778BCB12A56248ED * get_validation_5() const { return ___validation_5; }
	inline ConfigurationValidatorBase_tD691540223153E9DEA98BDBD778BCB12A56248ED ** get_address_of_validation_5() { return &___validation_5; }
	inline void set_validation_5(ConfigurationValidatorBase_tD691540223153E9DEA98BDBD778BCB12A56248ED * value)
	{
		___validation_5 = value;
		Il2CppCodeGenWriteBarrier((&___validation_5), value);
	}

	inline static int32_t get_offset_of_flags_6() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400, ___flags_6)); }
	inline int32_t get_flags_6() const { return ___flags_6; }
	inline int32_t* get_address_of_flags_6() { return &___flags_6; }
	inline void set_flags_6(int32_t value)
	{
		___flags_6 = value;
	}

	inline static int32_t get_offset_of_description_7() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400, ___description_7)); }
	inline String_t* get_description_7() const { return ___description_7; }
	inline String_t** get_address_of_description_7() { return &___description_7; }
	inline void set_description_7(String_t* value)
	{
		___description_7 = value;
		Il2CppCodeGenWriteBarrier((&___description_7), value);
	}

	inline static int32_t get_offset_of_collectionAttribute_8() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400, ___collectionAttribute_8)); }
	inline ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318 * get_collectionAttribute_8() const { return ___collectionAttribute_8; }
	inline ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318 ** get_address_of_collectionAttribute_8() { return &___collectionAttribute_8; }
	inline void set_collectionAttribute_8(ConfigurationCollectionAttribute_t306510D81BCFFB7163142679D521E72022976318 * value)
	{
		___collectionAttribute_8 = value;
		Il2CppCodeGenWriteBarrier((&___collectionAttribute_8), value);
	}
};

struct ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400_StaticFields
{
public:
	// System.Object System.Configuration.ConfigurationProperty::NoDefaultValue
	RuntimeObject * ___NoDefaultValue_0;

public:
	inline static int32_t get_offset_of_NoDefaultValue_0() { return static_cast<int32_t>(offsetof(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400_StaticFields, ___NoDefaultValue_0)); }
	inline RuntimeObject * get_NoDefaultValue_0() const { return ___NoDefaultValue_0; }
	inline RuntimeObject ** get_address_of_NoDefaultValue_0() { return &___NoDefaultValue_0; }
	inline void set_NoDefaultValue_0(RuntimeObject * value)
	{
		___NoDefaultValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___NoDefaultValue_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONPROPERTY_T4AE3E848FDE59333B2B83BF7BBD40F0A66837400_H
#ifndef CONFIGURATIONPROPERTYATTRIBUTE_TFBC82E620CACDE37B530C525FEC7653F77CE0101_H
#define CONFIGURATIONPROPERTYATTRIBUTE_TFBC82E620CACDE37B530C525FEC7653F77CE0101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ConfigurationPropertyAttribute
struct  ConfigurationPropertyAttribute_tFBC82E620CACDE37B530C525FEC7653F77CE0101  : public Attribute_t60F25EB48D5935E4C6C2BAF7F90F57A43528E469
{
public:
	// System.String System.Configuration.ConfigurationPropertyAttribute::name
	String_t* ___name_0;
	// System.Object System.Configuration.ConfigurationPropertyAttribute::default_value
	RuntimeObject * ___default_value_1;
	// System.Configuration.ConfigurationPropertyOptions System.Configuration.ConfigurationPropertyAttribute::flags
	int32_t ___flags_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ConfigurationPropertyAttribute_tFBC82E620CACDE37B530C525FEC7653F77CE0101, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_default_value_1() { return static_cast<int32_t>(offsetof(ConfigurationPropertyAttribute_tFBC82E620CACDE37B530C525FEC7653F77CE0101, ___default_value_1)); }
	inline RuntimeObject * get_default_value_1() const { return ___default_value_1; }
	inline RuntimeObject ** get_address_of_default_value_1() { return &___default_value_1; }
	inline void set_default_value_1(RuntimeObject * value)
	{
		___default_value_1 = value;
		Il2CppCodeGenWriteBarrier((&___default_value_1), value);
	}

	inline static int32_t get_offset_of_flags_2() { return static_cast<int32_t>(offsetof(ConfigurationPropertyAttribute_tFBC82E620CACDE37B530C525FEC7653F77CE0101, ___flags_2)); }
	inline int32_t get_flags_2() const { return ___flags_2; }
	inline int32_t* get_address_of_flags_2() { return &___flags_2; }
	inline void set_flags_2(int32_t value)
	{
		___flags_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONPROPERTYATTRIBUTE_TFBC82E620CACDE37B530C525FEC7653F77CE0101_H
#ifndef EXECONFIGURATIONHOST_T41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B_H
#define EXECONFIGURATIONHOST_T41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.ExeConfigurationHost
struct  ExeConfigurationHost_t41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B  : public InternalConfigurationHost_t89A94135BBC91E89941129D05624642760575086
{
public:
	// System.Configuration.ExeConfigurationFileMap System.Configuration.ExeConfigurationHost::map
	ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB * ___map_0;
	// System.Configuration.ConfigurationUserLevel System.Configuration.ExeConfigurationHost::level
	int32_t ___level_1;

public:
	inline static int32_t get_offset_of_map_0() { return static_cast<int32_t>(offsetof(ExeConfigurationHost_t41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B, ___map_0)); }
	inline ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB * get_map_0() const { return ___map_0; }
	inline ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB ** get_address_of_map_0() { return &___map_0; }
	inline void set_map_0(ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB * value)
	{
		___map_0 = value;
		Il2CppCodeGenWriteBarrier((&___map_0), value);
	}

	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(ExeConfigurationHost_t41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B, ___level_1)); }
	inline int32_t get_level_1() const { return ___level_1; }
	inline int32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(int32_t value)
	{
		___level_1 = value;
	}
};

struct ExeConfigurationHost_t41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Configuration.ExeConfigurationHost::<>f__switchU24map0
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map0_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_2() { return static_cast<int32_t>(offsetof(ExeConfigurationHost_t41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B_StaticFields, ___U3CU3Ef__switchU24map0_2)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map0_2() const { return ___U3CU3Ef__switchU24map0_2; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map0_2() { return &___U3CU3Ef__switchU24map0_2; }
	inline void set_U3CU3Ef__switchU24map0_2(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECONFIGURATIONHOST_T41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B_H
#ifndef PROPERTYINFORMATION_T855727B59A0F1EFADF230F0A209DAF108D666BDF_H
#define PROPERTYINFORMATION_T855727B59A0F1EFADF230F0A209DAF108D666BDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.PropertyInformation
struct  PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF  : public RuntimeObject
{
public:
	// System.Boolean System.Configuration.PropertyInformation::isModified
	bool ___isModified_0;
	// System.Object System.Configuration.PropertyInformation::val
	RuntimeObject * ___val_1;
	// System.Configuration.PropertyValueOrigin System.Configuration.PropertyInformation::origin
	int32_t ___origin_2;
	// System.Configuration.ConfigurationElement System.Configuration.PropertyInformation::owner
	ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 * ___owner_3;
	// System.Configuration.ConfigurationProperty System.Configuration.PropertyInformation::property
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * ___property_4;

public:
	inline static int32_t get_offset_of_isModified_0() { return static_cast<int32_t>(offsetof(PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF, ___isModified_0)); }
	inline bool get_isModified_0() const { return ___isModified_0; }
	inline bool* get_address_of_isModified_0() { return &___isModified_0; }
	inline void set_isModified_0(bool value)
	{
		___isModified_0 = value;
	}

	inline static int32_t get_offset_of_val_1() { return static_cast<int32_t>(offsetof(PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF, ___val_1)); }
	inline RuntimeObject * get_val_1() const { return ___val_1; }
	inline RuntimeObject ** get_address_of_val_1() { return &___val_1; }
	inline void set_val_1(RuntimeObject * value)
	{
		___val_1 = value;
		Il2CppCodeGenWriteBarrier((&___val_1), value);
	}

	inline static int32_t get_offset_of_origin_2() { return static_cast<int32_t>(offsetof(PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF, ___origin_2)); }
	inline int32_t get_origin_2() const { return ___origin_2; }
	inline int32_t* get_address_of_origin_2() { return &___origin_2; }
	inline void set_origin_2(int32_t value)
	{
		___origin_2 = value;
	}

	inline static int32_t get_offset_of_owner_3() { return static_cast<int32_t>(offsetof(PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF, ___owner_3)); }
	inline ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 * get_owner_3() const { return ___owner_3; }
	inline ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 ** get_address_of_owner_3() { return &___owner_3; }
	inline void set_owner_3(ConfigurationElement_t0102651B4A826E7C1A21DB9AF0C7565C66A53D63 * value)
	{
		___owner_3 = value;
		Il2CppCodeGenWriteBarrier((&___owner_3), value);
	}

	inline static int32_t get_offset_of_property_4() { return static_cast<int32_t>(offsetof(PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF, ___property_4)); }
	inline ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * get_property_4() const { return ___property_4; }
	inline ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 ** get_address_of_property_4() { return &___property_4; }
	inline void set_property_4(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400 * value)
	{
		___property_4 = value;
		Il2CppCodeGenWriteBarrier((&___property_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFORMATION_T855727B59A0F1EFADF230F0A209DAF108D666BDF_H
#ifndef SECTIONINFO_T4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0_H
#define SECTIONINFO_T4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.SectionInfo
struct  SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0  : public ConfigInfo_tCCB111FBFC8E13282C1FACD3B69A2709D96EA698
{
public:
	// System.Boolean System.Configuration.SectionInfo::allowLocation
	bool ___allowLocation_6;
	// System.Boolean System.Configuration.SectionInfo::requirePermission
	bool ___requirePermission_7;
	// System.Boolean System.Configuration.SectionInfo::restartOnExternalChanges
	bool ___restartOnExternalChanges_8;
	// System.Configuration.ConfigurationAllowDefinition System.Configuration.SectionInfo::allowDefinition
	int32_t ___allowDefinition_9;
	// System.Configuration.ConfigurationAllowExeDefinition System.Configuration.SectionInfo::allowExeDefinition
	int32_t ___allowExeDefinition_10;

public:
	inline static int32_t get_offset_of_allowLocation_6() { return static_cast<int32_t>(offsetof(SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0, ___allowLocation_6)); }
	inline bool get_allowLocation_6() const { return ___allowLocation_6; }
	inline bool* get_address_of_allowLocation_6() { return &___allowLocation_6; }
	inline void set_allowLocation_6(bool value)
	{
		___allowLocation_6 = value;
	}

	inline static int32_t get_offset_of_requirePermission_7() { return static_cast<int32_t>(offsetof(SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0, ___requirePermission_7)); }
	inline bool get_requirePermission_7() const { return ___requirePermission_7; }
	inline bool* get_address_of_requirePermission_7() { return &___requirePermission_7; }
	inline void set_requirePermission_7(bool value)
	{
		___requirePermission_7 = value;
	}

	inline static int32_t get_offset_of_restartOnExternalChanges_8() { return static_cast<int32_t>(offsetof(SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0, ___restartOnExternalChanges_8)); }
	inline bool get_restartOnExternalChanges_8() const { return ___restartOnExternalChanges_8; }
	inline bool* get_address_of_restartOnExternalChanges_8() { return &___restartOnExternalChanges_8; }
	inline void set_restartOnExternalChanges_8(bool value)
	{
		___restartOnExternalChanges_8 = value;
	}

	inline static int32_t get_offset_of_allowDefinition_9() { return static_cast<int32_t>(offsetof(SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0, ___allowDefinition_9)); }
	inline int32_t get_allowDefinition_9() const { return ___allowDefinition_9; }
	inline int32_t* get_address_of_allowDefinition_9() { return &___allowDefinition_9; }
	inline void set_allowDefinition_9(int32_t value)
	{
		___allowDefinition_9 = value;
	}

	inline static int32_t get_offset_of_allowExeDefinition_10() { return static_cast<int32_t>(offsetof(SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0, ___allowExeDefinition_10)); }
	inline int32_t get_allowExeDefinition_10() const { return ___allowExeDefinition_10; }
	inline int32_t* get_address_of_allowExeDefinition_10() { return &___allowExeDefinition_10; }
	inline void set_allowExeDefinition_10(int32_t value)
	{
		___allowExeDefinition_10 = value;
	}
};

struct SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Configuration.SectionInfo::<>f__switchU24map1
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map1_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1_11() { return static_cast<int32_t>(offsetof(SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0_StaticFields, ___U3CU3Ef__switchU24map1_11)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map1_11() const { return ___U3CU3Ef__switchU24map1_11; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map1_11() { return &___U3CU3Ef__switchU24map1_11; }
	inline void set_U3CU3Ef__switchU24map1_11(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map1_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTIONINFO_T4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0_H
#ifndef SECTIONINFORMATION_TB3EAD382083CF190583FA144FAFC95BA6E0AFC9A_H
#define SECTIONINFORMATION_TB3EAD382083CF190583FA144FAFC95BA6E0AFC9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Configuration.SectionInformation
struct  SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A  : public RuntimeObject
{
public:
	// System.Configuration.ConfigurationSection System.Configuration.SectionInformation::parent
	ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD * ___parent_0;
	// System.Configuration.ConfigurationAllowDefinition System.Configuration.SectionInformation::allow_definition
	int32_t ___allow_definition_1;
	// System.Configuration.ConfigurationAllowExeDefinition System.Configuration.SectionInformation::allow_exe_definition
	int32_t ___allow_exe_definition_2;
	// System.Boolean System.Configuration.SectionInformation::allow_location
	bool ___allow_location_3;
	// System.Boolean System.Configuration.SectionInformation::allow_override
	bool ___allow_override_4;
	// System.Boolean System.Configuration.SectionInformation::inherit_on_child_apps
	bool ___inherit_on_child_apps_5;
	// System.Boolean System.Configuration.SectionInformation::restart_on_external_changes
	bool ___restart_on_external_changes_6;
	// System.Boolean System.Configuration.SectionInformation::require_permission
	bool ___require_permission_7;
	// System.String System.Configuration.SectionInformation::config_source
	String_t* ___config_source_8;
	// System.String System.Configuration.SectionInformation::name
	String_t* ___name_9;
	// System.String System.Configuration.SectionInformation::raw_xml
	String_t* ___raw_xml_10;
	// System.Configuration.ProtectedConfigurationProvider System.Configuration.SectionInformation::protection_provider
	ProtectedConfigurationProvider_t6D1CFE3639AFF7A42A854B97BD575CFE05ED68A0 * ___protection_provider_11;
	// System.String System.Configuration.SectionInformation::<ConfigFilePath>k__BackingField
	String_t* ___U3CConfigFilePathU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___parent_0)); }
	inline ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD * get_parent_0() const { return ___parent_0; }
	inline ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_allow_definition_1() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___allow_definition_1)); }
	inline int32_t get_allow_definition_1() const { return ___allow_definition_1; }
	inline int32_t* get_address_of_allow_definition_1() { return &___allow_definition_1; }
	inline void set_allow_definition_1(int32_t value)
	{
		___allow_definition_1 = value;
	}

	inline static int32_t get_offset_of_allow_exe_definition_2() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___allow_exe_definition_2)); }
	inline int32_t get_allow_exe_definition_2() const { return ___allow_exe_definition_2; }
	inline int32_t* get_address_of_allow_exe_definition_2() { return &___allow_exe_definition_2; }
	inline void set_allow_exe_definition_2(int32_t value)
	{
		___allow_exe_definition_2 = value;
	}

	inline static int32_t get_offset_of_allow_location_3() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___allow_location_3)); }
	inline bool get_allow_location_3() const { return ___allow_location_3; }
	inline bool* get_address_of_allow_location_3() { return &___allow_location_3; }
	inline void set_allow_location_3(bool value)
	{
		___allow_location_3 = value;
	}

	inline static int32_t get_offset_of_allow_override_4() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___allow_override_4)); }
	inline bool get_allow_override_4() const { return ___allow_override_4; }
	inline bool* get_address_of_allow_override_4() { return &___allow_override_4; }
	inline void set_allow_override_4(bool value)
	{
		___allow_override_4 = value;
	}

	inline static int32_t get_offset_of_inherit_on_child_apps_5() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___inherit_on_child_apps_5)); }
	inline bool get_inherit_on_child_apps_5() const { return ___inherit_on_child_apps_5; }
	inline bool* get_address_of_inherit_on_child_apps_5() { return &___inherit_on_child_apps_5; }
	inline void set_inherit_on_child_apps_5(bool value)
	{
		___inherit_on_child_apps_5 = value;
	}

	inline static int32_t get_offset_of_restart_on_external_changes_6() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___restart_on_external_changes_6)); }
	inline bool get_restart_on_external_changes_6() const { return ___restart_on_external_changes_6; }
	inline bool* get_address_of_restart_on_external_changes_6() { return &___restart_on_external_changes_6; }
	inline void set_restart_on_external_changes_6(bool value)
	{
		___restart_on_external_changes_6 = value;
	}

	inline static int32_t get_offset_of_require_permission_7() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___require_permission_7)); }
	inline bool get_require_permission_7() const { return ___require_permission_7; }
	inline bool* get_address_of_require_permission_7() { return &___require_permission_7; }
	inline void set_require_permission_7(bool value)
	{
		___require_permission_7 = value;
	}

	inline static int32_t get_offset_of_config_source_8() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___config_source_8)); }
	inline String_t* get_config_source_8() const { return ___config_source_8; }
	inline String_t** get_address_of_config_source_8() { return &___config_source_8; }
	inline void set_config_source_8(String_t* value)
	{
		___config_source_8 = value;
		Il2CppCodeGenWriteBarrier((&___config_source_8), value);
	}

	inline static int32_t get_offset_of_name_9() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___name_9)); }
	inline String_t* get_name_9() const { return ___name_9; }
	inline String_t** get_address_of_name_9() { return &___name_9; }
	inline void set_name_9(String_t* value)
	{
		___name_9 = value;
		Il2CppCodeGenWriteBarrier((&___name_9), value);
	}

	inline static int32_t get_offset_of_raw_xml_10() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___raw_xml_10)); }
	inline String_t* get_raw_xml_10() const { return ___raw_xml_10; }
	inline String_t** get_address_of_raw_xml_10() { return &___raw_xml_10; }
	inline void set_raw_xml_10(String_t* value)
	{
		___raw_xml_10 = value;
		Il2CppCodeGenWriteBarrier((&___raw_xml_10), value);
	}

	inline static int32_t get_offset_of_protection_provider_11() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___protection_provider_11)); }
	inline ProtectedConfigurationProvider_t6D1CFE3639AFF7A42A854B97BD575CFE05ED68A0 * get_protection_provider_11() const { return ___protection_provider_11; }
	inline ProtectedConfigurationProvider_t6D1CFE3639AFF7A42A854B97BD575CFE05ED68A0 ** get_address_of_protection_provider_11() { return &___protection_provider_11; }
	inline void set_protection_provider_11(ProtectedConfigurationProvider_t6D1CFE3639AFF7A42A854B97BD575CFE05ED68A0 * value)
	{
		___protection_provider_11 = value;
		Il2CppCodeGenWriteBarrier((&___protection_provider_11), value);
	}

	inline static int32_t get_offset_of_U3CConfigFilePathU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A, ___U3CConfigFilePathU3Ek__BackingField_12)); }
	inline String_t* get_U3CConfigFilePathU3Ek__BackingField_12() const { return ___U3CConfigFilePathU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CConfigFilePathU3Ek__BackingField_12() { return &___U3CConfigFilePathU3Ek__BackingField_12; }
	inline void set_U3CConfigFilePathU3Ek__BackingField_12(String_t* value)
	{
		___U3CConfigFilePathU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConfigFilePathU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SECTIONINFORMATION_TB3EAD382083CF190583FA144FAFC95BA6E0AFC9A_H
#ifndef DATETIME_T816BBD9125EA02B42B87CB07643FC5F803391DA2_H
#define DATETIME_T816BBD9125EA02B42B87CB07643FC5F803391DA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  ___ticks_10;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_11;

public:
	inline static int32_t get_offset_of_ticks_10() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2, ___ticks_10)); }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  get_ticks_10() const { return ___ticks_10; }
	inline TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374 * get_address_of_ticks_10() { return &___ticks_10; }
	inline void set_ticks_10(TimeSpan_t26DE4373EA745C072A5FBDEAD8373348955FC374  value)
	{
		___ticks_10 = value;
	}

	inline static int32_t get_offset_of_kind_11() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2, ___kind_11)); }
	inline int32_t get_kind_11() const { return ___kind_11; }
	inline int32_t* get_address_of_kind_11() { return &___kind_11; }
	inline void set_kind_11(int32_t value)
	{
		___kind_11 = value;
	}
};

struct DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___MaxValue_12;
	// System.DateTime System.DateTime::MinValue
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___MinValue_13;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseTimeFormats_14;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseYearDayMonthFormats_15;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseYearMonthDayFormats_16;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseDayMonthYearFormats_17;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___ParseMonthDayYearFormats_18;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___MonthDayShortFormats_19;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* ___DayMonthShortFormats_20;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___daysmonth_21;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* ___daysmonthleap_22;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_23;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_24;

public:
	inline static int32_t get_offset_of_MaxValue_12() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___MaxValue_12)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_MaxValue_12() const { return ___MaxValue_12; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_MaxValue_12() { return &___MaxValue_12; }
	inline void set_MaxValue_12(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___MaxValue_12 = value;
	}

	inline static int32_t get_offset_of_MinValue_13() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___MinValue_13)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_MinValue_13() const { return ___MinValue_13; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_MinValue_13() { return &___MinValue_13; }
	inline void set_MinValue_13(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___MinValue_13 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_14() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseTimeFormats_14)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseTimeFormats_14() const { return ___ParseTimeFormats_14; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseTimeFormats_14() { return &___ParseTimeFormats_14; }
	inline void set_ParseTimeFormats_14(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseTimeFormats_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_14), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_15() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseYearDayMonthFormats_15)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseYearDayMonthFormats_15() const { return ___ParseYearDayMonthFormats_15; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseYearDayMonthFormats_15() { return &___ParseYearDayMonthFormats_15; }
	inline void set_ParseYearDayMonthFormats_15(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseYearDayMonthFormats_15 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_15), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_16() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseYearMonthDayFormats_16)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseYearMonthDayFormats_16() const { return ___ParseYearMonthDayFormats_16; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseYearMonthDayFormats_16() { return &___ParseYearMonthDayFormats_16; }
	inline void set_ParseYearMonthDayFormats_16(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseYearMonthDayFormats_16 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_16), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_17() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseDayMonthYearFormats_17)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseDayMonthYearFormats_17() const { return ___ParseDayMonthYearFormats_17; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseDayMonthYearFormats_17() { return &___ParseDayMonthYearFormats_17; }
	inline void set_ParseDayMonthYearFormats_17(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseDayMonthYearFormats_17 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_17), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_18() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___ParseMonthDayYearFormats_18)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_ParseMonthDayYearFormats_18() const { return ___ParseMonthDayYearFormats_18; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_ParseMonthDayYearFormats_18() { return &___ParseMonthDayYearFormats_18; }
	inline void set_ParseMonthDayYearFormats_18(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___ParseMonthDayYearFormats_18 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_18), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_19() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___MonthDayShortFormats_19)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_MonthDayShortFormats_19() const { return ___MonthDayShortFormats_19; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_MonthDayShortFormats_19() { return &___MonthDayShortFormats_19; }
	inline void set_MonthDayShortFormats_19(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___MonthDayShortFormats_19 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_19), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_20() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___DayMonthShortFormats_20)); }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* get_DayMonthShortFormats_20() const { return ___DayMonthShortFormats_20; }
	inline StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B** get_address_of_DayMonthShortFormats_20() { return &___DayMonthShortFormats_20; }
	inline void set_DayMonthShortFormats_20(StringU5BU5D_t632E9CB8D244841312F333CBF60404AFA46E0B3B* value)
	{
		___DayMonthShortFormats_20 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_20), value);
	}

	inline static int32_t get_offset_of_daysmonth_21() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___daysmonth_21)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_daysmonth_21() const { return ___daysmonth_21; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_daysmonth_21() { return &___daysmonth_21; }
	inline void set_daysmonth_21(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___daysmonth_21 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_21), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_22() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___daysmonthleap_22)); }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* get_daysmonthleap_22() const { return ___daysmonthleap_22; }
	inline Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074** get_address_of_daysmonthleap_22() { return &___daysmonthleap_22; }
	inline void set_daysmonthleap_22(Int32U5BU5D_t20AF77B812DFA3168922AE8F35FB9FD20D7EA074* value)
	{
		___daysmonthleap_22 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_22), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_23() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___to_local_time_span_object_23)); }
	inline RuntimeObject * get_to_local_time_span_object_23() const { return ___to_local_time_span_object_23; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_23() { return &___to_local_time_span_object_23; }
	inline void set_to_local_time_span_object_23(RuntimeObject * value)
	{
		___to_local_time_span_object_23 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_23), value);
	}

	inline static int32_t get_offset_of_last_now_24() { return static_cast<int32_t>(offsetof(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2_StaticFields, ___last_now_24)); }
	inline int64_t get_last_now_24() const { return ___last_now_24; }
	inline int64_t* get_address_of_last_now_24() { return &___last_now_24; }
	inline void set_last_now_24(int64_t value)
	{
		___last_now_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T816BBD9125EA02B42B87CB07643FC5F803391DA2_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef SYMMETRICALGORITHM_T8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924_H
#define SYMMETRICALGORITHM_T8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Cryptography.SymmetricAlgorithm
struct  SymmetricAlgorithm_t8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924  : public RuntimeObject
{
public:
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::BlockSizeValue
	int32_t ___BlockSizeValue_0;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::IVValue
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___IVValue_1;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::KeySizeValue
	int32_t ___KeySizeValue_2;
	// System.Byte[] System.Security.Cryptography.SymmetricAlgorithm::KeyValue
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___KeyValue_3;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalBlockSizesValue
	KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* ___LegalBlockSizesValue_4;
	// System.Security.Cryptography.KeySizes[] System.Security.Cryptography.SymmetricAlgorithm::LegalKeySizesValue
	KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* ___LegalKeySizesValue_5;
	// System.Int32 System.Security.Cryptography.SymmetricAlgorithm::FeedbackSizeValue
	int32_t ___FeedbackSizeValue_6;
	// System.Security.Cryptography.CipherMode System.Security.Cryptography.SymmetricAlgorithm::ModeValue
	int32_t ___ModeValue_7;
	// System.Security.Cryptography.PaddingMode System.Security.Cryptography.SymmetricAlgorithm::PaddingValue
	int32_t ___PaddingValue_8;
	// System.Boolean System.Security.Cryptography.SymmetricAlgorithm::m_disposed
	bool ___m_disposed_9;

public:
	inline static int32_t get_offset_of_BlockSizeValue_0() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924, ___BlockSizeValue_0)); }
	inline int32_t get_BlockSizeValue_0() const { return ___BlockSizeValue_0; }
	inline int32_t* get_address_of_BlockSizeValue_0() { return &___BlockSizeValue_0; }
	inline void set_BlockSizeValue_0(int32_t value)
	{
		___BlockSizeValue_0 = value;
	}

	inline static int32_t get_offset_of_IVValue_1() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924, ___IVValue_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_IVValue_1() const { return ___IVValue_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_IVValue_1() { return &___IVValue_1; }
	inline void set_IVValue_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___IVValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___IVValue_1), value);
	}

	inline static int32_t get_offset_of_KeySizeValue_2() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924, ___KeySizeValue_2)); }
	inline int32_t get_KeySizeValue_2() const { return ___KeySizeValue_2; }
	inline int32_t* get_address_of_KeySizeValue_2() { return &___KeySizeValue_2; }
	inline void set_KeySizeValue_2(int32_t value)
	{
		___KeySizeValue_2 = value;
	}

	inline static int32_t get_offset_of_KeyValue_3() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924, ___KeyValue_3)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_KeyValue_3() const { return ___KeyValue_3; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_KeyValue_3() { return &___KeyValue_3; }
	inline void set_KeyValue_3(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___KeyValue_3 = value;
		Il2CppCodeGenWriteBarrier((&___KeyValue_3), value);
	}

	inline static int32_t get_offset_of_LegalBlockSizesValue_4() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924, ___LegalBlockSizesValue_4)); }
	inline KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* get_LegalBlockSizesValue_4() const { return ___LegalBlockSizesValue_4; }
	inline KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C** get_address_of_LegalBlockSizesValue_4() { return &___LegalBlockSizesValue_4; }
	inline void set_LegalBlockSizesValue_4(KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* value)
	{
		___LegalBlockSizesValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___LegalBlockSizesValue_4), value);
	}

	inline static int32_t get_offset_of_LegalKeySizesValue_5() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924, ___LegalKeySizesValue_5)); }
	inline KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* get_LegalKeySizesValue_5() const { return ___LegalKeySizesValue_5; }
	inline KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C** get_address_of_LegalKeySizesValue_5() { return &___LegalKeySizesValue_5; }
	inline void set_LegalKeySizesValue_5(KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* value)
	{
		___LegalKeySizesValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___LegalKeySizesValue_5), value);
	}

	inline static int32_t get_offset_of_FeedbackSizeValue_6() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924, ___FeedbackSizeValue_6)); }
	inline int32_t get_FeedbackSizeValue_6() const { return ___FeedbackSizeValue_6; }
	inline int32_t* get_address_of_FeedbackSizeValue_6() { return &___FeedbackSizeValue_6; }
	inline void set_FeedbackSizeValue_6(int32_t value)
	{
		___FeedbackSizeValue_6 = value;
	}

	inline static int32_t get_offset_of_ModeValue_7() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924, ___ModeValue_7)); }
	inline int32_t get_ModeValue_7() const { return ___ModeValue_7; }
	inline int32_t* get_address_of_ModeValue_7() { return &___ModeValue_7; }
	inline void set_ModeValue_7(int32_t value)
	{
		___ModeValue_7 = value;
	}

	inline static int32_t get_offset_of_PaddingValue_8() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924, ___PaddingValue_8)); }
	inline int32_t get_PaddingValue_8() const { return ___PaddingValue_8; }
	inline int32_t* get_address_of_PaddingValue_8() { return &___PaddingValue_8; }
	inline void set_PaddingValue_8(int32_t value)
	{
		___PaddingValue_8 = value;
	}

	inline static int32_t get_offset_of_m_disposed_9() { return static_cast<int32_t>(offsetof(SymmetricAlgorithm_t8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924, ___m_disposed_9)); }
	inline bool get_m_disposed_9() const { return ___m_disposed_9; }
	inline bool* get_address_of_m_disposed_9() { return &___m_disposed_9; }
	inline void set_m_disposed_9(bool value)
	{
		___m_disposed_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYMMETRICALGORITHM_T8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924_H
#ifndef RC4_T15F68CBEADB40270775AA750D85808CC5FE85F16_H
#define RC4_T15F68CBEADB40270775AA750D85808CC5FE85F16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.RC4
struct  RC4_t15F68CBEADB40270775AA750D85808CC5FE85F16  : public SymmetricAlgorithm_t8F7FDD6257ACFEEBEC01C602A300C31FFEDD1924
{
public:

public:
};

struct RC4_t15F68CBEADB40270775AA750D85808CC5FE85F16_StaticFields
{
public:
	// System.Security.Cryptography.KeySizes[] Mono.Security.Cryptography.RC4::s_legalBlockSizes
	KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* ___s_legalBlockSizes_10;
	// System.Security.Cryptography.KeySizes[] Mono.Security.Cryptography.RC4::s_legalKeySizes
	KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* ___s_legalKeySizes_11;

public:
	inline static int32_t get_offset_of_s_legalBlockSizes_10() { return static_cast<int32_t>(offsetof(RC4_t15F68CBEADB40270775AA750D85808CC5FE85F16_StaticFields, ___s_legalBlockSizes_10)); }
	inline KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* get_s_legalBlockSizes_10() const { return ___s_legalBlockSizes_10; }
	inline KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C** get_address_of_s_legalBlockSizes_10() { return &___s_legalBlockSizes_10; }
	inline void set_s_legalBlockSizes_10(KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* value)
	{
		___s_legalBlockSizes_10 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalBlockSizes_10), value);
	}

	inline static int32_t get_offset_of_s_legalKeySizes_11() { return static_cast<int32_t>(offsetof(RC4_t15F68CBEADB40270775AA750D85808CC5FE85F16_StaticFields, ___s_legalKeySizes_11)); }
	inline KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* get_s_legalKeySizes_11() const { return ___s_legalKeySizes_11; }
	inline KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C** get_address_of_s_legalKeySizes_11() { return &___s_legalKeySizes_11; }
	inline void set_s_legalKeySizes_11(KeySizesU5BU5D_t131B7C0FEFAA71CAB587F57C7292E1ECE043913C* value)
	{
		___s_legalKeySizes_11 = value;
		Il2CppCodeGenWriteBarrier((&___s_legalKeySizes_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RC4_T15F68CBEADB40270775AA750D85808CC5FE85F16_H
#ifndef KEYGENERATEDEVENTHANDLER_T40F3B0E6400CF6FB02BED8AB74B8BFEC1DBA5042_H
#define KEYGENERATEDEVENTHANDLER_T40F3B0E6400CF6FB02BED8AB74B8BFEC1DBA5042_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.RSAManaged_KeyGeneratedEventHandler
struct  KeyGeneratedEventHandler_t40F3B0E6400CF6FB02BED8AB74B8BFEC1DBA5042  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYGENERATEDEVENTHANDLER_T40F3B0E6400CF6FB02BED8AB74B8BFEC1DBA5042_H
#ifndef X509CERTIFICATE_TF81384408319B1A7C4393A41881A123CF518E4A7_H
#define X509CERTIFICATE_TF81384408319B1A7C4393A41881A123CF518E4A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Certificate
struct  X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7  : public RuntimeObject
{
public:
	// Mono.Security.ASN1 Mono.Security.X509.X509Certificate::decoder
	ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * ___decoder_0;
	// System.Byte[] Mono.Security.X509.X509Certificate::m_encodedcert
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___m_encodedcert_1;
	// System.DateTime Mono.Security.X509.X509Certificate::m_from
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___m_from_2;
	// System.DateTime Mono.Security.X509.X509Certificate::m_until
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___m_until_3;
	// Mono.Security.ASN1 Mono.Security.X509.X509Certificate::issuer
	ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * ___issuer_4;
	// System.String Mono.Security.X509.X509Certificate::m_issuername
	String_t* ___m_issuername_5;
	// System.String Mono.Security.X509.X509Certificate::m_keyalgo
	String_t* ___m_keyalgo_6;
	// System.Byte[] Mono.Security.X509.X509Certificate::m_keyalgoparams
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___m_keyalgoparams_7;
	// Mono.Security.ASN1 Mono.Security.X509.X509Certificate::subject
	ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * ___subject_8;
	// System.String Mono.Security.X509.X509Certificate::m_subject
	String_t* ___m_subject_9;
	// System.Byte[] Mono.Security.X509.X509Certificate::m_publickey
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___m_publickey_10;
	// System.Byte[] Mono.Security.X509.X509Certificate::signature
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___signature_11;
	// System.String Mono.Security.X509.X509Certificate::m_signaturealgo
	String_t* ___m_signaturealgo_12;
	// System.Byte[] Mono.Security.X509.X509Certificate::m_signaturealgoparams
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___m_signaturealgoparams_13;
	// System.Byte[] Mono.Security.X509.X509Certificate::certhash
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___certhash_14;
	// System.Security.Cryptography.RSA Mono.Security.X509.X509Certificate::_rsa
	RSA_t565B1E1F95589A0E37D0C66406CF730DC4A43051 * ____rsa_15;
	// System.Security.Cryptography.DSA Mono.Security.X509.X509Certificate::_dsa
	DSA_tF8DC81D58B7A574DF51517FD242D76B4FC225808 * ____dsa_16;
	// System.Int32 Mono.Security.X509.X509Certificate::version
	int32_t ___version_17;
	// System.Byte[] Mono.Security.X509.X509Certificate::serialnumber
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___serialnumber_18;
	// System.Byte[] Mono.Security.X509.X509Certificate::issuerUniqueID
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___issuerUniqueID_19;
	// System.Byte[] Mono.Security.X509.X509Certificate::subjectUniqueID
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___subjectUniqueID_20;
	// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Certificate::extensions
	X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991 * ___extensions_21;

public:
	inline static int32_t get_offset_of_decoder_0() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___decoder_0)); }
	inline ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * get_decoder_0() const { return ___decoder_0; }
	inline ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA ** get_address_of_decoder_0() { return &___decoder_0; }
	inline void set_decoder_0(ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * value)
	{
		___decoder_0 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_0), value);
	}

	inline static int32_t get_offset_of_m_encodedcert_1() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___m_encodedcert_1)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_m_encodedcert_1() const { return ___m_encodedcert_1; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_m_encodedcert_1() { return &___m_encodedcert_1; }
	inline void set_m_encodedcert_1(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___m_encodedcert_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_encodedcert_1), value);
	}

	inline static int32_t get_offset_of_m_from_2() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___m_from_2)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_m_from_2() const { return ___m_from_2; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_m_from_2() { return &___m_from_2; }
	inline void set_m_from_2(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___m_from_2 = value;
	}

	inline static int32_t get_offset_of_m_until_3() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___m_until_3)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_m_until_3() const { return ___m_until_3; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_m_until_3() { return &___m_until_3; }
	inline void set_m_until_3(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___m_until_3 = value;
	}

	inline static int32_t get_offset_of_issuer_4() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___issuer_4)); }
	inline ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * get_issuer_4() const { return ___issuer_4; }
	inline ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA ** get_address_of_issuer_4() { return &___issuer_4; }
	inline void set_issuer_4(ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * value)
	{
		___issuer_4 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_4), value);
	}

	inline static int32_t get_offset_of_m_issuername_5() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___m_issuername_5)); }
	inline String_t* get_m_issuername_5() const { return ___m_issuername_5; }
	inline String_t** get_address_of_m_issuername_5() { return &___m_issuername_5; }
	inline void set_m_issuername_5(String_t* value)
	{
		___m_issuername_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_issuername_5), value);
	}

	inline static int32_t get_offset_of_m_keyalgo_6() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___m_keyalgo_6)); }
	inline String_t* get_m_keyalgo_6() const { return ___m_keyalgo_6; }
	inline String_t** get_address_of_m_keyalgo_6() { return &___m_keyalgo_6; }
	inline void set_m_keyalgo_6(String_t* value)
	{
		___m_keyalgo_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyalgo_6), value);
	}

	inline static int32_t get_offset_of_m_keyalgoparams_7() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___m_keyalgoparams_7)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_m_keyalgoparams_7() const { return ___m_keyalgoparams_7; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_m_keyalgoparams_7() { return &___m_keyalgoparams_7; }
	inline void set_m_keyalgoparams_7(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___m_keyalgoparams_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_keyalgoparams_7), value);
	}

	inline static int32_t get_offset_of_subject_8() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___subject_8)); }
	inline ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * get_subject_8() const { return ___subject_8; }
	inline ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA ** get_address_of_subject_8() { return &___subject_8; }
	inline void set_subject_8(ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA * value)
	{
		___subject_8 = value;
		Il2CppCodeGenWriteBarrier((&___subject_8), value);
	}

	inline static int32_t get_offset_of_m_subject_9() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___m_subject_9)); }
	inline String_t* get_m_subject_9() const { return ___m_subject_9; }
	inline String_t** get_address_of_m_subject_9() { return &___m_subject_9; }
	inline void set_m_subject_9(String_t* value)
	{
		___m_subject_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_subject_9), value);
	}

	inline static int32_t get_offset_of_m_publickey_10() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___m_publickey_10)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_m_publickey_10() const { return ___m_publickey_10; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_m_publickey_10() { return &___m_publickey_10; }
	inline void set_m_publickey_10(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___m_publickey_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_publickey_10), value);
	}

	inline static int32_t get_offset_of_signature_11() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___signature_11)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_signature_11() const { return ___signature_11; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_signature_11() { return &___signature_11; }
	inline void set_signature_11(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___signature_11 = value;
		Il2CppCodeGenWriteBarrier((&___signature_11), value);
	}

	inline static int32_t get_offset_of_m_signaturealgo_12() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___m_signaturealgo_12)); }
	inline String_t* get_m_signaturealgo_12() const { return ___m_signaturealgo_12; }
	inline String_t** get_address_of_m_signaturealgo_12() { return &___m_signaturealgo_12; }
	inline void set_m_signaturealgo_12(String_t* value)
	{
		___m_signaturealgo_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_signaturealgo_12), value);
	}

	inline static int32_t get_offset_of_m_signaturealgoparams_13() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___m_signaturealgoparams_13)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_m_signaturealgoparams_13() const { return ___m_signaturealgoparams_13; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_m_signaturealgoparams_13() { return &___m_signaturealgoparams_13; }
	inline void set_m_signaturealgoparams_13(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___m_signaturealgoparams_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_signaturealgoparams_13), value);
	}

	inline static int32_t get_offset_of_certhash_14() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___certhash_14)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_certhash_14() const { return ___certhash_14; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_certhash_14() { return &___certhash_14; }
	inline void set_certhash_14(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___certhash_14 = value;
		Il2CppCodeGenWriteBarrier((&___certhash_14), value);
	}

	inline static int32_t get_offset_of__rsa_15() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ____rsa_15)); }
	inline RSA_t565B1E1F95589A0E37D0C66406CF730DC4A43051 * get__rsa_15() const { return ____rsa_15; }
	inline RSA_t565B1E1F95589A0E37D0C66406CF730DC4A43051 ** get_address_of__rsa_15() { return &____rsa_15; }
	inline void set__rsa_15(RSA_t565B1E1F95589A0E37D0C66406CF730DC4A43051 * value)
	{
		____rsa_15 = value;
		Il2CppCodeGenWriteBarrier((&____rsa_15), value);
	}

	inline static int32_t get_offset_of__dsa_16() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ____dsa_16)); }
	inline DSA_tF8DC81D58B7A574DF51517FD242D76B4FC225808 * get__dsa_16() const { return ____dsa_16; }
	inline DSA_tF8DC81D58B7A574DF51517FD242D76B4FC225808 ** get_address_of__dsa_16() { return &____dsa_16; }
	inline void set__dsa_16(DSA_tF8DC81D58B7A574DF51517FD242D76B4FC225808 * value)
	{
		____dsa_16 = value;
		Il2CppCodeGenWriteBarrier((&____dsa_16), value);
	}

	inline static int32_t get_offset_of_version_17() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___version_17)); }
	inline int32_t get_version_17() const { return ___version_17; }
	inline int32_t* get_address_of_version_17() { return &___version_17; }
	inline void set_version_17(int32_t value)
	{
		___version_17 = value;
	}

	inline static int32_t get_offset_of_serialnumber_18() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___serialnumber_18)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_serialnumber_18() const { return ___serialnumber_18; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_serialnumber_18() { return &___serialnumber_18; }
	inline void set_serialnumber_18(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___serialnumber_18 = value;
		Il2CppCodeGenWriteBarrier((&___serialnumber_18), value);
	}

	inline static int32_t get_offset_of_issuerUniqueID_19() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___issuerUniqueID_19)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_issuerUniqueID_19() const { return ___issuerUniqueID_19; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_issuerUniqueID_19() { return &___issuerUniqueID_19; }
	inline void set_issuerUniqueID_19(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___issuerUniqueID_19 = value;
		Il2CppCodeGenWriteBarrier((&___issuerUniqueID_19), value);
	}

	inline static int32_t get_offset_of_subjectUniqueID_20() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___subjectUniqueID_20)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_subjectUniqueID_20() const { return ___subjectUniqueID_20; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_subjectUniqueID_20() { return &___subjectUniqueID_20; }
	inline void set_subjectUniqueID_20(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___subjectUniqueID_20 = value;
		Il2CppCodeGenWriteBarrier((&___subjectUniqueID_20), value);
	}

	inline static int32_t get_offset_of_extensions_21() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7, ___extensions_21)); }
	inline X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991 * get_extensions_21() const { return ___extensions_21; }
	inline X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991 ** get_address_of_extensions_21() { return &___extensions_21; }
	inline void set_extensions_21(X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991 * value)
	{
		___extensions_21 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_21), value);
	}
};

struct X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7_StaticFields
{
public:
	// System.String Mono.Security.X509.X509Certificate::encoding_error
	String_t* ___encoding_error_22;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.X509Certificate::<>f__switchU24mapF
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24mapF_23;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.X509Certificate::<>f__switchU24map10
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map10_24;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.X509Certificate::<>f__switchU24map11
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map11_25;

public:
	inline static int32_t get_offset_of_encoding_error_22() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7_StaticFields, ___encoding_error_22)); }
	inline String_t* get_encoding_error_22() const { return ___encoding_error_22; }
	inline String_t** get_address_of_encoding_error_22() { return &___encoding_error_22; }
	inline void set_encoding_error_22(String_t* value)
	{
		___encoding_error_22 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_error_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24mapF_23() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7_StaticFields, ___U3CU3Ef__switchU24mapF_23)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24mapF_23() const { return ___U3CU3Ef__switchU24mapF_23; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24mapF_23() { return &___U3CU3Ef__switchU24mapF_23; }
	inline void set_U3CU3Ef__switchU24mapF_23(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24mapF_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24mapF_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map10_24() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7_StaticFields, ___U3CU3Ef__switchU24map10_24)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map10_24() const { return ___U3CU3Ef__switchU24map10_24; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map10_24() { return &___U3CU3Ef__switchU24map10_24; }
	inline void set_U3CU3Ef__switchU24map10_24(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map10_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map10_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map11_25() { return static_cast<int32_t>(offsetof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7_StaticFields, ___U3CU3Ef__switchU24map11_25)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map11_25() const { return ___U3CU3Ef__switchU24map11_25; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map11_25() { return &___U3CU3Ef__switchU24map11_25; }
	inline void set_U3CU3Ef__switchU24map11_25(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map11_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map11_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CERTIFICATE_TF81384408319B1A7C4393A41881A123CF518E4A7_H
#ifndef X509CRL_T0F4FF7AC198D6861956B4C959663AA0C23A534BA_H
#define X509CRL_T0F4FF7AC198D6861956B4C959663AA0C23A534BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Crl
struct  X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA  : public RuntimeObject
{
public:
	// System.String Mono.Security.X509.X509Crl::issuer
	String_t* ___issuer_0;
	// System.Byte Mono.Security.X509.X509Crl::version
	uint8_t ___version_1;
	// System.DateTime Mono.Security.X509.X509Crl::thisUpdate
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___thisUpdate_2;
	// System.DateTime Mono.Security.X509.X509Crl::nextUpdate
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___nextUpdate_3;
	// System.Collections.ArrayList Mono.Security.X509.X509Crl::entries
	ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * ___entries_4;
	// System.String Mono.Security.X509.X509Crl::signatureOID
	String_t* ___signatureOID_5;
	// System.Byte[] Mono.Security.X509.X509Crl::signature
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___signature_6;
	// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl::extensions
	X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991 * ___extensions_7;
	// System.Byte[] Mono.Security.X509.X509Crl::encoded
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___encoded_8;
	// System.Byte[] Mono.Security.X509.X509Crl::hash_value
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___hash_value_9;

public:
	inline static int32_t get_offset_of_issuer_0() { return static_cast<int32_t>(offsetof(X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA, ___issuer_0)); }
	inline String_t* get_issuer_0() const { return ___issuer_0; }
	inline String_t** get_address_of_issuer_0() { return &___issuer_0; }
	inline void set_issuer_0(String_t* value)
	{
		___issuer_0 = value;
		Il2CppCodeGenWriteBarrier((&___issuer_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA, ___version_1)); }
	inline uint8_t get_version_1() const { return ___version_1; }
	inline uint8_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(uint8_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_thisUpdate_2() { return static_cast<int32_t>(offsetof(X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA, ___thisUpdate_2)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_thisUpdate_2() const { return ___thisUpdate_2; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_thisUpdate_2() { return &___thisUpdate_2; }
	inline void set_thisUpdate_2(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___thisUpdate_2 = value;
	}

	inline static int32_t get_offset_of_nextUpdate_3() { return static_cast<int32_t>(offsetof(X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA, ___nextUpdate_3)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_nextUpdate_3() const { return ___nextUpdate_3; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_nextUpdate_3() { return &___nextUpdate_3; }
	inline void set_nextUpdate_3(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___nextUpdate_3 = value;
	}

	inline static int32_t get_offset_of_entries_4() { return static_cast<int32_t>(offsetof(X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA, ___entries_4)); }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * get_entries_4() const { return ___entries_4; }
	inline ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F ** get_address_of_entries_4() { return &___entries_4; }
	inline void set_entries_4(ArrayList_t438A4032A4FBAF49F565FAD10C546E26CFBD847F * value)
	{
		___entries_4 = value;
		Il2CppCodeGenWriteBarrier((&___entries_4), value);
	}

	inline static int32_t get_offset_of_signatureOID_5() { return static_cast<int32_t>(offsetof(X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA, ___signatureOID_5)); }
	inline String_t* get_signatureOID_5() const { return ___signatureOID_5; }
	inline String_t** get_address_of_signatureOID_5() { return &___signatureOID_5; }
	inline void set_signatureOID_5(String_t* value)
	{
		___signatureOID_5 = value;
		Il2CppCodeGenWriteBarrier((&___signatureOID_5), value);
	}

	inline static int32_t get_offset_of_signature_6() { return static_cast<int32_t>(offsetof(X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA, ___signature_6)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_signature_6() const { return ___signature_6; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_signature_6() { return &___signature_6; }
	inline void set_signature_6(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___signature_6 = value;
		Il2CppCodeGenWriteBarrier((&___signature_6), value);
	}

	inline static int32_t get_offset_of_extensions_7() { return static_cast<int32_t>(offsetof(X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA, ___extensions_7)); }
	inline X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991 * get_extensions_7() const { return ___extensions_7; }
	inline X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991 ** get_address_of_extensions_7() { return &___extensions_7; }
	inline void set_extensions_7(X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991 * value)
	{
		___extensions_7 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_7), value);
	}

	inline static int32_t get_offset_of_encoded_8() { return static_cast<int32_t>(offsetof(X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA, ___encoded_8)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_encoded_8() const { return ___encoded_8; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_encoded_8() { return &___encoded_8; }
	inline void set_encoded_8(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___encoded_8 = value;
		Il2CppCodeGenWriteBarrier((&___encoded_8), value);
	}

	inline static int32_t get_offset_of_hash_value_9() { return static_cast<int32_t>(offsetof(X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA, ___hash_value_9)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_hash_value_9() const { return ___hash_value_9; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_hash_value_9() { return &___hash_value_9; }
	inline void set_hash_value_9(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___hash_value_9 = value;
		Il2CppCodeGenWriteBarrier((&___hash_value_9), value);
	}
};

struct X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Mono.Security.X509.X509Crl::<>f__switchU24map13
	Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * ___U3CU3Ef__switchU24map13_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map13_10() { return static_cast<int32_t>(offsetof(X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA_StaticFields, ___U3CU3Ef__switchU24map13_10)); }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * get_U3CU3Ef__switchU24map13_10() const { return ___U3CU3Ef__switchU24map13_10; }
	inline Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 ** get_address_of_U3CU3Ef__switchU24map13_10() { return &___U3CU3Ef__switchU24map13_10; }
	inline void set_U3CU3Ef__switchU24map13_10(Dictionary_2_tC40CE8B8795121971E021F04C9E151F97814FCA1 * value)
	{
		___U3CU3Ef__switchU24map13_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map13_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CRL_T0F4FF7AC198D6861956B4C959663AA0C23A534BA_H
#ifndef X509CRLENTRY_TBF2584E52778416F84EA34B6F693BF1BED81EFF1_H
#define X509CRLENTRY_TBF2584E52778416F84EA34B6F693BF1BED81EFF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.X509.X509Crl_X509CrlEntry
struct  X509CrlEntry_tBF2584E52778416F84EA34B6F693BF1BED81EFF1  : public RuntimeObject
{
public:
	// System.Byte[] Mono.Security.X509.X509Crl_X509CrlEntry::sn
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___sn_0;
	// System.DateTime Mono.Security.X509.X509Crl_X509CrlEntry::revocationDate
	DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  ___revocationDate_1;
	// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl_X509CrlEntry::extensions
	X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991 * ___extensions_2;

public:
	inline static int32_t get_offset_of_sn_0() { return static_cast<int32_t>(offsetof(X509CrlEntry_tBF2584E52778416F84EA34B6F693BF1BED81EFF1, ___sn_0)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_sn_0() const { return ___sn_0; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_sn_0() { return &___sn_0; }
	inline void set_sn_0(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___sn_0 = value;
		Il2CppCodeGenWriteBarrier((&___sn_0), value);
	}

	inline static int32_t get_offset_of_revocationDate_1() { return static_cast<int32_t>(offsetof(X509CrlEntry_tBF2584E52778416F84EA34B6F693BF1BED81EFF1, ___revocationDate_1)); }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  get_revocationDate_1() const { return ___revocationDate_1; }
	inline DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2 * get_address_of_revocationDate_1() { return &___revocationDate_1; }
	inline void set_revocationDate_1(DateTime_t816BBD9125EA02B42B87CB07643FC5F803391DA2  value)
	{
		___revocationDate_1 = value;
	}

	inline static int32_t get_offset_of_extensions_2() { return static_cast<int32_t>(offsetof(X509CrlEntry_tBF2584E52778416F84EA34B6F693BF1BED81EFF1, ___extensions_2)); }
	inline X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991 * get_extensions_2() const { return ___extensions_2; }
	inline X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991 ** get_address_of_extensions_2() { return &___extensions_2; }
	inline void set_extensions_2(X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991 * value)
	{
		___extensions_2 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // X509CRLENTRY_TBF2584E52778416F84EA34B6F693BF1BED81EFF1_H
#ifndef ARC4MANAGED_T59C99F05E24FC417CC36AFD9202DCFB233462561_H
#define ARC4MANAGED_T59C99F05E24FC417CC36AFD9202DCFB233462561_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mono.Security.Cryptography.ARC4Managed
struct  ARC4Managed_t59C99F05E24FC417CC36AFD9202DCFB233462561  : public RC4_t15F68CBEADB40270775AA750D85808CC5FE85F16
{
public:
	// System.Byte[] Mono.Security.Cryptography.ARC4Managed::key
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___key_12;
	// System.Byte[] Mono.Security.Cryptography.ARC4Managed::state
	ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* ___state_13;
	// System.Byte Mono.Security.Cryptography.ARC4Managed::x
	uint8_t ___x_14;
	// System.Byte Mono.Security.Cryptography.ARC4Managed::y
	uint8_t ___y_15;
	// System.Boolean Mono.Security.Cryptography.ARC4Managed::m_disposed
	bool ___m_disposed_16;

public:
	inline static int32_t get_offset_of_key_12() { return static_cast<int32_t>(offsetof(ARC4Managed_t59C99F05E24FC417CC36AFD9202DCFB233462561, ___key_12)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_key_12() const { return ___key_12; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_key_12() { return &___key_12; }
	inline void set_key_12(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___key_12 = value;
		Il2CppCodeGenWriteBarrier((&___key_12), value);
	}

	inline static int32_t get_offset_of_state_13() { return static_cast<int32_t>(offsetof(ARC4Managed_t59C99F05E24FC417CC36AFD9202DCFB233462561, ___state_13)); }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* get_state_13() const { return ___state_13; }
	inline ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8** get_address_of_state_13() { return &___state_13; }
	inline void set_state_13(ByteU5BU5D_t8E14BD25C7BEB727CB1849CF449DE26B113309E8* value)
	{
		___state_13 = value;
		Il2CppCodeGenWriteBarrier((&___state_13), value);
	}

	inline static int32_t get_offset_of_x_14() { return static_cast<int32_t>(offsetof(ARC4Managed_t59C99F05E24FC417CC36AFD9202DCFB233462561, ___x_14)); }
	inline uint8_t get_x_14() const { return ___x_14; }
	inline uint8_t* get_address_of_x_14() { return &___x_14; }
	inline void set_x_14(uint8_t value)
	{
		___x_14 = value;
	}

	inline static int32_t get_offset_of_y_15() { return static_cast<int32_t>(offsetof(ARC4Managed_t59C99F05E24FC417CC36AFD9202DCFB233462561, ___y_15)); }
	inline uint8_t get_y_15() const { return ___y_15; }
	inline uint8_t* get_address_of_y_15() { return &___y_15; }
	inline void set_y_15(uint8_t value)
	{
		___y_15 = value;
	}

	inline static int32_t get_offset_of_m_disposed_16() { return static_cast<int32_t>(offsetof(ARC4Managed_t59C99F05E24FC417CC36AFD9202DCFB233462561, ___m_disposed_16)); }
	inline bool get_m_disposed_16() const { return ___m_disposed_16; }
	inline bool* get_address_of_m_disposed_16() { return &___m_disposed_16; }
	inline void set_m_disposed_16(bool value)
	{
		___m_disposed_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARC4MANAGED_T59C99F05E24FC417CC36AFD9202DCFB233462561_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[6] = 
{
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C::get_offset_of_names_0(),
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C::get_offset_of_element_1(),
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C::get_offset_of_lockType_2(),
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C::get_offset_of_is_modified_3(),
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C::get_offset_of_valid_name_hash_4(),
	ConfigurationLockCollection_t2F264A39EDCF3C2B8F16D2D2843154BC6FFA244C::get_offset_of_valid_names_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (ConfigurationManager_t78E461FD29B1306CEC46889C728A32A2828AAC1C), -1, sizeof(ConfigurationManager_t78E461FD29B1306CEC46889C728A32A2828AAC1C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1801[3] = 
{
	ConfigurationManager_t78E461FD29B1306CEC46889C728A32A2828AAC1C_StaticFields::get_offset_of_configFactory_0(),
	ConfigurationManager_t78E461FD29B1306CEC46889C728A32A2828AAC1C_StaticFields::get_offset_of_configSystem_1(),
	ConfigurationManager_t78E461FD29B1306CEC46889C728A32A2828AAC1C_StaticFields::get_offset_of_lockobj_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400), -1, sizeof(ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1802[9] = 
{
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400_StaticFields::get_offset_of_NoDefaultValue_0(),
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400::get_offset_of_name_1(),
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400::get_offset_of_type_2(),
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400::get_offset_of_default_value_3(),
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400::get_offset_of_converter_4(),
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400::get_offset_of_validation_5(),
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400::get_offset_of_flags_6(),
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400::get_offset_of_description_7(),
	ConfigurationProperty_t4AE3E848FDE59333B2B83BF7BBD40F0A66837400::get_offset_of_collectionAttribute_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (ConfigurationPropertyAttribute_tFBC82E620CACDE37B530C525FEC7653F77CE0101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[3] = 
{
	ConfigurationPropertyAttribute_tFBC82E620CACDE37B530C525FEC7653F77CE0101::get_offset_of_name_0(),
	ConfigurationPropertyAttribute_tFBC82E620CACDE37B530C525FEC7653F77CE0101::get_offset_of_default_value_1(),
	ConfigurationPropertyAttribute_tFBC82E620CACDE37B530C525FEC7653F77CE0101::get_offset_of_flags_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[1] = 
{
	ConfigurationPropertyCollection_t3704E32C4A8B0F542BBCBC51FD42167C200961E9::get_offset_of_collection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (ConfigurationPropertyOptions_t64C3CDD6F3EAD472075436034225B6AF610468A9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1805[5] = 
{
	ConfigurationPropertyOptions_t64C3CDD6F3EAD472075436034225B6AF610468A9::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (ConfigurationSaveMode_tC56A23C692BDA4E51A37965E1B4B3B088076EDD5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1806[4] = 
{
	ConfigurationSaveMode_tC56A23C692BDA4E51A37965E1B4B3B088076EDD5::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[4] = 
{
	ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD::get_offset_of_sectionInformation_13(),
	ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD::get_offset_of_section_handler_14(),
	ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD::get_offset_of_externalDataXml_15(),
	ConfigurationSection_tEEA9EEE1BC7B9D5DDEFA6430A02FD78CC9A235BD::get_offset_of__configContext_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[2] = 
{
	ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC::get_offset_of_group_10(),
	ConfigurationSectionCollection_t2BACE8691335F63E842B656C3688BDAFA4AC58EC::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (U3CGetEnumeratorU3Ec__Iterator0_t02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[5] = 
{
	U3CGetEnumeratorU3Ec__Iterator0_t02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82::get_offset_of_U3CU24s_32U3E__0_0(),
	U3CGetEnumeratorU3Ec__Iterator0_t02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82::get_offset_of_U3CkeyU3E__1_1(),
	U3CGetEnumeratorU3Ec__Iterator0_t02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82::get_offset_of_U24PC_2(),
	U3CGetEnumeratorU3Ec__Iterator0_t02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82::get_offset_of_U24current_3(),
	U3CGetEnumeratorU3Ec__Iterator0_t02CF1AAD9A0635059B0EEDE23CE1FED3BB906F82::get_offset_of_U3CU3Ef__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[5] = 
{
	ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79::get_offset_of_sections_0(),
	ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79::get_offset_of_groups_1(),
	ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79::get_offset_of_config_2(),
	ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79::get_offset_of_group_3(),
	ConfigurationSectionGroup_tFC54951AF5D7CFA7474D80E5D901D04DE8CB5E79::get_offset_of_initialized_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (ConfigurationSectionGroupCollection_t8FBD02E07771CB420A315A28E362FC9B48499052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[2] = 
{
	ConfigurationSectionGroupCollection_t8FBD02E07771CB420A315A28E362FC9B48499052::get_offset_of_group_10(),
	ConfigurationSectionGroupCollection_t8FBD02E07771CB420A315A28E362FC9B48499052::get_offset_of_config_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (ConfigurationUserLevel_tEC7EA3B13C8E11D592EBA37FEED1A1CC10BEC7D5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1812[4] = 
{
	ConfigurationUserLevel_tEC7EA3B13C8E11D592EBA37FEED1A1CC10BEC7D5::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (ConfigurationValidatorAttribute_t76B8E3B6ADAF51BE9EB82BBA4A5C7213DDBEC724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1813[2] = 
{
	ConfigurationValidatorAttribute_t76B8E3B6ADAF51BE9EB82BBA4A5C7213DDBEC724::get_offset_of_validatorType_0(),
	ConfigurationValidatorAttribute_t76B8E3B6ADAF51BE9EB82BBA4A5C7213DDBEC724::get_offset_of_instance_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (ConfigurationValidatorBase_tD691540223153E9DEA98BDBD778BCB12A56248ED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (ConfigXmlTextReader_t8DBF4B876F941205E972027A42FC799AA145780A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[1] = 
{
	ConfigXmlTextReader_t8DBF4B876F941205E972027A42FC799AA145780A::get_offset_of_fileName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (DefaultSection_tE9AB28D010EDC291C1BCA7552C69C901BE76643D), -1, sizeof(DefaultSection_tE9AB28D010EDC291C1BCA7552C69C901BE76643D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1816[1] = 
{
	DefaultSection_tE9AB28D010EDC291C1BCA7552C69C901BE76643D_StaticFields::get_offset_of_properties_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (DefaultValidator_t6B6B01DC537A705EB34A6757737ED4287BF4CC7A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[3] = 
{
	ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26::get_offset_of_propertyInfo_0(),
	ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26::get_offset_of_owner_1(),
	ElementInformation_tE8DA87B8E081226EC6DA3E9F34E0E435A62D1C26::get_offset_of_properties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[3] = 
{
	ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB::get_offset_of_exeConfigFilename_1(),
	ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB::get_offset_of_localUserConfigFilename_2(),
	ExeConfigurationFileMap_t07633C2760732AD65BCFFCFB0D2F0D6C05F527AB::get_offset_of_roamingUserConfigFilename_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (IgnoreSection_t9052979AAF08694DF92570F6DB85067EE68BC901), -1, sizeof(IgnoreSection_t9052979AAF08694DF92570F6DB85067EE68BC901_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1820[2] = 
{
	IgnoreSection_t9052979AAF08694DF92570F6DB85067EE68BC901::get_offset_of_xml_17(),
	IgnoreSection_t9052979AAF08694DF92570F6DB85067EE68BC901_StaticFields::get_offset_of_properties_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (InternalConfigurationFactory_tDBD58CD148E84569F905006D415041A082593382), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (InternalConfigurationSystem_tD42591E3647EAD9A1CD177FE17AE59324A0B8BB7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[3] = 
{
	InternalConfigurationSystem_tD42591E3647EAD9A1CD177FE17AE59324A0B8BB7::get_offset_of_host_0(),
	InternalConfigurationSystem_tD42591E3647EAD9A1CD177FE17AE59324A0B8BB7::get_offset_of_root_1(),
	InternalConfigurationSystem_tD42591E3647EAD9A1CD177FE17AE59324A0B8BB7::get_offset_of_hostInitParams_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (InternalConfigurationHost_t89A94135BBC91E89941129D05624642760575086), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (ExeConfigurationHost_t41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B), -1, sizeof(ExeConfigurationHost_t41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1824[3] = 
{
	ExeConfigurationHost_t41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B::get_offset_of_map_0(),
	ExeConfigurationHost_t41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B::get_offset_of_level_1(),
	ExeConfigurationHost_t41D5C5AA9BC5E5969209093E87088F0EDE8F5A7B_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (InternalConfigurationRoot_tA6A1CDE68BFB644A6D68A1B07D5D805A81F9EF20), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[2] = 
{
	InternalConfigurationRoot_tA6A1CDE68BFB644A6D68A1B07D5D805A81F9EF20::get_offset_of_host_0(),
	InternalConfigurationRoot_tA6A1CDE68BFB644A6D68A1B07D5D805A81F9EF20::get_offset_of_isDesignTime_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[5] = 
{
	PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF::get_offset_of_isModified_0(),
	PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF::get_offset_of_val_1(),
	PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF::get_offset_of_origin_2(),
	PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF::get_offset_of_owner_3(),
	PropertyInformation_t855727B59A0F1EFADF230F0A209DAF108D666BDF::get_offset_of_property_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (PropertyInformationCollection_t6CBE051D205C8065A196AD13E4E5FA575B82AD52), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (PropertyInformationEnumerator_t8A4F098D93D66DFF14EEB4B536ABF022CC0DF231), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[2] = 
{
	PropertyInformationEnumerator_t8A4F098D93D66DFF14EEB4B536ABF022CC0DF231::get_offset_of_collection_0(),
	PropertyInformationEnumerator_t8A4F098D93D66DFF14EEB4B536ABF022CC0DF231::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (PropertyValueOrigin_t5FFE6EAB47694726F7A3E9B0A5D9DF1C0035A11C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1829[4] = 
{
	PropertyValueOrigin_t5FFE6EAB47694726F7A3E9B0A5D9DF1C0035A11C::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (ProtectedConfiguration_t6CA812ECE2C605D4FDCB495584DB7D34E804428C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (ProtectedConfigurationProvider_t6D1CFE3639AFF7A42A854B97BD575CFE05ED68A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (ProtectedConfigurationProviderCollection_t6F2A506BF598B7730852E9C57D207277687FBCDE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (ProtectedConfigurationSection_tD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0), -1, sizeof(ProtectedConfigurationSection_tD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1833[4] = 
{
	ProtectedConfigurationSection_tD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0_StaticFields::get_offset_of_defaultProviderProp_17(),
	ProtectedConfigurationSection_tD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0_StaticFields::get_offset_of_providersProp_18(),
	ProtectedConfigurationSection_tD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0_StaticFields::get_offset_of_properties_19(),
	ProtectedConfigurationSection_tD43BA42BC583FF8AD6E2F3052D4DDF14CF13E0A0::get_offset_of_providers_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (ProviderSettings_t99D7F6BF7553F7E9365222895DCB944F66F282B0), -1, sizeof(ProviderSettings_t99D7F6BF7553F7E9365222895DCB944F66F282B0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1834[4] = 
{
	ProviderSettings_t99D7F6BF7553F7E9365222895DCB944F66F282B0::get_offset_of_parameters_13(),
	ProviderSettings_t99D7F6BF7553F7E9365222895DCB944F66F282B0_StaticFields::get_offset_of_nameProp_14(),
	ProviderSettings_t99D7F6BF7553F7E9365222895DCB944F66F282B0_StaticFields::get_offset_of_typeProp_15(),
	ProviderSettings_t99D7F6BF7553F7E9365222895DCB944F66F282B0_StaticFields::get_offset_of_properties_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (ProviderSettingsCollection_t3955D5E0D34A2639D9BD61A7E7C4A375496B5CEB), -1, sizeof(ProviderSettingsCollection_t3955D5E0D34A2639D9BD61A7E7C4A375496B5CEB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1835[1] = 
{
	ProviderSettingsCollection_t3955D5E0D34A2639D9BD61A7E7C4A375496B5CEB_StaticFields::get_offset_of_props_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0), -1, sizeof(SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1836[6] = 
{
	SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0::get_offset_of_allowLocation_6(),
	SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0::get_offset_of_requirePermission_7(),
	SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0::get_offset_of_restartOnExternalChanges_8(),
	SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0::get_offset_of_allowDefinition_9(),
	SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0::get_offset_of_allowExeDefinition_10(),
	SectionInfo_t4ACCD27A8F4E403A1A72F2F96A20B2524BF22EF0_StaticFields::get_offset_of_U3CU3Ef__switchU24map1_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61), -1, sizeof(SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1837[3] = 
{
	SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61::get_offset_of_sections_6(),
	SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61::get_offset_of_groups_7(),
	SectionGroupInfo_t5B331E18DED09B455B66D186D9F9CBF2D5B2FE61_StaticFields::get_offset_of_emptyList_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (ConfigInfoCollection_t7EE8EFC5E6CBB02D7C45C2D65449085FD5540D3A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[13] = 
{
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_parent_0(),
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_allow_definition_1(),
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_allow_exe_definition_2(),
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_allow_location_3(),
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_allow_override_4(),
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_inherit_on_child_apps_5(),
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_restart_on_external_changes_6(),
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_require_permission_7(),
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_config_source_8(),
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_name_9(),
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_raw_xml_10(),
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_protection_provider_11(),
	SectionInformation_tB3EAD382083CF190583FA144FAFC95BA6E0AFC9A::get_offset_of_U3CConfigFilePathU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (MonoTODOAttribute_tC7E5E9F0CD72D820EAE8779D8457C93F3E5A8333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[1] = 
{
	MonoTODOAttribute_tC7E5E9F0CD72D820EAE8779D8457C93F3E5A8333::get_offset_of_comment_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (MonoInternalNoteAttribute_tFAF383C49F7DE1A5AB26457F9B9CFAC8B1B03728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (U3CModuleU3E_t2CD222707D881B4F268233C02FD6259FB0EB68C8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (Locale_t98FA504E7F660E562AC58D271613184BD1DEF66F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E), -1, sizeof(BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1844[4] = 
{
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E::get_offset_of_length_0(),
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E::get_offset_of_data_1(),
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E_StaticFields::get_offset_of_smallPrimes_2(),
	BigInteger_tC6980D601BAEF7199637D9135ACA5ADFD7D5AF8E_StaticFields::get_offset_of_rng_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (Sign_tDBA68F5BB24DC260357E23BAC3ADF8B8640AD541)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1845[4] = 
{
	Sign_tDBA68F5BB24DC260357E23BAC3ADF8B8640AD541::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (ModulusRing_t5F514F6941DA17EC8D1FD6D170AAD9F9FA233541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[2] = 
{
	ModulusRing_t5F514F6941DA17EC8D1FD6D170AAD9F9FA233541::get_offset_of_mod_0(),
	ModulusRing_t5F514F6941DA17EC8D1FD6D170AAD9F9FA233541::get_offset_of_constant_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (Kernel_tCDD9163849EF7FF83071E05F980FD8813EB50A37), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (ConfidenceFactor_t09F5717EE806C44975FD23B3ED7DB48019FFD84E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1848[7] = 
{
	ConfidenceFactor_t09F5717EE806C44975FD23B3ED7DB48019FFD84E::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (PrimalityTests_tE581B64657A89C85DBF15ED2C7C39C79AAFF7B70), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (PrimeGeneratorBase_tE8C14F85FCFF73ABFBC8B57DFE8F5A634DF477FE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (SequentialSearchPrimeGeneratorBase_t3E8A2759CFF3EBCCA4D5F2B5514FE449D8AEBD08), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[3] = 
{
	ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA::get_offset_of_m_nTag_0(),
	ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA::get_offset_of_m_aValue_1(),
	ASN1_t21F71D5E42416849A26C9ACE043F386E7F03D7FA::get_offset_of_elist_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (ASN1Convert_tC746863213FE0090C01434A73EF88A69F48D093B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (BitConverterLE_t7137470AF89208272C3686402978485B112F3F66), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (PKCS7_t3D2A0269E93DED7E5D15D8C7FF375E7FD2D0AFA0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (ContentInfo_t51049CE410146B36AF37C7218807051D5996866F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[2] = 
{
	ContentInfo_t51049CE410146B36AF37C7218807051D5996866F::get_offset_of_contentType_0(),
	ContentInfo_t51049CE410146B36AF37C7218807051D5996866F::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (EncryptedData_t3170C44CDA01A6629A7A44AA069A720A5326EB2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[4] = 
{
	EncryptedData_t3170C44CDA01A6629A7A44AA069A720A5326EB2C::get_offset_of__version_0(),
	EncryptedData_t3170C44CDA01A6629A7A44AA069A720A5326EB2C::get_offset_of__content_1(),
	EncryptedData_t3170C44CDA01A6629A7A44AA069A720A5326EB2C::get_offset_of__encryptionAlgorithm_2(),
	EncryptedData_t3170C44CDA01A6629A7A44AA069A720A5326EB2C::get_offset_of__encrypted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (ARC4Managed_t59C99F05E24FC417CC36AFD9202DCFB233462561), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[5] = 
{
	ARC4Managed_t59C99F05E24FC417CC36AFD9202DCFB233462561::get_offset_of_key_12(),
	ARC4Managed_t59C99F05E24FC417CC36AFD9202DCFB233462561::get_offset_of_state_13(),
	ARC4Managed_t59C99F05E24FC417CC36AFD9202DCFB233462561::get_offset_of_x_14(),
	ARC4Managed_t59C99F05E24FC417CC36AFD9202DCFB233462561::get_offset_of_y_15(),
	ARC4Managed_t59C99F05E24FC417CC36AFD9202DCFB233462561::get_offset_of_m_disposed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (CryptoConvert_t2C19EA750A34EFDF9DF6C017758C5C0E0191374C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (KeyBuilder_t3FFC7E782BE25F71226B048DAAFAA2CEB74C3C9C), -1, sizeof(KeyBuilder_t3FFC7E782BE25F71226B048DAAFAA2CEB74C3C9C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1860[1] = 
{
	KeyBuilder_t3FFC7E782BE25F71226B048DAAFAA2CEB74C3C9C_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (MD2_tD12D2F4F3A46581687DEC22F27262968546E1DC7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3), -1, sizeof(MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1862[6] = 
{
	MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3::get_offset_of_state_4(),
	MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3::get_offset_of_checksum_5(),
	MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3::get_offset_of_buffer_6(),
	MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3::get_offset_of_count_7(),
	MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3::get_offset_of_x_8(),
	MD2Managed_t90C3C60E33C09EB91A59C28E21E858A3CC56BAA3_StaticFields::get_offset_of_PI_SUBST_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (MD4_t089B8663437920C4CA21F863DD9E87FB3B95E023), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (MD4Managed_tD87CADDB8B09C3BE9F77AEC14BA1E56765908F47), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1864[5] = 
{
	MD4Managed_tD87CADDB8B09C3BE9F77AEC14BA1E56765908F47::get_offset_of_state_4(),
	MD4Managed_tD87CADDB8B09C3BE9F77AEC14BA1E56765908F47::get_offset_of_buffer_5(),
	MD4Managed_tD87CADDB8B09C3BE9F77AEC14BA1E56765908F47::get_offset_of_count_6(),
	MD4Managed_tD87CADDB8B09C3BE9F77AEC14BA1E56765908F47::get_offset_of_x_7(),
	MD4Managed_tD87CADDB8B09C3BE9F77AEC14BA1E56765908F47::get_offset_of_digest_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (PKCS1_tB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B), -1, sizeof(PKCS1_tB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1865[4] = 
{
	PKCS1_tB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_StaticFields::get_offset_of_emptySHA1_0(),
	PKCS1_tB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_StaticFields::get_offset_of_emptySHA256_1(),
	PKCS1_tB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_StaticFields::get_offset_of_emptySHA384_2(),
	PKCS1_tB286C1D33BAF0F9EF5071ACDCB50E025C5008D2B_StaticFields::get_offset_of_emptySHA512_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (PKCS8_tE374B294C7A9FC98A06529BA5AA4CB40800D35DF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (PrivateKeyInfo_t9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1867[4] = 
{
	PrivateKeyInfo_t9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D::get_offset_of__version_0(),
	PrivateKeyInfo_t9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D::get_offset_of__algorithm_1(),
	PrivateKeyInfo_t9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D::get_offset_of__key_2(),
	PrivateKeyInfo_t9FC5F40611B515FCAE76EB2A7164704FAC7C9B7D::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (EncryptedPrivateKeyInfo_tEC05D1815F3B6911C2A72B36A0519FA958BFA86E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[4] = 
{
	EncryptedPrivateKeyInfo_tEC05D1815F3B6911C2A72B36A0519FA958BFA86E::get_offset_of__algorithm_0(),
	EncryptedPrivateKeyInfo_tEC05D1815F3B6911C2A72B36A0519FA958BFA86E::get_offset_of__salt_1(),
	EncryptedPrivateKeyInfo_tEC05D1815F3B6911C2A72B36A0519FA958BFA86E::get_offset_of__iterations_2(),
	EncryptedPrivateKeyInfo_tEC05D1815F3B6911C2A72B36A0519FA958BFA86E::get_offset_of__data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (RC4_t15F68CBEADB40270775AA750D85808CC5FE85F16), -1, sizeof(RC4_t15F68CBEADB40270775AA750D85808CC5FE85F16_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1869[2] = 
{
	RC4_t15F68CBEADB40270775AA750D85808CC5FE85F16_StaticFields::get_offset_of_s_legalBlockSizes_10(),
	RC4_t15F68CBEADB40270775AA750D85808CC5FE85F16_StaticFields::get_offset_of_s_legalKeySizes_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[13] = 
{
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_isCRTpossible_2(),
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_keyBlinding_3(),
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_keypairGenerated_4(),
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_m_disposed_5(),
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_d_6(),
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_p_7(),
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_q_8(),
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_dp_9(),
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_dq_10(),
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_qInv_11(),
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_n_12(),
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_e_13(),
	RSAManaged_tDCF904147E249A455F681A10FE7CCBA6250E44EA::get_offset_of_KeyGenerated_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (KeyGeneratedEventHandler_t40F3B0E6400CF6FB02BED8AB74B8BFEC1DBA5042), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (SafeBag_tDDDE39DC5EA8E3F77F5E1502E73A97224B77AAC3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[2] = 
{
	SafeBag_tDDDE39DC5EA8E3F77F5E1502E73A97224B77AAC3::get_offset_of__bagOID_0(),
	SafeBag_tDDDE39DC5EA8E3F77F5E1502E73A97224B77AAC3::get_offset_of__asn1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (PKCS12_tED2078A34511432AD97B7B647986EF0858868441), -1, sizeof(PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1873[17] = 
{
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields::get_offset_of_recommendedIterationCount_0(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441::get_offset_of__password_1(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441::get_offset_of__keyBags_2(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441::get_offset_of__secretBags_3(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441::get_offset_of__certs_4(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441::get_offset_of__keyBagsChanged_5(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441::get_offset_of__secretBagsChanged_6(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441::get_offset_of__certsChanged_7(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441::get_offset_of__iterations_8(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441::get_offset_of__safeBags_9(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441::get_offset_of__rng_10(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields::get_offset_of_password_max_length_11(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_12(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_13(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_14(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_15(),
	PKCS12_tED2078A34511432AD97B7B647986EF0858868441_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460), -1, sizeof(DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1874[7] = 
{
	DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460_StaticFields::get_offset_of_keyDiversifier_0(),
	DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460_StaticFields::get_offset_of_ivDiversifier_1(),
	DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460_StaticFields::get_offset_of_macDiversifier_2(),
	DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460::get_offset_of__hashName_3(),
	DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460::get_offset_of__iterations_4(),
	DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460::get_offset_of__password_5(),
	DeriveBytes_t9C20DD7277261025CA74878A4C04D066B5211460::get_offset_of__salt_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4), -1, sizeof(X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1875[15] = 
{
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_countryName_0(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_organizationName_1(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_organizationalUnitName_2(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_commonName_3(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_localityName_4(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_stateOrProvinceName_5(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_streetAddress_6(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_domainComponent_7(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_userid_8(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_email_9(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_dnQualifier_10(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_title_11(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_surname_12(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_givenName_13(),
	X501_tE168FA15DF59DEE465AFDD39E5B8B51B857605F4_StaticFields::get_offset_of_initial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7), -1, sizeof(X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1876[26] = 
{
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_decoder_0(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_m_encodedcert_1(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_m_from_2(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_m_until_3(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_issuer_4(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_m_issuername_5(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_m_keyalgo_6(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_m_keyalgoparams_7(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_subject_8(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_m_subject_9(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_m_publickey_10(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_signature_11(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_m_signaturealgo_12(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_m_signaturealgoparams_13(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_certhash_14(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of__rsa_15(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of__dsa_16(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_version_17(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_serialnumber_18(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_issuerUniqueID_19(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_subjectUniqueID_20(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7::get_offset_of_extensions_21(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7_StaticFields::get_offset_of_encoding_error_22(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_23(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7_StaticFields::get_offset_of_U3CU3Ef__switchU24map10_24(),
	X509Certificate_tF81384408319B1A7C4393A41881A123CF518E4A7_StaticFields::get_offset_of_U3CU3Ef__switchU24map11_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (X509CertificateCollection_t59402ED1601796E9D33AA78F60D998BC0DDA12F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (X509CertificateEnumerator_t73F2B8AEDEF6F187870E8A2750847EEE931E16D3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[1] = 
{
	X509CertificateEnumerator_t73F2B8AEDEF6F187870E8A2750847EEE931E16D3::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (X509Chain_t4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[5] = 
{
	X509Chain_t4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67::get_offset_of_roots_0(),
	X509Chain_t4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67::get_offset_of_certs_1(),
	X509Chain_t4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67::get_offset_of__root_2(),
	X509Chain_t4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67::get_offset_of__chain_3(),
	X509Chain_t4EEB0B913205B0D6B063AFB393AFA17BEFCA3D67::get_offset_of__status_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (X509ChainStatusFlags_t1256D2CC5C46DA044CAF8A56776A978115A2B79D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1880[8] = 
{
	X509ChainStatusFlags_t1256D2CC5C46DA044CAF8A56776A978115A2B79D::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA), -1, sizeof(X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1881[11] = 
{
	X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA::get_offset_of_issuer_0(),
	X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA::get_offset_of_version_1(),
	X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA::get_offset_of_thisUpdate_2(),
	X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA::get_offset_of_nextUpdate_3(),
	X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA::get_offset_of_entries_4(),
	X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA::get_offset_of_signatureOID_5(),
	X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA::get_offset_of_signature_6(),
	X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA::get_offset_of_extensions_7(),
	X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA::get_offset_of_encoded_8(),
	X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA::get_offset_of_hash_value_9(),
	X509Crl_t0F4FF7AC198D6861956B4C959663AA0C23A534BA_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (X509CrlEntry_tBF2584E52778416F84EA34B6F693BF1BED81EFF1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[3] = 
{
	X509CrlEntry_tBF2584E52778416F84EA34B6F693BF1BED81EFF1::get_offset_of_sn_0(),
	X509CrlEntry_tBF2584E52778416F84EA34B6F693BF1BED81EFF1::get_offset_of_revocationDate_1(),
	X509CrlEntry_tBF2584E52778416F84EA34B6F693BF1BED81EFF1::get_offset_of_extensions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1883[3] = 
{
	X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698::get_offset_of_extnOid_0(),
	X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698::get_offset_of_extnCritical_1(),
	X509Extension_tE4755045C6BD26F3323152F1E6F86FCDC72D6698::get_offset_of_extnValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[1] = 
{
	X509ExtensionCollection_t755DFCBDA8823D949578ED886BAFA4BF87706991::get_offset_of_readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1885[4] = 
{
	X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302::get_offset_of__storePath_0(),
	X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302::get_offset_of__certificates_1(),
	X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302::get_offset_of__crls_2(),
	X509Store_t754E7C7E6C15F74AA8E7A3F0695EE7E1AD0C4302::get_offset_of__crl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (X509StoreManager_t84D6FAF6F4DB5D367306FC494991364A0752BDB7), -1, sizeof(X509StoreManager_t84D6FAF6F4DB5D367306FC494991364A0752BDB7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1886[2] = 
{
	X509StoreManager_t84D6FAF6F4DB5D367306FC494991364A0752BDB7_StaticFields::get_offset_of__userStore_0(),
	X509StoreManager_t84D6FAF6F4DB5D367306FC494991364A0752BDB7_StaticFields::get_offset_of__machineStore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[2] = 
{
	X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28::get_offset_of__storePath_0(),
	X509Stores_t2DA9540B452CEB83E67BC6CDF9B0346FA2B06C28::get_offset_of__trusted_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (AuthorityKeyIdentifierExtension_t76714D3A7DEB58D0D703AC8AD0F7D17F4430FDB3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[1] = 
{
	AuthorityKeyIdentifierExtension_t76714D3A7DEB58D0D703AC8AD0F7D17F4430FDB3::get_offset_of_aki_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (BasicConstraintsExtension_t519E68578F3BFB9B3588596B24BF4B729D26494E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[2] = 
{
	BasicConstraintsExtension_t519E68578F3BFB9B3588596B24BF4B729D26494E::get_offset_of_cA_3(),
	BasicConstraintsExtension_t519E68578F3BFB9B3588596B24BF4B729D26494E::get_offset_of_pathLenConstraint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (ExtendedKeyUsageExtension_tC9B7CEC7328C06886E6F95EC50C8FE821ECCD8A1), -1, sizeof(ExtendedKeyUsageExtension_tC9B7CEC7328C06886E6F95EC50C8FE821ECCD8A1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1890[2] = 
{
	ExtendedKeyUsageExtension_tC9B7CEC7328C06886E6F95EC50C8FE821ECCD8A1::get_offset_of_keyPurpose_3(),
	ExtendedKeyUsageExtension_tC9B7CEC7328C06886E6F95EC50C8FE821ECCD8A1_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[5] = 
{
	GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45::get_offset_of_rfc822Name_0(),
	GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45::get_offset_of_dnsName_1(),
	GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45::get_offset_of_directoryNames_2(),
	GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45::get_offset_of_uris_3(),
	GeneralNames_tADF51C0BC03F95F71335B31D2E417D8A6E1F9F45::get_offset_of_ipAddr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (KeyUsages_tB76B0C5F4E8A5C3C1ADD8E5036836F9513550A76)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1892[11] = 
{
	KeyUsages_tB76B0C5F4E8A5C3C1ADD8E5036836F9513550A76::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (KeyUsageExtension_tA1D7257D3C3251C4199CD4B7DAEBB97E15CA895C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1893[1] = 
{
	KeyUsageExtension_tA1D7257D3C3251C4199CD4B7DAEBB97E15CA895C::get_offset_of_kubits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (NetscapeCertTypeExtension_t8C5A13576C3C530FB3D477C716AA0E2D89868973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[1] = 
{
	NetscapeCertTypeExtension_t8C5A13576C3C530FB3D477C716AA0E2D89868973::get_offset_of_ctbits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (CertTypes_t3C0F8B516A7FA9EFAD20B3AAC9BF91F97B674825)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1895[8] = 
{
	CertTypes_t3C0F8B516A7FA9EFAD20B3AAC9BF91F97B674825::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (SubjectAltNameExtension_tAA251D1FFC8AC88B0F91F3188F5F7F0A5CDA864D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1896[1] = 
{
	SubjectAltNameExtension_tAA251D1FFC8AC88B0F91F3188F5F7F0A5CDA864D::get_offset_of__names_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (HMAC_tEEBB2925FEAD31F6F5009153CF0056C327490FA1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[4] = 
{
	HMAC_tEEBB2925FEAD31F6F5009153CF0056C327490FA1::get_offset_of_hash_5(),
	HMAC_tEEBB2925FEAD31F6F5009153CF0056C327490FA1::get_offset_of_hashing_6(),
	HMAC_tEEBB2925FEAD31F6F5009153CF0056C327490FA1::get_offset_of_innerPad_7(),
	HMAC_tEEBB2925FEAD31F6F5009153CF0056C327490FA1::get_offset_of_outerPad_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (MD5SHA1_tC00142BACF5232E18AF953932BB12EB25ED58539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1898[3] = 
{
	MD5SHA1_tC00142BACF5232E18AF953932BB12EB25ED58539::get_offset_of_md5_4(),
	MD5SHA1_tC00142BACF5232E18AF953932BB12EB25ED58539::get_offset_of_sha_5(),
	MD5SHA1_tC00142BACF5232E18AF953932BB12EB25ED58539::get_offset_of_hashing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (AlertLevel_t9C9CDF74FA43002F88644710D78D35D7C1BDE8DF)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1899[3] = 
{
	AlertLevel_t9C9CDF74FA43002F88644710D78D35D7C1BDE8DF::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
